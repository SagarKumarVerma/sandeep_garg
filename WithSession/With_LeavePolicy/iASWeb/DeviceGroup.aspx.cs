﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using FKWeb;
using Newtonsoft.Json.Linq;
using DevExpress.Web;
public partial class DeviceGroup : System.Web.UI.Page
{
    OleDbConnection MyConn = new OleDbConnection();
    OleDbCommand MyCmd = new OleDbCommand();
    Class_Connection cn = new Class_Connection();
    public string KeyValue = "";
    public string TableMain = null;
    public string TableName = null;
    public string ColumnName = null;
    const int GET_DEVICE_STATUS = 1;
    ErrorClass ec = new ErrorClass();
    string sSql = string.Empty;
    SqlConnection msqlConn;
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["TimeWatchConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            if (Session["LoginUserType"] == null || Session["LoginUserType"].ToString().Trim() == "U")
            {
                Response.Redirect("Login.aspx");
            }
            //GetDeviceType();
            GetGroupAll();
            msqlConn = FKWebTools.GetDBPool();
        }
    }
    private void GetDeviceType()
    {
        try
        {
            if (Session["DeviceModel"].ToString() == null)
            {
                Response.Redirect("Login.aspx");
            }
            if (Session["DeviceModel"].ToString().Trim() == "B")
            {
                GetGroupBio();
            }
            else if (Session["DeviceModel"].ToString().Trim() == "H")
            {
                GetGroupHT();
            }
            else if (Session["DeviceModel"].ToString().Trim() == "Z")
            {
                GetGroupZK();
            }
            else
            {

            }

        }
        catch (Exception Ex)
        {
            Error_Occured("GetGroup", Ex.Message);
        }

    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        //Call The function to write the error log file 

        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }
    private void AddError(Dictionary<GridViewColumn, string> errors, GridViewColumn column, string errorText)
    {
        if (!errors.ContainsKey(column))
            errors[column] = errorText;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        //if (Session["DeviceModel"].ToString().Trim() == "B")
        //{
        //    GetGroupBio();
        //}
        //else if (Session["DeviceModel"].ToString().Trim() == "H")
        //{
        //    GetGroupHT();
        //}
        //else if (Session["DeviceModel"].ToString().Trim() == "Z")
        //{
        //    GetGroupZK();
        //}
        GetGroupAll();
        ASPxGridView1.DataBind();
    }
    public void GetGroupBio()
    {
        try
        {

            DataTable DT = new DataTable();
            DT.Columns.Add("GroupID");
            DT.Columns.Add("GroupName");
            DT.Columns.Add("Device");



            string str = "";
            DataSet DSDev = new DataSet();
            if (Session["username"].ToString().Trim().ToUpper() == "ADMIN")
            {
                str = "select * from BioDeviceGroup order by 1  ";
            }
            else
            {
                str = "select * from BioDeviceGroup where groupid in ('" + Session["Group"] + "') order by 1  ";
            }
            DSDev = cn.FillDataSet(str);

            if (DSDev.Tables[0].Rows.Count > 0)
            {
                for (int D = 0; D < DSDev.Tables[0].Rows.Count; D++)
                {
                    DT.Rows.Add(DSDev.Tables[0].Rows[D]["GroupID"].ToString().Trim(), DSDev.Tables[0].Rows[D]["GroupName"].ToString().Trim(), "BIO");
                }
            }

            if (DT.Rows.Count > 0)
            {
                ASPxGridView1.DataSource = DT;
                ASPxGridView1.KeyFieldName = "GroupID";
                ASPxGridView1.DataBind();
            }
            else
            {
                ASPxGridView1.DataSource = null;
                ASPxGridView1.DataBind();
            }


        }

        catch (Exception Ex)
        {
            Error_Occured("GetGroup", Ex.Message);
        }

    }

    public void GetGroupAll()
    {
        try
        {
            DataTable DT = new DataTable();
            DT.Columns.Add("GroupID");
            DT.Columns.Add("GroupName");
            DT.Columns.Add("Device");



            string str = "";
            DataSet DSDev = new DataSet();
            if (Session["username"].ToString().Trim().ToUpper() == "ADMIN")
            {
                str = "select * from BioDeviceGroup order by 1  ";
            }
            else
            {
                str = "select * from BioDeviceGroup where groupid in ('" + Session["Group"] + "') order by 1  ";
            }
            DSDev = cn.FillDataSet(str);

            if (DSDev.Tables[0].Rows.Count > 0)
            {
                for (int D = 0; D < DSDev.Tables[0].Rows.Count; D++)
                {
                    DT.Rows.Add(DSDev.Tables[0].Rows[D]["GroupID"].ToString().Trim(), DSDev.Tables[0].Rows[D]["GroupName"].ToString().Trim(), "BIO");
                }
            }
            str = "select * from ClientDeviceGroup where DeviceType='ZK'";
            DataSet DsZK = new DataSet();
            DsZK = cn.FillDataSet(str);
            if (DsZK.Tables[0].Rows.Count > 0)
            {
                for (int Z = 0; Z < DsZK.Tables[0].Rows.Count; Z++)
                {
                    DT.Rows.Add(DsZK.Tables[0].Rows[Z]["GroupID"].ToString().Trim(), DsZK.Tables[0].Rows[Z]["GroupName"].ToString().Trim(), "ZK");
                }
            }
            str = "select * from ClientDeviceGroup where DeviceType='HTSeries'";
            DsZK = new DataSet();
            DsZK = cn.FillDataSet(str);
            if (DsZK.Tables[0].Rows.Count > 0)
            {
                for (int Z = 0; Z < DsZK.Tables[0].Rows.Count; Z++)
                {
                    DT.Rows.Add(DsZK.Tables[0].Rows[Z]["GroupID"].ToString().Trim(), DsZK.Tables[0].Rows[Z]["GroupName"].ToString().Trim(), "HTSeries");
                }
            }

            if (DT.Rows.Count > 0)
            {
                ASPxGridView1.DataSource = DT;
                ASPxGridView1.KeyFieldName = "GroupID";
                ASPxGridView1.DataBind();
            }
            else
            {
                ASPxGridView1.DataSource = null;
                ASPxGridView1.DataBind();
            }
        }
        catch (Exception Ex)
        {
            Error_Occured("GetGroupAll", Ex.Message);
        }
    }


    public void GetGroupHT()
    {
        try
        {

            DataTable DT = new DataTable();
            DT.Columns.Add("GroupID");
            DT.Columns.Add("GroupName");
            DT.Columns.Add("Device");



            string str = "";
            DataSet DSDev = new DataSet();
            str = "select * from ClientDeviceGroup where DeviceType='HTSeries'";
            DataSet DsZK = new DataSet();
            DsZK = cn.FillDataSet(str);
            if (DsZK.Tables[0].Rows.Count > 0)
            {
                for (int Z = 0; Z < DsZK.Tables[0].Rows.Count; Z++)
                {
                    DT.Rows.Add(DsZK.Tables[0].Rows[Z]["GroupID"].ToString().Trim(), DsZK.Tables[0].Rows[Z]["GroupName"].ToString().Trim(), "HTSeries");
                }
            }
            if (DT.Rows.Count > 0)
            {
                ASPxGridView1.DataSource = DT;
                ASPxGridView1.KeyFieldName = "GroupID";
                ASPxGridView1.DataBind();
            }
            else
            {
                ASPxGridView1.DataSource = null;
                ASPxGridView1.DataBind();
            }


        }

        catch (Exception Ex)
        {
            Error_Occured("GetGroupHT", Ex.Message);
        }

    }

    public void GetGroupZK()
    {
        try
        {

            DataTable DT = new DataTable();
            DT.Columns.Add("GroupID");
            DT.Columns.Add("GroupName");
            DT.Columns.Add("Device");



            string str = "";

            str = "select * from ClientDeviceGroup where DeviceType='ZK'";
            DataSet DsZK = new DataSet();
            DsZK = cn.FillDataSet(str);
            if (DsZK.Tables[0].Rows.Count > 0)
            {
                for (int Z = 0; Z < DsZK.Tables[0].Rows.Count; Z++)
                {
                    DT.Rows.Add(DsZK.Tables[0].Rows[Z]["GroupID"].ToString().Trim(), DsZK.Tables[0].Rows[Z]["GroupName"].ToString().Trim(), "ZK");
                }
            }
            if (DT.Rows.Count > 0)
            {
                ASPxGridView1.DataSource = DT;
                ASPxGridView1.KeyFieldName = "GroupID";
                ASPxGridView1.DataBind();
            }
            else
            {
                ASPxGridView1.DataSource = null;
                ASPxGridView1.DataBind();
            }


        }

        catch (Exception Ex)
        {
            Error_Occured("GetGroupZK", Ex.Message);
        }

    }
    protected void ASPxGridView1_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.Grid.IsNewRowEditing == false)
        {
            if (e.Column.FieldName == "GroupID")
            {
                e.Editor.ReadOnly = true;

            }

        }
        else
        {
            e.Editor.ReadOnly = false;
        }

    }
    private bool ValidData(string CCode)
    {
        bool Check = false;
        try
        {
            sSql = "Select * from tbl_fkdevice_status where GroupID like '%" + CCode.ToString().Trim() + "%' ";
            DataSet DsCheck = new DataSet();
            DsCheck = cn.FillDataSet(sSql);
            if (DsCheck.Tables[0].Rows.Count > 0)
            {
                Check = true;

            }

        }
        catch (Exception Ex)
        {
            Error_Occured("ValidData", Ex.Message);
        }

        return Check;
    }
    protected void ASPxGridView1_RowDeleting1(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        try
        {
            e.Cancel = true;
            string IsMaster = Convert.ToString(e.Keys["GroupID"]);
            bool Validate = false;
            Validate = ValidData(IsMaster);
            if (Validate == true)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Group Already Assigned To Device');document.location='GroupMapping.aspx'", true);
            }
            else
            {
                string sSql = "delete from BioDeviceGroup where GroupID='" + IsMaster + "'   ";
                cn.execute_NonQuery(sSql);

            }
            GetGroupBio();
        }
        catch (Exception Ex)
        {
            Error_Occured("ASPxGridView1_RowDeleting1", Ex.Message);
        }
    }

    protected void ASPxGridView1_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        try
        {

            string sSql = "";
            string IsMaster = Convert.ToString(e.NewValues["GroupID"]);
            string DevType = Convert.ToString(e.NewValues["Device"]);

            if (DevType.Trim() == "B")
            {
                sSql = "Insert Into BioDeviceGroup (GroupID,GroupName) Values( '" + IsMaster.ToString().Trim() + "', '" + e.NewValues["GroupName"].ToString().Trim() + "')";
                cn.execute_NonQuery(sSql);
                e.Cancel = true;
                ASPxGridView1.CancelEdit();
                GetGroupAll();
            }
            else if (DevType.Trim() == "Z")
            {
                sSql = "insert into ClientDeviceGroup (GroupID,GroupName,DeviceType,ClientID) values ('" + IsMaster.ToString().Trim() + "','" + e.NewValues["GroupName"].ToString().Trim() + "','ZK',"+ DataFilter.ClientID +")";
                cn.execute_NonQuery(sSql);
                e.Cancel = true;
                ASPxGridView1.CancelEdit();
                GetGroupAll();
            }

            else if (DevType.Trim() == "H")
            {
                sSql = "insert into ClientDeviceGroup (GroupID,GroupName,DeviceType,ClientID) values ('" + IsMaster.ToString().Trim() + "','" + e.NewValues["GroupName"].ToString().Trim() + "','HTSeries'," + DataFilter.ClientID + ")";
                cn.execute_NonQuery(sSql);
                e.Cancel = true;
                ASPxGridView1.CancelEdit();
                GetGroupAll();
            }

        }
        catch (Exception Ex)
        {
            Error_Occured("ASPxGridView1_RowInserting", Ex.Message);
        }
    }

    protected void ASPxGridView1_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        try
        {
            string sSql = "";


            string DevType = Session["DeviceModel"].ToString().Trim();
            if (DevType.Trim() == "B")
            {
                sSql = "update BioDeviceGroup set GroupName= '" + e.NewValues["GroupName"].ToString().Trim() + "' where " +
                      " GroupID='" + e.NewValues["GroupID"].ToString().Trim() + "'";
                cn.execute_NonQuery(sSql);
                ASPxGridView1.CancelEdit();
                e.Cancel = true;
                GetGroupBio();
            }
            else if (DevType.Trim() == "Z")
            {
                sSql = "update ClientDeviceGroup set GroupName= '" + e.NewValues["GroupName"].ToString().Trim() + "' where " +
                      " GroupID='" + e.NewValues["GroupID"].ToString().Trim() + "'";
                cn.execute_NonQuery(sSql);
                ASPxGridView1.CancelEdit();
                e.Cancel = true;
                GetGroupZK();
            }
            else if (DevType.Trim() == "H")
            {
                sSql = "update ClientDeviceGroup set GroupName= '" + e.NewValues["GroupName"].ToString().Trim() + "' where " +
                      " GroupID='" + e.NewValues["GroupID"].ToString().Trim() + "'";
                cn.execute_NonQuery(sSql);
                ASPxGridView1.CancelEdit();
                e.Cancel = true;
                GetGroupHT();
            }


        }
        catch (Exception Ex)
        {
            Error_Occured("ASPxGridView1_RowUpdating", Ex.Message);
        }
    }

    protected void ASPxGridView1_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
    {
        string IsMaster = Convert.ToString(e.NewValues["GroupID"]);
        string DeviceType = "";

        try
        {
            DeviceType = Convert.ToString(e.NewValues["Device"]);
        }
        catch
        {
            DeviceType = "";

        }



        if (e.IsNewRow == true)
        {
            if (DeviceType.Trim() == "")
            {
                AddError(e.Errors, ASPxGridView1.Columns[0], "Select Device Type");
                e.RowError = "*Select Device Type";
                return;
            }

            else if (DeviceType.Trim() == "B")
            {
                sSql = "Select GroupID from BioDeviceGroup where GroupID='" + IsMaster + "' ";
            }
            else
            {
                sSql = "Select GroupID from ClientDeviceGroup where GroupID='" + IsMaster + "' ";
            }

            DataSet DsCheck = new DataSet();
            DsCheck = cn.FillDataSet(sSql);
            if (DsCheck.Tables[0].Rows.Count > 0)
            {
                AddError(e.Errors, ASPxGridView1.Columns[0], "Group Code Already Exist");
                e.RowError = "*Group Code Already Exist";
                return;
            }




        }
    }


}