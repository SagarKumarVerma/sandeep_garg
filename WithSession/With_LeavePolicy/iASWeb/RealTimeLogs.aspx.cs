﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using FKWeb;
using Newtonsoft.Json.Linq;
using System.Text;
using DevExpress.Web;


public partial class RealTimeLogs : System.Web.UI.Page
{
    OleDbConnection MyConn = new OleDbConnection();
    OleDbCommand MyCmd = new OleDbCommand();
    Class_Connection cn = new Class_Connection();
    public string KeyValue = "";
    public string TableMain = null;
    public string TableName = null;
    public string ColumnName = null;
    const int GET_DEVICE_STATUS = 1;
    DateTime FromDate = System.DateTime.MinValue;
    DateTime ToDate = System.DateTime.MinValue;


    ErrorClass ec = new ErrorClass();

    SqlConnection msqlConn;
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SqlConnFkWeb"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            if (Session["LoginUserType"] == null || Session["LoginUserType"].ToString().Trim() == "U")
            {
                Response.Redirect("Login.aspx");
            }
            GetLogs();
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        GetLogs();
        ASPxGridView1.DataBind();
    }
        
    public void GetLogs()
    {
        try
        {
            DataTable DT = new DataTable();
            DT.Columns.Add("user_id");
            DT.Columns.Add("EMPNAME");
            DT.Columns.Add("io_time");
            DT.Columns.Add("device_id");
            DT.Columns.Add("verify_mode");
            DT.Columns.Add("io_mode");
            DT.Columns.Add("io_workcode");
           // DT.Columns.Add("io_workcode");


            string str = "";
            DataSet DSDev = new DataSet();
            if (Session["username"].ToString().Trim().ToUpper() == "ADMIN")
            {

                str = "select RT.device_id,dev.device_name,RT.user_id,TE.EMPNAME,rt.io_time,rt.verify_mode,rt.io_workcode,rt.io_mode from tbl_realtime_glog RT inner join tbl_fkdevice_status Dev on " +
                      "   RT.device_id=Dev.device_id left join tblemployee TE on te.PRESENTCARDNO=rt.user_id order by IO_Time desc ";
            }
            else
            {

                str = "select RT.device_id,dev.device_name,RT.user_id,TE.EMPNAME,rt.io_time,rt.verify_mode,rt.io_workcode,rt.io_mode  from tbl_realtime_glog RT inner join tbl_fkdevice_status Dev on " +
                       "   RT.device_id=Dev.device_id left join tblemployee TE on te.PRESENTCARDNO=rt.user_id where  " +
                       "  Dev.groupid in ('" + Session["Group"] + "') order by IO_Time desc ";

            }
            DSDev = cn.FillDataSet(str);
            if (DSDev.Tables[0].Rows.Count > 0)
            {
                for (int R = 0; R < DSDev.Tables[0].Rows.Count; R++)
                {
                    DT.Rows.Add(DSDev.Tables[0].Rows[R]["user_id"], DSDev.Tables[0].Rows[R]["EMPNAME"], DSDev.Tables[0].Rows[R]["io_time"],
                    DSDev.Tables[0].Rows[R]["device_id"], DSDev.Tables[0].Rows[R]["verify_mode"], DSDev.Tables[0].Rows[R]["io_mode"], DSDev.Tables[0].Rows[R]["io_workcode"]);
                }

            }

            str = " select UA.rowid,UA.DeviceID 'device_id',isnull(TM.Branch,'NA') DevName,UA.UserID 'user_id',CASE WHEN UA.AttState = 0 THEN 'IN' WHEN UA.AttState = 1 THEN 'Out' WHEN UA.AttState = 2 THEN 'Break Out' WHEN UA.AttState = 3 THEN " +
                  " 'Break In' WHEN UA.AttState = 4 THEN 'OT In' WHEN UA.AttState = 5 THEN 'OT Out' ELSE UA.AttState END as AttState,CASE WHEN UA.VerifyMode = 1 THEN 'Finger' WHEN UA.VerifyMode = 4 " +
                  " THEN 'Card' WHEN UA.VerifyMode = 15 THEN 'Face'  WHEN UA.VerifyMode = 3 THEN 'Pin' ELSE UA.VerifyMode END as verify_mode,UA.WorkCode 'io_mode' ,UA.AttDateTime 'io_time' from UserAttendance UA inner join TblMachine TM on" +
                  " TM.SerialNumber=UA.DeviceID order by AttDateTime desc";
            DataSet DsZk = new DataSet();
            DsZk = cn.FillDataSet(str);
            if (DsZk.Tables[0].Rows.Count > 0)
            {
                for (int Z = 0; Z < DsZk.Tables[0].Rows.Count; Z++)
                {
                    DT.Rows.Add(DsZk.Tables[0].Rows[Z]["user_id"], "", DsZk.Tables[0].Rows[Z]["io_time"],
                        DsZk.Tables[0].Rows[Z]["device_id"], DsZk.Tables[0].Rows[Z]["verify_mode"], DsZk.Tables[0].Rows[Z]["io_mode"], "");
                }

            }
         if (DT.Rows.Count > 0)
            {
                ASPxGridView1.DataSource = DT;
                ASPxGridView1.DataBind();
            }
            else
            {
                ASPxGridView1.DataSource = null;
                ASPxGridView1.DataBind();
            }


        }

        catch (Exception er)
        {
            Error_Occured("GetGroup", er.Message);
        }

    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        //Call The function to write the error log file 

        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
        }
    }
}