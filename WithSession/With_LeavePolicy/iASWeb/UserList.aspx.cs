﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using FKWeb;
using Newtonsoft.Json.Linq;

public partial class UserList : System.Web.UI.Page
{
    OleDbCommand MyCmd = new OleDbCommand();
    Class_Connection cn = new Class_Connection();
    public string KeyValue = "";
    public string TableMain = null;
    public string TableName = null;
    public string ColumnName = null;
    const int GET_DEVICE_STATUS = 1;
    DateTime FromDate = System.DateTime.MinValue;
    DateTime ToDate = System.DateTime.MinValue;
    private String mDeviceId;
    ErrorClass ec = new ErrorClass();

    SqlConnection msqlConn;
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SqlConnFkWeb"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            if (Session["LoginUserType"] == null || Session["LoginUserType"].ToString().Trim() == "U")
            {
                Response.Redirect("Login.aspx");
            }
            msqlConn = FKWebTools.GetDBPool();
            //  GetDevice();
            GetUsers();

        }
    }
    public void GetUsers()
    {
        try
        {
            DataTable DT = new DataTable();
            DT.Columns.Add("DeviceID");
            DT.Columns.Add("DeviceType");
            DT.Columns.Add("UserID");
            DT.Columns.Add("UserName");
            DT.Columns.Add("DevName");

            string sSql = "select user_id,user_name,case when RIGHT(password,1)='?' then '' else 'PIN' end +''+case when card IS null then '' else 'Card' end+''+case when PHOTO is null then ''" +
                         " else 'Face' end+''+case when fp_0 is null then '' else 'FP' end as TemplateType,tbl_realtime_userinfo.Device_ID, tbl_fkdevice_status.device_name 'DevName' from tbl_realtime_userinfo inner join tbl_fkdevice_status on " +
                         " tbl_fkdevice_status.device_id = tbl_realtime_userinfo.Device_ID and tbl_fkdevice_status.CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"'  ";
            DataSet DsDev = new DataSet();
            DsDev = cn.FillDataSet(sSql);
            if (DsDev.Tables[0].Rows.Count > 0)
            {
                for (int B = 0; B < DsDev.Tables[0].Rows.Count; B++)
                {
                    DT.Rows.Add(DsDev.Tables[0].Rows[B]["Device_ID"].ToString().Trim(), "BIO", DsDev.Tables[0].Rows[B]["user_id"].ToString().Trim(), DsDev.Tables[0].Rows[B]["user_name"].ToString().Trim(),
                     DsDev.Tables[0].Rows[B]["DevName"].ToString().Trim());
                }
            }
            if (Session["username"].ToString().Trim().ToUpper() == "ADMIN")
            {
                sSql = " select UD.RowID,UD.DeviceID,ISNULL(TM.DeviceName,'NA') DevName,UD.UserID,UD.Name,UD.Pri,UD.Password,UD.Card,UD.DeviceGroup,UD.TimeZone,UD.Verify from " +
                 " UserDetail UD inner join tblMachine TM ON tm.SerialNumber=UD.DeviceID where tm.DeviceMode='ZKT'    order by UD.UserID ";
            }
            else
            {
                sSql = " select UD.RowID,UD.DeviceID,ISNULL(TM.DeviceName,'NA') DevName,UD.UserID,UD.Name,UD.Pri,UD.Password,UD.Card,UD.DeviceGroup,UD.TimeZone,UD.Verify from " +
                   " UserDetail UD inner join tblMachine TM ON tm.SerialNumber=UD.DeviceID  where   tm.CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' and  tm.DeviceMode='ZKT'    order by UD.UserID ";
            }
           
            DataSet DsZK = new DataSet();
            DsZK = cn.FillDataSet(sSql);
            if (DsZK.Tables[0].Rows.Count > 0)
            {
                for (int Z = 0; Z < DsZK.Tables[0].Rows.Count; Z++)
                {

                    DT.Rows.Add(DsZK.Tables[0].Rows[Z]["DeviceID"].ToString().Trim(), "ZK", DsZK.Tables[0].Rows[Z]["UserID"].ToString().Trim(), DsZK.Tables[0].Rows[Z]["Name"].ToString().Trim(),
                         DsZK.Tables[0].Rows[Z]["DevName"].ToString().Trim());
                }
            }

            if (Session["username"].ToString().Trim().ToUpper() == "ADMIN")
            {
                sSql = " select UD.RowID,UD.DeviceID,ISNULL(TM.DeviceName,'NA') DevName,UD.UserID,UD.Name,UD.Pri,UD.Password,UD.Card,UD.DeviceGroup,UD.TimeZone,UD.Verify from " +
                 " UserDetail UD inner join tblMachine TM ON tm.SerialNumber=UD.DeviceID where tm.DeviceMode='HTSeries'   order by UD.UserID ";
            }
            else
            {
                sSql = " select UD.RowID,UD.DeviceID,ISNULL(TM.DeviceName,'NA') DevName,UD.UserID,UD.Name,UD.Pri,UD.Password,UD.Card,UD.DeviceGroup,UD.TimeZone,UD.Verify from " +
                   " UserDetail UD inner join tblMachine TM ON tm.SerialNumber=UD.DeviceID  where   tm.CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'  and tm.DeviceMode='HTSeries'  order by UD.UserID ";
            }

            DataSet DsHT = new DataSet();
            DsHT = cn.FillDataSet(sSql);
            if (DsHT.Tables[0].Rows.Count > 0)
            {
                for (int Z = 0; Z < DsHT.Tables[0].Rows.Count; Z++)
                {

                    DT.Rows.Add(DsHT.Tables[0].Rows[Z]["DeviceID"].ToString().Trim(), "HT Series", DsHT.Tables[0].Rows[Z]["UserID"].ToString().Trim(), DsHT.Tables[0].Rows[Z]["Name"].ToString().Trim(),
                         DsHT.Tables[0].Rows[Z]["DevName"].ToString().Trim());
                }
            }

            if (DT.Rows.Count > 0)
            {
                ASPxGridView1.DataSource = DT;
                ASPxGridView1.DataBind();
            }
        }
        catch (Exception Exc)
        {
            Error_Occured("GetUsers", Exc.Message);

        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        GetUsers();
        ASPxGridView1.DataBind();
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        //Call The function to write the error log file 

        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }

    protected void ASPxGridView1_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
    {
        try
        {
            string DeviceID = Convert.ToString(e.GetValue("DeviceID"));
            string UID = Convert.ToString(e.GetValue("UserID"));
            string DeviceMode = Convert.ToString(e.GetValue("DeviceType"));
            string sSql = string.Empty;
            DataSet DsRec = new DataSet();
            if (DeviceMode.Trim() == "HT Series" || DeviceMode.Trim() == "ZK")
            {
               
                if (e.DataColumn.Caption.ToString().Trim() == "Face")
                {
                    sSql = "Select distinct UserID from userface Where DeviceID='" + DeviceID.Trim() + "' and UserID='" + UID + "' ";
                    DsRec = new DataSet();
                    DsRec = cn.FillDataSet(sSql);
                    if (DsRec.Tables[0].Rows.Count > 0)
                    {
                        e.Cell.Text = DsRec.Tables[0].Rows.Count.ToString();
                    }
                    else
                    {
                        e.Cell.Text = "0";
                    }
                }
                if (e.DataColumn.Caption.ToString().Trim() == "Finger")
                {
                    sSql = "Select distinct UserID from UserFinger Where DeviceID='" + DeviceID.Trim() + "' and UserID='" + UID + "' ";
                    DsRec = new DataSet();
                    DsRec = cn.FillDataSet(sSql);
                    if (DsRec.Tables[0].Rows.Count > 0)
                    {
                        e.Cell.Text = DsRec.Tables[0].Rows.Count.ToString();
                    }
                    else
                    {
                        e.Cell.Text = "0";
                    }
                }
                if (e.DataColumn.Caption.ToString().Trim() == "Card")
                {
                    sSql = "Select distinct UserID from UserDetail Where DeviceID='" + DeviceID.Trim() + "' and UserID='" + UID + "' and (Card is not null or Card!='') ";
                    DsRec = new DataSet();
                    DsRec = cn.FillDataSet(sSql);
                    if (DsRec.Tables[0].Rows.Count > 0)
                    {
                        e.Cell.Text = DsRec.Tables[0].Rows.Count.ToString();
                    }
                    else
                    {
                        e.Cell.Text = "0";
                    }
                }
                if (e.DataColumn.Caption.ToString().Trim() == "Pin")
                {
                    sSql = "Select distinct UserID from UserDetail Where DeviceID='" + DeviceID.Trim() + "' and UserID='" + UID + "' and (Password is not null and Password!='') ";
                    DsRec = new DataSet();
                    DsRec = cn.FillDataSet(sSql);
                    if (DsRec.Tables[0].Rows.Count > 0)
                    {
                        e.Cell.Text = DsRec.Tables[0].Rows.Count.ToString();
                    }
                    else
                    {
                        e.Cell.Text = "0";
                    }
                }
            }
            else if(DeviceMode.Trim()=="BIO")
            {
                if (e.DataColumn.Caption.ToString().Trim() == "Finger")
                {
                    sSql = "Select user_id  from tbl_fkcmd_trans_cmd_result_user_id_list  where device_id='" + DeviceID.Trim() + "' and backup_number<10";
                    DsRec = new DataSet();
                    DsRec = cn.FillDataSet(sSql);
                    if (DsRec.Tables[0].Rows.Count > 0)
                    {
                        e.Cell.Text = DsRec.Tables[0].Rows.Count.ToString();
                    }
                    else
                    {
                        e.Cell.Text = "0";
                    }
                }
                if (e.DataColumn.Caption.ToString().Trim() == "Face")
                {
                    sSql = "Select user_id  from tbl_fkcmd_trans_cmd_result_user_id_list  where device_id='" + DeviceID.Trim() + "' and backup_number=12";
                    DsRec = new DataSet();
                    DsRec = cn.FillDataSet(sSql);
                    if (DsRec.Tables[0].Rows.Count > 0)
                    {
                        e.Cell.Text = DsRec.Tables[0].Rows.Count.ToString();
                    }
                    else
                    {
                        e.Cell.Text = "0";
                    }
                }
                if (e.DataColumn.Caption.ToString().Trim() == "Pin")
                {
                    sSql = "Select user_id  from tbl_fkcmd_trans_cmd_result_user_id_list  where device_id='" + DeviceID.Trim() + "' and backup_number=10";
                    DsRec = new DataSet();
                    DsRec = cn.FillDataSet(sSql);
                    if (DsRec.Tables[0].Rows.Count > 0)
                    {
                        e.Cell.Text = DsRec.Tables[0].Rows.Count.ToString();
                    }
                    else
                    {
                        e.Cell.Text = "0";
                    }
                }
                if (e.DataColumn.Caption.ToString().Trim() == "Card")
                {
                    sSql = "Select user_id  from tbl_fkcmd_trans_cmd_result_user_id_list  where device_id='" + DeviceID.Trim() + "' and backup_number=11";
                    DsRec = new DataSet();
                    DsRec = cn.FillDataSet(sSql);
                    if (DsRec.Tables[0].Rows.Count > 0)
                    {
                        e.Cell.Text = DsRec.Tables[0].Rows.Count.ToString();
                    }
                    else
                    {
                        e.Cell.Text = "0";
                    }
                }
            }
        }
        catch
        {

        }
    }
}