﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="frmLeaveAccural.aspx.cs" Inherits="frmLeaveAccural" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
     <script language="javascript" src="JS/Leave.js" type="text/javascript"></script>
    <table align="center" style="width:100%" cellpadding="0" cellspacing="0" >
<tr>
<td style="text-align:center" >Leave Accural<br />
    </td>
</tr>

<tr>
<td align="center">


                                        
  </td>
</tr>
        <tr>
<td >
    <dx:ASPxFormLayout ID="formLayout" runat="server" AlignItemCaptionsInAllGroups="True" UseDefaultPaddings="False">
            <SettingsAdaptivity AdaptivityMode="SingleColumnWindowLimit" SwitchToSingleColumnAtWindowInnerWidth="200" />
            <Items>

                <dx:EmptyLayoutItem />
                <dx:LayoutGroup Caption="Employee & Date Selection" ColCount="3" Width="100%">
                    <Items>
                        <dx:LayoutItem Caption="Employee Code">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                    <dx:ASPxComboBox ID="ddlPayCode" runat="server" ValueType="System.String" Width="200px" AutoPostBack="True" SelectedIndex="-1" NullText="NONE" OnSelectedIndexChanged="ddlPayCode_SelectedIndexChanged">
                                        <ClearButton DisplayMode="Always">
                                        </ClearButton>
                                    </dx:ASPxComboBox>
                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>

                        <dx:LayoutItem Caption="Employee Name">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                    <dx:ASPxLabel ID="lblName" runat="server" Text="Employee Name"></dx:ASPxLabel>
                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>
                        <dx:LayoutItem Caption="Department">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                    <dx:ASPxLabel ID="lblLocation" runat="server" Text="Department"></dx:ASPxLabel>
                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>
                        <dx:LayoutItem Caption="Leave Year">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server">
                                    <dx:ASPxTextBox runat="server" ID="txtYear" Width="200px" AutoPostBack="True" HelpText="Input Leave Year In YYYY Format">
                                        <MaskSettings ErrorText="*Invalid Input" Mask="0000" ShowHints="True" />
                                        <HelpTextSettings DisplayMode="Popup">
                                        </HelpTextSettings>
                                    </dx:ASPxTextBox>

                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>
                        <dx:LayoutItem Caption="Designation">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server">
                                    <dx:ASPxLabel ID="lblDesignation" runat="server" Text="Designation"></dx:ASPxLabel>

                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>
                        <dx:LayoutItem Caption="Company">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server">
                                    <dx:ASPxLabel ID="lblCompany" runat="server" Text="NA"></dx:ASPxLabel>

                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>
                    </Items>

                </dx:LayoutGroup>
                
                  <dx:LayoutGroup Caption="Leave Details" ColCount="1" Width="100%">
                    <Items>
                        <dx:LayoutItem Caption="">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                   
                                    <table style="width:auto">
                                        <tr>
                                            <td style="width: 150px">
                                                <dx:ASPxLabel ID="Label1" runat="server" Text="ASPxLabel" Visible="false">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td style="width: 100px">
                                                <dx:ASPxTextBox ID="TextBox1" runat="server" Width="80px" MaxLength="5">
                                                    <MaskSettings ErrorText="*Invalid Input" Mask="00.00" ShowHints="True" />
                                                   </dx:ASPxTextBox>
                                            </td>
                                             <td style="width: 150px">
                                                <dx:ASPxLabel ID="Label11" runat="server" Text="ASPxLabel"  Visible="false">
                                                </dx:ASPxLabel>
                                            </td>
                                             <td style="width: 100px">
                                                <dx:ASPxTextBox ID="TextBox11" runat="server" Width="80px">
                                                    <MaskSettings ErrorText="*Invalid Input" Mask="00.00" ShowHints="True" />
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 150px">
                                                <dx:ASPxLabel ID="Label2" runat="server" Text="ASPxLabel"  Visible="false">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td style="width: 100px">
                                                <dx:ASPxTextBox ID="TextBox2" runat="server" Width="80px">
                                                    <MaskSettings ErrorText="*Invalid Input" Mask="00.00" ShowHints="True" />
                                                </dx:ASPxTextBox>
                                            </td>
                                             <td style="width: 150px">
                                                <dx:ASPxLabel ID="Label12" runat="server" Text="ASPxLabel"  Visible="false">
                                                </dx:ASPxLabel>
                                            </td>
                                             <td style="width: 100px">
                                                <dx:ASPxTextBox ID="TextBox12" runat="server" Width="80px">
                                                    <MaskSettings ErrorText="*Invalid Input" Mask="00.00" ShowHints="True" />
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 150px">
                                                <dx:ASPxLabel ID="Label3" runat="server" Text="ASPxLabel"  Visible="false">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td style="width: 100px">
                                                <dx:ASPxTextBox ID="TextBox3" runat="server" Width="80px">
                                                    <MaskSettings ErrorText="*Invalid Input" Mask="00.00" ShowHints="True" />
                                                </dx:ASPxTextBox>
                                            </td>
                                             <td style="width: 150px">
                                                <dx:ASPxLabel ID="Label13" runat="server" Text="ASPxLabel"  Visible="false">
                                                </dx:ASPxLabel>
                                            </td>
                                             <td style="width: 100px">
                                                <dx:ASPxTextBox ID="TextBox13" runat="server" Width="80px">
                                                    <MaskSettings ErrorText="*Invalid Input" Mask="00.00" ShowHints="True" />
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 150px">
                                                <dx:ASPxLabel ID="Label4" runat="server" Text="ASPxLabel"  Visible="false">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td style="width: 100px">
                                                <dx:ASPxTextBox ID="TextBox4" runat="server" Width="80px">
                                                    <MaskSettings ErrorText="*Invalid Input" Mask="00.00" ShowHints="True" />
                                                </dx:ASPxTextBox>
                                            </td>
                                             <td style="width: 150px">
                                                <dx:ASPxLabel ID="Label14" runat="server" Text="ASPxLabel"  Visible="false">
                                                </dx:ASPxLabel>
                                            </td>
                                             <td style="width: 100px">
                                                <dx:ASPxTextBox ID="TextBox14" runat="server" Width="80px">
                                                    <MaskSettings ErrorText="*Invalid Input" Mask="00.00" ShowHints="True" />
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 150px">
                                                <dx:ASPxLabel ID="Label5" runat="server" Text="ASPxLabel"  Visible="false">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td style="width: 100px">
                                                <dx:ASPxTextBox ID="TextBox5" runat="server" Width="80px">
                                                    <MaskSettings ErrorText="*Invalid Input" Mask="00.00" ShowHints="True" />
                                                </dx:ASPxTextBox>
                                            </td>
                                             <td style="width: 150px">
                                                <dx:ASPxLabel ID="Label15" runat="server" Text="ASPxLabel"  Visible="false">
                                                </dx:ASPxLabel>
                                            </td>
                                             <td style="width: 100px">
                                                <dx:ASPxTextBox ID="TextBox15" runat="server" Width="80px">
                                                    <MaskSettings ErrorText="*Invalid Input" Mask="00.00" ShowHints="True" />
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 150px">
                                                <dx:ASPxLabel ID="Label6" runat="server" Text="ASPxLabel"  Visible="false">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td style="width: 100px">
                                                <dx:ASPxTextBox ID="TextBox6" runat="server" Width="80px">
                                                    <MaskSettings ErrorText="*Invalid Input" Mask="00.00" ShowHints="True" />
                                                </dx:ASPxTextBox>
                                            </td>
                                             <td style="width: 150px">
                                                <dx:ASPxLabel ID="Label16" runat="server" Text="ASPxLabel"  Visible="false">
                                                </dx:ASPxLabel>
                                            </td>
                                             <td style="width: 100px">
                                                <dx:ASPxTextBox ID="TextBox16" runat="server" Width="80px">
                                                    <MaskSettings ErrorText="*Invalid Input" Mask="00.00" ShowHints="True" />
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 150px">
                                                <dx:ASPxLabel ID="Label7" runat="server" Text="ASPxLabel"  Visible="false">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td style="width: 100px">
                                                <dx:ASPxTextBox ID="TextBox7" runat="server" Width="80px">
                                                    <MaskSettings ErrorText="*Invalid Input" Mask="00.00" ShowHints="True" />
                                                </dx:ASPxTextBox>
                                            </td>
                                             <td style="width: 150px">
                                                <dx:ASPxLabel ID="Label17" runat="server" Text="ASPxLabel"  Visible="false">
                                                </dx:ASPxLabel>
                                            </td>
                                             <td style="width: 100px">
                                                <dx:ASPxTextBox ID="TextBox17" runat="server" Width="80px">
                                                    <MaskSettings ErrorText="*Invalid Input" Mask="00.00" ShowHints="True" />
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 150px">
                                                <dx:ASPxLabel ID="Label8" runat="server" Text="ASPxLabel"  Visible="false">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td style="width: 100px">
                                                <dx:ASPxTextBox ID="TextBox8" runat="server" Width="80px">
                                                    <MaskSettings ErrorText="*Invalid Input" Mask="00.00" ShowHints="True" />
                                                </dx:ASPxTextBox>
                                            </td>
                                             <td style="width: 150px">
                                                <dx:ASPxLabel ID="Label18" runat="server" Text="ASPxLabel"  Visible="false">
                                                </dx:ASPxLabel>
                                            </td>
                                             <td style="width: 100px">
                                                <dx:ASPxTextBox ID="TextBox18" runat="server" Width="80px">
                                                    <MaskSettings ErrorText="*Invalid Input" Mask="00.00" ShowHints="True" />
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 150px">
                                                <dx:ASPxLabel ID="Label9" runat="server" Text="ASPxLabel"  Visible="false">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td style="width: 100px">
                                                <dx:ASPxTextBox ID="TextBox9" runat="server" Width="80px">
                                                    <MaskSettings ErrorText="*Invalid Input" Mask="00.00" ShowHints="True" />
                                                </dx:ASPxTextBox>
                                            </td>
                                             <td style="width: 150px">
                                                <dx:ASPxLabel ID="Label19" runat="server" Text="ASPxLabel"  Visible="false">
                                                </dx:ASPxLabel>
                                            </td>
                                             <td style="width: 100px">
                                                <dx:ASPxTextBox ID="TextBox19" runat="server" Width="80px">
                                                    <MaskSettings ErrorText="*Invalid Input" Mask="00.00" ShowHints="True" />
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 150px">
                                                <dx:ASPxLabel ID="Label10" runat="server" Text="ASPxLabel"  Visible="false">
                                                </dx:ASPxLabel>
                                            </td>
                                            <td style="width: 100px">
                                                <dx:ASPxTextBox ID="TextBox10" runat="server" Width="80px">
                                                    <MaskSettings ErrorText="*Invalid Input" Mask="00.00" ShowHints="True" />
                                                </dx:ASPxTextBox>
                                            </td>
                                             <td style="width: 150px">
                                                <dx:ASPxLabel ID="Label20" runat="server" Text="ASPxLabel"  Visible="false">
                                                </dx:ASPxLabel>
                                            </td>
                                             <td style="width: 100px">
                                                <dx:ASPxTextBox ID="TextBox20" runat="server" Width="80px">
                                                    <MaskSettings ErrorText="*Invalid Input" Mask="00.00" ShowHints="True" />
                                                </dx:ASPxTextBox>
                                            </td>
                                        </tr>
                                       
                                    </table>
                                   
                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>

                        
                    </Items>

                </dx:LayoutGroup>
               <dx:LayoutItem ShowCaption="False" CaptionSettings-HorizontalAlign="Right" Width="100%" HorizontalAlign="Right">
                    <LayoutItemNestedControlCollection>
                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                            <dx:ASPxButton ID="btnPost" runat="server" Text="Save" Width="100px" OnClick="btnPost_Click"  />
                        </dx:LayoutItemNestedControlContainer>
                    </LayoutItemNestedControlCollection>

                    <CaptionSettings HorizontalAlign="Right"></CaptionSettings>
                </dx:LayoutItem>
            </Items>
        </dx:ASPxFormLayout>

</td>
</tr>
</table>
</asp:Content>

