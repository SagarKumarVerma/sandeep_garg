﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using FKWeb;
using Newtonsoft.Json.Linq;
using DevExpress.Web;
using System.Text.RegularExpressions;
using System.IO;
using Newtonsoft.Json;
//using BioCloud;
using System.Data.Odbc;

public partial class frmLeaveUpload : System.Web.UI.Page
{

    OleDbConnection MyConn = new OleDbConnection();
    OleDbCommand MyCmd = new OleDbCommand();
    Class_Connection cn = new Class_Connection();
    public string KeyValue = "";
    public string TableMain = null;
    public string TableName = null;
    public string ColumnName = null;
    const int GET_DEVICE_STATUS = 1;
    const int DEL_USER_INFO = 3;
    ErrorClass ec = new ErrorClass();
    SqlConnection msqlConn;
    string fileName = "";
    string UName, PhotoPath, Face;


    DataSet Ds;
    string StrSql, Qry, change, Tmp;
    Class1 cls = new Class1();

    OdbcCommand ocmd;
    OdbcDataAdapter oDa;


    string Strsql = null;
    string Msg;
    DataSet ds = null;
    int result = 0;
    OleDbDataReader dr;


    string PayCode = null;
    DateTime dateofjoin;
    string Shift = null;
    string ShiftAttended = null;
    string ALTERNATEOFFDAYS = null;
    string firstoff = null;
    string secondofftype = null;
    string secondoff = null;
    string HALFDAYSHIFT = null;
    string SSN = null;
    DateTime selecteddate;
    DateTime FirstDate;
    DateTime LastDate;
    DateTime date;
    string ShiftType = null;
    string shiftpattern = null;
    string WOInclude = null;
    string[] words;
    int shiftcount = 0;

    string Status = "";
    int AbsentValue = 0;
    int WoVal = 0;
    int PresentValue = 0;
    string p = null;
    int shiftRemainDays = 0;

    string btnGoBack = "";
    string resMsg = "";
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SqlConnFkWeb"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        lblStatus.Text = "";
        if (!IsPostBack)
        {
            lblStatus.Text = "";
            msqlConn = FKWebTools.GetDBPool();

            if (Session["LoginUserType"].ToString().Trim() == null)
            {
                Response.Redirect("Login.aspx");
            }
            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            // bindLeave();
            if (Session["LevelUpdate"] == null)
            {
                Response.Redirect("Login.aspx");
            }
        }
    }


    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        //Call The function to write the error log file 

        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }

    public static string[] ExcelSheetNames_First(String excelFile)
    {
        DataTable dt;
        string[] res = new string[0];
        //string connString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelFile + ";Extended Properties=HTML Import;";
        string connString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelFile + ";Extended Properties=Excel 8.0;";
        using (OleDbConnection objConn = new OleDbConnection(connString))
        {
            objConn.Open();
            dt =
            objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            if (dt == null)
            {
                return null;
            }
            res = new string[dt.Rows.Count];
            for (int i = 0; i < res.Length; i++)
            {
                string name = dt.Rows[i]["TABLE_NAME"].ToString();
                if (name[0] == '\'')
                {
                    //numeric sheetnames get single quotes around
                    //remove them here
                    if (Regex.IsMatch(name, @"^'\d\w+\$'$"))
                    {
                        name = name.Substring(1, name.Length - 2);
                    }
                }
                res[i] = name;
            }
            return res;
        }
    }
    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        try
        {
            lblStatus.Text = "";
            string str_FileLocation = "";
            string xConnStr = "";
            str_FileLocation = Server.MapPath("UploadTemp");
            str_FileLocation = str_FileLocation + "\\" + fileName;
            int date = 0;
            date = Convert.ToInt32(System.DateTime.Now.Day);
            string SSN = "";

            DataSet dsResult = new DataSet();
           
            DataSet dscon = new DataSet();
            DataSet dsDept = new DataSet();
         
            DateTime dob = System.DateTime.MinValue, doj = System.DateTime.MinValue;
            DataSet dsRecord = new DataSet();
            
            DataSet dspaycode = new DataSet();
            string[] d = new string[3];
            string[] dj = new string[3];
            string[] a = new string[3];

            string PayCode = "";
            string Company = "";
            int L01 = 0;
            int L02 = 0;
            int L03 = 0;
            int L04 = 0;
            int L05 = 0;
            int L06 = 0;
            int L07 = 0;
            int L08 = 0;
            int L09 = 0;
            int L10 = 0;

            int LYear = System.DateTime.Now.Year;
            Strsql = "select * from tblleavemaster WHERE CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "' ";
            DataSet dsG = cn.FillDataSet(Strsql);
            if (dsG.Tables[0].Rows.Count == 0)
            {
                change = "<script language='JavaScript'>alert('Leave Master Not Created')</script>";
                Page.RegisterStartupScript("frmchange", change);
                return;
            }
            

            if (File.Exists(str_FileLocation))
            {
                File.Delete(str_FileLocation);
            }
            ASPxUploadControl1.SaveAs(str_FileLocation);
            string[] f = ExcelSheetNames_First(str_FileLocation);

            if (fileName.ToString().Trim() == "")
            {
                lblStatus.Text = "Please Select File";
                return;
            }
            else
            {
                try
                {
                    if (string.IsNullOrEmpty(f[0].ToString().Trim()))
                    {

                        lblStatus.Text = "Excel File Is Not In Valid Format...";
                        return;

                    }
                    StrSql = "select * from tblleavemaster where CompanyCode='"+ Session["LoginCompany"].ToString().Trim() + "'";
                    DataSet DsCheck = new DataSet();
                    DsCheck = cn.FillDataSet(StrSql);
                    if(DsCheck.Tables[0].Rows.Count==0)
                    {
                        lblStatus.Text = "No Leave Master Is Defined In Leave Master";
                        return;
                    }



                    xConnStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + str_FileLocation + @";Extended Properties=""Excel 8.0;IMEX=1;HDR=YES;TypeGuessRows=0;ImportMixedTypes=Text""";

                    using (OleDbConnection connection = new OleDbConnection(xConnStr))
                    {
                        OleDbCommand command = new OleDbCommand("Select * FROM [" + f[0] + "] ", connection);
                        connection.Open();
                        using (OleDbDataReader dr1 = command.ExecuteReader())
                        {
                            while (dr1.Read())
                            {
                                PayCode = dr1[0].ToString().Trim();
                                Company = dr1[1].ToString().Trim();
                                SSN = Company.Trim() + "_" + PayCode.Trim();
                                try
                                {
                                    LYear = Convert.ToInt32(dr1[2].ToString().Trim());
                                }
                                catch
                                {
                                    lblStatus.Text = "Invalid Leave Year-- " + LYear.ToString().Trim();
                                    return;
                                }
                                try
                                {
                                    L01 = Convert.ToInt32(dr1[3].ToString().Trim());
                                }
                                catch

                                {
                                    lblStatus.Text = "Invalid Value-- " + dr1[3].ToString().Trim();
                                    return;
                                }

                                try
                                {
                                    L02 = Convert.ToInt32(dr1[4].ToString().Trim());
                                }
                                catch

                                {
                                    lblStatus.Text = "Invalid Value-- " + dr1[4].ToString().Trim();
                                    return;
                                }
                                try
                                {
                                    L03 = Convert.ToInt32(dr1[5].ToString().Trim());
                                }
                                catch

                                {
                                    lblStatus.Text = "Invalid Value-- " + dr1[5].ToString().Trim();
                                    return;
                                }
                                try
                                {
                                    L04 = Convert.ToInt32(dr1[6].ToString().Trim());
                                }
                                catch

                                {
                                    lblStatus.Text = "Invalid Value-- " + dr1[6].ToString().Trim();
                                    return;
                                }
                                try
                                {
                                    L05 = Convert.ToInt32(dr1[7].ToString().Trim());
                                }
                                catch

                                {
                                    lblStatus.Text = "Invalid Value-- " + dr1[7].ToString().Trim();
                                    return;
                                }
                                try
                                {
                                    L06 = Convert.ToInt32(dr1[8].ToString().Trim());
                                }
                                catch

                                {
                                    lblStatus.Text = "Invalid Value-- " + dr1[8].ToString().Trim();
                                    return;
                                }
                                try
                                {
                                    L07 = Convert.ToInt32(dr1[9].ToString().Trim());
                                }
                                catch

                                {
                                    lblStatus.Text = "Invalid Value-- " + dr1[9].ToString().Trim();
                                    return;
                                }
                                try
                                {
                                    L08 = Convert.ToInt32(dr1[10].ToString().Trim());
                                }
                                catch

                                {
                                    lblStatus.Text = "Invalid Value-- " + dr1[10].ToString().Trim();
                                    return;
                                }
                                try
                                {
                                    L09 = Convert.ToInt32(dr1[11].ToString().Trim());
                                }
                                catch

                                {
                                    lblStatus.Text = "Invalid Value-- " + dr1[11].ToString().Trim();
                                    return;
                                }
                                try
                                {
                                    L10 = Convert.ToInt32(dr1[12].ToString().Trim());
                                }
                                catch

                                {
                                    lblStatus.Text = "Invalid Value-- " + dr1[12].ToString().Trim();
                                    return;
                                }
                                StrSql = "select * from tblleaveledger where LYEAR="+ LYear +" and SSN='"+SSN.Trim()+"'";
                                DataSet DsLeave = new DataSet();
                                DsLeave = cn.FillDataSet(StrSql);
                                if (DsLeave.Tables[0].Rows.Count>0)
                                {
                                    StrSql = "update tblleaveledger Set L01_Add="+L01+ ",L02_Add=" + L02 + ",L03_Add=" + L03 + ",L04_Add=" + L04 + ",L05_Add=" + L05 + ",L06_Add=" + L06 + ",L07_Add=" + L07 + ",L08_Add=" + L08 + ",L09_Add=" + L09 + ",L10_Add=" + L10 + " " +
                                             " Where LYEAR=" + LYear + " and SSN='" + SSN.Trim() + "' ";
                                }
                                else
                                {
                                    StrSql = " Insert into tblleaveledger (paycode,lyear,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13,L14,L15,L16,L17,L18,L19,L20,L01_Add,L02_Add,L03_Add,L04_Add,L05_Add,L06_Add,L07_Add,L08_Add,L09_Add,L10_Add,L11_Add,L12_Add,L13_Add,L14_Add,L15_Add,L16_Add,L17_Add,L18_Add,L19_Add,L20_Add,acc_flag,SSN) "+
                                             "  values('" + PayCode + "', '" + LYear.ToString() + "',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,"+L01+ "," + L01 + "," + L01 + "," + L01 + "," + L01 + "," + L01 + "," + L01 + "," + L01 + "," + L01 + "," + L01 + ",0,0,0,0,0,0,0,0,0,0,'Y','" + SSN.Trim() + "')";
                                   
                                }
                                cn.execute_NonQuery(StrSql);



                            }
                            dr1.Close();
                        }
                    }





                    lblStatus.Text = "Data Uploaded  Successfully";
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Upload Successfully');document.location='Home.aspx'", true);
                }
                catch (Exception Ex)
                {
                    lblStatus.Text = Ex.Message;
                    Error_Occured("Upload Function", Ex.Message);
                }
            }



        }
        catch (Exception Ex)
        {
            lblStatus.Text = Ex.Message;
            Error_Occured("ASPxButton1_Click", Ex.Message);
        }
    }




    public byte[] StreamToByteArray(Stream input)
    {
        byte[] total_stream = new byte[0];
        byte[] stream_array = new byte[0];
        byte[] buffer = new byte[1024];

        int read = 0;
        while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
        {
            stream_array = new byte[total_stream.Length + read];
            total_stream.CopyTo(stream_array, 0);
            Array.Copy(buffer, 0, stream_array, total_stream.Length, read);
            total_stream = stream_array;
        }

        return total_stream;
    }
    protected void ASPxUploadControl1_FilesUploadComplete(object sender, FilesUploadCompleteEventArgs e)
    {
        System.Threading.Thread.Sleep(2000);

        ASPxUploadControl uploadControl = sender as ASPxUploadControl;

        if (uploadControl.UploadedFiles != null && uploadControl.UploadedFiles.Length > 0)
        {
            for (int i = 0; i < uploadControl.UploadedFiles.Length; i++)
            {
                UploadedFile file = uploadControl.UploadedFiles[i];
                if (file.FileName != "")
                {
                    fileName = file.FileName.ToString().Trim();//string.Format("{0}{1}", MapPath("~/UploadTemp/"), file.FileName);

                }
            }
        }

    }
}