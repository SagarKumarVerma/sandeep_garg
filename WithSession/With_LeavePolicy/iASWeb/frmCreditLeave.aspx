﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="frmCreditLeave.aspx.cs" Inherits="frmCreditLeave" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <table align="center" style="width: 100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="text-align: center">Leave Credit Utility</td>
        </tr>
        <tr>
            <td align="center" colspan="6" class="tableHeaderCss" style="height: 25px;">&nbsp;</td>
        </tr>

        <tr>
            <td align="center">
                <dx:ASPxFormLayout ID="formLayout" runat="server" AlignItemCaptionsInAllGroups="True" UseDefaultPaddings="False">
           <%--         <SettingsAdaptivity AdaptivityMode="SingleColumnWindowLimit" SwitchToSingleColumnAtWindowInnerWidth="200" />--%>
                    <Items>

                        <dx:EmptyLayoutItem />
                        <dx:LayoutGroup Caption="Company & Date Selection" ColCount="1" Width="200px">
                            <Items>
                                <dx:LayoutItem Caption="Select Company">
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                            <dx:ASPxComboBox ID="ddlCompany" runat="server" ValueType="System.String" Width="200px" Theme="SoftOrange"></dx:ASPxComboBox>
                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                </dx:LayoutItem>

                                <dx:LayoutItem Caption="For Month">
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                            <dx:ASPxDateEdit ID="TxtFromDate" runat="server" Width="200px" Theme="SoftOrange" EditFormatString="dd/MM/yyyy" />
                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                </dx:LayoutItem>

                                <dx:LayoutItem Caption="Leave">
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                           <%-- <dx:ASPxGridLookup ID="GridLookup" runat="server" SelectionMode="Multiple" ClientInstanceName="gridLookup"
                                                KeyFieldName="leavefield" TextFormatString="{0}" MultiTextSeparator="," Width="200px">
                                                <Columns>
                                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" Caption="" Width="10px" />
                                                    <dx:GridViewDataColumn FieldName="leavefield" Caption="Code" Width="20px" />
                                                    <dx:GridViewDataColumn FieldName="LeaveDesc" Caption="Name" />
                                                </Columns>
                                                <GridViewProperties>
                                                    <Templates>
                                                        <StatusBar>
                                                            <table class="OptionsTable" style="float: right">
                                                                <tr>
                                                                    <td></td>
                                                                </tr>
                                                            </table>
                                                        </StatusBar>
                                                    </Templates>
                                                    <Settings ShowFilterRow="True" ShowStatusBar="Visible" />
                                                    <SettingsPager PageSize="7" EnableAdaptivity="true" />
                                                </GridViewProperties>
                                            </dx:ASPxGridLookup>--%>
                                            <dx:ASPxCheckBox ID="chkEL" runat="server" AutoPostBack="true" Text="EL"  ></dx:ASPxCheckBox>
                                            <dx:ASPxCheckBox ID="chkSL" runat="server" AutoPostBack="true"  Text="SL"></dx:ASPxCheckBox>

                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                </dx:LayoutItem>

                            </Items>
                        </dx:LayoutGroup>
                        
                        <dx:LayoutItem ShowCaption="False" CaptionSettings-HorizontalAlign="Center" Width="100%" HorizontalAlign="Center">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                    <dx:ASPxButton ID="btnCreate" runat="server" Text="Credit" Width="100" OnClick="btnCreate_Click" HorizontalAlign="Center" />
                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>

                            <CaptionSettings HorizontalAlign="Right"></CaptionSettings>
                        </dx:LayoutItem>
                    </Items>
                </dx:ASPxFormLayout>
            </td>
        </tr>
    </table>

</asp:Content>

