﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using GlobalSettings;

public partial class frmLeaveAccural : System.Web.UI.Page
{
    OleDbConnection MyConn = new OleDbConnection();
    OleDbCommand MyCmd = new OleDbCommand();
    Class1 cCount = new Class1();
    Class_Connection Con = new Class_Connection();
    string Strsql = null;
    string Msg;
    DataSet ds = null;
    int result = 0;
    OleDbDataReader dr;
    string ReportView = "";
    string PayCode = null;
    DateTime dateofjoin;
    string Shift = null;
    string ShiftAttended = null;
    string ALTERNATEOFFDAYS = null;
    string firstoff = null;
    string secondofftype = null;
    string secondoff = null;
    string HALFDAYSHIFT = null;
    DateTime selecteddate;
    DateTime FirstDate;
    DateTime LastDate;
    DateTime date;
    string ShiftType = null;
    string shiftpattern = null;
    string WOInclude = null;
    string[] words;
    int shiftcount = 0;
    string sSql = string.Empty;
    string Status = "";
    int AbsentValue = 0;
    int WoVal = 0;
    int PresentValue = 0;
    string p = null;
    int shiftRemainDays = 0;
    DateTime PunchDate;

    string msg;
    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
            {
                Request.Browser.Adapters.Clear();
            }
            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            if (Session["LeaveAccuralVisible"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            txtYear.Text = System.DateTime.Today.ToString("yyyy");  //"01/" + System.DateTime.Now.Month.ToString("00") + "/" + System.DateTime.Now.Year.ToString("0000");

            if (Request.QueryString["ID"] != null)
            {
                string Grade = Request.QueryString["ID"].ToString();

            }
            TextBox1.Visible = false;
            TextBox2.Visible = false;
            TextBox3.Visible = false;
            TextBox4.Visible = false;
            TextBox5.Visible = false;
            TextBox6.Visible = false;
            TextBox7.Visible = false;
            TextBox8.Visible = false;
            TextBox9.Visible = false;
            TextBox10.Visible = false;
            TextBox11.Visible = false;
            TextBox12.Visible = false;
            TextBox13.Visible = false;
            TextBox14.Visible = false;
            TextBox15.Visible = false;
            TextBox16.Visible = false;
            TextBox17.Visible = false;
            TextBox18.Visible = false;
            TextBox19.Visible = false;
            TextBox20.Visible = false;
            bindLeaveCode();
            bindPaycode();
        }
    }
    protected void Page_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();
        if (ex is HttpRequestValidationException)
        {
            string resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" +
                   "Form doesn't need to contain valid HTML, just anything with opening and closing angled brackets (<...>).<br />" +
                   btnGoBack +
                   "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
    }


    protected void bindPaycode()
    {
        ReportView = ConfigurationSettings.AppSettings["ReportView_HeadID"].ToString();
        Strsql = "Select paycode,rtrim(Empname) + ' - ' + paycode as Empname1  from tblemployee where Active='Y' ";
        if (Session["usertype"].ToString() == "A")
        {
            if (Session["Auth_Comp"] != null)
            {
                Strsql += " and companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
            }
            if (Session["Auth_Dept"] != null)
            {
                Strsql += " and Departmentcode in (" + Session["Auth_Dept"].ToString().Trim() + ") ";
            }
            Strsql += "  Order by EmpName ";
        }
        else if (Session["usertype"].ToString().Trim() == "H")
        {
            if (ReportView.ToString().Trim() == "Y")
            {
                Strsql += " and headid = '" + Session["PAYCODE"].ToString().ToString().Trim() + "' ";
            }
            else
            {
                if (Session["Auth_Comp"] != null)
                {
                    Strsql += " and companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
                }
                if (Session["Auth_Dept"] != null)
                {
                    Strsql += " and Departmentcode in (" + Session["Auth_Dept"].ToString().Trim() + ") ";
                }
            }
            Strsql += "  Order by EmpName ";
        }
        else
            return;


        ds = new DataSet();
       
        ds = Con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            
            ddlPayCode.DataSource = ds;
            ddlPayCode.TextField = "EmpName1";
            ddlPayCode.ValueField = "paycode";
            ddlPayCode.DataBind();          
            ddlPayCode.SelectedIndex = -1;
        }
        else
        {
            ddlPayCode.Items.Add("NONE");
            ddlPayCode.SelectedIndex = 0;


        }
    }
    protected void bindLeaveCode()
    {
        try
        {
            Strsql = "select LeaveField,LeaveCode,LeaveDescription from tblleavemaster where CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "' order by cast(substring(leavefield,2,3) as int) ";
            ds = Con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (ds.Tables[0].Rows[i]["LeaveDescription"].ToString().Trim() != "")
                    {
                        TextBox1.Visible = true;
                        Label1.Visible = true;
                        Label1.Text = ds.Tables[0].Rows[i]["LeaveDescription"].ToString().Trim();
                    }
                    else
                    {
                        Label1.Visible = false;
                        Label1.Text = "";
                    }
                    if (ds.Tables[0].Rows[i + 1]["LeaveDescription"].ToString().Trim() != "")
                    {
                        TextBox2.Visible = true;
                        Label2.Visible = true;
                        Label2.Text = ds.Tables[0].Rows[i + 1]["LeaveDescription"].ToString().Trim();
                    }
                    else
                    {
                        Label2.Visible = false;
                        Label2.Text = "";
                    }
                    if (ds.Tables[0].Rows[i + 2]["LeaveDescription"].ToString().Trim() != "")
                    {
                        TextBox3.Visible = true;
                        Label3.Visible = true;
                        Label3.Text = ds.Tables[0].Rows[i + 2]["LeaveDescription"].ToString().Trim();
                    }
                    else
                    {
                        Label3.Visible = false;
                        Label3.Text = "";
                    }
                    if (ds.Tables[0].Rows[i + 3]["LeaveDescription"].ToString().Trim() != "")
                    {
                        TextBox4.Visible = true;
                        Label4.Visible = true;
                        Label4.Text = ds.Tables[0].Rows[i + 3]["LeaveDescription"].ToString().Trim();
                    }
                    else
                    {
                        Label4.Visible = false;
                        Label4.Text = "";
                    }
                    if (ds.Tables[0].Rows[i + 4]["LeaveDescription"].ToString().Trim() != "")
                    {
                        TextBox5.Visible = true;
                        Label5.Visible = true;
                        Label5.Text = ds.Tables[0].Rows[i + 4]["LeaveDescription"].ToString().Trim();
                    }
                    else
                    {
                        Label5.Visible = false;
                        Label5.Text = "";
                    }
                    if (ds.Tables[0].Rows[i + 5]["LeaveDescription"].ToString().Trim() != "")
                    {
                        TextBox6.Visible = true;
                        Label6.Visible = true;
                        Label6.Text = ds.Tables[0].Rows[i + 5]["LeaveDescription"].ToString().Trim();
                    }
                    else
                    {
                        Label6.Visible = false;
                        Label6.Text = "";
                    }
                    if (ds.Tables[0].Rows[i + 6]["LeaveDescription"].ToString().Trim() != "")
                    {
                        TextBox7.Visible = true;
                        Label7.Visible = true;
                        Label7.Text = ds.Tables[0].Rows[i + 6]["LeaveDescription"].ToString().Trim();
                    }
                    else
                    {
                        Label7.Visible = false;
                        Label7.Text = "";
                    }
                    if (ds.Tables[0].Rows[i + 7]["LeaveDescription"].ToString().Trim() != "")
                    {
                        TextBox8.Visible = true;
                        Label8.Visible = true;
                        Label8.Text = ds.Tables[0].Rows[i + 7]["LeaveDescription"].ToString().Trim();
                    }
                    else
                    {
                        Label8.Visible = false;
                        Label8.Text = "";
                    }
                    if (ds.Tables[0].Rows[i + 8]["LeaveDescription"].ToString().Trim() != "")
                    {
                        TextBox9.Visible = true;
                        Label9.Visible = true;
                        Label9.Text = ds.Tables[0].Rows[i + 8]["LeaveDescription"].ToString().Trim();
                    }
                    else
                    {
                        Label9.Visible = false;
                        Label9.Text = "";
                    }
                    if (ds.Tables[0].Rows[i + 9]["LeaveDescription"].ToString().Trim() != "")
                    {
                        TextBox10.Visible = true;
                        Label10.Visible = true;
                        Label10.Text = ds.Tables[0].Rows[i + 9]["LeaveDescription"].ToString().Trim();
                    }
                    else
                    {
                        Label10.Visible = false;
                        Label10.Text = "";
                    }

                    /////////////////////////////


                    if (ds.Tables[0].Rows[i + 10]["LeaveDescription"].ToString().Trim() != "")
                    {
                        TextBox11.Visible = true;
                        Label11.Visible = true;
                        Label11.Text = ds.Tables[0].Rows[i + 10]["LeaveDescription"].ToString().Trim();
                    }
                    else
                    {
                        Label11.Visible = false;
                        Label11.Text = "";
                    }
                    if (ds.Tables[0].Rows[i + 11]["LeaveDescription"].ToString().Trim() != "")
                    {
                        TextBox12.Visible = true;
                        Label12.Visible = true;
                        Label12.Text = ds.Tables[0].Rows[i + 11]["LeaveDescription"].ToString().Trim();
                    }
                    else
                    {
                        Label12.Visible = false;
                        Label12.Text = "";
                    }

                    if (ds.Tables[0].Rows[i + 12]["LeaveDescription"].ToString().Trim() != "")
                    {
                        TextBox13.Visible = true;
                        Label13.Visible = true;
                        Label13.Text = ds.Tables[0].Rows[i + 12]["LeaveDescription"].ToString().Trim();
                    }
                    else
                    {
                        Label13.Visible = false;
                        Label13.Text = "";
                    }

                    if (ds.Tables[0].Rows[i + 13]["LeaveDescription"].ToString().Trim() != "")
                    {
                        TextBox14.Visible = true;
                        Label14.Visible = true;
                        Label14.Text = ds.Tables[0].Rows[i + 13]["LeaveDescription"].ToString().Trim();
                    }
                    else
                    {
                        Label14.Visible = false;
                        Label14.Text = "";
                    }
                    if (ds.Tables[0].Rows[i + 14]["LeaveDescription"].ToString().Trim() != "")
                    {
                        TextBox15.Visible = true;
                        TextBox15.Visible = true;
                        Label15.Visible = true;
                        Label15.Text = ds.Tables[0].Rows[i + 14]["LeaveDescription"].ToString().Trim();
                    }
                    else
                    {
                        Label15.Visible = false;
                        Label15.Text = "";
                    }

                    if (ds.Tables[0].Rows[i + 15]["LeaveDescription"].ToString().Trim() != "")
                    {
                        TextBox16.Visible = true;
                        Label16.Visible = true;
                        Label16.Text = ds.Tables[0].Rows[i + 15]["LeaveDescription"].ToString().Trim();
                    }
                    else
                    {
                        Label16.Visible = false;
                        Label16.Text = "";
                    }

                    if (ds.Tables[0].Rows[i + 16]["LeaveDescription"].ToString().Trim() != "")
                    {
                        TextBox17.Visible = true;
                        Label17.Visible = true;
                        Label17.Text = ds.Tables[0].Rows[i + 16]["LeaveDescription"].ToString().Trim();
                    }
                    else
                    {
                        Label17.Visible = false;
                        Label17.Text = "";
                    }

                    if (ds.Tables[0].Rows[i + 17]["LeaveDescription"].ToString().Trim() != "")
                    {
                        TextBox18.Visible = true;
                        Label18.Visible = true;
                        Label18.Text = ds.Tables[0].Rows[i + 17]["LeaveDescription"].ToString().Trim();
                    }
                    else
                    {
                        Label18.Visible = false;
                        Label18.Text = "";
                    }

                    if (ds.Tables[0].Rows[i + 18]["LeaveDescription"].ToString().Trim() != "")
                    {
                        TextBox19.Visible = true;
                        Label19.Visible = true;
                        Label19.Text = ds.Tables[0].Rows[i + 18]["LeaveDescription"].ToString().Trim();
                    }
                    else
                    {
                        Label19.Visible = false;
                        Label19.Text = "";
                    }

                    if (ds.Tables[0].Rows[i + 19]["LeaveDescription"].ToString().Trim() != "")
                    {
                        TextBox20.Visible = true;
                        Label20.Visible = true;
                        Label20.Text = ds.Tables[0].Rows[i + 19]["LeaveDescription"].ToString().Trim();
                    }
                    else
                    {
                        Label20.Visible = false;
                        Label20.Text = "";
                    }
                }
            }
        }
        catch { }
    }
    protected void ddlPayCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            sSql = " SELECT SSN,EMPNAME,tblDepartment.DEPARTMENTNAME,tblcompany.companyname,TBLEMPLOYEE.designation FROM TBLEMPLOYEE inner join tblDepartment on  " +
              " tblDepartment.DEPARTMENTCODE=TBLEMPLOYEE.DepartmentCode inner join tblcompany on tblcompany.companycode=TBLEMPLOYEE.companycode  " +
              " and TBLEMPLOYEE.paycode='" + ddlPayCode.SelectedItem.Value.ToString().Trim() + "' and TBLEMPLOYEE.CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "' ";
            DataSet dsEmp = new DataSet();
            dsEmp = Con.FillDataSet(sSql);
            if (dsEmp.Tables[0].Rows.Count > 0)
            {
                lblName.Text = dsEmp.Tables[0].Rows[0]["Empname"].ToString().Trim();
                lblLocation.Text = dsEmp.Tables[0].Rows[0]["DEPARTMENTNAME"].ToString().Trim();
                lblDesignation.Text = dsEmp.Tables[0].Rows[0]["designation"].ToString().Trim();
                lblCompany.Text = dsEmp.Tables[0].Rows[0]["companyname"].ToString().Trim();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Please select Any Record');", true);
                ddlPayCode.Focus();
                return;
            }
            fillLvBalance(dsEmp.Tables[0].Rows[0]["SSN"].ToString().Trim());

        }
        catch
        {

        }
    }
    public void fillLvBalance(string SSN)
    {
        //string SSN = "";

        if ((txtYear.Text.ToString().Trim() != "") && (txtYear.Text.ToString().Length == 4))
        {


            Strsql = "select count(paycode) from tblleaveledger where SSN ='" + SSN.Trim() + "' and lyear= '" + txtYear.Text.ToString().Trim() + "'";
            result = Con.execute_Scalar(Strsql);
            if (result == 0)
            {

                //Strsql = "SELECT SSN FROM TBLEMPLOYEE WHERE PAYCODE='" + ddlPayCode.SelectedItem.Value.ToString().Trim() + "'";
                //DataSet DsSSN = new DataSet();
                //DsSSN = Con.FillDataSet(Strsql);
                //if (DsSSN.Tables[0].Rows.Count > 0)
                //{
                //    SSN = DsSSN.Tables[0].Rows[0]["SSN"].ToString().Trim();
                //}

                Strsql = "Insert into tblleaveledger (paycode,lyear,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13,L14,L15,L16,L17,L18,L19,L20,L01_Add,L02_Add,L03_Add,L04_Add,L05_Add,L06_Add,L07_Add,L08_Add,L09_Add,L10_Add,L11_Add,L12_Add,L13_Add,L14_Add,L15_Add,L16_Add,L17_Add,L18_Add,L19_Add,L20_Add,L01_CASH,L02_CASH,L03_CASH,L04_CASH,L05_CASH,L06_CASH,L07_CASH,L08_CASH,L09_CASH,L10_CASH,L11_CASH,L12_CASH,L13_CASH,L14_CASH,L15_CASH,L16_CASH,L17_CASH,L18_CASH,L19_CASH,L20_CASH,SSN) values " +
                   "('" + ddlPayCode.SelectedItem.Value.ToString().Trim() + "','" + txtYear.Text.ToString().Trim() + "',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'" + SSN + "')";

                result = Con.execute_NonQuery(Strsql);
            }
            Strsql = "select isnull(L01_Add,0) 'L01_Add', isnull(L02_Add,0.0) 'L02_Add',isnull(L03_Add,0.0) 'L03_Add',isnull(L04_Add,0.0) 'L04_Add',isnull(L05_Add,0.0) 'L05_Add', " +
                    "isnull(L06_Add,0.0) 'L06_Add',isnull(L07_Add,0.0) 'L07_Add',isnull(L08_Add,0.0) 'L08_Add',isnull(L09_Add,0.0) 'L09_Add',isnull(L10_Add,0.0) 'L10_Add', " +
                    "isnull(L11_Add,0.0) 'L11_Add', isnull(L12_Add,0.0) 'L12_Add',isnull(L13_Add,0.0) 'L13_Add',isnull(L14_Add,0.0) 'L14_Add',isnull(L15_Add,0.0) 'L15_Add', " +
                    "isnull(L16_Add,0.0) 'L16_Add',isnull(L17_Add,0.0) 'L17_Add',isnull(L18_Add,0.0) 'L18_Add',isnull(L19_Add,0.0) 'L19_Add',isnull(L20_Add,0.0) 'L20_Add' " +
                    "from tblleaveledger where SSN ='" + SSN.Trim() + "' and lyear= '" + txtYear.Text.ToString().Trim() + "'";
            dr = Con.ExecuteReader(Strsql);
            if (dr.Read())
            {
                TextBox1.Text = string.Format("{0:00.00}", dr["L01_Add"]);
                TextBox2.Text = string.Format("{0:00.00}", dr["L02_Add"]);
                TextBox3.Text = string.Format("{0:00.00}", dr["L03_Add"]);
                TextBox4.Text = string.Format("{0:00.00}", dr["L04_Add"]);
                TextBox5.Text = string.Format("{0:00.00}", dr["L05_Add"]);
                TextBox6.Text = string.Format("{0:00.00}", dr["L06_Add"]);
                TextBox7.Text = string.Format("{0:00.00}", dr["L07_Add"]);
                TextBox8.Text = string.Format("{0:00.00}", dr["L08_Add"]);
                TextBox9.Text = string.Format("{0:00.00}", dr["L09_Add"]);
                TextBox10.Text = string.Format("{0:00.00}", dr["L10_Add"]);
                TextBox11.Text = string.Format("{0:00.00}", dr["L11_Add"]);
                TextBox12.Text = string.Format("{0:00.00}", dr["L12_Add"]);
                TextBox13.Text = string.Format("{0:00.00}", dr["L13_Add"]);
                TextBox14.Text = string.Format("{0:00.00}", dr["L14_Add"]);
                TextBox15.Text = string.Format("{0:00.00}", dr["L15_Add"]);
                TextBox16.Text = string.Format("{0:00.00}", dr["L16_Add"]);
                TextBox17.Text = string.Format("{0:00.00}", dr["L17_Add"]);
                TextBox18.Text = string.Format("{0:00.00}", dr["L18_Add"]);
                TextBox19.Text = string.Format("{0:00.00}", dr["L19_Add"]);
                TextBox20.Text = string.Format("{0:00.00}", dr["L20_Add"]);

            }
        }
    }
    protected void btnPost_Click(object sender, EventArgs e)
    {

        try
        {

            if (ddlPayCode.SelectedIndex == -1)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Please Input PayCode');", true);
                ddlPayCode.Focus();
                return;
            }   

            string paycode = "";
            string SSN = "";
            
                Strsql = "select tblemployee.empname 'EmpName',tblemployee.presentcardno 'PRESENTCARDNO',tblemployee.designation 'DESIGNATION',tbldepartment.departmentname ,tblcompany.companyname 'COMPANYCODE',tblCatagory.CATAGORYNAME 'CAT' from " +
                            "  tblemployee join tbldepartment on tblemployee.departmentcode=tbldepartment.departmentcode " +
                            "  join tblcompany on tblemployee.companycode=tblcompany.companycode " +
                            "  join tblCatagory  on tblemployee.cat=tblCatagory.CAT  " +
                            "  where paycode ='" + ddlPayCode.SelectedItem.Value.ToString().Trim() + "' and tblemployee.active='Y' ";
                dr = Con.ExecuteReader(Strsql);
                if (!dr.Read())
                {
                    lblName.Text = "";
                   
                    lblDesignation.Text = "";
                    lblLocation.Text = "";
                    lblCompany.Text = "";
                    TextBox1.Text = "";
                    TextBox2.Text = "";
                    TextBox3.Text = "";
                    TextBox4.Text = "";
                    TextBox5.Text = "";
                    TextBox6.Text = "";
                    TextBox7.Text = "";
                    TextBox8.Text = "";
                    TextBox9.Text = "";
                    TextBox10.Text = "";
                    TextBox11.Text = "";
                    TextBox12.Text = "";
                    TextBox13.Text = "";
                    TextBox14.Text = "";
                    TextBox15.Text = "";
                    TextBox16.Text = "";
                    TextBox17.Text = "";
                    TextBox18.Text = "";
                    TextBox19.Text = "";
                    TextBox20.Text = "";

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Invalid Paycode');", true);
                    return;
                }



                Strsql = "select paycode,SSN from tblemployee where paycode ='" + ddlPayCode.SelectedItem.Value.ToString().Trim() + "' and Active='Y' ";
                dr = Con.ExecuteReader(Strsql);
                while (dr.Read())
                {
                    paycode = dr["paycode"].ToString().Trim();
                    SSN = dr["SSN"].ToString().Trim();
                    Strsql = "select count(paycode) from tblLeaveLedger where paycode='" + paycode.ToString().Trim() + "' and " +
                        "LYear='" + txtYear.Text.ToString().Trim() + "'";
                    result = Con.execute_Scalar(Strsql);
                    if (result <= 0)
                    {
                        Strsql = "Insert into tblleaveledger (paycode,lyear,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13,L14,L15,L16,L17,L18,L19,L20,L01_Add,L02_Add,L03_Add,L04_Add,L05_Add,L06_Add,L07_Add,L08_Add,L09_Add,L10_Add,L11_Add,L12_Add,L13_Add,L14_Add,L15_Add,L16_Add,L17_Add,L18_Add,L19_Add,L20_Add,L01_CASH,L02_CASH,L03_CASH,L04_CASH,L05_CASH,L06_CASH,L07_CASH,L08_CASH,L09_CASH,L10_CASH,L11_CASH,L12_CASH,L13_CASH,L14_CASH,L15_CASH,L16_CASH,L17_CASH,L18_CASH,L19_CASH,L20_CASH,SSN) values " +
                         "('" + paycode.ToString().Trim() + "','" + txtYear.Text.ToString().Trim() + "',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, " +
                          " '" + TextBox1.Text.ToString().Trim() + "','" + TextBox2.Text.ToString().Trim() + "','" + TextBox3.Text.ToString().Trim() + "','" + TextBox4.Text.ToString().Trim() + "','" + TextBox5.Text.ToString().Trim() + "', " +
                          "'" + TextBox6.Text.ToString().Trim() + "','" + TextBox7.Text.ToString().Trim() + "','" + TextBox8.Text.ToString().Trim() + "','" + TextBox9.Text.ToString().Trim() + "','" + TextBox10.Text.ToString().Trim() + "','" + TextBox11.Text.ToString().Trim() + "','" + TextBox12.Text.ToString().Trim() + "', " +
                          "'" + TextBox13.Text.ToString().Trim() + "','" + TextBox14.Text.ToString().Trim() + "','" + TextBox15.Text.ToString().Trim() + "','" + TextBox16.Text.ToString().Trim() + "','" + TextBox17.Text.ToString().Trim() + "','" + TextBox18.Text.ToString().Trim() + "','" + TextBox19.Text.ToString().Trim() + "','" + TextBox20.Text.ToString().Trim() + "', " +
                          "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'"+SSN.Trim()+"')";
                    }
                    else
                    {
                        Strsql = "update tblleaveledger set L01_Add='" + TextBox1.Text.ToString().Trim() + "',L02_Add='" + TextBox2.Text.ToString().Trim() + "',L03_Add='" + TextBox3.Text.ToString().Trim() + "',L04_Add='" + TextBox4.Text.ToString().Trim() + "',L05_Add='" + TextBox5.Text.ToString().Trim() + "', " +
                        "L06_Add='" + TextBox6.Text.ToString().Trim() + "',L07_Add='" + TextBox7.Text.ToString().Trim() + "',L08_Add='" + TextBox8.Text.ToString().Trim() + "',L09_Add='" + TextBox9.Text.ToString().Trim() + "',L10_Add='" + TextBox10.Text.ToString().Trim() + "',L11_Add='" + TextBox11.Text.ToString().Trim() + "',L12_Add='" + TextBox12.Text.ToString().Trim() + "', " +
                        "L13_Add='" + TextBox13.Text.ToString().Trim() + "',L14_Add='" + TextBox14.Text.ToString().Trim() + "',L15_Add='" + TextBox15.Text.ToString().Trim() + "',L16_Add='" + TextBox16.Text.ToString().Trim() + "',L17_Add='" + TextBox17.Text.ToString().Trim() + "',L18_Add='" + TextBox18.Text.ToString().Trim() + "',L19_Add='" + TextBox19.Text.ToString().Trim() + "', " +
                        "L20_Add='" + TextBox20.Text.ToString().Trim() + "',SSN='"+SSN.Trim()+"' where paycode='" + paycode.ToString().Trim() + "' and lyear= '" + txtYear.Text.ToString().Trim() + "' ";
                    }
                    result = Con.execute_NonQuery(Strsql);
                    if (result > 0)
                    {
                        lblName.Text = "";
                        
                        lblDesignation.Text = "";
                        lblLocation.Text = "";
                        lblCompany.Text = "";
                        TextBox1.Text = "";
                        TextBox2.Text = "";
                        TextBox3.Text = "";
                        TextBox4.Text = "";
                        TextBox5.Text = "";
                        TextBox6.Text = "";
                        TextBox7.Text = "";
                        TextBox8.Text = "";
                        TextBox9.Text = "";
                        TextBox10.Text = "";
                        TextBox11.Text = "";
                        TextBox12.Text = "";
                        TextBox13.Text = "";
                        TextBox14.Text = "";
                        TextBox15.Text = "";
                        TextBox16.Text = "";
                        TextBox17.Text = "";
                        TextBox18.Text = "";
                        TextBox19.Text = "";
                        TextBox20.Text = "";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Leaves Updated Successfully.');", true);
                        return;
                    }


                }
           
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "alert('" + ex.Message.ToString() + "');", true);
            return;
        }
    }
}