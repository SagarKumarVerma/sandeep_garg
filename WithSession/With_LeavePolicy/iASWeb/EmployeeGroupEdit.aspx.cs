﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Text;
using System.IO;
using System.Globalization;
using System.Data.SqlClient;
using System.Web.Caching;
using System.Text.RegularExpressions;
using System.Data.Odbc;
using GlobalSettings;
using System.Collections.Generic;

public partial class EmployeeGroupEdit : System.Web.UI.Page
{
    OleDbConnection OleDbCon = new OleDbConnection(ConfigurationManager.AppSettings["connectionstring"].ToString());
    OleDbConnection MyConn = new OleDbConnection();
    OleDbCommand MyCmd = new OleDbCommand();
    Setting_App Settings = new Setting_App();
    Class_Connection cn = new Class_Connection();
    hourToMinute hm = new hourToMinute();
    string Strsql = null;
    string Strsql_new = null;
    DataSet ds = null;
    int result = 0;
    int result1 = 0;
    int result2 = 0;
    int result3 = 0;
    ErrorClass ec = new ErrorClass();
    OleDbDataAdapter da = null;
    OleDbConnection connection = null;
    string Strsql1 = null;
    string Strsql2 = null;
    string Strsql3 = null;
    string Authrun = null;
    string str = null;
    string str1 = null;
    DateTime dob;
    string DB = null;
    string finalShifts = null;
    Byte[] imgByte = null;
    Byte[] imgByte1 = null;
    string PF1 = null;
    string BankCode = null;
    string BankAcc = null;
    string ShiftCode = null;
    string FileName = null;
    string location = null;
    string guid = null;

    string FileName1 = null;
    string EmpSignatureLocation = null;
    string guid1 = null;

    string SecondWeeklyoff = null;
    string HalfDayShift = null;
    string SecondOffType = null;
    string FirstWeeklyoff = null;

    string isLDAP = "";
    DateTime DOB;
    DateTime DOJ;
    bool MasterCheck = false;

    //For Global
    string HalfDayMarking = null;
    string ShortDayMarking = null;
    string WOinclude = null;
    string FourPunch_Night = null;
    string OTOutMinusShiftEndTime = null;
    string OTWrkgHrsMinusShiftHrs = null;
    string OTEarlyComePlusLateDep = null;
    string OTAllowed = null;
    string OTRound = null;
    string OutWork = null;
    string OverStay = null;
    string OT_EARLYCOMING = null;
    string PresentOn_HLDPresent = null;
    string PresentOn_WOPresent = null;
    string AbsentOn_WOAbsent = null;
    string awa = null;
    string AW = null;
    string WA = null;

    string Wo_Absent = null;
    string AutoShift = null;
    string SkipPage = null;
    string SmartMac = null;
    string InOut = null;
    string Help = null;
    string MachineAccess = null;
    string EndTime_In = null;
    string EndTime_RTCEmp = null;


    //For EmpShift
    string inonly = "";
    string isPunchAll = "";
    string TimeLoss = "";
    string RoundClock = "";
    string IsOt = "";
    string IsAuthShift = "";
    string AuthShifts = "";
    string OS = "";
    string ishalfday = "";
    string isshort = "";
    string Twopunch = "";
    string days = "";
    string IsOutWork = "";
    string StrSql4 = "";
    int result4 = 0;
    string IsCoff = "";
    string IsMIS = "";
    string EnableAutoResign = "N";


    //New Change 
    int late = 0;
    int EarlyDept = 0;
    int MaxDayWork = 0;
    int PresentMarkingDuration = 0;
    int workinghourforhalfday = 0;
    int workhourforshort = 0;
    int HalfAfter = 0;
    int HalfBefore = 0;
    int ResignedAfter = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            if (Session["EmployeeGrpVisible"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            //if (Session["EmployeeVisible"] == null)
            //{
            //    Response.Redirect("PageNotFound.aspx");
            //}
            //ddlFirstWeeklyOff.Attributes.Add("onChange", "return chkvalue_new('" + ddlFirstWeeklyOff.ClientID + "','" + ddlSecondWeeklyOff.ClientID + "','" + ddlSecondWeeklyOffType.ClientID + "','" + ddlHalfDayShift.ClientID + "','" + lbl1.ClientID + "','" + lbl2.ClientID + "','" + lbl3.ClientID + "','" + lbl4.ClientID + "','" + lbl5.ClientID + "','" + PanelSecondWeeklyOff.ClientID + "','" + chk1.ClientID + "','" + chk2.ClientID + "','" + chk3.ClientID + "','" + chk4.ClientID + "','" + chk5.ClientID + "');");

            //ddlSecondWeeklyOffType.Attributes.Add("onChange", "return SecondOffType('" + ddlSecondWeeklyOffType.ClientID + "','" + ddlHalfDayShift.ClientID + "');");

            //Page.ClientScript.RegisterStartupScript(Type.GetType("System.String"), "addScript", "pageLoadHide1('" + radPunch.ClientID + "','" + chkSinglepunchonly.ClientID + "');", true);
            //ddlSecondWeeklyOff.Attributes.Add("onChange", "return chkvalue('" + ddlFirstWeeklyOff.ClientID + "','" + ddlSecondWeeklyOff.ClientID + "','" + ddlSecondWeeklyOffType.ClientID + "','" + ddlHalfDayShift.ClientID + "','" + lbl1.ClientID + "','" + lbl2.ClientID + "','" + lbl3.ClientID + "','" + lbl4.ClientID + "','" + lbl5.ClientID + "','" + PanelSecondWeeklyOff.ClientID + "','" + chk1.ClientID + "','" + chk2.ClientID + "','" + chk3.ClientID + "','" + chk4.ClientID + "','" + chk5.ClientID + "');");
            /////  Page.ClientScript.RegisterStartupScript(Type.GetType("System.String"), "addScript", "pageLoadHide2('" + radPunch.ClientID + "','" + chkSinglepunchonly.ClientID + "','"+ddlBank.ClientID+"','"+txtAC.ClientID+"');", true);
            //radPunch.Attributes.Add("onClick", "return getCheckedRadio('" + radPunch.ClientID + "','" + chkSinglepunchonly.ClientID + "');");

            //   Page.ClientScript.RegisterStartupScript(Type.GetType("System.String"), "addScript", "pageLoadHide2('" + radPunch.ClientID + "','" + chkSinglepunchonly.ClientID + "');", true);


            txtOTRate.Enabled = false;
            chkSinglepunchonly.Enabled = false;
            ddlSecondWeeklyOffType.Enabled = false;

            ddlHalfDayShift.Enabled = false;

            if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
            {
                Request.Browser.Adapters.Clear();
            }


            txtLateArrival.Text = "00:15";
            txtEarlyDept.Text = "00:15";
            txtMaxWorkHours.Text = "23:59";
            txtPresentMarkingDuration.Text = "04:00";
            txtMaxWorkHours_halfDay.Text = "06:00";
            MaxWorkHoursForshort.Text = "02:00";
            txtOTRate.Text = "000.00";
            txtEndTime_In.Text = "05:00";
            txtEndTime_RTCEmp.Text = "05:00";
            txtPermisEarlyMin.Text = "240";
            txtPermisLateMin.Text = "240";
            //txtResignedAfter.Text = "365";
            txtHLFAfter.Text = "00:00";
            txtHLFBefore.Text = "00:00";
            //PanelAutoRunShift.Enabled = false;
            ddlRemoveShiftPattern.Enabled = false;
            ddlAddShiftPattern.Enabled = false;
            txtShiftRemainigDays.Enabled = false;
            txtShiftChangesDays.Enabled = false;
            cmdAddShift.Enabled = false;
            cmdRemovePattern.Enabled = false;
            txtDuplicateCheck.Text = "5";
            txtWO.Text = "3";
            txtOT_EarlyComing.Text = "0";
            txtOT_LateComing.Text = "0";
            txtOT_Restricted.Text = "0";
            txtDeductHLD.Text = "0";
            txtDeductWO.Text = "0";
            ASPxPageControl1.ActiveTabIndex = 0;
            ddlFirstWeeklyOff.SelectedIndex = 0;
            ddlFirstWeeklyOff.SelectedIndex = 0;

            //ChkOTAppli.Attributes.Add("onclick", "return OTCheck('" + ChkOTAppli.ClientID + "','" + txtOTRate.ClientID + "');");
            ddlShiftType.Attributes.Add("onChange", "return ShiftChange('" + ddlShiftType.ClientID + "','" + ddlRemoveShiftPattern.ClientID + "','" + ddlAddShiftPattern.ClientID + "' " +
                         ",'" + ddlShift.ClientID + "','" + txtShiftRemainigDays.ClientID + "','" + txtShiftChangesDays.ClientID + "','" + cmdAddShift.ClientID + "','" + cmdRemovePattern.ClientID + "');");




            //Menu1.Items[0].Selected = true;
            bindShift();


            if (Request.QueryString["Value"] != null)
            {

                string Groupocde = Request.QueryString["Value"].ToString();

                Session["IsNewRecord"] = "N";
                bindTimeOffice(Groupocde);
            }
            else
            {
                Session["IsNewRecord"] = "Y";
            }
        }
    }
    protected void bindShift()
    {
        Strsql = "select shift 'shiftcode',shift+' - ' +substring(convert(varchar(10),starttime,108),0,6)+' - '+substring(convert(varchar(10),endtime,108),0,6) 'shiftname'  from tblshiftMaster where CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"'";
        ds = new DataSet();
        ds = cn.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlShift.DataSource = ds.Tables[0];
            ddlShift.DataBind();
            
            ddlAutoRun_AddShift.DataSource = ds.Tables[0];
            ddlAutoRun_AddShift.TextField = "shiftcode";
            ddlAutoRun_AddShift.ValueField = "shiftcode";
            ddlAutoRun_AddShift.DataBind();
            ddlAutoRun_AddShift.SelectedIndex = 0;

            ddlAutoRun_RemoveShift.DataSource = ds.Tables[0];
            ddlAutoRun_RemoveShift.TextField = "shiftcode";
            ddlAutoRun_RemoveShift.ValueField = "shiftcode";
            ddlAutoRun_RemoveShift.DataBind();
            ddlAutoRun_RemoveShift.SelectedIndex = 0;

            
            ddlAddShiftPattern.DataSource = ds.Tables[0];
            ddlAddShiftPattern.TextField = "shiftcode";
            ddlAddShiftPattern.ValueField = "shiftcode";
            ddlAddShiftPattern.DataBind();
            ddlAddShiftPattern.SelectedIndex = 0;

            
            ddlRemoveShiftPattern.DataSource = ds.Tables[0];
            ddlRemoveShiftPattern.TextField = "shiftcode";
            ddlRemoveShiftPattern.ValueField = "shiftcode";
            ddlRemoveShiftPattern.DataBind();
            ddlRemoveShiftPattern.SelectedIndex = 0;

            ddlHalfDayShift.DataSource = ds.Tables[0];
            ddlHalfDayShift.TextField = "shiftcode";
            ddlHalfDayShift.ValueField = "shiftcode";
            ddlHalfDayShift.DataBind();
            ddlHalfDayShift.SelectedIndex = 0;
            //ddlHalfDayShift.Items.Insert(0, "");
        }
        else
        {
            ddlShift.DataSource = null;
            ddlShift.DataBind();

            ddlAddShiftPattern.DataSource = null;
            ddlAddShiftPattern.DataBind();

            ddlRemoveShiftPattern.DataSource = null;
            ddlRemoveShiftPattern.DataBind();

            ddlAutoRun_AddShift.DataSource = null;
            ddlAutoRun_AddShift.DataBind();

            ddlAutoRun_RemoveShift.DataSource = ds.Tables[0];
            ddlAutoRun_RemoveShift.DataBind();

            ddlHalfDayShift.DataSource = null;
            ddlHalfDayShift.DataBind();
        }
    }

    protected void bindTimeOffice(string Groupocde)
    {
        try
        {

            Strsql = " SELECT [GroupID],[GroupName],[SHIFT],[SHIFTTYPE],[SHIFTPATTERN],[SHIFTREMAINDAYS],[LASTSHIFTPERFORMED],[INONLY],[ISPUNCHALL],[ISTIMELOSSALLOWED],[ALTERNATE_OFF_DAYS],[CDAYS] " +
                   " ,[ISROUNDTHECLOCKWORK],[ISOT],[OTRATE],[FIRSTOFFDAY],[SECONDOFFTYPE],[HALFDAYSHIFT],[SECONDOFFDAY],[PERMISLATEARRIVAL],[PERMISEARLYDEPRT],[ISAUTOSHIFT],[ISOUTWORK],[MAXDAYMIN],[ISOS],[AUTH_SHIFTS] " +
                   " ,[TIME],[SHORT],[HALF],[ISHALFDAY],[ISSHORT],[TWO],[isReleaver],[isWorker],[isFlexi],[SSN],[MIS],[IsCOF],[HLFAfter],[HLFBefore],[ResignedAfter],[EnableAutoResign],convert(varchar(5), S_End, 108) 'S_End'" +
                   " ,convert(varchar(5), S_Out, 108) 'S_Out',[AUTOSHIFT_LOW],[AUTOSHIFT_UP],[ISPRESENTONWOPRESENT],[ISPRESENTONHLDPRESENT],[NightShiftFourPunch],[ISAUTOABSENT],[ISAWA],[ISWA],[ISAW],[ISPREWO],[ISOTOUTMINUSSHIFTENDTIME]" +
                   " ,[ISOTWRKGHRSMINUSSHIFTHRS],[ISOTEARLYCOMEPLUSLATEDEP],[DEDUCTHOLIDAYOT],[DEDUCTWOOT],[ISOTEARLYCOMING],[OTMinus],[OTROUND],[OTEARLYDUR],[OTLATECOMINGDUR],[OTRESTRICTENDDUR],DUPLICATECHECKMIN,PREWO FROM [dbo].[tblEmployeeGroupPolicy] where GroupID='" + Groupocde.ToString().Trim() + "' and CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"'";



            ds = cn.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {

                txtGroupID.Text = ds.Tables[0].Rows[0]["GroupID"].ToString().Trim();
                txtGroupname.Text = ds.Tables[0].Rows[0]["GroupName"].ToString().Trim();

                txtDuplicateCheck.Text = ds.Tables[0].Rows[0]["DuplicateCheckMin"].ToString().Trim();
                txtWO.Text = ds.Tables[0].Rows[0]["PREWO"].ToString().Trim();
                EndTime_In = ds.Tables[0].Rows[0]["S_End"].ToString().Trim();
                EndTime_RTCEmp = ds.Tables[0].Rows[0]["S_Out"].ToString().Trim();
                txtPermisEarlyMin.Text = ds.Tables[0].Rows[0]["AutoShift_Low"].ToString().Trim();
                txtPermisLateMin.Text = ds.Tables[0].Rows[0]["AutoShift_Up"].ToString().Trim();
                txtDeductHLD.Text = ds.Tables[0].Rows[0]["DeductHolidayOT"].ToString().Trim();
                txtDeductWO.Text = ds.Tables[0].Rows[0]["DeductWoOT"].ToString().Trim();
                txtOT_EarlyComing.Text = ds.Tables[0].Rows[0]["OtEarlyDur"].ToString().Trim();
                txtOT_LateComing.Text = ds.Tables[0].Rows[0]["OtLateComingDur"].ToString().Trim();
                txtOT_Restricted.Text = ds.Tables[0].Rows[0]["OtRestrictEndDur"].ToString().Trim();
                FourPunch_Night = ds.Tables[0].Rows[0]["NightShiftFourPunch"].ToString().Trim();
                OTOutMinusShiftEndTime = ds.Tables[0].Rows[0]["isOTOutMinusShiftEndTime"].ToString().Trim();
                OTWrkgHrsMinusShiftHrs = ds.Tables[0].Rows[0]["isOTWrkgHrsMinusShiftHrs"].ToString().Trim();
                OTEarlyComePlusLateDep = ds.Tables[0].Rows[0]["isOTEarlyComePlusLateDep"].ToString().Trim();
                OTAllowed = ds.Tables[0].Rows[0]["isOT"].ToString().Trim();
                OTRound = ds.Tables[0].Rows[0]["OTROUND"].ToString().Trim();
                // OutWork = ds.Tables[0].Rows[0]["isOutWork"].ToString().Trim();
                OverStay = ds.Tables[0].Rows[0]["IsOS"].ToString().Trim();
                OT_EARLYCOMING = ds.Tables[0].Rows[0]["ISOTEARLYCOMING"].ToString().Trim();
                PresentOn_HLDPresent = ds.Tables[0].Rows[0]["isPresentOnHLDPresent"].ToString().Trim();
                PresentOn_WOPresent = ds.Tables[0].Rows[0]["isPresentOnWOPresent"].ToString().Trim();
                AbsentOn_WOAbsent = ds.Tables[0].Rows[0]["IsAutoAbsent"].ToString().Trim();
                awa = ds.Tables[0].Rows[0]["Isawa"].ToString().Trim();
                Wo_Absent = ds.Tables[0].Rows[0]["Isprewo"].ToString().Trim();
                AutoShift = ds.Tables[0].Rows[0]["IsAutoShift"].ToString().Trim();
                txtEndTime_In.Text = EndTime_In.ToString().Trim();
                txtEndTime_RTCEmp.Text = EndTime_RTCEmp.ToString();
                AW = ds.Tables[0].Rows[0]["ISAW"].ToString().Trim();
                WA = ds.Tables[0].Rows[0]["ISWA"].ToString().Trim();
                //Half Day marking



                //four punch in night shift
                if (FourPunch_Night == "Y")
                {
                    chkFourPunchNight.Checked = true;
                }
                else
                {
                    chkFourPunchNight.Checked = false;
                }

                if (OTOutMinusShiftEndTime == "Y")
                {
                    radOT.SelectedIndex = 0;
                }

                if (OTWrkgHrsMinusShiftHrs == "Y")
                {
                    radOT.SelectedIndex = 1;
                }
                if (OTEarlyComePlusLateDep == "Y")
                {
                    radOT.SelectedIndex = 2;
                }

                //OT allowed


                if (OTRound == "Y")
                {
                    chkRoundOT.Checked = true;
                }
                else
                {
                    chkRoundOT.Checked = false;
                }

                if (OT_EARLYCOMING == "Y")
                {
                    chkOT_Early.Checked = true;
                }
                else
                {
                    chkOT_Early.Checked = false;
                }

                if (PresentOn_HLDPresent == "Y")
                {
                    chkPresent_Present.Checked = true;
                }
                else
                {
                    chkPresent_Present.Checked = false;
                }

                if (PresentOn_WOPresent == "Y")
                {
                    chkPresent_WO.Checked = true;
                }
                else
                {
                    chkPresent_WO.Checked = false;
                }
                if (AbsentOn_WOAbsent == "Y")
                {
                    chkAutoAbsent.Checked = true;
                }
                else
                {
                    chkAutoAbsent.Checked = false;
                }

                if (awa == "Y")
                {
                    chkAWA_AAA.Checked = true;
                }
                else
                {
                    chkAWA_AAA.Checked = false;
                }
                if (AW == "Y")
                {
                    chkAW_AA.Checked = true;
                }
                else
                {
                    chkAW_AA.Checked = false;
                }
                if (WA == "Y")
                {
                    chkWA_AA.Checked = true;
                }
                else
                {
                    chkWA_AA.Checked = false;
                }


                if (Wo_Absent == "Y")
                {
                    chkWo_Absent.Checked = true;
                }
                else
                {
                    chkWo_Absent.Checked = false;
                }
                txtLateArrival.Text = hm.minutetohourNew(ds.Tables[0].Rows[0]["PERMISLATEARRIVAL"].ToString().Trim());
                txtEarlyDept.Text = hm.minutetohourNew(ds.Tables[0].Rows[0]["PERMISEARLYDEPRT"].ToString().Trim());
                txtMaxWorkHours.Text = hm.minutetohourNew(ds.Tables[0].Rows[0]["MAXDAYMIN"].ToString().Trim());



                //Round The clock
                string chkround = ds.Tables[0].Rows[0]["ISROUNDTHECLOCKWORK"].ToString().Trim();
                if (chkround.ToString() == "Y")
                {
                    chkRoundtheClock.Checked = true;
                }
                else
                {
                    chkRoundtheClock.Checked = false;
                }

                //Present day marking
                string considettimeloss = ds.Tables[0].Rows[0]["ISTIMELOSSALLOWED"].ToString().Trim();
                if (considettimeloss.ToString() == "Y")
                {
                    chkConsiderTmeloss.Checked = true;
                }
                else
                {
                    chkConsiderTmeloss.Checked = false;
                }
                //half day marking
                string ISHALFDAY = ds.Tables[0].Rows[0]["ISHALFDAY"].ToString().Trim();
                if (ISHALFDAY.ToString() == "Y")
                {
                    chkHalfdayMarking.Checked = true;
                }
                else
                {
                    chkHalfdayMarking.Checked = false;
                }

                //Short day marking
                string isshortday = ds.Tables[0].Rows[0]["ISSHORT"].ToString().Trim();
                if (isshortday.ToString() == "Y")
                {
                    chkShortMarking.Checked = true;
                }
                else
                {
                    chkShortMarking.Checked = false;
                }
                //Over Time
                string isOT = ds.Tables[0].Rows[0]["ISOT"].ToString().Trim();
                if (isOT.ToString() == "Y")
                {
                    ChkOTAppli.Checked = true;
                    txtOTRate.Enabled = true;
                    txtOTRate.Text = ds.Tables[0].Rows[0]["OTRATE"].ToString().Trim();
                }
                else
                {
                    ChkOTAppli.Checked = false;
                    txtOTRate.Enabled = false;
                    txtOTRate.Text = "";
                }
                //Over stay
                string isOS = ds.Tables[0].Rows[0]["ISOS"].ToString().Trim();
                if (isOS.ToString() == "Y")
                {
                    chkOverStay.Checked = true;
                }
                else
                {
                    chkOverStay.Checked = false;
                }

                string IsMIS = ds.Tables[0].Rows[0]["MIS"].ToString().Trim();
                if (IsMIS.ToString() == "H")
                {
                    chkMISAsHLF.Checked = true;
                    chkMISAsAbs.Checked = false;
                }
                else if (IsMIS.ToString() == "A")
                {
                    chkMISAsHLF.Checked = false;
                    chkMISAsAbs.Checked = false;
                }

                else
                {
                    chkMISAsHLF.Checked = false;
                    chkMISAsAbs.Checked = false;
                }
                string IsCoff = ds.Tables[0].Rows[0]["IsCOF"].ToString().Trim();
                if (IsCoff.ToString() == "C")
                {
                    chkAllowCOff.Checked = true;
                }

                else
                {
                    chkAllowCOff.Checked = false;
                }

                txtPresentMarkingDuration.Text = hm.minutetohour(ds.Tables[0].Rows[0]["Time"].ToString().Trim());
                txtMaxWorkHours_halfDay.Text = hm.minutetohour(ds.Tables[0].Rows[0]["half"].ToString().Trim());
                MaxWorkHoursForshort.Text = hm.minutetohour(ds.Tables[0].Rows[0]["short"].ToString().Trim());

                txtHLFAfter.Text = hm.minutetohour(ds.Tables[0].Rows[0]["HLFAfter"].ToString().Trim());
                txtHLFBefore.Text = hm.minutetohour(ds.Tables[0].Rows[0]["HLFBefore"].ToString().Trim());
               
                //Punches values

                inonly = ds.Tables[0].Rows[0]["INONLY"].ToString().Trim();
                isPunchAll = ds.Tables[0].Rows[0]["ISPUNCHALL"].ToString().Trim();
                Twopunch = ds.Tables[0].Rows[0]["TWO"].ToString().Trim();
                IsOutWork = ds.Tables[0].Rows[0]["ISOUTWORK"].ToString().Trim();

                if (inonly.ToString().Trim() == "N" && isPunchAll.ToString().Trim() == "N" && Twopunch.ToString().Trim() == "N" && IsOutWork.ToString().Trim() == "N")
                {
                   // radPunch.SelectedItem.Value = "NoPunch";
                    radPunch.SelectedIndex = 0;

                    chkSinglepunchonly.Enabled = false;
                }
                else if (inonly.ToString().Trim() == "O" && isPunchAll.ToString().Trim() == "Y" && Twopunch.ToString().Trim() == "N" && IsOutWork.ToString().Trim() == "N")
                {
                  //  radPunch.SelectedItem.Value = "SinglePunchOnly";
                    radPunch.SelectedIndex = 1;
                    
                    chkSinglepunchonly.Enabled = true;
                }
                else if (inonly.ToString().Trim() == "N" && isPunchAll.ToString().Trim() == "Y" && Twopunch.ToString().Trim() == "Y" && IsOutWork.ToString().Trim() == "N")
                {
                   // radPunch.SelectedItem.Value = "TwoPunch";
                    radPunch.SelectedIndex = 2;
                    chkSinglepunchonly.Enabled = false;
                }
                else if (inonly.ToString().Trim() == "N" && isPunchAll.ToString().Trim() == "Y" && Twopunch.ToString().Trim() == "N" && IsOutWork.ToString().Trim() == "N")
                {
                  //  radPunch.SelectedItem.Value = "FourPunch";
                    radPunch.SelectedIndex = 3;
                    chkSinglepunchonly.Enabled = false;
                }
                else if (inonly.ToString().Trim() == "N" && isPunchAll.ToString().Trim() == "Y" && Twopunch.ToString().Trim() == "N" && IsOutWork.ToString().Trim() == "Y")
                {
                  //  radPunch.SelectedItem.Value = "MultiplePunch";
                    radPunch.SelectedIndex = 4;
                    chkSinglepunchonly.Enabled = false;
                }
                //Shift Patterns
                ShiftCode = ds.Tables[0].Rows[0]["SHIFT"].ToString().Trim();
                if (ShiftCode != string.Empty)
                {
                    //ddlDepartment.SelectedItem.Text = Dept;// ds.Tables[0].Rows[0]["DepartmentCode"].ToString();
                    ddlShift.Value = ShiftCode;
                }
               // ddlShiftType.SelectedItem.Value = ds.Tables[0].Rows[0]["SHIFTTYPE"].ToString().Trim(); ;
                string ShiftType = ds.Tables[0].Rows[0]["SHIFTTYPE"].ToString().Trim();
                if (ShiftType.ToString().Trim() == "F")
                {
                    ddlShiftType.SelectedIndex = 0;
                    ddlShift.Enabled = true;
                    ddlAddShiftPattern.Enabled = false;
                    ddlRemoveShiftPattern.Enabled = false;
                    cmdAddShift.Enabled = false;
                    cmdRemovePattern.Enabled = false;
                    lblShiftPattern.Text = "";
                    txtShiftChangesDays.Text = "";
                    txtShiftRemainigDays.Text = "";
                }
                if (ShiftType.ToString().Trim() == "I")
                {
                    ddlShiftType.SelectedIndex = 2;
                    ddlShift.Enabled = false;
                    ddlAddShiftPattern.Enabled = false;
                    ddlRemoveShiftPattern.Enabled = false;
                    cmdAddShift.Enabled = false;
                    cmdRemovePattern.Enabled = false;
                    lblShiftPattern.Text = "";
                    txtShiftChangesDays.Text = "";
                    txtShiftRemainigDays.Text = "";
                }
                else if (ShiftType.ToString().Trim() == "R")
                {
                    ddlShiftType.SelectedIndex = 1;
                    txtShiftChangesDays.Text = ds.Tables[0].Rows[0]["CDAYS"].ToString().Trim();
                    txtShiftRemainigDays.Text = ds.Tables[0].Rows[0]["SHIFTREMAINDAYS"].ToString().Trim();
                    txtShiftRemainigDays.Enabled = true;
                    txtShiftChangesDays.Enabled = true;
                    ddlShift.Enabled = true;
                    ddlAddShiftPattern.Enabled = true;
                    ddlRemoveShiftPattern.Enabled = true;
                    cmdAddShift.Enabled = true;
                    cmdRemovePattern.Enabled = true;
                    string lstshift = "";
                    int lstindex = 0;
                    int diff = 0;
                    int totallen = 0;
                    string applen = "";
                    chkAutoRunShift.Checked = true;
                   // PanelAutoRunShift.Enabled = true;
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["shiftpattern"].ToString().Trim()))
                    {
                        totallen = Convert.ToInt32(ds.Tables[0].Rows[0]["shiftpattern"].ToString().Trim().Length);
                        lstindex = ds.Tables[0].Rows[0]["shiftpattern"].ToString().Trim().LastIndexOf(',');
                        if (lstindex > 0)
                        {
                            diff = totallen - lstindex;
                            lstshift = ds.Tables[0].Rows[0]["shiftpattern"].ToString().Substring(lstindex, diff);
                            lstindex = Convert.ToInt32(lstshift.Length) - 1;
                            if (lstindex == 1)
                            {
                                applen = "  ,";
                            }
                            else if (lstindex == 2)
                            {
                                applen = " ,";
                            }
                            else
                            {
                                applen = ",";
                            }
                        }
                        else
                        {
                            if (totallen == 1)
                            {
                                applen = "  ,";
                            }
                            else if (totallen == 2)
                            {
                                applen = " ,";
                            }
                            else
                            {
                                applen = ",";
                            }
                        }
                        lblShiftPattern.Text = ds.Tables[0].Rows[0]["shiftpattern"].ToString().Trim() + applen.ToString();
                    }

                }
                //Auto Shift
                string autoshift = ds.Tables[0].Rows[0]["isautoshift"].ToString().Trim();
                if (autoshift == "Y")
                {
                    string lstshift = "";
                    int lstindex = 0;
                    int diff = 0;
                    int totallen = 0;
                    string applen = "";
                    chkAutoRunShift.Checked = true;
                   // PanelAutoRunShift.Enabled = true;
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["auth_shifts"].ToString().Trim()))
                    {
                        totallen = Convert.ToInt32(ds.Tables[0].Rows[0]["auth_shifts"].ToString().Trim().Length);
                        lstindex = ds.Tables[0].Rows[0]["auth_shifts"].ToString().Trim().LastIndexOf(',');
                        if (lstindex > 0)
                        {
                            diff = totallen - lstindex;
                            lstshift = ds.Tables[0].Rows[0]["auth_shifts"].ToString().Substring(lstindex, diff);
                            lstindex = Convert.ToInt32(lstshift.Length) - 1;
                            if (lstindex == 1)
                            {
                                applen = "  ,";
                            }
                            else if (lstindex == 2)
                            {
                                applen = " ,";
                            }
                            else
                            {
                                applen = ",";
                            }
                        }
                        else
                        {
                            if (totallen == 1)
                            {
                                applen = "  ,";
                            }
                            else if (totallen == 2)
                            {
                                applen = " ,";
                            }
                            else
                            {
                                applen = ",";
                            }
                        }
                        txtAutoRun_Shift.Text = ds.Tables[0].Rows[0]["auth_shifts"].ToString().Trim() + applen.ToString();
                    }
                }
                else
                {
                    chkAutoRunShift.Checked = false;
                   // PanelAutoRunShift.Enabled = false;
                    txtAutoRun_Shift.Text = "";
                }

                string firstoff = ds.Tables[0].Rows[0]["FIRSTOFFDAY"].ToString();
                if (firstoff.ToString().Trim() == "NON")
                {
                    ddlFirstWeeklyOff.SelectedIndex = 0;
                }
                else if (firstoff.Trim()=="SUN")
                {
                    ddlFirstWeeklyOff.SelectedIndex = 1;
                    ddlFirstWeeklyOff.SelectedItem.Value = ds.Tables[0].Rows[0]["FIRSTOFFDAY"].ToString();
                }
                else if (firstoff.Trim() == "MON")
                {
                    ddlFirstWeeklyOff.SelectedIndex = 2;
                    ddlFirstWeeklyOff.SelectedItem.Value = ds.Tables[0].Rows[0]["FIRSTOFFDAY"].ToString();
                }
                else if (firstoff.Trim() == "TUE")
                {
                    ddlFirstWeeklyOff.SelectedIndex = 3;
                    ddlFirstWeeklyOff.SelectedItem.Value = ds.Tables[0].Rows[0]["FIRSTOFFDAY"].ToString();
                }
                else if (firstoff.Trim() == "WED")
                {
                    ddlFirstWeeklyOff.SelectedIndex = 4;
                    ddlFirstWeeklyOff.SelectedItem.Value = ds.Tables[0].Rows[0]["FIRSTOFFDAY"].ToString();
                }
                else if (firstoff.Trim() == "THU")
                {
                    ddlFirstWeeklyOff.SelectedIndex = 5;
                    ddlFirstWeeklyOff.SelectedItem.Value = ds.Tables[0].Rows[0]["FIRSTOFFDAY"].ToString();
                }
                else if (firstoff.Trim() == "FRI")
                {
                    ddlFirstWeeklyOff.SelectedIndex = 6;
                    ddlFirstWeeklyOff.SelectedItem.Value = ds.Tables[0].Rows[0]["FIRSTOFFDAY"].ToString();
                }
                else if (firstoff.Trim() == "SAT")
                {
                    ddlFirstWeeklyOff.SelectedIndex = 7;
                    ddlFirstWeeklyOff.SelectedItem.Value = ds.Tables[0].Rows[0]["FIRSTOFFDAY"].ToString();
                }

                string secondoff = ds.Tables[0].Rows[0]["SECONDOFFDAY"].ToString().Trim();
                if ((secondoff.ToString().Trim() == "NON") || (secondoff.ToString().Trim() == ""))
                {
                    ddlSecondWeeklyOff.SelectedIndex = 0;
                    ddlSecondWeeklyOffType.Enabled = false;
                   // ddlSecondWeeklyOffType.Items.Insert(0, "");
                    ddlHalfDayShift.Enabled = false;
                  //  ddlHalfDayShift.Items.Insert(0, "");
                    chk1.Enabled = false;
                    chk2.Enabled = false;
                    chk3.Enabled = false;
                    chk4.Enabled = false;
                    chk5.Enabled = false;
                    chk1.Text = "";
                    chk2.Text = "";
                    chk3.Text = "";
                    chk4.Text = "";
                    chk5.Text = "";
                }
                else
                {

                    if (secondoff.ToString().Trim() == "NON")
                    {
                        ddlSecondWeeklyOff.SelectedIndex = 0;
                    }
                    else if (secondoff.Trim() == "SUN")
                    {
                        ddlSecondWeeklyOff.SelectedIndex = 1;
                        ddlSecondWeeklyOff.SelectedItem.Value = ds.Tables[0].Rows[0]["SECONDOFFDAY"].ToString();
                    }
                    else if (secondoff.Trim() == "MON")
                    {
                        ddlSecondWeeklyOff.SelectedIndex = 2;
                        ddlSecondWeeklyOff.SelectedItem.Value = ds.Tables[0].Rows[0]["SECONDOFFDAY"].ToString();
                    }
                    else if (secondoff.Trim() == "TUE")
                    {
                        ddlSecondWeeklyOff.SelectedIndex = 3;
                        ddlSecondWeeklyOff.SelectedItem.Value = ds.Tables[0].Rows[0]["SECONDOFFDAY"].ToString();
                    }
                    else if (secondoff.Trim() == "WED")
                    {
                        ddlSecondWeeklyOff.SelectedIndex = 4;
                        ddlSecondWeeklyOff.SelectedItem.Value = ds.Tables[0].Rows[0]["SECONDOFFDAY"].ToString();
                    }
                    else if (secondoff.Trim() == "THU")
                    {
                        ddlSecondWeeklyOff.SelectedIndex = 5;
                        ddlSecondWeeklyOff.SelectedItem.Value = ds.Tables[0].Rows[0]["SECONDOFFDAY"].ToString();
                    }
                    else if (secondoff.Trim() == "FRI")
                    {
                        ddlSecondWeeklyOff.SelectedIndex = 6;
                        ddlSecondWeeklyOff.SelectedItem.Value = ds.Tables[0].Rows[0]["SECONDOFFDAY"].ToString();
                    }
                    else if (secondoff.Trim() == "SAT")
                    {
                        ddlSecondWeeklyOff.SelectedIndex = 7;
                        ddlSecondWeeklyOff.SelectedItem.Value = ds.Tables[0].Rows[0]["SECONDOFFDAY"].ToString();
                    }
                    
                    ddlSecondWeeklyOffType.Enabled = true;
                    chk1.Enabled = true;
                    chk2.Enabled = true;
                    chk3.Enabled = true;
                    chk4.Enabled = true;
                    chk5.Enabled = true;
                    chk1.Text = "1";
                    chk2.Text = "2";
                    chk3.Text = "3";
                    chk4.Text = "4";
                    chk5.Text = "5";
                }
                string SecondWeeklyOffType = ds.Tables[0].Rows[0]["SECONDOFFTYPE"].ToString().Trim();
                if (SecondWeeklyOffType.ToString().Trim() == "")
                {
                    ddlSecondWeeklyOffType.SelectedIndex = 0;
                }
                else if (SecondWeeklyOffType.ToString().Trim() == "F")
                {
                    ddlSecondWeeklyOffType.SelectedIndex = 0;
                    ddlSecondWeeklyOffType.SelectedItem.Value = ds.Tables[0].Rows[0]["SECONDOFFTYPE"].ToString();
                    ddlHalfDayShift.Enabled = false;
                    //ddlHalfDayShift.Items.Insert(0, "");
                }
                else if (SecondWeeklyOffType.ToString().Trim() == "H")
                {
                    ddlSecondWeeklyOffType.SelectedIndex = 1;
                    ddlSecondWeeklyOffType.SelectedItem.Value = ds.Tables[0].Rows[0]["SECONDOFFTYPE"].ToString();
                    ddlHalfDayShift.Enabled = true;
                }

                if (ds.Tables[0].Rows[0]["HALFDAYSHIFT"].ToString().Trim() == "")
                {
                    ddlHalfDayShift.Enabled = false;
                }
                else
                {
                    ddlHalfDayShift.Enabled = true;
                    ddlHalfDayShift.SelectedItem.Value = ds.Tables[0].Rows[0]["HALFDAYSHIFT"].ToString();
                }

                //Alternate off days
                string alternatedays = ds.Tables[0].Rows[0]["ALTERNATE_OFF_DAYS"].ToString().Trim();
                if (alternatedays.Contains("1"))
                {
                    chk1.Checked = true;
                }
                else
                {
                    chk1.Checked = false;
                }
                if (alternatedays.Contains("2"))
                {
                    chk2.Checked = true;
                }
                else
                {
                    chk2.Checked = false;
                }
                if (alternatedays.Contains("3"))
                {
                    chk3.Checked = true;
                }
                else
                {
                    chk3.Checked = false;
                }
                if (alternatedays.Contains("4"))
                {
                    chk4.Checked = true;
                }
                else
                {
                    chk4.Checked = false;
                }
                if (alternatedays.Contains("5"))
                {
                    chk5.Checked = true;
                }
                else
                {
                    chk5.Checked = false;
                }

                txtGroupID.Enabled = false;
                Session["IsNewRecord"] = "N";
            }


        }
        catch (Exception ex)
        {

            Error_Occured("BindData", ex.Message);
        }
    }


    protected void BindGridData(string GroupCode)
    {
        try
        {



        }
        catch
        {

        }

    }


    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        try
        {


            if (txtGroupname.Text.Length == 0 || txtGroupID.Text.Length == 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Enter Group Name');", true);
                txtGroupID.Focus();
                return;
            }
            if ((ddlShiftType.SelectedItem.Value.ToString().Trim() == "F") && ddlShift.Text.ToString().Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Please Select Shift');", true);
                ASPxPageControl1.ActiveTabIndex = 1;
                ddlShift.Focus();
                return;
            }



            if ((ddlShiftType.SelectedItem.Value.ToString().Trim() == "R") && ((txtShiftChangesDays.Text.ToString().Trim() == "") || (txtShiftChangesDays.Text.ToString().Trim() == "00")))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Shift Change after how many days should not be empty when shift type is Rotational.');", true);
                ASPxPageControl1.ActiveTabIndex = 1;
                txtShiftChangesDays.Enabled = true;
                txtShiftRemainigDays.Enabled = true;
                ddlAddShiftPattern.Enabled = true;
                cmdAddShift.Enabled = true;
                cmdRemovePattern.Enabled = true;
                txtShiftChangesDays.Focus();
                return;
            }
            if ((ddlShiftType.SelectedItem.Value.ToString().Trim() == "R") && (lblShiftPattern.Text.ToString().Trim() == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Shift choosen for employee is not available in Shift Pattern.');", true);
                ASPxPageControl1.ActiveTabIndex = 1;
                ddlAddShiftPattern.Enabled = true;
                cmdAddShift.Enabled = true;
                cmdRemovePattern.Enabled = true;
                txtShiftChangesDays.Enabled = true;
                txtShiftRemainigDays.Enabled = true;
                ddlShift.Focus();
                return;
            }

            try
            {
                string[]  AllotShift = ddlShift.Text.ToString().Trim().Split('-');
                ShiftCode = ddlShift.Value.ToString().Trim();

            }
            catch
            {
                ShiftCode = "GEN";
            }
            if (ShiftCode.Trim()=="")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Select Shift');", true);
                ddlShift.Focus();
                return;
            }



            if (!string.IsNullOrEmpty(txtLateArrival.Text.ToString()))
            {
                late = hm.hour(txtLateArrival.Text);
            }
            if (!string.IsNullOrEmpty(txtEarlyDept.Text.ToString()))
            {
                EarlyDept = hm.hour(txtEarlyDept.Text);
            }
            if (!string.IsNullOrEmpty(txtMaxWorkHours.Text.ToString()))
            {
                MaxDayWork = hm.hour(txtMaxWorkHours.Text);
            }
            if (!string.IsNullOrEmpty(txtPresentMarkingDuration.Text.ToString()))
            {
                PresentMarkingDuration = hm.hour(txtPresentMarkingDuration.Text);
            }
            if (!string.IsNullOrEmpty(txtMaxWorkHours_halfDay.Text.ToString()))
            {
                workinghourforhalfday = hm.hour(txtMaxWorkHours_halfDay.Text);
            }
            if (!string.IsNullOrEmpty(MaxWorkHoursForshort.Text.ToString()))
            {
                workhourforshort = hm.hour(MaxWorkHoursForshort.Text);
            }
            if (!string.IsNullOrEmpty(txtHLFAfter.Text.ToString()))
            {
                HalfAfter = hm.hour(txtHLFAfter.Text);
            }
            if (!string.IsNullOrEmpty(txtHLFBefore.Text.ToString()))
            {
                HalfBefore = hm.hour(txtHLFBefore.Text);
            }
            //if (!string.IsNullOrEmpty(txtResignedAfter.Text.ToString()))
            //{
            //    ResignedAfter = Convert.ToInt32(txtResignedAfter.Text.ToString());
            //}


            //







            //------------Remove last comma from shift pattern
            if (lblShiftPattern.Text.Trim() != "")
            {
                finalShifts = lblShiftPattern.Text.ToString().Trim();
                if (!string.IsNullOrEmpty(finalShifts))
                {
                    finalShifts = finalShifts.Remove(finalShifts.LastIndexOf(','));
                }
                lblShiftPattern.Text = finalShifts.ToString();

                //finalShifts = lblShiftPattern.Text.ToString();
                //finalShifts = finalShifts.Remove(finalShifts.LastIndexOf(','));
                //lblShiftPattern.Text = finalShifts.ToString();
            }

            //--------------Alter off days
            if (!chk1.Checked && !chk2.Checked && !chk3.Checked && !chk4.Checked && !chk5.Checked)
            {
                days = "";
            }
            if (chk1.Checked)
            {
                days += chk1.Text;
            }
            if (chk2.Checked)
            {
                days += chk2.Text;
            }
            if (chk3.Checked)
            {
                days += chk3.Text;
            }
            if (chk4.Checked)
            {
                days += chk4.Text;
            }
            if (chk5.Checked)
            {
                days += chk5.Text;
            }

            //----------------Consider Time Loss
            if (chkConsiderTmeloss.Checked)
                TimeLoss = "Y";
            else
                TimeLoss = "N";

            //----------------Round Clock
            if (chkRoundtheClock.Checked)
                RoundClock = "Y";
            else
                RoundClock = "N";

            //---------------Short day
            if (chkShortMarking.Checked)
            {
                isshort = "Y";
            }
            else
            {
                isshort = "N";
                workhourforshort = 0;
            }

            //------------Half Holiday
            if (chkHalfdayMarking.Checked)
            {
                ishalfday = "Y";
            }
            else
            {
                ishalfday = "N";
                workinghourforhalfday = 0;
            }

            //------------------Over Stay
            if (chkOverStay.Checked)
                OS = "Y";
            else
                OS = "N";


            //------------Over Time----------------
            if (ChkOTAppli.Checked)
            {
                IsOt = "Y";
            }
            else
            {
                IsOt = "N";
                txtOTRate.Text = "000.00";
            }

            if (chkMISAsHLF.Checked)
            {
                IsMIS = "H";
            }
            else if (chkMISAsAbs.Checked)
            {
                IsMIS = "A";
            }
            else
            {
                IsMIS = "P";
            }
            if (chkAllowCOff.Checked)
            {
                IsCoff = "O";
            }
            else
            {
                IsCoff = "C";
            }
            //if (chkAutoResign.Checked)
            //{
            //    EnableAutoResign = "Y";
            //}



            //------------Punches combinations----------------
            if (radPunch.SelectedItem.Value.ToString().Trim() == "NoPunch")
            {
                inonly = "N";
                isPunchAll = "N";
                Twopunch = "N";
                IsOutWork = "N";
            }
            else if (radPunch.SelectedItem.Value.ToString().Trim() == "SinglePunchOnly")
            {
                inonly = "O";
                isPunchAll = "Y";
                Twopunch = "N";
                IsOutWork = "N";
            }
            else if (radPunch.SelectedItem.Value.ToString().Trim() == "TwoPunch")
            {
                inonly = "N";
                isPunchAll = "Y";
                Twopunch = "Y";
                IsOutWork = "N";
            }
            else if (radPunch.SelectedItem.Value.ToString().Trim() == "FourPunch")
            {
                inonly = "N";
                isPunchAll = "Y";
                Twopunch = "N";
                IsOutWork = "N";
            }
            else if (radPunch.SelectedItem.Value.ToString().Trim() == "MultiplePunch")
            {
                inonly = "N";
                isPunchAll = "Y";
                Twopunch = "N";
                IsOutWork = "Y";
            }

            //-----------------Auto Shift----------------------
            if (chkAutoRunShift.Checked)
            {
                IsAuthShift = "Y";
                AuthShifts = txtAutoRun_Shift.Text.ToString().Trim();
                if (!string.IsNullOrEmpty(AuthShifts))
                {
                    AuthShifts = AuthShifts.Remove(AuthShifts.LastIndexOf(','));
                }
                txtAutoRun_Shift.Text = AuthShifts.ToString();
            }
            else
            {
                IsAuthShift = "N";
                AuthShifts = "";
            }
            if (AuthShifts.ToString().Length > 50)
            {
                AuthShifts = AuthShifts.Substring(0, 50);
            }
            if (ddlFirstWeeklyOff.SelectedItem.Text.ToString().Trim() == "None")
            {
                FirstWeeklyoff = "";
            }
            else
            {
                FirstWeeklyoff = ddlFirstWeeklyOff.SelectedItem.Value.ToString().Trim().ToUpper();
            }
            if (ddlSecondWeeklyOff.SelectedItem.Text.ToString().Trim() == "NONE")
            {
                SecondWeeklyoff = "NON";
                HalfDayShift = " ";
                SecondOffType = "";
                days = "";
            }
            if (ddlSecondWeeklyOff.SelectedItem.Text.Trim().ToString() != "NONE" && ddlSecondWeeklyOffType.SelectedItem.Value.ToString().Trim() == "")
            {
                SecondWeeklyoff = ddlSecondWeeklyOff.SelectedItem.Value.ToString().Trim().ToUpper();
                HalfDayShift = "";
                SecondOffType = "F";
            }
            else if (ddlSecondWeeklyOff.SelectedItem.Text.Trim().ToString() != "NONE" && ddlSecondWeeklyOffType.SelectedItem.Value.ToString().Trim() == "F")
            {
                SecondWeeklyoff = ddlSecondWeeklyOff.SelectedItem.Value.ToString().Trim().ToUpper();
                HalfDayShift = "";
                SecondOffType = ddlSecondWeeklyOffType.SelectedItem.Value.ToString().ToString();
            }
            else if (ddlSecondWeeklyOff.SelectedItem.Text.ToString().Trim() != "NONE" && ddlSecondWeeklyOffType.SelectedItem.Value.ToString().Trim() == "H")
            {
                SecondWeeklyoff = ddlSecondWeeklyOff.SelectedItem.Value.ToString().Trim().ToUpper();
                HalfDayShift = ddlHalfDayShift.SelectedItem.Value.ToString().Trim();
                SecondOffType = ddlSecondWeeklyOffType.SelectedItem.Value.ToString().ToString();
            }

            if (ddlSecondWeeklyOff.SelectedItem.Text != "NONE" && string.IsNullOrEmpty(days))
            {
                chk1.Enabled = true;
                chk2.Enabled = true;
                chk3.Enabled = true;
                chk4.Enabled = true;
                chk5.Enabled = true;
                chk1.Text = ddlSecondWeeklyOff.SelectedItem.Text;
                chk2.Text =  ddlSecondWeeklyOff.SelectedItem.Text;
                chk3.Text =  ddlSecondWeeklyOff.SelectedItem.Text;
                chk4.Text =  ddlSecondWeeklyOff.SelectedItem.Text;
                chk5.Text =  ddlSecondWeeklyOff.SelectedItem.Text;
                //Menu1.Items[3].Selected = true;
                ASPxPageControl1.ActiveTabIndex = 1;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Please Select Second Weekly Off Days From Checkboxs');", true);
                return;
            }
            // if (ddlFirstWeeklyOff.SelectedItem.Text == "NONE" && ddlSecondWeeklyOff.SelectedItem.Text == "NONE")
            //if (ddlFirstWeeklyOff.SelectedItem.Text.ToString().Trim() == ddlSecondWeeklyOff.SelectedItem.Text.ToString().Trim() && ddlFirstWeeklyOff.SelectedItem.Text != "NONE" && ddlSecondWeeklyOff.SelectedItem.Text != "NONE")
            //{
            //    ASPxPageControl1.ActiveTabIndex = 1;
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('First Off & Second Off Can Not Be Same');", true);
            //    return;
            //}



            //four punch in night shift
            if (chkFourPunchNight.Checked == true)
            {
                FourPunch_Night = "Y";
            }
            else
            {
                FourPunch_Night = "N";
            }

            if (radOT.SelectedItem.Value == "1")
            {
                OTOutMinusShiftEndTime = "Y";
            }
            else
            {
                OTOutMinusShiftEndTime = "N";
            }

            if (radOT.SelectedItem.Value == "2")
            {
                OTWrkgHrsMinusShiftHrs = "Y";
            }
            else
            {
                OTWrkgHrsMinusShiftHrs = "N";
            }

            if (radOT.SelectedItem.Value == "3")
            {
                OTEarlyComePlusLateDep = "Y";
            }
            else
            {
                OTEarlyComePlusLateDep = "N";
            }

            //OT allowed


            if (chkRoundOT.Checked == true)
            {
                OTRound = "Y";
            }
            else
            {
                OTRound = "N";
            }




            if (chkOT_Early.Checked == true)
            {
                OT_EARLYCOMING = "Y";
            }
            else
            {
                OT_EARLYCOMING = "N";
            }

            if (chkPresent_Present.Checked == true)
            {
                PresentOn_HLDPresent = "Y";
            }
            else
            {
                PresentOn_HLDPresent = "N";
            }

            if (chkPresent_WO.Checked == true)
            {
                PresentOn_WOPresent = "Y";
            }
            else
            {
                PresentOn_WOPresent = "N";
            }
            if (chkAutoAbsent.Checked == true)
            {
                AbsentOn_WOAbsent = "Y";
            }
            else
            {
                AbsentOn_WOAbsent = "N";
            }

            if (chkAWA_AAA.Checked == true)
            {
                awa = "Y";
            }
            else
            {
                awa = "N";
            }

            if (chkAW_AA.Checked)
            {
                AW = "Y";
            }
            else
            {
                AW = "N";
            }
            if (chkWA_AA.Checked)
            {
                WA = "Y";
            }
            else
            {
                WA = "N";
            }



            if (chkWo_Absent.Checked == true)
            {
                Wo_Absent = "Y";
            }
            else
            {
                Wo_Absent = "N";
            }

            if (txtOT_EarlyComing.Text.ToString().Trim() == "")
            {
                txtOT_EarlyComing.Text = "0";
            }
            if (txtOT_LateComing.Text.ToString().Trim() == "")
            {
                txtOT_LateComing.Text = "0";
            }
            if (txtPermisEarlyMin.Text.ToString().Trim() == "")
            {
                txtPermisEarlyMin.Text = "0";
            }
            if (txtPermisLateMin.Text.ToString().Trim() == "")
            {
                txtPermisLateMin.Text = "0";
            }
            if (txtDeductHLD.Text.ToString().Trim() == "")
            {
                txtDeductHLD.Text = "0";
            }
            if (txtDeductWO.Text.ToString().Trim() == "")
            {
                txtDeductWO.Text = "0";
            }
            string SSN = "";
            EndTime_In = System.DateTime.Now.ToString("yyyy-MM-dd ") + txtEndTime_In.Text.ToString().Trim();
            EndTime_RTCEmp = System.DateTime.Now.ToString("yyyy-MM-dd ") + txtEndTime_RTCEmp.Text.ToString().Trim();
            if (Session["IsNewRecord"] == "Y")
            {
                Strsql = "select * from tblEmployeeGroupPolicy where GroupID='" + txtGroupID.Text.Trim().ToString() + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() +"' ";
                DataSet DsTest = new DataSet();

                DsTest = cn.FillDataSet(Strsql);
                if (DsTest.Tables[0].Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Group Code Already Exists');", true);
                    return;
                }


                else
                {
                    try
                    {
                        Strsql1 = " INSERT INTO [dbo].[tblEmployeeGroupPolicy]([GroupID],[GroupName],[SHIFT],[SHIFTTYPE],[SHIFTPATTERN],[SHIFTREMAINDAYS],[INONLY],[ISPUNCHALL],[ISTIMELOSSALLOWED] " +
                   " ,[ALTERNATE_OFF_DAYS],[CDAYS],[ISROUNDTHECLOCKWORK],[ISOT],[OTRATE],[FIRSTOFFDAY],[SECONDOFFTYPE],[HALFDAYSHIFT],[SECONDOFFDAY],[PERMISLATEARRIVAL],[PERMISEARLYDEPRT],[ISAUTOSHIFT],[ISOUTWORK] " +
                   " ,[MAXDAYMIN],[ISOS],[AUTH_SHIFTS],[TIME],[SHORT],[HALF],[ISHALFDAY],[ISSHORT],[TWO],[isReleaver],[isWorker],[isFlexi],[SSN],[MIS],[IsCOF],[HLFAfter],[HLFBefore],[ResignedAfter],[EnableAutoResign] " +
                   " ,[S_END],[S_OUT],[AUTOSHIFT_LOW],[AUTOSHIFT_UP],[ISPRESENTONWOPRESENT],[ISPRESENTONHLDPRESENT],[NightShiftFourPunch],[ISAUTOABSENT],[ISAWA],[ISWA],[ISAW],[ISPREWO],[ISOTOUTMINUSSHIFTENDTIME],[ISOTWRKGHRSMINUSSHIFTHRS] " +
                   " ,[ISOTEARLYCOMEPLUSLATEDEP],[DEDUCTHOLIDAYOT],[DEDUCTWOOT],[ISOTEARLYCOMING],[OTMinus],[OTROUND],[OTEARLYDUR],[OTLATECOMINGDUR],[OTRESTRICTENDDUR],DUPLICATECHECKMIN,PREWO,CompanyCode) VALUES ";



                        Strsql1 += "('" + txtGroupID.Text.Trim().ToString().Trim() + "','" + txtGroupname.Text.ToString().Trim() + "', ";
                        Strsql1 += " '" + ShiftCode.Trim() + "','" + ddlShiftType.SelectedItem.Value.ToString().Trim() + "', ";
                        Strsql1 += " '" + lblShiftPattern.Text.ToString() + "','" + txtShiftRemainigDays.Text.ToString().Trim() + "', ";
                        Strsql1 += " '" + inonly.ToString().Trim() + "','" + isPunchAll.ToString().Trim() + "', ";
                        Strsql1 += " '" + TimeLoss.ToString().Trim() + "','" + days.ToString().Trim() + "','" + txtShiftChangesDays.Text.ToString().Trim() + "', ";
                        Strsql1 += " '" + RoundClock.ToString().Trim() + "','" + IsOt.ToString().Trim() + "' , ";
                        Strsql1 += " '" + txtOTRate.Text.ToString().Trim() + "' , '" + FirstWeeklyoff.ToString().Trim() + "','" + SecondOffType.ToString().ToString().Trim() + "', ";
                        Strsql1 += " '" + HalfDayShift.ToString().Trim() + "','" + SecondWeeklyoff.ToString().Trim() + "','" + late.ToString().Trim() + "', ";
                        Strsql1 += " '" + EarlyDept.ToString().Trim() + "','" + IsAuthShift.ToString().Trim() + "','" + IsOutWork.ToString().Trim() + "','" + MaxDayWork.ToString().Trim() + "' , ";
                        Strsql1 += " '" + OS.ToString().Trim() + "','" + AuthShifts.ToString() + "', ";
                        Strsql1 += " '" + PresentMarkingDuration.ToString().Trim() + "','" + workhourforshort.ToString().Trim() + "', ";
                        Strsql1 += " '" + workinghourforhalfday.ToString().Trim() + "','" + ishalfday.ToString().Trim() + "','" + isshort.ToString().Trim() + "', ";
                        Strsql1 += " '" + Twopunch.ToString().Trim() + "','N','N','N','" + SSN + "', '" + IsMIS + "','" + IsCoff + "','" + HalfAfter + "','" + HalfBefore + "','" + ResignedAfter + "','" + EnableAutoResign + "',";
                        Strsql1 += " '" + EndTime_In.ToString().Trim() + "','" + EndTime_RTCEmp.ToString().Trim() + "','" + txtPermisEarlyMin.Text.ToString().Trim() + "','" + txtPermisLateMin.Text.ToString().Trim() + "',";
                        Strsql1 += " '" + PresentOn_WOPresent + "','" + PresentOn_HLDPresent + "','" + FourPunch_Night + "','" + AbsentOn_WOAbsent + "','" + awa + "','" + WA + "','" + AW + "','" + Wo_Absent + "','" + OTOutMinusShiftEndTime.Trim() + "','" + OTWrkgHrsMinusShiftHrs + "',";
                        Strsql1 += " '" + OTEarlyComePlusLateDep.ToString().Trim() + "','" + txtDeductHLD.Text.Trim().ToString() + "','" + txtDeductWO.Text.Trim().ToString() + "','" + OT_EARLYCOMING + "','N','" + OTRound + "',";
                        Strsql1 += " " + txtOT_EarlyComing.Text.Trim().ToString() + "," + txtOT_LateComing.Text.Trim().ToString() + "," + txtOT_Restricted.Text.Trim().ToString() + "," + txtDuplicateCheck.Text.ToString().Trim() + "," + txtWO.Text.ToString().Trim() + ",'"+Session["LoginCompany"].ToString().Trim()+"')";

                        result = cn.execute_NonQuery(Strsql1);
                        if (result > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Employee Group Saved Successfully');", true);
                            Response.Redirect("EmployeeGroup.aspx");
                        }
                    }

                    catch (Exception ex)
                    {
                        string output = "Error";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('" + output + "');", true);
                        return;
                    }
                }




            }
            else
            {
                try
                {
                    Strsql1 = "UPDATE [dbo].[tblEmployeeGroupPolicy]SET [GroupName] ='" + txtGroupname.Text.ToString().Trim() + "',[SHIFT] ='" + ddlShift.Value.ToString().Trim() + "',[SHIFTTYPE] ='" + ddlShiftType.SelectedItem.Value.ToString().Trim() + "'" +
                                         ",[SHIFTPATTERN] ='" + lblShiftPattern.Text.ToString().Trim() + "',[SHIFTREMAINDAYS] ='" + txtShiftRemainigDays.Text.ToString().Trim() + "',[LASTSHIFTPERFORMED] = '',[INONLY] = '" + inonly.ToString().Trim() + "',[ISPUNCHALL] = '" + isPunchAll.ToString().Trim() + "'" +
                                         ",[ISTIMELOSSALLOWED] ='" + TimeLoss.ToString().Trim() + "',[ALTERNATE_OFF_DAYS] = '" + days.ToString().Trim() + "',[CDAYS] = '" + txtShiftChangesDays.Text.ToString().Trim() + "',[ISROUNDTHECLOCKWORK] = '" + RoundClock.ToString().Trim() + "',[ISOT] = '" + IsOt.ToString().Trim() + "'" +
                                         ",[OTRATE] ='" + txtOTRate.Text.ToString().Trim() + "',[FIRSTOFFDAY] = '" + FirstWeeklyoff.ToString().Trim() + "',[SECONDOFFTYPE] = '" + SecondOffType.ToString().Trim() + "',[HALFDAYSHIFT] ='" + HalfDayShift.ToString().Trim() + "',[SECONDOFFDAY] = '" + SecondWeeklyoff.ToString().Trim() + "'" +
                                         ",[PERMISLATEARRIVAL] = '" + late.ToString().Trim() + "',[PERMISEARLYDEPRT] = '" + EarlyDept.ToString().Trim() + "',[ISAUTOSHIFT] = '" + IsAuthShift.ToString().Trim() + "',[ISOUTWORK] = '" + IsOutWork.ToString().Trim() + "',[MAXDAYMIN] = '" + MaxDayWork.ToString().Trim() + "'" +
                                         ",[ISOS] = '" + OS.ToString().Trim() + "',[AUTH_SHIFTS] ='" + AuthShifts.ToString().Trim() + "',[TIME] = '" + PresentMarkingDuration.ToString().Trim() + "',[SHORT] ='" + workhourforshort.ToString().Trim() + "',[HALF] = '" + workinghourforhalfday.ToString().Trim() + "',[ISHALFDAY] = '" + ishalfday.ToString().Trim() + "',[ISSHORT] ='" + isshort.ToString().Trim() + "'" +
                                         ",[TWO] = '" + Twopunch.ToString().Trim() + "',[isReleaver] = 'N',[isWorker] ='N',[isFlexi] ='N',[SSN] ='',[MIS] = '" + IsMIS.ToString().Trim() + "',[IsCOF] = '" + IsCoff.ToString().Trim() + "'" +
                                         ",[HLFAfter] ='" + HalfAfter.ToString().Trim() + "',[HLFBefore] ='" + HalfBefore.ToString().Trim() + "',[ResignedAfter] = '" + ResignedAfter.ToString().Trim() + "',[EnableAutoResign] = '" + EnableAutoResign.ToString().Trim() + "',[S_END] = '" + EndTime_In.ToString().Trim() + "',[S_OUT] ='" + EndTime_RTCEmp.ToString().Trim() + "'" +
                                         ",[AUTOSHIFT_LOW] = '" + txtPermisEarlyMin.Text.ToString().Trim() + "',[AUTOSHIFT_UP] ='" + txtPermisLateMin.Text.ToString().Trim() + "',[ISPRESENTONWOPRESENT] = '" + PresentOn_WOPresent.ToString().Trim() + "',[ISPRESENTONHLDPRESENT] = '" + PresentOn_HLDPresent.ToString().Trim() + "'" +
                                         ",[NightShiftFourPunch] ='" + FourPunch_Night.ToString().Trim() + "',[ISAUTOABSENT] = '" + AbsentOn_WOAbsent.ToString().Trim() + "',[ISAWA] ='" + awa.ToString().Trim() + "',[ISWA] ='" + WA.ToString().Trim() + "',[ISAW] ='" + AW.ToString().Trim() + "',[ISPREWO] = '" + Wo_Absent.ToString().Trim() + "'" +
                                         ",[ISOTOUTMINUSSHIFTENDTIME] ='" + OTOutMinusShiftEndTime.ToString().Trim() + "',[ISOTWRKGHRSMINUSSHIFTHRS] ='" + OTWrkgHrsMinusShiftHrs.ToString().Trim() + "',[ISOTEARLYCOMEPLUSLATEDEP] = '" + OTEarlyComePlusLateDep.ToString().Trim() + "'" +
                                         ",[DEDUCTHOLIDAYOT] = '" + txtDeductHLD.Text.Trim().ToString() + "',[DEDUCTWOOT] = '" + txtDeductWO.Text.Trim().ToString() + "',[ISOTEARLYCOMING] = '" + OT_EARLYCOMING + "',[OTMinus] = 'N',[OTROUND] = '" + OTRound + "',[OTEARLYDUR] = '" + txtOT_EarlyComing.Text.ToString().Trim() + "'" +
                                         ",[OTLATECOMINGDUR] = '" + txtOT_LateComing.Text.ToString().Trim() + "',[OTRESTRICTENDDUR] = " + txtOT_Restricted.Text.Trim().ToString() + ",[DUPLICATECHECKMIN] = " + txtDuplicateCheck.Text.Trim().ToString() + ",[PREWO] =" + txtWO.Text.ToString().Trim() + " where GroupID='" + txtGroupID.Text.ToString().Trim() + "' and CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"'";



                    result = cn.execute_NonQuery(Strsql1);
                    if (result > 0)
                    {

                        UpdateEmployeeShiftMaster(txtGroupID.Text.ToString().Trim());

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Employee Group Updated Successfully');", true);
                        Response.Redirect("EmployeeGroup.aspx");
                    }
                }
                catch (Exception ex)
                {
                    Error_Occured("Save", ex.Message);
                    string output = "Error:" + ex.Message;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('" + output + "');", true);
                    return;
                }
            }
            


        }
        catch (Exception ex)
        {
            Error_Occured("Save", ex.Message);
            string output = "Error: "+ex.Message;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('" + output + "');", true);
            return;
        }
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        //Call The function to write the error log file 

        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        //try
        //{
        ec.Write_Log(PageName, FunctionName, ErrorMsg);
        //}
        //catch (Exception ess)
        //{ }
    }
    protected void UpdateEmployeeShiftMaster(string GroupID)
    {
        string sSql = "";
        DataSet DsEmp = new DataSet();
        List<string> EmployeeList = new List<string>();
        int AUTOSHIFT_LOW = 240;
        int AUTOSHIFT_UP = 240;

        int DEDUCTHOLIDAYOT = 0;
        int DEDUCTWOOT = 0;
        string ISOTEARLYCOMING = "N";
        string OTMinus = "N";
        int OTEARLYDUR = 0;
        int OTLATECOMINGDUR = 0;
        int OTRESTRICTENDDUR = 0;
        int DUPLICATECHECKMIN = 0;
        int PREWO = 0;
        DateTime Endtime_IN1 = System.DateTime.MinValue;
        DateTime EndTime_RTCEmp1 = System.DateTime.MinValue;
        try
        {
            sSql = "select SSN from tblEmployee where GroupID='" + GroupID.Trim() + "' and Active='Y' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
            DsEmp = cn.FillDataSet(sSql);
            if (DsEmp.Tables[0].Rows.Count > 0)
            {
                for (int K = 0; K < DsEmp.Tables[0].Rows.Count; K++)
                {
                    EmployeeList.Add(DsEmp.Tables[0].Rows[K]["SSN"].ToString().Trim());
                }
                sSql = "";
                sSql = "select * from tblEmployeeGroupPolicy where GroupID='" + GroupID.Trim() + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' ";
                DataSet DsEmpGrp = new DataSet();
                DsEmpGrp = cn.FillDataSet(sSql);
                if (DsEmpGrp.Tables[0].Rows.Count > 0)
                {

                    try
                    {

                       
                        late = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["PERMISLATEARRIVAL"].ToString().Trim());
                        EarlyDept = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["PERMISEARLYDEPRT"].ToString().Trim());
                        MaxDayWork = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["MAXDAYMIN"].ToString().Trim());
                        PresentMarkingDuration = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["TIME"].ToString().Trim());
                        workinghourforhalfday = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["HALF"].ToString().Trim());
                        workhourforshort = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["SHORT"].ToString().Trim());
                        HalfAfter = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["HLFAfter"].ToString().Trim());
                        HalfBefore = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["HLFBefore"].ToString().Trim());
                        ResignedAfter = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["ResignedAfter"].ToString().Trim());
                        lblShiftPattern.Text = DsEmpGrp.Tables[0].Rows[0]["SHIFTPATTERN"].ToString().Trim();
                        days = DsEmpGrp.Tables[0].Rows[0]["ALTERNATE_OFF_DAYS"].ToString().Trim();
                        TimeLoss = DsEmpGrp.Tables[0].Rows[0]["ISTIMELOSSALLOWED"].ToString().Trim();
                        RoundClock = DsEmpGrp.Tables[0].Rows[0]["ISROUNDTHECLOCKWORK"].ToString().Trim();
                        isshort = DsEmpGrp.Tables[0].Rows[0]["ISSHORT"].ToString().Trim();
                        if (isshort == "N")
                        {
                            workhourforshort = 0;
                        }
                        ishalfday = DsEmpGrp.Tables[0].Rows[0]["ISHALFDAY"].ToString().Trim();
                        if (ishalfday == "N")
                        {
                            workinghourforhalfday = 0;
                        }
                        OS = DsEmpGrp.Tables[0].Rows[0]["ISOS"].ToString().Trim();
                        IsOt = DsEmpGrp.Tables[0].Rows[0]["ISOT"].ToString().Trim();
                        if (IsOt == "N")
                        {
                            txtOTRate.Text = "000.00";
                        }
                        IsMIS = DsEmpGrp.Tables[0].Rows[0]["MIS"].ToString().Trim();
                        IsCoff = DsEmpGrp.Tables[0].Rows[0]["IsCOF"].ToString().Trim();
                        EnableAutoResign = DsEmpGrp.Tables[0].Rows[0]["EnableAutoResign"].ToString().Trim();
                        inonly = DsEmpGrp.Tables[0].Rows[0]["INONLY"].ToString().Trim();
                        isPunchAll = DsEmpGrp.Tables[0].Rows[0]["ISPUNCHALL"].ToString().Trim();
                        Twopunch = DsEmpGrp.Tables[0].Rows[0]["TWO"].ToString().Trim();
                        IsOutWork = DsEmpGrp.Tables[0].Rows[0]["ISOUTWORK"].ToString().Trim();
                        IsAuthShift = DsEmpGrp.Tables[0].Rows[0]["ISAUTOSHIFT"].ToString().Trim();

                        if (IsAuthShift == "Y")
                        {
                            txtAutoRun_Shift.Text = DsEmpGrp.Tables[0].Rows[0]["AUTH_SHIFTS"].ToString().Trim();
                            AuthShifts = txtAutoRun_Shift.Text.ToString().Trim();
                            if (!string.IsNullOrEmpty(AuthShifts))
                            {
                                AuthShifts = AuthShifts.Remove(AuthShifts.LastIndexOf(','));
                            }
                            txtAutoRun_Shift.Text = AuthShifts.ToString();
                        }
                        else
                        {
                            IsAuthShift = "N";
                            AuthShifts = "";
                        }

                        if (AuthShifts.ToString().Length > 50)
                        {
                            AuthShifts = AuthShifts.Substring(0, 50);
                        }
                        FirstWeeklyoff = DsEmpGrp.Tables[0].Rows[0]["FIRSTOFFDAY"].ToString().Trim();
                        SecondWeeklyoff = DsEmpGrp.Tables[0].Rows[0]["SECONDOFFDAY"].ToString().Trim();
                        HalfDayShift = DsEmpGrp.Tables[0].Rows[0]["HALFDAYSHIFT"].ToString().Trim();
                        SecondOffType = DsEmpGrp.Tables[0].Rows[0]["SECONDOFFTYPE"].ToString().Trim();
                        days = DsEmpGrp.Tables[0].Rows[0]["ALTERNATE_OFF_DAYS"].ToString().Trim();


                        FourPunch_Night = DsEmpGrp.Tables[0].Rows[0]["NightShiftFourPunch"].ToString().Trim();
                        OTOutMinusShiftEndTime = DsEmpGrp.Tables[0].Rows[0]["ISOTOUTMINUSSHIFTENDTIME"].ToString().Trim();
                        OTWrkgHrsMinusShiftHrs = DsEmpGrp.Tables[0].Rows[0]["ISOTWRKGHRSMINUSSHIFTHRS"].ToString().Trim();
                        OTEarlyComePlusLateDep = DsEmpGrp.Tables[0].Rows[0]["ISOTEARLYCOMEPLUSLATEDEP"].ToString().Trim();

                        OTRound = DsEmpGrp.Tables[0].Rows[0]["OTROUND"].ToString().Trim();
                        OT_EARLYCOMING = DsEmpGrp.Tables[0].Rows[0]["ISOTEARLYCOMING"].ToString().Trim();
                        PresentOn_HLDPresent = DsEmpGrp.Tables[0].Rows[0]["ISPRESENTONHLDPRESENT"].ToString().Trim();
                        PresentOn_WOPresent = DsEmpGrp.Tables[0].Rows[0]["ISPRESENTONWOPRESENT"].ToString().Trim();
                        AbsentOn_WOAbsent = DsEmpGrp.Tables[0].Rows[0]["ISAUTOABSENT"].ToString().Trim();
                        awa = DsEmpGrp.Tables[0].Rows[0]["ISAWA"].ToString().Trim();
                        AW = DsEmpGrp.Tables[0].Rows[0]["ISAW"].ToString().Trim();
                        WA = DsEmpGrp.Tables[0].Rows[0]["ISWA"].ToString().Trim();
                        Wo_Absent = DsEmpGrp.Tables[0].Rows[0]["ISPREWO"].ToString().Trim();
                        EndTime_In = DsEmpGrp.Tables[0].Rows[0]["S_END"].ToString().Trim();
                        EndTime_RTCEmp = DsEmpGrp.Tables[0].Rows[0]["S_OUT"].ToString().Trim();
                        try
                        {
                            Endtime_IN1 = Convert.ToDateTime(EndTime_In.ToString());
                            EndTime_RTCEmp1 = Convert.ToDateTime(EndTime_RTCEmp.ToString());
                        }
                        catch
                        {

                        }
                        DEDUCTHOLIDAYOT = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["DEDUCTHOLIDAYOT"].ToString().Trim());
                        DEDUCTWOOT = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["DEDUCTWOOT"].ToString().Trim());
                        ISOTEARLYCOMING = DsEmpGrp.Tables[0].Rows[0]["ISOTEARLYCOMING"].ToString().Trim();
                        OTMinus = DsEmpGrp.Tables[0].Rows[0]["OTMinus"].ToString().Trim();
                        OTEARLYDUR = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["OTEARLYDUR"].ToString().Trim());
                        OTLATECOMINGDUR = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["OTLATECOMINGDUR"].ToString().Trim());
                        OTRESTRICTENDDUR = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["OTRESTRICTENDDUR"].ToString().Trim());
                        DUPLICATECHECKMIN = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["DUPLICATECHECKMIN"].ToString().Trim());
                        PREWO = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["PREWO"].ToString().Trim());
                        AUTOSHIFT_LOW = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["AUTOSHIFT_LOW"].ToString().Trim());
                        AUTOSHIFT_UP = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["AUTOSHIFT_UP"].ToString().Trim());
                
                        Strsql1 = "update tblEmployeeShiftMaster set SHIFT='" + ddlShift.Value.ToString().Trim() + "',SHIFTTYPE='" + ddlShiftType.SelectedItem.Value.ToString().Trim() + "', " +
                         "SHIFTPATTERN='" + lblShiftPattern.Text.ToString() + "', " +
                         "SHIFTREMAINDAYS='" + txtShiftRemainigDays.Text.ToString().Trim() + "',INONLY='" + inonly.ToString().Trim() + "', " +
                         "ISPUNCHALL='" + isPunchAll.ToString().Trim() + "',ISTIMELOSSALLOWED='" + TimeLoss.ToString().Trim() + "', " +
                         "ALTERNATE_OFF_DAYS='" + days.ToString().Trim() + "',CDAYS='" + txtShiftChangesDays.Text.ToString().Trim() + "', " +
                         "ISROUNDTHECLOCKWORK='" + RoundClock.ToString().Trim() + "',ISOT='" + IsOt.ToString().Trim() + "' , " +
                         "OTRATE='" + txtOTRate.Text.ToString().Trim() + "',FIRSTOFFDAY='" + ddlFirstWeeklyOff.SelectedItem.Value.ToString().Trim() + "', " +
                         "SECONDOFFTYPE='" + SecondOffType.ToString() + "',HALFDAYSHIFT='" + HalfDayShift.ToString() + "', " +
                         "SECONDOFFDAY='" + SecondWeeklyoff.ToString() + "',PERMISLATEARRIVAL='" + late.ToString().Trim() + "',PERMISEARLYDEPRT='" + EarlyDept.ToString().Trim() + "',ISAUTOSHIFT='" + IsAuthShift.ToString() + "', " +
                         "IsOutWork='" + IsOutWork.ToString().Trim() + "',MAXDAYMIN='" + MaxDayWork.ToString().Trim() + "'," +
                         "ISOS='" + OS.ToString().Trim() + "',AUTH_SHIFTS='" + AuthShifts.ToString() + "', " +
                         "[TIME]='" + PresentMarkingDuration.ToString().Trim() + "',Short='" + workhourforshort.ToString().Trim() + "', " +
                         "Half='" + workinghourforhalfday.ToString().Trim() + "',ISHALFDAY='" + ishalfday.ToString().Trim() + "', " +
                         "ISSHORT='" + isshort.ToString().Trim() + "',TWO='" + Twopunch.ToString().Trim() + "',MIS='" + IsMIS + "',IsCOF='" + IsCoff + "',  " +
                         " HLFAfter='" + HalfAfter + "',HLFBefore='" + HalfBefore + "',ResignedAfter='" + ResignedAfter + "',EnableAutoResign='" + EnableAutoResign + "', " +
                         " [S_END]='" + Endtime_IN1.ToString("yyyy-MM-dd HH:ss") + "',[S_OUT]='" + EndTime_RTCEmp1.ToString("yyyy-MM-dd HH:mm") + "',[AUTOSHIFT_LOW]='" + AUTOSHIFT_LOW + "',[AUTOSHIFT_UP]='" + AUTOSHIFT_UP + "',[ISPRESENTONWOPRESENT]='" + PresentOn_WOPresent + "',[ISPRESENTONHLDPRESENT]='" + PresentOn_HLDPresent + "',[NightShiftFourPunch]='" + FourPunch_Night + "'," +
                         " [ISAUTOABSENT]='" + AbsentOn_WOAbsent + "',[ISAWA]='" + awa + "',[ISWA]='" + WA + "',[ISAW]='" + AW + "',[ISPREWO]='" + Wo_Absent + "',[ISOTOUTMINUSSHIFTENDTIME]='" + OTOutMinusShiftEndTime + "' ,[ISOTWRKGHRSMINUSSHIFTHRS]='" + OTWrkgHrsMinusShiftHrs + "',[ISOTEARLYCOMEPLUSLATEDEP]='" + OTEarlyComePlusLateDep + "'," +
                         "[DEDUCTHOLIDAYOT]='" + DEDUCTHOLIDAYOT.ToString().Trim() + "',[DEDUCTWOOT]='" + DEDUCTWOOT.ToString().Trim() + "',[ISOTEARLYCOMING]='" + ISOTEARLYCOMING.ToString().Trim() + "',[OTMinus]='" + OTMinus.ToString().Trim() + "',[OTROUND]='" + OTRound.ToString().Trim() + "',[OTEARLYDUR]='" + OTEARLYDUR.ToString().Trim() + "',[OTLATECOMINGDUR]='" + OTLATECOMINGDUR.ToString().Trim() + "',[OTRESTRICTENDDUR]='" + OTRESTRICTENDDUR.ToString().Trim() + "',[DUPLICATECHECKMIN]='" + DUPLICATECHECKMIN.ToString().Trim() + "' " +
                         ",[PREWO]='" + PREWO.ToString().Trim() + "' where SSN IN ('" + String.Join("', '", EmployeeList.ToArray()) + "')  ";

                        result1 = cn.execute_NonQuery(Strsql1);


                    }
                    catch
                    {

                        Strsql1 = "update tblEmployeeShiftMaster set SHIFT='" + ddlShift.Value.ToString().Trim() + "',SHIFTTYPE='" + ddlShiftType.SelectedItem.Value.ToString().Trim() + "', " +
                           "SHIFTPATTERN='" + lblShiftPattern.Text.ToString() + "', " +
                           "SHIFTREMAINDAYS='" + txtShiftRemainigDays.Text.ToString().Trim() + "',INONLY='" + inonly.ToString().Trim() + "', " +
                           "ISPUNCHALL='" + isPunchAll.ToString().Trim() + "',ISTIMELOSSALLOWED='" + TimeLoss.ToString().Trim() + "', " +
                           "ALTERNATE_OFF_DAYS='" + days.ToString().Trim() + "',CDAYS='" + txtShiftChangesDays.Text.ToString().Trim() + "', " +
                           "ISROUNDTHECLOCKWORK='" + RoundClock.ToString().Trim() + "',ISOT='" + IsOt.ToString().Trim() + "' , " +
                           "OTRATE='" + txtOTRate.Text.ToString().Trim() + "',FIRSTOFFDAY='" + ddlFirstWeeklyOff.SelectedItem.Value.ToString().Trim() + "', " +
                           "SECONDOFFTYPE='" + SecondOffType.ToString() + "',HALFDAYSHIFT='" + HalfDayShift.ToString() + "', " +
                           "SECONDOFFDAY='" + SecondWeeklyoff.ToString() + "',PERMISLATEARRIVAL='" + late.ToString().Trim() + "',PERMISEARLYDEPRT='" + EarlyDept.ToString().Trim() + "',ISAUTOSHIFT='" + IsAuthShift.ToString() + "', " +
                           "IsOutWork='" + IsOutWork.ToString().Trim() + "',MAXDAYMIN='" + MaxDayWork.ToString().Trim() + "'," +
                           "ISOS='" + OS.ToString().Trim() + "',AUTH_SHIFTS='" + AuthShifts.ToString() + "', " +
                           "[TIME]='" + PresentMarkingDuration.ToString().Trim() + "',Short='" + workhourforshort.ToString().Trim() + "', " +
                           "Half='" + workinghourforhalfday.ToString().Trim() + "',ISHALFDAY='" + ishalfday.ToString().Trim() + "', " +
                           "ISSHORT='" + isshort.ToString().Trim() + "',TWO='" + Twopunch.ToString().Trim() + "',MIS='" + IsMIS + "',IsCOF='" + IsCoff + "',  " +
                           " HLFAfter='" + HalfAfter + "',HLFBefore='" + HalfBefore + "',ResignedAfter='" + ResignedAfter + "',EnableAutoResign='" + EnableAutoResign + "', " +
                           " [S_END]='" + Endtime_IN1.ToString("yyyy-MM-dd HH:ss") + "',[S_OUT]='" + EndTime_RTCEmp1.ToString("yyyy-MM-dd HH:ss") + "',[AUTOSHIFT_LOW]='" + AUTOSHIFT_LOW + "',[AUTOSHIFT_UP]='" + AUTOSHIFT_UP + "',[ISPRESENTONWOPRESENT]='" + PresentOn_WOPresent + "',[ISPRESENTONHLDPRESENT]='" + PresentOn_HLDPresent + "',[NightShiftFourPunch]='" + FourPunch_Night + "'," +
                           " [ISAUTOABSENT]='" + AbsentOn_WOAbsent + "',[ISAWA]='" + awa + "',[ISWA]='" + WA + "',[ISAW]='" + AW + "',[ISPREWO]='" + Wo_Absent + "',[ISOTOUTMINUSSHIFTENDTIME]='" + OTOutMinusShiftEndTime + "' ,[ISOTWRKGHRSMINUSSHIFTHRS]='" + OTWrkgHrsMinusShiftHrs + "',[ISOTEARLYCOMEPLUSLATEDEP]='" + OTEarlyComePlusLateDep + "'," +
                           "[DEDUCTHOLIDAYOT]='" + DEDUCTHOLIDAYOT.ToString().Trim() + "',[DEDUCTWOOT]='" + DEDUCTWOOT.ToString().Trim() + "',[ISOTEARLYCOMING]='" + ISOTEARLYCOMING.ToString().Trim() + "',[OTMinus]='" + OTMinus.ToString().Trim() + "',[OTROUND]='" + OTRound.ToString().Trim() + "',[OTEARLYDUR]='" + OTEARLYDUR.ToString().Trim() + "',[OTLATECOMINGDUR]='" + OTLATECOMINGDUR.ToString().Trim() + "',[OTRESTRICTENDDUR]='" + OTRESTRICTENDDUR.ToString().Trim() + "',[DUPLICATECHECKMIN]='" + DUPLICATECHECKMIN.ToString().Trim() + "' " +
                           ",[PREWO]='" + PREWO.ToString().Trim() + "' where SSN IN ('" + String.Join("', '", EmployeeList.ToArray()) + "') ";

                        result1 = cn.execute_NonQuery(Strsql1);

                    }



                }






            }
        }
        catch
        {

        }


    }
    protected void ddlSecondWeeklyOff_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlSecondWeeklyOff.SelectedItem.Value.ToString() == "NON")
        {
            ddlSecondWeeklyOffType.Enabled = false;
            ddlHalfDayShift.Enabled = false;
            chk1.Enabled = false;
            chk2.Enabled = false;
            chk3.Enabled = false;
            chk4.Enabled = false;
            chk5.Enabled = false;
            chk1.Checked = false;
            chk2.Checked = false;
            chk3.Checked = false;
            chk4.Checked = false;
            chk5.Checked = false;
            chk1.Text = "";
            chk2.Text = "";
            chk3.Text = "";
            chk4.Text = "";
            chk5.Text = "";
        }
        else if (ddlSecondWeeklyOff.SelectedItem.Value.ToString() == ddlFirstWeeklyOff.SelectedItem.Value.ToString())
        {
            ddlSecondWeeklyOff.SelectedIndex = 0;
            ddlSecondWeeklyOffType.Enabled = false;
            ddlHalfDayShift.Enabled = false;
            chk1.Enabled = false;
            chk2.Enabled = false;
            chk3.Enabled = false;
            chk4.Enabled = false;
            chk5.Enabled = false;
            chk1.Text = "";
            chk2.Text = "";
            chk3.Text = "";
            chk4.Text = "";
            chk5.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('First And Second Weekly Off Can Not Be Same');", true);
            return;
        }
        else
        {
            //ddlSecondWeeklyOff.SelectedIndex = 0;
            ddlSecondWeeklyOffType.Enabled = true;
            ddlSecondWeeklyOffType.SelectedIndex = 1;
            ddlHalfDayShift.Enabled = true;
            chk1.Enabled = true;
            chk2.Enabled = true;
            chk3.Enabled = true;
            chk4.Enabled = true;
            chk5.Enabled = true;
            chk1.Text = "I";// ddlSecondWeeklyOff.SelectedItem.Text;
            chk2.Text = "II";// ddlSecondWeeklyOff.SelectedItem.Text;
            chk3.Text = "III";// ddlSecondWeeklyOff.SelectedItem.Text;
            chk4.Text = "IV";// ddlSecondWeeklyOff.SelectedItem.Text;
            chk5.Text = "V";// ddlSecondWeeklyOff.SelectedItem.Text;
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('First and Second weekly off may not be same');", true);
            //return;
        }
    }
    protected void cmdAddShift_Click(object sender, ImageClickEventArgs e)
    {
        int sLen = 0;
        string applen = "";
        str += ddlAddShiftPattern.Value.ToString().Trim();
        str1 = Label1.Text.ToString().Trim();
        if (str1.Contains(ddlAddShiftPattern.Value.ToString().Trim()))
        {

        }
        else
        {
            sLen = Convert.ToInt32(str.Length);
            if (sLen == 1)
            {
                applen = "  ";
            }
            else if (sLen == 2)
            {
                applen = " ";
            }
            else
            {
                applen = "";
            }

            Authrun += str.ToString() + applen.ToString() + ",";
            lblShiftPattern.Text += Authrun.ToString();
        }



        cmdAddShift.Enabled = true;
        cmdRemovePattern.Enabled = true;
        ddlAddShiftPattern.Enabled = true;
        ddlRemoveShiftPattern.Enabled = true;
        txtShiftChangesDays.Enabled = true;
        txtShiftRemainigDays.Enabled = true;
    }
    protected void chkAutoRunShift_CheckedChanged(object sender, EventArgs e)
    {
        if (chkAutoRunShift.Checked)
        {
            RunAutoShift.Visible = true;
        }
        else
        {
            RunAutoShift.Visible = true;
            txtAutoRun_Shift.Text = "";
        }
    }
    protected void cmdRemovePattern_Click(object sender, ImageClickEventArgs e)
    {
        int sLen = 0;
        string applen = "";
        str = ddlAutoRun_RemoveShift.SelectedItem.Text.ToString();
        sLen = Convert.ToInt32(str.Length);
        if (sLen == 1)
        {
            applen = "  ";
        }
        else if (sLen == 2)
        {
            applen = " ";
        }
        else
        {
            applen = "";
        }

        str1 = txtAutoRun_Shift.Text.ToString();
        if (str1.Contains(ddlAutoRun_RemoveShift.SelectedItem.Text.ToString()))
        {
            str = str + applen + ",";
            str1 = str1.Replace(str, "");
            txtAutoRun_Shift.Text = str1.ToString();
        }
        cmdAddShift.Enabled = true;
        cmdRemovePattern.Enabled = true;
        ddlAddShiftPattern.Enabled = true;
        ddlRemoveShiftPattern.Enabled = true;
        txtShiftChangesDays.Enabled = true;
        txtShiftRemainigDays.Enabled = true;
    }
    protected void cmdAddShift_AutoRun_Click(object sender, ImageClickEventArgs e)
    {
        int sLen = 0;
        string applen = "";
        str += ddlAutoRun_AddShift.SelectedItem.Text.ToString().Trim();
        str1 = txtAutoRun_Shift.Text.ToString().Trim();
        if (str1.Contains(ddlAutoRun_AddShift.SelectedItem.Text.ToString().Trim()))
        {

        }
        else
        {
            sLen = Convert.ToInt32(str.Length);
            if (sLen == 1)
            {
                applen = "  ";
            }
            else if (sLen == 2)
            {
                applen = " ";
            }
            else
            {
                applen = "";
            }

            Authrun += str.ToString() + applen.ToString() + ",".ToString().Trim();
            txtAutoRun_Shift.Text += Authrun.ToString();
        }

        cmdAddShift.Enabled = true;
        cmdRemovePattern.Enabled = true;
        ddlAddShiftPattern.Enabled = true;
        ddlRemoveShiftPattern.Enabled = true;
        txtShiftChangesDays.Enabled = true;
        txtShiftRemainigDays.Enabled = true;
    }
    protected void chkSinglepunchonly_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void radPunch_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (radPunch.SelectedItem.Value == "SinglePunchOnly")
        {
            chkSinglepunchonly.Enabled = true;
        }
        else
        {
            chkSinglepunchonly.Enabled = false;
        }
    }
    protected void ChkOTAppli_CheckedChanged(object sender, EventArgs e)
    {
        if (ChkOTAppli.Checked)
        {
            txtOTRate.Enabled = true;
        }
        else
        {
            txtOTRate.Enabled = true;
            txtOTRate.Text = "000.00";
        }
    }
    protected void ddlShiftType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(ddlShiftType.SelectedItem.Value.ToString().Trim()=="F")
        {
            ddlShift.Enabled = true;
            ddlRemoveShiftPattern.Enabled = false;
            ddlAddShiftPattern.Enabled = false;
            txtShiftChangesDays.Enabled = false;
            txtShiftRemainigDays.Enabled = false;
            cmdAddShift.Enabled = false;
            cmdRemovePattern.Enabled = false;
            lblShiftPattern.Text = "";

        }
        else if(ddlShiftType.SelectedItem.Value.ToString().Trim()=="R")
        {
            ddlShift.Enabled = true;
            ddlRemoveShiftPattern.Enabled = true;
            ddlAddShiftPattern.Enabled = true;
            txtShiftChangesDays.Enabled = true;
            txtShiftRemainigDays.Enabled = true;
            cmdAddShift.Enabled = true;
            cmdRemovePattern.Enabled = true;
           

        }
        else if (ddlShiftType.SelectedItem.Value.ToString().Trim() == "I")
        {
            ddlShift.Enabled = false;
            ddlRemoveShiftPattern.Enabled = false;
            ddlAddShiftPattern.Enabled = false;
            txtShiftChangesDays.Enabled = false;
            txtShiftRemainigDays.Enabled = false;
            cmdAddShift.Enabled = false;
            cmdRemovePattern.Enabled = false;
            lblShiftPattern.Text = "";
        }
    }
    protected void ddlSecondWeeklyOffType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(ddlSecondWeeklyOff.SelectedItem.Value.ToString().Trim()=="H")
        {
            ddlHalfDayShift.Enabled = true;
        }
        else
        {
            ddlHalfDayShift.Enabled = false;
        }
    }
}