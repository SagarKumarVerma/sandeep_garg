﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="frmLeavelevel.aspx.cs" Inherits="frmLeavelevel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
   <table align="center" style="width:100%" cellpadding="0" cellspacing="0" >
<tr>
<td ></td>
</tr>

<tr>
<td align="center">


    <table align="center" style="width:100%; height: 400px;vertical-align:top"  cellpadding="0" cellspacing="0" class="tableCss">
        <tr>
            <td colspan="2" style="width: 100%;">
                <table style="width: 100%;" cellpadding="0" cellspacing="0" >
                    <tr align="left">
                        <td colspan="3" align="center" class="tableHeaderCss" style="width:850px;height:25px">                            
                            <asp:Label ID="LblFrmName" runat="server" CssClass="lblCss"></asp:Label></td>
                    </tr>
                    <tr align="center">
                        <td align="left" colspan="3" style="width: 730px; height: 10px">
                        </td>
                    </tr>
                    <tr align="center">
                        <td align="left" colspan="3" style="width: 730px; height: 15px" abbr="a" class="text_new">
                        <asp:Label ID="lblDept" runat="server"></asp:Label>&nbsp;
                            <asp:DropDownList ID="DdlDept" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DdlDept_SelectedIndexChanged" CssClass="DropDownCss" Width="173px">
                            </asp:DropDownList>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<asp:Label ID="Label1" runat="server" Text="Update " Width="56px" ForeColor="#333366"></asp:Label>
                            <asp:RadioButton ID="RadEmpLevel" runat="server" AutoPostBack="True" GroupName="Group2" OnCheckedChanged="RadEmpLevel_CheckedChanged" Text="Employee Level" Visible="False" ForeColor="#333366" />
                            <asp:RadioButton ID="RadLvApproval" runat="server" AutoPostBack="True" GroupName="Group2" OnCheckedChanged="RadLvApproval_CheckedChanged" Text="Leave Approval Stages" Checked="True" ForeColor="#333366" />
                            <asp:RadioButton ID="RdHeadId" runat="server" AutoPostBack="True" GroupName="Group2"
                                OnCheckedChanged="RdHeadId_CheckedChanged" Text="HeadId" Width="74px" ForeColor="#333366" /></td>
                    </tr>
                    <tr align="center">
                        <td abbr="a" align="left" class="text_new" colspan="3" style="width: 730px; height: 5px">
                        </td>
                    </tr>
                    <tr align="center">
                        <td align="left" colspan="3" style="width: 730px; height: 10px" class="text_new" valign="middle">
                            <asp:Label ID="LblSel" runat="server" Font-Bold="True" Text="Stages" ></asp:Label>
                            &nbsp;&nbsp; &nbsp;&nbsp;
                            <asp:DropDownList ID="combo_Level" runat="server" OnSelectedIndexChanged="combo_Level_SelectedIndexChanged"
                                Width="48px" CssClass="DropDownCss"></asp:DropDownList>&nbsp;
                            <asp:RadioButton ID="RadPaycode1" runat="server" Checked="True" GroupName="Group3" OnCheckedChanged="RadPaycode1_CheckedChanged"
                                Text="Employee Code" AutoPostBack="True" /><asp:RadioButton ID="RadName1" runat="server" GroupName="Group3" OnCheckedChanged="RadPaycode1_CheckedChanged"
                                Text="Name" AutoPostBack="True" />
                            
                            <asp:DropDownList ID="TxtHeadId1" runat="server" CssClass="DropDownCss"
                                Width="250px">
                            </asp:DropDownList>&nbsp;
                            <asp:Button ID="btn_Update" runat="server" Text="Update" OnClick="btn_Update_Click" CssClass="buttoncss"  /></td>
                    </tr>
                    <tr align="center">
                        <td align="left" colspan="3" style="width: 730px; height: 5px">
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" colspan="3" class="text_new" >
                        Search&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;
                            <asp:RadioButton ID="RadPaycode" runat="server" Checked="True" GroupName="Group1" OnCheckedChanged="RadPaycode_CheckedChanged"
                                Text="Employee Code" AutoPostBack="True" />
                            <asp:RadioButton ID="RadName" runat="server" GroupName="Group1" OnCheckedChanged="RadPaycode_CheckedChanged"
                                Text="Name" AutoPostBack="True" />&nbsp;<asp:DropDownList ID="ddlPaycode" runat="server"
                                    CssClass="DropDownCss" Width="250px">
                                </asp:DropDownList>&nbsp;
                            <asp:Button ID="Button1" runat="server" CssClass="buttoncss" OnClick="Button1_Click1" Text="Search"  />
                            <asp:Button ID="cmdShowAll" runat="server" Text="Show All " CssClass="buttoncss" OnClick="cmdShowAll_Click"  />
                            <asp:Button ID="cmdCancel" runat="server" Text="Cancel" CssClass="buttoncss" OnClick="cmdCancel_Click" />
                            </td>
                    </tr>
                    <tr>
                        <td align="right" class=" " colspan="3" width="100%">
                        <asp:TextBox ID="TxtSearch" runat="server" CssClass="TextBoxCss" Width="175px" Visible="False"></asp:TextBox><asp:TextBox ID="TxtHeadId" runat="server" Width="75px" Visible="False"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td  colspan="3" style="width: 730px; height: 5px">
                        </td>
                    </tr>
                    <tr align="justify" >
                        <td colspan="3" style="height: 15px; width: 100%;" valign="top">
                  <div style="overflow:auto;width:99%;height:400px" >
                  
                  
                  <asp:DataGrid ID="DataGrid1" runat ="server"  GridLines="Vertical" Width="98%" CssClass="mData" HeaderStyle-CssClass="mDatath" ItemStyle-CssClass="mDatatd" >
                      <Columns>
                          <asp:TemplateColumn HeaderText="Select">
                              <HeaderTemplate>                                  
                                  <asp:CheckBox ID="chk_SelectAll" runat="server" AutoPostBack="True" OnCheckedChanged="chk_SelectAll_CheckedChanged1" />
                              </HeaderTemplate>
                              <ItemTemplate>
                                  <asp:CheckBox ID="chk_EmpCode" runat="server" />
                              </ItemTemplate>
                          </asp:TemplateColumn>
                      </Columns>
                      <ItemStyle CssClass="mDatatd" />
                      <HeaderStyle CssClass="mDatath" />
                  
                  </asp:DataGrid>
                  
                      </div>
                      
                            
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        </table>
      </td>
</tr>
</table>
</asp:Content>

