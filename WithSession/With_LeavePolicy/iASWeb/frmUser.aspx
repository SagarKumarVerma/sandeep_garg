﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="frmUser.aspx.cs" Inherits="frmUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <div>
        <table style="width:100%">
            <tr>
                <td style="padding:5px">
                    <dx:ASPxGridView ID="GrdUser" runat="server" AutoGenerateColumns="False" Width="100%" KeyFieldName="UserID" ClientInstanceName="GrdUser"  >
                        <Settings ShowFilterRow="True" ShowTitlePanel="True"/>
                        <ClientSideEvents CustomButtonClick="function(s, e) {

var obj=GrdUser.GetRowKey(e.visibleIndex)

	window.location = &quot;EditUser.aspx?Value=&quot; + obj;
}"  />
                        <SettingsAdaptivity AdaptivityMode="HideDataCells" />
                        <SettingsPager>
                            <PageSizeItemSettings Visible="true" Items="10, 20, 50,100" />
                        </SettingsPager>
                        <SettingsEditing Mode="PopupEditForm">
                        </SettingsEditing>
                        <Settings ShowFilterRow="True" />
                        <SettingsBehavior ConfirmDelete="True" />
                        <SettingsDataSecurity AllowEdit="False" AllowInsert="False" />
                        <SettingsPopup>
                            <EditForm Modal="True" Width="800px" HorizontalAlign="WindowCenter" 
                PopupAnimationType="Slide" VerticalAlign="WindowCenter" />
                        </SettingsPopup>
                        <SettingsSearchPanel Visible="True" />
                        <SettingsText PopupEditFormCaption="User Manage" Title="User Manage" />
                        <Columns>
                            <dx:GridViewCommandColumn ShowNewButton="true" ShowEditButton="true" VisibleIndex="1" ButtonRenderMode="Image" ShowDeleteButton="True">
                                <CustomButtons >
                                    <dx:GridViewCommandColumnCustomButton ID="Edit"  >
                                        <Image ToolTip="Edit" IconID="actions_edit_16x16devav"/>
                                    </dx:GridViewCommandColumnCustomButton>
                                </CustomButtons>
                            </dx:GridViewCommandColumn>
                            <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowClearFilterButton="True" ShowSelectCheckbox="True" VisibleIndex="0" Caption=" ">
                            </dx:GridViewCommandColumn>
                            <dx:GridViewDataTextColumn FieldName="UserID" VisibleIndex="2" Caption="User Id">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Discription" VisibleIndex="3" Caption="Name">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="paycode" VisibleIndex="4" Caption="PayCode">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="SSN" VisibleIndex="5" Caption="SSN">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="LoginType" VisibleIndex="8" Caption="Login Type">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="auth_comp" VisibleIndex="9" Caption="Authorized Company">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Auth_dept" VisibleIndex="10" Caption="Authorized Department">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Main" VisibleIndex="11" Caption="Main">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="DataProcess" VisibleIndex="12" Caption="Data Process">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Leave" VisibleIndex="13" Caption="Leave Management">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Reports" VisibleIndex="14" Caption="Reports">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="UserType" VisibleIndex="6" Caption="User Type">
                                <PropertiesTextEdit DisplayFormatString="d">
                                </PropertiesTextEdit>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="CompanyCode" VisibleIndex="7" Caption="Company Code">
                                <PropertiesTextEdit DisplayFormatString="d">
                                </PropertiesTextEdit>
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:ASPxGridView>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

