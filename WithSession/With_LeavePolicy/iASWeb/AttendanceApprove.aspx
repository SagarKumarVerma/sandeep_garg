﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="AttendanceApprove.aspx.cs" Inherits="AttendanceApprove" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <table align="center" style="width:100%" cellpadding="0" cellspacing="0" >
    <tr>
        <td ></td>
    </tr>
    <tr>
        <td align="center">
            <table   style="height: 250px" cellpadding="0" cellspacing="0" class="tableCss" width="100%" id="TABLE1">
                <tr>
                    <td colspan="6" style="height:25px;width:850px" align="center" class="tableHeaderCss">
                        <%--<dx:ASPxLabel ID="lblMsg" runat="server" Text="Leave Approval">
                        </dx:ASPxLabel>--%>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="width: 794px; height: 10px"></td>
                </tr>
                <tr>
                    <td align="center" style="width: 100%; height: 22px;">&nbsp; &nbsp;
                              <dx:ASPxButton ID="btnLeaveReport" runat="server" Text="Leave Report"  Visible="False">
                        </dx:ASPxButton>
                        <dx:ASPxButton ID="btnApprove" runat="server" Text="Approve" OnClick="btnApprove_Click" >
                        </dx:ASPxButton>
                        <dx:ASPxButton ID="btnReject" runat="server" Text="Reject" OnClick="btnReject_Click" >
                        </dx:ASPxButton>
                        <dx:ASPxButton ID="btncan" runat="server" Text="Cancel" OnClick="btncan_Click" >
                        </dx:ASPxButton>
                              &nbsp; &nbsp;
                
                </td>
                </tr>
                <tr>
                    <td align="center" style="width: 794px; height: 5px"></td>
                </tr>
                <tr>
                    <td align="center" >
                        <div style="overflow:auto; width:100%; height:450px">
                            <dx:ASPxGridView ID="DataGrid1" runat="server" Width="99%" Theme="SoftOrange" AutoGenerateColumns="False" KeyFieldName="application_no">
                                <SettingsDataSecurity AllowDelete="False" AllowInsert="False" AllowEdit="False">
                                </SettingsDataSecurity>
                                <SettingsPager>
                                    <PageSizeItemSettings Visible="true" Items="10, 20, 50,100" />
                                </SettingsPager>
                                <Columns>
                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Caption=" ">
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn VisibleIndex="4" FieldName="Punch Time" Caption="Punch Time">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Request Date" VisibleIndex="2" FieldName="REQUEST DATE">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Employee Code" VisibleIndex="3" FieldName="paycode">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Request ID" VisibleIndex="1" FieldName="application_no">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Remarks" VisibleIndex="5" FieldName="Remarks">
                                       
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Stage 1 Staus" VisibleIndex="6" FieldName="Stage1_Status">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Stage 2 Status" VisibleIndex="7" FieldName="Stage2_Status">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:ASPxGridView>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</asp:Content>

