﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Text;
using System.IO;
using System.Globalization;
using System.Data.SqlClient;
using System.Web.Caching;
using System.Text.RegularExpressions;
using System.Data.Odbc;
using GlobalSettings;
using System.Net.Mail;


public partial class frmEmailSetting : System.Web.UI.Page
{
    Class_Connection cn = new Class_Connection();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetSettings();
        }
    }
    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        string email = "", smtp = "", port = "", displayname = "", username = "", password = "", sql = "";
        string ssl = "N";
        if (chkssl.Checked == true)
        {
            ssl = "Y";
        }
        if (txtEmail.Text.ToString().Trim() == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Please Enter Email');", true);
            return;
        }

        if (txtSmtp.Text.ToString().Trim() == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Please Enter SMTP');", true);
            return;
        }

        if (txtPort.Text.ToString().Trim() == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Please Enter SMTP Port');", true);
            return;
        }

        if (txtDisplayName.Text.ToString().Trim() == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Please Enter Display Name');", true);
            return;
        }

        if (txtUserName.Text.ToString().Trim() == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Please Enter User Name');", true);
            return;
        }

        if (txtPassword.Text.ToString().Trim() == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Please Enter Password');", true);
            return;
        }

        if (Session["Update"] == null)
        {
            sql = "insert into tblMailSetup (server,sender,port,sendername,username,password,enablessl)values('" + txtSmtp.Text.ToString().Replace("'", "").Trim() + "','" + txtEmail.Text.ToString().Replace("'", "").Trim() + "','" + txtPort.Text.ToString().Replace("'", "").Trim() + "','" + txtDisplayName.Text.ToString().Replace("'", "").Trim() + "','" + txtUserName.Text.ToString().Replace("'", "").Trim() + "','" + txtPassword.Text.ToString().Replace("'", "").Trim() + "','" + ssl + "')";
            cn.execute_NonQuery(sql);
            //if (txtPassword.Text.ToString().Trim() == "")
            //{
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Record Saved Successfully.');", true);

            // }
        }
        else
        {
            sql = "update tblMailSetup set server='" + txtSmtp.Text.ToString().Replace("'", "").Trim() + "',sender='" + txtEmail.Text.ToString().Replace("'", "").Trim() + "',port='" + txtPort.Text.ToString().Replace("'", "").Trim() + "',sendername='" + txtDisplayName.Text.ToString().Replace("'", "").Trim() + "',username='" + txtUserName.Text.ToString().Replace("'", "").Trim() + "',password='" + txtPassword.Text.ToString().Replace("'", "").Trim() + "',enablessl='" + ssl + "'";
            cn.execute_NonQuery(sql);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Record Updated Successfully.');", true);
        }
    }

    protected void ASPxButton2_Click(object sender, EventArgs e)
    {
        if (txtEmailTo.Text.Trim() != "")
        {
            bool result = SendMail.SendEMail(txtEmailTo.Text.Trim(), "This is test mail", "This is test mail");
            if (result == true)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Test E-mail Sent.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Test Mail Failed.');", true);
            }


        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Please Enter To E-mail.');", true);
        }
    }

    public void GetSettings()
    {
        string sql = "select  server,sender,port,sendername,username,password,enablessl from tblMailSetup";
        DataSet ds = new DataSet();
        ds = cn.FillDataSet(sql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            txtEmail.Text = ds.Tables[0].Rows[0]["sender"].ToString();
            txtSmtp.Text = ds.Tables[0].Rows[0]["server"].ToString();
            txtPort.Text = ds.Tables[0].Rows[0]["port"].ToString();
            txtDisplayName.Text = ds.Tables[0].Rows[0]["sendername"].ToString();
            txtUserName.Text = ds.Tables[0].Rows[0]["username"].ToString();
            txtPassword.Text = ds.Tables[0].Rows[0]["password"].ToString();
            if (ds.Tables[0].Rows[0]["enablessl"].ToString() == "Y")
            {
                chkssl.Checked = true;
            }
            Session["Update"] = "Yes";

        }
    }
}