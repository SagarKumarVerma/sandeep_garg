﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="FrmLeaveSummary.aspx.cs" Inherits="FrmLeaveSummary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    
    <table cellpadding="5" style="width: 100%; height: 5px">
        
        <tr>
            <td colspan="6">
                 <dx:ASPxRoundPanel ID="pnlAttendance" runat="server" Width="100%" Height="100%" HeaderText="View Leave Summary" Theme="Default">
        <HeaderStyle VerticalAlign="Middle" />
        <HeaderContent>
            <BackgroundImage HorizontalPosition="left" />
        </HeaderContent>
        <PanelCollection>
            <dx:PanelContent runat="server">
             
                <dx:ASPxFormLayout ID="formLayout" runat="server" AlignItemCaptionsInAllGroups="True" UseDefaultPaddings="False">
                    <SettingsAdaptivity AdaptivityMode="SingleColumnWindowLimit" SwitchToSingleColumnAtWindowInnerWidth="200" />
                    <Items>
                        <dx:EmptyLayoutItem />
                        <dx:LayoutGroup Caption="Employee & Date Selection" ColCount="3" Width="100%">
                            <Items>
                                <dx:LayoutItem Caption="Employee Code">
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                            <dx:ASPxComboBox ID="EmpCombo" runat="server" ValueType="System.String" Width="150px" AutoPostBack="True" SelectedIndex="-1" NullText="NONE" OnSelectedIndexChanged="EmpCombo_SelectedIndexChanged"  >
                                                <ClearButton DisplayMode="Always">
                                                </ClearButton>
                                            </dx:ASPxComboBox>
                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                </dx:LayoutItem>
                                <dx:LayoutItem Caption="Employee Name">
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                            <dx:ASPxLabel ID="LblEmpName" runat="server" Text="Employee Name">
                                            </dx:ASPxLabel>
                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                </dx:LayoutItem>
                                <dx:LayoutItem Caption="Department">
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                            <dx:ASPxLabel ID="lblLocation" runat="server" Text="Department">
                                            </dx:ASPxLabel>
                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                </dx:LayoutItem>
                                <dx:LayoutItem Caption="From Date" >
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer runat="server">
                                            <dx:ASPxDateEdit runat="server" ID="txtfromdate" Width="150px" AutoPostBack="True" >
                                                
                                            </dx:ASPxDateEdit>
                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                </dx:LayoutItem>
                                <dx:LayoutItem Caption="To Date" >
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer runat="server">
                                            <dx:ASPxDateEdit ID="txttodate" runat="server" Width="150px" AutoPostBack="True"  >
                                            </dx:ASPxDateEdit>
                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                </dx:LayoutItem>
                                <dx:LayoutItem Caption="" Width="100px">
                                    <LayoutItemNestedControlCollection>
                                        <dx:LayoutItemNestedControlContainer runat="server" Width="100px">
                                            <dx:ASPxButton ID="btnView" runat="server" Text="View" OnClick="btnView_Click" >
                                            </dx:ASPxButton>
                                        </dx:LayoutItemNestedControlContainer>
                                    </LayoutItemNestedControlCollection>
                                </dx:LayoutItem>
                            </Items>
                        </dx:LayoutGroup>
                    </Items>
                </dx:ASPxFormLayout>
                <dx:ASPxFormLayout ID="ASPxFormLayout1" runat="server" AlignItemCaptionsInAllGroups="True" UseDefaultPaddings="False">
                    <SettingsAdaptivity AdaptivityMode="SingleColumnWindowLimit" SwitchToSingleColumnAtWindowInnerWidth="200" />
                    <Items>
                        <dx:EmptyLayoutItem />
                     
                        <dx:LayoutItem ShowCaption="False" CaptionSettings-HorizontalAlign="Right" Width="100%" HorizontalAlign="Right">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                            <CaptionSettings HorizontalAlign="Right">
                            </CaptionSettings>
                        </dx:LayoutItem>
                        <dx:LayoutItem ShowCaption="False" CaptionSettings-HorizontalAlign="Center" Width="100%" HorizontalAlign="Center">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                    <dx:ASPxGridView ID="grdrequest" runat="server" Theme="SoftOrange" Width="100%" OnPageIndexChanged="grdrequest_PageIndexChanged" >
                                        <SettingsPager Visible="true" PageSize="100">
                                        </SettingsPager>
                                        <SettingsBehavior AllowSort="False" />
                                        <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                                    </dx:ASPxGridView>
                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                            <CaptionSettings HorizontalAlign="Right">
                            </CaptionSettings>
                        </dx:LayoutItem>
                    </Items>
                </dx:ASPxFormLayout>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxRoundPanel>
            </td>
        </tr>
        
    </table>
    
</asp:Content>

