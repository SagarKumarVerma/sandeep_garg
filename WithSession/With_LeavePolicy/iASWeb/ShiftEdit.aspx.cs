﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
public partial class ShiftEdit : System.Web.UI.Page
{
    Class_Connection Con = new Class_Connection();
    string Strsql = null;
    DataSet ds = null;
    int result = 0;
    int mint, hours, lunchmin;
    int mint1, hours1;
    string time = null;
    PayRoll pay = new PayRoll();
    hourToMinute HM = new hourToMinute();
    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";

    public class ShiftData{
        public string SHIFT;
        public string STARTTIME;
        public string ENDTIME;
        public string LUNCHTIME;
        public string LUNCHDURATION;
        public string LUNCHENDTIME;
        public string ORDERINF12;
        public string OTSTARTAFTER;
        public string OTDEDUCTHRS;
        public string LUNCHDEDUCTION;
        public string SHIFTPOSITION;
        public string SHIFTDURATION;
        public string OTDEDUCTAFTER;
        public string ShiftDiscription;
    }
    

    protected void Page_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();
        if (ex is HttpRequestValidationException)
        {
            string resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" +
                   "Form doesn't need to contain valid HTML, just anything with opening and closing angled brackets (<...>).<br />" +
                   btnGoBack +
                   "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        lblError.Text = "";
        if (!Page.IsPostBack)
        {
            if (Session["ShiftVisible"] == null)
            {
                Response.Redirect("Login.aspx");
            }
        }
    }
    //protected void btnSave_Click(object sender, EventArgs e)
    //{
    //    lblError.Text = "";
    //   // CalculateShiftHours();
    //    int lunchduration = 0;
    //    int OTSTARTAFTER = 0;
    //    int OtDeductHrs = 0;
    //    int lunchDeduction = 0;
    //    int shiftDuration = 0;
    //    int OtDeductAfter = 0;

    //    int lunchend = 0;
    //    int shiftend = 0;

    //    if (!string.IsNullOrEmpty(txtLunchDuration.Text.ToString().Trim()))
    //    {
    //        lunchduration = HM.hour(txtLunchDuration.Text.ToString().Trim());
    //    }
    //    if (!string.IsNullOrEmpty(txtOTStarts.Text.ToString().Trim()))
    //    {
    //        OTSTARTAFTER = HM.hour(txtOTStarts.Text.ToString().Trim());
    //    }
    //    if (!string.IsNullOrEmpty(txtOTDeduction.Text.ToString().Trim()))
    //    {
    //        OtDeductHrs = HM.hour(txtOTDeduction.Text.ToString().Trim());
    //    }
    //    if (!string.IsNullOrEmpty(txtLunchDeduction.Text.ToString().Trim()))
    //    {
    //        lunchDeduction = HM.hour(txtLunchDeduction.Text.ToString().Trim());
    //    }
    //    if (!string.IsNullOrEmpty(txtShiftHours.Text.ToString().Trim()))
    //    {
    //        shiftDuration = HM.hour(txtShiftHours.Text.ToString().Trim());
    //    }
    //    if (!string.IsNullOrEmpty(txtOTDeductAfter.Text.ToString().Trim()))
    //    {
    //        OtDeductAfter = HM.hour(txtOTDeductAfter.Text.ToString().Trim());
    //    }
    //    if (!string.IsNullOrEmpty(txtLunchEndTime.Text.ToString().Trim()))
    //    {
    //        lunchend = HM.hour(txtLunchEndTime.Text.ToString().Trim());
    //    }
    //    if (!string.IsNullOrEmpty(txtEndTime.Text.ToString().Trim()))
    //    {
    //        shiftend = HM.hour(txtEndTime.Text.ToString().Trim());
    //    }
    //    if (shiftDuration <= 0)
    //    {
    //        lblError.Text = "Shift Hours must be not equal or less than to 0";
    //        //ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "alert('Shift Hours must be not equal or less than to 0');", true);
    //        txtStartTime.Focus();
    //        //ScriptManager1.SetFocus(txtStartTime);
    //        return;
    //    }
    //    if (lunchend >= shiftend)
    //    {
    //        //ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "alert('Lunch End Time must be less than to Shift End Time');", true);
    //        lblError.Text = "Lunch End Time Must Be Less Than Shift End Time";
    //        txtLunchTime.Focus();
    //        //ScriptManager1.SetFocus(txtLunchTime);
    //        return;
    //    }
    //    //if (Session["IsNewRecord"].ToString() == "Y")
    //    //{
    //        //Select Id_No, Location as 'Machine IP', CommKey, MachineLocation as Location, A_R, In_Out, IsFP from tblmachine order by id_no 
    //        Strsql = "select count(Shift) from tblShiftMaster where shift='" + txtShiftCode.Text.ToString().Trim() + "' ";
    //        result = Con.execute_Scalar(Strsql);
    //        if (result > 0)
    //        {
    //            lblError.Text = "Shift Code Already Exists";
    //            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Shift Code Already Exists');", true);
    //            return;
    //        }
    //        else
    //        {
    //            try
    //            {
    //                Strsql = "insert into tblshiftmaster(shift,STARTTIME,ENDTIME," +
    //                   "LUNCHTIME,LUNCHDURATION,LUNCHENDTIME,OTSTARTAFTER," +
    //                       "OTDEDUCTHRS,LUNCHDEDUCTION,SHIFTPOSITION,SHIFTDURATION,OTDEDUCTAFTER) values " +
    //                       "('" + txtShiftCode.Text.ToString().Trim() + "','" + txtStartTime.Text.ToString().Trim() + "','" + txtEndTime.Text.ToString().Trim() + "','" + txtLunchTime.Text.ToString().Trim() + "', " +
    //                       " '" + lunchduration.ToString() + "','" + txtLunchEndTime.Text.ToString().Trim() + "','" + OTSTARTAFTER.ToString().Trim() + "', " +
    //                       " '" + OtDeductHrs.ToString() + "','" + lunchDeduction.ToString().Trim() + "','" + ddlShift.SelectedItem.Text + "','" + shiftDuration.ToString().Trim() + "', " +
    //                       "'" + OtDeductAfter.ToString() + "')";

    //                result = Con.execute_NonQuery(Strsql);
    //                if (result > 0)
    //                {


    //                    lblError.Text = "Saved Successfully";
    //                    txtStartTime.Text = "00:00";
    //                    txtEndTime.Text = "00:00";
    //                    txtLunchTime.Text = "00:00";
    //                    txtLunchDuration.Text = "00:00";
    //                    txtLunchEndTime.Enabled = false;
    //                    txtShiftHours.Enabled = false;
    //                    txtOTDeductAfter.Text = "00:00";
    //                    txtOTStarts.Text = "00:00";
    //                    txtOTDeduction.Text = "00:00";
    //                    txtLunchDeduction.Text = "00:00";
    //                    txtShiftHours.Text = "00:00";
    //                    txtLunchEndTime.Text = "00:00";
    //                    txtShiftCode.Text = "";
    //                    txtShiftCode.Focus();
    //                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Shift Created Successfully');", true);
    //                    //Response.Redirect("frmShift.aspx");
    //                }
    //            }
    //            catch (Exception ex)
    //            {

    //                lblError.Text = ex.Message.ToString().Trim();
    //                //string output = "Error";
    //                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('" + output + "');", true);
    //                return;
    //            }
    //        }
    //  //  }
    //    //else
    //    //{
    //    //    try
    //    //    {
    //    //        Strsql = "update tblshiftmaster set shift='" + txtShiftCode.Text.ToString().Trim() + "',STARTTIME='" + txtStartTime.Text.ToString().Trim() + "',ENDTIME='" + txtEndTime.Text.ToString().Trim() + "'," +
    //    //            " LUNCHTIME='" + txtLunchTime.Text.ToString().Trim() + "',LUNCHDURATION='" + lunchduration.ToString() + "',LUNCHENDTIME='" + txtLunchEndTime.Text.ToString().Trim() + "',OTSTARTAFTER='" + OTSTARTAFTER.ToString().Trim() + "', " +
    //    //            " OTDEDUCTHRS='" + OtDeductHrs.ToString() + "',LUNCHDEDUCTION='" + lunchDeduction.ToString().Trim() + "',SHIFTPOSITION='" + ddlShift.SelectedItem.Text + "',SHIFTDURATION='" + shiftDuration.ToString().Trim() + "', " +
    //    //            " OTDEDUCTAFTER='" + OtDeductAfter.ToString() + "' where shift='" + txtShiftCode.Text.ToString().Trim() + "' ";

    //    //        result = Con.execute_NonQuery(Strsql);
    //    //        if (result > 0)
    //    //        {
    //    //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Shift Updated Successfully');", true);
    //    //            Response.Redirect("Browse.aspx");
    //    //        }
    //    //    }
    //    //    catch
    //    //    {
    //    //        string output = "Error";
    //    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('" + output + "');", true);
    //    //        return;
    //    //    }
    //    //}
    //}
    //private void CalculateShiftHours()
    //{
    //    try
    //    {
    //        DateTime dt = Convert.ToDateTime(txtLunchTime.Text);
    //        TimeSpan ts = TimeSpan.Parse(txtLunchDuration.Text);
    //        dt = dt.Add(TimeSpan.Parse(ts.ToString()));
    //        txtLunchEndTime.Text = dt.ToString("HH:mm");

    //        DateTime dt1 = Convert.ToDateTime(txtStartTime.Text);
    //        DateTime dt2 = Convert.ToDateTime(txtEndTime.Text);

    //        if (dt1 > dt2)
    //        {
    //            dt2 = dt2.AddDays(1);
    //        }
    //        TimeSpan ts1 = dt2.Subtract(dt1);
    //        TimeSpan ts2 = TimeSpan.Parse(txtLunchDuration.Text);

    //        if (ts1.Hours < 0)
    //        {
    //            hours = ts1.Hours;
    //            mint = ts1.Minutes;
    //            mint = Math.Abs(hours * 60 + mint);

    //            hours1 = ts2.Hours;
    //            mint1 = ts2.Minutes;
    //            mint1 = Math.Abs(hours1 * 60 + mint1);

    //            lunchmin = mint - mint1;

    //            time = pay.M_To_hour(lunchmin.ToString());
    //            txtShiftHours.Text = time.ToString();
    //        }
    //        else
    //        {
    //            ts1 = ts1.Subtract(ts2);
    //            txtShiftHours.Text = ts1.Hours.ToString("00") + ":" + ts1.Minutes.ToString("00");
    //        }
    //        txtOTDeductAfter.Focus();
    //    }
    //    catch
    //    {
    //    }
    //}
    //protected void txtStartTime_TextChanged(object sender, EventArgs e)
    //{
    //    CalculateShiftHours();
    //    txtEndTime.Focus();
    //}
    //protected void txtEndTime_TextChanged(object sender, EventArgs e)
    //{
    //    CalculateShiftHours();
    //    txtLunchTime.Focus();
    //}
    //protected void txtLunchTime_TextChanged(object sender, EventArgs e)
    //{
    //    CalculateShiftHours();
    //    txtLunchDuration.Focus();
    //}
    internal static class SimulateVal
    {
        internal static double Val(string expression)
        {
            if (expression == null)
                return 0;

            //try the entire string, then progressively smaller
            //substrings to simulate the behavior of VB's 'Val',
            //which ignores trailing characters after a recognizable value:
            for (int size = expression.Length; size > 0; size--)
            {
                double testDouble;
                if (double.TryParse(expression.Substring(0, size), out testDouble))
                    return testDouble;
            }

            //no value is recognized, so return 0:
            return 0;
        }
        internal static double Val(object expression)
        {
            if (expression == null)
                return 0;

            double testDouble;
            if (double.TryParse(expression.ToString(), out testDouble))
                return testDouble;

            //VB's 'Val' function returns -1 for 'true':
            bool testBool;
            if (bool.TryParse(expression.ToString(), out testBool))
                return testBool ? -1 : 0;

            //VB's 'Val' function returns the day of the month for dates:
            System.DateTime testDate;
            if (System.DateTime.TryParse(expression.ToString(), out testDate))
                return testDate.Day;

            //no value is recognized, so return 0:
            return 0;
        }
        internal static int Val(char expression)
        {
            int testInt;
            if (int.TryParse(expression.ToString(), out testInt))
                return testInt;
            else
                return 0;
        }
    }

    [System.Web.Services.WebMethod()]
    [System.Web.Script.Services.ScriptMethod()]
    public static string SaveShift(Boolean IsUpdate, string ShiftCode, string SSftStr, string ESftStr, string SSHIFTDURATION, string SLUNCHDURATION, string SLStr, string ELStr, string SOTSTARTAFTER, string SOTDEDUCTAFTER, string SOTDEDUCTHRS, string SLUNCHDEDUCTION, string SHIFTPOSITION, string Details, string LoginCompany)
    {
        string sSql ;
        SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString1"].ToString());
        SqlCommand Cmd;

        if (IsUpdate)
        {
            try
            {
               string x= DataFilter.AuthComp;
               sSql = "update tblshiftmaster set  ShiftDiscription='"+Details.Trim()+"',  shift='" + ShiftCode + "',STARTTIME='" + SSftStr + "',ENDTIME='" + ESftStr + "'," +
                    " LUNCHTIME='" + SLStr + "',LUNCHDURATION='" + SLUNCHDURATION + "',LUNCHENDTIME='" + ELStr + "',OTSTARTAFTER='" + SOTSTARTAFTER + "', " +
                    " OTDEDUCTHRS='" + SOTDEDUCTHRS + "',LUNCHDEDUCTION='" + SLUNCHDEDUCTION + "',SHIFTPOSITION='" + SHIFTPOSITION + "',SHIFTDURATION='" + SSHIFTDURATION + "', " +
                    " OTDEDUCTAFTER='" + SOTDEDUCTAFTER + "' where shift='" + ShiftCode + "' and CompanyCode='" + LoginCompany + "' ";
                if (Conn.State != ConnectionState.Open)
                    Conn.Open();
                Cmd = new SqlCommand(sSql,Conn);
                int result = Cmd.ExecuteNonQuery();
                if (Conn.State != ConnectionState.Closed)
                    Conn.Close();
                if (result > 0)
                {                    
                   return "true";
                }
            }
            catch (Exception ex)
            {
                if (Conn.State != ConnectionState.Closed)
                    Conn.Close();
                return ex.Message;
            }
        }
        else
        {
            //insert
            try
            {

                if (ShiftCode.Length!=3)
                {
                    return "*Input 3 Digit Shift Code";
                }

                SqlDataAdapter adap = new SqlDataAdapter();
                DataSet ds = new DataSet();
                sSql = "select Shift from tblShiftMaster where shift='" + ShiftCode + "' and CompanyCode='" + LoginCompany + "' ";
                adap = new SqlDataAdapter(sSql, Conn);
                adap.Fill(ds);
                int result =ds.Tables[0].Rows.Count;
                if (result > 0)
                {
                    return "*Shift Code Already Exists";
                    
                }
                sSql = "select ShiftDiscription from tblShiftMaster where ShiftDiscription='" + Details + "' and CompanyCode='" + LoginCompany + "' ";
                adap = new SqlDataAdapter(sSql, Conn);
                adap.Fill(ds);
                result = ds.Tables[0].Rows.Count;
                if (result > 0)
                {
                    return "*Shift Details Already Exists";

                }

                sSql =  "insert into tblshiftmaster(shift,STARTTIME,ENDTIME," +
                       "LUNCHTIME,LUNCHDURATION,LUNCHENDTIME,OTSTARTAFTER," +
                           "OTDEDUCTHRS,LUNCHDEDUCTION,SHIFTPOSITION,SHIFTDURATION,OTDEDUCTAFTER,CompanyCode,ShiftDiscription) values " +
                           "('" + ShiftCode + "','" + SSftStr + "','" + ESftStr + "','" + SLStr + "', " +
                           " '" + SLUNCHDURATION + "','" + ELStr + "','" + SOTSTARTAFTER + "', " +
                           " '" + SOTDEDUCTHRS + "','" + SLUNCHDEDUCTION + "','" + SHIFTPOSITION + "','" + SSHIFTDURATION + "', " +
                           "'" + SOTDEDUCTAFTER + "','" + LoginCompany + "','" + Details.Trim() + "')";
                if (Conn.State != ConnectionState.Open)
                    Conn.Open();
                Cmd = new SqlCommand(sSql, Conn);
                result = Cmd.ExecuteNonQuery();
                if (Conn.State != ConnectionState.Closed)
                    Conn.Close();
                if (result > 0)
                {
                    return "true";
                }
            }
            catch (Exception ex)
            {
                if (Conn.State != ConnectionState.Closed)
                    Conn.Close();
                return ex.Message;
            }
        }
        return "true";
    }
    
     [System.Web.Services.WebMethod()]
    [System.Web.Script.Services.ScriptMethod()]
    public static string LoadShift(string ShiftCode, string LoginCompany)
    {
        string ShiftData;
        SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString1"].ToString());
        SqlDataAdapter adap = new SqlDataAdapter();
        DataSet ds = new DataSet();
        string sSql = "select * from tblShiftMaster where shift='" + ShiftCode + "'  and CompanyCode='" + LoginCompany + "' ";
        adap = new SqlDataAdapter(sSql, Conn);
        adap.Fill(ds);
        ShiftData LS = new ShiftData();
         LS.SHIFT = ShiftCode;
         LS.STARTTIME = Convert.ToDateTime(ds.Tables[0].Rows[0]["STARTTIME"].ToString().Trim()).ToString("yyyy-MM-dd HH:mm:ss");
         LS.ENDTIME = Convert.ToDateTime(ds.Tables[0].Rows[0]["ENDTIME"].ToString().Trim()).ToString("yyyy-MM-dd HH:mm:ss");
         LS.LUNCHTIME =Convert.ToDateTime( ds.Tables[0].Rows[0]["LUNCHTIME"].ToString().Trim()).ToString("yyyy-MM-dd HH:mm:ss");
         LS.LUNCHDURATION = ds.Tables[0].Rows[0]["LUNCHDURATION"].ToString().Trim();
         LS.LUNCHENDTIME = Convert.ToDateTime(ds.Tables[0].Rows[0]["LUNCHENDTIME"].ToString().Trim()).ToString("yyyy-MM-dd HH:mm:ss");
         LS.ORDERINF12 = ds.Tables[0].Rows[0]["ORDERINF12"].ToString().Trim();
         LS.OTSTARTAFTER = ds.Tables[0].Rows[0]["OTSTARTAFTER"].ToString().Trim();
         LS.OTDEDUCTHRS = ds.Tables[0].Rows[0]["OTDEDUCTHRS"].ToString().Trim();
         LS.LUNCHDEDUCTION = ds.Tables[0].Rows[0]["LUNCHDEDUCTION"].ToString().Trim();
         LS.SHIFTPOSITION = ds.Tables[0].Rows[0]["SHIFTPOSITION"].ToString().Trim();
         LS.SHIFTDURATION = ds.Tables[0].Rows[0]["SHIFTDURATION"].ToString().Trim();
         LS.OTDEDUCTAFTER = ds.Tables[0].Rows[0]["OTDEDUCTAFTER"].ToString().Trim();
         LS.ShiftDiscription = ds.Tables[0].Rows[0]["ShiftDiscription"].ToString().Trim();
         ShiftData = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(LS);
         return ShiftData ;
    }
}