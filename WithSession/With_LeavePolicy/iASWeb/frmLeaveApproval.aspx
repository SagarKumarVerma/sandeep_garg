﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="frmLeaveApproval.aspx.cs" Inherits="frmLeaveApproval" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
      <table align="center" style="width:100%" cellpadding="0" cellspacing="0" >
    <tr>
        <td ></td>
    </tr>
    <tr>
        <td align="center">
            <table   style="height: 250px" cellpadding="0" cellspacing="0" class="tableCss" width="100%" id="TABLE1">
                <tr>
                    <td colspan="6" style="height:25px;width:850px" align="center" class="tableHeaderCss">
                        <dx:ASPxLabel ID="lblMsg" runat="server" Text="Leave Approval">
                        </dx:ASPxLabel>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="width: 794px; height: 10px"></td>
                </tr>
                <tr>
                    <td align="center" style="width: 100%; height: 22px;">&nbsp; &nbsp;
                              <dx:ASPxButton ID="btnLeaveReport" runat="server" Text="Leave Report" OnClick="btnLeaveReport_Click">
                        </dx:ASPxButton>
                        <dx:ASPxButton ID="btnApprove" runat="server" Text="Approve" OnClick="btnApprove_Click">
                        </dx:ASPxButton>
                        <dx:ASPxButton ID="btnReject" runat="server" Text="Reject" OnClick="btnReject_Click">
                        </dx:ASPxButton>
                        <dx:ASPxButton ID="btncan" runat="server" Text="Cancel" OnClick="btncan_Click">
                        </dx:ASPxButton>
                              &nbsp; &nbsp;
                
                </td>
                </tr>
                <tr>
                    <td align="center" style="width: 794px; height: 5px"></td>
                </tr>
                <tr>
                    <td align="center" style="width: 794px; height: 5px">
                        <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Remarks:"></dx:ASPxLabel>
                        <dx:ASPxMemo ID="appRemakrs" runat="server" Height="71px" Width="450px"></dx:ASPxMemo>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="width: 794px; height: 5px"></td>
                </tr>
                <tr>
                    <td align="center" >
                        <div style="overflow:auto; width:100%; height:450px">
                            <dx:ASPxGridView ID="DataGrid1" runat="server" Width="99%" Theme="SoftOrange" AutoGenerateColumns="False" KeyFieldName="application_no">
                                <SettingsDataSecurity AllowDelete="False" AllowInsert="False" AllowEdit="False">
                                </SettingsDataSecurity>
                                <SettingsPager>
                                    <PageSizeItemSettings Visible="true" Items="10, 20, 50,100" />
                                </SettingsPager>
                                <Columns>
                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Caption=" ">
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn Caption="Stage 2 Status" FieldName="Stage2_approved" VisibleIndex="13">
                                       
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Stage 1 Status" FieldName="Stage1_approved" VisibleIndex="11">
                                       
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="User Remarks" VisibleIndex="10" FieldName="userremarks">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Days" VisibleIndex="9" FieldName="leavedays">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Duration" VisibleIndex="8" FieldName="Duration">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Date To" VisibleIndex="7" FieldName="leave_to">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Date From" VisibleIndex="6" FieldName="leave_from">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Leave Code" VisibleIndex="5" FieldName="LeaveCode">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Employee Name" VisibleIndex="4" FieldName="EmpName">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Application Date" VisibleIndex="2" FieldName="RequestDate">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Employee Code" VisibleIndex="3" FieldName="paycode">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Application No." VisibleIndex="1" FieldName="application_no">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Visible="False" VisibleIndex="15" FieldName="D">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataMemoColumn Caption="Stage 1 Approver  Remarks" VisibleIndex="12" FieldName="Stage1_approval_Remarks">
                                    </dx:GridViewDataMemoColumn>
                                     <dx:GridViewDataMemoColumn Caption="Stage 2 Approver  Remarks" VisibleIndex="14" FieldName="Stage2_Approval_Remarks">
                                    </dx:GridViewDataMemoColumn>
                                </Columns>
                            </dx:ASPxGridView>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</asp:Content>

