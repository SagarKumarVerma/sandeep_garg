﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="frmCategory.aspx.cs" Inherits="frmCategory" %>

 

<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">  
        function State_OnKeyUp(s, e) {  
            s.SetText(s.GetText().toUpperCase().trim());
        }  
    </script>  
    <script language="javascript" type="text/javascript" src="JS/validation_Employee.js"></script>

    <%-- DXCOMMENT: Configure ASPxGridView control --%>

    <dx:ASPxGridView ID="GrdCat" runat="server" AutoGenerateColumns="False"
        Width="100%" KeyFieldName="CAT" OnCellEditorInitialize="GrdCat_CellEditorInitialize" OnRowValidating="GrdCat_RowValidating" OnPageIndexChanged="GrdCat_PageIndexChanged" OnRowDeleting="GrdCat_RowDeleting" OnRowInserting="GrdCat_RowInserting" OnRowUpdating="GrdCat_RowUpdating" OnInitNewRow="GrdCat_InitNewRow" OnRowDeleted="GrdCat_RowDeleted" OnStartRowEditing="GrdCat_StartRowEditing">
        <%-- DXCOMMENT: Configure ASPxGridView's columns in accordance with datasource fields --%>
        
				 <ClientSideEvents EndCallback="function(s, e) {
	 if (s.cp_isSuccess) 
{ alert('Can Not Delete Record As It Is Already Assigned To Employee'); 
delete s.isSuccess; } 
}" />
        
        <SettingsAdaptivity AdaptivityMode="HideDataCells" />

        <SettingsPager>
            <PageSizeItemSettings Visible="true" Items="10, 20, 50,100" />
        </SettingsPager>
        <SettingsEditing Mode="PopupEditForm" EditFormColumnCount="1">
        </SettingsEditing>
        <Settings ShowFilterRow="True" ShowTitlePanel="True" />
        <SettingsBehavior ConfirmDelete="True" />
        <SettingsCommandButton>
            <ClearFilterButton Text=" ">
                <Image IconID="filter_clearfilter_16x16">
                </Image>
            </ClearFilterButton>
            <NewButton Text=" ">
                <Image IconID="actions_add_16x16">
                </Image>
            </NewButton>
            <UpdateButton ButtonType="Image" RenderMode="Image">
                <Image IconID="save_save_16x16office2013">
                </Image>
            </UpdateButton>
            <CancelButton ButtonType="Image" RenderMode="Image">
                <Image IconID="actions_cancel_16x16office2013">
                </Image>
            </CancelButton>
            <EditButton Text=" ">
                <Image IconID="actions_edit_16x16devav">
                </Image>
            </EditButton>
            <DeleteButton Text=" ">
                <Image IconID="actions_trash_16x16">
                </Image>
            </DeleteButton>
        </SettingsCommandButton>
        <SettingsPopup>
            <EditForm Modal="True" Width="500px" HorizontalAlign="WindowCenter"
                PopupAnimationType="Slide" VerticalAlign="WindowCenter" />
        </SettingsPopup>
        <SettingsSearchPanel Visible="True" />
        <SettingsText PopupEditFormCaption="Category" Title="Category" />
        <Columns>
            <dx:GridViewCommandColumn ShowClearFilterButton="True" ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0" Width="40px">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="CAT" VisibleIndex="1" Caption="Code">
                <PropertiesTextEdit MaxLength="3">
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="CATAGORYNAME" VisibleIndex="2" Caption="Name">
                <PropertiesTextEdit MaxLength="100">
                   
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
           <%-- <dx:GridViewDataCheckColumn FieldName="LateVerification" Caption="Is LateType" VisibleIndex="3">
                <PropertiesCheckEdit DisplayTextChecked="Y" DisplayTextGrayed="N" DisplayTextUnchecked="N" DisplayTextUndefined="N" ValueChecked="Y" ValueGrayed="N" ValueType="System.String" ValueUnchecked="N" ConvertEmptyStringToNull="False">
                </PropertiesCheckEdit>
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataCheckColumn FieldName="EveryInterval" Caption="Every Interval" VisibleIndex="4">
                <PropertiesCheckEdit DisplayTextChecked="Y" DisplayTextGrayed="N" DisplayTextUnchecked="N" DisplayTextUndefined="N" ValueChecked="Y" ValueGrayed="N" ValueType="System.String" ValueUnchecked="N" ConvertEmptyStringToNull="False">
                </PropertiesCheckEdit>
            </dx:GridViewDataCheckColumn>

            <dx:GridViewDataCheckColumn FieldName="DeductFrom" Caption="Deduct From Leave" VisibleIndex="5">
                <PropertiesCheckEdit DisplayTextChecked="Y" DisplayTextGrayed="N" DisplayTextUnchecked="N" DisplayTextUndefined="N" ValueChecked="Y" ValueGrayed="N" ValueType="System.String" ValueUnchecked="N" ConvertEmptyStringToNull="False">
                </PropertiesCheckEdit>
            </dx:GridViewDataCheckColumn>
            <dx:GridViewDataComboBoxColumn Caption="1st Leave" FieldName="FromLeave" ShowInCustomizationForm="True" VisibleIndex="9">
                <PropertiesComboBox DataSourceID="SqlDataSource1" TextField="LEAVECODE" ValueField="LEAVEFIELD">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn Caption="2nd Leave" FieldName="FromLeave1" ShowInCustomizationForm="True" VisibleIndex="10">
                <PropertiesComboBox DataSourceID="SqlDataSource1" TextField="LEAVECODE" ValueField="LEAVEFIELD">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataTextColumn FieldName="LateDays" VisibleIndex="6" Caption="Late Days">

                <PropertiesTextEdit>
                    <MaskSettings Mask="&lt;0..31&gt;" ShowHints="True" />
                    <ValidationSettings>
                        <RegularExpression ErrorText="" ValidationExpression="\d+" />
                    </ValidationSettings>
                </PropertiesTextEdit>

            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="DeductDay" VisibleIndex="7" Caption="Deduct Days">

                <PropertiesTextEdit>
                    <MaskSettings Mask="00.00" />
                </PropertiesTextEdit>

            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="MaxLateDur" VisibleIndex="8" Caption="Max Late Duration (In Minutes)">

                <PropertiesTextEdit>
                    <MaskSettings Mask="&lt;0..600&gt;" ShowHints="True" />
                    <ValidationSettings>
                        <RegularExpression ErrorText="" ValidationExpression="\d+" />
                    </ValidationSettings>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>--%>
        </Columns>
        <Paddings Padding="0px" />
        <Border BorderWidth="0px" />
        <BorderBottom BorderWidth="1px" />
    </dx:ASPxGridView>

<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:TimeWatchConnectionString %>" SelectCommand="SELECT [LEAVEFIELD], [LEAVECODE] FROM [tblLeaveMaster] ORDER BY 2 "></asp:SqlDataSource>
<%-- DXCOMMENT: Configure your datasource for ASPxGridView --%>



</asp:Content>
