﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="EmployeeGroupEdit.aspx.cs" Inherits="EmployeeGroupEdit" %>
<%@ Import Namespace="System.Threading" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <script language="javascript" type="text/javascript" src="JS/validation_Employee.js"></script>
     <div align="center">
         <table align="center" style="width: 1000px" cellpadding="0" cellspacing="0">
             <tr>
                 <td style="height: 25px"></td>
             </tr>
             <tr>
                 <td align="center">
                     <table cellpadding="0" cellspacing="0" style="text-align:left;width:850px" class="tableCss">
                         <tr>
                             <td align="center" class="tableHeaderCss" style="width:25px;width:850px">
                                 <asp:Label ID="lblMsg" runat="server" CssClass="lblCss" Text="Employee"  ></asp:Label>
                            &nbsp;Group</td>
                         </tr>
                         <tr>
                             <td style="height:5px"></td>
                         </tr>
                         <tr>
                             <td style="height:5px">
                                 <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="1" EnableTheming="True" Theme="SoftOrange" Width="100%">
                                     <TabPages>
                                         <dx:TabPage Name="Gen" Text="General Setup">
                                             <ActiveTabStyle Border-BorderColor="#666666" Border-BorderStyle="Solid">
                                             </ActiveTabStyle>
                                             <ContentCollection>
                                                 <dx:ContentControl runat="server">
                                                     <table align="center" class="dxflInternalEditorTable" width="100%">
                                                         <tr>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Group ID*" Theme="SoftOrange">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxTextBox ID="txtGroupID" runat="server" MaxLength="3" Width="70px">
                                                                     <ValidationSettings ValidationGroup="MK">
                                                                         <RequiredField IsRequired="True" />
                                                                     </ValidationSettings>
                                                                 </dx:ASPxTextBox>
                                                             </td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Group Name*">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxTextBox ID="txtGroupname" runat="server" MaxLength="20" Width="170px">
                                                                     <ValidationSettings ValidationGroup="MK">
                                                                         <RequiredField IsRequired="True" />
                                                                     </ValidationSettings>
                                                                 </dx:ASPxTextBox>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="lblEmpCode" runat="server" Theme="SoftOrange" Text="Permissible Late Arrival">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxTextBox ID="txtLateArrival" runat="server" Width="70px" NullText="00:00" Height="26px">
                                                                     <MaskSettings Mask="HH:mm" />
                                                                 </dx:ASPxTextBox>
                                                             </td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel25" runat="server" Text="Punches Req In A Day" Theme="SoftOrange">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px" rowspan="5">
                                                                 <dx:ASPxRadioButtonList ID="radPunch" runat="server" ClientInstanceName="radPunch" Width="180px" OnSelectedIndexChanged="radPunch_SelectedIndexChanged">
                                                                     <Items>
                                                                         <dx:ListEditItem Text="No Punch" Value="NoPunch"  />
                                                                         <dx:ListEditItem Text="Single Punch Only" Value="SinglePunchOnly" />
                                                                         <dx:ListEditItem Text="Two Punches" Value="TwoPunch" />
                                                                         <dx:ListEditItem Text="Four Punches" Value="FourPunch" Selected="true" />
                                                                         <dx:ListEditItem Text="Multiple Punches" Value="MultiplePunch" />
                                                                     </Items>
                                                                 </dx:ASPxRadioButtonList>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="lblCardNo" runat="server" Theme="SoftOrange" Text="Permissible Early Departure">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxTextBox ID="txtEarlyDept" runat="server" Width="70px" MaxLength="5" onkeypress="fncInputNumericValuesOnly('txtCardNo')" NullText="00:00">
                                                                     <MaskSettings Mask="HH:mm" />
                                                                 </dx:ASPxTextBox>
                                                             </td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="lblName" runat="server" Text="Max Working Hour In A Day" Theme="SoftOrange">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxTextBox ID="txtMaxWorkHours" runat="server" Width="70px" MaxLength="5" NullText="00:00">
                                                                     <MaskSettings Mask="HH:mm" />
                                                                 </dx:ASPxTextBox>
                                                             </td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ISRTC" runat="server" Theme="SoftOrange" Text="Is Round The Clock Work">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxCheckBox ID="chkRoundtheClock" runat="server" CheckState="Unchecked" Theme="SoftOrange">
                                                                 </dx:ASPxCheckBox>
                                                             </td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px; height: 35px;">
                                                                 <dx:ASPxLabel ID="lblCompany" runat="server" Theme="SoftOrange" Text="Consider Time Loss">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px; height: 35px;">
                                                                 <dx:ASPxCheckBox ID="chkConsiderTmeloss" runat="server" CheckState="Unchecked" Theme="SoftOrange">
                                                                 </dx:ASPxCheckBox>
                                                             </td>
                                                             <td style="padding: 5px; height: 35px;"></td>
                                                             <td style="padding: 5px; height: 35px;"></td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="lblDepartment" runat="server" Theme="SoftOrange" Text="Is Half Day">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxCheckBox ID="chkHalfdayMarking" runat="server" CheckState="Unchecked" Theme="SoftOrange">
                                                                 </dx:ASPxCheckBox>
                                                             </td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="Single Punch Option" Theme="SoftOrange">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="lblCatagory" runat="server" Text="Is Sort Leave" Theme="SoftOrange">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxCheckBox ID="chkShortMarking" runat="server" CheckState="Unchecked" Theme="SoftOrange">
                                                                 </dx:ASPxCheckBox>
                                                             </td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px" rowspan="2">
                                                                 <dx:ASPxRadioButtonList ID="chkSinglepunchonly" runat="server" ClientInstanceName="RadPunch" Width="180px" OnSelectedIndexChanged="chkSinglepunchonly_SelectedIndexChanged">
                                                                     <Items>
                                                                         <dx:ListEditItem Text="Fixed Out Time" Value="Fixed Out Time" />
                                                                         <dx:ListEditItem Text="Overwrite" Value="Overwrite" />
                                                                     </Items>
                                                                 </dx:ASPxRadioButtonList>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="lblSection" runat="server" Theme="SoftOrange" Text="Present Marking Duration">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxTextBox ID="txtPresentMarkingDuration" runat="server" MaxLength="5" NullText="00:00" Width="70px">
                                                                     <MaskSettings Mask="HH:mm" />
                                                                 </dx:ASPxTextBox>
                                                             </td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="lblGrade" runat="server" Theme="SoftOrange" Text="Max Working For Half Day">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxTextBox ID="txtMaxWorkHours_halfDay" runat="server" MaxLength="5" NullText="00:00" Width="70px">
                                                                     <MaskSettings Mask="HH:mm" />
                                                                 </dx:ASPxTextBox>
                                                             </td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="Is Over Stay" Theme="SoftOrange">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxCheckBox ID="chkOverStay" runat="server" CheckState="Unchecked" Theme="SoftOrange">
                                                                 </dx:ASPxCheckBox>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="lblEmployeeGroup" runat="server" Theme="SoftOrange" Text="Max Working For Sort Day">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxTextBox ID="MaxWorkHoursForshort" runat="server" MaxLength="5" NullText="00:00" Width="70px">
                                                                     <MaskSettings Mask="HH:mm" />
                                                                 </dx:ASPxTextBox>
                                                             </td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="lblEmployeeGroup0" runat="server" Text="Mark Half Day After " Theme="SoftOrange">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxTextBox ID="txtHLFAfter" runat="server" MaxLength="5" NullText="00:00" Width="70px">
                                                                     <MaskSettings Mask="HH:mm" />
                                                                 </dx:ASPxTextBox>
                                                             </td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="lblEmployeeGroup1" runat="server" Text="Mark Half Day Before" Theme="SoftOrange">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxTextBox ID="txtHLFBefore" runat="server" MaxLength="5" NullText="00:00" Width="70px">
                                                                     <MaskSettings Mask="HH:mm" />
                                                                 </dx:ASPxTextBox>
                                                             </td>
                                                         </tr>
                                                     </table>
                                                 </dx:ContentControl>
                                             </ContentCollection>
                                         </dx:TabPage>
                                         <dx:TabPage Name="PD" Text="Shift Week Off Policy">
                                             <ActiveTabStyle Border-BorderColor="#666666" Border-BorderStyle="Solid">
                                             </ActiveTabStyle>
                                             <ContentCollection>
                                                 <dx:ContentControl runat="server">
                                                     <table align="center" class="dxflInternalEditorTable" width="100%">
                                                         <tr>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Shift Type" Theme="SoftOrange">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxComboBox ID="ddlShiftType" runat="server" Theme="SoftOrange" AutoPostBack="true" OnSelectedIndexChanged="ddlShiftType_SelectedIndexChanged">
                                                                     <Items>
                                                                         <dx:ListEditItem Text="Fixed" Value="F" Selected="true" />
                                                                         <dx:ListEditItem Text="Rotational" Value="R" />
                                                                         <dx:ListEditItem Text="Ignore" Value="I" />
                                                                     </Items>
                                                                 </dx:ASPxComboBox>
                                                             </td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel30" runat="server" Text="First Off Day" Theme="SoftOrange">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxComboBox ID="ddlFirstWeeklyOff" runat="server" Theme="SoftOrange" AutoPostBack="True">
                                                                     <Items>
                                                                         <dx:ListEditItem Text="NONE" Value="NON" />
                                                                         <dx:ListEditItem Text="Sunday" Value="SUN" Selected="True" />
                                                                         <dx:ListEditItem Text="Monday" Value="MON" />
                                                                         <dx:ListEditItem Text="Tuesday" Value="TUE" />
                                                                         <dx:ListEditItem Text="Wednesday" Value="WED" />
                                                                         <dx:ListEditItem Text="Thursday" Value="THU" />
                                                                         <dx:ListEditItem Text="Friday" Value="FRI" />
                                                                         <dx:ListEditItem Text="Saturday" Value="SAT" />
                                                                     </Items>
                                                                 </dx:ASPxComboBox>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel5" runat="server" Theme="SoftOrange" Text="Shift">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                               <%--  <dx:ASPxComboBox ID="ddlShift" runat="server" Theme="SoftOrange" ValueField="shiftcode">
                                                                     
                                                                 </dx:ASPxComboBox>--%>
                                                                     <dx:ASPxGridLookup ID="ddlShift" runat="server" KeyFieldName="shiftcode" AutoGenerateColumns="False" MultiTextSeparator="-">
                                                                    <GridViewProperties>
                                                                        <SettingsBehavior AllowFocusedRow="True" AllowSelectSingleRowOnly="True" />
                                                                        <Settings GroupFormat="{0}-{1}" ShowFilterRow="True" />
                                                                    </GridViewProperties>
                                                                    <Columns>
                                                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="shiftcode" ShowInCustomizationForm="True" VisibleIndex="0">
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="Name" FieldName="shiftname" ShowInCustomizationForm="True" VisibleIndex="1">
                                                                        </dx:GridViewDataTextColumn>
                                                                    </Columns>
                                                                </dx:ASPxGridLookup>
                                                             </td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel31" runat="server" Text="Second Off Day" Theme="SoftOrange">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px" >
                                                                 <dx:ASPxComboBox ID="ddlSecondWeeklyOff" runat="server"  Theme="SoftOrange" OnSelectedIndexChanged="ddlSecondWeeklyOff_SelectedIndexChanged" AutoPostBack="True">
                                                                     <Items>
                                                                         <dx:ListEditItem Text="NONE" Value="NON" Selected="True"/>
                                                                         <dx:ListEditItem  Text="Sunday" Value="SUN" />
                                                                         <dx:ListEditItem Text="Monday" Value="MON" />
                                                                         <dx:ListEditItem Text="Tuesday" Value="TUE" />
                                                                         <dx:ListEditItem Text="Wednesday" Value="WED" />
                                                                         <dx:ListEditItem Text="Thursday" Value="THU" />
                                                                         <dx:ListEditItem Text="Friday" Value="FRI" />
                                                                         <dx:ListEditItem Text="Saturday" Value="SAT" />
                                                                     </Items>
                                                                 </dx:ASPxComboBox>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel8" runat="server" Theme="SoftOrange" Text="Shift Pattern">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxComboBox ID="ddlAddShiftPattern" runat="server" Theme="SoftOrange">
                                                                     <Items>
                                                                         <dx:ListEditItem Text="Fixed" Value="F" />
                                                                         <dx:ListEditItem Text="Rotational" Value="R" />
                                                                         <dx:ListEditItem Text="Ignore" Value="I" />
                                                                     </Items>
                                                                 </dx:ASPxComboBox>
                                                                <%--  <dx:ASPxGridLookup ID="ddlAddShiftPattern" runat="server" SelectionMode="Multiple"  ClientInstanceName="ddlAddShiftPattern"
                                                                            KeyFieldName="shiftcode"  TextFormatString="{0}" MultiTextSeparator=",">
                                                                            <Columns>
                                                                                <dx:GridViewCommandColumn ShowSelectCheckbox="True" Caption="" Width="10px" />
                                                                                <dx:GridViewDataColumn FieldName="shiftcode" Caption="Code" Width="50px" />
                                                                                <dx:GridViewDataColumn FieldName="shiftname" Settings-AllowAutoFilter="False" Caption="Name"/>
                                                                            </Columns>
                                                                            <GridViewProperties>
                                                                                <Templates>
                                                                                    <StatusBar>
                                                                                        <table class="OptionsTable" style="float: right">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <dx:ASPxButton ID="Close" runat="server" AutoPostBack="false" Text="Close" ClientSideEvents-Click="ddlAddShiftPattern" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </StatusBar>
                                                                                </Templates>
                                                                                <Settings ShowFilterRow="True" ShowStatusBar="Visible" />
                                                                                <SettingsPager PageSize="7" EnableAdaptivity="true" />
                                                                            </GridViewProperties>
                                                                        </dx:ASPxGridLookup>--%>
                                                                 <asp:ImageButton ID="cmdAddShift" runat="server" ImageUrl="~/Images/ADD.bmp" OnClick="cmdAddShift_Click" />
                                                             </td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel32" runat="server" Text="Second Off Type" Theme="SoftOrange">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxComboBox ID="ddlSecondWeeklyOffType" runat="server" SelectedIndex="0" Theme="SoftOrange" AutoPostBack="True" OnSelectedIndexChanged="ddlSecondWeeklyOffType_SelectedIndexChanged">
                                                                     <Items>
                                                                         <dx:ListEditItem Selected="True" Text="Full" Value="F" />
                                                                         <dx:ListEditItem Text="Half" Value="H" />
                                                                     </Items>
                                                                 </dx:ASPxComboBox>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="lblShiftPattern" runat="server" Theme="SoftOrange">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel33" runat="server" Text="Half Day Shift" Theme="SoftOrange">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxComboBox ID="ddlHalfDayShift" runat="server" Theme="SoftOrange" Enabled="False">
                                                                     <Items>
                                                                         <dx:ListEditItem Text="Fixed" Value="F"  />
                                                                         <dx:ListEditItem Text="Rotational" Value="R" />
                                                                         <dx:ListEditItem Text="Ignore" Value="I" />
                                                                     </Items>
                                                                 </dx:ASPxComboBox>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel10" runat="server" Theme="SoftOrange" Text="Remove">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxComboBox ID="ddlRemoveShiftPattern" runat="server" Theme="SoftOrange">
                                                                     <Items>
                                                                         <dx:ListEditItem Text="Fixed" Value="F" />
                                                                         <dx:ListEditItem Text="Rotational" Value="R" />
                                                                         <dx:ListEditItem Text="Ignore" Value="I" />
                                                                     </Items>
                                                                 </dx:ASPxComboBox>
                                                                 <asp:ImageButton ID="cmdRemovePattern" runat="server" ImageUrl="~/Images/ADD.bmp" OnClick="cmdRemovePattern_Click" />
                                                             </td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel34" runat="server" Text="Second Week Days  " Theme="SoftOrange">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px" >
                                                                 <dx:ASPxCheckBox ID="chk1" runat="server" CheckState="Unchecked">
                                                                 </dx:ASPxCheckBox>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px; height: 35px;">
                                                                 <dx:ASPxLabel ID="ASPxLabel11" runat="server" Theme="SoftOrange" Text="Run Auto Shift">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px; height: 35px;">
                                                                 <dx:ASPxCheckBox ID="chkAutoRunShift" runat="server" CheckState="Unchecked" OnCheckedChanged="chkAutoRunShift_CheckedChanged">
                                                                 </dx:ASPxCheckBox>
                                                             </td>
                                                             <td style="padding: 5px; height: 35px;"></td>
                                                             <td style="padding: 5px; height: 35px;"></td>
                                                             <td style="padding: 5px; height: 35px;">
                                                                 <dx:ASPxCheckBox ID="chk2" runat="server" CheckState="Unchecked">
                                                                 </dx:ASPxCheckBox>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px"  >
                                                                 <dx:ASPxLabel ID="Label1" runat="server" Theme="SoftOrange" Visible="False">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px" rowspan="3" >
                                                                 <table style="width: 100%" runat="server" id="RunAutoShift" >
                                                                     <tr>
                                                                         <%--  <td>
                                                                             <dx:ASPxLabel ID="ASPxLabel12" runat="server" Text="Add Shifts" Theme="SoftOrange">
                                                                             </dx:ASPxLabel>
                                                                         </td>--%>
                                                                         <td>
                                                                             <dx:ASPxComboBox ID="ddlAutoRun_AddShift" runat="server" Theme="SoftOrange">
                                                                                 <Items>
                                                                                     <dx:ListEditItem Text="Fixed" Value="F" />
                                                                                     <dx:ListEditItem Text="Rotational" Value="R" />
                                                                                     <dx:ListEditItem Text="Ignore" Value="I" />
                                                                                 </Items>
                                                                             </dx:ASPxComboBox>
                                                                         </td>
                                                                     </tr>
                                                                     <tr>
                                                                         <%--<td>&nbsp;</td>--%>
                                                                         <td>
                                                                             <asp:ImageButton ID="cmdAddShift_AutoRun" runat="server" ImageUrl="~/Images/ADD.bmp" OnClick="cmdAddShift_AutoRun_Click" />
                                                                         </td>
                                                                     </tr>
                                                                     <tr>
                                                                         <%--<td>&nbsp;</td>--%>
                                                                         <td>
                                                                             <dx:ASPxTextBox ID="txtAutoRun_Shift" runat="server" Width="170px">
                                                                             </dx:ASPxTextBox>
                                                                         </td>
                                                                     </tr>
                                                                     <tr>
                                                                         <%-- <td>
                                                                             <dx:ASPxLabel ID="ASPxLabel26" runat="server" Text="Remove Shifts" Theme="SoftOrange">
                                                                             </dx:ASPxLabel>
                                                                         </td>--%>
                                                                         <td>
                                                                             <dx:ASPxComboBox ID="ddlAutoRun_RemoveShift" runat="server" Theme="SoftOrange">
                                                                                 <Items>
                                                                                     <dx:ListEditItem Text="Fixed" Value="F" />
                                                                                     <dx:ListEditItem Text="Rotational" Value="R" />
                                                                                     <dx:ListEditItem Text="Ignore" Value="I" />
                                                                                 </Items>
                                                                             </dx:ASPxComboBox>
                                                                         </td>
                                                                     </tr>
                                                                     <tr>
                                                                         <%--<td>&nbsp;</td>--%>
                                                                         <td>
                                                                             <asp:ImageButton ID="cmdAutoRun_RemoveShift" runat="server" ImageUrl="~/Images/ADD.bmp" />
                                                                         </td>
                                                                     </tr>
                                                                 </table>
                                                             </td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxCheckBox ID="chk3" runat="server" CheckState="Unchecked">
                                                                 </dx:ASPxCheckBox>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px" >
                                                                 <dx:ASPxCheckBox ID="chk4" runat="server" CheckState="Unchecked">
                                                                 </dx:ASPxCheckBox>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxCheckBox ID="chk5" runat="server" CheckState="Unchecked">
                                                                 </dx:ASPxCheckBox>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px; height: 38px;">
                                                                 <dx:ASPxLabel ID="ASPxLabel27" runat="server" Text="Early Min Auto Shift" Theme="SoftOrange">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px; height: 38px;">
                                                                 <dx:ASPxTextBox ID="txtPermisEarlyMin" runat="server" MaxLength="3" onkeypress="fncInputNumericValuesOnly('txtPermisEarlyMin')" Width="70px">
                                                                 </dx:ASPxTextBox>
                                                             </td>
                                                             <td style="padding: 5px; height: 38px;"></td>
                                                             <td style="padding: 5px; height: 38px;">
                                                                 <dx:ASPxLabel ID="ASPxLabel19" runat="server" Text="Shift Remaining Days" Theme="SoftOrange">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px; height: 38px;">
                                                                 <dx:ASPxTextBox ID="txtShiftRemainigDays" runat="server" onkeypress="fncInputNumericValuesOnly('txtShiftRemainigDays')" Width="70px">
                                                                 </dx:ASPxTextBox>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel18" runat="server" Theme="SoftOrange" Text="Late MIn Auto Shift">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxTextBox ID="txtPermisLateMin" runat="server" onkeypress="fncInputNumericValuesOnly('txtPermisLateMin')" MaxLength="3" Width="70px" >
                                                                 </dx:ASPxTextBox>
                                                             </td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel29" runat="server" Text="Shift Change Days" Theme="SoftOrange">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxTextBox ID="txtShiftChangesDays" runat="server" onkeypress="fncInputNumericValuesOnly('txtShiftChangesDays')" Width="70px">
                                                                 </dx:ASPxTextBox>
                                                             </td>
                                                         </tr>
                                                     </table>
                                                 </dx:ContentControl>
                                             </ContentCollection>
                                         </dx:TabPage>
                                         <dx:TabPage Text="Time Office Policy">
                                             <ContentCollection>
                                                 <dx:ContentControl runat="server">
                                                     <table align="center" class="dxflInternalEditorTable" width="100%">
                                                         <tr>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel35" runat="server" Text="Duplicate Check Min">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxTextBox ID="txtDuplicateCheck" runat="server" MaxLength="2" onkeypress= "fncInputNumericValuesOnly('txtDuplicateCheck')" Width="70px">
                                                                 </dx:ASPxTextBox>
                                                             </td>
                                                             <td style="padding: 5px"></td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel36" runat="server" Text="No Of Pre For Week Off">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxTextBox ID="txtWO" runat="server" MaxLength="1"  onkeypress= "fncInputNumericValuesOnly('txtWO')" Width="70px">
                                                                 </dx:ASPxTextBox>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel37" runat="server" Text="4 Punch In Night Shift">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxCheckBox ID="chkFourPunchNight" runat="server" CheckState="Unchecked">
                                                                 </dx:ASPxCheckBox>
                                                             </td>
                                                             <td style="padding: 5px"></td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel47" runat="server" Text="Is Auto Absent">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxCheckBox ID="chkAutoAbsent" runat="server" CheckState="Unchecked">
                                                                 </dx:ASPxCheckBox>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel38" runat="server" Text="End Time For In Punch">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxTextBox ID="txtEndTime_In" runat="server" Width="70px">
                                                                     <MaskSettings Mask="HH:mm" />
                                                                 </dx:ASPxTextBox>
                                                             </td>
                                                             <td style="padding: 5px"></td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel48" runat="server" Text="Week Off Include In Duty Roster">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxCheckBox ID="chkWOInclude" runat="server" CheckState="Unchecked">
                                                                 </dx:ASPxCheckBox>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel39" runat="server" Text="End Time For In Punch For RTC Emp">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxTextBox ID="txtEndTime_RTCEmp" runat="server" Width="70px">
                                                                     <MaskSettings Mask="HH:mm" />
                                                                 </dx:ASPxTextBox>
                                                             </td>
                                                             <td style="padding: 5px"></td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel49" runat="server" Text="Mark Wo As Abs When No Of Pre&lt; No Of Pre For WO">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxCheckBox ID="chkWo_Absent" runat="server" CheckState="Unchecked">
                                                                 </dx:ASPxCheckBox>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel40" runat="server" Text="Is Persent On WO Pre">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxCheckBox ID="chkPresent_Present" runat="server" CheckState="Unchecked">
                                                                 </dx:ASPxCheckBox>
                                                             </td>
                                                             <td style="padding: 5px"></td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel50" runat="server" Text="Mask MIS As Absent">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxCheckBox ID="chkMISAsAbs" runat="server" CheckState="Unchecked">
                                                                 </dx:ASPxCheckBox>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel41" runat="server" Text="Is Pre On HLD Pre">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxCheckBox ID="chkPresent_WO" runat="server" CheckState="Unchecked">
                                                                 </dx:ASPxCheckBox>
                                                             </td>
                                                             <td style="padding: 5px"></td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel51" runat="server" Text="Mark MIS AS Half Day">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxCheckBox ID="chkMISAsHLF" runat="server" CheckState="Unchecked">
                                                                 </dx:ASPxCheckBox>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px; height: 35px;">
                                                                 <dx:ASPxLabel ID="ASPxLabel42" runat="server" Text="Mark A W/H A As AAA">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px; height: 35px;">
                                                                 <dx:ASPxCheckBox ID="chkAWA_AAA" runat="server" CheckState="Unchecked">
                                                                 </dx:ASPxCheckBox>
                                                             </td>
                                                             <td style="padding: 5px; height: 35px;"></td>
                                                             <td style="padding: 5px; height: 35px;">
                                                                 <dx:ASPxLabel ID="ASPxLabel52" runat="server" Text="Out Work Minus For Working Hours">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px; height: 35px;">
                                                                 <dx:ASPxCheckBox ID="ChkOWMinus" runat="server" CheckState="Unchecked">
                                                                 </dx:ASPxCheckBox>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel43" runat="server" Text="Mark AW/H As AA">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxCheckBox ID="chkAW_AA" runat="server" CheckState="Unchecked">
                                                                 </dx:ASPxCheckBox>
                                                             </td>
                                                             <td style="padding: 5px"></td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel53" runat="server" Text="Is COF Allowed On POW/POH">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxCheckBox ID="chkAllowCOff" runat="server" CheckState="Unchecked">
                                                                 </dx:ASPxCheckBox>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxLabel ID="ASPxLabel44" runat="server" Text="Mark W/H A As AA">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxCheckBox ID="chkWA_AA" runat="server" CheckState="Unchecked">
                                                                 </dx:ASPxCheckBox>
                                                             </td>
                                                             <td style="padding: 5px"></td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px"></td>
                                                             <td style="padding: 5px"></td>
                                                             <td style="padding: 5px"></td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px"></td>
                                                             <td style="padding: 5px"></td>
                                                             <td style="padding: 5px"></td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px"></td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px"></td>
                                                             <td style="padding: 5px"></td>
                                                             <td style="padding: 5px"></td>
                                                         </tr>
                                                     </table>
                                                 </dx:ContentControl>
                                             </ContentCollection>
                                         </dx:TabPage>
                                         <dx:TabPage Text="OT Setup">
                                             <ContentCollection>
                                                 <dx:ContentControl runat="server">
                                                     <table align="center" class="dxflInternalEditorTable" width="100%">
                                                         <tr>
                                                             <td style="padding: 5px; height: 38px;">
                                                                 <dx:ASPxCheckBox ID="ChkOTAppli" runat="server" CheckState="Unchecked" Text="Is OT Applicable" ClientInstanceName="ChkOTAppli" OnCheckedChanged="ChkOTAppli_CheckedChanged">
                                                                     <ClientSideEvents CheckedChanged="function(s, e) {
	var&nbsp;checkBox =&nbsp;document.getElementById(&quot;ChkOTAppli&quot;);
	
}" />
                                                                 </dx:ASPxCheckBox>
                                                             </td>
                                                             <td style="padding: 5px; height: 38px;"></td>
                                                             <td style="padding: 5px; height: 38px;"></td>
                                                             <td style="padding: 5px; height: 38px;">
                                                                 <dx:ASPxLabel ID="ASPxLabel54" runat="server" Text="Over Rate/Hour" Theme="SoftOrange">
                                                                 </dx:ASPxLabel>
                                                             </td>
                                                             <td style="padding: 5px; height: 38px;">
                                                                 <dx:ASPxTextBox ID="txtOTRate" runat="server" Width="70px" MaxLength="6" NullText="000.00">
                                                                     <MaskSettings Mask="000.00" />
                                                                 </dx:ASPxTextBox>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxRoundPanel ID="ASPxRoundPanel9" runat="server" HeaderText="OT Option" Theme="Office2010Black" Width="300px" View="GroupBox">
                                                                     <PanelCollection>
                                                                         <dx:PanelContent runat="server">
                                                                             <dx:ASPxRadioButtonList ID="radOT" runat="server" EnableTheming="True" SelectedIndex="0" Theme="SoftOrange" Width="280px">
                                                                                 <Items>
                                                                                     <dx:ListEditItem Selected="True" Text="OT=OutTime-ShiftEndTime" Value="1" />
                                                                                     <dx:ListEditItem Text="OT=WorkingHours-ShiftHours" Value="2" />
                                                                                     <dx:ListEditItem Text="OT=EarlyComing+LateDeparture" Value="3" />
                                                                                 </Items>
                                                                             </dx:ASPxRadioButtonList>
                                                                         </dx:PanelContent>
                                                                     </PanelCollection>
                                                                 </dx:ASPxRoundPanel>
                                                             </td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px"></td>
                                                             <td style="padding: 5px" colspan="2">
                                                                 <dx:ASPxRoundPanel ID="ASPxRoundPanel10" runat="server" HeaderText="OT Parameter Option" Theme="Office2010Black" View="GroupBox" Width="300px">
                                                                     <PanelCollection>
                                                                         <dx:PanelContent runat="server">
                                                                             <dx:ASPxCheckBox ID="chkOT_Early" runat="server" CheckState="Unchecked" ClientInstanceName="IsOTOnEarly" Text="OT Allowd In Case Of Ealry Arrival" ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                                                                             </dx:ASPxCheckBox>
                                                                             <br />
                                                                             <dx:ASPxCheckBox ID="IsOTinMinus" runat="server" CheckState="Unchecked" ClientInstanceName="IsOTinMinus" Text="OT In (-) Calculation" ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                                                                             </dx:ASPxCheckBox>
                                                                             <br />
                                                                             <dx:ASPxCheckBox ID="chkRoundOT" runat="server" CheckState="Unchecked" ClientInstanceName="IsOTRound" Text="Round Over Time" ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                                                                             </dx:ASPxCheckBox>
                                                                         </dx:PanelContent>
                                                                     </PanelCollection>
                                                                 </dx:ASPxRoundPanel>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px; height: 36px;"></td>
                                                             <td style="padding: 5px; height: 36px;"></td>
                                                             <td style="padding: 5px; height: 36px;"></td>
                                                             <td style="padding: 5px; height: 36px;"></td>
                                                             <td style="padding: 5px; height: 36px;"></td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px">
                                                                 <dx:ASPxRoundPanel ID="ASPxRoundPanel11" runat="server" HeaderText="OT Deduction" Theme="Office2010Black" View="GroupBox" Width="300px">
                                                                     <PanelCollection>
                                                                         <dx:PanelContent runat="server">
                                                                             <table width="100%">
                                                                                 <tr>
                                                                                     <td class="auto-style9">
                                                                                         <dx:ASPxLabel ID="ASPxLabel46" runat="server" Text="Deduct OT On Holiday">
                                                                                         </dx:ASPxLabel>
                                                                                     </td>
                                                                                     <td class="auto-style9">
                                                                                         <dx:ASPxTextBox ID="txtDeductHLD" runat="server" onkeypress="fncInputNumericValuesOnly('txtDeductHLD')" Width="70px">
                                                                                         </dx:ASPxTextBox>
                                                                                     </td>
                                                                                 </tr>
                                                                                 <tr>
                                                                                     <td>
                                                                                         <dx:ASPxLabel ID="ASPxLabel55" runat="server" Text="Deduct OT On WeekOff">
                                                                                         </dx:ASPxLabel>
                                                                                     </td>
                                                                                     <td>
                                                                                         <dx:ASPxTextBox ID="txtDeductWO" runat="server" onkeypress="fncInputNumericValuesOnly('txtDeductWO')" Width="70px">
                                                                                         </dx:ASPxTextBox>
                                                                                     </td>
                                                                                 </tr>
                                                                             </table>
                                                                         </dx:PanelContent>
                                                                     </PanelCollection>
                                                                 </dx:ASPxRoundPanel>
                                                             </td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px"></td>
                                                             <td style="padding: 5px" colspan="2">
                                                                 <dx:ASPxRoundPanel ID="ASPxRoundPanel12" runat="server" HeaderText="OT Duration" Theme="Office2010Black" View="GroupBox" Width="300px">
                                                                     <PanelCollection>
                                                                         <dx:PanelContent runat="server">
                                                                             <table width="100%">
                                                                                 <tr>
                                                                                     <td class="auto-style9">
                                                                                         <dx:ASPxLabel ID="ASPxLabel56" runat="server" Text="OT Early Coming Duration">
                                                                                         </dx:ASPxLabel>
                                                                                     </td>
                                                                                     <td class="auto-style9">
                                                                                         <dx:ASPxTextBox ID="txtOT_EarlyComing" runat="server" onkeypress="fncInputNumericValuesOnly('txtOT_EarlyComing')" Width="70px" MaxLength="2">
                                                                                         </dx:ASPxTextBox>
                                                                                     </td>
                                                                                 </tr>
                                                                                 <tr>
                                                                                     <td>
                                                                                         <dx:ASPxLabel ID="ASPxLabel57" runat="server" Text="OT Late Coming Duration">
                                                                                         </dx:ASPxLabel>
                                                                                     </td>
                                                                                     <td>
                                                                                         <dx:ASPxTextBox ID="txtOT_LateComing" runat="server" onkeypress="fncInputNumericValuesOnly('txtOT_LateComing')" Width="70px" MaxLength="2">
                                                                                         </dx:ASPxTextBox>
                                                                                     </td>
                                                                                 </tr>
                                                                                 <tr>
                                                                                     <td>
                                                                                         <dx:ASPxLabel ID="ASPxLabel58" runat="server" Text="OT Restict End Duration">
                                                                                         </dx:ASPxLabel>
                                                                                     </td>
                                                                                     <td>
                                                                                         <dx:ASPxTextBox ID="txtOT_Restricted" runat="server" MaxLength="2" onkeypress="fncInputNumericValuesOnly('txtOT_Restricted')" Width="70px">
                                                                                         </dx:ASPxTextBox>
                                                                                     </td>
                                                                                 </tr>
                                                                             </table>
                                                                         </dx:PanelContent>
                                                                     </PanelCollection>
                                                                 </dx:ASPxRoundPanel>
                                                             </td>
                                                         </tr>
                                                         <tr>
                                                             <td style="padding: 5px"></td>
                                                             <td style="padding: 5px">&nbsp;</td>
                                                             <td style="padding: 5px"></td>
                                                             <td style="padding: 5px"></td>
                                                             <td style="padding: 5px"></td>
                                                         </tr>
                                                     </table>
                                                 </dx:ContentControl>
                                             </ContentCollection>
                                         </dx:TabPage>
                                     </TabPages>
                                 </dx:ASPxPageControl>
                             </td>
                         </tr>
                         <tr>
                             <td align="left" style="padding: 5px">
                                 <dx:ASPxLabel ID="lblstatus" runat="server" Text="" ForeColor="Red">
                                 </dx:ASPxLabel>
                             </td>
                         </tr>
                         <tr>
                             <td align="right" style="padding: 5px">
                                 <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Save" ValidationGroup="MK" OnClick="ASPxButton1_Click" >
                                 </dx:ASPxButton>
                             </td>
                         </tr>
                     </table>
                 </td>
             </tr>
         </table>
    </div>
</asp:Content>

