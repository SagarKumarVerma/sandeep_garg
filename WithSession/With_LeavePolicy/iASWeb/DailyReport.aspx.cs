﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Globalization;
using System.Text;
using System.IO;
using System.Diagnostics;
using GlobalSettings;

public partial class DailyReport : System.Web.UI.Page
{

    Class_Connection con = new Class_Connection();
    string datefrom = null;
    string dateTo = null;
    string strsql = null;
    string Strsql = null;
    DataSet ds = null;
    string OpenPopupPage = "";
    string Selection = "";
    string status = null;
    string leave = null;
    DateTime toDate;
    DateTime FromDate;
    TimeSpan ts = TimeSpan.Zero;
    int day = 0;

    string btnGoBack = "";
    string resMsg = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            lookupComp.DataSource = DataFilter.CompanyNonAdmin;
            lookupDept.DataSource = DataFilter.LocationNonAdmin;
        }
    }
    protected void txtDateFrom_TextChanged(object sender, EventArgs e)
    {
        try
        {
            FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            toDate = FromDate.AddMonths(1).AddDays(-1);
            txtToDate.Text = toDate.ToString("dd/MM/yyyy");
            //if (radcontlatearrival.Checked || radContEarlyDept.Checked || radContAbsent1.Checked || radTextPunch.Checked)
            //{
            //    txtToDate.Visible = true;
            //    //imgCal.Visible = true;
            //    lblto.Visible = true;
            //}
            //else
            //{
            //    txtToDate.Visible = false;
            //    // imgCal.Visible = false;
            //    lblto.Visible = false;
            //}
        }
        catch
        {
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('Invalid Date Format');", true);
            return;
        }
    }
}