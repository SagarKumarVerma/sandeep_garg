using System;
using System.Data;
using System.IO;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

public class ErrorClass
{
    string sSql = string.Empty;
    Class_Connection Con = new Class_Connection();
    public ErrorClass()
    {

    }
    
    public void Write_Log( string PageName, string FunctionName, string ErrorDescription)
    {

        try
        {
            string FilePath = HttpContext.Current.Session["SERVER_PATH"].ToString();
            FilePath = FilePath + "\\" + ConfigurationManager.AppSettings["ErrorFileName"].ToString();

            // FUNCTION TO WRITE ERRORLOG(ELMSLOG.TXT)
            int a, ErNo;
            string Emsg = "";
            StringBuilder sBuilder = new StringBuilder();
            string Dt = System.DateTime.Now.Date.ToString("dd/MM/yyyy") + " " + System.DateTime.Now.Hour.ToString("00") + " " + System.DateTime.Now.Minute.ToString("00");

            if (!File.Exists(FilePath)) //If Not File Exists
            {
                TextWriter tw = new StreamWriter(FilePath);
                tw.WriteLine("=========================================================================");
                tw.WriteLine("Error No.   Date\tSource\t\tFunction Name\t\tError   ");
                tw.WriteLine("=========================================================================");
                tw.Close();
            }

            ErNo = GenerateErrorNumber(FilePath);

            //Open the File and Write the Error 

            FileStream Fs = File.Open(FilePath, FileMode.Append);
            StreamWriter Sw = new StreamWriter(Fs);

            Emsg = " " + ErNo.ToString() + "\t" + Dt + "\t" + PageName.ToString() + " \t" + FunctionName.ToString() + "\t" + ErrorDescription.ToString();
            Sw.WriteLine(Emsg.ToString());
            Sw.WriteLine("-------------------------------------------------------------------------");

            Sw.Close();
        }
        catch 
        {
            
           
        }
       // System.Web.HttpContext.Current.Response.Redirect("FrmErrorPage.aspx?ErrorNumber=" + ErNo.ToString() + "&ErrDescription="+ ErrorDescription.ToString());
    }

    public void TrapError(string PageName, string FunctionName, string ErrorDescription)
    {
        
        
        string FilePath = HttpContext.Current.Session["SERVER_PATH"].ToString();
        FilePath = FilePath + "\\" + ConfigurationManager.AppSettings["TrapFileName"].ToString();
        // FUNCTION TO WRITE Trapfile
        int a, ErNo;
        string Emsg = "";
        StringBuilder sBuilder = new StringBuilder();
        string Dt = System.DateTime.Now.Date.ToString("dd/MM/yyyy") + " " + System.DateTime.Now.Hour.ToString("00") + " " + System.DateTime.Now.Minute.ToString("00"); 

        if (!File.Exists(FilePath)) //If Not File Exists
        {
            TextWriter tw = new StreamWriter(FilePath);
            tw.WriteLine("=========================================================================");
            tw.WriteLine("Error No.   Date\tSource\t\tFunction Name\t\tError   ");
            tw.WriteLine("=========================================================================");
            tw.Close();
        }

        ErNo = GenerateErrorNumber(FilePath);

        //Open the File and Write the Error 

        FileStream Fs = File.Open(FilePath, FileMode.Append);
        StreamWriter Sw = new StreamWriter(Fs);

        Emsg = " " + ErNo.ToString() + "\t" + Dt + "\t" + PageName.ToString() + " \t" + FunctionName.ToString() + "\t" + ErrorDescription.ToString();
        Sw.WriteLine(Emsg.ToString());
        Sw.WriteLine("-------------------------------------------------------------------------");

        Sw.Close();
    }
    public void InsertLog(string LogDetails, string OldValues, string NewValues, String LoginUserName, string LoginCompany)
    {
        try
        {
            sSql = "INSERT INTO [dbo].[UserEditLog]([UserName],[LogDate],[LogDetails],[OldValue],[NewValue],[CompanyCode])values('" + LoginUserName + "',getdate(),'" + LogDetails.Trim() + "','" + OldValues.Trim() + "','" + NewValues + "','" + LoginCompany + "')";
            Con.execute_NonQuery(sSql);
        }
        catch
        {

        }
    }
    private int GenerateErrorNumber(string FilePath)
    {
        // Counting Records of the File 
        // StreamReader Sr = File.OpenText(Server.MapPath("ELMSLOG.TXT")); 
        int RecNo = -2;
        try
        {
            StreamReader Sr = File.OpenText(FilePath);
            string TxtLine;
           // int RecNo = -2;
            do
            {
                TxtLine = Sr.ReadLine();
                if (TxtLine != null && (TxtLine.ToString().Substring(0, 5)) != "-----")
                    RecNo++;
            } while (TxtLine != null);

            Sr.Close();
            return RecNo;
        }
        catch
        {
            
            
        }
        return RecNo;
    }



}
