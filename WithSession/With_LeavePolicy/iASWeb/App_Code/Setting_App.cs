using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Setting_App
/// </summary>
public class Setting_App
{
    public string isDemo = "";
    public int UserLicence = 0;
    public int MaxDevice = 0;
    public string  View_InActive= "";
    public string ExpireDate = "";
	public Setting_App()
	{
        isDemo = "Y";
        UserLicence=1500;
        View_InActive = "Y";
        ExpireDate = "30/08/2021";
	}
}
