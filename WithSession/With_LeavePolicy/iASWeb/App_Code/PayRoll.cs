using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;


public class PayRoll
{
    DataSet dsFormula;
    string strsql;
    int iPos, iPosThen, iPosElse;
    string sPOS, STHEN, sELSE, sEndIf;
    Class_Connection con = new Class_Connection();
    string result;
    double g_pfmaxlimit;
    
    public double Hr2Min(string mMIN)
    {
        mMIN = Update_Time(mMIN);
        return ((int)Math.Floor(Convert.ToDecimal(mMIN.Substring(0, (mMIN.IndexOf(":", 0) + 1) - 1)) * 60)) + (int)Math.Floor(Convert.ToDecimal(mMIN.Substring(mMIN.Length - 2)));
        
        //double Value = 1.75;
        //double Hours = Math.Floor(Value);
        //double Minutes = (Value % 1 * 100);
        //Minutes = Math.Round(Minutes, 2);
        //if(Minutes >= 60)
        //{
        //Minutes = Math.Floor(Minutes/ 60) + (Minutes % 60 / 100);

        //Minutes = Math.Round(Minutes,2);
        //}
        //else
        //{
        //Minutes = Math.Round(Minutes / 100, 2);
        //}
        //string HoursAndMinutes = Convert.ToString((Hours + Minutes));
    }
    public string M_To_hour(string min)
    {        
        int tme = Convert.ToInt32(min);
        int hour = tme / 60;
        int minute = tme % 60;
        return hour.ToString("00")+"."+minute.ToString("00");
    }
    public string M_To_hour1(string min)
    {
        int tme = Convert.ToInt32(min);
        int hour = tme / 60;
        int minute = tme % 60;
        return hour.ToString("00") + ":" + minute.ToString("00");
    }
    public string Update_Time(string iTime)
    {
        string iLeftShift = null;
        string iRightShift = null;
        int iStrLen = 0;
        string iStr = null;
        int i = 0;
        iStrLen = ((iTime.ToUpper().IndexOf(".".ToUpper(), 0) + 1) - 1);
        iStr = ".";
        if (iStrLen <= 0)
        {
            iStrLen = ((iTime.ToUpper().IndexOf(":".ToUpper(), 0) + 1) - 1);
            iStr = ":";
        }
        if (iStrLen <= 0)
        {
            iLeftShift = ((iTime.Trim(' ').Length == 1) ? "0" + iTime.Trim(' ') : iTime.Trim(' '));
            iRightShift = "00";
        }
        else
        {
            iLeftShift = SimulateVal.Val(iTime.Substring(0, iStrLen)).ToString("00");
            iRightShift = Convert.ToString(iTime.Substring(iStrLen + 2 - 1)).Trim(' ');
            for (i = 1; i <= 2 - iRightShift.Length; i++)
            {
                iRightShift = iRightShift.Trim(' ') + "0";
            }
        }
        return iLeftShift + ":" + iRightShift;
    }
    internal static class SimulateVal
    {
        internal static double Val(string expression)
        {
            if (expression == null)
                return 0;
           
            for (int size = expression.Length; size > 0; size--)
            {
                double testDouble;
                if (double.TryParse(expression.Substring(0, size), out testDouble))
                    return testDouble;
            }
            return 0;
        }
        internal static double Val(object expression)
        {
            if (expression == null)
                return 0;

            double testDouble;
            if (double.TryParse(expression.ToString(), out testDouble))
                return testDouble;
   
            bool testBool;
            if (bool.TryParse(expression.ToString(), out testBool))
                return testBool ? -1 : 0;
   
            System.DateTime testDate;
            if (System.DateTime.TryParse(expression.ToString(), out testDate))
                return testDate.Day;
   
            return 0;
        }
        internal static int Val(char expression)
        {
            int testInt;
            if (int.TryParse(expression.ToString(), out testInt))
                return testInt;
            else
                return 0;
        }
    }
    public int NoOfDay(DateTime ProcDate)
    {
        DateTime dtm1 = DateTime.MinValue;
        DateTime dtm2 = DateTime.MinValue;

        dtm1 = Microsoft.VisualBasic.DateAndTime.DateSerial(ProcDate.Year, ProcDate.Month, 1);
        dtm2 = dtm1.AddMonths(1);
        dtm2 = dtm2.AddDays(-1);
        return dtm2.Day;
    }
    public string ProcFormula(string sFORMULA)
    {
        strsql = "Select CODE,FORM From Pay_Formula Where Code='" +sFORMULA.ToString().Trim()+"' ";
        dsFormula = con.FillDataSet(strsql);
        if (dsFormula.Tables[0].Rows.Count > 0)
        {
            result = dsFormula.Tables[0].Rows[0]["FORM"].ToString().Trim();
        }
        else
            result = "";
        return result;
    }
    public double FormulaCal(string a, double BASIC, double DA, double HRA, double PF, double FPF, double ESI, double PRE, double ABS1, double HLD, double LATE, double EARLY, double OT, double CL, double SL, double PL_EL, double OTHER_LV, double LEAVE, double TDAYS, double T_LATE, double T_EARLY, double OT_RATE, double MON_DAY, double ided1, double ided2, double ided3, double ided4, double ided5, double ided6, double ided7, double ided8, double ided9, double ided10, double iern1, double iern2, double iern3, double iern4, double iern5, double iern6, double iern7, double iern8, double iern9, double iern10, double iSalary, double iHRAAMT, double iOtAmt, double iErnAmt1, double iErnAmt2, double iErnAmt3, double iErnAmt4, double iErnAmt5, double iErnAmt6, double iErnAmt7, double iErnAmt8, double iErnAmt9, double iErnAmt10, double TDS, double PROF_TAX, double CONV, double MED)
    {
        float MyFloat ;
		string b = null;		
		int iLeftPos1 = 0;
		int iRightPos1 = 0;
		int iPos = 0;
		int iPos1 = 0;
		int iPos2 = 0;
		int iLen = 0;
		int AK = 0;
		string ak1 = null;
		object AK2 = null;
		object AK3 = null;
		object AK4 = null;
		bool bAkIf = false;
		double mValue = 0;
        
        ak1 =  FormulaValueSet(a, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED);   
        mValue = Evaluate(ak1);
		return Math.Round(mValue,2);        
    }
     public static double Evaluate(string expression)
    {
        return (double)new System.Xml.XPath.XPathDocument
        (new System.IO.StringReader("<r/>")).CreateNavigator().Evaluate
        (string.Format("number({0})", new
        System.Text.RegularExpressions.Regex(@"([\+\-\*])").Replace(expression, " ${1} ").Replace("/", " div ").Replace("%", " mod ")));
    }
    public string FormulaValueSet
        (string a, double BASIC, double DA, double HRA, double PF, double FPF,
        double ESI, double PRE, double ABS1, double HLD, double LATE, double EARLY, double OT, double CL, 
        double SL, double PL_EL, double OTHER_LV, double LEAVE, double TDAYS, double T_LATE, 
        double T_EARLY, double OT_RATE, double MON_DAY, double ided1, double ided2, double ided3,
        double ided4, double ided5, double ided6, double ided7, double ided8, double ided9, double ided10, 
        double iern1, double iern2, double iern3, double iern4, double iern5, double iern6, double iern7, 
        double iern8, double iern9, double iern10, double iSalary, double iHRAAMT, double iOtAmt, 
        double iErnAmt1, double iErnAmt2, double iErnAmt3, double iErnAmt4, double iErnAmt5,
        double iErnAmt6, double iErnAmt7, double iErnAmt8, double iErnAmt9, double iErnAmt10,
        double TDS, double PROF_TAX, double CONV, double MED)
        {
        int d = 0;
        DataSet mform = new DataSet();
        string sSql = null;
        string fhalf = null;
        string lhalf = null;
        int formulalen = 0;
        string bm = null;
        string a1 = null;
        int m = 0;
        bool mifChk = false;
        string St = null;
        int mCtr = 0;
        string Rt = null;
        string Tstring = null;
        string Fstring = null;
        int lCnt = 0;
        int Pcnt = 0;
        bm = a;

    a:
        a = " " + a.ToString() + " ";
        if ((a.ToUpper().IndexOf("#A#", 0) + 1) != 0)
        {
            d = (a.ToUpper().IndexOf("#A#", 0) + 1);
            sSql = "select * from pay_formula where code='A'";            
            mform = con.FillDataSet(sSql);
            formulalen = mform.Tables[0].Rows[0]["Form"].ToString().Trim().Length;
            fhalf = a.Substring(0, d - 1);
            formulalen = d + 3;
            lhalf = a.Substring(formulalen - 1);
            a = fhalf + "[ " + Convert.ToString(mform.Tables[0].Rows[0]["Form"].ToString().Trim()).Trim(' ') + " ]" + lhalf;
            if ((a.ToUpper().IndexOf("#A#", 0) + 1) != 0)
            {
                goto a;
            }
        }

        b:
        if ((a.ToUpper().IndexOf("#B#", 0) + 1) != 0)
        {
            d = (a.ToUpper().IndexOf("#B#", 0) + 1);
            sSql = "select * from pay_formula where code='B'";
            mform = con.FillDataSet(sSql);
            formulalen = mform.Tables[0].Rows[0]["Form"].ToString().Trim().Length;
            fhalf = a.Substring(0, d - 1);
            formulalen = d + 3;
            lhalf = a.Substring(formulalen - 1);
            a = fhalf + "[ " + Convert.ToString(mform.Tables[0].Rows[0]["Form"].ToString()).Trim(' ') + " ]" + lhalf;
            if ((a.ToUpper().IndexOf("#B#", 0) + 1) != 0)
            {
                goto b;
            }
        }

        c:
        if ((a.ToUpper().IndexOf("#C#", 0) + 1) != 0)
        {
            d = (a.ToUpper().IndexOf("#C#", 0) + 1);
            sSql = "select * from pay_formula where code='C'";
            mform = con.FillDataSet(sSql);
            formulalen = mform.Tables[0].Rows[0]["Form"].ToString().Trim().Length;
            fhalf = a.Substring(0, d - 1);
            formulalen = d + 3;
            lhalf = a.Substring(formulalen - 1);
            a = fhalf + "[ " + Convert.ToString(mform.Tables[0].Rows[0]["Form"].ToString()).Trim(' ') + " ]" + lhalf;
            if ((a.ToUpper().IndexOf("#C#", 0) + 1) != 0)
            {
                goto c;
            }
        }

        d:
        if ((a.ToUpper().IndexOf("#D#", 0) + 1) != 0)
        {
            d = (a.ToUpper().IndexOf("#D#", 0) + 1);
            sSql = "select * from pay_formula where code='D'";
            mform = con.FillDataSet(sSql);
            formulalen = mform.Tables[0].Rows[0]["Form"].ToString().Trim().Length;         
            fhalf = a.Substring(0, d - 1);
            formulalen = d + 3;
            lhalf = a.Substring(formulalen - 1);
            a = fhalf + "[ " + Convert.ToString(mform.Tables[0].Rows[0]["Form"].ToString()).Trim(' ') + " ]" + lhalf;
            if ((a.ToUpper().IndexOf("#D#", 0) + 1) != 0)
            {
                goto d;
            }
        }

        e:
        if ((a.ToUpper().IndexOf("#E#", 0) + 1) != 0)
        {
            d = (a.ToUpper().IndexOf("#E#", 0) + 1);
            sSql = "select * from pay_formula where code='E'";
            mform = con.FillDataSet(sSql);
            formulalen = mform.Tables[0].Rows[0]["Form"].ToString().Trim().Length;         
            fhalf = a.Substring(0, d - 1);
            formulalen = d + 3;
            lhalf = a.Substring(formulalen - 1);
            a = fhalf + "[ " + mform.Tables[0].Rows[0]["Form"].ToString().Trim(' ') + " ]" + lhalf;
            if ((a.ToUpper().IndexOf("#E#", 0) + 1) != 0)
            {
                goto e;
            }
        }

        f:
        if ((a.ToUpper().IndexOf("#F#", 0) + 1) != 0)
        {
            d = (a.ToUpper().IndexOf("#F#", 0) + 1);
            sSql = "select * from pay_formula where code='F'";
            mform = con.FillDataSet(sSql);
            formulalen = mform.Tables[0].Rows[0]["Form"].ToString().Trim().Length;         
            fhalf = a.Substring(0, d - 1);
            formulalen = d + 3;
            lhalf = a.Substring(formulalen - 1);
            a = fhalf + "[ " + mform.Tables[0].Rows[0]["Form"].ToString().Trim(' ') + " ]" + lhalf;
            if ((a.ToUpper().IndexOf("#F#", 0) + 1) != 0)
            {
                goto f;
            }
        }

        g:
        if ((a.ToUpper().IndexOf("#G#", 0) + 1) != 0)
        {
            d = (a.ToUpper().IndexOf("#G#", 0) + 1);
            sSql = "select * from pay_formula where code='G'";
            mform = con.FillDataSet(sSql);
            formulalen = mform.Tables[0].Rows[0]["Form"].ToString().Trim().Length;         
            fhalf = a.Substring(0, d - 1);
            formulalen = d + 3;
            lhalf = a.Substring(formulalen - 1);
            a = fhalf + "[ " + mform.Tables[0].Rows[0]["Form"].ToString().Trim(' ') + " ]" + lhalf;
            if ((a.ToUpper().IndexOf("#G#", 0) + 1) != 0)
            {
                goto g;
            }
        }

        h:
        if ((a.ToUpper().IndexOf("#H#", 0) + 1) != 0)
        {
            d = (a.ToUpper().IndexOf("#H#", 0) + 1);
            sSql = "select * from pay_formula where code='H'";
            mform = con.FillDataSet(sSql);
            formulalen = mform.Tables[0].Rows[0]["Form"].ToString().Trim().Length;         
            fhalf = a.Substring(0, d - 1);
            formulalen = d + 3;
            lhalf = a.Substring(formulalen - 1);
            a = fhalf + "[ " + mform.Tables[0].Rows[0]["Form"].ToString().Trim(' ') + " ]" + lhalf;
            if ((a.ToUpper().IndexOf("#H#", 0) + 1) != 0)
            {
                goto h;
            }
        }

        i:
        if ((a.ToUpper().IndexOf("#I#", 0) + 1) != 0)
        {
            d = (a.ToUpper().IndexOf("#I#", 0) + 1);
            sSql = "select * from pay_formula where code='I'";
            mform = con.FillDataSet(sSql);
            formulalen = mform.Tables[0].Rows[0]["Form"].ToString().Trim().Length; 
            fhalf = a.Substring(0, d - 1);
            formulalen = d + 3;
            lhalf = a.Substring(formulalen - 1);
            a = fhalf + "[ " + mform.Tables[0].Rows[0]["Form"].ToString().Trim(' ') + " ]" + lhalf;
            if ((a.ToUpper().IndexOf("#I#", 0) + 1) != 0)
            {
                goto i;
            }
        }

        j:
        if ((a.ToUpper().IndexOf("#J#", 0) + 1) != 0)
        {
            d = (a.ToUpper().IndexOf("#J#", 0) + 1);
            sSql = "select * from pay_formula where code='J'";
            mform = con.FillDataSet(sSql);
            formulalen = mform.Tables[0].Rows[0]["Form"].ToString().Trim().Length; 
            fhalf = a.Substring(0, d - 1);
            formulalen = d + 3;
            lhalf = a.Substring(formulalen - 1);
            a = fhalf + "[ " + mform.Tables[0].Rows[0]["Form"].ToString().Trim(' ') + " ]" + lhalf;
            if ((a.ToUpper().IndexOf("#J#", 0) + 1) != 0)
            {
                goto j;
            }
        }

        k:
        if ((a.ToUpper().IndexOf("#K#", 0) + 1) != 0)
        {
            d = (a.ToUpper().IndexOf("#K#", 0) + 1);
            sSql = "select * from pay_formula where code='K'";
            mform = con.FillDataSet(sSql);
            formulalen = mform.Tables[0].Rows[0]["Form"].ToString().Trim().Length;         
            fhalf = a.Substring(0, d - 1);
            formulalen = d + 3;
            lhalf = a.Substring(formulalen - 1);
            a = fhalf + "[ " + mform.Tables[0].Rows[0]["Form"].ToString().Trim(' ') + " ]" + lhalf;
            if ((a.ToUpper().IndexOf("#K#", 0) + 1) != 0)
            {
                goto k;
            }
        }

        l:
        if ((a.ToUpper().IndexOf("#L#", 0) + 1) != 0)
        {
            d = (a.ToUpper().IndexOf("#L#", 0) + 1);
            sSql = "select * from pay_formula where code='L'";
            mform = con.FillDataSet(sSql);
            formulalen = mform.Tables[0].Rows[0]["Form"].ToString().Trim().Length;         
            fhalf = a.Substring(0, d - 1);
            formulalen = d + 3;
            lhalf = a.Substring(formulalen - 1);
            a = fhalf + "[ " + mform.Tables[0].Rows[0]["Form"].ToString().Trim(' ') + " ]" + lhalf;
            if ((a.ToUpper().IndexOf("#L#", 0) + 1) != 0)
            {
                goto l;
            }
        }

        m:
        if ((a.ToUpper().IndexOf("#M#", 0) + 1) != 0)
        {
            d = (a.ToUpper().IndexOf("#M#", 0) + 1);
            sSql = "select * from pay_formula where code='M'";
            mform = con.FillDataSet(sSql);
            formulalen = mform.Tables[0].Rows[0]["Form"].ToString().Trim().Length;         
            fhalf = a.Substring(0, d - 1);
            formulalen = d + 3;
            lhalf = a.Substring(formulalen - 1);
            a = fhalf + "[ " + mform.Tables[0].Rows[0]["Form"].ToString().Trim(' ') + " ]" + lhalf;
            if ((a.ToUpper().IndexOf("#M#", 0) + 1) != 0)
            {
                goto m;
            }
        }

        n:
        if ((a.ToUpper().IndexOf("#N#", 0) + 1) != 0)
        {
            d = (a.ToUpper().IndexOf("#N#", 0) + 1);
            sSql = "select * from pay_formula where code='N'";
            mform = con.FillDataSet(sSql);
            formulalen = mform.Tables[0].Rows[0]["Form"].ToString().Trim().Length;         
            fhalf = a.Substring(0, d - 1);
            formulalen = d + 3;
            lhalf = a.Substring(formulalen - 1);
            a = fhalf + "[ " + mform.Tables[0].Rows[0]["Form"].ToString().Trim(' ') + " ]" + lhalf;
            if ((a.ToUpper().IndexOf("#N#", 0) + 1) != 0)
            {
                goto n;
            }
        }

        o:
        if ((a.ToUpper().IndexOf("#O#", 0) + 1) != 0)
        {
            d = (a.ToUpper().IndexOf("#O#", 0) + 1);
            sSql = "select * from pay_formula where code='O'";
            mform = con.FillDataSet(sSql);
            formulalen = mform.Tables[0].Rows[0]["Form"].ToString().Trim().Length;         
            fhalf = a.Substring(0, d - 1);
            formulalen = d + 3;
            lhalf = a.Substring(formulalen - 1);
            a = fhalf + "[ " + mform.Tables[0].Rows[0]["Form"].ToString().Trim(' ') + " ]" + lhalf;
            if ((a.ToUpper().IndexOf("#O#", 0) + 1) != 0)
            {
                goto o;
            }
        }

        p:
        if ((a.ToUpper().IndexOf("#P#", 0) + 1) != 0)
        {
            d = (a.ToUpper().IndexOf("#P#", 0) + 1);
            sSql = "select * from pay_formula where code='P'";
            mform = con.FillDataSet(sSql);
            formulalen = mform.Tables[0].Rows[0]["Form"].ToString().Trim().Length;         
            fhalf = a.Substring(0, d - 1);
            formulalen = d + 3;
            lhalf = a.Substring(formulalen - 1);
            a = fhalf + "[ " + mform.Tables[0].Rows[0]["Form"].ToString().Trim(' ') + " ]" + lhalf;
            if ((a.ToUpper().IndexOf("#P#", 0) + 1) != 0)
            {
                goto p;
            }
        }

        q:
        if ((a.ToUpper().IndexOf("#Q#", 0) + 1) != 0)
        {
            d = (a.ToUpper().IndexOf("#Q#", 0) + 1);
            sSql = "select * from pay_formula where code='Q'";
            mform = con.FillDataSet(sSql);
            formulalen = mform.Tables[0].Rows[0]["Form"].ToString().Trim().Length;         
            fhalf = a.Substring(0, d - 1);
            formulalen = d + 3;
            lhalf = a.Substring(formulalen - 1);
            a = fhalf + "[ " + mform.Tables[0].Rows[0]["Form"].ToString().Trim(' ') + " ]" + lhalf;
            if ((a.ToUpper().IndexOf("#Q#", 0) + 1) != 0)
            {
                goto q;
            }
        }

        r:
        if ((a.ToUpper().IndexOf("#R#", 0) + 1) != 0)
        {
            d = (a.ToUpper().IndexOf("#R#", 0) + 1);
            sSql = "select * from pay_formula where code='R'";
            mform = con.FillDataSet(sSql);
            formulalen = mform.Tables[0].Rows[0]["Form"].ToString().Trim().Length;         
            fhalf = a.Substring(0, d - 1);
            formulalen = d + 3;
            lhalf = a.Substring(formulalen - 1);
            a = fhalf + "[ " + mform.Tables[0].Rows[0]["Form"].ToString().Trim(' ') + " ]" + lhalf;
            if ((a.ToUpper().IndexOf("#R#", 0) + 1) != 0)
            {
                goto r;
            }
        }

        s:
        if ((a.ToUpper().IndexOf("#S#", 0) + 1) != 0)
        {
            d = (a.ToUpper().IndexOf("#S#", 0) + 1);
            sSql = "select * from pay_formula where code='S'";
            mform = con.FillDataSet(sSql);
            formulalen = mform.Tables[0].Rows[0]["Form"].ToString().Trim().Length;         
            fhalf = a.Substring(0, d - 1);
            formulalen = d + 3;
            lhalf = a.Substring(formulalen - 1);
            a = fhalf + "[ " + mform.Tables[0].Rows[0]["Form"].ToString().Trim(' ') + " ]" + lhalf;
            if ((a.ToUpper().IndexOf("#S#", 0) + 1) != 0)
            {
                goto s;
            }
        }
        t:
        if ((a.ToUpper().IndexOf("#T#", 0) + 1) != 0)
        {
            d = (a.ToUpper().IndexOf("#T#", 0) + 1);
            sSql = "select * from pay_formula where code='T'";
            mform = con.FillDataSet(sSql);
            formulalen = mform.Tables[0].Rows[0]["Form"].ToString().Trim().Length;         
            fhalf = a.Substring(0, d - 1);
            formulalen = d + 3;
            lhalf = a.Substring(formulalen - 1);
            a = fhalf + "[ " + mform.Tables[0].Rows[0]["Form"].ToString().Trim(' ') + " ]" + lhalf;
            if ((a.ToUpper().IndexOf("#T#", 0) + 1) != 0)
            {
                goto t;
            }
        }

        u:
        if ((a.ToUpper().IndexOf("#U#", 0) + 1) != 0)
        {
            d = (a.ToUpper().IndexOf("#U#", 0) + 1);
            sSql = "select * from pay_formula where code='U'";
            mform = con.FillDataSet(sSql);
            formulalen = mform.Tables[0].Rows[0]["Form"].ToString().Trim().Length;         
            fhalf = a.Substring(0, d - 1);
            formulalen = d + 3;
            lhalf = a.Substring(formulalen - 1);
            a = fhalf + "[ " + mform.Tables[0].Rows[0]["Form"].ToString().Trim(' ') + " ]" + lhalf;
            if ((a.ToUpper().IndexOf("#U#", 0) + 1) != 0)
            {
                goto u;
            }
        }

        v:
        if ((a.ToUpper().IndexOf("#V#", 0) + 1) != 0)
        {
            d = (a.ToUpper().IndexOf("#V#", 0) + 1);
            sSql = "select * from pay_formula where code='V'";
            mform = con.FillDataSet(sSql);
            formulalen = mform.Tables[0].Rows[0]["Form"].ToString().Trim().Length; 
            fhalf = a.Substring(0, d - 1);
            formulalen = d + 3;
            lhalf = a.Substring(formulalen - 1);
            a = fhalf + "[ " + mform.Tables[0].Rows[0]["Form"].ToString().Trim(' ') + " ]" + lhalf;
            if ((a.ToUpper().IndexOf("#V#", 0) + 1) != 0)
            {
                goto v;
            }
        }

        w:
        if ((a.ToUpper().IndexOf("#W#", 0) + 1) != 0)
        {
            d = (a.ToUpper().IndexOf("#W#", 0) + 1);
            sSql = "select * from pay_formula where code='W'";
            mform = con.FillDataSet(sSql);
            formulalen = mform.Tables[0].Rows[0]["Form"].ToString().Trim().Length;         
            fhalf = a.Substring(0, d - 1);
            formulalen = d + 3;
            lhalf = a.Substring(formulalen - 1);
            a = fhalf + "[ " + mform.Tables[0].Rows[0]["Form"].ToString().Trim(' ') + " ]" + lhalf;
            if ((a.ToUpper().IndexOf("#W#", 0) + 1) != 0)
            {
                goto w;
            }
        }

        X:
        if ((a.ToUpper().IndexOf("#X#", 0) + 1) != 0)
        {
            d = (a.ToUpper().IndexOf("#X#", 0) + 1);
            sSql = "select * from pay_formula where code='X'";
            mform = con.FillDataSet(sSql);
            formulalen = mform.Tables[0].Rows[0]["Form"].ToString().Trim().Length;         
            fhalf = a.Substring(0, d - 1);
            formulalen = d + 3;
            lhalf = a.Substring(formulalen - 1);
            a = fhalf + "[ " + mform.Tables[0].Rows[0]["Form"].ToString().Trim(' ') + " ]" + lhalf;
            if ((a.ToUpper().IndexOf("#X#", 0) + 1) != 0)
            {
                goto X;
            }
        }

        Y:
        if ((a.ToUpper().IndexOf("#Y#", 0) + 1) != 0)
        {
            d = (a.ToUpper().IndexOf("#Y#", 0) + 1);
            sSql = "select * from pay_formula where code='Y'";
            mform = con.FillDataSet(sSql);
            formulalen = mform.Tables[0].Rows[0]["Form"].ToString().Trim().Length;         
            fhalf = a.Substring(0, d - 1);
            formulalen = d + 3;
            lhalf = a.Substring(formulalen - 1);
            a = fhalf + "[ " + mform.Tables[0].Rows[0]["Form"].ToString().Trim(' ') + " ]" + lhalf;
            if ((a.ToUpper().IndexOf("#Y#", 0) + 1) != 0)
            {
                goto Y;
            }
        }

        z:
        if ((a.ToUpper().IndexOf("#Z#", 0) + 1) != 0)
        {
            d = (a.ToUpper().IndexOf("#Z#", 0) + 1);
            sSql = "select * from pay_formula where code='Z'";
            mform = con.FillDataSet(sSql);
            formulalen = mform.Tables[0].Rows[0]["Form"].ToString().Trim().Length;         
            fhalf = a.Substring(0, d - 1);
            formulalen = d + 3;
            lhalf = a.Substring(formulalen - 1);
            a = fhalf + "[ " + mform.Tables[0].Rows[0]["Form"].ToString().Trim(' ') + " ]" + lhalf;
            if ((a.ToUpper().IndexOf("#Z#", 0) + 1) != 0)
            {
                goto z;
            }
        }
        
        if ((a.ToUpper().IndexOf(" BASIC ", 0) + 1) != 0)
        {
            a = a.Replace(" BASIC ", BASIC.ToString("000000"));
        }
        
        if ((a.ToUpper().IndexOf(" HRA ", 0) + 1) != 0)
        {
            a = a.Replace(" HRA ", HRA.ToString("000000"));
        }
        
        if ((a.ToUpper().IndexOf(" DA ", 0) + 1) != 0)
        {
            a = a.Replace(" DA ", DA.ToString("000000"));
        }
        
        if ((a.ToUpper().IndexOf(" PF ", 0) + 1) != 0)
        {
            a = a.Replace(" PF ", PF.ToString("00.00"));
        }
        
        if ((a.ToUpper().IndexOf(" FPF ", 0) + 1) != 0)
        {
            a = a.Replace(" FPF ", FPF.ToString("00.00"));
        }
        
        if ((a.ToUpper().IndexOf(" ESI ", 0) + 1) != 0)
        {
            a = a.Replace(" ESI ", ESI.ToString("00.00"));
        }
        
        if ((a.ToUpper().IndexOf(" PRE ", 0) + 1) != 0)
        {
            a = a.Replace(" PRE ", PRE.ToString("00.00"));
        }
        
        if ((a.ToUpper().IndexOf(" ABS ", 0) + 1) != 0)
        {
            a = a.Replace(" ABS ", ABS1.ToString("00.00"));
        }
        
        if ((a.ToUpper().IndexOf(" HLD ", 0) + 1) != 0)
        {
            a = a.Replace(" HLD ", HLD.ToString("00.00"));
        }
        
        if ((a.ToUpper().IndexOf(" LATE ", 0) + 1) != 0)
        {
            a = a.Replace(" LATE ", LATE.ToString("0000"));
        }
        
        if ((a.ToUpper().IndexOf(" EARLY ", 0) + 1) != 0)
        {
            a = a.Replace(" EARLY ", EARLY.ToString("0000"));
        }
        
        if ((a.ToUpper().IndexOf(" O.T. ", 0) + 1) != 0)
        {
            a = a.Replace(" O.T. ", OT.ToString("000.00"));
        }
        
        if ((a.ToUpper().IndexOf(" C.L. ", 0) + 1) != 0)
        {
            a = a.Replace(" C.L. ", CL.ToString("00.00"));
        }
        
        if ((a.ToUpper().IndexOf(" S.L. ", 0) + 1) != 0)
        {
            a = a.Replace(" S.L. ", SL.ToString("00.00"));
        }
        
        if ((a.ToUpper().IndexOf(" PL_EL ", 0) + 1) != 0)
        {
            a = a.Replace(" PL_EL ", PL_EL.ToString("00.00"));
        }
        
        if ((a.ToUpper().IndexOf(" OTHER_LV ", 0) + 1) != 0)
        {
            a = a.Replace(" OTHER_LV ", OTHER_LV.ToString("00.00"));
        }
        
        if ((a.ToUpper().IndexOf(" LEAVE ", 0) + 1) != 0)
        {
            a = a.Replace(" LEAVE ", LEAVE.ToString("00.00"));
        }
        
        if ((a.ToUpper().IndexOf("TDAYS", 0) + 1) != 0)
        {
            a = a.Replace("TDAYS", TDAYS.ToString("00.00"));
        }
        
        if ((a.ToUpper().IndexOf(" T_LATE ", 0) + 1) != 0)
        {
            a = a.Replace(" T_LATE ", T_LATE.ToString("000.00"));
        }
        
        if ((a.ToUpper().IndexOf(" T_EARLY ", 0) + 1) != 0)
        {
            a = a.Replace(" T_EARLY ", T_EARLY.ToString("000.00"));
        }
        
        if ((a.ToUpper().IndexOf(" EARN_1 ", 0) + 1) != 0)
        {
            a = a.Replace(" EARN_1 ", iern1.ToString("00000.00"));
        }
        
        if ((a.ToUpper().IndexOf(" EARN_2 ", 0) + 1) != 0)
        {
            a = a.Replace(" EARN_2 ", iern2.ToString("00000.00"));
        }

        
        if ((a.ToUpper().IndexOf(" EARN_3 ", 0) + 1) != 0)
        {
            a = a.Replace(" EARN_3 ", iern3.ToString("00000.00"));
        }

        
        if ((a.ToUpper().IndexOf(" EARN_4 ", 0) + 1) != 0)
        {
            a = a.Replace(" EARN_4 ", iern4.ToString("00000.00"));
        }

        
        if ((a.ToUpper().IndexOf(" EARN_5 ", 0) + 1) != 0)
        {
            a = a.Replace(" EARN_5 ", iern5.ToString("00000.00"));
        }

        
        if ((a.ToUpper().IndexOf(" EARN_6 ", 0) + 1) != 0)
        {
            a = a.Replace(" EARN_6 ", iern6.ToString("00000.00"));
        }


        if ((a.ToUpper().IndexOf(" EARN_7 ", 0) + 1) != 0)
        {
            a = a.Replace(" EARN_7 ", iern7.ToString("00000.00"));
        }


        if ((a.ToUpper().IndexOf(" EARN_8 ", 0) + 1) != 0)
        {
            a = a.Replace(" EARN_8 ", iern8.ToString("00000.00"));
        }


        if ((a.ToUpper().IndexOf(" EARN_9 ", 0) + 1) != 0)
        {
            a = a.Replace(" EARN_9 ", iern9.ToString("00000.00"));
        }

        if ((a.ToUpper().IndexOf(" EARN_10 ", 0) + 1) != 0)
        {
            a = a.Replace(" EARN_10 ", iern10.ToString("00000.00"));
        }


        if ((a.ToUpper().IndexOf(" DEDUCT_1 ", 0) + 1) != 0)
        {
            a = a.Replace(" DEDUCT_1 ", ided1.ToString("00000.00"));
        }

        if ((a.ToUpper().IndexOf(" DEDUCT_2 ", 0) + 1) != 0)
        {
            a = a.Replace(" DEDUCT_2 ", ided2.ToString("00000.00"));
        }


        if ((a.ToUpper().IndexOf(" DEDUCT_3 ", 0) + 1) != 0)
        {
            a = a.Replace(" DEDUCT_3 ", ided3.ToString("00000.00"));
        }


        if ((a.ToUpper().IndexOf(" DEDUCT_4 ", 0) + 1) != 0)
        {
            a = a.Replace(" DEDUCT_4 ", ided4.ToString("00000.00"));
        }


        if ((a.ToUpper().IndexOf(" DEDUCT_5 ", 0) + 1) != 0)
        {
            a = a.Replace(" DEDUCT_5 ", ided5.ToString("00000.00"));
        }


        if ((a.ToUpper().IndexOf(" DEDUCT_6 ", 0) + 1) != 0)
        {
            a = a.Replace(" DEDUCT_6 ", ided6.ToString("00000.00"));
        }


        if ((a.ToUpper().IndexOf(" DEDUCT_7 ", 0) + 1) != 0)
        {
            a = a.Replace(" DEDUCT_7 ", ided7.ToString("00000.00"));
        }


        if ((a.ToUpper().IndexOf(" DEDUCT_8 ", 0) + 1) != 0)
        {
            a = a.Replace(" DEDUCT_8 ", ided8.ToString("00000.00"));
        }


        if ((a.ToUpper().IndexOf(" DEDUCT_9 ", 0) + 1) != 0)
        {
            a = a.Replace(" DEDUCT_9 ", ided9.ToString("00000.00"));
        }


        if ((a.ToUpper().IndexOf(" DEDUCT_10 ", 0) + 1) != 0)
        {
            a = a.Replace(" DEDUCT_10 ", ided10.ToString("00000.00"));
        }


        if ((a.ToUpper().IndexOf(" OT_RATE ", 0) + 1) != 0)
        {
            a = a.Replace(" OT_RATE ", OT_RATE.ToString("0000.00"));
        }

        if ((a.ToUpper().IndexOf(" MON_DAY ", 0) + 1) != 0)
        {
            a = a.Replace(" MON_DAY ", MON_DAY.ToString("0000"));
        }

        if ((a.ToUpper().IndexOf(" CONV ", 0) + 1) != 0)
        {
            a = a.Replace(" CONV ", CONV.ToString("000000"));
        }

        if ((a.ToUpper().IndexOf(" MED ", 0) + 1) != 0)
        {
            a = a.Replace(" MED ", MED.ToString("000000"));
        }


        if ((a.ToUpper().IndexOf(" TDS ", 0) + 1) != 0)
        {
            a = a.Replace(" TDS ", TDS.ToString("000000"));
        }

        if ((a.ToUpper().IndexOf(" PROF_TAX ", 0) + 1) != 0)
        {
            a = a.Replace(" PROF_TAX ", PROF_TAX.ToString("000000"));
        }


        if ((a.ToUpper().IndexOf(" ISALARY ", 0) + 1) != 0)
        {
            a = a.Replace(" ISALARY ", iSalary.ToString("000000.00"));
        }

        if ((a.ToUpper().IndexOf(" IOTAMT ", 0) + 1) != 0)
        {
            a = a.Replace(" IOTAMT ", iOtAmt.ToString("00000.00"));
        }

        if ((a.ToUpper().IndexOf(" IHRAAMT ", 0) + 1) != 0)
        {
            a = a.Replace(" IHRAAMT ", iHRAAMT.ToString("000000.00"));
        }
        
        if ((a.ToUpper().IndexOf(" IERNAMT1 ", 0) + 1) != 0)
        {
            a = a.Replace(" IERNAMT1 ", iErnAmt1.ToString("0000000.00"));
        }
        
        if ((a.ToUpper().IndexOf(" IERNAMT2 ", 0) + 1) != 0)
        {
            a = a.Replace(" IERNAMT2 ", iErnAmt2.ToString("0000000.00"));
        }
        
        if ((a.ToUpper().IndexOf(" IERNAMT3 ", 0) + 1) != 0)
        {
            a = a.Replace(" IERNAMT3 ", iErnAmt3.ToString("0000000.00"));
        }
        
        if ((a.ToUpper().IndexOf(" IERNAMT4 ", 0) + 1) != 0)
        {
            a = a.Replace(" IERNAMT4 ", iErnAmt4.ToString("0000000.00"));
        }
        
        if ((a.ToUpper().IndexOf(" IERNAMT5 ", 0) + 1) != 0)
        {
            a = a.Replace(" IERNAMT5 ", iErnAmt5.ToString("0000000.00"));
        }
        
        if ((a.ToUpper().IndexOf(" IERNAMT6 ", 0) + 1) != 0)
        {
            a = a.Replace(" IERNAMT6 ", iErnAmt6.ToString("0000000.00"));
        }
        
        if ((a.ToUpper().IndexOf(" IERNAMT7 ", 0) + 1) != 0)
        {
            a = a.Replace(" IERNAMT7 ", iErnAmt7.ToString("0000000.00"));
        }
        
        if ((a.ToUpper().IndexOf(" IERNAMT8 ", 0) + 1) != 0)
        {
            a = a.Replace(" IERNAMT8 ", iErnAmt8.ToString("0000000.00"));
        }
        
        if ((a.ToUpper().IndexOf(" IERNAMT9 ", 0) + 1) != 0)
        {
            a = a.Replace(" IERNAMT9 ", iErnAmt9.ToString("0000000.00"));
        }
        
        if ((a.ToUpper().IndexOf(" IERNAMT10 ", 0) + 1) != 0)
        {
            a = a.Replace(" IERNAMT10 ", iErnAmt10.ToString("0000000.00"));
        }

        
        if ((a.ToUpper().IndexOf(" PFMAX ", 0) + 1) != 0)
        {
            a = a.Replace(" PFMAX ", g_pfmaxlimit.ToString("0000000"));
        }
        a1 = "";
        m = 1;
        for (int m1 = 1; m1 <= a.Trim(' ').Length; m1++)
        {
            if (a.Trim(' ').Substring(m1 - 1, 1) != " ")
            {
                a1 = a1 + a.Trim(' ').Substring(m1 - 1, 1);
            }
        }
       
        if (a1.Trim(' ').Substring(0, 2) == "IF")
        {
		lCnt = 0;
		Pcnt = 0;
		mCtr = 1;
		while ((a1.Trim(' ').IndexOf("IF", mCtr - 1) + 1) != 0)
		{
			lCnt = (a1.Trim(' ').IndexOf("IF", mCtr - 1) + 1);
			mCtr = lCnt + 1;
		}
		St = a1.Trim(' ').Substring(0, lCnt - 1);
		if (lCnt > 1)
		{
			mCtr = lCnt;
			while ((a1.Trim(' ').IndexOf("]", mCtr - 1) + 1) != 0)
			{
				Pcnt = Pcnt + 1;
				if (Pcnt == 2)
				{
					Pcnt = (a1.Trim(' ').IndexOf("]", mCtr - 1) + 1);
					break;
				}
				mCtr = (a1.Trim(' ').IndexOf("]", mCtr - 1) + 1) + 1;
			}
			Rt = a1.Trim(' ').Substring(Pcnt + 2 - 1, a1.Trim(' ').Length - (Pcnt + 1));
			a1 = a1.Trim(' ').Substring(lCnt - 1, Pcnt + 2 - lCnt);
		}
		else
		{
			Rt = "";
		}
		mifChk = false;
		a = a1.Trim(' ').Substring((a1.Trim(' ').IndexOf("(", 0) + 1), (a1.Trim(' ').IndexOf(",", 0) + 1) - ((a1.Trim(' ').IndexOf("(", 0) + 1) + 1));
		mifChk = Convert.ToBoolean(a.Substring(0, a.Length));
		a = a1.Trim(' ').Substring((a1.Trim(' ').IndexOf(",", 0) + 1), a1.Trim(' ').Length - ((a1.Trim(' ').IndexOf(",", 0) + 1) + 1));
		Tstring = a.Trim(' ').Substring(1, ((a.Trim(' ').IndexOf(",", 0) + 1) - 3));
		Fstring = a.Trim(' ').Substring(((a.Trim(' ').IndexOf(",", 0) + 1) + 2) - 1, (a.Trim(' ').Length - ((a.Trim(' ').IndexOf(",", 0) + 1) + 2)));
		if (mifChk)
		{
			a1 = Tstring;
		}
		else
		{
			a1 = Fstring;
		}
		a = St + a1 + Rt;
		a1 = a;
		
	}
	//FormulaValueSet = a1;

return a1;
}

public double Min2Hr(string mMIN)
    {
    double tempMin2Hr = 0;
    if (SimulateVal1.Val(mMIN) >= 0)
        {
        tempMin2Hr = ((double)Math.Floor(SimulateVal1.Val(mMIN) / 60) + ((Convert.ToInt32(mMIN) % 60) / 100));
        }
    else
        {
            tempMin2Hr = -((double)Math.Floor(SimulateVal1.Val(Math.Abs(Convert.ToSByte(mMIN))) / 60) + ((Math.Abs(Convert.ToSByte(mMIN)) % 60) / 100));
        }
    return tempMin2Hr;
    }

internal static class SimulateVal1
{
	internal static double Val(string expression)
	{
		if (expression == null)
			return 0;

		for (int size = expression.Length; size > 0; size--)
		{
			double testDouble;
			if (double.TryParse(expression.Substring(0, size), out testDouble))
				return testDouble;
		}

		return 0;
	}
	internal static double Val(object expression)
	{
		if (expression == null)
			return 0;

		double testDouble;
		if (double.TryParse(expression.ToString(), out testDouble))
			return testDouble;


		bool testBool;
		if (bool.TryParse(expression.ToString(), out testBool))
			return testBool ? -1 : 0;


		System.DateTime testDate;
		if (System.DateTime.TryParse(expression.ToString(), out testDate))
			return testDate.Day;

		//no value is recognized, so return 0:
		return 0;
	}
	internal static int Val(char expression)
	{
		int testInt;
		if (int.TryParse(expression.ToString(), out testInt))
			return testInt;
		else
			return 0;
	}
}
    /// <summary>
    /// ////////
    /// </summary>

    

    public int MonthDays(DateTime ProcDate)
    {
    DateTime dtm1 = DateTime.MinValue;
    DateTime dtm2 = DateTime.MinValue;
    dtm1 = Microsoft.VisualBasic.DateAndTime.DateSerial(ProcDate.Year, ProcDate.Month, 1);
    dtm2 = dtm1.AddMonths(1);
    dtm2 = dtm2.AddDays(-1);
    return dtm2.Day;
    }
    //Minutes to hour
    public string MinToHr(string mMIN)
    {
        string[] min = mMIN.Split('.');
        string min1 = min[0].ToString();
        
        double m;
        double h;
        m = Convert.ToInt32(min1) / 60;
        h = Convert.ToInt32(min1) % 60;
        return  m.ToString("000") + "." + h.ToString("00");
    }
    public int indexof(string name)
    {
        int n = name.IndexOf('m', 0);
        return n;
    }


    //DataCapture

    public static void Main(string[] args)
    {
        //PaySetup p = new PaySetupOverride();

    }


    public PayRoll()
	{
	
	}
}
	
//public class PaySetup
//{
//    protected virtual string pay_setup()
//    {
//        return "formula";
//    }
//}

//public class PaySetupOverride : PaySetup
//{
//    public new string pay_setup()
//    {
//        return "Overides";
//    }
//}

/*Make foreign key in the same table
create table Table1
( T1_Id bigint not null primary key, T1_Data  varchar(9)  not null)

create table Table2
( T2_Id bigint not null primary key, T2_Data varchar(37) not null, foreign key (T2_Id) references Table1 (T1_Id)
)*/



