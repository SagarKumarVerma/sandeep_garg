﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Drawing;
using System.Globalization;
using System.Xml;
using System.Collections.Generic;

public partial class DeviceGroupMapping : System.Web.UI.Page
{
    OleDbConnection MyConn = new OleDbConnection();
    OleDbCommand MyCmd = new OleDbCommand();
    int result = 0;
    Class_Connection dal = new Class_Connection();
    string Temp;
    string strSql;
    int m_iRowIdx;
    public string KeyValue = "";// 
    public string TableMain = null;
    public string TableName = null;
    public string ColumnName = null;
    string Value = null;
    OleDbDataReader dr;
    DataSet Ds;
    DataSet ds;
    string str = null;
    string selection = "";
    ErrorClass ec = new ErrorClass();
    string com = "";
    DateTime dt;
    DataSet ds1;
    string find = "";
    string Strsql = null;
    string resMsg = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            if (Session["LoginUserType"] == null || Session["LoginUserType"].ToString().Trim() == "U")
            {
                Response.Redirect("Login.aspx");
            }
            if (Session["username"] == null)
            {
                Response.Redirect("Login.aspx");
            }

            GetDevices();
        }
    }
    public void GetDevices()
    {
        DataSet ds = new DataSet();
        DataSet ds1 = new DataSet();

        string sql = "select GroupID,GroupName from ClientDeviceGroup where DeviceType='HTSeries'";
        ds = dal.FillDataSet(sql);
        if (ds.Tables[0].Rows.Count > 0)
        {

            ddlGroup.DataSource = ds.Tables[0];
            ddlGroup.TextField = "GroupName";
            ddlGroup.ValueField = "GroupID";
            ddlGroup.DataBind();

        }


        sql = "select id_No,SerialNumber,DeviceName from tblMachine where devicemode in ('HTSeries') ";
        ds1 = dal.FillDataSet(sql);
        if (ds1.Tables[0].Rows.Count > 0)
        {
            grdDevice.DataSource = ds1;
            grdDevice.DataBind();
        }
        else
        {

            grdDevice.DataSource = null;
            grdDevice.DataBind();
        }

    }
    protected void Page_Init(object sender, EventArgs e)
    {
        GetDevices();
        grdDevice.DataBind();
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {

        try
        {
            string Group = "";
            try
            {
                Group = ddlGroup.SelectedItem.Text.ToString().Trim();
            }
            catch
            {

            }

            if (Group.Trim() == "" || Group.Trim() == string.Empty)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Please Select Group')</script>");
                return;
            }
            int GroupId = Convert.ToInt32(ddlGroup.SelectedItem.Value.ToString().Trim());
            List<object> keys = grdDevice.GetSelectedFieldValues(new string[] { grdDevice.KeyFieldName });
            if (keys.Count == 0)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Please Select Atleast One Device')</script>");
                return;
            }
            string sql = "delete from ClientDeviceGroupMapping where GroupID=" + GroupId + " ";
            dal.execute_NonQuery(sql);
            for (int i = 0; i < grdDevice.VisibleRowCount; i++)
            {
                if (grdDevice.Selection.IsRowSelected(i) == true)
                {
                    string mDevId = grdDevice.GetRowValues(i, "SerialNumber").ToString().Trim();
                    string s = "insert into ClientDeviceGroupMapping(GroupID,DeviceID) values(" + GroupId + ",'" + mDevId.Trim() + "')";
                    dal.execute_NonQuery(s);
                    s = "update tblMachine set GroupID =" + GroupId + " where SerialNumber ='" + mDevId.Trim() + "'";
                    dal.execute_NonQuery(s);
                }
            }
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Group Mapped Successfully')", true);
            GetDevices();

        }
        catch (Exception Exc)
        {
            Error_Occured("Reboot", Exc.Message);
            ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Error In Mapping Group ')</script>");
        }
    }

    protected void ddlGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();

            if (ddlGroup.SelectedIndex != -1)
            {
                grdDevice.Selection.UnselectAll();
                string sql = "select DeviceID from ClientDeviceGroupMapping where GroupId=" + ddlGroup.SelectedItem.Value.ToString().Trim();
                ds = dal.FillDataSet(sql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int x = 0; x < ds.Tables[0].Rows.Count; x++)
                    {
                        string mDevId = ds.Tables[0].Rows[x]["DeviceID"].ToString().Trim();
                        for (int i = 0; i < grdDevice.VisibleRowCount; i++)
                        {
                            if (mDevId == grdDevice.GetRowValues(i, "SerialNumber").ToString().Trim())
                            {
                                grdDevice.Selection.SelectRow(i);
                            }

                        }
                    }
                }
                else
                {
                    //foreach (GridViewRow dr in grdDevice.Rows)
                    //{
                    //    CheckBox chkDevice = (CheckBox)dr.FindControl("chkDevice");


                    //    chkDevice.Checked = false;

                    //}

                }
            }
        }
        catch
        {

        }
    }
}