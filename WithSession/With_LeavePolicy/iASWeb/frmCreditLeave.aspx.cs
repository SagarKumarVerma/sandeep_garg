﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Globalization;
using System.Data.OleDb;

public partial class frmCreditLeave : System.Web.UI.Page
{

    OleDbConnection MyConn = new OleDbConnection();
    OleDbCommand MyCmd = new OleDbCommand();
    DataSet ds = null;
    string Strsql = null;
    Class_Connection Con = new Class_Connection();
    string C_Code = null;
    string D_Code = null;
    string Msg;
    ArrayList lasset = new ArrayList();
    ArrayList lsubordinate = new ArrayList();
    static ArrayList UpdateList = new ArrayList();
    int result = 0;
    int result1 = 0;
    string OtFactor = "";
    string CompanyCode = null;
    string DeptCode = null;
    DateTime Hdate, AdjustedDate;
    ArrayList lasset1 = new ArrayList();
    ArrayList lsubordinate1 = new ArrayList();
    static ArrayList UpdateList1 = new ArrayList();
    string StrSql = string.Empty;
    DateTime Dt;
    
    DataSet dsLeave = new DataSet();
    int year = 0;
    int month = 0;
    int curyear = 0;
    int curmonth = 0;
    string JoinDate = "";
    double bCL = 0;
    double bEL = 0, CreditEL=0;
    string Paycode = null;
    string ESSN = null;
    string btnGoBack = "";
    string resMsg = "";
    int Result;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserName"] == null)
        {
            Session.Abandon();
            Response.Redirect("Login.aspx");
        }
        // bindLeave();
        if (Session["LevelUpdate"] == null)
        {
            Response.Redirect("Login.aspx");
        }
        CheckLeaveTable();
        bindCompany();
    }

    protected void CheckLeaveTable()
    {
        try
        {
            string strsql = "";
            DataSet dsAtt = new DataSet();
            string isRun = "";


            strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblCreditLeave' and col.name='SL' ";
            dsAtt = Con.FillDataSet(strsql);
            if (dsAtt.Tables[0].Rows.Count == 0)
            {
                strsql = "alter table tblCreditLeave add SL float";
                Con.execute_NonQuery(strsql);

            }


            strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblCreditLeave' and col.name='EL' ";
            dsAtt = Con.FillDataSet(strsql);
            if (dsAtt.Tables[0].Rows.Count == 0)
            {
                strsql = "alter table tblCreditLeave add EL float";
                Con.execute_NonQuery(strsql);

            }



            strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblCreditLeave' and col.name='SLCredit' ";
            dsAtt = Con.FillDataSet(strsql);
            if (dsAtt.Tables[0].Rows.Count == 0)
            {
                strsql = "alter table tblCreditLeave add SLCredit char(1)";
                Con.execute_NonQuery(strsql);

            }


            strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblCreditLeave' and col.name='ELCredit' ";
            dsAtt = Con.FillDataSet(strsql);
            if (dsAtt.Tables[0].Rows.Count == 0)
            {
                strsql = "alter table tblCreditLeave add ELCredit char(1)";
                Con.execute_NonQuery(strsql);

            }
        }
        catch
        {

        }
    }

    protected void bindCompany()
    {
        try
        {
            Strsql = "select CompanyCode,convert(varchar(100),CompanyName)+' - '+convert(varchar(100),CompanyCode) as 'Comid' from tblCompany where CompanyCode!=''";
            if ((Session["Auth_Comp"] != null) && (Session["PAYCODE"].ToString().Trim().ToUpper() != "ADMIN"))
            {
                Strsql += " and companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
            }
            else if (Session["usertype"].ToString().Trim() == "H")
            {
                Strsql += " and companycode in (select companycode from tblemployee where paycode='" + Session["PAYCODE"].ToString().Trim() + "') ";
            }

            ds = new DataSet();
            ds = Con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {

                ddlCompany.DataSource = ds;
                ddlCompany.TextField = "Comid";
                ddlCompany.ValueField = "CompanyCode";
                ddlCompany.DataBind();
                ddlCompany.SelectedIndex = 0;
            }
            else
            {
                ddlCompany.Items.Add("NONE");
                ddlCompany.SelectedIndex = 0;

            }
        }
        catch (Exception ex)
        {
            Error_Occured("bindCompany", ex.Message);
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {

      //  bindLeave();
       
    }
    //protected void bindLeave()
    //{
    //    Strsql = "Select leavefield,leavecode+' - ' +ltrim(leavedescription) as LeaveDesc from tblleavemaster where LeaveCode in('EL','SL') and  CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' order by 2";
    //    ds = new DataSet();
    //    ds = Con.FillDataSet(Strsql);
    //    if (ds.Tables[0].Rows.Count > 0)
    //    {
    //        GridLookup.DataSource = ds.Tables[0];

    //        GridLookup.DataBind();
    //    }
    //}

    protected void btnCreate_Click(object sender, EventArgs e)
    {
       try
        {
            if(chkEL.Checked)
            {
                CreditMonthlyLeaveEL();
            }
            else if (chkSL.Checked)
            {
                CreditMonthlyLeaveSL();
            }
            else
            {

            }
        }
        catch (Exception er)
        {
            Error_Occured("btnCreate_Click", er.Message);
        }
    }
    protected void CreditMonthlyLeaveEL()
    {
        DataSet DsDiv = new DataSet();
        DataSet DsLeave = new DataSet();
        DataSet DsEmp = new DataSet();
        int Result = 0;
        int ELDate = 30;
        string DivCode = "";

        double El = 1.5;
        string sSql = "";
        string ELCode = "";
        Dt = Convert.ToDateTime(TxtFromDate.Value);
        string CurrentMonth = System.DateTime.Today.Month.ToString();
        string CurrentDate = System.DateTime.Today.Day.ToString();
        year = Convert.ToInt32(Dt.Year.ToString());
        month = Convert.ToInt32(Dt.Month.ToString());
        ELDate = 1;// Convert.ToInt32(Dt.Day.ToString());
        //if (month == 2)
        //{
        //    ELDate = 28;
        //}

        double CreditEL = 0;
        try
        {

            sSql = " select LEAVEFIELD from tblleavemaster where LEAVECODE='EL' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
            DataSet dsLeaveCode = new DataSet();
            dsLeaveCode = Con.FillDataSet(sSql);
            if (dsLeaveCode.Tables[0].Rows.Count > 0)
            {
                ELCode = dsLeaveCode.Tables[0].Rows[0][0].ToString().Trim()+"_ADD";
            }
            else
            {
               string Msge = "<script language='javascript'>alert('EL Not Defined In Leave Master.')</script>";
                Page.RegisterStartupScript("Msg", Msge);
                return;
            }
            JoinDate = ELDate.ToString("00") + "/" + month.ToString("00") + "/" + year.ToString("0000");
            Dt = DateTime.ParseExact(JoinDate.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);

            //For Ragular
            StrSql = "select paycode,dateofjoin,SSN from tblemployee where Active='Y' and left(convert(varchar,dateofjoin, 120), 7)< '" + Dt.ToString("yyyy-MM") + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' ";
            DataSet ds = new DataSet();
            ds = Con.FillDataSet(StrSql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    try
                    {
                        Paycode = ds.Tables[0].Rows[i]["paycode"].ToString().Trim();
                        ESSN = ds.Tables[0].Rows[i]["SSN"].ToString().Trim();
                        CreditEL = 0;
                        StrSql = "select * from tblCreditLeave where LYEAR=" + year.ToString() + " and LMONTH=" + month.ToString() + " and PayCode='" + Paycode.ToString() + "'";
                        DataSet dsEmp = new DataSet();
                        dsEmp = Con.FillDataSet(StrSql);
                        if (dsEmp.Tables[0].Rows.Count > 0 && dsEmp.Tables[0].Rows[0]["ELCredit"].ToString().Trim() == "Y")
                        {
                            continue;
                        }
                        else if (dsEmp.Tables[0].Rows.Count == 0)
                        {
                            StrSql = "insert into tblCreditLeave(Lyear,LMonth,SLCredit,ELCredit,PayCode)values(" + year.ToString() + "," + month.ToString() + ",'N','N','" + Paycode.Trim() + "')";
                            Con.execute_NonQuery(StrSql);


                            StrSql = "select isnull(" + ELCode + ",0) 'L02_Add' from tblleaveledger where lyear=" + year.ToString("0000") + " and paycode='" + Paycode.ToString() + "'";
                            dsLeave = Con.FillDataSet(StrSql);

                            if (dsLeave.Tables[0].Rows.Count > 0)
                            {
                                CreditEL = Convert.ToDouble(dsLeave.Tables[0].Rows[0]["L02_Add"]) + El;

                                StrSql = "update tblleaveledger set " + ELCode + "=" + CreditEL + " where lyear=" + year.ToString("0000") + " and paycode='" + Paycode.ToString() + "' ";
                                Result = Con.execute_NonQuery(StrSql);

                                if (Result > 0)
                                {
                                    StrSql = "update tblCreditLeave set ELCredit='Y',EL='" + El + "' where paycode='" + Paycode.Trim() + "' and LMonth='" + month.ToString() + "' and Lyear=' " + year.ToString() + "'    ";
                                    Con.execute_NonQuery(StrSql);
                                }

                            }
                            else
                            {
                                string Strsql = "Insert into tblleaveledger (paycode,lyear,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13,L14,L15,L16,L17,L18,L19,L20,L01_Add,L02_Add,L03_Add,L04_Add,L05_Add,L06_Add,L07_Add,L08_Add,L09_Add,L10_Add,L11_Add,L12_Add,L13_Add,L14_Add,L15_Add,L16_Add,L17_Add,L18_Add,L19_Add,L20_Add,L01_CASH,L02_CASH,L03_CASH,L04_CASH,L05_CASH,L06_CASH,L07_CASH,L08_CASH,L09_CASH,L10_CASH,L11_CASH,L12_CASH,L13_CASH,L14_CASH,L15_CASH,L16_CASH,L17_CASH,L18_CASH,L19_CASH,L20_CASH,SSN) values('" + Paycode.ToString() + "','" + year.ToString("0000") + "',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'" + ESSN + "')";
                                Con.execute_NonQuery(Strsql);

                                StrSql = "update tblleaveledger set " + ELCode + "=" + El + " where lyear=" + year.ToString("0000") + " and paycode='" + Paycode.ToString() + "' ";
                                Result = Con.execute_NonQuery(StrSql);

                                if (Result > 0)
                                {
                                    StrSql = "update tblCreditLeave set ELCredit='Y',EL='" + El + "' where paycode='" + Paycode.Trim() + "' and LMonth='" + month.ToString() + "' and Lyear=' " + year.ToString() + "'    ";
                                    Con.execute_NonQuery(StrSql);
                                }

                            }
                        }
                        else
                        {
                            StrSql = "select isnull(" + ELCode + ",0) 'L02_Add' from tblleaveledger where lyear=" + year.ToString("0000") + " and paycode='" + Paycode.ToString() + "'";
                            dsLeave = Con.FillDataSet(StrSql);

                            if (dsLeave.Tables[0].Rows.Count > 0)
                            {
                                CreditEL = Convert.ToDouble(dsLeave.Tables[0].Rows[0]["L02_Add"]) + El;



                                StrSql = "update tblleaveledger set " + ELCode + "=" + CreditEL + " where lyear=" + year.ToString("0000") + " and paycode='" + Paycode.ToString() + "' ";
                                Result = Con.execute_NonQuery(StrSql);

                                if (Result > 0)
                                {
                                    StrSql = "update tblCreditLeave set ELCredit='Y',EL='" + El + "' where paycode='" + Paycode.Trim() + "' and LMonth='" + month.ToString() + "' and Lyear=' " + year.ToString() + "'    ";
                                    Con.execute_NonQuery(StrSql);
                                }

                            }
                            else
                            {
                                string Strsql = "Insert into tblleaveledger (paycode,lyear,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13,L14,L15,L16,L17,L18,L19,L20,L01_Add,L02_Add,L03_Add,L04_Add,L05_Add,L06_Add,L07_Add,L08_Add,L09_Add,L10_Add,L11_Add,L12_Add,L13_Add,L14_Add,L15_Add,L16_Add,L17_Add,L18_Add,L19_Add,L20_Add,L01_CASH,L02_CASH,L03_CASH,L04_CASH,L05_CASH,L06_CASH,L07_CASH,L08_CASH,L09_CASH,L10_CASH,L11_CASH,L12_CASH,L13_CASH,L14_CASH,L15_CASH,L16_CASH,L17_CASH,L18_CASH,L19_CASH,L20_CASH,SSN) values('" + Paycode.ToString() + "','" + year.ToString("0000") + "',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'" + ESSN + "')";
                                Con.execute_NonQuery(Strsql);

                                StrSql = "update tblleaveledger set " + ELCode + "=" + El + " where lyear=" + year.ToString("0000") + " and paycode='" + Paycode.ToString() + "' ";
                                Result = Con.execute_NonQuery(StrSql);

                                if (Result > 0)
                                {
                                    StrSql = "update tblCreditLeave set ELCredit='Y',EL='" + El + "' where paycode='" + Paycode.Trim() + "' and LMonth='" + month.ToString() + "' and Lyear=' " + year.ToString() + "'    ";
                                    Con.execute_NonQuery(StrSql);
                                }

                            }

                        }

                    }
                    catch (Exception er)
                    {
                        Error_Occured("For Loop EL", er.Message + "For Code" + Paycode);
                    }

                }
            }


            //For Date Of Join
            DataSet DsT = new DataSet();
            int ldate = 0;
            int doj = 0;
            int balDay = 0;
            DateTime Ldt = System.DateTime.MinValue;
            string str = "select datepart(dd,dateadd(dd,-1,dateadd(mm,1,dateadd(mm,datediff(mm,0,getdate()),0)))) 'dl',convert(varchar(10),dateadd(dd,-1,dateadd(mm,datediff(mm,0,getdate()),0)),126) 'LastDt' ";
            DsT = Con.FillDataSet(str);
            ldate = Convert.ToInt32(ds.Tables[0].Rows[0]["dl"]);
            Ldt = Convert.ToDateTime(ds.Tables[0].Rows[0]["LastDt"]);  //DateTime.ParseExact(ds.Tables[0].Rows[0]["LastDt"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture);

            StrSql = "select paycode,dateofjoin,SSN from tblemployee where Active='Y' and left(convert(varchar,dateofjoin, 120), 7)>= '" + Dt.ToString("yyyy-MM") + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' ";
            ds = new DataSet();
            ds = Con.FillDataSet(StrSql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    try
                    {
                        Paycode = ds.Tables[0].Rows[i]["paycode"].ToString().Trim();
                        ESSN = ds.Tables[0].Rows[i]["SSN"].ToString().Trim();
                        CreditEL = 0;

                        doj = Convert.ToInt32(ds.Tables[0].Rows[i]["dt"]);
                        balDay = ldate - doj;
                        El = (Convert.ToDouble(balDay) / Convert.ToDouble(ldate)) * 1.5;
                        if (El > 0)
                        {
                            El = Math.Round(El, 2);
                        }
                        else
                        {
                            El = 0;
                        }

                        StrSql = "select * from tblCreditLeave where LYEAR=" + year.ToString() + " and LMONTH=" + month.ToString() + " and PayCode='" + Paycode.ToString() + "'";
                        DataSet dsEmp = new DataSet();
                        dsEmp = Con.FillDataSet(StrSql);
                        if (dsEmp.Tables[0].Rows.Count > 0 && dsEmp.Tables[0].Rows[0]["ELCredit"].ToString().Trim() == "Y")
                        {
                            continue;
                        }
                        else if (dsEmp.Tables[0].Rows.Count == 0)
                        {
                            StrSql = "insert into tblCreditLeave(Lyear,LMonth,SLCredit,ELCredit,PayCode)values(" + year.ToString() + "," + month.ToString() + ",'N','N','" + Paycode.Trim() + "')";
                            Con.execute_NonQuery(StrSql);


                            StrSql = "select isnull(" + ELCode + ",0) 'L02_Add' from tblleaveledger where lyear=" + year.ToString("0000") + " and paycode='" + Paycode.ToString() + "'";
                            dsLeave = Con.FillDataSet(StrSql);

                            if (dsLeave.Tables[0].Rows.Count > 0)
                            {
                                CreditEL = Convert.ToDouble(dsLeave.Tables[0].Rows[0]["L02_Add"]) + El;

                                StrSql = "update tblleaveledger set " + ELCode + "=" + CreditEL + " where lyear=" + year.ToString("0000") + " and paycode='" + Paycode.ToString() + "' ";
                                Result = Con.execute_NonQuery(StrSql);

                                if (Result > 0)
                                {
                                    StrSql = "update tblCreditLeave set ELCredit='Y',EL='" + El + "' where paycode='" + Paycode.Trim() + "' and LMonth='" + month.ToString() + "' and Lyear=' " + year.ToString() + "'    ";
                                    Con.execute_NonQuery(StrSql);
                                }

                            }
                            else
                            {
                                string Strsql = "Insert into tblleaveledger (paycode,lyear,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13,L14,L15,L16,L17,L18,L19,L20,L01_Add,L02_Add,L03_Add,L04_Add,L05_Add,L06_Add,L07_Add,L08_Add,L09_Add,L10_Add,L11_Add,L12_Add,L13_Add,L14_Add,L15_Add,L16_Add,L17_Add,L18_Add,L19_Add,L20_Add,L01_CASH,L02_CASH,L03_CASH,L04_CASH,L05_CASH,L06_CASH,L07_CASH,L08_CASH,L09_CASH,L10_CASH,L11_CASH,L12_CASH,L13_CASH,L14_CASH,L15_CASH,L16_CASH,L17_CASH,L18_CASH,L19_CASH,L20_CASH,SSN) values('" + Paycode.ToString() + "','" + year.ToString("0000") + "',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'" + ESSN + "')";
                                Con.execute_NonQuery(Strsql);

                                StrSql = "update tblleaveledger set " + ELCode + "=" + El + " where lyear=" + year.ToString("0000") + " and paycode='" + Paycode.ToString() + "' ";
                                Result = Con.execute_NonQuery(StrSql);

                                if (Result > 0)
                                {
                                    StrSql = "update tblCreditLeave set ELCredit='Y',EL='" + El + "' where paycode='" + Paycode.Trim() + "' and LMonth='" + month.ToString() + "' and Lyear=' " + year.ToString() + "'    ";
                                    Con.execute_NonQuery(StrSql);
                                }

                            }
                        }
                        else
                        {
                            StrSql = "select isnull(" + ELCode + ",0) 'L02_Add' from tblleaveledger where lyear=" + year.ToString("0000") + " and paycode='" + Paycode.ToString() + "'";
                            dsLeave = Con.FillDataSet(StrSql);

                            if (dsLeave.Tables[0].Rows.Count > 0)
                            {
                                CreditEL = Convert.ToDouble(dsLeave.Tables[0].Rows[0]["L02_Add"]) + El;



                                StrSql = "update tblleaveledger set " + ELCode + "=" + CreditEL + " where lyear=" + year.ToString("0000") + " and paycode='" + Paycode.ToString() + "' ";
                                Result = Con.execute_NonQuery(StrSql);

                                if (Result > 0)
                                {
                                    StrSql = "update tblCreditLeave set ELCredit='Y',EL='" + El + "' where paycode='" + Paycode.Trim() + "' and LMonth='" + month.ToString() + "' and Lyear=' " + year.ToString() + "'    ";
                                    Con.execute_NonQuery(StrSql);
                                }

                            }
                            else
                            {
                                string Strsql = "Insert into tblleaveledger (paycode,lyear,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13,L14,L15,L16,L17,L18,L19,L20,L01_Add,L02_Add,L03_Add,L04_Add,L05_Add,L06_Add,L07_Add,L08_Add,L09_Add,L10_Add,L11_Add,L12_Add,L13_Add,L14_Add,L15_Add,L16_Add,L17_Add,L18_Add,L19_Add,L20_Add,L01_CASH,L02_CASH,L03_CASH,L04_CASH,L05_CASH,L06_CASH,L07_CASH,L08_CASH,L09_CASH,L10_CASH,L11_CASH,L12_CASH,L13_CASH,L14_CASH,L15_CASH,L16_CASH,L17_CASH,L18_CASH,L19_CASH,L20_CASH,SSN) values('" + Paycode.ToString() + "','" + year.ToString("0000") + "',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'" + ESSN + "')";
                                Con.execute_NonQuery(Strsql);

                                StrSql = "update tblleaveledger set " + ELCode + "=" + El + " where lyear=" + year.ToString("0000") + " and paycode='" + Paycode.ToString() + "' ";
                                Result = Con.execute_NonQuery(StrSql);

                                if (Result > 0)
                                {
                                    StrSql = "update tblCreditLeave set ELCredit='Y',EL='" + El + "' where paycode='" + Paycode.Trim() + "' and LMonth='" + month.ToString() + "' and Lyear=' " + year.ToString() + "'    ";
                                    Con.execute_NonQuery(StrSql);
                                }

                            }

                        }
                    }
                    catch (Exception er)
                    {
                        Error_Occured("For Loop EL", er.Message + "For Code" + Paycode);
                    }


                }
            }



            string Msg = "<script language='javascript'>alert('EL Credited Successfully For This Month.')</script>";
            Page.RegisterStartupScript("Msg", Msg);
        }

        catch (Exception er)
        {
            Error_Occured("Credit EL", er.Message);
        }
    }

    protected void CreditMonthlyLeaveSL()
    {
        DataSet DsDiv = new DataSet();
        DataSet DsLeave = new DataSet();
        DataSet DsEmp = new DataSet();
        int Result = 0;
        int ELDate = 30;
       
        double El =0.5;
        string sSql = "";
        string ELCode = "";
        Dt = Convert.ToDateTime(TxtFromDate.Value);
        string CurrentMonth = System.DateTime.Today.Month.ToString();
        string CurrentDate = System.DateTime.Today.Day.ToString();
        year = Convert.ToInt32(Dt.Year.ToString());
        month = Convert.ToInt32(Dt.Month.ToString());
        ELDate = 1;// Convert.ToInt32(Dt.Day.ToString());
        //if (month == 2)
        //{
        //    ELDate = 28;
        //}

        double CreditEL = 0;
        try
        {

            sSql = " select LEAVEFIELD from tblleavemaster where LEAVECODE='SL' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
            DataSet dsLeaveCode = new DataSet();
            dsLeaveCode = Con.FillDataSet(sSql);
            if (dsLeaveCode.Tables[0].Rows.Count > 0)
            {
                ELCode = dsLeaveCode.Tables[0].Rows[0][0].ToString().Trim() + "_ADD";
            }
            else
            {
                string Msge = "<script language='javascript'>alert('SL Not Defined In Leave Master.')</script>";
                Page.RegisterStartupScript("Msg", Msge);
                return;
            }
            JoinDate = ELDate.ToString("00") + "/" + month.ToString("00") + "/" + year.ToString("0000");
            Dt = DateTime.ParseExact(JoinDate.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);

            //For Ragular
            StrSql = "select paycode,dateofjoin,SSN from tblemployee where Active='Y' and left(convert(varchar,dateofjoin, 120), 7)< '" + Dt.ToString("yyyy-MM") + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' ";
            DataSet ds = new DataSet();
            ds = Con.FillDataSet(StrSql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    try
                    {

                        Paycode = ds.Tables[0].Rows[i]["paycode"].ToString().Trim();
                        ESSN = ds.Tables[0].Rows[i]["SSN"].ToString().Trim();
                        CreditEL = 0;
                        StrSql = "select * from tblCreditLeave where LYEAR=" + year.ToString() + " and LMONTH=" + month.ToString() + " and PayCode='" + Paycode.ToString() + "'";
                        DataSet dsEmp = new DataSet();
                        dsEmp = Con.FillDataSet(StrSql);
                        if (dsEmp.Tables[0].Rows.Count > 0 && dsEmp.Tables[0].Rows[0]["SLCredit"].ToString().Trim() == "Y")
                        {
                            continue;
                        }
                        else if (dsEmp.Tables[0].Rows.Count == 0)
                        {
                            StrSql = "insert into tblCreditLeave(Lyear,LMonth,SLCredit,ELCredit,PayCode)values(" + year.ToString() + "," + month.ToString() + ",'N','N','" + Paycode.Trim() + "')";
                            Con.execute_NonQuery(StrSql);


                            StrSql = "select isnull(" + ELCode + ",0) 'L02_Add' from tblleaveledger where lyear=" + year.ToString("0000") + " and paycode='" + Paycode.ToString() + "'";
                            dsLeave = Con.FillDataSet(StrSql);

                            if (dsLeave.Tables[0].Rows.Count > 0)
                            {
                                CreditEL = Convert.ToDouble(dsLeave.Tables[0].Rows[0]["L02_Add"]) + El;

                                StrSql = "update tblleaveledger set " + ELCode + "=" + CreditEL + " where lyear=" + year.ToString("0000") + " and paycode='" + Paycode.ToString() + "' ";
                                Result = Con.execute_NonQuery(StrSql);

                                if (Result > 0)
                                {
                                    StrSql = "update tblCreditLeave set SLCredit='Y',SL='" + El + "' where paycode='" + Paycode.Trim() + "' and LMonth='" + month.ToString() + "' and Lyear=' " + year.ToString() + "'    ";
                                    Con.execute_NonQuery(StrSql);
                                }

                            }
                            else
                            {
                                string Strsql = "Insert into tblleaveledger (paycode,lyear,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13,L14,L15,L16,L17,L18,L19,L20,L01_Add,L02_Add,L03_Add,L04_Add,L05_Add,L06_Add,L07_Add,L08_Add,L09_Add,L10_Add,L11_Add,L12_Add,L13_Add,L14_Add,L15_Add,L16_Add,L17_Add,L18_Add,L19_Add,L20_Add,L01_CASH,L02_CASH,L03_CASH,L04_CASH,L05_CASH,L06_CASH,L07_CASH,L08_CASH,L09_CASH,L10_CASH,L11_CASH,L12_CASH,L13_CASH,L14_CASH,L15_CASH,L16_CASH,L17_CASH,L18_CASH,L19_CASH,L20_CASH,SSN) values('" + Paycode.ToString() + "','" + year.ToString("0000") + "',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'" + ESSN + "')";
                                Con.execute_NonQuery(Strsql);

                                StrSql = "update tblleaveledger set " + ELCode + "=" + El + " where lyear=" + year.ToString("0000") + " and paycode='" + Paycode.ToString() + "' ";
                                Result = Con.execute_NonQuery(StrSql);

                                if (Result > 0)
                                {
                                    StrSql = "update tblCreditLeave set SLCredit='Y',SL='" + El + "' where paycode='" + Paycode.Trim() + "' and LMonth='" + month.ToString() + "' and Lyear=' " + year.ToString() + "'    ";
                                    Con.execute_NonQuery(StrSql);
                                }

                            }
                        }
                        else
                        {
                            StrSql = "select isnull(" + ELCode + ",0) 'L02_Add' from tblleaveledger where lyear=" + year.ToString("0000") + " and paycode='" + Paycode.ToString() + "'";
                            dsLeave = Con.FillDataSet(StrSql);

                            if (dsLeave.Tables[0].Rows.Count > 0)
                            {
                                CreditEL = Convert.ToDouble(dsLeave.Tables[0].Rows[0]["L02_Add"]) + El;



                                StrSql = "update tblleaveledger set " + ELCode + "=" + CreditEL + " where lyear=" + year.ToString("0000") + " and paycode='" + Paycode.ToString() + "' ";
                                Result = Con.execute_NonQuery(StrSql);

                                if (Result > 0)
                                {
                                    StrSql = "update tblCreditLeave set SLCredit='Y',SL='" + El + "' where paycode='" + Paycode.Trim() + "' and LMonth='" + month.ToString() + "' and Lyear=' " + year.ToString() + "'    ";
                                    Con.execute_NonQuery(StrSql);
                                }

                            }
                            else
                            {
                                string Strsql = "Insert into tblleaveledger (paycode,lyear,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13,L14,L15,L16,L17,L18,L19,L20,L01_Add,L02_Add,L03_Add,L04_Add,L05_Add,L06_Add,L07_Add,L08_Add,L09_Add,L10_Add,L11_Add,L12_Add,L13_Add,L14_Add,L15_Add,L16_Add,L17_Add,L18_Add,L19_Add,L20_Add,L01_CASH,L02_CASH,L03_CASH,L04_CASH,L05_CASH,L06_CASH,L07_CASH,L08_CASH,L09_CASH,L10_CASH,L11_CASH,L12_CASH,L13_CASH,L14_CASH,L15_CASH,L16_CASH,L17_CASH,L18_CASH,L19_CASH,L20_CASH,SSN) values('" + Paycode.ToString() + "','" + year.ToString("0000") + "',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'" + ESSN + "')";
                                Con.execute_NonQuery(Strsql);

                                StrSql = "update tblleaveledger set " + ELCode + "=" + El + " where lyear=" + year.ToString("0000") + " and paycode='" + Paycode.ToString() + "' ";
                                Result = Con.execute_NonQuery(StrSql);

                                if (Result > 0)
                                {
                                    StrSql = "update tblCreditLeave set SLCredit='Y',SL='" + El + "' where paycode='" + Paycode.Trim() + "' and LMonth='" + month.ToString() + "' and Lyear=' " + year.ToString() + "'    ";
                                    Con.execute_NonQuery(StrSql);
                                }

                            }

                        }

                    }
                    catch (Exception er)
                    {
                        Error_Occured("For Loop SL", er.Message  +"For Code" + Paycode);
                        continue;
                    }

                }
            }


            //For Date Of Join
            DataSet DsT = new DataSet();
            int ldate = 0;
            int doj = 0;
            int balDay = 0;
            DateTime Ldt = System.DateTime.MinValue;
            string str = "select datepart(dd,dateadd(dd,-1,dateadd(mm,1,dateadd(mm,datediff(mm,0,getdate()),0)))) 'dl',convert(varchar(10),dateadd(dd,-1,dateadd(mm,datediff(mm,0,getdate()),0)),126) 'LastDt' ";
            DsT = Con.FillDataSet(str);
            ldate = Convert.ToInt32(ds.Tables[0].Rows[0]["dl"]);
            Ldt = Convert.ToDateTime(ds.Tables[0].Rows[0]["LastDt"]);  //DateTime.ParseExact(ds.Tables[0].Rows[0]["LastDt"].ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture);

            StrSql = "select paycode,dateofjoin,SSN from tblemployee where Active='Y' and left(convert(varchar,dateofjoin, 120), 7)>= '" + Dt.ToString("yyyy-MM") + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' ";
            ds = new DataSet();
            ds = Con.FillDataSet(StrSql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    try
                    {

                        Paycode = ds.Tables[0].Rows[i]["paycode"].ToString().Trim();
                        ESSN = ds.Tables[0].Rows[i]["SSN"].ToString().Trim();
                        CreditEL = 0;

                        doj = Convert.ToInt32(ds.Tables[0].Rows[i]["dt"]);
                        balDay = ldate - doj;
                        El = (Convert.ToDouble(balDay) / Convert.ToDouble(ldate)) * 0.5;
                        if (El > 0)
                        {
                            El = Math.Round(El, 2);
                        }
                        else
                        {
                            El = 0;
                        }

                        StrSql = "select * from tblCreditLeave where LYEAR=" + year.ToString() + " and LMONTH=" + month.ToString() + " and PayCode='" + Paycode.ToString() + "'";
                        DataSet dsEmp = new DataSet();
                        dsEmp = Con.FillDataSet(StrSql);
                        if (dsEmp.Tables[0].Rows.Count > 0 && dsEmp.Tables[0].Rows[0]["SLCredit"].ToString().Trim() == "Y")
                        {
                            continue;
                        }
                        else if (dsEmp.Tables[0].Rows.Count == 0)
                        {
                            StrSql = "insert into tblCreditLeave(Lyear,LMonth,SLCredit,ELCredit,PayCode)values(" + year.ToString() + "," + month.ToString() + ",'N','N','" + Paycode.Trim() + "')";
                            Con.execute_NonQuery(StrSql);


                            StrSql = "select isnull(" + ELCode + ",0) 'L02_Add' from tblleaveledger where lyear=" + year.ToString("0000") + " and paycode='" + Paycode.ToString() + "'";
                            dsLeave = Con.FillDataSet(StrSql);

                            if (dsLeave.Tables[0].Rows.Count > 0)
                            {
                                CreditEL = Convert.ToDouble(dsLeave.Tables[0].Rows[0]["L02_Add"]) + El;

                                StrSql = "update tblleaveledger set " + ELCode + "=" + CreditEL + " where lyear=" + year.ToString("0000") + " and paycode='" + Paycode.ToString() + "' ";
                                Result = Con.execute_NonQuery(StrSql);

                                if (Result > 0)
                                {
                                    StrSql = "update tblCreditLeave set SLCredit='Y',SL='" + El + "' where paycode='" + Paycode.Trim() + "' and LMonth='" + month.ToString() + "' and Lyear=' " + year.ToString() + "'    ";
                                    Con.execute_NonQuery(StrSql);
                                }

                            }
                            else
                            {
                                string Strsql = "Insert into tblleaveledger (paycode,lyear,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13,L14,L15,L16,L17,L18,L19,L20,L01_Add,L02_Add,L03_Add,L04_Add,L05_Add,L06_Add,L07_Add,L08_Add,L09_Add,L10_Add,L11_Add,L12_Add,L13_Add,L14_Add,L15_Add,L16_Add,L17_Add,L18_Add,L19_Add,L20_Add,L01_CASH,L02_CASH,L03_CASH,L04_CASH,L05_CASH,L06_CASH,L07_CASH,L08_CASH,L09_CASH,L10_CASH,L11_CASH,L12_CASH,L13_CASH,L14_CASH,L15_CASH,L16_CASH,L17_CASH,L18_CASH,L19_CASH,L20_CASH,SSN) values('" + Paycode.ToString() + "','" + year.ToString("0000") + "',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'" + ESSN + "')";
                                Con.execute_NonQuery(Strsql);

                                StrSql = "update tblleaveledger set " + ELCode + "=" + El + " where lyear=" + year.ToString("0000") + " and paycode='" + Paycode.ToString() + "' ";
                                Result = Con.execute_NonQuery(StrSql);

                                if (Result > 0)
                                {
                                    StrSql = "update tblCreditLeave set SLCredit='Y',SL='" + El + "' where paycode='" + Paycode.Trim() + "' and LMonth='" + month.ToString() + "' and Lyear=' " + year.ToString() + "'    ";
                                    Con.execute_NonQuery(StrSql);
                                }

                            }
                        }
                        else
                        {
                            StrSql = "select isnull(" + ELCode + ",0) 'L02_Add' from tblleaveledger where lyear=" + year.ToString("0000") + " and paycode='" + Paycode.ToString() + "'";
                            dsLeave = Con.FillDataSet(StrSql);

                            if (dsLeave.Tables[0].Rows.Count > 0)
                            {
                                CreditEL = Convert.ToDouble(dsLeave.Tables[0].Rows[0]["L02_Add"]) + El;



                                StrSql = "update tblleaveledger set " + ELCode + "=" + CreditEL + " where lyear=" + year.ToString("0000") + " and paycode='" + Paycode.ToString() + "' ";
                                Result = Con.execute_NonQuery(StrSql);

                                if (Result > 0)
                                {
                                    StrSql = "update tblCreditLeave set SLCredit='Y',SL='" + El + "' where paycode='" + Paycode.Trim() + "' and LMonth='" + month.ToString() + "' and Lyear=' " + year.ToString() + "'    ";
                                    Con.execute_NonQuery(StrSql);
                                }

                            }
                            else
                            {
                                string Strsql = "Insert into tblleaveledger (paycode,lyear,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13,L14,L15,L16,L17,L18,L19,L20,L01_Add,L02_Add,L03_Add,L04_Add,L05_Add,L06_Add,L07_Add,L08_Add,L09_Add,L10_Add,L11_Add,L12_Add,L13_Add,L14_Add,L15_Add,L16_Add,L17_Add,L18_Add,L19_Add,L20_Add,L01_CASH,L02_CASH,L03_CASH,L04_CASH,L05_CASH,L06_CASH,L07_CASH,L08_CASH,L09_CASH,L10_CASH,L11_CASH,L12_CASH,L13_CASH,L14_CASH,L15_CASH,L16_CASH,L17_CASH,L18_CASH,L19_CASH,L20_CASH,SSN) values('" + Paycode.ToString() + "','" + year.ToString("0000") + "',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'" + ESSN + "')";
                                Con.execute_NonQuery(Strsql);

                                StrSql = "update tblleaveledger set " + ELCode + "=" + El + " where lyear=" + year.ToString("0000") + " and paycode='" + Paycode.ToString() + "' ";
                                Result = Con.execute_NonQuery(StrSql);

                                if (Result > 0)
                                {
                                    StrSql = "update tblCreditLeave set SLCredit='Y',SL='" + El + "' where paycode='" + Paycode.Trim() + "' and LMonth='" + month.ToString() + "' and Lyear=' " + year.ToString() + "'    ";
                                    Con.execute_NonQuery(StrSql);
                                }

                            }

                        }


                    }
                    catch (Exception er)
                    {
                        Error_Occured("For Loop SL", er.Message + "For Code" + Paycode);
                        continue;
                    }
                }
            }



            string Msg = "<script language='javascript'>alert('SL Credited Successfully For This Month.')</script>";
            Page.RegisterStartupScript("Msg", Msg);
        }

        catch (Exception er)
        {
            Error_Occured("Credit SL", er.Message);
        }
    }
    ErrorClass ec = new ErrorClass();
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        //Call The function to write the error log file 

        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        { }
    }
}