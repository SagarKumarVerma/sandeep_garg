﻿using FKWeb;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class OnlineEnrollment : System.Web.UI.Page
{
    SqlConnection msqlConn;
    string mDevId;
    string mUserId;

    const int GET_USER_ID_LIST = 0;
    const int GET_USER_INFO = 1;
    const int SET_USER_INFO = 2;
    const int DEL_USER_INFO = 3;
    const int ALL_DEL = 4;
    const int GET_ALL_USER_INFO = 5;
    const int SET_ALL_USER_INFO = 6;
    const int SET_USER_NAME = 7;
    const int SET_USER_PRIVILEGE = 8;


    const int STATE_WAIT = 0;
    const int STATE_VIEW = 1;
    const int STATE_EDIT = 2;

    const int LIST_OF_DEVICE = 0;
    const int LIST_OF_PC = 1;
    const int LIST_OF_DEVICE_FOR_BATCH = 2;
    Class_Connection Con = new Class_Connection();
    ErrorClass ec = new ErrorClass();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Session["username"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            msqlConn = FKWebTools.GetDBPool();
            GetDeviceHTSeries();
        }
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }

    protected void GetDeviceType()
    {
        try
        {
            if (Session["DeviceModel"].ToString() == null)
            {
                Response.Redirect("Login.aspx");
            }
            if (Session["DeviceModel"].ToString().Trim() == "Z")
            {
                lblDevType.Visible = true;
                radZK.Visible = true;
                radHK.Visible = false;
                radHK.Checked = false;
                radZK.Checked = true;
                GetDeviceZK();
            }
            else if (Session["DeviceModel"].ToString().Trim() == "H")
            {
                radZK.Visible = false;
                lblDevType.Visible = true;
                radHK.Visible = true;
                radHK.Checked = true;
                radZK.Checked = false;
                GetDeviceHTSeries();
            }
            else
            {
                radZK.Visible = false;
                radHK.Visible = false;
                lblDevType.Visible = false;
            }


        }
        catch (Exception Exc)
        {
            Error_Occured("GetDeviceType", Exc.Message);
        }
    }
   
    protected void GetDeviceZK()
    {
        try
        {
            GrdDevice.DataSource = null;
            GrdDevice.DataBind();
            string sSql = "Select ltrim(rtrim(SerialNumber)) 'DevID' from tblmachine Where DeviceMode='ZKT' and CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"'";
            DataSet DsDev = new DataSet();
            DsDev = Con.FillDataSet(sSql);
            if (DsDev.Tables[0].Rows.Count > 0)
            {
                GrdDevice.DataSource = DsDev;
                GrdDevice.TextField = "DevID";
                GrdDevice.ValueField = "DevID";
                GrdDevice.DataBind();
                GrdDevice.SelectedIndex = -1;
            }
            else
            {
                GrdDevice.DataSource = null;
                GrdDevice.DataBind();
                GrdDevice.SelectedIndex = -1;
            }


        }
        catch (Exception Exc)
        {
            Error_Occured("GetDeviceZK", Exc.Message);
        }
    }
    protected void GetDeviceHTSeries()
    {
        try
        {
            GrdDevice.DataSource = null;
            GrdDevice.DataBind();
            string sSql = "Select ltrim(rtrim(SerialNumber)) 'DevID',DeviceName 'Name' from tblmachine Where DeviceMode='HTSeries' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
            DataSet DsDev = new DataSet();
            DsDev = Con.FillDataSet(sSql);
            if (DsDev.Tables[0].Rows.Count > 0)
            {

                GrdDevice.DataSource = DsDev;
                GrdDevice.TextField = "Name";
                GrdDevice.ValueField = "DevID";
                GrdDevice.DataBind();
                GrdDevice.SelectedIndex = -1;

            }
            else
            {
                GrdDevice.DataSource = null;
                GrdDevice.DataBind();
                GrdDevice.SelectedIndex = -1;

            }


        }
        catch (Exception Exc)
        {
            Error_Occured("GetDeviceHTSeries", Exc.Message);
        }
    }
    protected void radZK_CheckedChanged(object sender, EventArgs e)
    {
        if (radZK.Checked)
        {
            GetDeviceZK();
        }
        else
        {
            GetDeviceHTSeries();
        }
    }

    protected void btnUploadOffline_Click(object sender, EventArgs e)
    {

        try
        {
            #region Device Commands begins
            if (GrdDevice.SelectedIndex == -1)
            {
                lblStatus.Text = "Please Select Any Device";
                txtuserID.Text = "";
                UserName.Text = "";
                CardNum.Text = "";
                Password.Text = "";
                return;
            }
            if (GrdDevice.SelectedItem.Text.ToString().Trim() == "NONE" || GrdDevice.SelectedItem.Text.ToString().Trim() == "" || GrdDevice.SelectedItem.Text.ToString().Trim() == string.Empty)
            {
                lblStatus.Text = "Please Select Any Device";
                txtuserID.Text = "";
                UserName.Text = "";
                CardNum.Text = "";
                Password.Text = "";
                return;
            }
            DateTime AccessFrom = System.DateTime.Today;
            DateTime AccessTo = System.DateTime.Today.AddYears(1);
            if (radZK.Checked)
            {
                if (radFP.Checked == true)
                {
                    string dsql = "";
                    string TempCard = "";
                    try
                    {
                        double TempCardP = Convert.ToDouble(txtuserID.Text.ToString().Trim());
                        TempCard = TempCardP.ToString();
                    }
                    catch
                    {
                        TempCard = txtuserID.Text.ToString().Trim();
                    }
                    dsql = "insert into UserDetail(DeviceID,UserID,Name,Pri,Password,DeviceGroup,TimeZone,Verify) values ('" + GrdDevice.SelectedItem.Value.ToString().Trim() + "','" + TempCard.Trim().ToString() + "','" + UserName.Text.ToString().Trim().ToUpper() + "','0','','1','0001000100000000','')";
                    Con.execute_NonQuery(dsql);

                    dsql = "insert into DeviceCommands(SerialNumber,CommandContent,TransferToDevice,UserID,Executed,IsOnline,CreatedOn,Priority ) values ('" + GrdDevice.SelectedItem.Value.ToString().Trim() + "','flagupduserinfo','" + GrdDevice.SelectedItem.Value.ToString().Trim() + "','" + TempCard.ToString() + "',0,1,getdate(),'1')";
                    Con.execute_NonQuery(dsql);

                    dsql = "insert into DeviceCommands(SerialNumber,CommandContent,TransferToDevice,UserID,Executed,IsOnline,CreatedOn,Priority) values ('" + GrdDevice.SelectedItem.Value.ToString().Trim() + "','flagremoteenroll','" + GrdDevice.SelectedItem.Value.ToString().Trim() + "','" + TempCard.ToString() + "',0,1,getdate(),'1')";
                    Con.execute_NonQuery(dsql);
                }
                else if (radFace.Checked == true)
                {
                    string dsql = "";

                    string TempCard = "";
                    try
                    {
                        double TempCardP = Convert.ToDouble(txtuserID.Text.ToString().Trim());
                        TempCard = TempCardP.ToString();
                    }
                    catch
                    {
                        TempCard = txtuserID.Text.ToString().Trim();
                    }
                    dsql = "Select * from UserDetail where DeviceID='" + GrdDevice.SelectedItem.Value.ToString().Trim() + "' and UserID ='" + TempCard.Trim().ToString() + "' ";
                    DataSet DsTemp = new DataSet();
                    DsTemp = Con.FillDataSet(dsql);
                    if (DsTemp.Tables[0].Rows.Count==0)
                    {
                        dsql = "insert into UserDetail(DeviceID,UserID,Name,Pri,Password,DeviceGroup,TimeZone,Verify,AccesstimeFrom,AccessTimeTo) values ('" + GrdDevice.SelectedItem.Value.ToString().Trim() + "','" + TempCard.Trim().ToString() + "','" + UserName.Text.ToString().Trim().ToUpper() + "','0','','1','0001000100000000','','" + AccessFrom.ToString("yyyy-MM-dd") + "','" + AccessTo.ToString("yyyy-MM-dd") + "')";
                        Con.execute_NonQuery(dsql);
                    }
                    dsql = "insert into DeviceCommands(SerialNumber,CommandContent,TransferToDevice,UserID,Executed,IsOnline,CreatedOn ) values ('" + GrdDevice.SelectedItem.Value.ToString().Trim() + "','flagupduserinfo','" + GrdDevice.SelectedItem.Value.ToString().Trim() + "','" + TempCard.ToString() + "',0,1,getdate())";
                    Con.execute_NonQuery(dsql);

                    dsql = "insert into DeviceCommands(SerialNumber,CommandContent,TransferToDevice,UserID,Executed,IsOnline,CreatedOn) values ('" + GrdDevice.SelectedItem.Value.ToString().Trim() + "','flagremoteenrollFace','" + GrdDevice.SelectedItem.Value.ToString().Trim() + "','" + TempCard.ToString() + "',0,1,getdate())";
                    Con.execute_NonQuery(dsql);
                }
                else
                {

                }
                txtuserID.Text = "";
                UserName.Text = "";
                CardNum.Text = "";
                Password.Text = "";

            }
            else
            {
                if (radFP.Checked == true)
                {
                    string dsql = "";
                    string TempCard = "";
                    try
                    {
                        double TempCardP = Convert.ToDouble(txtuserID.Text.ToString().Trim());
                        TempCard = TempCardP.ToString();
                    }
                    catch
                    {
                        TempCard = txtuserID.Text.ToString().Trim();
                    }
                    dsql = "insert into UserDetail(DeviceID,UserID,Name,Pri,Password,DeviceGroup,TimeZone,Verify) values ('" + GrdDevice.SelectedItem.Value.ToString().Trim() + "','" + TempCard.Trim().ToString() + "','" + UserName.Text.ToString().Trim().ToUpper() + "','0','','1','0001000100000000','')";
                    Con.execute_NonQuery(dsql);

                    //dsql = "insert into DeviceCommands(SerialNumber,CommandContent,TransferToDevice,UserID,Executed,IsOnline,CreatedOn ) values ('" + GrdDevice.SelectedItem.Value.ToString().Trim() + "','flagupduserinfo','" + GrdDevice.SelectedItem.Value.ToString().Trim() + "','" + TempCard.ToString() + "',0,1,getdate())";
                    //Con.execute_NonQuery(dsql);

                    dsql = "insert into DeviceCommands(SerialNumber,CommandContent,TransferToDevice,UserID,Executed,IsOnline,CreatedOn,Priority) values ('" + GrdDevice.SelectedItem.Value.ToString().Trim() + "','flagremoteenroll','" + GrdDevice.SelectedItem.Value.ToString().Trim() + "','" + TempCard.ToString() + "',0,1,getdate(),'1')";
                    Con.execute_NonQuery(dsql);
                }
                else if (radFace.Checked == true)
                {
                    string dsql = "";

                    string TempCard = "";
                    try
                    {
                        double TempCardP = Convert.ToDouble(txtuserID.Text.ToString().Trim());
                        TempCard = TempCardP.ToString();
                    }
                    catch
                    {
                        TempCard = txtuserID.Text.ToString().Trim();
                    }
                    dsql = "insert into UserDetail(DeviceID,UserID,Name,Pri,Password,DeviceGroup,TimeZone,Verify,AccesstimeFrom,AccessTimeTo) values ('" + GrdDevice.SelectedItem.Value.ToString().Trim() + "','" + TempCard.Trim().ToString() + "','" + UserName.Text.ToString().Trim().ToUpper() + "','0','','1','0001000100000000','','" + AccessFrom.ToString("yyyy-MM-dd") + "','" + AccessTo.ToString("yyyy-MM-dd") + "')";
                    Con.execute_NonQuery(dsql);

                    //dsql = "insert into DeviceCommands(SerialNumber,CommandContent,TransferToDevice,UserID,Executed,IsOnline,CreatedOn ) values ('" + GrdDevice.SelectedItem.Text.ToString().Trim() + "','flagupduserinfo','" + GrdDevice.SelectedItem.Text.ToString().Trim() + "','" + TempCard.ToString() + "',0,1,getdate())";
                    //Con.execute_NonQuery(dsql);

                    dsql = "insert into DeviceCommands(SerialNumber,CommandContent,TransferToDevice,UserID,Executed,IsOnline,CreatedOn,Priority) values ('" + GrdDevice.SelectedItem.Value.ToString().Trim() + "','flagremoteenrollFace','" + GrdDevice.SelectedItem.Value.ToString().Trim() + "','" + TempCard.ToString() + "',0,1,getdate(),'1')";
                    Con.execute_NonQuery(dsql);
                }
                else
                {

                }
                txtuserID.Text = "";
                UserName.Text = "";
                CardNum.Text = "";
                Password.Text = "";
            }


            #endregion
            ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Command Sent Successfully');document.location='OnlineEnrollment.aspx'", true);
        }
        catch (Exception Exc)
        {
            txtuserID.Text = "";
            UserName.Text = "";
            CardNum.Text = "";
            Password.Text = "";
            Error_Occured("btnUploadOffline_Click", Exc.Message);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Error');document.location='OnlineEnrollment.aspx'", true);
        }
    }

}