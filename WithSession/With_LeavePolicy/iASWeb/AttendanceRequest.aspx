﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="AttendanceRequest.aspx.cs" Inherits="AttendanceRequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {
  box-sizing: border-box;
}

input[type=text], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  resize: vertical;
}

label {
  padding: 12px 12px 12px 0;
  display: inline-block;
}

input[type=button] {
  background-color: #4CAF50;
  color: white;
  padding: 12px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  float: right;
}

input[type=submit]:hover {
  background-color: #45a049;
}

.container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}

.col-25 {
  float: left;
  width: 25%;
  margin-top: 6px;
}

.col-75 {
  float: left;
  width: 75%;
  margin-top: 6px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .col-25, .col-75, input[type=submit] {
    width: 100%;
    margin-top: 0;
  }
}
</style>

    <div class="container">
  
    <div class="row">
      <div class="col-25">
        <label for="fname">Employee Code:</label>
      </div>
      <div class="col-75">
      <%--  <input type="text" id="fname" name="firstname" placeholder="Your name..">--%>
          <dx:ASPxComboBox ID="ddlPayCode" runat="server" ValueType="System.String" OnSelectedIndexChanged="ddlPayCode_SelectedIndexChanged" AutoPostBack="true">
              <ValidationSettings ValidationGroup="MK">
                  <RequiredField ErrorText="Input Employee Code*" IsRequired="True" />
              </ValidationSettings>
          </dx:ASPxComboBox>
      </div>
    </div>
    <div class="row">
      <div class="col-25">
        <label for="lname">Employee Name:</label>
      </div>
      <div class="col-75">
          <dx:ASPxLabel ID="lblName" runat="server" Text=""></dx:ASPxLabel>
       
      </div>
    </div>
    <div class="row">
      <div class="col-25">
        <label for="country">Department</label>
      </div>
      <div class="col-75">
         <dx:ASPxLabel ID="lblDepartment" runat="server" Text=""></dx:ASPxLabel>
      </div>
    </div>
            <div class="row">
      <div class="col-25">
        <label for="country">Punch Date:</label>
      </div>
      <div class="col-75">
                                    

          <dx:ASPxDateEdit ID="txtDate" runat="server"  Theme="SoftOrange" EditFormatString="dd/MM/yyyy" >
              <ValidationSettings ValidationGroup="MK">
                  <RequiredField ErrorText="Input Date*" IsRequired="True" />
              </ValidationSettings>
          </dx:ASPxDateEdit>
      </div>
    </div>
                  <div class="row">
      <div class="col-25">
        <label for="country">Punch Time:</label>
      </div>
      <div class="col-75">
           <dx:ASPxTextBox ID="txtTime" runat="server" Width="50px">
                                        <MaskSettings Mask="HH:mm" />
                <ValidationSettings ValidationGroup="MK">
                  <RequiredField ErrorText="Input Time*" IsRequired="True" />
              </ValidationSettings>
                                    </dx:ASPxTextBox>

        

      </div>
    </div>
    <div class="row">
      <div class="col-25">
        <label for="subject">Remarks</label>
      </div>
      <div class="col-75">
       <%-- <textarea id="subject" name="subject" placeholder="Write Something.." style="height:100px"></textarea>--%>
          <dx:ASPxMemo ID="userRemarks" runat="server" Height="100px" Width="300px">
              <ValidationSettings ValidationGroup="MK">
                  <RequiredField ErrorText="Input Remarks*" IsRequired="True" />
              </ValidationSettings>
          </dx:ASPxMemo>
      </div>
    </div>
    <div class="row">
      <dx:ASPxButton ID="btnSave" runat="server" Text="Request" ValidationGroup="MK" OnClick="btnSave_Click"></dx:ASPxButton>
    </div>
         <div class="row">
     <h4> Request Details </h4>
    </div>
        <div class="row">
            <dx:ASPxGridView ID="grdAtt" runat="server" Width="100%" AutoGenerateColumns="False">
                <SettingsPager Visible="False">
                </SettingsPager>
                <SettingsCommandButton>
                    <DeleteButton Text=" ">
                        <Image IconID="actions_trash_16x16">
                        </Image>
                    </DeleteButton>
                </SettingsCommandButton>
                <SettingsDataSecurity AllowEdit="False" AllowInsert="False" />
                <Columns>
                    <dx:GridViewCommandColumn Caption=" " ShowInCustomizationForm="True" VisibleIndex="0">
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn Caption="Request ID" FieldName="application_no" ShowInCustomizationForm="True" VisibleIndex="1">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Request Date" FieldName="REQUEST DATE" ShowInCustomizationForm="True" VisibleIndex="2">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Punch Time" FieldName="Punch Time" ShowInCustomizationForm="True" VisibleIndex="3">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Remarks" FieldName="Remarks" ShowInCustomizationForm="True" VisibleIndex="4">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Stage 1 Status" FieldName="Stage1_Status" ShowInCustomizationForm="True" VisibleIndex="5">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Remarks" FieldName="Stage1_Remarks" ShowInCustomizationForm="True" VisibleIndex="6">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Stage 2 Status" FieldName="Stage2_Status" ShowInCustomizationForm="True" VisibleIndex="7">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Stage 2 Remarks" FieldName="Stage2_Remarks" ShowInCustomizationForm="True" VisibleIndex="8">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Approval/Rejection Date" FieldName="Approval/Rejection Date" ShowInCustomizationForm="True" VisibleIndex="9">
                    </dx:GridViewDataTextColumn>
                </Columns>
            </dx:ASPxGridView>
      
    </div>
  
</div>

</asp:Content>

