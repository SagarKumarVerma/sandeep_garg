﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using GlobalSettings;
using System.Collections.Generic;
using DevExpress.Web;
using System.Text.RegularExpressions;

public partial class frmLeaveMaster : System.Web.UI.Page

{
    Class_Connection Con = new Class_Connection();
    string Strsql = null;
    DataSet ds = null;
    int result = 0;
    ErrorClass ec = new ErrorClass();
    string DName, DHOD, DEmail;
    bool isNewRecord = false;

    string LeaveField = "";


    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            if (Session["LeaveMasterVisible"] == null)
            {
                Response.Redirect("Login.aspx");
            }

            BindData();
        }

    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }

    protected void BindData()
    {
        try
        {
            if (Session["LoginUserName"].ToString().Trim().Trim().ToUpper() == "ADMIN")
            {
                Strsql = " select Leavefield,leavecode,leavedescription,case ShowOnWeb when 'Y' then 'Yes' else 'No' End as 'Show on Web', case IsLeaveAccrual when " +
                         " 'Y' then 'Yes' else 'No' End as 'Is Accrual', case ISOFFINCLUDE when 'Y' then 'Yes' else 'No' end 'Off Include', " +
                         " case ISHOLIDAYINCLUDE when 'Y' then 'Yes' else 'No' end 'Holiday Include',LeaveType  from tblLeaveMaster";
            }
            else
            {
                Strsql = " select Leavefield,leavecode,leavedescription,case ShowOnWeb when 'Y' then 'Yes' else 'No' End as 'Show on Web', case IsLeaveAccrual when " +
                        " 'Y' then 'Yes' else 'No' End as 'Is Accrual', case ISOFFINCLUDE when 'Y' then 'Yes' else 'No' end 'Off Include', " +
                        " case ISHOLIDAYINCLUDE when 'Y' then 'Yes' else 'No' end 'Holiday Include',LeaveType  from tblLeaveMaster Where CompanyCode='"+Session["LoginCompany"].ToString().Trim().Trim()+"'";
            }

            ds = new DataSet();
            ds = Con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                grdLeave.DataSource = ds.Tables[0];
                grdLeave.DataBind();
            }
        }
        catch (Exception E)
        {
            Error_Occured("BindData", E.Message);
        }


    }
    protected void Page_Init(object sender, EventArgs e)
    {
        BindData();
        grdLeave.DataBind();
    }
 
    protected void grdLeave_PageIndexChanged(object sender, EventArgs e)
    {
        int pageIndex = (sender as ASPxGridView).PageIndex;
        grdLeave.PageIndex = pageIndex;
        this.BindData();  
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("frmAddLeave.aspx");
    }
    protected void btnDel_Click(object sender, EventArgs e)
    {
        try
        {
            List<object> keys = grdLeave.GetSelectedFieldValues(new string[] { grdLeave.KeyFieldName });
            if (keys.Count == 0)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Please Select Atleast Record To Delete')</script>");
                return;
            }
            for (int i = 0; i < grdLeave.VisibleRowCount; i++)
            {
                if (grdLeave.Selection.IsRowSelected(i) == true)
                {
                    string Leavefield = grdLeave.GetRowValues(i, "Leavefield").ToString().Trim();
                    string LeaveCode = grdLeave.GetRowValues(i, "leavecode").ToString().Trim();
                    try
                    {
                        bool IsDelete = IsValid(LeaveCode);
                        if(IsDelete==true)
                        {
                            string sSql = "delete from tblleavemaster  where CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' and LEAVEFIELD='" + Leavefield.Trim() + "'";
                            Con.execute_NonQuery(sSql);
                        }
                        ec.InsertLog("Leave Record Deleted", LeaveCode.Trim().ToUpper(), LeaveCode.Trim().ToUpper(), Session["LoginUserName"].ToString().Trim(), Session["LoginCompany"].ToString().Trim().Trim());
                    
                    }
                    catch
                    {

                    }
                }
            }

        }
        catch
        {

        }
    }


    private bool IsValid(string LeaveCode)
    {
        bool IsValid = false;
        string sSql;
        DataSet Dsleave = new DataSet();
        try
        {
            sSql = "select * from leave_request where leavecode='" + LeaveCode + "' and paycode in (select paycode from tblemployee where companycode='" + Session["LoginCompany"].ToString().Trim() + "')";
            Dsleave = Con.FillDataSet(sSql);
            if (Dsleave.Tables[0].Rows.Count>0)
            {
                IsValid = false;
            }
            else
            {
                IsValid = true;
            }

        }
        catch
        {
            IsValid = false;
            return IsValid;
        }





        return IsValid;
    }

    protected void grdLeave_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
    {
        ((ASPxGridView)sender).JSProperties["cp_isSuccess"] = false;
    }

    protected void grdLeave_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
    {
        ((ASPxGridView)sender).JSProperties["cp_isSuccess"] = false;
    }

    protected void grdLeave_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
    {
        ((ASPxGridView)sender).JSProperties["cp_isSuccess"] = false;
    }
}