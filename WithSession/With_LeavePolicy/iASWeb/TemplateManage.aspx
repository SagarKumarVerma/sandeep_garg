﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="TemplateManage.aspx.cs" Inherits="TemplateManage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <div>

        <table style="width: 100%">
            <tr>
                <td colspan="2"><dx:ASPxTextBox ID="mTransIdTxt" runat="server" Visible="false" Width="170px">
                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td style="height: 20px"></td>
               
                <td style="height: 20px">
                    <dx:ASPxCheckBox ID="chkAdmin" runat="server" Height="19px" Text="Make Admin" Theme="SoftOrange">
                    </dx:ASPxCheckBox>
                    <dx:ASPxButton ID="ASPxButton1" runat="server"  Text="Upload" OnClick="ASPxButton1_Click" Theme="SoftOrange">
                        <Image IconID="miscellaneous_publish_16x16">
                        </Image>
                    </dx:ASPxButton>
                    <dx:ASPxButton ID="ASPxButton2" runat="server"  Text="Delete" Theme="SoftOrange" OnClick="ASPxButton2_Click">
                        <Image IconID="actions_trash_16x16">
                        </Image>
                    </dx:ASPxButton>
                </td>
            </tr>
            <tr>
                 <td style="height: 20px" valign="Top">
                    <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" EnableTheming="True" Font-Size="Small" KeyFieldName="user_id" OnHtmlDataCellPrepared="ASPxGridView1_HtmlDataCellPrepared" OnPageIndexChanged="ASPxGridView1_PageIndexChanged" OnProcessColumnAutoFilter="ASPxGridView1_ProcessColumnAutoFilter" Theme="SoftOrange" Width="100%">
                        <SettingsPager EnableAdaptivity="True" PageSize="1000">
                        </SettingsPager>
                        <Settings ShowFilterRow="True" />
                        <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                        <Columns>
                            <dx:GridViewCommandColumn Caption=" " SelectAllCheckboxMode="Page" ShowClearFilterButton="True" ShowInCustomizationForm="True" ShowSelectCheckbox="True" VisibleIndex="0" Width="8%">
                            </dx:GridViewCommandColumn>
                            <dx:GridViewDataTextColumn Caption="User ID" FieldName="user_id" ShowInCustomizationForm="True" VisibleIndex="1">
                                <Settings ShowFilterRowMenuLikeItem="True" />
                                <SettingsHeaderFilter Mode="CheckedList">
                                </SettingsHeaderFilter>
                                <CellStyle ForeColor="Blue">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="User Name" FieldName="empname" ShowInCustomizationForm="True" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Enroll Mode" FieldName="TemplateType" ShowInCustomizationForm="True" VisibleIndex="3">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Device" FieldName="Device_ID" ShowInCustomizationForm="True" VisibleIndex="4">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:ASPxGridView>
                </td>
               
                <td style="height: 20px" valign="Top">
                    <dx:ASPxGridView ID="ASPxGridView2" runat="server" AutoGenerateColumns="False" EnableTheming="True" Font-Size="Small" KeyFieldName="device_id" OnPageIndexChanged="ASPxGridView2_PageIndexChanged" OnProcessColumnAutoFilter="ASPxGridView2_ProcessColumnAutoFilter" Theme="SoftOrange" Width="100%">
                        <SettingsPager PageSize="50">
                        </SettingsPager>
                        <Settings ShowFilterRow="True" />
                        <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                        <Columns>
                            <dx:GridViewCommandColumn Caption=" " SelectAllCheckboxMode="Page" ShowClearFilterButton="True" ShowInCustomizationForm="True" ShowSelectCheckbox="True" VisibleIndex="0" Width="8%">
                            </dx:GridViewCommandColumn>
                            <dx:GridViewDataTextColumn Caption="Device ID" FieldName="device_id" ShowInCustomizationForm="True" VisibleIndex="1">
                                <CellStyle ForeColor="Blue">
                                </CellStyle>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Device Name" FieldName="device_name" ShowInCustomizationForm="True" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Group" FieldName="GroupID" ShowInCustomizationForm="True" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:ASPxGridView>
                </td>
            </tr>
        </table>

    </div>
</asp:Content>

