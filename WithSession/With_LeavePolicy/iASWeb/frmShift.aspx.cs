﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
//using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using GlobalSettings;
using System.Collections.Generic;
using DevExpress.Web;

public partial class frmShift : System.Web.UI.Page
{
    Class_Connection Con = new Class_Connection();
    string Strsql = null;
    DataSet ds = null;
    int result = 0;
    ErrorClass ec = new ErrorClass();
    string DName, DHOD, DEmail;
    bool isNewRecord = false;


    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            if (Session["ShiftVisible"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            BindData();
        }
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }

    protected void BindData()
    {
        if (Session["LoginCompany"].ToString().Trim().ToUpper() == "ADMIN")
        {
            Strsql = "select Shift,ShiftDiscription,convert(char(5),starttime, 108) 'Start Time', convert(char(5),endtime, 108) 'End Time',convert(char(5),lunchtime, 108) 'Lunch Time' " +
                 ",convert(char(5),dateadd(n,lunchduration,0),108) 'lunchduration',convert(char(5),lunchendtime, 108) 'LunchEnd Time',convert(char(5),dateadd(n,OTDEDUCTHRS,0),108)'OTDEDUCTHRS',convert(char(5),dateadd(n,OTStartafter,0),108) 'OTStartafter',convert(char(5),dateadd(n,LUNCHDEDUCTION,0),108) 'LUNCHDEDUCTION',SHIFTPOSITION,convert(char(5),dateadd(n,shiftduration,0),108) 'shiftduration',convert(char(5),dateadd(n,otdeductafter,0),108) 'otdeductafter' " +
                 " from  TblShiftMaster ";
        }
        else
        {
            Strsql = "select Shift,ShiftDiscription,convert(char(5),starttime, 108) 'Start Time', convert(char(5),endtime, 108) 'End Time',convert(char(5),lunchtime, 108) 'Lunch Time' " +
                  ",convert(char(5),dateadd(n,lunchduration,0),108) 'lunchduration',convert(char(5),lunchendtime, 108) 'LunchEnd Time',convert(char(5),dateadd(n,OTDEDUCTHRS,0),108)'OTDEDUCTHRS',convert(char(5),dateadd(n,OTStartafter,0),108) 'OTStartafter',convert(char(5),dateadd(n,LUNCHDEDUCTION,0),108) 'LUNCHDEDUCTION',SHIFTPOSITION,convert(char(5),dateadd(n,shiftduration,0),108) 'shiftduration',convert(char(5),dateadd(n,otdeductafter,0),108) 'otdeductafter' " +
                  " from  TblShiftMaster where CompanyCode='" + Session["LoginCompany"].ToString()  + "'";
        }

      
     
        ds = new DataSet();
        ds = Con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            grdShift.DataSource = ds.Tables[0];
            grdShift.DataBind();
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        BindData();
        grdShift.DataBind();
    }

    protected void grdShift_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
    {
        ((ASPxGridView)sender).JSProperties["cp_isSuccess"] = false;
        //pcLogin.ShowOnPageLoad = true;
        //e.Cancel = true;
    }
    protected void grdShift_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        ((ASPxGridView)sender).JSProperties["cp_isSuccess"] = false;
        //ASPxPopupControl popup = grdShift.FindEditFormTemplateControl("pcLogin") as ASPxPopupControl;
        //ASPxMemo memo = popup.FindControl("ASPxMemo1") as ASPxMemo;
        //e.NewValues["Notes"] = memo.Text;
        //throw new InvalidOperationException("Data modifications are not allowed in the online demo");
    }
    protected void grdShift_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
    {
        ((ASPxGridView)sender).JSProperties["cp_isSuccess"] = false;
        //pcLogin.ShowOnPageLoad = true;
    }
    protected void grdShift_ContextMenuItemClick(object sender, ASPxGridViewContextMenuItemClickEventArgs e)
    {
        switch (e.Item.Name)
        {
            case "Add":
                //GridExporter.WritePdfToResponse();
                break;
            case "Edit":
                //GridExporter.WriteXlsToResponse();
                break;
        }
    }
    protected void grdShift_ToolbarItemClick(object source, DevExpress.Web.Data.ASPxGridViewToolbarItemClickEventArgs e)
    {
        string test = e.Item.Name;
        string z = test;
    }
    protected void grdShift_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        try
        {
            string IsMaster = Convert.ToString(e.Keys["Shift"]);
            bool Validate = false;
            Validate = ValidData(IsMaster);
            if (Validate == true)
            {

                ((ASPxGridView)sender).JSProperties["cp_isSuccess"] = true;
                grdShift.CancelEdit();
                BindData();
                //grdShift.CancelEdit();
                //e.Cancel = true;
                //BindData();
                //return;

            }
            else
            {
                Strsql = "Delete From tblshiftmaster where SHIFT='" + IsMaster.Trim() + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'  ";
                Con.execute_NonQuery(Strsql);
            }
            e.Cancel = true;
            grdShift.CancelEdit();

            BindData();
        }
        catch (Exception Ex)
        {

            Error_Occured("GrdComp_RowDeleting", Ex.Message);
        }
    }
    private bool ValidData(string CCode)
    {
        bool Check = false;
        try
        {
            // Strsql = "Select * from tblemployeeshiftmaster where SHIFT='" + CCode + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' ";
            Strsql = "Select * from tbltimeregister where ltrim(rtrim(SHIFTattended))='" + CCode.Trim() + "' or ltrim(rtrim(SHIFT))='" + CCode.Trim() + "' and ssn like '" + Session["LoginCompany"].ToString().Trim() + "_%' ";
            DataSet DsCheck = new DataSet();
            DsCheck = Con.FillDataSet(Strsql);
            if (DsCheck.Tables[0].Rows.Count > 0)
            {
                Check = true;

            }

        }
        catch (Exception Ex)
        {
            Check = true;
            Error_Occured("ValidData", Ex.Message);
        }

        return Check;
    }
}