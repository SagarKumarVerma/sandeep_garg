﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using GlobalSettings;
using System.Collections.Generic;
using DevExpress.Web;
public partial class frmCategory : System.Web.UI.Page
{
    Class_Connection Con = new Class_Connection();
    string Strsql = null;
    DataSet ds = null;
    int result = 0;
    ErrorClass ec = new ErrorClass();
    bool isNewRecord = false;
    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    string Cat, CatName, IsLate, EveryInterval, Deductfrom, Leave1, Leave2;
    double LayDays = 0;
    double MaxLateDur = 0;
    double DeductDays = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            if (Session["CategoryVisible"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            BindData();
        }
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }

    protected void BindData()
    {
        try
        {
            if (Session["LoginUserName"].ToString().Trim().ToUpper() == "ADMIN")
            {
                Strsql = "Select * from tblCatagory order by 1";
            }
            else
            {
                Strsql = "Select * from tblCatagory where CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' order by 1";
            }
            
            ds = new DataSet();
            ds = Con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                GrdCat.DataSource = ds.Tables[0];
                GrdCat.DataBind();
            }
        }
        catch (Exception Ex)
        {
            Error_Occured("BindData", Ex.Message);
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        BindData();
        GrdCat.DataBind();
    }
    private void AddError(Dictionary<GridViewColumn, string> errors, GridViewColumn column, string errorText)
    {
        if (!errors.ContainsKey(column))
            errors[column] = errorText;
    }


    protected void GrdCat_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
    {
        string CCode = Convert.ToString(e.NewValues["CAT"]);
        string CName = Convert.ToString(e.NewValues["CATAGORYNAME"]);
        // string k = e.Keys["COMPANYCODE"].ToString();
        if (CCode.ToString().Trim() == string.Empty)
        {
            AddError(e.Errors, GrdCat.Columns[0], "Input Category Code");

            e.RowError = "*Input Category Code";
        }
        if (CName.ToString().Trim() == string.Empty)
        {
            AddError(e.Errors, GrdCat.Columns[0], "Input Category Name");
            e.RowError = "*Input Category Name";
        }
        if (isNewRecord == true)
        {
            Strsql = "Select * from tblCatagory where CAT='" + CCode + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'  ";
            DataSet DsIP = new DataSet();
            DsIP = Con.FillDataSet(Strsql);
            if (DsIP.Tables[0].Rows.Count > 0)
            {
                AddError(e.Errors, GrdCat.Columns[0], "Duplicate Category Code");
                e.RowError = "*Duplicate Category Code";
            }
        }
        

    }
    protected void GrdCat_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.Grid.IsNewRowEditing == false)
        {
            if (e.Column.FieldName == "CAT")
            {
                e.Editor.ReadOnly = true;

            }
            else
            {
                e.Editor.ReadOnly = false;
            }
        }
        else
        {
            isNewRecord = true;
            e.Editor.ReadOnly = false;
        }
    }
    protected void GrdCat_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        try
        {
            string IsMaster = Convert.ToString(e.Keys["CAT"]);
            bool Validate = false;
            Validate = ValidData(IsMaster);
            if (Validate == true)
            {
                //ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Company Already Assigned To Employee')</script>");
                ((ASPxGridView)sender).JSProperties["cp_isSuccess"] = true;
                GrdCat.CancelEdit();
               // e.Cancel = true;
                BindData();
               // return;

            }
            else
            {
                Strsql = "Delete From tblCatagory where CAT='" + IsMaster.Trim() + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' ";
                Con.execute_NonQuery(Strsql);
            }

            GrdCat.CancelEdit();
            e.Cancel = true;
            BindData();
            DataFilter.LoadCat(Session["LoginCompany"].ToString().Trim());
        }
        catch
        {

        }
    }
    private bool ValidData(string CCode)
    {
        bool Check = false;
        try
        {
            Strsql = "Select * from tblemployee where CAT='" + CCode + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'  ";
            DataSet DsCheck = new DataSet();
            DsCheck = Con.FillDataSet(Strsql);
            if (DsCheck.Tables[0].Rows.Count > 0)
            {
                Check = true;

            }

        }
        catch (Exception Ex)
        {
            Error_Occured("ValidData", Ex.Message);
        }

        return Check;
    }
    protected void GrdCat_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        try
        {
            Cat = e.NewValues["CAT"].ToString();
            CatName = e.NewValues["CATAGORYNAME"].ToString();

            Strsql = "Insert Into tblCatagory(CAT,CATAGORYNAME,CompanyCode) values('" + Cat.Trim() + "','" + CatName.Trim() + "','" + Session["LoginCompany"].ToString().Trim() + "') ";
            Con.execute_NonQuery(Strsql);
            e.Cancel = true;
            GrdCat.CancelEdit();
            BindData();
            DataFilter.LoadCat(Session["LoginCompany"].ToString().Trim());
        }
        catch
        {

        }

    }
    protected void GrdCat_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        try
        {

           
            Cat = e.Keys["CAT"].ToString();
            CatName = e.NewValues["CATAGORYNAME"].ToString();



            Strsql = "update tblCatagory set CATAGORYNAME='" + CatName.Trim() + "' where CAT='" + Cat.Trim() + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' ";
            Con.execute_NonQuery(Strsql);
            e.Cancel = true;
            GrdCat.CancelEdit();
            BindData();
            DataFilter.LoadCat(Session["LoginCompany"].ToString().Trim());
        }
        catch (Exception Ex)
        {

        }
    }
    protected void GrdCat_PageIndexChanged(object sender, EventArgs e)
    {
        int pageIndex = (sender as ASPxGridView).PageIndex;
        GrdCat.PageIndex = pageIndex;
        this.BindData();
    }

    protected void GrdCat_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
    {
        ((ASPxGridView)sender).JSProperties["cp_isSuccess"] = false;
    }

    protected void GrdCat_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
    {
        ((ASPxGridView)sender).JSProperties["cp_isSuccess"] = false;
    }

    protected void GrdCat_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
    {
        ((ASPxGridView)sender).JSProperties["cp_isSuccess"] = false;
    }
}