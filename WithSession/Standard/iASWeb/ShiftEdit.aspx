﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ShiftEdit.aspx.cs" Inherits="ShiftEdit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <script type="text/javascript">  
        function State_OnKeyUp(s, e) {  
            s.SetText(s.GetText().toUpperCase().trim());            
        }
        function LoadShiftScript()
        {
            //alert(window.parent.document.getElementById('MainPane_Content_MainContent_HdnRowId').value);
            if (window.parent.document.getElementById('MainPane_Content_MainContent_HdnRowId').value == "") {
                document.getElementById('ASPxRoundPanel1_txtShiftCode_I').disabled = false;
                document.getElementById('ASPxRoundPanel1_txtShiftCode_I').focus();
                document.getElementById('ASPxRoundPanel1_txtShiftCode_I').value = "";
                document.getElementById('ASPxRoundPanel1_txtStartTime_I').value = "00:00";
                document.getElementById('ASPxRoundPanel1_txtEndTime_I').value = "00:00";
                document.getElementById('ASPxRoundPanel1_txtLunchTime_I').value = "00:00";
                document.getElementById('ASPxRoundPanel1_txtLunchDuration_I').value = "00:00";
                document.getElementById('ASPxRoundPanel1_txtLunchEndTime_I').value = "00:00";
                document.getElementById('ASPxRoundPanel1_txtShiftHours_I').value = "00:00";
                document.getElementById('ASPxRoundPanel1_txtOTStarts_I').value = "00:00";
                document.getElementById('ASPxRoundPanel1_txtOTDeductAfter_I').value = "00:00";
                document.getElementById('ASPxRoundPanel1_txtOTDeduction_I').value = "00:00";
                document.getElementById('ASPxRoundPanel1_txtLunchDeduction_I').value = "00:00";
                document.getElementById('ASPxRoundPanel1_ddlShift_I').value = "Day";
            }
            else {
               // alert(LoginCompany);
                document.getElementById('ASPxRoundPanel1_txtShiftCode_I').disabled = true;
                document.getElementById('ASPxRoundPanel1_txtStartTime_I').focus();
                PageMethods.LoadShift(window.parent.document.getElementById('MainPane_Content_MainContent_HdnRowId').value, LoginCompany,ShftDataFunc);
            }
        }
        function ShftDataFunc(ShftData) {
            var temp = JSON.parse(ShftData);
            document.getElementById('ASPxRoundPanel1_txtShiftCode_I').value = temp.SHIFT;
            document.getElementById('ASPxRoundPanel1_txtStartTime_I').value = temp.STARTTIME.substring(11, 16);
            document.getElementById('ASPxRoundPanel1_txtEndTime_I').value = temp.ENDTIME.substring(11, 16);
            document.getElementById('ASPxRoundPanel1_txtLunchTime_I').value = temp.LUNCHTIME.substring(11, 16);
            document.getElementById('ASPxRoundPanel1_txtLunchDuration_I').value = MinutestoHourFormat(temp.LUNCHDURATION);
            document.getElementById('ASPxRoundPanel1_txtLunchEndTime_I').value = temp.LUNCHENDTIME.substring(11, 16);
            document.getElementById('ASPxRoundPanel1_txtShiftHours_I').value = MinutestoHourFormat(temp.SHIFTDURATION);
            document.getElementById('ASPxRoundPanel1_txtOTStarts_I').value = MinutestoHourFormat(temp.OTSTARTAFTER);
            document.getElementById('ASPxRoundPanel1_txtOTDeductAfter_I').value = MinutestoHourFormat(temp.OTDEDUCTAFTER);
            document.getElementById('ASPxRoundPanel1_txtOTDeduction_I').value = MinutestoHourFormat(temp.OTDEDUCTHRS);
            document.getElementById('ASPxRoundPanel1_txtLunchDeduction_I').value = MinutestoHourFormat(temp.LUNCHDEDUCTION);
            document.getElementById('ASPxRoundPanel1_ddlShift_I').value = temp.SHIFTPOSITION;
            document.getElementById('ASPxRoundPanel1_txtShiftDetails_I').value = temp.ShiftDiscription;
            window.parent.document.getElementById('MainPane_Content_MainContent_HdnRowId').value = "";
        }
        function OnSaveClick() {
            window.parent.document.getElementById('MainPane_Content_MainContent_HdnRowId').value="";
            var ShiftCode = document.getElementById('ASPxRoundPanel1_txtShiftCode_I').value
            var STARTTIME = document.getElementById('ASPxRoundPanel1_txtStartTime_I').value
            var ENDTIME = document.getElementById('ASPxRoundPanel1_txtEndTime_I').value
            var LUNCHTIME = document.getElementById('ASPxRoundPanel1_txtLunchTime_I').value
            var LUNCHDURATION = document.getElementById('ASPxRoundPanel1_txtLunchDuration_I').value
            var LUNCHENDTIME = document.getElementById('ASPxRoundPanel1_txtLunchEndTime_I').value
            var SHIFTDURATION = document.getElementById('ASPxRoundPanel1_txtShiftHours_I').value
            var OTSTARTAFTER = document.getElementById('ASPxRoundPanel1_txtOTStarts_I').value
            var OTDEDUCTAFTER = document.getElementById('ASPxRoundPanel1_txtOTDeductAfter_I').value
            var OTDEDUCTHRS = document.getElementById('ASPxRoundPanel1_txtOTDeduction_I').value
            var LUNCHDEDUCTION = document.getElementById('ASPxRoundPanel1_txtLunchDeduction_I').value
            var SHIFTPOSITION = document.getElementById('ASPxRoundPanel1_ddlShift_I').value
            var Discription = document.getElementById('ASPxRoundPanel1_txtShiftDetails_I').value
            if (ShiftCode == "") {
                lblError.SetText("Input ShiftCode ");
                document.getElementById('ASPxRoundPanel1_txtShiftCode_I').focus();
                return;
            }
            
            if (Discription == "") {
                lblError.SetText("Input Shift Discription ");
                document.getElementById('ASPxRoundPanel1_txtShiftCode_I').focus();
                return;
            }
            if (STARTTIME == ENDTIME)
            {
                lblError.SetText("Shift Start Time and End Time cannot be same ");
                document.getElementById('ASPxRoundPanel1_txtStartTime_I').focus();
                return;
            }
            if (HourFormatToMinutes(document.getElementById('ASPxRoundPanel1_txtLunchDuration_I').value) < HourFormatToMinutes(document.getElementById('ASPxRoundPanel1_txtLunchDeduction_I').value)) {
                lblError.SetText("Lunch Deduction Can Not Be Greater Than Lunch Duration");
                document.getElementById('ASPxRoundPanel1_txtLunchDeduction_I').focus();
            return;
            }
            var SSftStr = "1900-01-01 " + STARTTIME + ":00";
            var ESftStr = "1900-01-01 " + ENDTIME + ":00";
            var SSHIFTDURATION = HourFormatToMinutes(SHIFTDURATION);
            var SLUNCHDURATION = HourFormatToMinutes(LUNCHDURATION);
            var SLStr = "1900-01-01 " + LUNCHTIME + ":00";
            var ELStr = "1900-01-01 " + LUNCHENDTIME + ":00";

            var SOTSTARTAFTER = HourFormatToMinutes(OTSTARTAFTER);
            var SOTDEDUCTAFTER = HourFormatToMinutes(OTDEDUCTAFTER);
            var SOTDEDUCTHRS = HourFormatToMinutes(OTDEDUCTHRS);
            var SLUNCHDEDUCTION = HourFormatToMinutes(LUNCHDEDUCTION);
            var IsUpdate = document.getElementById('ASPxRoundPanel1_txtShiftCode_I').disabled
            //alert(IsUpdate);
            PageMethods.SaveShift(IsUpdate,ShiftCode, SSftStr, ESftStr, SSHIFTDURATION, SLUNCHDURATION, SLStr, ELStr, SOTSTARTAFTER, SOTDEDUCTAFTER, SOTDEDUCTHRS, SLUNCHDEDUCTION, SHIFTPOSITION,Discription,LoginCompany, HandleReturn);
           
        }
        function HandleReturn(returnValue) {
             if (returnValue == "true")
             {
                 window.parent.pcLogin.Hide();
                 //MainPane_Content_MainContent_grdShift
                 window.parent.ASPxCallbackPanel2.PerformCallback();
                 //window.location = "frmShift.aspx";
             }
             else {
                 //document.getElementById("lblError.ClientID").text = returnValue;
                 lblError.SetText(returnValue);                
             }
         }
        function HourFormatToMinutes(HrForm) {
             var min;
             var list = HrForm.split(":");
             min = parseInt(list[0]) * 60 + parseInt(list[1]);
             return min;
         }
        function MinutestoHourFormat(Min) {
             var HFormat ;
             var hours = Math.floor(Min / 60);
             var minutes = Min % 60;
             if (hours < 10)
                 hours = "0" + hours;
             if (minutes < 10)
                 minutes = "0" + minutes;
             HFormat = hours + ":" + minutes;
             return HFormat;
         }
        
        function get_time_diff(Strdatetime,EndDatetime) {
             //var datetime = typeof datetime !== 'undefined' ? datetime : "2014-01-01 01:02:03.123456";
             var Sdatetime = new Date(Strdatetime).getTime();
             var Edatetime = new Date(EndDatetime).getTime();             
             //if (isNaN(Strdatetime)) {
             //    return "";
             //}
             //if (isNaN(EndDatetime)) {
             //    return "";
             //}
             //console.log(datetime + " " + now);
             if (Sdatetime < Edatetime) {
                 var milisec_diff = Edatetime - Sdatetime;
             } else {
                 var milisec_diff = Sdatetime - Edatetime;
             }
             // for lunch hours deduction
             var lchDeduct = HourFormatToMinutes(document.getElementById('ASPxRoundPanel1_txtLunchDeduction_I').value) * 60000;
             milisec_diff = milisec_diff - lchDeduct;
             // end for lunch hours deduction
             //var days = Math.floor(milisec_diff / 1000 / 60 / (60 * 24));  // original
             var Hrs = Math.floor(milisec_diff / 1000 / 60 / (60));
             var Mins = Math.floor(milisec_diff / 60000) % (60);

             
             if (Hrs < 10)
                 Hrs = "0" + Hrs;
             if (Mins < 10)
                 Mins = "0" + Mins;
             return Hrs + ":" + Mins;
         }
        function findShiftHours() {
             var STARTTIME = document.getElementById('ASPxRoundPanel1_txtStartTime_I').value;
             var ENDTIME = document.getElementById('ASPxRoundPanel1_txtEndTime_I').value;
             var SSftStr = "1900-01-01 " + STARTTIME + ":00";
             var ESftStr = "1900-01-01 " + ENDTIME + ":00";

             var Sdatetime = new Date(SSftStr).getTime();
             var Edatetime = new Date(ESftStr).getTime();
             if (Sdatetime > Edatetime) {
                 ESftStr = "1900-01-02 " + ENDTIME + ":00";
             }            
             document.getElementById('ASPxRoundPanel1_txtShiftHours_I').value=get_time_diff(ESftStr,SSftStr);
         }
    </script>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
           align-items:center
        }
        .auto-style2 {
            height: 18px;
        }
        .auto-style_Tr_Height {
            height: 28px;
        }
         .auto-style_Td1_Width {
            width: 40%;
        }
          .auto-style_Td2_Width {
            width: 50%;
        }
        .auto-style3 {
            width: 40%;
            height: 28px;
        }
        .auto-style4 {
            width: 50%;
            height: 28px;
        }
        </style>
</head>
<body>
     <script type="text/javascript">  
         var LoginCompany='<%=Session["LoginCompany"] %>'
         </script>
    <form id="form1" runat="server">
        
    <div>
    
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
        </asp:ScriptManager>
    
        <table class="auto-style1">           
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" HeaderText="" HorizontalAlign="Left" View="GroupBox" Width="100%">
                                <HeaderStyle Font-Bold="True" />
                                <ClientSideEvents Init="function(s, e) {
	LoadShiftScript();
}" />
                                <PanelCollection>
                                    <dx:PanelContent runat="server">
                                        <table class="auto-style1">
                                            <tr class="auto-style_Tr_Height">
                                                <td class="auto-style_Td1_Width">
                                                    <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Shift Code">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td class="auto-style_Td2_Width" colspan="2">
                                                    <dx:ASPxTextBox ID="txtShiftCode" runat="server" ClientInstanceName="txtShiftCode" MaxLength="3" Width="120px">
                                                        <ClientSideEvents KeyUp="function(s, e) {
	s.SetText(s.GetText().toUpperCase().trim());
}" />
                                                        <MaskSettings IncludeLiterals="None" />
                    
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr class="auto-style_Tr_Height">
                                                <td class="auto-style_Td1_Width">Discription</td>
                                                <td class="auto-style_Td2_Width" colspan="2">
                                                    <dx:ASPxTextBox ID="txtShiftDetails" runat="server" MaxLength="50" Width="120px">
                                                       
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr class="auto-style_Tr_Height">
                                                <td class="auto-style_Td1_Width">Start Time (In 24Hrs)</td>
                                                <td class="auto-style_Td2_Width" colspan="2">
                                                    <dx:ASPxTextBox ID="txtStartTime" runat="server" MaxLength="5" Width="120px">
                                                        <MaskSettings Mask="HH:mm" />
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr class="auto-style_Tr_Height">
                                                <td class="auto-style_Td1_Width">
                                                    <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="End Time (In 24Hrs)">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td class="auto-style_Td2_Width" colspan="2">
                                                    <dx:ASPxTextBox ID="txtEndTime" runat="server"  MaxLength="5" Width="120px" >
                                                        <ClientSideEvents LostFocus="function(s, e) {
	findShiftHours();
}" />
                                                        <MaskSettings Mask="HH:mm" />
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td class="auto-style2"></td>
                                            </tr>
                                            <tr class="auto-style_Tr_Height">
                                                <td class="auto-style_Td1_Width">
                                                    <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Lunch Start Time (In 24Hrs)">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td class="auto-style_Td2_Width" colspan="2">
                                                    <dx:ASPxTextBox ID="txtLunchTime" runat="server" MaxLength="5" Width="120px">
                                                        <MaskSettings Mask="HH:mm" />
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr class="auto-style_Tr_Height">
                                                <td class="auto-style_Td1_Width">
                                                    <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Lunch Duration">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td class="auto-style_Td2_Width" colspan="2">
                                                    <dx:ASPxTextBox ID="txtLunchDuration" runat="server" MaxLength="5" Width="120px" >
                                                        <ClientSideEvents LostFocus="function(s, e) {
	document.getElementById('ASPxRoundPanel1_txtLunchDeduction_I').value=document.getElementById('ASPxRoundPanel1_txtLunchDuration_I').value;

var LUNCHTIME = document.getElementById('ASPxRoundPanel1_txtLunchTime_I').value;
var LMin=HourFormatToMinutes(LUNCHTIME);
var LUNCHDURATION = document.getElementById('ASPxRoundPanel1_txtLunchDuration_I').value;
var DMin = HourFormatToMinutes(LUNCHDURATION);
var LEMin =parseInt(LMin) + parseInt(DMin);
document.getElementById('ASPxRoundPanel1_txtLunchEndTime_I').value=MinutestoHourFormat(LEMin);
findShiftHours();
}" />
                                                        <MaskSettings Mask="HH:mm" />
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr class="auto-style_Tr_Height">
                                                <td class="auto-style_Td1_Width">
                                                    <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Lunch End Time">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td class="auto-style_Td2_Width" colspan="2">
                                                    <dx:ASPxTextBox ID="txtLunchEndTime" runat="server" Enabled="False" MaxLength="5" Width="120px">
                                                        <MaskSettings Mask="HH:mm" />
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td class="auto-style_Tr_Height"></td>
                                            </tr>
                                            <tr class="auto-style_Tr_Height">
                                                <td class="auto-style3">
                                                    <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="Shift Hours">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td class="auto-style4" colspan="2">
                                                    <dx:ASPxTextBox ID="txtShiftHours" runat="server" MaxLength="5" Width="120px">
                                                        <MaskSettings Mask="HH:mm" />
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td class="auto-style_Tr_Height"></td>
                                            </tr>
                                            <tr class="auto-style_Tr_Height">
                                                <td class="auto-style_Td2_Width" colspan="4">
                                                    <dx:ASPxLabel ID="ASPxLabel7" runat="server" style="font-weight: 700" Text="Advance Setup">
                                                    </dx:ASPxLabel>
                                                </td>
                                            </tr>
                                            <tr class="auto-style_Tr_Height">
                                                <td class="auto-style_Td1_Width">
                                                    <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="OT Start After (In Minutes)">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td class="auto-style_Td2_Width" colspan="2">
                                                    <dx:ASPxTextBox ID="txtOTStarts" runat="server" MaxLength="5" Width="120px">
                                                        <MaskSettings Mask="HH:mm" />
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr class="auto-style_Tr_Height">
                                                <td class="auto-style_Td1_Width">
                                                    <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="OT Deduct After OT Start After (In Minutes)">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td class="auto-style_Td2_Width" colspan="2">
                                                    <dx:ASPxTextBox ID="txtOTDeductAfter" runat="server" MaxLength="5" Width="120px">
                                                        <MaskSettings Mask="HH:mm" />
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr class="auto-style_Tr_Height">
                                                <td class="auto-style_Td1_Width">
                                                    <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="OT Deduction OT Start After (In Minutes)">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td class="auto-style_Td2_Width" colspan="2">
                                                    <dx:ASPxTextBox ID="txtOTDeduction" runat="server" MaxLength="5" Width="120px">
                                                        <MaskSettings Mask="HH:mm" />
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr class="auto-style_Tr_Height">
                                                <td>
                                                    <dx:ASPxLabel ID="ASPxLabel11" runat="server" Text="Lunch Deduction">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td colspan="2">
                                                    <dx:ASPxTextBox ID="txtLunchDeduction" runat="server" MaxLength="5" Width="120px">
                                                        <ClientSideEvents LostFocus="function(s, e) {
	findShiftHours();
}" />
                                                        <MaskSettings Mask="HH:mm" />
                                                    </dx:ASPxTextBox>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr class="auto-style_Tr_Height">
                                                <td>
                                                    <dx:ASPxLabel ID="ASPxLabel12" runat="server" Text="Shift Position">
                                                    </dx:ASPxLabel>
                                                </td>
                                                <td colspan="2">
                                                    <dx:ASPxComboBox ID="ddlShift" runat="server" Theme="SoftOrange" Width="120px">
                                                        <Items>
                                                            <dx:ListEditItem Selected="true" Text="Day" Value="DAY" />
                                                            <dx:ListEditItem Text="Afternoon" Value="HALFDAY" />
                                                             <dx:ListEditItem Text="Evening" Value="NIGHT" />
                                                        </Items>
                                                    </dx:ASPxComboBox>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr class="auto-style_Tr_Height">
                                                <td align="center">&nbsp;</td>
                                                <td align="left">
                                                    <dx:ASPxButton ID="btnSave" runat="server" AutoPostBack="False" style="height: 21px" Text="Save" Theme="SoftOrange" Width="100px">
                                                        <ClientSideEvents Click="function(s, e) {
	OnSaveClick();
}" />
                                                    </dx:ASPxButton>
                                                </td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr class="auto-style_Tr_Height">
                                                <td colspan="4">
                                                    <dx:ASPxLabel ID="lblError" runat="server" ClientInstanceName="lblError" ForeColor="Red" Text="   ">
                                                    </dx:ASPxLabel>
                                                </td>
                                            </tr>
                                        </table>
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxRoundPanel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
