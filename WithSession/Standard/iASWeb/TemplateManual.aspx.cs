﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using FKWeb;
using Newtonsoft.Json.Linq;
using DevExpress.Web;
using DevExpress.Data.Filtering;
using Newtonsoft.Json;
using System.IO;
using System.Collections;
using BioCloud;

public partial class TemplateManual : System.Web.UI.Page
{
    OleDbConnection MyConn = new OleDbConnection();
    OleDbCommand MyCmd = new OleDbCommand();
    Class_Connection cn = new Class_Connection();
    public string KeyValue = "";
    public string TableMain = null;
    public string TableName = null;
    public string ColumnName = null;
    const int GET_DEVICE_STATUS = 1;
    const int DEL_USER_INFO = 3;
    ErrorClass ec = new ErrorClass();
    SqlConnection msqlConn;
    static int count = 0;
    ArrayList selectTargetDevID = new ArrayList();
    ArrayList myArrayList1 = new ArrayList();
    private string msDbConn;
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SqlConnFkWeb"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetDevice();
            GetUser();
            msqlConn = FKWebTools.GetDBPool();
            msDbConn = ConfigurationManager.ConnectionStrings["SqlConnFkWeb"].ConnectionString.ToString();


        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        GetUser();
        ASPxGridView1.DataBind();
        GetDevice();
        ASPxGridView2.DataBind();

    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }
    public void GetUser()
    {
        try
        {
            string str = "";
            DataSet DSDev = new DataSet();

            //str = " select user_id,user_name,case when RIGHT(password,1)='?' then '' else 'Password' end +''+case when card IS null then '' else 'Card' end+''+case when face is null then ''" +
            //             " else 'Face' end+''+case when fp_0 is null then '' else 'Finger' end as TemplateType,Device_ID from tbl_realtime_userinfo";

            str = " select distinct a.device_id 'Cloud ID',ltrim(rtrim(a.user_id))  'Biometric ID',tblemployee.Empname 'NAME',case when a.backup_number ='-1' then 'Re-Enroll'  when a.backup_number ='12' then 'Face' when  a.backup_number ='10' then  'Pin' " +
                      " when  a.backup_number ='11' then  'Card'  end  'Enroll Mode'  from tbl_fkcmd_trans_cmd_result_user_id_list a left join tblemployee on cast(tblemployee.presentcardno as int)=a.user_id ";
            DSDev = cn.FillDataSet(str);

            if (DSDev.Tables[0].Rows.Count > 0)
            {
                ASPxGridView1.DataSource = DSDev;
                ASPxGridView1.DataBind();
            }

        }

        catch
        {

        }
    }
    public void GetDevice()
    {
        try
        {
            DataSet DSDev = new DataSet();
            string str = "";
            if (Session["username"].ToString().Trim().ToUpper() == "ADMIN")
            {
                str = "select device_id,device_name,GroupID from tbl_fkdevice_status where   IsAuthorized='Y' order by device_name ";
            }
            else
            {
                str = "select device_id,device_name,GroupID from tbl_fkdevice_status where companycode='"+Session["LoginCompany"].ToString().Trim()+"' and connected=1 and IsAuthorized='Y' order by device_name ";
            }

            DSDev = cn.FillDataSet(str);

            if (DSDev.Tables[0].Rows.Count > 0)
            {
                ASPxGridView2.DataSource = DSDev;
                ASPxGridView2.DataBind();
            }

        }

        catch
        {

        }

    }

    protected void DownloadUser()
    {
        //try
        //{


        //    if (ASPxGridView1.VisibleRowCount == 0)
        //    {
        //        ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('No User To Download')</script>");
        //        return;
        //    }

        //    string sDevId = mDeviceID.Text.ToString().Trim();
        //    string sUserId = mUserID.Text.ToString().Trim();

        //    if (sDevId.Trim() == "")
        //    {
        //        ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Input Details')</script>");
        //        return;
        //    }
        //    int SelCount = ASPxGridView1.Selection.Count;

        //    string sSql = "select user_id from tbl_fkcmd_trans_cmd_result_user_id_list where backup_number=12 and device_id='"+ sDevId + "'";
        //    DataSet DsChk = new DataSet();
        //    DsChk = cn.FillDataSet(sSql);
        //    if (DsChk.Tables[0].Rows.Count>0)

        //    {
        //        for (int K=0;K<DsChk.Tables[0].Rows.Count;K++)
        //        {
        //            sUserId = DsChk.Tables[0].Rows[K]["user_id"].ToString().Trim();
        //            try
        //            {

        //                JObject vResultJson = new JObject();
        //                FKWebCmdTrans cmdTrans;
        //                cmdTrans = new FKWebCmdTrans();
        //                byte[] mByteParam = new byte[0];
        //                string mStrParam;
        //                vResultJson.Add("user_id", sUserId);
        //                mStrParam = vResultJson.ToString(Formatting.None);
        //                cmdTrans.CreateBSCommBufferFromString(mStrParam, out mByteParam);
        //                msqlConn = FKWebTools.GetDBPool();
        //                mTransIdTxt.Text = FKWebTools.MakeCmdupload(msqlConn, "GET_USER_INFO", sDevId, mByteParam, sUserId);
        //                System.Threading.Thread.Sleep(2000);
        //            }
        //            catch
        //            {


        //            }
        //            try
        //            {

        //            Checkrec:
        //                string TempQuery = "select cmd_result from tbl_fkcmd_trans_cmd_result where trans_id='" + sUserId + "'";
        //                DataSet DsRes = new DataSet();
        //                DsRes = cn.FillDataSet(TempQuery);
        //                if (DsRes.Tables[0].Rows.Count > 0 && DsRes.Tables[0].Rows[0][0].ToString().Trim() != null)
        //                {
        //                    if (msqlConn.State != ConnectionState.Open)
        //                    {
        //                        msqlConn.Open();
        //                    }
        //                    sSql = "select @cmd_result=cmd_result from tbl_fkcmd_trans_cmd_result where trans_id='" + sUserId + "'";
        //                    SqlCommand sqlCmd = new SqlCommand(sSql, msqlConn);
        //                    SqlParameter sqlParamCmdParamBin = new SqlParameter("@cmd_result", SqlDbType.VarBinary);
        //                    sqlParamCmdParamBin.Direction = ParameterDirection.Output;
        //                    sqlParamCmdParamBin.Size = -1;
        //                    sqlCmd.Parameters.Add(sqlParamCmdParamBin);

        //                    sqlCmd.ExecuteNonQuery();
        //                    if (msqlConn.State != ConnectionState.Closed)
        //                    {
        //                        msqlConn.Close();
        //                    }
        //                    byte[] bytCmdResult = null;
        //                    bytCmdResult = (byte[])sqlParamCmdParamBin.Value;
        //                    byte[] bytResultBin = new byte[0];
        //                    string sResultText;
        //                    try
        //                    {
        //                        InsertUserinfo(bytCmdResult, sDevId.Trim());
        //                    }

        //                    catch (Exception Ex)
        //                    {
        //                        Error_Occured("ASPxButton4_Click", Ex.Message);
        //                    }
        //                }

        //                else
        //                {
        //                    goto Checkrec;
        //                }


        //            }
        //            catch
        //            {


        //            }
        //        }

              

        //    }



        //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Data Download Command Sent Successfully')", true);
        //    return;
        //}
        //catch (Exception Exc)
        //{
        //    Error_Occured("DownloadUser", Exc.Message);
        //    ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Error In Sending Command ')</script>");
        //}
    }

    protected void ASPxButton3_Click(object sender, EventArgs e)
    {
        try
        {
            //DownloadUser();
            //return;

            if (ASPxGridView1.VisibleRowCount == 0)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('No User To Download')</script>");
                return;
            }

            string sDevId = "";
            string sUserId = "";
            int SelCount = ASPxGridView1.Selection.Count;
            for (int i = 0; i < ASPxGridView1.VisibleRowCount; i++)
            {

                if (ASPxGridView1.Selection.IsRowSelected(i) == true)
                {
                    try
                    {
                        sDevId = ASPxGridView1.GetRowValues(i, "Cloud ID").ToString().Trim();
                        sUserId = ASPxGridView1.GetRowValues(i, "Biometric ID").ToString().Trim();
                        JObject vResultJson = new JObject();
                        FKWebCmdTrans cmdTrans;
                        cmdTrans = new FKWebCmdTrans();
                        byte[] mByteParam = new byte[0];
                        string mStrParam;
                        vResultJson.Add("user_id", sUserId);
                        mStrParam = vResultJson.ToString(Formatting.None);
                        cmdTrans.CreateBSCommBufferFromString(mStrParam, out mByteParam);
                        msqlConn = FKWebTools.GetDBPool();
                        mTransIdTxt.Text = FKWebTools.MakeCmdupload(msqlConn, "GET_USER_INFO", sDevId, mByteParam, sUserId);
                       // System.Threading.Thread.Sleep(10000);
                    }
                    catch
                    {


                    }
                    //try
                    //{

                    //Checkrec:
                    //    string TempQuery = "select cmd_result from tbl_fkcmd_trans_cmd_result where trans_id='" + sUserId + "'";
                    //    DataSet DsRes = new DataSet();
                    //    DsRes = cn.FillDataSet(TempQuery);
                    //    if (DsRes.Tables[0].Rows.Count==0)
                    //    {
                    //        continue;
                    //    }
                    //    else if (DsRes.Tables[0].Rows[0][0].ToString().Trim() != null)
                    //    {
                    //        if (msqlConn.State != ConnectionState.Open)
                    //        {
                    //            msqlConn.Open();
                    //        }
                    //        string sSql = "select @cmd_result=cmd_result from tbl_fkcmd_trans_cmd_result where trans_id='" + sUserId + "'";
                    //        SqlCommand sqlCmd = new SqlCommand(sSql, msqlConn);
                    //        SqlParameter sqlParamCmdParamBin = new SqlParameter("@cmd_result", SqlDbType.VarBinary);
                    //        sqlParamCmdParamBin.Direction = ParameterDirection.Output;
                    //        sqlParamCmdParamBin.Size = -1;
                    //        sqlCmd.Parameters.Add(sqlParamCmdParamBin);

                    //        sqlCmd.ExecuteNonQuery();
                    //        if (msqlConn.State != ConnectionState.Closed)
                    //        {
                    //            msqlConn.Close();
                    //        }
                    //        byte[] bytCmdResult = null;
                    //        bytCmdResult = (byte[])sqlParamCmdParamBin.Value;
                    //        byte[] bytResultBin = new byte[0];
                    //        string sResultText;
                    //        try
                    //        {
                    //            InsertUserinfo(bytCmdResult, sDevId.Trim());
                    //        }

                    //        catch (Exception Ex)
                    //        {
                    //            Error_Occured("ASPxButton4_Click", Ex.Message);
                    //        }
                    //    }

                    //    else
                    //    {
                    //        goto Checkrec;
                    //    }


                    //}
                    //catch
                    //{


                    //}

                }
            }

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Data Download Command Sent Successfully')", true);
            return;
        }
        catch (Exception Exc)
        {
            Error_Occured("ASPxButton3_Click", Exc.Message);
            ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Error In Sending Command ')</script>");
        }
    }
    protected void ASPxGridView1_PageIndexChanged(object sender, EventArgs e)
    {
        int pageIndex = (sender as ASPxGridView).PageIndex;

        ASPxGridView1.PageIndex = pageIndex;
        this.GetDevice();
    }
    protected void ASPxGridView1_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
    {
        if (e.Column.FieldName == "Biometric ID")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                string[] strContainerNumbers = e.Value.ToString().Split(',');
                Session["filter"] = e.Value;
                CriteriaOperator cop = new InOperator("Biometric ID", strContainerNumbers);
                e.Criteria = cop;
            }
            else
            {
                if (Session["filter"] != null)
                    e.Value = Session["filter"].ToString();
            }
        }
    }
    protected void ASPxGridView2_PageIndexChanged(object sender, EventArgs e)
    {
        int pageIndex = (sender as ASPxGridView).PageIndex;

        ASPxGridView2.PageIndex = pageIndex;
        this.GetDevice();
    }

    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        try
        {
            int facecount = 0, fpcount = 0;
            string sSql = string.Empty;
            string sDevId = "";
            string sUserId = "", sUserName = "", sPreviledge = "";
            string Password = "", CardNum = "", UserName = "";
            FKWebCmdTrans cmdTrans = new FKWebCmdTrans();
            JObject vResultJson;
            string mDeviceId = "";
            //#region Validation

            //List<object> KeyUser = new List<object>();
            //KeyUser = ASPxGridView1.GetSelectedFieldValues(new string[] { ASPxGridView1.KeyFieldName });
            //List<object> KeyDevice = new List<object>();
            //KeyDevice = ASPxGridView2.GetSelectedFieldValues(new string[] { ASPxGridView2.KeyFieldName });
            //    //List<object> KeyUser = ASPxGridView1.GetSelectedFieldValues(new string[] { ASPxGridView1.KeyFieldName });
            //   // List<object> KeyDevice = ASPxGridView2.GetSelectedFieldValues(new string[] { ASPxGridView2.KeyFieldName });
            //    if (KeyUser.Count == 0 || KeyDevice.Count == 0)
            //    {
            //        ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Please Select Atleast One Device/User')</script>");
            //        return;
            //    }
            //#endregion
            if (ASPxGridView2.VisibleRowCount == 0 || ASPxGridView1.VisibleRowCount == 0)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Please Select Atleast One Device/User')</script>");
                return;
            }

            for (int i = 0; i < ASPxGridView2.VisibleRowCount; i++)
            {
                if (ASPxGridView2.Selection.IsRowSelected(i) == true)
                {
                    try
                    {
                        count = count + 1;
                        sDevId = ASPxGridView2.GetRowValues(i, "device_id").ToString().Trim();
                        string mDeviceIdTemp = Convert.ToString(sDevId);
                        selectTargetDevID.Add(mDeviceIdTemp);
                    }
                    catch
                    {

                    }
                }
            }


            //for (int DevRec = 0; DevRec < KeyDevice.Count; DevRec++)
            //{
            //    count = count + 1;
            //    string mDeviceIdTemp = Convert.ToString(KeyDevice[DevRec]);
            //    selectTargetDevID.Add(mDeviceIdTemp);
            //}

            for (int iU = 0; iU < ASPxGridView1.VisibleRowCount; iU++)
            {
                if (ASPxGridView1.Selection.IsRowSelected(iU) == true)
                {
                    try
                    {
                        facecount = 0; fpcount = 0;
                        Password = ""; CardNum = "";
                        msqlConn = FKWebTools.GetDBPool();
                        if (msqlConn.State != ConnectionState.Open)
                        {
                            msqlConn.Open();
                        }
                        //ASPxGridView2.GetRowValues(i, "Cloud ID").ToString().Trim();
                        string mUserId = Convert.ToString(ASPxGridView1.GetRowValues(iU, "Biometric ID").ToString().Trim());
                        UserName = GetName(mUserId);
                        if (UserName.ToString().Trim().Length > 14)
                        {
                            UserName = UserName.Substring(0, 14);
                        }

                        try
                        {
                            sSql = "select @cmd_result=cmd_result from tbl_fkcmd_trans_cmd_result where trans_id='" + mUserId + "'";
                            SqlCommand sqlCmd = new SqlCommand(sSql, msqlConn);
                            SqlParameter sqlParamCmdParamBin = new SqlParameter("@cmd_result", SqlDbType.VarBinary);
                            sqlParamCmdParamBin.Direction = ParameterDirection.Output;
                            sqlParamCmdParamBin.Size = -1;
                            sqlCmd.Parameters.Add(sqlParamCmdParamBin);

                            sqlCmd.ExecuteNonQuery();
                            if (msqlConn.State != ConnectionState.Closed)
                            {
                                msqlConn.Close();
                            }
                            byte[] bytCmdResult = null;
                            bytCmdResult = (byte[])sqlParamCmdParamBin.Value;
                            byte[] bytResultBin = new byte[0];
                            string sResultText;
                            try
                            {
                                InsertUserinfo(bytCmdResult, "");
                            }

                            catch
                            {

                            }
                            cmdTrans.GetStringAndBinaryFromBSCommBuffer(bytCmdResult, out sResultText, out bytResultBin);
                            vResultJson = JObject.Parse(sResultText);

                            String UserID = vResultJson["user_id"].ToString();

                            string sUserpriv = "USER";
                            int vnBinIndex = 0;
                            int vnBinCount = FKWebTools.BACKUP_MAX + 1;
                            int[] vnBackupNumbers = new int[vnBinCount];

                            for (int i = 0; i < vnBinCount; i++)
                            {
                                vnBackupNumbers[i] = -1;
                                if (i <= FKWebTools.BACKUP_FP_9)
                                    FKWebTools.mFinger[i] = new byte[0];
                            }
                            FKWebTools.mFace = new byte[0];
                            FKWebTools.mPhoto = new byte[0];
                            try
                            {
                                string vStrUserPhotoBinIndex = vResultJson["user_photo"].ToString(); //aCmdParamJson.get("user_photo", "").asString();
                                if (vStrUserPhotoBinIndex.Length != 0)
                                {
                                    vnBinIndex = FKWebTools.GetBinIndex(vStrUserPhotoBinIndex) - 1;
                                    vnBackupNumbers[vnBinIndex] = FKWebTools.BACKUP_USER_PHOTO;
                                }
                            }
                            catch (Exception Ex)
                            {
                                goto ASGT;
                            }
                        ASGT:
                            string tmp = "";

                            JArray vEnrollDataArrayJson = JArray.Parse(vResultJson["enroll_data_array"].ToString());

                            foreach (JObject content in vEnrollDataArrayJson.Children<JObject>())
                            {
                                int vnBackupNumber = Convert.ToInt32(content["backup_number"].ToString());

                                string vStrBinIndex = content["enroll_data"].ToString();
                                vnBinIndex = FKWebTools.GetBinIndex(vStrBinIndex) - 1;
                                vnBackupNumbers[vnBinIndex] = vnBackupNumber;
                                tmp = tmp + ":" + Convert.ToInt32(vnBinIndex) + "-" + Convert.ToInt32(vnBackupNumber);
                            }
                            for (int i = 0; i < vnBinCount; i++)
                            {
                                if (vnBackupNumbers[i] == -1) continue;

                                if (vnBackupNumbers[i] == FKWebTools.BACKUP_USER_PHOTO)
                                {
                                    byte[] bytResultBinParam = new byte[0];
                                    int vnBinLen = FKWebTools.GetBinarySize(bytResultBin, out bytResultBin);
                                    string AbsImgUri = Server.MapPath(".") + "\\photo\\" + mDeviceId + "_" + mUserId + ".jpg";
                                    string relativeImgUrl = ".\\photo\\" + mDeviceId + "_" + mUserId + ".jpg";
                                    FKWebTools.GetBinaryData(bytResultBin, vnBinLen, out bytResultBinParam, out bytResultBin);
                                    FKWebTools.mPhoto = new byte[vnBinLen];

                                    Array.Copy(bytResultBinParam, FKWebTools.mPhoto, vnBinLen);
                                    try
                                    {
                                        FileStream fs = new FileStream(AbsImgUri, FileMode.Create, FileAccess.Write);
                                        fs.Write(bytResultBinParam, 0, bytResultBinParam.Length);
                                        fs.Close();
                                    }
                                    catch (Exception Ex)
                                    {

                                    }

                                }
                                if (vnBackupNumbers[i] == FKWebTools.BACKUP_PSW)
                                {
                                    byte[] bytResultBinParam = new byte[0];
                                    Password = cmdTrans.GetStringFromBSCommBuffer(bytResultBin);
                                    int vnBinLen = FKWebTools.GetBinarySize(bytResultBin, out bytResultBin);
                                    FKWebTools.GetBinaryData(bytResultBin, vnBinLen, out bytResultBinParam, out bytResultBin);
                                }
                                if (vnBackupNumbers[i] == FKWebTools.BACKUP_CARD)
                                {
                                    byte[] bytResultBinParam = new byte[0];
                                    CardNum = cmdTrans.GetStringFromBSCommBuffer(bytResultBin);
                                    int vnBinLen = FKWebTools.GetBinarySize(bytResultBin, out bytResultBin);
                                    FKWebTools.GetBinaryData(bytResultBin, vnBinLen, out bytResultBinParam, out bytResultBin);

                                }

                                if (vnBackupNumbers[i] == FKWebTools.BACKUP_FACE)
                                {
                                    byte[] bytResultBinParam = new byte[0];
                                    int vnBinLen = FKWebTools.GetBinarySize(bytResultBin, out bytResultBin);
                                    FKWebTools.GetBinaryData(bytResultBin, vnBinLen, out bytResultBinParam, out bytResultBin);
                                    facecount = 1;
                                    FKWebTools.mFace = new byte[vnBinLen];
                                    Array.Copy(bytResultBinParam, FKWebTools.mFace, vnBinLen);
                                }
                                if (vnBackupNumbers[i] >= FKWebTools.BACKUP_FP_0 && vnBackupNumbers[i] <= FKWebTools.BACKUP_FP_9)
                                {
                                    byte[] bytResultBinParam = new byte[0];
                                    int vnBinLen = FKWebTools.GetBinarySize(bytResultBin, out bytResultBin);
                                    FKWebTools.GetBinaryData(bytResultBin, vnBinLen, out bytResultBinParam, out bytResultBin);
                                    fpcount += 1;
                                    FKWebTools.mFinger[vnBackupNumbers[i]] = new byte[vnBinLen];
                                    Array.Copy(bytResultBinParam, FKWebTools.mFinger[vnBackupNumbers[i]], vnBinLen);
                                }
                            }
                            SetInfo(UserID, UserName, sUserpriv, fpcount, facecount, Password, CardNum);
                        }
                        catch
                        {


                        }
                    }
                    catch
                    {

                    }
                }
            }


            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Data Uploaded Successfully')", true);
            return;




        }
        catch
        {

        }
    }
    //static public string GetStringFromBSCommBuffer(byte[] abytBSComm)
    //{
    //    if (abytBSComm.Length < 4)
    //        return "";

    //    try
    //    {
    //        string sRet;

    //        int lenText = BitConverter.ToInt32(abytBSComm, 0);
    //        if (lenText > abytBSComm.Length - 4)
    //            return "";

    //        if (lenText == 0)
    //            return "";

    //        if (abytBSComm[4 + lenText - 1] == 0) // if last value of string buufer is 0x0
    //            sRet = System.Text.Encoding.UTF8.GetString(abytBSComm, 4, lenText - 1);
    //        else
    //            sRet = System.Text.Encoding.UTF8.GetString(abytBSComm, 4, lenText);

    //        return sRet;
    //    }
    //    catch
    //    {
    //        return "";
    //    }
    //}

    //===============================================================================
    // BS통신바퍼로부터 문자렬과 뒤에 놓이는 첫번째 바이너리자료를 얻는다.
    static public void GetStringAnd1stBinaryFromBSCommBuffer(
        byte[] abytBSComm,
        out string asString,
        out byte[] abytBinary)
    {
        asString = "";
        abytBinary = new byte[0];
        //PrintDebugMsg("Parse Json", "4 length = " + abytBSComm.Length);
        if (abytBSComm.Length < 4)
            return;

        try
        {
            int lenText = BitConverter.ToInt32(abytBSComm, 0);
            if (lenText > abytBSComm.Length - 4)
                return;

            if (lenText == 0)
            {
                asString = "";
            }
            else
            {
                if (abytBSComm[4 + lenText - 1] == 0) // if last value of string buufer is 0x0
                    asString = System.Text.Encoding.UTF8.GetString(abytBSComm, 4, lenText - 1);
                else
                    asString = System.Text.Encoding.UTF8.GetString(abytBSComm, 4, lenText);
            }

            int posBin = 4 + lenText;
            int lenBin = BitConverter.ToInt32(abytBSComm, posBin);
            if (lenBin < 1)
                return;

            if (lenBin > abytBSComm.Length - posBin - 4)
                return;

            abytBinary = new byte[lenBin];
            System.Buffer.BlockCopy(
                abytBSComm, posBin + 4,
                abytBinary, 0,
                lenBin);
            return;
        }
        catch
        {
            return;
        }
    }

    static public bool CreateBSCommBufferFromString(string asCmdParamText, out byte[] abytBuffer)
    {
        abytBuffer = new byte[0];

        try
        {
            if (asCmdParamText.Length == 0)
                return true;

            // 문자렬자료를 바이트배렬로 변환하고 마지막에 0을 붙인다. 그리고 그 길이를 문자렬자료길이로 본다.
            // 전송할 파라메터바이트배렬의 첫 4바이트는 문자렬자료의 길이를 나타낸다.
            byte[] bytText = System.Text.Encoding.UTF8.GetBytes(asCmdParamText);
            byte[] bytTextLen = BitConverter.GetBytes(Convert.ToUInt32(bytText.Length + 1));
            abytBuffer = new byte[4 + bytText.Length + 1];
            bytTextLen.CopyTo(abytBuffer, 0);
            bytText.CopyTo(abytBuffer, 4);
            abytBuffer[4 + bytText.Length] = 0;
            return true;
        }
        catch
        {
            abytBuffer = new byte[0];
            return false;
        }
    }

    static public void AppendBinData(ref byte[] abytBSComm, byte[] abytToAdd)
    {
        try
        {
            byte[] bytBSComm = (byte[])abytBSComm;
            byte[] bytToAdd = (byte[])abytToAdd;

            if (bytToAdd.Length == 0)
                return;

            int len_dest = bytBSComm.Length + 4 + bytToAdd.Length;
            byte[] bytRet = new byte[len_dest];
            byte[] bytAddLen = BitConverter.GetBytes(Convert.ToUInt32(bytToAdd.Length));
            System.Buffer.BlockCopy(bytBSComm, 0, bytRet, 0, bytBSComm.Length);
            System.Buffer.BlockCopy(bytAddLen, 0, bytRet, bytBSComm.Length, 4);
            System.Buffer.BlockCopy(bytToAdd, 0, bytRet, bytBSComm.Length + 4, bytToAdd.Length);
            abytBSComm = bytRet;
            return;
        }
        catch (Exception)
        {
            return;
        }
    }

    public static byte[] GetBinDataFromBSCommBinData(int anOrder, byte[] abytBSComm)
    {
        byte[] bytEmpty = new byte[0];

        try
        {
            // Convert SQLSever Data types to C# data types
            // SQLSever Data type (varbinary) <-> C# data type (byte [])
            byte[] bytBSComm = (byte[])abytBSComm;
            if (bytBSComm.Length < 4)
                return bytEmpty;

            if (anOrder < 1 || anOrder > 255)
                return bytEmpty;

            int orderBin;
            int posBin;
            int lenBin;
            int lenText = BitConverter.ToInt32(bytBSComm, 0);
            if (lenText > bytBSComm.Length - 4)
                return bytEmpty;

            posBin = 4 + lenText;
            orderBin = 0;
            while (true)
            {
                if (posBin > bytBSComm.Length - 4)
                    return bytEmpty;

                lenBin = BitConverter.ToInt32(bytBSComm, posBin);
                if (lenBin > bytBSComm.Length - posBin - 4)
                    return bytEmpty;

                orderBin++;
                if (orderBin == anOrder)
                    break;

                posBin = posBin + 4 + lenBin;
            }

            byte[] bytRet = new byte[lenBin];
            System.Buffer.BlockCopy(
                 bytBSComm, posBin + 4,
                 bytRet, 0,
                 lenBin);
            return bytRet;
        }
        catch (Exception)
        {
            return bytEmpty;
        }
    }

    public void GetStringAndBinaryFromBSCommBuffer(byte[] abytBSCommBuffer, out string asString, out byte[] abytBinary)
    {
        asString = "";
        abytBinary = new byte[0];

        if (abytBSCommBuffer.Length < 4)
            return;

        try
        {
            int lenText = BitConverter.ToInt32(abytBSCommBuffer, 0);
            if (lenText > abytBSCommBuffer.Length - 4)
                return;

            if (lenText > 0)
            {
                asString = System.Text.Encoding.UTF8.GetString(abytBSCommBuffer, 4, lenText);
            }

            int lenBin = abytBSCommBuffer.Length - lenText - 4;
            if (lenBin < 1)
                return;

            abytBinary = new byte[lenBin];
            System.Buffer.BlockCopy(
                abytBSCommBuffer, lenText + 4,
                abytBinary, 0,
                lenBin);
            return;
        }
        catch
        {
            return;
        }
    }
    public void DelectUserinfo(SqlConnection sqlConn, Userinfo userinfo, string DevID)
    {
        if (userinfo.user_id == null) return;
        string strSql = "select @count=count(*) from tbl_realtime_userinfo where Device_ID='" + DevID.Trim() + "' and user_id = " + userinfo.user_id;
        SqlCommand sqlCmd = new SqlCommand(strSql, sqlConn);
        SqlParameter sql_count = new SqlParameter("@count", SqlDbType.Int);
        sql_count.Direction = ParameterDirection.Output;
        sql_count.Size = -1;
        sqlCmd.Parameters.Add(sql_count);
        sqlCmd.ExecuteNonQuery();

        int count = (int)sql_count.Value;
        if (count > 0)
        {
            strSql = "delete tbl_realtime_userinfo where Device_ID='" + DevID.Trim() + "' and user_id = " + userinfo.user_id;
            sqlCmd = new SqlCommand(strSql, sqlConn);
            sqlCmd.ExecuteNonQuery();
        }
    }
    public void InsertUserinfo(byte[] bytCmdResult, string DeviceID)
    {
        string csFuncName = "InsertUserinfo--->>" + DeviceID;
        // PrintDebugMsg(csFuncName, "Ajitesh Test");
        // msqlConn = FKWebTools.GetDBPool();
        msDbConn = ConfigurationManager.ConnectionStrings["SqlConnFkWeb"].ConnectionString.ToString();
        SqlConnection sqlConn = new SqlConnection(msDbConn);
        sqlConn.Open();
        try
        {
            byte[] bytResultBin = new byte[0];
            string sResultText;
            GetStringAndBinaryFromBSCommBuffer(bytCmdResult, out sResultText, out bytResultBin);
            //   PrintDebugMsg(csFuncName, "sResultText-->" + sResultText);
            // var userinfo = (Userinfo)JsonConvert.DeserializeObject(sResultText, Type.GetType("Userinfo"));//result为上面的Json数据  
            // PrintDebugMsg(csFuncName, "len =========> " + bytCmdResult.Length + "," + bytResultBin.Length);

            var userinfo = (Userinfo)JsonConvert.DeserializeObject(sResultText, typeof(Userinfo));


            DelectUserinfo(sqlConn, userinfo, DeviceID);

            int vnBinIndex = 0;
            int vnBinCount = FKWebTools.BACKUP_MAX + 1;
            int[] vnBackupNumbers = new int[vnBinCount];

            for (int i = 0; i < vnBinCount; i++)
            {
                vnBackupNumbers[i] = -1;
            }
            foreach (Enroll_Data enroll_data in userinfo.enroll_data_array)
            {
                int vnBackupNumber = enroll_data.backup_number;

                string vStrBinIndex = enroll_data.enroll_data;
                vnBinIndex = FKWebTools.GetBinIndex(vStrBinIndex) - 1;
                vnBackupNumbers[vnBinIndex] = vnBackupNumber;
            }
            if (userinfo.user_photo != null)
            {
                vnBinIndex = FKWebTools.GetBinIndex(userinfo.user_photo) - 1;
                vnBackupNumbers[vnBinIndex] = FKWebTools.BACKUP_USER_PHOTO;
            }

            for (int i = 0; i < vnBinCount; i++)
            {
                if (vnBackupNumbers[i] == -1) continue;

                if (vnBackupNumbers[i] == FKWebTools.BACKUP_USER_PHOTO)
                {
                    byte[] bytResultBinParam = new byte[0];
                    int vnBinLen = FKWebTools.GetBinarySize(bytResultBin, out bytResultBin);
                    FKWebTools.GetBinaryData(bytResultBin, vnBinLen, out bytResultBinParam, out bytResultBin);
                    userinfo.photoData = new byte[vnBinLen];
                    Array.Copy(bytResultBinParam, userinfo.photoData, vnBinLen);
                    // PrintDebugMsg(csFuncName, "BACKUP_USER_PHOTO =========> " + vnBinLen + "," + userinfo.photoData.Length + "," + bytResultBin.Length);
                }
                if (vnBackupNumbers[i] == FKWebTools.BACKUP_PSW)
                {
                    byte[] bytResultBinParam = new byte[0];
                    userinfo.pwdData = GetStringFromBSCommBuffer(bytResultBin);
                    int vnBinLen = FKWebTools.GetBinarySize(bytResultBin, out bytResultBin);
                    FKWebTools.GetBinaryData(bytResultBin, vnBinLen, out bytResultBinParam, out bytResultBin);
                }
                if (vnBackupNumbers[i] == FKWebTools.BACKUP_CARD)
                {
                    byte[] bytResultBinParam = new byte[0];
                    userinfo.cardData = GetStringFromBSCommBuffer(bytResultBin);
                    int vnBinLen = FKWebTools.GetBinarySize(bytResultBin, out bytResultBin);
                    FKWebTools.GetBinaryData(bytResultBin, vnBinLen, out bytResultBinParam, out bytResultBin);
                }

                if (vnBackupNumbers[i] == FKWebTools.BACKUP_FACE)
                {
                    byte[] bytResultBinParam = new byte[0];
                    int vnBinLen = FKWebTools.GetBinarySize(bytResultBin, out bytResultBin);
                    FKWebTools.GetBinaryData(bytResultBin, vnBinLen, out bytResultBinParam, out bytResultBin);
                    userinfo.faceData = new byte[vnBinLen];
                    Array.Copy(bytResultBinParam, userinfo.faceData, vnBinLen);
                }
                if (vnBackupNumbers[i] >= FKWebTools.BACKUP_FP_0 && vnBackupNumbers[i] <= FKWebTools.BACKUP_FP_9)
                {
                    byte[] bytResultBinParam = new byte[0];
                    int vnBinLen = FKWebTools.GetBinarySize(bytResultBin, out bytResultBin);
                    FKWebTools.GetBinaryData(bytResultBin, vnBinLen, out bytResultBinParam, out bytResultBin);
                    userinfo.fpData[vnBackupNumbers[i]] = new byte[vnBinLen];
                    Array.Copy(bytResultBinParam, userinfo.fpData[vnBackupNumbers[i]], vnBinLen);
                    // PrintDebugMsg(csFuncName, "BACKUP_FP_" + i + "=========> " + vnBinLen + "," + userinfo.fpData[vnBackupNumbers[i]].Length + "," + bytResultBin.Length);
                }
            }

            Error_Occured("Insert RealTimeLog", userinfo.user_id);




            try
            {

                string strSql;
                strSql = "INSERT INTO tbl_realtime_userinfo";
                strSql = strSql + "(user_id, user_name, privilige, card, Password, Face, photo, Fp_0, Fp_1, Fp_2, Fp_3, Fp_4, Fp_5, Fp_6, Fp_7, Fp_8, Fp_9,Device_ID)";
                strSql = strSql + "VALUES(@user_id, @user_name, @privilige, @card, @Password, @Face, @photo, @Fp0, @Fp1, @Fp2, @Fp3, @Fp4, @Fp5, @Fp6, @Fp7, @Fp8, @Fp9,@DeID)";

                SqlCommand sqlCmd = new SqlCommand(strSql, sqlConn);

                sqlCmd.Parameters.Add("@user_id", SqlDbType.VarChar).Value = SqlNull(userinfo.user_id);
                sqlCmd.Parameters.Add("@user_name", SqlDbType.VarChar).Value = SqlNull(userinfo.user_name);
                sqlCmd.Parameters.Add("@privilige", SqlDbType.VarChar).Value = SqlNull(userinfo.user_privilege);
                sqlCmd.Parameters.Add("@card", SqlDbType.VarChar).Value = SqlNull(userinfo.cardData);
                sqlCmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = SqlNull(userinfo.pwdData);

                SqlParameter sqlParamUserData;

                sqlParamUserData = new SqlParameter("@Face", SqlDbType.VarBinary);
                sqlParamUserData.Direction = ParameterDirection.Input;
                if (userinfo.faceData != null)
                {
                    sqlParamUserData.Size = userinfo.faceData.Length;
                    sqlParamUserData.Value = userinfo.faceData;
                }
                else
                    sqlParamUserData.Value = DBNull.Value;
                sqlCmd.Parameters.Add(sqlParamUserData);

                sqlParamUserData = new SqlParameter("@photo", SqlDbType.VarBinary);
                sqlParamUserData.Direction = ParameterDirection.Input;
                if (userinfo.photoData != null)
                {
                    sqlParamUserData.Size = userinfo.photoData.Length;
                    sqlParamUserData.Value = userinfo.photoData;
                }
                else
                    sqlParamUserData.Value = DBNull.Value;
                sqlCmd.Parameters.Add(sqlParamUserData);

                sqlParamUserData = new SqlParameter("@Fp0", SqlDbType.VarBinary);
                sqlParamUserData.Direction = ParameterDirection.Input;
                if (userinfo.fpData[0] != null)
                {
                    sqlParamUserData.Size = userinfo.fpData[0].Length;
                    sqlParamUserData.Value = userinfo.fpData[0];
                }
                else
                    sqlParamUserData.Value = DBNull.Value;
                sqlCmd.Parameters.Add(sqlParamUserData);

                sqlParamUserData = new SqlParameter("@Fp1", SqlDbType.VarBinary);
                sqlParamUserData.Direction = ParameterDirection.Input;
                if (userinfo.fpData[1] != null)
                {
                    sqlParamUserData.Size = userinfo.fpData[1].Length;
                    sqlParamUserData.Value = userinfo.fpData[1];
                }
                else
                    sqlParamUserData.Value = DBNull.Value;
                sqlCmd.Parameters.Add(sqlParamUserData);

                sqlParamUserData = new SqlParameter("@Fp2", SqlDbType.VarBinary);
                sqlParamUserData.Direction = ParameterDirection.Input;
                if (userinfo.fpData[2] != null)
                {
                    sqlParamUserData.Size = userinfo.fpData[2].Length;
                    sqlParamUserData.Value = userinfo.fpData[2];
                }
                else
                    sqlParamUserData.Value = DBNull.Value;
                sqlCmd.Parameters.Add(sqlParamUserData);

                sqlParamUserData = new SqlParameter("@Fp3", SqlDbType.VarBinary);
                sqlParamUserData.Direction = ParameterDirection.Input;
                if (userinfo.fpData[3] != null)
                {
                    sqlParamUserData.Size = userinfo.fpData[0].Length;
                    sqlParamUserData.Value = userinfo.fpData[0];
                }
                else
                    sqlParamUserData.Value = DBNull.Value;
                sqlCmd.Parameters.Add(sqlParamUserData);

                sqlParamUserData = new SqlParameter("@Fp4", SqlDbType.VarBinary);
                sqlParamUserData.Direction = ParameterDirection.Input;
                if (userinfo.fpData[4] != null)
                {
                    sqlParamUserData.Size = userinfo.fpData[4].Length;
                    sqlParamUserData.Value = userinfo.fpData[4];
                }
                else
                    sqlParamUserData.Value = DBNull.Value;
                sqlCmd.Parameters.Add(sqlParamUserData);

                sqlParamUserData = new SqlParameter("@Fp5", SqlDbType.VarBinary);
                sqlParamUserData.Direction = ParameterDirection.Input;
                if (userinfo.fpData[5] != null)
                {
                    sqlParamUserData.Size = userinfo.fpData[5].Length;
                    sqlParamUserData.Value = userinfo.fpData[5];
                }
                else
                    sqlParamUserData.Value = DBNull.Value;
                sqlCmd.Parameters.Add(sqlParamUserData);

                sqlParamUserData = new SqlParameter("@Fp6", SqlDbType.VarBinary);
                sqlParamUserData.Direction = ParameterDirection.Input;
                if (userinfo.fpData[6] != null)
                {
                    sqlParamUserData.Size = userinfo.fpData[6].Length;
                    sqlParamUserData.Value = userinfo.fpData[6];
                }
                else
                    sqlParamUserData.Value = DBNull.Value;
                sqlCmd.Parameters.Add(sqlParamUserData);

                sqlParamUserData = new SqlParameter("@Fp7", SqlDbType.VarBinary);
                sqlParamUserData.Direction = ParameterDirection.Input;
                if (userinfo.fpData[7] != null)
                {
                    sqlParamUserData.Size = userinfo.fpData[7].Length;
                    sqlParamUserData.Value = userinfo.fpData[7];
                }
                else
                    sqlParamUserData.Value = DBNull.Value;
                sqlCmd.Parameters.Add(sqlParamUserData);

                sqlParamUserData = new SqlParameter("@Fp8", SqlDbType.VarBinary);
                sqlParamUserData.Direction = ParameterDirection.Input;
                if (userinfo.fpData[8] != null)
                {
                    sqlParamUserData.Size = userinfo.fpData[8].Length;
                    sqlParamUserData.Value = userinfo.fpData[8];
                }
                else
                    sqlParamUserData.Value = DBNull.Value;
                sqlCmd.Parameters.Add(sqlParamUserData);

                sqlParamUserData = new SqlParameter("@Fp9", SqlDbType.VarBinary);
                sqlParamUserData.Direction = ParameterDirection.Input;
                if (userinfo.fpData[9] != null)
                {
                    sqlParamUserData.Size = userinfo.fpData[9].Length;
                    sqlParamUserData.Value = userinfo.fpData[9];
                }
                else
                    sqlParamUserData.Value = DBNull.Value;
                sqlCmd.Parameters.Add(sqlParamUserData);
                sqlCmd.Parameters.Add("@DeID", SqlDbType.VarChar).Value = SqlNull(DeviceID);
                sqlCmd.ExecuteNonQuery();
            }
            catch
            {

            }



        }
        catch (Exception e)
        {
            Error_Occured("Insert RealTimeLog Query", e.Message);
        }
        finally
        {
            sqlConn.Close();
            sqlConn.Dispose();
        }
    }
    static public object SqlNull(object obj)
    {
        if (obj == null)
            return DBNull.Value;

        return obj;
    }
    static public string GetStringFromBSCommBuffer(byte[] abytBSComm)
    {
        if (abytBSComm.Length < 4)
            return "";

        try
        {
            string sRet;

            int lenText = BitConverter.ToInt32(abytBSComm, 0);
            if (lenText > abytBSComm.Length - 4)
                return "";

            if (lenText == 0)
                return "";

            if (abytBSComm[4 + lenText - 1] == 0) // if last value of string buufer is 0x0
                sRet = System.Text.Encoding.UTF8.GetString(abytBSComm, 4, lenText - 1);
            else
                sRet = System.Text.Encoding.UTF8.GetString(abytBSComm, 4, lenText);

            return sRet;
        }
        catch
        {
            return "";
        }
    }

    protected string GetName(string UserId)
    {
        string Uname = "";
        try
        {
            string sSql = "Select NAME from tblemployee where cast(presentcardno as int)='" + UserId.Trim() + "'";
            DataSet DsU = new DataSet();
            DsU = cn.FillDataSet(sSql);
            if (DsU.Tables[0].Rows.Count > 0)
            {
                Uname = DsU.Tables[0].Rows[0][0].ToString().Trim();
            }
        }
        catch
        {
            Uname = "";
        }


        return Uname;
    }
    private void SetInfo(string UserID, string UserName, string UserPriv, int fpcount, int facecount, string Password, string CardNum)
    {

        try
        {
            msqlConn = FKWebTools.GetDBPool();
            if (msqlConn.State == ConnectionState.Closed)
            {
                msqlConn.Open();
            }
            JObject vResultJson = new JObject();
            JArray vEnrollDataArrayJson = new JArray();
            FKWebCmdTrans cmdTrans = new FKWebCmdTrans();
            string sUserPwd = Password;
            string sUserCard = CardNum;
            int index = 1;
            string sUserId = UserID;
            string sUserName = UserName;
            string sUserPriv = UserPriv;

            vResultJson.Add("user_id", sUserId);
            vResultJson.Add("user_name", sUserName);
            vResultJson.Add("user_privilege", sUserPriv);
            if (FKWebTools.mPhoto.Length > 0)
                vResultJson.Add("user_photo", FKWebTools.GetBinIndexString(index++));

            for (int nIndex = 0; nIndex <= FKWebTools.BACKUP_FP_9; nIndex++)
            {

                if (FKWebTools.mFinger[nIndex].Length > 0)
                {
                    JObject vEnrollDataJson = new JObject();
                    vEnrollDataJson.Add("backup_number", nIndex);
                    vEnrollDataJson.Add("enroll_data", FKWebTools.GetBinIndexString(index++));
                    vEnrollDataArrayJson.Add(vEnrollDataJson);
                }

            }
            if (sUserPwd.Length > 0)
            {
                JObject vEnrollDataJson = new JObject();
                vEnrollDataJson.Add("backup_number", FKWebTools.BACKUP_PSW);
                vEnrollDataJson.Add("enroll_data", FKWebTools.GetBinIndexString(index++));
                vEnrollDataArrayJson.Add(vEnrollDataJson);
            }

            if (sUserCard.Length > 0)
            {
                JObject vEnrollDataJson = new JObject();
                vEnrollDataJson.Add("backup_number", FKWebTools.BACKUP_CARD);
                vEnrollDataJson.Add("enroll_data", FKWebTools.GetBinIndexString(index++));
                vEnrollDataArrayJson.Add(vEnrollDataJson);
            }
            if (FKWebTools.mFace.Length > 0)
            {
                JObject vEnrollDataJson = new JObject();
                vEnrollDataJson.Add("backup_number", FKWebTools.BACKUP_FACE);
                vEnrollDataJson.Add("enroll_data", FKWebTools.GetBinIndexString(index++));
                vEnrollDataArrayJson.Add(vEnrollDataJson);
            }
            vResultJson.Add("enroll_data_array", vEnrollDataArrayJson);
            string sFinal = vResultJson.ToString(Formatting.None);

            byte[] binData = new byte[0];
            byte[] strParam = new byte[0];

            if (FKWebTools.mPhoto.Length > 0)
            {
                FKWebTools.AppendBinaryData(ref binData, FKWebTools.mPhoto);
            }

            for (int nIndex = 0; nIndex <= FKWebTools.BACKUP_FP_9; nIndex++)
            {

                if (FKWebTools.mFinger[nIndex].Length > 0)
                {
                    FKWebTools.AppendBinaryData(ref binData, FKWebTools.mFinger[nIndex]);
                }

            }
            if (sUserPwd.Length > 0)
            {
                byte[] mPwdBin = new byte[0];
                cmdTrans.CreateBSCommBufferFromString(sUserPwd, out mPwdBin);
                FKWebTools.ConcateByteArray(ref binData, mPwdBin);
            }
            if (sUserCard.Length > 0)
            {
                byte[] mCardBin = new byte[0];
                cmdTrans.CreateBSCommBufferFromString(sUserCard, out mCardBin);
                FKWebTools.ConcateByteArray(ref binData, mCardBin);
            }
            if (FKWebTools.mFace.Length > 0)
            {
                FKWebTools.AppendBinaryData(ref binData, FKWebTools.mFace);
            }

            cmdTrans.CreateBSCommBufferFromString(sFinal, out strParam);

            FKWebTools.ConcateByteArray(ref strParam, binData);

            FKWebTools.MakeCmdupload1(msqlConn, "SET_USER_INFO", selectTargetDevID, strParam, "", count);

        }
        catch (Exception Ex)
        {

        }
        finally
        {
            if (msqlConn.State == ConnectionState.Open)
            {
                msqlConn.Close();
            }
        }
    }

    protected void ASPxButton4_Click(object sender, EventArgs e)
    {
        try
        {
            int facecount = 0, fpcount = 0;
            string sSql = string.Empty;
            string sDevId = "";
            string sUserId = "", sUserName = "", sPreviledge = "";
            string Password = "", CardNum = "", UserName = "";
            FKWebCmdTrans cmdTrans = new FKWebCmdTrans();
            JObject vResultJson;
            string mDeviceId = "";


            for (int iU = 0; iU < ASPxGridView1.VisibleRowCount; iU++)
            {
                if (ASPxGridView1.Selection.IsRowSelected(iU) == true)
                {
                    try
                    {
                        facecount = 0; fpcount = 0;
                        Password = ""; CardNum = "";
                        msqlConn = FKWebTools.GetDBPool();
                        if (msqlConn.State != ConnectionState.Open)
                        {
                            msqlConn.Open();
                        }
                        //ASPxGridView2.GetRowValues(i, "Cloud ID").ToString().Trim();
                        string mUserId = Convert.ToString(ASPxGridView1.GetRowValues(iU, "Biometric ID").ToString().Trim());
                        string DeviceID = Convert.ToString(ASPxGridView1.GetRowValues(iU, "Cloud ID").ToString().Trim());

                        try
                        {
                            sSql = "select @cmd_result=cmd_result from tbl_fkcmd_trans_cmd_result where trans_id='" + mUserId + "'";
                            SqlCommand sqlCmd = new SqlCommand(sSql, msqlConn);
                            SqlParameter sqlParamCmdParamBin = new SqlParameter("@cmd_result", SqlDbType.VarBinary);
                            sqlParamCmdParamBin.Direction = ParameterDirection.Output;
                            sqlParamCmdParamBin.Size = -1;
                            sqlCmd.Parameters.Add(sqlParamCmdParamBin);

                            sqlCmd.ExecuteNonQuery();
                            if (msqlConn.State != ConnectionState.Closed)
                            {
                                msqlConn.Close();
                            }
                            byte[] bytCmdResult = null;
                            bytCmdResult = (byte[])sqlParamCmdParamBin.Value;
                            byte[] bytResultBin = new byte[0];
                            try
                            {
                                InsertUserinfo(bytCmdResult, DeviceID.Trim());
                            }

                            catch (Exception Ex)
                            {
                                Error_Occured("ASPxButton4_Click", Ex.Message);
                            }

                        }
                        catch
                        {


                        }
                    }
                    catch
                    {

                    }
                }
            }


            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Data Uploaded Successfully')", true);
            return;

        }
        catch (Exception Ex)
        {
            Error_Occured("Upload To DB", Ex.Message);
        }
    }

    protected void ASPxGridView2_ProcessColumnAutoFilter(object sender, ASPxGridViewAutoFilterEventArgs e)
    {
        if (e.Column.FieldName == "device_id")
        {
            if (e.Kind == GridViewAutoFilterEventKind.CreateCriteria)
            {
                string[] strContainerNumbers = e.Value.ToString().Split(',');
                Session["filter"] = e.Value;
                CriteriaOperator cop = new InOperator("device_id", strContainerNumbers);
                e.Criteria = cop;
            }
            else
            {
                if (Session["filter"] != null)
                    e.Value = Session["filter"].ToString();
            }
        }
    }
}