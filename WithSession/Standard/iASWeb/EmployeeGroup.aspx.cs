﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using GlobalSettings;
using System.Collections.Generic;
using DevExpress.Web;
using System.Data.SqlClient;
public partial class EmployeeGroup : System.Web.UI.Page
{
    Class_Connection Con = new Class_Connection();
    string Strsql = null;
    DataSet ds = null;
    int result = 0;
    ErrorClass ec = new ErrorClass();
    string DName, DHOD, DEmail;
    bool isNewRecord = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            if (Session["EmployeeGrpVisible"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }

            BindData();
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        BindData();
        GrdGrp.DataBind();
    }

    public void BindData()
    {
        try
        {
            if (Session["LoginUserName"].ToString().Trim().ToUpper()=="ADMIN")
            {

                Strsql = " SELECT [GroupID],[GroupName],[SHIFT],[SHIFTTYPE],[SHIFTPATTERN],[SHIFTREMAINDAYS],[LASTSHIFTPERFORMED],[INONLY],[ISPUNCHALL],[ISTIMELOSSALLOWED],[ALTERNATE_OFF_DAYS],[CDAYS] " +
                       " ,[ISROUNDTHECLOCKWORK],[ISOT],[OTRATE],[FIRSTOFFDAY],[SECONDOFFTYPE],[HALFDAYSHIFT],[SECONDOFFDAY],[PERMISLATEARRIVAL],[PERMISEARLYDEPRT],[ISAUTOSHIFT],[ISOUTWORK],[MAXDAYMIN],[ISOS],[AUTH_SHIFTS] " +
                       " ,[TIME],[SHORT],[HALF],[ISHALFDAY],[ISSHORT],[TWO],[isReleaver],[isWorker],[isFlexi],[SSN],[MIS],[IsCOF],[HLFAfter],[HLFBefore],[ResignedAfter],[EnableAutoResign],convert(varchar(5), S_End, 108) 'S_End'" +
                       " ,convert(varchar(5), S_Out, 108) 'S_Out',[AUTOSHIFT_LOW],[AUTOSHIFT_UP],[ISPRESENTONWOPRESENT],[ISPRESENTONHLDPRESENT],[NightShiftFourPunch],[ISAUTOABSENT],[ISAWA],[ISWA],[ISAW],[ISPREWO],[ISOTOUTMINUSSHIFTENDTIME]" +
                       " ,[ISOTWRKGHRSMINUSSHIFTHRS],[ISOTEARLYCOMEPLUSLATEDEP],[DEDUCTHOLIDAYOT],[DEDUCTWOOT],[ISOTEARLYCOMING],[OTMinus],[OTROUND],[OTEARLYDUR],[OTLATECOMINGDUR],[OTRESTRICTENDDUR],DUPLICATECHECKMIN,PREWO FROM [dbo].[tblEmployeeGroupPolicy] ";


            }
            else
            {

                Strsql = " SELECT [GroupID],[GroupName],[SHIFT],[SHIFTTYPE],[SHIFTPATTERN],[SHIFTREMAINDAYS],[LASTSHIFTPERFORMED],[INONLY],[ISPUNCHALL],[ISTIMELOSSALLOWED],[ALTERNATE_OFF_DAYS],[CDAYS] " +
                       " ,[ISROUNDTHECLOCKWORK],[ISOT],[OTRATE],[FIRSTOFFDAY],[SECONDOFFTYPE],[HALFDAYSHIFT],[SECONDOFFDAY],[PERMISLATEARRIVAL],[PERMISEARLYDEPRT],[ISAUTOSHIFT],[ISOUTWORK],[MAXDAYMIN],[ISOS],[AUTH_SHIFTS] " +
                       " ,[TIME],[SHORT],[HALF],[ISHALFDAY],[ISSHORT],[TWO],[isReleaver],[isWorker],[isFlexi],[SSN],[MIS],[IsCOF],[HLFAfter],[HLFBefore],[ResignedAfter],[EnableAutoResign],convert(varchar(5), S_End, 108) 'S_End'" +
                       " ,convert(varchar(5), S_Out, 108) 'S_Out',[AUTOSHIFT_LOW],[AUTOSHIFT_UP],[ISPRESENTONWOPRESENT],[ISPRESENTONHLDPRESENT],[NightShiftFourPunch],[ISAUTOABSENT],[ISAWA],[ISWA],[ISAW],[ISPREWO],[ISOTOUTMINUSSHIFTENDTIME]" +
                       " ,[ISOTWRKGHRSMINUSSHIFTHRS],[ISOTEARLYCOMEPLUSLATEDEP],[DEDUCTHOLIDAYOT],[DEDUCTWOOT],[ISOTEARLYCOMING],[OTMinus],[OTROUND],[OTEARLYDUR],[OTLATECOMINGDUR],[OTRESTRICTENDDUR],DUPLICATECHECKMIN,PREWO FROM [dbo].[tblEmployeeGroupPolicy] where CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";


                ds = Con.FillDataSet(Strsql);
                if(ds.Tables[0].Rows.Count>0)
                {
                    GrdGrp.DataSource = ds.Tables[0];
                    GrdGrp.DataBind();
                }
            }
            





        }
        catch
        {

        }
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Response.Redirect("EmployeeGroupEdit.aspx");
    }
    protected void GrdGrp_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        try
        {
            string IsMaster = Convert.ToString(e.Keys["GroupID"]);
            bool Validate = false;
            Validate = ValidData(IsMaster);
            if (Validate == true)
            {
                //ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Company Already Assigned To Employee')</script>");
                ((ASPxGridView)sender).JSProperties["cp_isSuccess"] = true;
                GrdGrp.CancelEdit();
                //e.Cancel = true;
                BindData();
                //return;

            }
            else
            {
                Strsql = "Delete From  [tblEmployeeGroupPolicy] where CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' and  GroupID='"+IsMaster.Trim()+"' ";
                Con.execute_NonQuery(Strsql);
            }

            GrdGrp.CancelEdit();
            e.Cancel = true;
            BindData();
        }
        catch
        {

        }
    }
    private bool ValidData(string CCode)
    {
        bool Check = false;
        try
        {
            Strsql = "Select * from tblemployee where CAT='" + CCode + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'  ";
            DataSet DsCheck = new DataSet();
            DsCheck = Con.FillDataSet(Strsql);
            if (DsCheck.Tables[0].Rows.Count > 0)
            {
                Check = true;

            }

        }
        catch (Exception Ex)
        {
            Error_Occured("ValidData", Ex.Message);
        }

        return Check;
    }

    protected void GrdGrp_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
    {
        ((ASPxGridView)sender).JSProperties["cp_isSuccess"] = false;
    }

    protected void GrdGrp_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
    {
        ((ASPxGridView)sender).JSProperties["cp_isSuccess"] = false;
    }

    protected void GrdGrp_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
    {
        ((ASPxGridView)sender).JSProperties["cp_isSuccess"] = false;
    }

    protected void GrdGrp_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
    {
        if (Session["AddGrp"].ToString().Trim() == "Y")
        {
            btnAdd.Enabled = true;
            if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.New)
            {
                e.Enabled = true;
            }
        }
        else
        {
            btnAdd.Enabled = false;
            if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.New)
            {
                e.Enabled = false;
            }
        }

        if (Session["EditGrp"].ToString().Trim() == "Y")
        {

            if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Edit)
            {
                e.Enabled = true;
            }
        }
        else
        {
            if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Edit)
            {
                e.Enabled = false;
            }
        }
        if (Session["DelGrp"].ToString().Trim() == "Y")
        {

            if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Delete)
            {
                e.Enabled = true;
            }
        }
        else
        {
            if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Delete)
            {
                e.Enabled = false;
            }
        }
    }
}