﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="frmEmployee.aspx.cs" Inherits="frmEmployee" %>
<%@ Register assembly="DevExpress.Web.v17.1, Version=17.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
        <script type="text/javascript">
        function ShowLoginWindow() {
            pcLogin.Show();
        }
       
    </script>
    <div>
        <table style="width:100%; border:solid;">
            <tr>
                <td align="center">
                    
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                    
                </td>
            </tr>
               <tr>
                <td>
                    <dx:ASPxButton ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click">
                        <Image IconID="people_ms_16x16devav">
                        </Image>
                    </dx:ASPxButton>
                     <dx:ASPxButton ID="btlDel" runat="server" Text="Delete" OnClick="btlDel_Click">
                         <Image IconID="actions_cancel_16x16office2013">
                         </Image>
                    </dx:ASPxButton>
                     <dx:ASPxButton ID="btnExport" runat="server" Text="Export" OnClick="btnExport_Click" >
                         <Image IconID="actions_download_16x16">
                         </Image>
                    </dx:ASPxButton>
                      <dx:ASPxButton ID="ASPxButton3" runat="server" Text="Upload To Device" Theme="SoftOrange">
                    <Image IconID="navigation_previous_16x16">
                    </Image>
                    <ClientSideEvents Click="function(s, e) { ShowLoginWindow(); }" />
                </dx:ASPxButton>
                </td>
                
            </tr>
            <tr>
                <td>
                    <dx:ASPxGridView ID="GrdEmp" runat="server" AutoGenerateColumns="False" Width="100%" KeyFieldName="PAYCODE" ClientInstanceName="GrdEmp" OnPageIndexChanged="GrdEmp_PageIndexChanged" OnCommandButtonInitialize="GrdEmp_CommandButtonInitialize"  >
                        <Settings ShowFilterRow="True" ShowTitlePanel="True"/>
                        <ClientSideEvents CustomButtonClick="function(s, e) {

var obj=GrdEmp.GetRowKey(e.visibleIndex)

	window.location = &quot;EmployeeEdit.aspx?Value=&quot; + obj;
}"  />
                        <SettingsAdaptivity AdaptivityMode="HideDataCells" />
                        <SettingsPager>
                            <PageSizeItemSettings Visible="true" Items="10, 20, 50,100" />
                        </SettingsPager>
                        <SettingsEditing Mode="PopupEditForm">
                        </SettingsEditing>
                        <Settings ShowFilterRow="True" />
                        <SettingsBehavior ConfirmDelete="True" />
                        <SettingsDataSecurity AllowEdit="False" AllowInsert="False" />
                        <SettingsPopup>
                            <EditForm Modal="True" Width="800px" HorizontalAlign="WindowCenter" 
                PopupAnimationType="Slide" VerticalAlign="WindowCenter" />
                        </SettingsPopup>
                        <SettingsSearchPanel Visible="True" />
                        <SettingsText PopupEditFormCaption="Employee" Title="Employee" />
                        <Columns>
                            <dx:GridViewCommandColumn ShowNewButton="true" ShowEditButton="true" VisibleIndex="1" ButtonRenderMode="Image" ShowDeleteButton="True" Caption="  ">
                                <CustomButtons >
                                    <dx:GridViewCommandColumnCustomButton ID="Edit"  >
                                        <Image ToolTip="Edit" IconID="actions_edit_16x16devav"/>
                                    </dx:GridViewCommandColumnCustomButton>
                                </CustomButtons>
                               
                            </dx:GridViewCommandColumn>
                            <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowClearFilterButton="True" ShowSelectCheckbox="True" VisibleIndex="0" Caption=" ">
                            </dx:GridViewCommandColumn>
                            <dx:GridViewDataTextColumn FieldName="ACTIVE" VisibleIndex="2">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="PAYCODE" VisibleIndex="3">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="EMPNAME" VisibleIndex="4">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GUARDIANNAME" VisibleIndex="5">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataDateColumn FieldName="DateOFBIRTH" VisibleIndex="6">
                            </dx:GridViewDataDateColumn>
                            <dx:GridViewDataDateColumn FieldName="DateOFJOIN" VisibleIndex="7">
                            </dx:GridViewDataDateColumn>
                            <dx:GridViewDataTextColumn FieldName="PRESENTCARDNO" VisibleIndex="8">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="COMPANYNAME" VisibleIndex="9">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="DivisionName" VisibleIndex="10">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="CATAGORYNAME" VisibleIndex="11">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="DEPARTMENTNAME" VisibleIndex="12">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="GradeName" VisibleIndex="13">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="SEX" VisibleIndex="14">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="ISMARRIED" VisibleIndex="15">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="DESIGNATION" VisibleIndex="16">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:ASPxGridView>
                </td>
            </tr>
         
        </table>
    </div>
   <div style="position:center">
           <dx:ASPxPopupControl ID="pcLogin" runat="server" Width="420" CloseAction="CloseButton" CloseOnEscape="true" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="Middle" ClientInstanceName="pcLogin" Theme="SoftOrange"
        HeaderText="Upload To Device" AllowDragging="True" PopupAnimationType="Fade"  EnableViewState="False" AutoUpdatePosition="true" OnLoad="pcLogin_Load" >
            <ClientSideEvents PopUp="function(s, e) { ASPxClientEdit.ClearGroup('entryGroup'); pcLogin.Focus(); }" />
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                 <dx:ASPxPanel ID="Panel1" runat="server" DefaultButton="btOK">
                    <PanelCollection>
                        <dx:PanelContent runat="server">
                            <dx:ASPxFormLayout runat="server" ID="ASPxFormLayout2" Width="100%" Height="100%">
                                <Items>

                                    <dx:LayoutItem Caption=" ">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>

                                                <asp:UpdatePanel runat="server" ID="upd" OnUnload="upd_Unload">
                                                    <ContentTemplate>
                                                        <dx:ASPxGridView ID="grdDevice" runat="server" AutoGenerateColumns="False" EnableTheming="True" KeyFieldName="SerialNumber" Theme="SoftOrange">
                                                            <Settings ShowFilterRow="True" />
                                                            <Columns>
                                                                <dx:GridViewCommandColumn Caption=" " SelectAllCheckboxMode="Page" ShowClearFilterButton="True" ShowSelectCheckbox="True" VisibleIndex="0">
                                                                </dx:GridViewCommandColumn>
                                                                <dx:GridViewDataTextColumn Caption="Serial Number" FieldName="SerialNumber" VisibleIndex="1" Width="150px">
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="Name" FieldName="DeviceName" VisibleIndex="2" Width="150px">
                                                                </dx:GridViewDataTextColumn>
                                                                 <dx:GridViewDataTextColumn Caption="Model" FieldName="Type" VisibleIndex="3" Width="150px">
                                                                </dx:GridViewDataTextColumn>
                                                            </Columns>
                                                        </dx:ASPxGridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>

                                    <dx:LayoutItem Caption=" ">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxLabel ID="lblStatus" runat="server" >
                                                </dx:ASPxLabel>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>
                                    </dx:LayoutItem>


                                  
                                    <dx:LayoutItem ShowCaption="False" Paddings-PaddingTop="19">
                                        <LayoutItemNestedControlCollection>
                                            <dx:LayoutItemNestedControlContainer>
                                                <dx:ASPxButton ID="btOK" runat="server" OnClick="btOK_Click" CausesValidation="true" ValidationGroup="entryGroup" Text="OK" Width="80px" AutoPostBack="true" Style="float: left; margin-right: 8px" Theme="SoftOrange" >
                                                    <ClientSideEvents Click="function(s, e) { if(ASPxClientEdit.ValidateGroup('entryGroup')) pcLogin.Hide(); }" />
                                                </dx:ASPxButton>
                                                <dx:ASPxButton ID="btCancel" runat="server" Text="Cancel" OnClick="btCancel_Click" Width="80px" AutoPostBack="False" Style="float: left; margin-right: 8px" Theme="SoftOrange" >
                                                    <ClientSideEvents Click="function(s, e) { pcLogin.Hide(); }" />
                                                </dx:ASPxButton>
                                            </dx:LayoutItemNestedControlContainer>
                                        </LayoutItemNestedControlCollection>

<Paddings PaddingTop="19px"></Paddings>
                                    </dx:LayoutItem>
                                </Items>
                            </dx:ASPxFormLayout>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxPanel>
            </dx:PopupControlContentControl>
            </ContentCollection>
            </dx:ASPxPopupControl>
    </div>
   
</asp:Content>

