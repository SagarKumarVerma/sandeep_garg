﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using GlobalSettings;
using System.Collections.Generic;
using DevExpress.Web;
public partial class frmGrade : System.Web.UI.Page
{
    Class_Connection Con = new Class_Connection();
    string Strsql = null;
    DataSet ds = null;
    int result = 0;
    ErrorClass ec = new ErrorClass();
    string Cname, CAdd, CSort, CPan;
    bool isNewRecord = false;


    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            if (Session["GradeVisible"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            BindData();
        }
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }

    protected void BindData()
    {
        try
        {
            if (Session["LoginUserName"].ToString().Trim().ToUpper() == "ADMIN")
            {
                Strsql = "Select * from tblGrade order by 1";
            }
            else
            {
                Strsql = "Select * from tblGrade where CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' order by 1";
            }
            ds = new DataSet();
            ds = Con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                GrdGrade.DataSource = ds.Tables[0];
                GrdGrade.DataBind();
            }

        }
        catch (Exception Ex)
        {
            Error_Occured("BindData", Ex.Message);
        }
		
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        BindData();
        GrdGrade.DataBind();
    }
    private void AddError(Dictionary<GridViewColumn, string> errors, GridViewColumn column, string errorText)
    {
        if (!errors.ContainsKey(column))
            errors[column] = errorText;
    }

    protected void GrdComp_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
    {
        string CCode = Convert.ToString(e.NewValues["GradeCode"]);
        string CName = Convert.ToString(e.NewValues["GradeName"]);
        // string k = e.Keys["COMPANYCODE"].ToString();
        if (CCode.ToString().Trim() == string.Empty)
        {
            AddError(e.Errors, GrdGrade.Columns[0], "Input Sub Contractor Code");

            e.RowError = "*Input Sub Contractor Code";
        }
        if (CName.ToString().Trim() == string.Empty)
        {
            AddError(e.Errors, GrdGrade.Columns[0], "Input Sub Contractor Name");
            e.RowError = "*Input Sub Contractor Name";
        }
        if (isNewRecord == true)
        {
            Strsql = "Select * from tblGrade where GradeCode='" + CCode + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' ";
            DataSet DsIP = new DataSet();
            DsIP = Con.FillDataSet(Strsql);
            if (DsIP.Tables[0].Rows.Count > 0)
            {
                AddError(e.Errors, GrdGrade.Columns[0], "Duplicate Sub Contractor Code");
                e.RowError = "*Duplicate Sub Contractor Code";
            }
        }




    }
    protected void GrdComp_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.Grid.IsNewRowEditing == false)
        {
            if (e.Column.FieldName == "GradeCode")
            {
                e.Editor.ReadOnly = true;
            }
            else
            {
                e.Editor.ReadOnly = false;
            }

        }
        else
        {
            isNewRecord = true;
            e.Editor.ReadOnly = false;
            
        }

    }
    protected void GrdComp_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        try
        {
            string k = e.Keys["GradeCode"].ToString();
            CAdd = e.NewValues["GradeName"].ToString();

            Strsql = "update tblGrade set GradeName='" + CAdd.Trim().ToUpper() + "' where GradeCode='" + k.Trim() + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'  ";
            Con.execute_NonQuery(Strsql);
            ec.InsertLog("Grade Record Updated", k, k, Session["LoginUserName"].ToString().Trim(), Session["LoginCompany"].ToString().Trim().Trim());
            e.Cancel = true;
            GrdGrade.CancelEdit();
            BindData();
            DataFilter.LoadGgrade(Session["LoginCompany"].ToString().Trim());
        }
        catch (Exception Ex)
        {

        }
    }
    protected void GrdComp_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        try
        {
            string k = e.NewValues["GradeCode"].ToString();
            CAdd = e.NewValues["GradeName"].ToString();
            Strsql = "insert into tblGrade (GradeCode,GradeName,CompanyCode)values ('" + k.Trim() + "','" + CAdd.Trim() + "','" + Session["LoginCompany"].ToString().Trim() + "')  ";
            Con.execute_NonQuery(Strsql);
            e.Cancel = true;
            ec.InsertLog("Grade Record Inserted", k, k, Session["LoginUserName"].ToString().Trim(), Session["LoginCompany"].ToString().Trim().Trim());
            GrdGrade.CancelEdit();
            BindData();
            DataFilter.LoadGgrade(Session["LoginCompany"].ToString().Trim());
        }
        catch (Exception Ex)
        {

        }
    }
    protected void GrdComp_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        try
        {
            string IsMaster = Convert.ToString(e.Keys["GradeCode"]);
            bool Validate = false;
            Validate = ValidData(IsMaster);
            if (Validate == true)
            {
                //ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Company Already Assigned To Employee')</script>");
                ((ASPxGridView)sender).JSProperties["cp_isSuccess"] = true;
                GrdGrade.CancelEdit();
               // e.Cancel = true;
                BindData();
               // return;

            }
            else
            {
                Strsql = "Delete From tblGrade where GradeCode='" + IsMaster.Trim() + "' and CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"' ";
                Con.execute_NonQuery(Strsql);
            }
            e.Cancel = true;
            GrdGrade.CancelEdit();
            ec.InsertLog("Grade Record Deleted", IsMaster, IsMaster, Session["LoginUserName"].ToString().Trim(), Session["LoginCompany"].ToString().Trim().Trim());
            DataFilter.LoadGgrade(Session["LoginCompany"].ToString().Trim());
            BindData();
        }
        catch(Exception Ex)
        {
            Error_Occured("ValidData", Ex.Message);
        }
    }
    private bool ValidData(string CCode)
    {
        bool Check = false;
        try
        {
            Strsql = "Select * from tblemployee where GradeCode='" + CCode + "' and CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"'  ";
            DataSet DsCheck = new DataSet();
            DsCheck = Con.FillDataSet(Strsql);
            if (DsCheck.Tables[0].Rows.Count > 0)
            {
                Check = true;

            }

        }
        catch (Exception Ex)
        {
            Check = false;
            Error_Occured("ValidData", Ex.Message);
        }

        return Check;
    }
    protected void GrdComp_PageIndexChanged(object sender, EventArgs e)
    {
        int pageIndex = (sender as ASPxGridView).PageIndex;
        GrdGrade.PageIndex = pageIndex;
        this.BindData();

    }

    protected void GrdGrade_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
    {
        ((ASPxGridView)sender).JSProperties["cp_isSuccess"] = false;
    }

    protected void GrdGrade_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
    {
        ((ASPxGridView)sender).JSProperties["cp_isSuccess"] = false;
    }

    protected void GrdGrade_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
    {
        ((ASPxGridView)sender).JSProperties["cp_isSuccess"] = false;
    }

    protected void GrdGrade_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
    {
        if (Session["AddGrade"].ToString().Trim() == "Y")
        {

            if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.New)
            {
                e.Enabled = true;
            }
        }
        else
        {
            if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.New)
            {
                e.Enabled = false;
            }
        }

        if (Session["EditGrade"].ToString().Trim() == "Y")
        {

            if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Edit)
            {
                e.Enabled = true;
            }
        }
        else
        {
            if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Edit)
            {
                e.Enabled = false;
            }
        }
        if (Session["DelGrade"].ToString().Trim() == "Y")
        {

            if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Delete)
            {
                e.Enabled = true;
            }
        }
        else
        {
            if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Delete)
            {
                e.Enabled = false;
            }
        }
    }
}