﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="Holiday.aspx.cs" Inherits="Holiday" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
     <script type="text/javascript">
        function CloseGridLookup() {
            GridLookup.ConfirmCurrentSelection();
            GridLookup.HideDropDown();
            GridLookup.Focus();
        }
          </script> 
     <script type="text/javascript">
        function ExitD() {
            GDep.ConfirmCurrentSelection();
            GDep.HideDropDown();
            GDep.Focus();
        }
   </script> 
    <style>
        .center {
  margin: auto;
  width: 50%;
  border: 3px solid green;
  padding: 10px;
}
    </style>
  <%--  <div style="border-color: #808080; border-style: solid; width:500px; align-self:center; position: center;">--%>
        <table align="center" cellpadding="0" cellspacing="0" style="border: medium solid #666666;" >
            <tr>
                <td colspan="3" style="padding: 5px; ">
                    <center>
                        <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Holiday">
                        </dx:ASPxLabel>
                    </center>
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Date" >
                    </dx:ASPxLabel>
                </td>
                <td style="padding: 5px">
                    <dx:ASPxDateEdit ID="txtDate" runat="server" EditFormatString="dd/MM/yyyy" OnCalendarDayCellPrepared="txtDate_CalendarDayCellPrepared" >
                        <ValidationSettings ValidationGroup="MK">
                            <RequiredField IsRequired="True" />
                        </ValidationSettings>
                    </dx:ASPxDateEdit>
                </td>
                <td style="padding: 5px">&nbsp;</td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Reason">
                    </dx:ASPxLabel>
                </td>
                <td rowspan="2" style="padding: 5px">
                    <dx:ASPxMemo ID="txtReason" runat="server" Height="71px" Width="170px" Theme="SoftOrange">
                        <ValidationSettings ValidationGroup="MK">
                            <RequiredField IsRequired="True" />
                        </ValidationSettings>
                    </dx:ASPxMemo>
                </td>
                <td style="padding: 5px">&nbsp;</td>
            </tr>
            <tr>
                <td style="padding: 5px">&nbsp;</td>
                <td style="padding: 5px">&nbsp;</td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="OT Factor">
                    </dx:ASPxLabel>
                </td>
                <td style="padding: 5px">
                    <dx:ASPxTextBox ID="txtOT" runat="server" Width="170px">
                    </dx:ASPxTextBox>
                </td>
                <td style="padding: 5px">&nbsp;</td>
            </tr>
            <tr>
                <td style="padding: 5px; height: 36px;">
                    <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="Adjust Against">
                    </dx:ASPxLabel>
                </td>
                <td style="padding: 5px; height: 36px;">
                    <dx:ASPxDateEdit ID="txtDateAdjusted" runat="server" EditFormatString="dd/MM/yyyy">
                    </dx:ASPxDateEdit>
                </td>
                <td style="padding: 5px; height: 36px;"></td>
            </tr>
            <tr>
                <td style="padding: 5px; height: 34px;">
                    <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="Select Company">
                    </dx:ASPxLabel>
                </td>
                <td style="padding: 5px; height: 34px;">
                    <dx:ASPxGridLookup ID="GridLookup" runat="server" SelectionMode="Multiple"  ClientInstanceName="gridLookup"
                                                                            KeyFieldName="companycode"  TextFormatString="{0}" MultiTextSeparator=",">
                                                                            <Columns>
                                                                                <dx:GridViewCommandColumn ShowSelectCheckbox="True" Caption="" Width="10px" />
                                                                                <dx:GridViewDataColumn FieldName="companycode" Caption="Code" Width="50px" />
                                                                                <dx:GridViewDataColumn FieldName="compDetails"  Caption="Name"/>
                                                                            </Columns>
                                                                            <GridViewProperties>
                                                                                <Templates>
                                                                                    <StatusBar>
                                                                                        <table class="OptionsTable" style="float: right">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </StatusBar>
                                                                                </Templates>
                                                                                <Settings ShowFilterRow="True" ShowStatusBar="Visible" />
                                                                                <SettingsPager PageSize="7" EnableAdaptivity="true" />
                                                                            </GridViewProperties>
                                                                        </dx:ASPxGridLookup>
                </td>
                <td style="padding: 5px; height: 34px;"></td>
            </tr>
            <tr>
                <td style="padding: 5px">
                    <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="Select Department">
                    </dx:ASPxLabel>
                </td>
                <td style="padding: 5px">
                   <dx:ASPxGridLookup ID="GDep" runat="server" ClientInstanceName="GDep" KeyFieldName="departmentcode" MultiTextSeparator="," SelectionMode="Multiple" TextFormatString="{0}">
                                                                            <Columns>
                                                                                <dx:GridViewCommandColumn Caption="" ShowSelectCheckbox="True" Width="10px" />
                                                                                <dx:GridViewDataColumn Caption="Code" FieldName="departmentcode" Width="50px" />
                                                                                <dx:GridViewDataColumn Caption="Name" FieldName="deptDetails"  />
                                                                            </Columns>
                                                                            <GridViewProperties>
                                                                                <Templates>
                                                                                    <StatusBar>
                                                                                        <table class="OptionsTable" style="float: right">
                                                                                            <tr>
                                                                                                <td>
                                                                                                   
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </StatusBar>
                                                                                </Templates>
                                                                                <Settings ShowFilterRow="True" ShowStatusBar="Visible" />
                                                                                <SettingsPager EnableAdaptivity="true" PageSize="7" />
                                                                            </GridViewProperties>
                                                                        </dx:ASPxGridLookup>
                </td>
                <td style="padding: 5px">&nbsp;</td>
            </tr>
            <tr>
                <td style="padding: 5px">&nbsp;</td>
                <td style="padding: 5px">&nbsp;</td>
                <td style="padding: 5px">&nbsp;</td>
            </tr>
            <tr>
                <td style="padding: 5px" colspan="2">
                    <dx:ASPxLabel ID="lblStatus" runat="server" ForeColor="#FF3300">
                    </dx:ASPxLabel>
                </td>
            </tr>
            <tr>
                <td style="padding: 5px">&nbsp;</td>
                <td style="padding: 5px">
                    <dx:ASPxButton ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" ValidationGroup="MK">
                    </dx:ASPxButton>
                </td>
                <td style="padding: 5px">&nbsp;</td>
            </tr>
        </table>
   <%-- </div>--%>
</asp:Content>

