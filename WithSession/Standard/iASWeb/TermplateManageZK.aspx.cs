﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using FKWeb;
using Newtonsoft.Json.Linq;
using DevExpress.Web;

public partial class TermplateManageZK : System.Web.UI.Page
{

    OleDbConnection MyConn = new OleDbConnection();
    OleDbCommand MyCmd = new OleDbCommand();
    Class_Connection cn = new Class_Connection();
    public string KeyValue = "";
    public string TableMain = null;
    public string TableName = null;
    public string ColumnName = null;
    const int GET_DEVICE_STATUS = 1;
    const int DEL_USER_INFO = 3;
    public string sSql = "";

    SqlConnection msqlConn;
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["TimeWatchConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            if (Session["LoginUserType"] == null || Session["LoginUserType"].ToString().Trim() == "U")
            {
                Response.Redirect("Login.aspx");
            }
            GetDevices();
            GetUser();
            msqlConn = FKWebTools.GetDBPool();


        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        GetUser();
        ASPxGridView1.DataBind();
        GetDevices();
        ASPxGridView2.DataBind();
    }
    public void GetDevices()
    {
        try
        {
            if (Session["username"].ToString().Trim().ToUpper() == "ADMIN")
            {
                sSql = "select SerialNumber,DeviceName,CDM.GroupName  from tblMachine M left outer join ClientDeviceGroup CDM on CDM.GroupID=M.GroupID " +
                        "where M.devicemode='ZKT' ";
            }
            else
            {
                sSql = "select SerialNumber,DeviceName,CDM.GroupName  from tblMachine M left outer join ClientDeviceGroup CDM on CDM.GroupID=M.GroupID " +
                       "where M.devicemode='ZKT' and M.CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' ";
            }

            DataSet DsDev = new DataSet();
            DsDev = cn.FillDataSet(sSql);
            if (DsDev.Tables[0].Rows.Count > 0)
            {
                ASPxGridView2.DataSource = DsDev;
                ASPxGridView2.DataBind();
            }
        }
        catch
        {

        }
    }
    public void GetUser()
    {
        try
        {
            //sSql = "select distinct UserID, RowID,DeviceID as SerialNumber,Name,M.branch from UserDetail UD " +
            //         " inner join tblMachine M on UD.DeviceID=M.SerialNumber where M.devicemode='ZKT' and  M.CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'   ";

            sSql = " select distinct UserID, RowID,DeviceID as SerialNumber,TE.EMPNAME,tblDepartment.DepartmentName,tblDivision.DivisionName, M.DeviceName from UserDetail UD " +
                   " inner join tblMachine M on UD.DeviceID=M.SerialNumber left outer join tblemployee TE on cast (TE.PRESENTCARDNO as int)=UD.UserID " +
                   " left outer join tbldepartment on tbldepartment.departmentcode=TE.departmentcode left outer join tblDivision on tblDivision.Divisioncode=TE.DivisionCode " +
                   " where M.devicemode='ZKT' and  M.CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";

            DataSet DsU = new DataSet();
            DsU = cn.FillDataSet(sSql);
            if (DsU.Tables[0].Rows.Count > 0)
            {
                ASPxGridView1.DataSource = DsU;
                ASPxGridView1.DataBind();
            }
        }
        catch
        {

        }
    }

    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        try
        {
            #region Validation

            string sDevId = "";

            DataTable dt = new DataTable();
            DataColumn dtColumn;
            DataRow myDataRow;
            string mDevice = "";
            string userid = "", UserName = "", Privilege = "", Password = "", CardNumber = "", Group = "", TimeZone = "", AccessTimeFrom = "", AccessTimeTo = "", Verify = "";
            string FPID = "", Valid = "", FPTMP = "", Size = "";

            // create Name column.

            dtColumn = new DataColumn();
            dtColumn.DataType = System.Type.GetType("System.String");
            dtColumn.ColumnName = "FromDevice";
            dtColumn.Caption = "FromDevice";
            dt.Columns.Add(dtColumn);

            dtColumn = new DataColumn();
            dtColumn.DataType = System.Type.GetType("System.String");
            dtColumn.ColumnName = "UserID";
            dtColumn.Caption = "UserID";
            dt.Columns.Add(dtColumn);

            dtColumn = new DataColumn();
            dtColumn.DataType = System.Type.GetType("System.String");
            dtColumn.ColumnName = "UserName";
            dtColumn.Caption = "UserName";
            dt.Columns.Add(dtColumn);

            dtColumn = new DataColumn();
            dtColumn.DataType = System.Type.GetType("System.String");
            dtColumn.ColumnName = "Privilege";
            dtColumn.Caption = "Privilege";
            dt.Columns.Add(dtColumn);

            dtColumn = new DataColumn();
            dtColumn.DataType = System.Type.GetType("System.String");
            dtColumn.ColumnName = "Password";
            dtColumn.Caption = "Password";
            dt.Columns.Add(dtColumn);

            dtColumn = new DataColumn();
            dtColumn.DataType = System.Type.GetType("System.String");
            dtColumn.ColumnName = "CardNumber";
            dtColumn.Caption = "CardNumber";
            dt.Columns.Add(dtColumn);

            dtColumn = new DataColumn();
            dtColumn.DataType = System.Type.GetType("System.String");
            dtColumn.ColumnName = "Group";
            dtColumn.Caption = "Group";
            dt.Columns.Add(dtColumn);


            dtColumn = new DataColumn();
            dtColumn.DataType = System.Type.GetType("System.String");
            dtColumn.ColumnName = "TimeZone";
            dtColumn.Caption = "TimeZone";
            dt.Columns.Add(dtColumn);

            dtColumn = new DataColumn();
            dtColumn.DataType = System.Type.GetType("System.String");
            dtColumn.ColumnName = "AccessTimeFrom";
            dtColumn.Caption = "AccessTimeFrom";
            dt.Columns.Add(dtColumn);

            dtColumn = new DataColumn();
            dtColumn.DataType = System.Type.GetType("System.String");
            dtColumn.ColumnName = "AccessTimeTo";
            dtColumn.Caption = "AccessTimeTo";
            dt.Columns.Add(dtColumn);

            dtColumn = new DataColumn();
            dtColumn.DataType = System.Type.GetType("System.String");
            dtColumn.ColumnName = "Verify";
            dtColumn.Caption = "Verify";
            dt.Columns.Add(dtColumn);

            dtColumn = new DataColumn();
            dtColumn.DataType = System.Type.GetType("System.String");
            dtColumn.ColumnName = "FPID";
            dtColumn.Caption = "FPID";
            dt.Columns.Add(dtColumn);

            dtColumn = new DataColumn();
            dtColumn.DataType = System.Type.GetType("System.String");
            dtColumn.ColumnName = "Valid";
            dtColumn.Caption = "Valid";
            dt.Columns.Add(dtColumn);

            dtColumn = new DataColumn();
            dtColumn.DataType = System.Type.GetType("System.String");
            dtColumn.ColumnName = "FPTMP";
            dtColumn.Caption = "FPTMP";
            dt.Columns.Add(dtColumn);

            dtColumn = new DataColumn();
            dtColumn.DataType = System.Type.GetType("System.String");
            dtColumn.ColumnName = "Size";
            dtColumn.Caption = "Size";
            dt.Columns.Add(dtColumn);

            List<object> KeyUser = ASPxGridView1.GetSelectedFieldValues(new string[] { ASPxGridView1.KeyFieldName });
            List<object> KeyDevice = ASPxGridView2.GetSelectedFieldValues(new string[] { ASPxGridView2.KeyFieldName });
            if (KeyUser.Count == 0 || KeyDevice.Count == 0)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Please Select Atleast One Device/User')</script>");
                return;
            }


            #endregion

            for (int i = 0; i < ASPxGridView1.VisibleRowCount; i++)
            {

                if (ASPxGridView1.Selection.IsRowSelected(i) == true)
                {

                    sDevId = ASPxGridView1.GetRowValues(i, "SerialNumber").ToString().Trim();
                    userid = ASPxGridView1.GetRowValues(i, "UserID").ToString().Trim();
                    TextBox lblSerial = (TextBox)ASPxGridView1.FindControl("SerialNumber");
                    string sql = "select * from userdetail where userid ='" + userid + "' and DeviceID='" + sDevId + "'";
                    DataSet DsTemp = new DataSet();
                    DsTemp = cn.FillDataSet(sql);

                    if (DsTemp.Tables[0].Rows.Count > 0)
                    {
                        UserName = DsTemp.Tables[0].Rows[0]["Name"].ToString();
                        Privilege = DsTemp.Tables[0].Rows[0]["Pri"].ToString();
                        Password = DsTemp.Tables[0].Rows[0]["Password"].ToString();
                        CardNumber = DsTemp.Tables[0].Rows[0]["Card"].ToString();
                        Group = DsTemp.Tables[0].Rows[0]["DeviceGroup"].ToString();
                        TimeZone = DsTemp.Tables[0].Rows[0]["TimeZone"].ToString();
                        AccessTimeFrom = DsTemp.Tables[0].Rows[0]["AccesstimeFrom"].ToString();
                        AccessTimeTo = DsTemp.Tables[0].Rows[0]["AccessTimeTo"].ToString();
                        myDataRow = dt.NewRow();
                        myDataRow["FromDevice"] = sDevId;
                        myDataRow["UserID"] = userid.ToString();
                        myDataRow["UserName"] = UserName.ToString();
                        myDataRow["Privilege"] = Privilege.ToString();
                        myDataRow["Password"] = Password.ToString();
                        myDataRow["CardNumber"] = CardNumber.ToString();
                        myDataRow["Group"] = Group.ToString();
                        myDataRow["TimeZone"] = TimeZone;
                        myDataRow["AccessTimeFrom"] = AccessTimeFrom;
                        myDataRow["AccessTimeTo"] = AccessTimeTo;
                        dt.Rows.Add(myDataRow);
                    }

                }




            }


            for (int i = 0; i < ASPxGridView2.VisibleRowCount; i++)
            {

                if (ASPxGridView2.Selection.IsRowSelected(i) == true)
                {

                    Session["DeviceSerialNumber"] = ASPxGridView2.GetRowValues(i, "SerialNumber").ToString().Trim();
                    mDevice = ASPxGridView2.GetRowValues(i, "SerialNumber").ToString().Trim();
                    foreach (DataRow dtr in dt.Rows)
                    {

                        string s = "insert into DeviceCommands(SerialNumber,CommandContent,TransfertoDevice,UserID,Executed,IsOnline,CreatedOn,Priority) values('" + dtr["FromDevice"] + "','flagupduserinfo','" + mDevice + "','" + dtr["userid"] + "',0,1,getdate(),'6')";
                        int RowsCount = cn.execute_NonQuery(s);

                         s = "insert into DeviceCommands(SerialNumber,CommandContent,TransfertoDevice,UserID,Executed,IsOnline,CreatedOn,Priority) values('" + dtr["FromDevice"] + "','flagsetfp','" + mDevice + "','" + dtr["userid"] + "',0,1,getdate(),'6')";
                         RowsCount = cn.execute_NonQuery(s);

                         s = "insert into DeviceCommands(SerialNumber,CommandContent,TransfertoDevice,UserID,Executed,IsOnline,CreatedOn,Priority) values('" + dtr["FromDevice"] + "','flagsetface','" + mDevice + "','" + dtr["userid"] + "',0,1,getdate(),'6')";
                         RowsCount = cn.execute_NonQuery(s);





                        #region check device is online from database
                        DataSet dstime = new DataSet();

                        string sqltime = "select SerialNumber,CONVERT(VARCHAR(12), DATEDIFF(minute, updatedon, GETDATE()), 114) as UpdateTimeDiff from tblmachine where SerialNumber='" + mDevice + "'";
                        dstime = cn.FillDataSet(sqltime);
                        if (dstime.Tables[0].Rows.Count > 0)
                        {

                            int Diffrence = Convert.ToInt32(dstime.Tables[0].Rows[0]["UpdateTimeDiff"].ToString());
                            if (Diffrence <= 2)
                            {

                            }
                            else
                            {

                                s = "update DeviceCommands set IsOnline=0  where TransferToDevice='" + mDevice + "' and Executed=0  and commandContent='flagupduserinfo'";
                                cn.execute_NonQuery(s);
                            }


                        }

                        #endregion


                    }
                }

                Session["DeviceSerialNumber"] = null;

            }


            for (int i = 0; i < ASPxGridView2.VisibleRowCount; i++)
            {

                if (ASPxGridView2.Selection.IsRowSelected(i) == true)
                {
                    mDevice = ASPxGridView2.GetRowValues(i, "SerialNumber").ToString().Trim();
                    string s = "insert into DeviceCommands(SerialNumber,CommandContent,transfertodevice,Executed,IsOnline,CreatedOn,Priority) values('" + mDevice + "','flaginfo','" + mDevice + "',0,1,getdate(),'7')";
                    cn.execute_NonQuery(s);
                }
            }
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Data Upload Command Sent Successfully')", true);
            return;
        }
        catch
        {

        }
    }

    protected void ASPxButton2_Click(object sender, EventArgs e)
    {
        try
        {
            #region Validation

            string sDevId = "";
            List<object> KeyUser = ASPxGridView1.GetSelectedFieldValues(new string[] { ASPxGridView1.KeyFieldName });
            List<object> KeyDevice = ASPxGridView2.GetSelectedFieldValues(new string[] { ASPxGridView2.KeyFieldName });
            if (KeyUser.Count == 0 || KeyDevice.Count == 0)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Please Select Atleast One Device/User')</script>");
                return;
            }


            #endregion

            for (int i = 0; i < KeyUser.Count; i++)
            {
                string UseriD = Convert.ToString(KeyDevice[i]);
                for (int DevRec = 0; DevRec < KeyDevice.Count; DevRec++)
                {
                    sDevId = Convert.ToString(KeyDevice[DevRec]);
                    string s = "insert into DeviceCommands(SerialNumber,CommandContent,userid,TransferToDevice,Executed,IsOnline,CreatedOn,Priority) values('" + sDevId + "','flagdeluserinfo'," + UseriD + ",'" + sDevId + "',0,1,getdate(),'4')";
                    cn.execute_NonQuery(s);

                }
                sSql = "insert into DeviceCommands(SerialNumber,CommandContent,userid,TransferToDevice,Executed,IsOnline,CreatedOn,Priority) values('" + sDevId + "','flaginfo'," + UseriD + ",'" + sDevId + "',0,1,getdate(),'7')";
                cn.execute_NonQuery(sSql);
            }


        }
        catch
        {

        }
    }

    protected void ASPxGridView1_PageIndexChanged(object sender, EventArgs e)
    {
        int pageIndex = (sender as ASPxGridView).PageIndex;

        ASPxGridView1.PageIndex = pageIndex;
        this.GetUser();
    }

    protected void ASPxGridView2_PageIndexChanged(object sender, EventArgs e)
    {
        int pageIndex = (sender as ASPxGridView).PageIndex;

        ASPxGridView2.PageIndex = pageIndex;
        this.GetDevices();

    }
}