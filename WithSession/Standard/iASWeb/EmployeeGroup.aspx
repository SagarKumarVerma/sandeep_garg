﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="EmployeeGroup.aspx.cs" Inherits="EmployeeGroup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    
    <script language="javascript" type="text/javascript" src="JS/validation_Employee.js"></script>
     <div align="center">
         <table align="center" style="width: 100%" cellpadding="0" cellspacing="0">
             <tr>
                 <td  padding="5px">
                     <dx:ASPxGridView ID="GrdGrp" runat="server" Width="100%" AutoGenerateColumns="False" Theme="SoftOrange" KeyFieldName="GroupID" ClientInstanceName="GrdGrp" OnRowDeleting="GrdGrp_RowDeleting" OnInitNewRow="GrdGrp_InitNewRow" OnRowDeleted="GrdGrp_RowDeleted" OnStartRowEditing="GrdGrp_StartRowEditing" OnCommandButtonInitialize="GrdGrp_CommandButtonInitialize">
                         <%-- <SettingsPager>
                             <PageSizeItemSettings Visible="true" Items="10, 20, 50,100" />
                         </SettingsPager>
                         <Settings ShowFilterRow="True" />
                         <SettingsCommandButton>
                             <EditButton Text=" ">
                                 <Image IconID="actions_edit_16x16devav" ToolTip="Edit">
                                 </Image>
                             </EditButton>
                             <DeleteButton Text=" ">
                                 <Image IconID="actions_trash_16x16" ToolTip="Delete">
                                 </Image>
                             </DeleteButton>
                         </SettingsCommandButton>
                         <SettingsDataSecurity AllowInsert="False" />
                         <SettingsSearchPanel Visible="True" />
                         <SettingsBehavior ConfirmDelete="True" />--%>
                         <Settings ShowFilterRow="True" ShowTitlePanel="True"/>
                         <ClientSideEvents CustomButtonClick="function(s, e) {

var obj=GrdGrp.GetRowKey(e.visibleIndex)

	window.location = &quot;EmployeeGroupEdit.aspx?Value=&quot; + obj;
}"  />
                         	
				 <ClientSideEvents EndCallback="function(s, e) {
	 if (s.cp_isSuccess) 
{ alert('Can Not Delete Record As It Is Already Assigned To Employee'); 
delete s.isSuccess; } 
}" />
                         <SettingsAdaptivity AdaptivityMode="HideDataCells" />
                         <SettingsPager>
                             <PageSizeItemSettings Visible="true" Items="10, 20, 50,100" />
                         </SettingsPager>
                         <SettingsEditing Mode="PopupEditForm">
                         </SettingsEditing>
                         <Settings ShowFilterRow="True" />
                         <SettingsBehavior ConfirmDelete="True" />
                         <SettingsCommandButton>
                             <DeleteButton Text=" ">
                                 <Image IconID="actions_trash_16x16">
                                 </Image>
                             </DeleteButton>
                         </SettingsCommandButton>
                         <SettingsDataSecurity AllowEdit="False" AllowInsert="False" />
                         <SettingsPopup>
                             <EditForm Modal="True" Width="800px" HorizontalAlign="WindowCenter" 
                PopupAnimationType="Slide" VerticalAlign="WindowCenter" />
                         </SettingsPopup>
                         <SettingsSearchPanel Visible="True" />
                         <SettingsText PopupEditFormCaption="Employee Group" Title="Employee Group" />
                         <Columns>
                              <dx:GridViewCommandColumn ShowNewButton="true" ShowEditButton="true" VisibleIndex="1" ButtonRenderMode="Image" Caption="  " >
                                <CustomButtons >
                                    <dx:GridViewCommandColumnCustomButton ID="Edit"  >
                                        <Image ToolTip="Edit" IconID="actions_edit_16x16devav"/>
                                    </dx:GridViewCommandColumnCustomButton>
                                </CustomButtons>
                            </dx:GridViewCommandColumn>
                             <dx:GridViewCommandColumn Caption=" " ShowClearFilterButton="True" ShowDeleteButton="True" ShowInCustomizationForm="True" VisibleIndex="0">
                             </dx:GridViewCommandColumn>
                             <dx:GridViewDataTextColumn VisibleIndex="2" FieldName="GroupID" Caption="Group ID">
                             </dx:GridViewDataTextColumn>
                             <dx:GridViewDataTextColumn VisibleIndex="3" FieldName="GroupName" >
                             </dx:GridViewDataTextColumn>
                             <dx:GridViewDataTextColumn VisibleIndex="4" FieldName="SHIFT" Caption="Shift">
                             </dx:GridViewDataTextColumn>
                             <dx:GridViewDataTextColumn VisibleIndex="5" FieldName="SHIFTTYPE" Caption="Shift Type">
                             </dx:GridViewDataTextColumn>
                             <dx:GridViewDataTextColumn VisibleIndex="11" FieldName="ALTERNATE_OFF_DAYS" Caption="Alternate Off Days">
                             </dx:GridViewDataTextColumn>
                             <dx:GridViewDataTextColumn VisibleIndex="14" FieldName="OTRATE" Caption="OT Rate">
                             </dx:GridViewDataTextColumn>
                             <dx:GridViewDataTextColumn VisibleIndex="15" FieldName="FIRSTOFFDAY" Caption="First Off Day">
                             </dx:GridViewDataTextColumn>
                             <dx:GridViewDataTextColumn VisibleIndex="16" FieldName="SECONDOFFDAY" Caption="Second Off Day">
                             </dx:GridViewDataTextColumn>
                             <dx:GridViewDataTextColumn VisibleIndex="18" FieldName="PERMISLATEARRIVAL" Caption="Permis Late ">
                             </dx:GridViewDataTextColumn>
                             <dx:GridViewDataTextColumn VisibleIndex="19" FieldName="PERMISEARLYDEPRT" Caption="Permis Early Departure">
                             </dx:GridViewDataTextColumn>
                             <dx:GridViewDataTextColumn VisibleIndex="24" FieldName="MAXDAYMIN" Caption="Max Working Hours In A Day">
                             </dx:GridViewDataTextColumn>
                             <dx:GridViewDataTextColumn VisibleIndex="17" FieldName="SECONDOFFTYPE" Caption="Second Off Type">
                             </dx:GridViewDataTextColumn>
                             <dx:GridViewDataTextColumn VisibleIndex="23" FieldName="SHORT" Caption="Max Working For Short Day">
                             </dx:GridViewDataTextColumn>
                             <dx:GridViewDataTextColumn VisibleIndex="22" FieldName="HALF" Caption="Max Working For Half Day">
                             </dx:GridViewDataTextColumn>
                             <dx:GridViewDataTextColumn VisibleIndex="21" FieldName="TIME" Caption="Present Marking Duration">
                             </dx:GridViewDataTextColumn>
                             <dx:GridViewDataCheckColumn Caption="Is RTC" FieldName="ISROUNDTHECLOCKWORK" VisibleIndex="12">
                                 <PropertiesCheckEdit ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                                 </PropertiesCheckEdit>
                             </dx:GridViewDataCheckColumn>
                             <dx:GridViewDataCheckColumn Caption="Is OT " FieldName="ISOT" VisibleIndex="13">
                                 <PropertiesCheckEdit ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                                 </PropertiesCheckEdit>
                             </dx:GridViewDataCheckColumn>
                             <dx:GridViewDataCheckColumn Caption="Is Half Day" FieldName="ISHALFDAY" VisibleIndex="6">
                                 <PropertiesCheckEdit ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                                 </PropertiesCheckEdit>
                             </dx:GridViewDataCheckColumn>
                             <dx:GridViewDataCheckColumn Caption="Is Short" FieldName="ISSHORT" VisibleIndex="7">
                                 <PropertiesCheckEdit ValueChecked="N" ValueType="System.String" ValueUnchecked="N">
                                 </PropertiesCheckEdit>
                             </dx:GridViewDataCheckColumn>
                             <dx:GridViewDataCheckColumn Caption="In Only" FieldName="INONLY" VisibleIndex="8">
                                 <PropertiesCheckEdit ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                                 </PropertiesCheckEdit>
                             </dx:GridViewDataCheckColumn>
                             <dx:GridViewDataCheckColumn Caption="Is Auto Shift" FieldName="ISAUTOSHIFT" VisibleIndex="9">
                                 <PropertiesCheckEdit ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                                 </PropertiesCheckEdit>
                             </dx:GridViewDataCheckColumn>
                             <dx:GridViewDataCheckColumn Caption="Is Time Loss" FieldName="ISTIMELOSSALLOWED" VisibleIndex="10">
                                 <PropertiesCheckEdit ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                                 </PropertiesCheckEdit>
                             </dx:GridViewDataCheckColumn>
                             <dx:GridViewDataCheckColumn Caption="Is OutWork" FieldName="ISOUTWORK" VisibleIndex="20">
                                 <PropertiesCheckEdit ValueChecked="Y" ValueType="System.String" ValueUnchecked="N">
                                 </PropertiesCheckEdit>
                             </dx:GridViewDataCheckColumn>
                         </Columns>
                     </dx:ASPxGridView>
                 </td>
             </tr>
             <tr>
                 <td>
                     <dx:ASPxButton ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" >
                     </dx:ASPxButton>
                 </td>
             </tr>
         </table>
    </div>
</asp:Content>

