﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="TemplateHTSeries.aspx.cs" Inherits="TemplateHTSeries" %>
<%@ Register assembly="DevExpress.Web.v17.1, Version=17.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
      <div style="border-style: solid; border-width: thin; border-color: inherit;">
    <table class="dxeBinImgCPnlSys">
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <dx:ASPxTextBox ID="mTransIdTxt" runat="server" Width="170px" Visible="false">
                </dx:ASPxTextBox>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="left">
                 <table>
                    <tr>
                        <td>
                            &nbsp;</td>
                         <td>
                             &nbsp;</td>
                        </tr>
                     </table>      
                </td>
            <td align="center">
                <table>
                    <tr>
                        <td>
                            <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Upload" Theme="SoftOrange" OnClick="ASPxButton1_Click">
                    <Image IconID="miscellaneous_publish_16x16">
                    </Image>
                </dx:ASPxButton>
                        </td>
                         <td>
                            <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Delete" Theme="SoftOrange" OnClick="ASPxButton2_Click" >
                    <Image IconID="actions_trash_16x16">
                    </Image>
                </dx:ASPxButton>

                        </td>
                        <td>
                            <dx:ASPxCheckBox ID="chkAdmin" Text="Make Admin" runat="server" Theme="SoftOrange"></dx:ASPxCheckBox>
                        </td>
                    </tr>
                </table>

            

            </td>
            <td align="center">
                
      
               </td>
            <td align="center">
              
            </td>
            <td align="center">
                &nbsp;</td>
            <td align="center">
                &nbsp;</td>
            <td align="center">
                &nbsp;</td>
        </tr>
        <tr>
            <td >
          

   <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" 
        EnableTheming="True" KeyFieldName="RowID"
        Theme="SoftOrange" Width="100%" Font-Size="Small" OnPageIndexChanged="ASPxGridView1_PageIndexChanged" >
       
        <SettingsPager EnableAdaptivity="True" PageSize="25">
        </SettingsPager>
       
        <Settings ShowFilterRow="True" />
       
        <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
       
               <Columns>
            <dx:GridViewCommandColumn ShowInCustomizationForm="True" VisibleIndex="0" Width="8%" Caption=" " SelectAllCheckboxMode="Page" ShowSelectCheckbox="True" ShowClearFilterButton="True">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn Caption="User ID" FieldName="UserID" 
                ShowInCustomizationForm="True" VisibleIndex="1">
                <CellStyle ForeColor="Blue">
                </CellStyle>
            </dx:GridViewDataTextColumn>
           
                   <dx:GridViewDataTextColumn Caption="Name" FieldName="EMPNAME" 
                ShowInCustomizationForm="True" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
             <%--      <dx:GridViewDataTextColumn Caption="Department" FieldName="DepartmentName" 
                ShowInCustomizationForm="True" VisibleIndex="3">
            </dx:GridViewDataTextColumn>--%>
                  <%--   <dx:GridViewDataTextColumn Caption="Location" FieldName="DivisionName" 
                ShowInCustomizationForm="True" VisibleIndex="4">
            </dx:GridViewDataTextColumn>--%>
             <dx:GridViewDataTextColumn Caption="Device" FieldName="SerialNumber" 
                ShowInCustomizationForm="True" VisibleIndex="5">
            </dx:GridViewDataTextColumn>
                     <dx:GridViewDataTextColumn Caption="Device Name" FieldName="DeviceName" 
                ShowInCustomizationForm="True" VisibleIndex="6">
            </dx:GridViewDataTextColumn>
         
           
        </Columns>
       
    </dx:ASPxGridView>

            </td>
                     <td valign="top" >
                   <dx:ASPxGridView ID="ASPxGridView2" runat="server" AutoGenerateColumns="False" 
        EnableTheming="True" KeyFieldName="SerialNumber" 
        Theme="SoftOrange" Width="100%" Font-Size="Small" OnPageIndexChanged="ASPxGridView2_PageIndexChanged"   >
       
                       <SettingsPager PageSize="25">
                       </SettingsPager>
       
                       <Settings ShowFilterRow="True" />
       
        <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
       
        <Columns>
            <dx:GridViewCommandColumn ShowInCustomizationForm="True" VisibleIndex="0" Width="8%" Caption=" " SelectAllCheckboxMode="Page" ShowSelectCheckbox="True" ShowClearFilterButton="True">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn Caption="Device ID" FieldName="SerialNumber" 
                ShowInCustomizationForm="True" VisibleIndex="1">
                <CellStyle ForeColor="Blue">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Device Name" FieldName="DeviceName" 
                ShowInCustomizationForm="True" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="Group" FieldName="GroupName" 
                ShowInCustomizationForm="True" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
         
           
        </Columns>
    </dx:ASPxGridView>
            </td>
        </tr>
    </table>
    </div>
</asp:Content>

