﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="EditUser.aspx.cs" Inherits="EditUser" %>

<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
        <script type="text/javascript" language="javascript" src="JS/JScript_1.js" ></script>   
     <script type="text/javascript">
        function CloseGridLookup() {
            gridLookup.ConfirmCurrentSelection();
            gridLookup.HideDropDown();
            gridLookup.Focus();
        }
          </script> 
     <script type="text/javascript">
        function ExitD() {
            GDep.ConfirmCurrentSelection();
            GDep.HideDropDown();
            GDep.Focus();
        }
   </script> 
        <div>
            <TABLE style="WIDTH: 1000px" cellSpacing=0 cellPadding=0 align=center>
                <TBODY>
                    <TR>
                        <TD style="HEIGHT: 25px"></TD>
                    </TR>
                    <TR>
                        <TD align=center>
                            <table style="width: 850px" class="tableCss" cellspacing="0" cellpadding="0" align="center">
                                <tbody>
                                    <tr>
                                        <td style="width: 850px; height: 25px" class="tableHeaderCss" align="center" colspan="6">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="lblCss">User Manage</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 20px" align="center" colspan="6"></td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 10px" colspan="6">
                                            <asp:Panel ID="Panel6" runat="server" CssClass="roundedPanel" Width="750px">
                                                <div style="padding-right: 10px; padding-left: 10px; padding-bottom: 10px; padding-top: 10px; text-align: left">
                                                    <div style="border-right: black thin solid; padding-right: 20px; border-top: black thin solid; padding-left: 20px; padding-bottom: 20px; border-left: black thin solid; padding-top: 20px; border-bottom: black thin solid; background-color: white; text-align: left">
                                                        <table style="padding: 5px; width: 750px; vertical-align: top;" cellspacing="5px" cellpadding="5px">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="width: 100px" class="text_new" padding="5px">
                                                                        <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="User Name">
                                                                        </dx:ASPxLabel>
                                                                    </td>
                                                                    <td style="width: 200px"  padding="5px">&nbsp; 
                                                                        <dx:ASPxTextBox ID="txtUserName" runat="server" Width="170px" Enabled="False">
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                    <td style="width: 10px" padding="5px"></td>
                                                                    <td style="width: 100px" class="text_new" padding="5px">
                                                                        <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="Password">
                                                                        </dx:ASPxLabel>
                                                                    </td>
                                                                    <td style="width: 200px" padding="5px">
                                                                        <dx:ASPxTextBox ID="txtPassword" runat="server" Width="170px">
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 100px" class="text_new" padding="5px"></td>
                                                                    <td style="width: 200px" padding="5px"></td>
                                                                    <td style="width: 10px" padding="5px"></td>
                                                                    <td style="width: 100px" class="text_new" padding="5px"></td>
                                                                    <td style="width: 200px" padding="5px"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 100px" class="text_new" padding="5px">
                                                                        <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Description">
                                                                        </dx:ASPxLabel>
                                                                    </td>
                                                                    <td style="width: 200px" padding="5px">
                                                                        <dx:ASPxTextBox ID="txtDescription" runat="server" Width="170px">
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                    <td style="width: 10px" padding="5px"></td>
                                                                    <td style="width: 100px" class="text_new">
                                                                        <dx:ASPxLabel ID="lblPaycode" runat="server" Text="PayCode">
                                                                        </dx:ASPxLabel>
                                                                    </td>
                                                                    <td style="width: 200px" padding="5px">
                                                                        <dx:ASPxTextBox ID="txtPaycode" runat="server" Width="170px">
                                                                        </dx:ASPxTextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 100px" class="text_new" padding="5px"></td>
                                                                    <td style="width: 200px" padding="5px"></td>
                                                                    <td style="width: 10px" padding="5px"></td>
                                                                    <td style="width: 100px" class="text_new" padding="5px"></td>
                                                                    <td style="width: 200px" padding="5px"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 100px; height: 21px" class="text_new" padding="5px">
                                                                        <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="User Type">
                                                                        </dx:ASPxLabel>
                                                                    </td>
                                                                    <td style="width: 200px; height: 21px" padding="5px">
                                                                        <dx:ASPxComboBox ID="ddlUserType" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlUserType_SelectedIndexChanged">
                                                                            <Items>
                                                                                <dx:ListEditItem Text="Admin" Value="A" Selected="true" />
                                                                                <dx:ListEditItem Text="HOD" Value="H" />
                                                                                <dx:ListEditItem Text="User" Value="U" />
                                                                            </Items>
                                                                        </dx:ASPxComboBox>
                                                                    </td>
                                                                    <td style="width: 10px; height: 21px" padding="5px"></td>
                                                                    <td style="width: 100px; height: 21px" class="text_new" padding="5px">
                                                                        <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="Login Type" Visible="False">
                                                                        </dx:ASPxLabel>
                                                                    </td>
                                                                    <td style="width: 200px; height: 21px" padding="5px">
                                                                        <dx:ASPxComboBox ID="ddlLoginType" runat="server" Visible="False" >
                                                                            <Items>
                                                                                <dx:ListEditItem Text="Normal User" Value="N"  Selected="true"/>
                                                                                <dx:ListEditItem Text="Login User " Value="L" />
                                                                            </Items>
                                                                        </dx:ASPxComboBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 100px" class="text_new" padding="5px"></td>
                                                                    <td style="width: 200px">
                                                                        <dx:ASPxCheckBox ID="chklapp" runat="server" Text="Roster Upload">
                                                                        </dx:ASPxCheckBox>
                                                                    </td>
                                                                    <td style="width: 10px" padding="5px"></td>
                                                                    <td style="width: 100px" class="text_new" padding="5px"></td>
                                                                    <td style="width: 200px" padding="5px"></td>
                                                                </tr>
                                                                <tr id ="TRSEL" runat="server">
                                                                    <td style="width: 100px" class="text_new" padding="5px">
                                                                        <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="Select Company">
                                                                        </dx:ASPxLabel>
                                                                    </td>
                                                                    <td style="width: 200px">
                                                                        <dx:ASPxGridLookup ID="GridLookup" runat="server" SelectionMode="Multiple"  ClientInstanceName="gridLookup"
                                                                            KeyFieldName="companycode"  TextFormatString="{0}" MultiTextSeparator=",">
                                                                            <Columns>
                                                                                <dx:GridViewCommandColumn ShowSelectCheckbox="True" Caption="" Width="10px" />
                                                                                <dx:GridViewDataColumn FieldName="companycode" Caption="Code" Width="50px" />
                                                                                <dx:GridViewDataColumn FieldName="compDetails"  Caption="Name"/>
                                                                            </Columns>
                                                                            <GridViewProperties>
                                                                                <Templates>
                                                                                    <StatusBar>
                                                                                        <table class="OptionsTable" style="float: right">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <dx:ASPxButton ID="Close" runat="server" AutoPostBack="false" Text="Close" ClientSideEvents-Click="CloseGridLookup" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </StatusBar>
                                                                                </Templates>
                                                                                <Settings ShowFilterRow="True" ShowStatusBar="Visible" />
                                                                                <SettingsPager PageSize="7" EnableAdaptivity="true" />
                                                                            </GridViewProperties>
                                                                        </dx:ASPxGridLookup>
                                                                    </td>
                                                                    <td style="width: 10px" padding="5px"></td>
                                                                    <td style="width: 100px" class="text_new" padding="5px">
                                                                        <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="Select Department">
                                                                        </dx:ASPxLabel>
                                                                    </td>
                                                                    <td style="width: 200px" padding="5px">
                                                                        <dx:ASPxGridLookup ID="GDep" runat="server" ClientInstanceName="GDep" KeyFieldName="departmentcode" MultiTextSeparator="," SelectionMode="Multiple" TextFormatString="{0}">
                                                                             <GridViewProperties>
                            <SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True" />
                        </GridViewProperties>
                                                                            <Columns>
                                                                                <dx:GridViewCommandColumn Caption="" ShowSelectCheckbox="True" Width="10px" />
                                                                                <dx:GridViewDataColumn Caption="Code" FieldName="departmentcode" Width="50px" />
                                                                                <dx:GridViewDataColumn Caption="Name" FieldName="deptDetails"  />
                                                                            </Columns>
                                                                            <GridViewProperties>
                                                                                <Templates>
                                                                                    <StatusBar>
                                                                                        <table class="OptionsTable" style="float: right">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <dx:ASPxButton ID="Exit" runat="server" AutoPostBack="false" ClientSideEvents-Click="ExitD" Text="Exit" />
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </StatusBar>
                                                                                </Templates>
                                                                                <Settings ShowFilterRow="True" ShowStatusBar="Visible" />
                                                                                <SettingsPager EnableAdaptivity="true" PageSize="7" />
                                                                            </GridViewProperties>
                                                                        </dx:ASPxGridLookup>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <br /></td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 10px" class="text_new" colspan="6">
                                            <ajaxToolkit:TabContainer ID="Tabs" runat="server" Width="740px" ActiveTabIndex="1" Height="250px">
                                                <ajaxToolkit:TabPanel runat="server" HeaderText="Previleges" ID="Panel1">
                                                    <ContentTemplate>
                                                        <table style="width: 400px" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="width: 400px">
                                                                        <asp:CheckBox ID="chkMain" runat="server" Text="Main Application" __designer:wfdid="w8">
                                                                        </asp:CheckBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkTransaction" runat="server" Text="Transaction Application" __designer:wfdid="w9">
                                                                        </asp:CheckBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkDataProcess" runat="server" Text="Data Process" __designer:wfdid="w10">
                                                                        </asp:CheckBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 400px">
                                                                        <asp:CheckBox ID="chkLeaveManagement" runat="server" Text="Leave Management" __designer:wfdid="w11">
                                                                        </asp:CheckBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 400px">
                                                                        <asp:CheckBox ID="chkReports" runat="server" Text="Reports" __designer:wfdid="w12">
                                                                        </asp:CheckBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 400px" id="tdAdmin" runat="server">
                                                                        <asp:CheckBox ID="chkAdmin" runat="server" Text="Administration" __designer:wfdid="w13">
                                                                        </asp:CheckBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 400px" id="tdPayroll" runat="server" visible="true">
                                                                        <asp:CheckBox ID="chkPayrollManagement" runat="server" Text="Device Management" __designer:wfdid="w14" Visible="false">
                                                                        </asp:CheckBox>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                <ajaxToolkit:TabPanel runat="server" HeaderText="Main" ID="Panel2">
                                                    <ContentTemplate>
                                                         <table style="width: 400px" cellpadding="0" cellspacing="0">
                                                            <tr runat="server" id="TRCOMP">
                                                                <td> 
                                                                    <dx:ASPxCheckBox ID="chkCompany" runat="server" Text="Company"  />
                                                                        <td> <dx:ASPxCheckBox ID="ChkAddComp" runat="server" Text="Add" ForeColor="#990000" /></td>
                                                                           <td> <dx:ASPxCheckBox ID="ChkEditComp" runat="server" Text="Edit" ForeColor="#990000" /></td>
                                                                           <td> <dx:ASPxCheckBox ID="ChkDeleteComp" runat="server" Text="Delete" ForeColor="#990000" /></td>
                                                                </td>
                                                                <td>
                                                                   
                                                                </td>
                                                            </tr>
                                                             <tr>
                                                                <td>
                                                              
                                                               
                                                                <dx:ASPxCheckBox ID="chkEmpGrp" runat="server" Text="Employee Group"  />
                                                                          <td> <dx:ASPxCheckBox ID="ChkAddGrp" runat="server" Text="Add" ForeColor="#990000" /></td>
                                                                           <td> <dx:ASPxCheckBox ID="ChkEditGrp" runat="server" Text="Edit"  ForeColor="#990000"/></td>
                                                                           <td> <dx:ASPxCheckBox ID="ChkDeleteGrp" runat="server" Text="Delete" ForeColor="#990000" /></td>
                                                                    
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                               

                                                                 <td>
                                                                    <dx:ASPxCheckBox ID="chkDept" runat="server" Text="Department" />
                                                                            <td> <dx:ASPxCheckBox ID="ChkAddDep" runat="server" Text="Add" ForeColor="#990000" /></td>
                                                                           <td> <dx:ASPxCheckBox ID="ChkEditDep" runat="server" Text="Edit" ForeColor="#990000" /></td>
                                                                           <td> <dx:ASPxCheckBox ID="ChkDeleteDep" runat="server" Text="Delete" ForeColor="#990000" /></td>
                                                                </td>
                                                            </tr>
                                                    
                                                            <tr>
                                                                <td>
                                                                    <dx:ASPxCheckBox ID="chkSection" runat="server" Text="Section"  />
                                                                     <td> <dx:ASPxCheckBox ID="ChkAddSec" runat="server" Text="Add" ForeColor="#990000" /></td>
                                                                           <td> <dx:ASPxCheckBox ID="ChkEditSec" runat="server" Text="Edit" ForeColor="#990000"/></td>
                                                                           <td> <dx:ASPxCheckBox ID="ChkDeleteSec" runat="server" Text="Delete" ForeColor="#990000" /></td>
                                                                </td>
                                                            </tr>
                                                       
                                                            <tr>
                                                                <td>
                                                                    <dx:ASPxCheckBox ID="chkGrade" runat="server" Text="Grade" />
                                                                      <td> <dx:ASPxCheckBox ID="ChkAddGrade" runat="server" Text="Add" ForeColor="#990000" /></td>
                                                                           <td> <dx:ASPxCheckBox ID="ChkEditGrade" runat="server" Text="Edit" ForeColor="#990000" /></td>
                                                                           <td> <dx:ASPxCheckBox ID="ChkDeleteGrade" runat="server" Text="Delete" ForeColor="#990000" /></td>
                                                                </td>
                                                            </tr>
                                                  
                                                            <tr>
                                                                <td>
                                                                    <dx:ASPxCheckBox ID="chkCategory" runat="server" Text="Catagory" />
                                                                            <td> <dx:ASPxCheckBox ID="ChkAddCat" runat="server" Text="Add" ForeColor="#990000" /></td>
                                                                           <td> <dx:ASPxCheckBox ID="ChkEditCat" runat="server" Text="Edit" ForeColor="#990000" /></td>
                                                                           <td> <dx:ASPxCheckBox ID="ChkDeleteCat" runat="server" Text="Delete" ForeColor="#990000" /></td>
                                                                </td>
                                                            </tr>
                                                     
                                                            <tr>
                                                                <td>
                                                                    <dx:ASPxCheckBox ID="chkShift" runat="server" Text="Shift" />
                                                                </td>
                                                                  <td> <dx:ASPxCheckBox ID="ChkAddShift" runat="server" Text="Add" ForeColor="#990000" /></td>
                                                                           <td> <dx:ASPxCheckBox ID="ChkEditShift" runat="server" Text="Edit" ForeColor="#990000" /></td>
                                                                           <td> <dx:ASPxCheckBox ID="ChkDeleteShift" runat="server" Text="Delete" ForeColor="#990000" /></td>
                                                            </tr>
                                                             <tr>
                                                                <td>
                                                                    <dx:ASPxCheckBox ID="chkDesignation" runat="server" Text="Designation" Visible="false" />
                                                                </td>
                                                                            <td> <dx:ASPxCheckBox ID="chkAddDesig" runat="server" Text="Add" Visible="false" ForeColor="#990000" /></td>
                                                                           <td> <dx:ASPxCheckBox ID="ChkEditDesig" runat="server" Text="Edit" Visible="false" ForeColor="#990000" /></td>
                                                                           <td> <dx:ASPxCheckBox ID="ChkDelDesig" runat="server" Text="Delete" Visible="false"  ForeColor="#990000"/></td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <dx:ASPxCheckBox ID="chkEmployee" runat="server" Text="Employee" />
                                                                </td>
                                                                            <td> <dx:ASPxCheckBox ID="ChkAddEmp" runat="server" Text="Add" ForeColor="#990000" /></td>
                                                                           <td> <dx:ASPxCheckBox ID="ChkEditEmp" runat="server" Text="Edit" ForeColor="#990000" /></td>
                                                                           <td> <dx:ASPxCheckBox ID="ChkDeleteEmp" runat="server" Text="Delete"  ForeColor="#990000"/></td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td>
                                                                     <dx:ASPxCheckBox ID="chkEmpUpload" runat="server" Text="Upload Employee" />
                                                                </td>
                                                            </tr>
                                                             <tr>
                                                                <td>
                                                                     <dx:ASPxCheckBox ID="chkDevice" runat="server"  Text="Device Mangement" />
                                                                </td>
                                                                 <td> <dx:ASPxCheckBox ID="chkBio" runat="server" Text="Bio Series"  ForeColor="#990000" /></td>
                                                                 <td> <dx:ASPxCheckBox ID="chkHT" runat="server" Text="HT Series"  ForeColor="#990000" /></td>
                                                            </tr>
                                                           
                                                        </table>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                <ajaxToolkit:TabPanel runat="server" HeaderText="Transaction" ID="Panel3">
                                                    <ContentTemplate>
                                                        <table style="width: 400px" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkPunchEntry" runat="server" Text="Punch Entry for Attendance" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkHLD" runat="server" Text="Holiday" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkOStoOT" runat="server" Text="OverStay To OverTime" Visible="false" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkShiftChange" runat="server" Text="Shift Change" Visible="false" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                <ajaxToolkit:TabPanel runat="server" HeaderText="DataProcess/Others" ID="Panel4">
                                                    <ContentTemplate>
                                                        <table style="width: 400px" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkAtt_Creation" runat="server" Text="Attendance Register Creation" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkAtt_Updation" runat="server" Text="Attendance Register Updation" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkBDP" runat="server" Text="Back Date Processing" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkReProcessing" runat="server" Text="Re-Processing" Visible="false" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                <ajaxToolkit:TabPanel runat="server" HeaderText="Leave" ID="Panel5">
                                                    <ContentTemplate>
                                                        <table style="width: 400px" cellspacing="0" cellpadding="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkLeaveMaster" runat="server" Text="Leave Master" __designer:wfdid="w5">
                                                                        </asp:CheckBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <asp:CheckBox ID="chkLeaveApplication" runat="server" Text="Leave Application" __designer:wfdid="w6">
                                                                        </asp:CheckBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td id="tdleaveAccural" runat="server">
                                                                        <asp:CheckBox ID="chkLeaveAccural" runat="server" Text="Leave Accural" __designer:wfdid="w7">
                                                                        </asp:CheckBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td id="tdauto_leave" runat="server">
                                                                        <asp:CheckBox ID="chkAuto_leaveAccural" runat="server" Text="Auto Leave Accural" __designer:wfdid="w8">
                                                                        </asp:CheckBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td runat="server"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td runat="server"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                <ajaxToolkit:TabPanel runat="server" HeaderText="Reports" ID="Panel7">
                                                    <ContentTemplate>
                                                        <table style="width: 400px" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkTimeOffice" runat="server" Text="Time Office Report" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkPayroll" runat="server" Text="Payroll Report" Visible="false" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkVisitorManagement" runat="server" Text="Visitor Management Report" Visible="false" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                <ajaxToolkit:TabPanel runat="server" HeaderText="Admin" ID="Panel8">
                                                    <ContentTemplate>
                                                        <table style="width: 400px" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkTimeOffSetup" runat="server" Text="Time Office Setup" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="tduserprivilge" runat="server">
                                                                    <asp:CheckBox ID="chkUserPrivilge" runat="server" Text="User Privilge" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkVerification" runat="server" Text="Verification" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                                <ajaxToolkit:TabPanel runat="server" HeaderText="Payroll" ID="tabPayroll" Visible="False">
                                                    <ContentTemplate>
                                                        <table style="width: 400px" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td id="tdEmployeeSetup" runat="server">
                                                                    <asp:CheckBox ID="chkEmployeeSetup" runat="server" Text="Employee Setup" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="tdArrear" runat="server">
                                                                    <asp:CheckBox ID="chkArrear" runat="server" Text="Arrear Entry" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="tdAdvanceLoan" runat="server">
                                                                    <asp:CheckBox ID="chkAdvanceLoan" runat="server" Text="Advance/Loan" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="tdForm16" runat="server">
                                                                    <asp:CheckBox ID="chkForm16" runat="server" Text="Form 16" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="tdPayrollProcess" runat="server">
                                                                    <asp:CheckBox ID="chkPayrollProcess" runat="server" Text="Payroll Process" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="tdPayrollFormula" runat="server">
                                                                    <asp:CheckBox ID="chkPayrollFormula" runat="server" Text="Payroll Formula" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="tdPayrollSetup" runat="server">
                                                                    <asp:CheckBox ID="chkPayrollSetup" runat="server" Text="Payroll Setup" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td id="tdPieceManagement" runat="server">
                                                                    <asp:CheckBox ID="chkPieceManagement" runat="server" Text="Piece Management" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </ajaxToolkit:TabPanel>
                                            </ajaxToolkit:TabContainer>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 150px; height: 10px" class="text_new"></td>
                                        <td style="width: 100px">
                                            <asp:ScriptManager ID="ScriptManager1" runat="server">
                                            </asp:ScriptManager>
                                        </td>
                                        <td style="width: 150px"></td>
                                        <td style="width: 100px" colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 150px" class="text_new"></td>
                                        <td style="width: 1000px"></td>
                                        <td style="width: 150px" class="text_new"></td>
                                        <td style="width: 100px" class="text_new"></td>
                                        <td style="text-align: right" class="text_new" colspan="2">&nbsp; &nbsp; </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 150px; height: 10px" class="text_new">
                                            <asp:Label ID="Label1" runat="server" Text="Label" Visible="False" __designer:wfdid="w2"></asp:Label>
                                        </td>
                                        <td style="width: 100px">
                                            <asp:Label ID="Label2" runat="server" Text="Label" Visible="False" __designer:wfdid="w3"></asp:Label>
                                        </td>
                                        <td style="width: 150px" class="text_new"></td>
                                        <td align="right" colspan="2"></td>
                                        <td style="width: 100px" align="left"></tr>
                                    <tr>
                                        <td style="height: 10px" class="text_new"></td>
                                        <td style="height: 10px"></td>
                                        <td style="height: 10px" class="text_new">
                                            <dx:ASPxButton ID="cmdCreate" runat="server" OnClick="cmdCreate_Click" Text="Save">
                                            </dx:ASPxButton>
                                        </td>
                                        <td style="height: 10px" align="right" colspan="2"></td>
                                        <td style="height: 10px" align="left"></td>
                                    </tr>
                                    <tr>
                                        <td style="height: 10px" class="text_new" colspan="6"></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 780px; height: 7px" colspan="7">
                                            <asp:Panel Style="display: none; width: 723px" ID="Panel9" runat="server" CssClass="modalPopup">
                                                <asp:Panel Style="border-right: gray 1px solid; border-top: gray 1px solid; border-left: gray 1px solid; cursor: move; color: black; border-bottom: gray 1px solid; background-color: #ffffff" ID="Panel10" runat="server">
                                                    <table style="width: 100%" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="width: 721px; background-color: #ffffff" align="right">
                                                                    <asp:Image Style="cursor: default" ID="imgClose" runat="server" ToolTip="Close" BackColor="Silver" ImageUrl="~/Images/Close.bmp">
                                                                    </asp:Image>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 721px"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </asp:Panel>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </TD>
                    </TR>
                </TBODY>
            </TABLE>
        </div>
</asp:Content>

