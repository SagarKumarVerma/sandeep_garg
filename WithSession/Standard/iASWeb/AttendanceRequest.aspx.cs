﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using GlobalSettings;
using DevExpress.Web;

public partial class AttendanceRequest : System.Web.UI.Page
{
    OleDbConnection MyConn = new OleDbConnection();
    OleDbCommand MyCmd = new OleDbCommand();
    Class1 cCount = new Class1();
    Class_Connection Con = new Class_Connection();
    string Strsql = null;
    string Msg;
    DataSet ds = null;
    int result = 0;
    OleDbDataReader dr;
    string ReportView = "";
    string PayCode = null;
    DateTime dateofjoin;
    string Shift = null;
    string ShiftAttended = null;
    string ALTERNATEOFFDAYS = null;
    string firstoff = null;
    string secondofftype = null;
    string secondoff = null;
    string HALFDAYSHIFT = null;
    DateTime selecteddate;
    DateTime FirstDate;
    DateTime LastDate;
    DateTime date;
    string ShiftType = null;
    string shiftpattern = null;
    string WOInclude = null;
    string[] words;
    int shiftcount = 0;
    string sSql = string.Empty;
    string Status = "";
    int AbsentValue = 0;
    int WoVal = 0;
    int PresentValue = 0;
    string p = null;
    int shiftRemainDays = 0;
    DateTime PunchDate;
    ErrorClass ec = new ErrorClass();
    string msg;
    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
            {
                Request.Browser.Adapters.Clear();
            }
            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            txtDate.Text = System.DateTime.Today.ToString("dd/MM/yyyy");  //"01/" + System.DateTime.Now.Month.ToString("00") + "/" + System.DateTime.Now.Year.ToString("0000");

            bindPaycode();

        }
    }

    protected void Page_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();
        if (ex is HttpRequestValidationException)
        {
            string resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" +
                   "Form doesn't need to contain valid HTML, just anything with opening and closing angled brackets (<...>).<br />" +
                   btnGoBack +
                   "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        BindGrid();
        grdAtt.DataBind();
       

    }
    protected void bindPaycode()
    {
        ReportView = ConfigurationSettings.AppSettings["ReportView_HeadID"].ToString();
        Strsql = "Select paycode,rtrim(Empname) + ' - ' + paycode as Empname1  from tblemployee where Active='Y' ";
        if (Session["usertype"].ToString() == "A")
        {
            if (Session["Auth_Comp"] != null)
            {
                Strsql += " and companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
            }
            if (Session["Auth_Dept"] != null)
            {
                Strsql += " and Departmentcode in (" + Session["Auth_Dept"].ToString().Trim() + ") ";
            }
            Strsql += "  Order by EmpName ";
        }
        else if (Session["usertype"].ToString().Trim() == "H")
        {
            if (ReportView.ToString().Trim() == "Y")
            {
                Strsql += " and headid = '" + Session["PAYCODE"].ToString().ToString().Trim() + "' ";
            }
            else
            {
                if (Session["Auth_Comp"] != null)
                {
                    Strsql += " and companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
                }
                if (Session["Auth_Dept"] != null)
                {
                    Strsql += " and Departmentcode in (" + Session["Auth_Dept"].ToString().Trim() + ") ";
                }
            }
            Strsql += "  Order by EmpName ";
        }
        else
        {
            Strsql += " and paycode='" + Session["PAYCODE"].ToString().Trim() + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
        }
            //return;


        ds = new DataSet();
        ds = Con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlPayCode.DataSource = ds;
            ddlPayCode.TextField = "EmpName1";
            ddlPayCode.ValueField = "paycode";
            ddlPayCode.DataBind();
            ddlPayCode.SelectedIndex = -1;
        }
        else
        {
            ddlPayCode.Items.Add("NONE");
            ddlPayCode.SelectedIndex = 0;


        }
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }


    private void BindGrid()
    {
        try
        {
            if(ddlPayCode.SelectedItem.Value.ToString().Trim()=="")
            {
                sSql = "select application_no ,CONVERT(CHAR(10),request_date,103) 'REQUEST DATE',CONVERT(CHAR(5),InTime,108) 'Punch Time',UserRemarks 'Remarks', Stage1_Status=CASE WHEN Stage1_approval='Y'" +
               " THEN 'APPROVED' ELSE CASE WHEN Stage1_approval = 'N' THEN 'Rejected' ELSE 'PENDING' END END, Stage1_Remarks = Stage1_Remarks1, " +
               " Stage2_Status = CASE WHEN Stage2_approval = 'Y'THEN 'APPROVED' ELSE CASE WHEN Stage2_approval = 'N' THEN 'Rejected' ELSE 'PENDING' END END," +
               " Stage2_Remarks = Stage2_Remarks1, CONVERT(CHAR(10),  stage1_approvalDate, 103) 'Approval/Rejection Date'  FROM Att_request " +
               "   order by Application_No Desc";

            }
            else
            {
                sSql = "select application_no ,CONVERT(CHAR(10),request_date,103) 'REQUEST DATE',CONVERT(CHAR(16),InTime,108) 'Punch Time',UserRemarks 'Remarks', Stage1_Status=CASE WHEN Stage1_approval='Y'" +
               " THEN 'APPROVED' ELSE CASE WHEN Stage1_approval = 'N' THEN 'Rejected' ELSE 'PENDING' END END, Stage1_Remarks = Stage1_Remarks1, " +
               " Stage2_Status = CASE WHEN Stage2_approval = 'Y'THEN 'APPROVED' ELSE CASE WHEN Stage2_approval = 'N' THEN 'Rejected' ELSE 'PENDING' END END," +
               " Stage2_Remarks = Stage2_Remarks1, CONVERT(CHAR(10),  stage1_approvalDate, 103) 'Approval/Rejection Date'  FROM Att_request WHERE " +
               " PAYCODE = '" + ddlPayCode.SelectedItem.Value.ToString().Trim() + "'   order by Application_No Desc";
            }
            DataSet DsReq = new DataSet();
            DsReq = Con.FillDataSet(sSql);
            if (DsReq.Tables[0].Rows.Count>0)
            {
                grdAtt.DataSource = DsReq.Tables[0];
                grdAtt.DataBind();
            }
            else
            {
                grdAtt.DataSource =null;
                grdAtt.DataBind();
            }
        }
        catch (Exception Ex)
        {
            Error_Occured("BindGrid", Ex.Message);
        }
    }

    protected void ddlPayCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlPayCode.SelectedItem.Text.ToString().Trim().ToUpper() == "NONE")
            {
                lblName.Text = "";
                lblDepartment.Text = "";
                grdAtt.DataSource = null;
                grdAtt.DataBind();
               
                return;

            }


            sSql = " SELECT EMPNAME,tblDepartment.DEPARTMENTNAME,case when tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK='Y' then 'YES' " +
                  " when tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK='N' then 'NO' else 'NA' end 'ISRTC' FROM TBLEMPLOYEE inner join tblDepartment on  " +
                  " tblDepartment.DEPARTMENTCODE=TBLEMPLOYEE.DepartmentCode inner join tblEmployeeShiftMaster on tblEmployeeShiftMaster.SSN=TBLEMPLOYEE.SSN " +
                  " and TBLEMPLOYEE.paycode='" + ddlPayCode.SelectedItem.Value.ToString().Trim() + "' and TBLEMPLOYEE.CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' ";
            DataSet dsEmp = new DataSet();
            dsEmp = Con.FillDataSet(sSql);
            if (dsEmp.Tables[0].Rows.Count > 0)
            {
                lblName.Text = dsEmp.Tables[0].Rows[0]["Empname"].ToString().Trim();
                lblDepartment.Text = dsEmp.Tables[0].Rows[0]["DEPARTMENTNAME"].ToString().Trim();
               
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Please select Any Record');", true);
                ddlPayCode.Focus();
                return;
            }
            BindGrid();
        }
        catch (Exception Ex)
        {
            Error_Occured("PayCode Change", Ex.Message);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            string Tmp;
            string Pcode = "", CardNo = "", CompanyCode = "", DepartmentCode = "", SSN;
            lblName.Text = "";
            string[] tme = new string[2];
            int hour = 23;
            int min = 59;

            if (ddlPayCode.SelectedItem.Text.Trim() == "-Select-" || ddlPayCode.SelectedItem.Text.Trim() == "NONE" || ddlPayCode.SelectedItem.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Please Select PayCode');", true);
                ddlPayCode.Focus();
                return;
            }
            if (txtDate.Text.ToString().Trim().Length < 10)
            {
                //Msg = "<script language='javascript'> alert('Enter A Valid Punch Date...')</script>";
                //Page.RegisterStartupScript("msg", Msg);
                //txtDate.Focus();
                //return;
            }
            if (txtTime.Text.ToString().Trim()=="00:00")
            {
                Msg = "<script language='javascript'> alert('Enter A Valid Punch Time...')</script>";
                Page.RegisterStartupScript("msg", Msg);
                txtTime.Focus();
                return;
            }
            try
            {
                // PunchDate = DateTime.ParseExact(TxtFromDate.Text.ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                PunchDate = Convert.ToDateTime(txtDate.Value.ToString().Trim());
                Tmp = PunchDate.ToString("yyyy-MM-dd") + " " + txtTime.Text.Trim();
            }
            catch
            {
                //Msg = "<script language='javascript'> alert('Invalid Punch Date...')</script>";
                //Page.RegisterStartupScript("msg", Msg);
                //TxtFromDate.Focus();
                return;
            }
            try
            {
               if(Convert.ToDateTime(Tmp)> System.DateTime.Now)
                {
                    Msg = "<script language='javascript'> alert('Can Not Post For Future Date')</script>";
                    Page.RegisterStartupScript("msg", Msg);
                    txtTime.Focus();
                    return;
                }
                
            }
            catch
            {
                //Msg = "<script language='javascript'> alert('Invalid Punch Date...')</script>";
                //Page.RegisterStartupScript("msg", Msg);
                //TxtFromDate.Focus();
                return;
            }
            Pcode = ddlPayCode.SelectedItem.Value.ToString().Trim();

            Strsql = "Select * from MachineRawPunch where paycode = '" + Pcode + "' And OfficePunch = '" + Tmp + "'  and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
            dr = Con.Execute_Reader(Strsql);
            if (dr.Read())
            {
                dr.Close();
                Msg = "<script language='javascript'> alert('This Punch Time Is Already Available...')</script>";
                Page.RegisterStartupScript("msg", Msg);
                return;
            }
            dr.Close();

            //Strsql = "Select * from Att_request where paycode = '" + Pcode + "' And Request_for = '" + PunchDate.ToString("yyyy-MM-dd") + "'  ";
            //dr = Con.Execute_Reader(Strsql);
            //if (dr.Read())
            //{
            //    dr.Close();
            //    Msg = "<script language='javascript'> alert('You Have Already Regulerized Attendance For This Date')</script>";
            //    Page.RegisterStartupScript("msg", Msg);
            //    return;
            //}
            //dr.Close();
           DateTime DtCurDate = Convert.ToDateTime(Session["Today"].ToString());
            string CurDate = "";

            // Pcode = EmpCombo.SelectedValue.ToString();    //TxtPayCode.Text.Trim();
            string headid = "";
            string divisionCode = "";
            string str = "Select headid from tblemployee where paycode='" + Pcode.ToString().Trim() + "' and CompanyCode='"+ Session["LoginCompany"].ToString().Trim() + "'";
            DataSet dsr = new DataSet();
            dsr = Con.FillDataSet(str);
            if (dsr.Tables[0].Rows.Count > 0)
            {
                headid = dsr.Tables[0].Rows[0][0].ToString().Trim();
                
            }
            if (string.IsNullOrEmpty(headid))
            {
                msg = "<script language='javascript'>alert('HeadID not Specified.')</script>";
                Page.RegisterStartupScript("msg", msg);
                return;
            }
            double graceDays = 0;
            if (Session["usertype"].ToString().Trim()=="U")
            {
                sSql = "Insert into Att_request(paycode, Request_date, InTime, Request_for, UserRemarks,RequestBy ) ";
                sSql += " Values ( '" + Pcode.ToString().Trim() + "',  getdate(), '" + Tmp + "',  '" + PunchDate.ToString("yyyy-MM-dd") + "', '" + userRemarks.Text.ToString().Trim() + "','" + Session["LoggedUser"].ToString().Trim() + "')";
                Con.execute_NonQuery(sSql);
            }
            else
            {
                sSql = "Insert into Att_request(paycode, Request_date, InTime, Request_for, UserRemarks,RequestBy,Stage1_approval,stage1_Remarks1,stage1_approvalID,stage1_ApprovalDate ,Stage2_approval,stage2_Remarks1,stage2_approvalID,stage2_ApprovalDate ) ";
                sSql += " Values ( '" + Pcode.ToString().Trim() + "',  getdate(), '" + Tmp + "',  '" + PunchDate.ToString("yyyy-MM-dd") + "', '" + userRemarks.Text.ToString().Trim() + "','" + Session["LoggedUser"].ToString().Trim() + "' ,"+
                    " 'Y','Auto Approved','"+ Session["LoginUserName"].ToString().Trim() + "',getdate(),'Y','Auto Approved','" + Session["LoginUserName"].ToString().Trim() + "',getdate())";
                Con.execute_NonQuery(sSql);
                try
                {

                    //Tmp = DataGrid1.GetRowValues(DR, "application_no").ToString().Trim();
                  

                   // ApplyDate = Convert.ToDateTime(txtfromdate);
                    sSql = "Select presentCardno,companycode,departmentcode,ssn from tblemployee where Active='Y' And paycode ='" + Pcode + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' ";
                    dr = Con.ExecuteReader(sSql);
                    if (dr.Read())
                        CardNo = dr[0].ToString().Trim();
                    CompanyCode = dr[1].ToString().Trim();
                    DepartmentCode = dr[2].ToString().Trim();
                    SSN = dr[3].ToString().Trim();
                    dr.Close();
                    if (CardNo.Trim() == "")
                        return;
                    string SetupID = "1";

                    DataSet Rs = new DataSet();
                    sSql = " Select setupid from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tblsetup)";
                    Rs = Con.FillDataSet(sSql);
                    if (Rs.Tables[0].Rows.Count > 0)
                    {
                        SetupID = Rs.Tables[0].Rows[0][0].ToString().Trim();
                    }
                    try
                    {

                        Strsql = "Insert into MachineRawPunch  (CARDNO,PAYCODE,ISMANUAL,P_Day,OFFICEPUNCH,CompanyCode,SSN)  VALUES('" + CardNo + "','" + Pcode + "','Y','N','" + Tmp.ToString() + "','" + CompanyCode + "','" + SSN + "') ";
                        int result = Con.execute_NonQuery(Strsql);

                        if (result > 0)
                        {

                            Strsql = "Insert into tblLog_ManualPunch (Paycode,PunchDate,LoggedUser,LoggedDate)  VALUES('" + Pcode + "','" + Tmp.ToString() + "','" + Session["PAYCODE"].ToString().Trim() + "',getdate() )";
                            result = Con.execute_NonQuery(Strsql);

                            try
                            {
                                sSql = "INSERT INTO [dbo].[UserEditLog]([UserName],[LogDate],[LogDetails],[OldValue],[NewValue],[CompanyCode])values('" + Session["UserName"].ToString().Trim() + "',getdate(),'Manual Punch Posted For Code: " + Pcode + "','Punch Date: " + PunchDate.ToString("dd/MM/yyyy") + "','Punch Time: " + Tmp.ToString() + "','" + Session["LoginCompany"].ToString().Trim() + "')";
                                Con.execute_NonQuery(sSql);

                            }
                            catch
                            {

                            }
                            try


                            {
                                using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString()))
                                {

                                    MyCmd = new OleDbCommand("ProcessBackDate", MyConn);
                                    MyCmd.CommandTimeout = 1800;
                                    MyCmd.CommandType = CommandType.StoredProcedure;
                                    MyCmd.Parameters.Add("@FromDate", SqlDbType.VarChar).Value = PunchDate.AddDays(-1).ToString("yyyy-MM-dd");
                                    MyCmd.Parameters.Add("@ToDate", SqlDbType.VarChar).Value = System.DateTime.Now.ToString("yyyy-MM-dd");
                                    MyCmd.Parameters.Add("@CompanyCode", SqlDbType.VarChar).Value = CompanyCode;
                                    MyCmd.Parameters.Add("@DepartmentCode", SqlDbType.VarChar).Value = DepartmentCode;
                                    MyCmd.Parameters.Add("@PayCode", SqlDbType.VarChar).Value = Pcode;
                                    MyCmd.Parameters.Add("@SetupId", SqlDbType.VarChar).Value = SetupID;
                                    MyConn.Open();
                                    result = MyCmd.ExecuteNonQuery();
                                    MyConn.Close();

                                }
                            }
                            catch (Exception Ex)
                            {
                                Error_Occured("SP", Ex.Message);
                            }
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Request Approved Successfully');", true);

                            result = 0;

                        }

                    }
                    catch (Exception Ex)
                    {
                        Error_Occured("MRP", Ex.Message);
                    }

                }
                catch
                {

                }


            }
           
            BindGrid();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Request Sent Successfully');document.location='AttendanceRequest.aspx'", true);


        }
        catch (Exception Ex)
        {
            Error_Occured("btnSave_Click", Ex.Message);
        }
    }
}