﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="frmRosterUpload.aspx.cs" Inherits="frmRosterUpload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
     
     <div style="border-style: solid; border-width: thin; border-color: inherit;">
         <table align="center">
             <tr>
                 <td style="padding:5px" colspan="2">
                     <dx:ASPxHyperLink ID="ASPxHyperLink1" runat="server" Text="Download Format" NavigateUrl="~/RosterFormat.xls" Theme="SoftOrange" />
                 </td>
             </tr>
             <tr>
                 <td style="padding:5px">
                     <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="From Date"></dx:ASPxLabel>
                 </td>
                  <td style="padding:5px">
                      <dx:ASPxDateEdit ID="txtDateFrom" runat="server" EditFormatString="dd/MM/yyyy"></dx:ASPxDateEdit>
                 </td>
             </tr>
              <tr>
                 <td style="padding:5px">
                     <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="To Date"></dx:ASPxLabel>
                 </td>
                  <td style="padding:5px">
                      <dx:ASPxDateEdit ID="txtToDate" runat="server" EditFormatString="dd/MM/yyyy" ></dx:ASPxDateEdit>
                 </td>
             </tr>
             <tr>
                 <td style="padding:5px">
                     <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Select File"></dx:ASPxLabel>
                 </td>
                 <td style="padding:5px" >
                               <dx:ASPxUploadControl ID="ASPxUploadControl1" runat="server" Width="300px"
            ClientInstanceName="uploadControl" 
            OnFilesUploadComplete="ASPxUploadControl1_FilesUploadComplete" 
            ShowProgressPanel="True" Theme="SoftOrange" >
            <ValidationSettings AllowedFileExtensions=".xls" GeneralErrorText="File Upload Failed Due To An External Error" MaxFileCount="1" MultiSelectionErrorText="Attention! 

{0} files are invalid and will not be uploaded. Possible reasons are: they exceed the allowed file size ({1}), their extensions are not allowed, or their filenames contain invalid characters. These files have been removed from the selection: 

{2}" NotAllowedFileExtensionErrorText="*File Format Not Valid">
            </ValidationSettings>
            <ClientSideEvents 
                Init="function(s, e) { UpdateUploadButton(); }" 
                TextChanged="function(s, e) { UpdateUploadButton(); }" 
                FilesUploadComplete="function(s, e) { UpdateUploadButton(); }" 
                FileUploadStart="function(s, e) {
                    btnUploadViaPostback.SetEnabled(false);
                    btnUploadViaCallback.SetEnabled(false);
                }" />
                          <UploadButton>
                              <Image IconID="conditionalformatting_iconsetarrows3_16x16">
                              </Image>
                          </UploadButton>
        </dx:ASPxUploadControl>
                 </td>
             </tr>
             <tr>
                 <td style="padding:5px;" colspan="2" >
                     <dx:ASPxLabel ID="lblStatus" runat="server" Text="" ForeColor="Red" Theme="SoftOrange">
                     </dx:ASPxLabel>
                 </td>
                
             </tr>
             <tr>
                 <td style="padding:5px ; align-items:center" colspan="2">
                     <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Upload Roster" Theme="SoftOrange" HorizontalAlign="Center" Width="100px" OnClick="ASPxButton1_Click" style="height: 21px">
                     </dx:ASPxButton>
                 </td>
                
             </tr>
             <tr>
                 <td>
                     <asp:FileUpload ID="FileUpload1" runat="server" Visible="False" />
                 </td>
             </tr>
         </table>
     </div>
    <script type="text/javascript">
        function UpdateUploadButton() {
            var isAnyFileSelected = false;
            for (var i = 0; i < uploadControl.GetFileInputCount() ; i++) {
                if (uploadControl.GetText(i) != "") { isAnyFileSelected = true; break; }
            }
            btnUploadViaPostback.SetEnabled(isAnyFileSelected);
            btnUploadViaCallback.SetEnabled(isAnyFileSelected);
        }
    </script>
</asp:Content>

