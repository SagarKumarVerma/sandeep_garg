﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="frmLeaveApplication.aspx.cs" Inherits="frmLeaveApplication" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <div>

        <table align="center" style="border-width: thin; padding: 5px; border-style: solid; width: 1000px" cellpadding="0" cellspacing="0">
            <tr>
                <td>

                </td>
            </tr>
            <tr>
                <td align="center">
                    
                    <table style="width: 900px" cellpadding="0" cellspacing="0" class="tableCss" id="TABLE1" runat="server">
                        <tr>
                            <td colspan="2" style="height: 25px; width: 850px" align="center" class="tableHeaderCss">
                                <asp:Label ID="lblMsg" runat="server" Text="Leave Application" CssClass="lblCss"></asp:Label></td>
                        </tr>

                        <tr>
                            <td colspan="2" style="text-align: left">
                                <table align="left" style="width: 100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="text_new" style="width: 120px; height: 10px" valign="top">
                                            &nbsp;</td>
                                        <td style="width: 170px" valign="top"></td>
                                        <td class="text_new" style="width: 120px" valign="top"></td>
                                        <td style="width: 450px" valign="top"></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 120px" valign="top">
                                            <dx:ASPxLabel ID="lblEmpCode" runat="server" Text="Employee Code"></dx:ASPxLabel>
                                        </td>
                                        <td style="width: 170px" valign="top">
                                            <asp:Label ID="lblerror" Style="z-index: 122; left: 303px; position: absolute; top: 133px" ForeColor="Red" runat="server" Height="21px" Width="49px" Visible="False">*</asp:Label>
                                            <dx:ASPxComboBox ID="ddlPayCode" runat="server" ValueType="System.String" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="ddlPayCode_SelectedIndexChanged" NullText="NONE">
                                                <ClearButton DisplayMode="Always">
                                                </ClearButton>
                                                <ValidationSettings SetFocusOnError="True" ValidationGroup="MK">
                                                    <RequiredField IsRequired="True" />
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                            <asp:Label ID="lblcompdate" ForeColor="Red" runat="server" Height="18px" Width="16px" Visible="False">*</asp:Label>

                                            <dx:ASPxTextBox ID="txtpaycode" runat="server" Width="70px" AutoPostBack="True" Visible="false"></dx:ASPxTextBox>

                                        </td>
                                        <td style="width: 120px" class="text_new" valign="top" rowspan="5">Balance</td>
                                        <td style="300: ;" valign="top" rowspan="5">

                                            <dx:ASPxGridView ID="LeaveGrid" runat="server" Visible="false" Width="300px">
                                                <SettingsPager Visible="False">
                                                </SettingsPager>
                                                <SettingsDataSecurity AllowEdit="False" AllowInsert="False" AllowDelete="false" />
                                            </dx:ASPxGridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 120px; height: 10px;" valign="top">

                                            <dx:ASPxLabel ID="lblEmpname" runat="server" Text="lblEmpName"></dx:ASPxLabel>
                                        </td>
                                        <td style="width: 170px" valign="top">
                                            <dx:ASPxLabel ID="LblName" runat="server"  Font-Bold="True" Width="210px" Style="left: 0px"></dx:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 120px; height: 10px" valign="top"></td>
                                        <td style="width: 170px" valign="top"></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 120px; height: 10px" valign="top" class="text_new">Date From</td>
                                        <td style="width: 170px" valign="top">
                                            <dx:ASPxDateEdit ID="txtfromdate" runat="server" AutoPostBack="True" EditFormatString="dd/MM/yyyy" Width="150px" OnDateChanged="txtfromdate_DateChanged" OnCalendarDayCellPrepared="txtfromdate_CalendarDayCellPrepared">
                                                <ValidationSettings SetFocusOnError="True" ValidationGroup="MK">
                                                    <RequiredField IsRequired="True" />
                                                </ValidationSettings>
                                            </dx:ASPxDateEdit>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 120px; height: 10px;" class="text_new" valign="top"></td>
                                        <td style="width: 170px" valign="top"></td>
                                    </tr>
                                    <tr>
                                          <td style="width: 120px" class="text_new" valign="top">&nbsp;</td>
                                        <td colspan="2" valign="top">
                                            </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 120px" class="text_new" valign="top">Date To</td>
                                        <td style="width: 170px" valign="top">

                                            <dx:ASPxDateEdit ID="txttodate" runat="server" EditFormatString="dd/MM/yyyy"  Width="150px" OnCalendarDayCellPrepared="txttodate_CalendarDayCellPrepared">
                                                <ValidationSettings SetFocusOnError="True" ValidationGroup="MK">
                                                    <RequiredField IsRequired="True" />
                                                </ValidationSettings>
                                            </dx:ASPxDateEdit>
                                        </td>
                                        <td style="width: 120px" class="text_new" valign="top">Remarks<span style="font-size: 16px; color: Red">*</span></td>
                                        <td rowspan="3" style="300: ;" valign="top">

                                            <dx:ASPxTextBox ID="TXTREMARKS" runat="server" Height="43px" MaxLength="30" Rows="2" Width="300px" TextMode="MultiLine">
                                                <ValidationSettings SetFocusOnError="True" ValidationGroup="MK">
                                                    <RequiredField IsRequired="True" />
                                                </ValidationSettings>
                                            </dx:ASPxTextBox>

                                            <dx:ASPxLabel ID="lblmessage" runat="server" Width="387px"></dx:ASPxLabel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 120px; height: 10px;" valign="top"></td>
                                        <td style="width: 170px" valign="top"></td>
                                        <td style="width: 120px" valign="top"></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 120px" class="text_new" valign="top">Duration</td>
                                        <td colspan="2" valign="top">

                                            <dx:ASPxRadioButtonList ID="radioday" runat="server" EnableTheming="True" Height="19px" RepeatDirection="Horizontal" Style="top: 0px" Theme="SoftOrange">
                                                <Items>
                                                    <dx:ListEditItem Text="Full Day" Selected="true" Value="FD" />
                                                    <dx:ListEditItem Text="First Half" Value="FH" />
                                                    <dx:ListEditItem Text="Seconf Half" Value="SH" />
                                                </Items>
                                            </dx:ASPxRadioButtonList>

                                        </td>
                                    </tr>
                                    <tr>
                                          <td style="width: 120px" class="text_new" valign="top">&nbsp;</td>
                                        <td colspan="2" valign="top">
                                            </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 120px; height: 10px;" class="text_new" valign="top">Leave</td>
                                        <td style="width: 170px" valign="top">
                                            <dx:ASPxComboBox ID="DDLREASON" runat="server" Width="229px" AutoPostBack="false" OnSelectedIndexChanged="DDLREASON_SelectedIndexChanged" ValueType="System.String">
                                                <ValidationSettings SetFocusOnError="True" ValidationGroup="MK">
                                                    <RequiredField IsRequired="True" />
                                                </ValidationSettings>
                                            </dx:ASPxComboBox>
                                        </td>
                                        <td style="width: 120px" class="text_new" valign="top">
                                            <dx:ASPxLabel ID="lblCOAgainst" runat="server" Text="CO Against" Visible="false"></dx:ASPxLabel>
                                        </td>
                                        <td style="width: 450px" valign="top" align="right">
                                            <table cellpadding="0" cellspacing="0" style="width: 450px">
                                                <tr>
                                                 <%--   <td id="tdCOFF" runat="server" align="left" valign="top" visible="false">
                                                        <dx:ASPxDateEdit ID="txtCOAgainst" runat="server"></dx:ASPxDateEdit>
                                                    </td>
                                                    <td style="width: 100px"></td>--%>
                                                    <td id="Td1" runat="server" align="left" style="width: 150px" valign="top">&nbsp;&nbsp;
                    
                                    <dx:ASPxButton ID="cmdApply" runat="server" Text="Apply" OnClick="cmdApply_Click" ValidationGroup="MK"></dx:ASPxButton>
                                                        <dx:ASPxButton ID="cmdCancel" runat="server" Text="Cancel" OnClick="cmdCancel_Click"></dx:ASPxButton>
                                                    </td>
                                                </tr>


                                            </table>
                                        </td>
                                    </tr>
                                    </table>
                            </td>
                        </tr>
                    </table>
                         
                </td>
            </tr>
        </table>
        <table align="center" style="width: 1000px" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="2">
                    <table align="left" cellpadding="2" cellspacing="1">
                        <tr>
                            <td class="tableHeaderCss" style="height: 15px">
                                <span class="lblCss"></span></td>
                        </tr>
                        <tr>
                            <td>
                                <div style="overflow: auto; width: 100%; text-align: left; height: 262px;">

                                    <dx:ASPxGridView ID="grdrequest" runat="server" AutoGenerateColumns="False" DataKeyField="application_no" EnableTheming="True" Theme="SoftOrange" KeyFieldName="application_no" OnRowDeleting="grdrequest_RowDeleting" OnCommandButtonInitialize="grdrequest_CommandButtonInitialize">
                                        <Settings ShowTitlePanel="True" />
                                        <SettingsCommandButton>
                                            <DeleteButton Text=" ">
                                                <Image IconID="actions_trash_16x16">
                                                </Image>
                                            </DeleteButton>
                                        </SettingsCommandButton>
                                        <SettingsDataSecurity AllowEdit="False" AllowInsert="False" />
                                        <SettingsText ConfirmDelete="Are You Sure Want To Delete This Application? " />
                                        <Columns>

                                            <dx:GridViewCommandColumn Caption="  " ShowDeleteButton="True" VisibleIndex="0">
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                            </dx:GridViewCommandColumn>
                                            <dx:GridViewDataTextColumn Caption="Remarks" FieldName="Stage2_Remarks" VisibleIndex="12">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Stage 2 Status" FieldName="Stage2_Status" VisibleIndex="11">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Remarks" FieldName="Stage1_Remarks" VisibleIndex="10">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Stage 1 Status" FieldName="Stage1_Status" VisibleIndex="9">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="User Remarks" FieldName="USERREMARKS" VisibleIndex="8">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Duration" FieldName="Duration" VisibleIndex="7">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Leave" FieldName="Leave Amount" VisibleIndex="6">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Days" FieldName="Days" VisibleIndex="5">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Leave To" FieldName="TO" VisibleIndex="4">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Leave From" FieldName="FROM" VisibleIndex="3">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Leave Code" FieldName="LEAVECODE" VisibleIndex="2">
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Apply Date" FieldName="REQUEST_DATE" VisibleIndex="1">
                                            </dx:GridViewDataTextColumn>
                                        </Columns>

                                    </dx:ASPxGridView>
                                </div>
                            </td>
                        </tr>

                    </table>
                </td>
            </tr>
        </table>
                <table style="width: 400px" border="0" cellpadding="0" cellspacing="0" id="TABLE2" runat="server" visible="false"  >
                        <tr>
                        <td style="width: 200px; height: 45px;" valign="top" >
                        <asp:RadioButtonList ID="RadioLeaveType" runat="server"  RepeatDirection="Horizontal" OnSelectedIndexChanged="RadioLeaveType_SelectedIndexChanged" AutoPostBack="True" CssClass="Radiocss"   >
                                            <asp:ListItem Selected="True" Value="1">Leave</asp:ListItem>
                                            <asp:ListItem Value="2">Holiday</asp:ListItem>
                       </asp:RadioButtonList></td>
                       <td style="width: 150px; height: 45px;" valign="top">
                       <asp:DropDownList ID ="DDType" runat="server" Visible="False" AutoPostBack="True" OnSelectedIndexChanged="DDType_SelectedIndexChanged" CssClass="DropDownCssM" ></asp:DropDownList></td>
                            <td style="width: 50px; height: 45px;" valign="top">
                                <asp:Label ID="LblDt" runat="server"  Width="55px" Visible="False" Height="16px" CssClass="CheckBox" style="left: 2px; top: 1px" ></asp:Label></td>
                            <td style="width: 50px; height: 45px;" valign="top">
                    <asp:Label ID="Label1" runat="server" Style="vertical-align: top" Text="Leave Type"
                        Visible="False" Width="74px"></asp:Label></td>
                        </tr>
                        </table>
    </div>

</asp:Content>

