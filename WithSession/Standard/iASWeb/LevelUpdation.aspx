﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="LevelUpdation.aspx.cs" Inherits="LevelUpdation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <div>
        <dx:ASPxCallbackPanel ID="ASPxCallbackPanelGrd" runat="server" ClientInstanceName="ASPxCallbackPanelGrd" Width="100%" Theme="SoftOrange">
            <PanelCollection>
                <dx:PanelContent runat="server">
                    <table style="width: 60%; align: center">
                        <tr>
                            <td style="padding: 5px; text-align: center" colspan="5">
                                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Leave Level / Head Id Update" Theme="SoftOrange">
                                </dx:ASPxLabel>
                            </td>

                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Department" Theme="SoftOrange">
                                </dx:ASPxLabel>
                            </td>
                            <td style="padding: 5px">
                                <dx:ASPxComboBox ID="DdlDept" runat="server" Theme="SoftOrange" ValueType="System.String" AutoPostBack="True"  NullText="NONE" OnSelectedIndexChanged="DdlDept_SelectedIndexChanged">
                                </dx:ASPxComboBox>
                            </td>
                            <td style="padding: 5px">
                                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Update">
                                </dx:ASPxLabel>
                            </td>
                            <td style="padding: 5px">
                                <dx:ASPxRadioButton ID="RadLvApproval" AutoPostBack="true" runat="server" Checked="True" GroupName="A" OnCheckedChanged="RadLvApproval_CheckedChanged" Text="Leave Approval Stage">
                                </dx:ASPxRadioButton>
                            </td>
                            <td style="padding: 5px">
                                <dx:ASPxRadioButton ID="RdHeadId" runat="server" AutoPostBack="true" GroupName="A" OnCheckedChanged="RdHeadId_CheckedChanged" Text="Reporting Head 1 ">
                                </dx:ASPxRadioButton>
                                <dx:ASPxRadioButton ID="RdHeadId1" runat="server" AutoPostBack="true" GroupName="A" OnCheckedChanged="RdHeadId_CheckedChanged" Text="Reporting Head 2 ">
                                </dx:ASPxRadioButton>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 5px">
                                <dx:ASPxLabel ID="LblSel" runat="server" Text="Approval Stage"  Theme="SoftOrange">
                                </dx:ASPxLabel>
                            </td>
                            <td style="padding: 5px">
                                <dx:ASPxComboBox ID="combo_Level" runat="server" AutoPostBack="false" Theme="SoftOrange" ValueType="System.String" >
                                    <Items>
                                        <dx:ListEditItem Selected="True" Text="1" Value="1" />
                                        <dx:ListEditItem Text="2" Value="2" />
                                    </Items>
                                </dx:ASPxComboBox>
                            </td>
                            <td style="padding: 5px" colspan="2">
                                <dx:ASPxComboBox ID="TxtHeadId1" runat="server" Theme="SoftOrange" AutoPostBack="false" SelectedIndex="-1" NullText="NONE"  ValueType="System.String">
                                </dx:ASPxComboBox>
                            </td>
                            <td style="padding: 5px">
                                <dx:ASPxButton ID="btn_Update" runat="server" Text="Update" OnClick="btn_Update_Click">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                         <tr>
                            <td style="padding: 5px" colspan="6">
                                <dx:ASPxGridView ID="DataGrid1" runat="server" Width="100%" Theme="SoftOrange" KeyFieldName="Paycode" AutoGenerateColumns="False" OnHtmlDataCellPrepared="DataGrid1_HtmlDataCellPrepared" OnInit="DataGrid1_Init">
                                   
                                    <SettingsPager Visible="true" PageSize="500"> 
                                    </SettingsPager>
                                   
                                    <Settings ShowFilterRow="True" />
                                    <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                                    <Columns>
                                        <dx:GridViewCommandColumn Caption=" " SelectAllCheckboxMode="Page" ShowInCustomizationForm="True" ShowSelectCheckbox="True" VisibleIndex="0" ShowClearFilterButton="True">
                                        </dx:GridViewCommandColumn>
                                        <dx:GridViewDataTextColumn Caption="Employee Code" FieldName="Paycode" ShowInCustomizationForm="True" VisibleIndex="1">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Employee Name" FieldName="Empname" ShowInCustomizationForm="True" VisibleIndex="2">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Department Name" FieldName="DepartmentName" ShowInCustomizationForm="True" VisibleIndex="3">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Leave Approval Stage/Reporting Head" FieldName="Level" ShowInCustomizationForm="True" VisibleIndex="4">
                                        </dx:GridViewDataTextColumn>
                                       <%-- <dx:GridViewDataTextColumn Caption="Head ID" ShowInCustomizationForm="True" VisibleIndex="5">
                                        </dx:GridViewDataTextColumn>--%>
                                    </Columns>
                                </dx:ASPxGridView>
                                </td>
                             </tr>
                    </table>
                </dx:PanelContent>
            </PanelCollection>
            <Border BorderStyle="Solid" />
        </dx:ASPxCallbackPanel>
    </div>
</asp:Content>

