﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using GlobalSettings;
using System.Collections.Generic;
using DevExpress.Web;
using System.Text.RegularExpressions;

public partial class frmLocation : System.Web.UI.Page
{
    Class_Connection Con = new Class_Connection();
    string Strsql = null;
    DataSet ds = null;
    int result = 0;
    ErrorClass ec = new ErrorClass();
    string DName,DHOD,DEmail;
    bool isNewRecord = false;


    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            if (Session["DeptVisible"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            BindData();
        }
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }

    protected void BindData()
    {
        try
        {
            if (Session["LoginUserName"].ToString().Trim().ToUpper() == "ADMIN")
            {
                Strsql = "Select DEPARTMENTCODE,DEPARTMENTNAME,DEPARTMENTHEAD,EMAIL_ID,CompanyCode from tblDepartment order by 1";
            }
            else if (Session["usertype"].ToString().Trim().ToUpper() == "H")
            {
                Strsql = "Select DEPARTMENTCODE,DEPARTMENTNAME,DEPARTMENTHEAD,EMAIL_ID,CompanyCode from tblDepartment where CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' and departmentcode in ("+ Session["Auth_Dept"].ToString().Trim() + ") order by 1";
            }
            else
            {
                Strsql = "Select DEPARTMENTCODE,DEPARTMENTNAME,DEPARTMENTHEAD,EMAIL_ID,CompanyCode from tblDepartment where CompanyCode='"+ Session["LoginCompany"].ToString().Trim() +"' order by 1";
            }

            ds = new DataSet();
            ds = Con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                GrdLocation.DataSource = ds.Tables[0];
                GrdLocation.DataBind();
            }
        }
        catch(Exception E)
        {
            Error_Occured("BindData", E.Message);
        }

        
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        BindData();
        GrdLocation.DataBind();
    }
    private void AddError(Dictionary<GridViewColumn, string> errors, GridViewColumn column, string errorText)
    {
        if (!errors.ContainsKey(column))
            errors[column] = errorText;
    }

    protected void GrdLocation_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.Grid.IsNewRowEditing == false)
        {
            if (e.Column.FieldName == "DEPARTMENTCODE")
            {
                e.Editor.ReadOnly = true;

            }
            else
            {
                e.Editor.ReadOnly = false;
            }

        }
        else
        {
            isNewRecord = true;
            e.Editor.ReadOnly = false;

            
        }
    }
    public bool IsEmail(string Email)
    {
        string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
        @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
        @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
        Regex re = new Regex(strRegex);
        if (re.IsMatch(Email))
            return (true);
        else
            return (false);
    } 
    protected void GrdLocation_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        try
        {
            string k = e.Keys["DEPARTMENTCODE"].ToString();
            DName = e.NewValues["DEPARTMENTNAME"].ToString();
           
            try
            {
                DHOD = e.NewValues["DEPARTMENTHEAD"].ToString().Trim();
            }
            catch
            {
                DHOD = "";
            }
            try
            {
                DEmail = e.NewValues["EMAIL_ID"].ToString().Trim();
            }
            catch
            {
                DEmail = "";
            }

            Strsql = "update tblDepartment set DEPARTMENTNAME='" + DName.Trim().ToUpper() + "',DEPARTMENTHEAD='" + DHOD.Trim().ToUpper() + "', " +
                " EMAIL_ID='" + DEmail.Trim().ToUpper() + "' where DEPARTMENTCODE='" + k.Trim() + "'  and CompanyCode='"+Session["LoginCompany"].ToString().Trim() +"' ";
            Con.execute_NonQuery(Strsql);
            e.Cancel = true;
            GrdLocation.CancelEdit();
            BindData();
            ec.InsertLog("Department Record Updated", k, k, Session["LoginUserName"].ToString().Trim(), Session["LoginCompany"].ToString().Trim().Trim());
            DataFilter.LoadDepartment(Session["LoginUserName"].ToString().Trim(), Session["LoginCompany"].ToString().Trim());
        }
        catch (Exception Ex)
        {
            Error_Occured("GrdLocation_RowUpdating", Ex.Message);
        }
    }
    protected void GrdLocation_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        try
        {
            string IsMaster = Convert.ToString(e.Keys["DEPARTMENTCODE"]);
            bool Validate = false;
            Validate = ValidData(IsMaster);
            if (Validate == true)
            {
                //ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Location Already Assigned To Employee')</script>");
                //lblStatus.Text = "Cant Delete";
                ((ASPxGridView)sender).JSProperties["cp_isSuccess"] = true;
                GrdLocation.CancelEdit();
               // e.Cancel = true;
                BindData();
               // return;

            }
            else
            {
                Strsql = "Delete From tblDepartment where DEPARTMENTCODE='" + IsMaster.Trim() + "' and CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"'  ";
                
                Con.execute_NonQuery(Strsql);
                e.Cancel = true;
                GrdLocation.CancelEdit();
                ec.InsertLog("Department Record Deleted", IsMaster, IsMaster, Session["LoginUserName"].ToString().Trim(), Session["LoginCompany"].ToString().Trim().Trim());
                DataFilter.LoadDepartment(Session["LoginUserName"].ToString().Trim(), Session["LoginCompany"].ToString().Trim());
                BindData();
            }

           
        }
        catch (Exception Ex)
        {
            Error_Occured("GrdLocation_RowDeleting", Ex.Message);
        }
    }
    private bool ValidData(string CCode)
    {
        bool Check = false;
        try
        {

            Strsql = "Select * from tblEmployee where DEPARTMENTCODE='" + CCode + "' and CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"' ";
            
            DataSet DsCheck = new DataSet();
            DsCheck = Con.FillDataSet(Strsql);
            if (DsCheck.Tables[0].Rows.Count > 0)
            {
                Check = true;

            }

        }
        catch (Exception Ex)
        {
            Error_Occured("ValidData", Ex.Message);
        }

        return Check;
    }
    protected void GrdLocation_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        try
        {
             string k = e.NewValues["DEPARTMENTCODE"].ToString().Trim();
             DName = e.NewValues["DEPARTMENTNAME"].ToString().Trim();
             try
             {
                 DHOD = e.NewValues["DEPARTMENTHEAD"].ToString().Trim();
             }
            catch
             {
                 DHOD = "";
             }
             try
             {
                 DEmail = e.NewValues["EMAIL_ID"].ToString().Trim();
             }
            catch
             {
                 DEmail = "";
             }
             Strsql = " insert into tblDepartment (DEPARTMENTCODE,DEPARTMENTNAME,DEPARTMENTHEAD,EMAIL_ID,CompanyCode)values ('" + k.Trim() + "','" + DName.Trim() + "','" + DHOD.Trim() + "', " +
                      " '" + DEmail.Trim() + "','"+Session["LoginCompany"].ToString().Trim()+"')  ";
             Con.execute_NonQuery(Strsql);
             e.Cancel = true;
             ec.InsertLog("Department Record Inserted", DName, DName, Session["LoginUserName"].ToString().Trim(), Session["LoginCompany"].ToString().Trim().Trim());
             GrdLocation.CancelEdit();
             BindData();
             DataFilter.LoadDepartment(Session["LoginUserName"].ToString().Trim(), Session["LoginCompany"].ToString().Trim());
        }
        catch (Exception Ex)
        {
           Error_Occured("GrdLocation_RowInserting", Ex.Message);
        }
    }
    protected void GrdLocation_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
    {
        string CCode = Convert.ToString(e.NewValues["DEPARTMENTCODE"]);
        string CName = Convert.ToString(e.NewValues["DEPARTMENTNAME"]);
        string EMailID= Convert.ToString(e.NewValues["EMAIL_ID"]);
        bool email = false;
        // string k = e.Keys["COMPANYCODE"].ToString();
        if (CCode.ToString().Trim() == string.Empty)
        {
            AddError(e.Errors, GrdLocation.Columns[0], "Input Department Code");

            e.RowError = "*Input Department Code";
        }
        if (CName.ToString().Trim() == string.Empty)
        {
            AddError(e.Errors, GrdLocation.Columns[0], "Input Department Name");
            e.RowError = "*Input Department Name";
        }
        if (isNewRecord == true)
        {
            Strsql = "Select * from tblDepartment where DEPARTMENTCODE='" + CCode + "' and CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"'  ";
            DataSet DsIP = new DataSet();
            DsIP = Con.FillDataSet(Strsql);
            if (DsIP.Tables[0].Rows.Count > 0)
            {
                AddError(e.Errors, GrdLocation.Columns[0], "Duplicate Department Code");
                e.RowError = "*Duplicate Department Code";
            }
        }
         if (EMailID.ToString().Trim() == "")
        {
            email = true;
        }
        else
        {
            email = IsEmail(EMailID.Trim());
        }
         if (!email)
         {
             AddError(e.Errors, GrdLocation.Columns[0], "Invalid Email ID");
             e.RowError = "*Invalid Email ID";
         }
        
    }

    protected void GrdLocation_PageIndexChanged(object sender, EventArgs e)
    {
        int pageIndex = (sender as ASPxGridView).PageIndex;
        GrdLocation.PageIndex = pageIndex;
        this.BindData();  
    }

    protected void GrdLocation_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption == "Head")
        {
            string PayCode = e.GetValue("DEPARTMENTHEAD").ToString().Trim();
            if (PayCode.Trim()!=string.Empty)
            {
                Strsql = "Select Empname from TblEmployee Where paycode='" + PayCode + "' and CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"'";
                DataSet dsEmp = new DataSet();
                dsEmp = Con.FillDataSet(Strsql);
                if(dsEmp.Tables[0].Rows.Count>0)
                {
                    e.Cell.Text = dsEmp.Tables[0].Rows[0][0].ToString().Trim();
                }
                else
                {
                    e.Cell.Text = "";
                }

            }
        }
    }
    protected void GrdLocation_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
    {
        string x = "";
    }

    protected void GrdLocation_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
    {
        ((ASPxGridView)sender).JSProperties["cp_isSuccess"] = false;
    }

    protected void GrdLocation_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
    {
        ((ASPxGridView)sender).JSProperties["cp_isSuccess"] = false;
    }

    protected void GrdLocation_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
    {
        ((ASPxGridView)sender).JSProperties["cp_isSuccess"] = false;
    }

    protected void GrdLocation_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
    {
        if (Session["AddDep"].ToString().Trim() == "Y")
        {

            if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.New)
            {
                e.Enabled = true;
            }
        }
        else
        {
            if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.New)
            {
                e.Enabled = false;
            }
        }

        if (Session["EditDep"].ToString().Trim() == "Y")
        {

            if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Edit)
            {
                e.Enabled = true;
            }
        }
        else
        {
            if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Edit)
            {
                e.Enabled = false;
            }
        }
        if (Session["DelDep"].ToString().Trim() == "Y")
        {

            if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Delete)
            {
                e.Enabled = true;
            }
        }
        else
        {
            if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Delete)
            {
                e.Enabled = false;
            }
        }
    }
}