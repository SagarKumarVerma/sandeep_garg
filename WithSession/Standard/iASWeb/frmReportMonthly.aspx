﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="frmReportMonthly.aspx.cs" Inherits="frmReportMonthly" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
     <div align="center" style="vertical-align:top" > 
               <dx:ASPxPanel ID="ASPxPanel2" runat="server" Width="100%">                   
                    <PanelCollection>
<dx:PanelContent runat="server">
 
<table align="center" style="width:100%" cellpadding="0" cellspacing="0" >
<tr>
<td ></td>
</tr>
<tr>
<td colspan="3" class="tableHeaderCss" align="center" style="height:25px; width: 100%;">
 <asp:Label ID="lblMsg" runat="server" Text="Monthly Attendance Report" CssClass="lblCss" ></asp:Label>  </td>
</tr>
    <tr>
<td >

    <table cellspacing="1" style="border-style: solid; border-width: thin; padding: 5px; width: 100%;">
        <tr style="padding: 5px; border-style: solid; border-width: thin; padding: 5px; width: 100%; height: 19px; text-align: left;">
      
              <td style="padding: 5px" colspan="8">
                <dx:ASPxPanel ID="ASPxPanel3" runat="server" Width="100%">                   
                    <PanelCollection>
<dx:PanelContent runat="server">
   <table style="padding: 5px; width: 100%;">
       <tr>
           <td style="padding: 5px; width: 10%;">
               <dx:ASPxLabel ID="lblfrom" runat="server" Text="From">
                </dx:ASPxLabel>
           </td>
           <td style="padding: 5px ; width: 10%;">
                <dx:ASPxDateEdit ID="txtDateFrom" runat="server" Width="120px" EditFormatString="dd/MM/yyyy" AutoPostBack="True" OnDateChanged="txtDateFrom_DateChanged" OnCalendarDayCellPrepared="txtDateFrom_CalendarDayCellPrepared">
                </dx:ASPxDateEdit>
            </td>
            <td style="padding: 5px ; width: 10%;">
                <dx:ASPxLabel ID="lblto" runat="server" Text="To Date" Visible="true" >
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px ; width: 10%;">
                <dx:ASPxDateEdit ID="txtToDate" runat="server" Width="120px" EditFormatString="dd/MM/yyyy" Visible="true" OnCalendarDayCellPrepared="txtToDate_CalendarDayCellPrepared" >
                </dx:ASPxDateEdit>
            </td>
            <td style="padding: 5px ; width: 10%;">
                <dx:ASPxButton ID="btnGenerate" runat="server" Text="Generate" OnClick="btnGenerate_Click" >
                </dx:ASPxButton>
            </td>
            <td style="padding: 5px ; width: 10%;">
                <dx:ASPxRadioButton ID="radExcel" runat="server" GroupName="A" Checked="true" Text="Excel">
                </dx:ASPxRadioButton>
            </td>
            <td style="padding: 5px ; width: 70%;">
                <dx:ASPxRadioButton ID="radPDF" runat="server" GroupName="A" Text="PDF">
                </dx:ASPxRadioButton>
            </td>
           <td style="padding: 5px ; width: 30%;">
               
            </td>
       </tr>
          </table>
                        </dx:PanelContent>
</PanelCollection>
                   
                    <Border BorderStyle="Solid" BorderWidth="1px" />
                   
 </dx:ASPxPanel>
       </td>

        </tr>
    
        <tr>
            <td style="padding: 5px">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Select Company">
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px">
                   <dx:ASPxGridLookup ID="LookupCompany" runat="server" SelectionMode="Multiple"  ClientInstanceName="gridLookup"
                                                                            KeyFieldName="CompanyCode"  TextFormatString="{0}" MultiTextSeparator=",">
                                                                            <Columns>
                                                                                <dx:GridViewCommandColumn ShowSelectCheckbox="True" Caption="" Width="10px"  SelectAllCheckboxMode="AllPages"/>
                                                                                <dx:GridViewDataColumn FieldName="CompanyCode" Caption="Code" Width="50px" />
                                                                                <dx:GridViewDataColumn FieldName="Companyname"  Caption="Name">

                                                                                </dx:GridViewDataColumn>
                                                                            </Columns>
                                                                            <GridViewProperties>
                                                                                <Templates>
                                                                                    <StatusBar>
                                                                                        <table class="OptionsTable" style="float: right">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </StatusBar>
                                                                                </Templates>
                                                                                <Settings ShowFilterRow="True" ShowStatusBar="Visible" />

<SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True"></SettingsBehavior>

                                                                                <SettingsPager PageSize="7" EnableAdaptivity="true" />
                                                                            </GridViewProperties>
                                                                            <ClearButton DisplayMode="Always">
                                                                            </ClearButton>
                                                                        </dx:ASPxGridLookup>
            </td>
            <td style="padding: 5px">
                <dx:ASPxLabel ID="selDep" runat="server" Text="Select Department" >
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px">
                <dx:ASPxGridLookup ID="LookUpDept" runat="server" ClientInstanceName="GDep" KeyFieldName="DepartmentCode" MultiTextSeparator="," SelectionMode="Multiple" TextFormatString="{0}">
                    <Columns>
                        <dx:GridViewCommandColumn Caption="" ShowSelectCheckbox="True" Width="10px" SelectAllCheckboxMode="AllPages" />
                        <dx:GridViewDataColumn Caption="Code" FieldName="DepartmentCode" Width="50px" />
                        <dx:GridViewDataColumn Caption="Name" FieldName="departmentname"  >

                        </dx:GridViewDataColumn>
                    </Columns>
                    <GridViewProperties>
                        <Templates>
                            <StatusBar>
                                <table class="OptionsTable" style="float: right">
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>
                            </StatusBar>
                        </Templates>
                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" />

<SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True"></SettingsBehavior>

                        <SettingsPager EnableAdaptivity="true" PageSize="7" />
                    </GridViewProperties>
                    <ClearButton DisplayMode="Always">
                    </ClearButton>
                </dx:ASPxGridLookup>
            </td>
            <td style="padding: 5px">
                &nbsp;</td>
            <td style="padding: 5px">
                &nbsp;</td>
            <td style="padding: 5px">
                &nbsp;</td>
            <td style="padding: 5px">
                &nbsp;</td>
        </tr>
         <tr>
            <td style="padding: 5px">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Select Section">
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px">
                 <dx:ASPxGridLookup ID="lookupSec" runat="server" ClientInstanceName="lookupSec" KeyFieldName="DivisionCode" MultiTextSeparator="," SelectionMode="Multiple" TextFormatString="{0}">
                    <Columns>
                        <dx:GridViewCommandColumn Caption="" ShowSelectCheckbox="True" Width="10px"  SelectAllCheckboxMode="AllPages"/>
                        <dx:GridViewDataColumn Caption="Code" FieldName="DivisionCode" Width="50px" />
                        <dx:GridViewDataColumn Caption="Name" FieldName="DivisionName"  >

                        </dx:GridViewDataColumn>
                    </Columns>
                    <GridViewProperties>
                        <Templates>
                            <StatusBar>
                                <table class="OptionsTable" style="float: right">
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>
                            </StatusBar>
                        </Templates>
                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" />

<SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True"></SettingsBehavior>

                        <SettingsPager EnableAdaptivity="true" PageSize="7" />
                    </GridViewProperties>
                     <ClearButton DisplayMode="Always">
                     </ClearButton>
                </dx:ASPxGridLookup>
            </td>
            <td style="padding: 5px">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Select Grade" >
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px">
                  <dx:ASPxGridLookup ID="lookupGrade" runat="server" ClientInstanceName="lookupGrade" KeyFieldName="GradeCode" MultiTextSeparator="," SelectionMode="Multiple" TextFormatString="{0}">
                    <Columns>
                        <dx:GridViewCommandColumn Caption="" ShowSelectCheckbox="True" Width="10px" SelectAllCheckboxMode="AllPages" />
                        <dx:GridViewDataColumn Caption="Code" FieldName="GradeCode" Width="50px" />
                        <dx:GridViewDataColumn Caption="Name" FieldName="GradeName"  >

                        </dx:GridViewDataColumn>
                    </Columns>
                    <GridViewProperties>
                        <Templates>
                            <StatusBar>
                                <table class="OptionsTable" style="float: right">
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>
                            </StatusBar>
                        </Templates>
                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" />

<SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True"></SettingsBehavior>

                        <SettingsPager EnableAdaptivity="true" PageSize="7" />
                    </GridViewProperties>
                      <ClearButton DisplayMode="Always">
                      </ClearButton>
                </dx:ASPxGridLookup>
            </td>
            <td style="padding: 5px">
                &nbsp;</td>
            <td style="padding: 5px">
                &nbsp;</td>
            <td style="padding: 5px">
                &nbsp;</td>
            <td style="padding: 5px">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="padding: 5px">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Select Category">
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px">
                <dx:ASPxGridLookup ID="lookupCat" runat="server" ClientInstanceName="lookupCat" KeyFieldName="CAT" MultiTextSeparator=","  SelectionMode="Multiple" TextFormatString="{0}">
                    <Columns>
                        <dx:GridViewCommandColumn Caption="" ShowSelectCheckbox="True" Width="10px"  SelectAllCheckboxMode="AllPages"/>
                        <dx:GridViewDataColumn Caption="Code" FieldName="CAT" Width="50px" />
                        <dx:GridViewDataColumn Caption="Name" FieldName="CATAGORYNAME"  >

                        </dx:GridViewDataColumn>
                    </Columns>
                    <GridViewProperties>
                        <Templates>
                            <StatusBar>
                                <table class="OptionsTable" style="float: right">
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>
                            </StatusBar>
                        </Templates>
                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" />

<SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True"></SettingsBehavior>

                        <SettingsPager EnableAdaptivity="true" PageSize="7" />
                    </GridViewProperties>
                    <ClearButton DisplayMode="Always">
                    </ClearButton>
                </dx:ASPxGridLookup>
            </td>
            <td style="padding: 5px">
                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Select Shift" >
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px">
                <dx:ASPxGridLookup ID="LookUpShift" runat="server" ClientInstanceName="LookUpShift" KeyFieldName="Shift" MultiTextSeparator="," SelectionMode="Multiple" TextFormatString="{0}" >
                    <Columns>
                        <dx:GridViewCommandColumn Caption="" ShowSelectCheckbox="True" Width="10px" SelectAllCheckboxMode="AllPages"/>
                        <dx:GridViewDataColumn Caption="Code" FieldName="Shift" Width="50px" />
                        <dx:GridViewDataColumn Caption="Name" FieldName="ShiftTime"  >

                        </dx:GridViewDataColumn>
                    </Columns>
                    <GridViewProperties>
                        <Templates>
                            <StatusBar>
                                <table class="OptionsTable" style="float: right">
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>
                            </StatusBar>
                        </Templates>
                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" />

<SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True"></SettingsBehavior>

                        <SettingsPager EnableAdaptivity="true" PageSize="7" />
                    </GridViewProperties>
                    <ClearButton DisplayMode="Always">
                    </ClearButton>
                </dx:ASPxGridLookup>
            </td>
            <td style="padding: 5px">&nbsp;</td>
            <td style="padding: 5px">&nbsp;</td>
            <td style="padding: 5px">&nbsp;</td>
            <td style="padding: 5px">&nbsp;</td>
        </tr>
          <tr>
            <td style="padding: 5px">
                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="Select Employee">
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px">
                <dx:ASPxGridLookup ID="LookEmp" runat="server" ClientInstanceName="LookEmp" KeyFieldName="PAYCODE" MultiTextSeparator="," SelectionMode="Multiple" TextFormatString="{0}">
                    <Columns>
                        <dx:GridViewCommandColumn Caption="" ShowSelectCheckbox="True" Width="10px" SelectAllCheckboxMode="AllPages" />
                        <dx:GridViewDataColumn Caption="Code" FieldName="PAYCODE" Width="50px" />
                        <dx:GridViewDataColumn Caption="Name" FieldName="EMPNAME"  >

                        </dx:GridViewDataColumn>
                    </Columns>
                    <GridViewProperties>
                        <Templates>
                            <StatusBar>
                                <table class="OptionsTable" style="float: right">
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>
                            </StatusBar>
                        </Templates>
                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" />

<SettingsBehavior AllowFocusedRow="True" AllowSelectByRowClick="True"></SettingsBehavior>

                        <SettingsPager EnableAdaptivity="true" PageSize="7" />
                    </GridViewProperties>
                    <ClearButton DisplayMode="Always">
                    </ClearButton>
                </dx:ASPxGridLookup>
            </td>
            <td style="padding: 5px">
                <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="Sort By">
                </dx:ASPxLabel>
            </td>
            <td style="padding: 5px">
                <dx:ASPxComboBox ID="ddlSorting" runat="server" SelectedIndex="0">
                                <Items>
                                    <dx:ListEditItem Selected="True" Text="PayCode" Value="tblemployee.paycode" />
                                      <dx:ListEditItem  Selected="True" Value="tblemployee.paycode" Text="Paycode" /> 
                 <dx:ListEditItem  Value="tblemployee.presentcardno" Text="Card Number" />
                 <dx:ListEditItem  Value="tblemployee.EmpName" Text="Employee Name" />
                 <dx:ListEditItem  Value="tbldepartment.Departmentcode,tblemployee.paycode" Text="Department + Paycode" />
                 <dx:ListEditItem  Value="tbldepartment.Departmentcode,tblemployee.presentcardno" Text="Departmentcode + Card Number" />
                 <dx:ListEditItem  Value="tbldepartment.Departmentcode,tblemployee.empname" Text="Department + Name" />
                 <dx:ListEditItem  Value="tbldivision.divisioncode,tblemployee.paycode" Text="Section+Paycode" />
                 <dx:ListEditItem  Value="tbldivision.divisioncode,tblemployee.presentcardno" Text="Section + Card Number" />
                 <dx:ListEditItem  Value="tbldivision.divisioncode,tblemployee.empname" Text="Section + Name" />
                 <dx:ListEditItem  Value="tblcatagory.cat,tblemployee.paycode" Text="Catagory + Paycode" />
                 <dx:ListEditItem  Value="tblcatagory.Cat,tblemployee.presentcardno" Text="Catagory + Card Number" />
                 <dx:ListEditItem  Value="tblcatagory.Cat,tblemployee.empname" Text="Catagory + Name" />
                                </Items>
                            </dx:ASPxComboBox>
            </td>
            <td style="padding: 5px">&nbsp;</td>
            <td style="padding: 5px">&nbsp;</td>
            <td style="padding: 5px">&nbsp;</td>
            <td style="padding: 5px">&nbsp;</td>
        </tr>
        <tr>
            <td style="padding: 5px" colspan="8">
                <dx:ASPxPanel ID="ASPxPanel1" runat="server" Width="100%">                   
                    <PanelCollection>
<dx:PanelContent runat="server">
   <table style="padding: 5px; width: 100%;">
                    <tr>
                         <td style="padding: 5px; width:25% ;">
                             <dx:ASPxRadioButton ID="radEmployeeWisePerformance" AutoPostBack="true" Text="Employee Wise Performance" Checked="True" GroupName="a"   runat="server">
                             </dx:ASPxRadioButton>
                         </td>
                         <td style="padding: 5px; width:25% ;">
                             <dx:ASPxRadioButton ID="radEmployeewiseAttendance" AutoPostBack="true"  Text="Employee Wise Attendance"  GroupName="a"   runat="server">
                             </dx:ASPxRadioButton>
                         </td>
                         <td style="padding: 5px; width:25% ;">
                             <dx:ASPxRadioButton ID="radDepartmentWise" AutoPostBack="true" Text="Department Wise"   GroupName="a"   runat="server">
                             </dx:ASPxRadioButton>
                         </td>
                        <td style="padding: 5px; width:32% ;">
                           
                         </td>
                         <td style="padding: 5px; width:33% ;"></td>
                    </tr>
                             <tr>
                         <td style="padding: 5px; width:25% ;">
                             <dx:ASPxRadioButton ID="radMonthlyLate" AutoPostBack="true"  Text="Monthly Late Register"  Checked="True" GroupName="a"   runat="server">
                             </dx:ASPxRadioButton>
                         </td>
                          <td style="padding: 5px; width:25% ;">
                             <dx:ASPxRadioButton ID="radEarlyDeparture" AutoPostBack="true" Text="Early Departure Register"   GroupName="a"   runat="server">
                             </dx:ASPxRadioButton>
                         </td>
                         <td style="padding: 5px; width:25% ;">
                             <dx:ASPxRadioButton ID="radAbsent" AutoPostBack="true"  Text="Absenteeism Register"  GroupName="a"   runat="server">
                             </dx:ASPxRadioButton>
                         </td>
                    </tr>
                             <tr>
                         <td style="padding: 5px; width:25% ;">
                             <dx:ASPxRadioButton ID="radshiftwise" AutoPostBack="true"  Text="Shift Wise Attendance"  Checked="True" GroupName="a"   runat="server">
                             </dx:ASPxRadioButton>
                         </td>
                         <td style="padding: 5px; width:25% ;">
                             <dx:ASPxRadioButton ID="radMontlyOT" AutoPostBack="true" Text="Over Time Register"  GroupName="a"   runat="server">
                             </dx:ASPxRadioButton>
                         </td>
                         <td style="padding: 5px; width:25% ;">
                             <dx:ASPxRadioButton ID="radOT" AutoPostBack="true" Text="Over Time Summary"  GroupName="a"   runat="server">
                             </dx:ASPxRadioButton>
                         </td>
                    </tr>
                    <tr>
                        <td style="padding: 5px; width:25% ;">
                            <dx:ASPxRadioButton ID="radOverStay" AutoPostBack="true" Text="Over Stay Register" Checked="True" GroupName="a"   runat="server">
                            </dx:ASPxRadioButton>
                        </td>
                        <td style="padding: 5px; width:25% ;">
                            <dx:ASPxRadioButton ID="radShiftSchedule" AutoPostBack="true" Text="Shift Schedule" GroupName="a"   runat="server">
                            </dx:ASPxRadioButton>
                        </td>
                       <td style="padding: 5px; width:25% ;">
                            <dx:ASPxRadioButton ID="radEarlyDeptSummary" AutoPostBack="true" Text="Early Departure Summary" GroupName="a"   runat="server">
                            </dx:ASPxRadioButton>
                        </td>
                    </tr>
                      <tr>
                        <td style="padding: 5px; width:25% ;">
                            <dx:ASPxRadioButton ID="radLateArrivalSummary" AutoPostBack="true" Text="Late Arrival Summary" Checked="True" GroupName="a"   runat="server">
                            </dx:ASPxRadioButton>
                        </td>
                         <td style="padding: 5px; width:25% ;">
                            <dx:ASPxRadioButton ID="radLate_OverStay" AutoPostBack="true" Text="Late And OverStay" GroupName="a"   runat="server">
                            </dx:ASPxRadioButton>
                        </td>
                         <td style="padding: 5px; width:25% ;">
                            <dx:ASPxRadioButton ID="radTotalTimeLoss" AutoPostBack="true" Text="TimeLoss And Over Stay" GroupName="a"   runat="server">
                            </dx:ASPxRadioButton>
                        </td>
                    </tr>
                      <tr>
                        <td style="padding: 5px; width:25%; height: 31px;">
                            <dx:ASPxRadioButton ID="radEmpAttendance_Percentage" AutoPostBack="true" Text="Employee Wise Attendance Percentage" Checked="True" GroupName="a"   runat="server">
                            </dx:ASPxRadioButton>
                        </td>
                         <td style="padding: 5px; width:25%; height: 31px;">
                            <dx:ASPxRadioButton ID="radDeptWiseAttendance_Percentagte" AutoPostBack="true" Text="Department Wise Attendance Percentage" GroupName="a"   runat="server">
                            </dx:ASPxRadioButton>
                        </td>
                        <td style="padding: 5px; width:25%; height: 31px;">
                            <dx:ASPxRadioButton ID="radPerformanceRegister" AutoPostBack="true" Text="Performance Register" GroupName="a"   runat="server">
                            </dx:ASPxRadioButton>
                        </td>
                    </tr>
                      <tr>
                         <td style="padding: 5px; width:25% ;">
                            <dx:ASPxRadioButton ID="radMusterRoll" AutoPostBack="true" Text="Muster Roll" Checked="True" GroupName="a"   runat="server">
                            </dx:ASPxRadioButton>
                        </td>
                         <td style="padding: 5px; width:25% ;">
                            <dx:ASPxRadioButton ID="radManualPunch" AutoPostBack="true" Text="Detailed Attendance" GroupName="a"  Font-Bold="True" ForeColor="#006600"   runat="server" Visible="true">
                            </dx:ASPxRadioButton>
                        </td>
                        <td style="padding: 5px; width:25% ;">
                            <dx:ASPxRadioButton ID="radLogReport" AutoPostBack="true" Text="Daily Total Time" GroupName="a" Font-Bold="True" ForeColor="#006600"   runat="server" Visible="false">
                            </dx:ASPxRadioButton>
                        </td>
                    </tr>
      
         <tr id="Tday" runat="server" visible="false">
                         <td style="padding: 5px; width:25% ;">
                            <dx:ASPxLabel ID="lblDays" runat="server" Text="Enter Days"></dx:ASPxLabel>
                        </td>
                        <td style="padding: 5px; width:25% ;">
                            <dx:ASPxTextBox ID="txtDays" runat="server" Width="50px"></dx:ASPxTextBox>
                        </td>
                        <td style="padding: 5px; width:25% ;">
                           
                            <dx:ASPxRadioButton ID="radMRP" runat="server" AutoPostBack="True" GroupName="a"   Text="Machine Raw Punch">
                            </dx:ASPxRadioButton>
                           
                        </td>
                    </tr>
            <tr >
                         <td style="padding: 5px; " colspan="3">
                           <asp:GridView ID="GridView1" runat="server" Visible="false">
               </asp:GridView>
                        </td>
                       
                    </tr>
                  
                </table>
                        </dx:PanelContent>
</PanelCollection>
                   
                    <Border BorderStyle="Solid" BorderWidth="1px" />
                   
 </dx:ASPxPanel>
                
                   
                </td>
        </tr>
        
    </table>

</td>
</tr>
    </table>
    </dx:PanelContent>
    </PanelCollection>
                   </dx:ASPxPanel>
        </div>
</asp:Content>

