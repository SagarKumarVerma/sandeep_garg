﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using GlobalSettings;
using System.Collections.Generic;
using DevExpress.Web;
using System.Web.SessionState;
using DevExpress.Utils;
using DevExpress.XtraCharts;
using DevExpress.XtraCharts.Web;

public partial class Home : System.Web.UI.Page
{
    Class_Connection Con = new Class_Connection();
    string Strsql = null;
    DataSet ds = null;
    int result = 0;
    ErrorClass ec = new ErrorClass();
    DataFilter DF = new DataFilter();
    bool isNewRecord = false;
    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    string Cat, CatName, IsLate, EveryInterval, Deductfrom, Leave1, Leave2;
    double LayDays = 0;
    double MaxLateDur = 0;
    double DeductDays = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        //DivAbs.Style.Add("width", "98%");
        if (!Page.IsPostBack)
        {
            string usertype = Session["usertype"].ToString().Trim();
            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            if (Session["LoginUserType"] == null || Session["LoginUserType"].ToString().Trim() == "U")
            {
                Response.Redirect("Login.aspx");
            }
            BindDashboard();
            if (usertype.Trim()!="U")
            {
                //BindChartData();
                BindLocationGrid();
            }
           
           
        }


    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }
    public void BindDashboard()
    {
        try
        {

            if(Session["LoginUserName"].ToString().Trim().ToUpper() =="ADMIN")
            {
                Strsql = "select count(paycode)  from tblemployee where active='Y'";
            }
            else if(Session["LoginUserName"].ToString().Trim().ToUpper()=="H")
            {
                Strsql = "select count(paycode)  from tblemployee where active='Y' and Companycode='" + Session["LoginCompany"].ToString().Trim() + "' and Departmentcode in ("+DataFilter.AuthDept.Trim()+") ";
            }

            else
            {
                Strsql = "select count(paycode)  from tblemployee where active='Y' and Companycode='" + Session["LoginCompany"].ToString().Trim() + "' ";
            }
            
            ds = new DataSet();
            ds = Con.FillDataSet(Strsql);
            lblTotal.Text = ds.Tables[0].Rows[0][0].ToString().Trim();
            if (Session["LoginUserName"].ToString().Trim().ToUpper() == "ADMIN")
            {
                Strsql = " select Count(PayCode)  from tbltimeregister where dateoffice='" + System.DateTime.Today.ToString("yyyy-MM-dd") + "' and In1 Is Not Null  and paycode in " +
                        " (select paycode  from tblemployee where active='Y')";
            }
            else if (Session["LoginUserName"].ToString().Trim().ToUpper() == "H")
            {
                Strsql = " select Count(PayCode) from tbltimeregister where dateoffice='" + System.DateTime.Today.ToString("yyyy-MM-dd") + "' and In1 Is Not Null   and paycode in " +
                       " (select paycode  from tblemployee where active='Y' and Companycode='" + Session["LoginCompany"].ToString().Trim() + "' and  Departmentcode in (" + DataFilter.AuthDept.Trim() + ")  )";
            }
            else
            {
                Strsql = " select Count(PayCode) from tbltimeregister where dateoffice='" + System.DateTime.Today.ToString("yyyy-MM-dd") + "' and In1 Is Not Null   and paycode in " +
                       " (select paycode  from tblemployee where active='Y' and Companycode='" + Session["LoginCompany"].ToString().Trim() + "' )";
            }
            ds = new DataSet();
            ds = Con.FillDataSet(Strsql);
            lblPresent.Text = ds.Tables[0].Rows[0][0].ToString().Trim();
            DivPer.Style.Add("width", "" + lblPresent.Text.Trim() + "%");
            if (Session["LoginUserName"].ToString().Trim().ToUpper() == "ADMIN")
            {
                Strsql = " select Count(PayCode)  from tbltimeregister where dateoffice='" + System.DateTime.Today.ToString("yyyy-MM-dd") + "' and SHIFT!='OFF' and In1 Is  Null  and paycode in " +
                        " (select paycode  from tblemployee where active='Y'  )";
            }
            else if (Session["LoginUserName"].ToString().Trim().ToUpper() == "H")
            {
                Strsql = " select Count(PayCode)  from tbltimeregister where dateoffice='" + System.DateTime.Today.ToString("yyyy-MM-dd") + "' and SHIFT!='OFF' and In1 Is  Null and paycode in " +
                       " (select paycode  from tblemployee where active='Y' and Companycode='" + Session["LoginCompany"].ToString().Trim() + "' and  Departmentcode in (" + DataFilter.AuthDept.Trim() + ")  )";
            }
            else
            {
                Strsql = " select Count(PayCode)  from tbltimeregister where dateoffice='" + System.DateTime.Today.ToString("yyyy-MM-dd") + "' and SHIFT!='OFF' and In1 Is  Null  and paycode in " +
                         " (select paycode  from tblemployee where active='Y' and Companycode='" + Session["LoginCompany"].ToString().Trim() + "' )";
            }
            ds = new DataSet();
            ds = Con.FillDataSet(Strsql);
            lblAbsent.Text = ds.Tables[0].Rows[0][0].ToString().Trim();
            DivAbs.Style.Add("width", ""+lblAbsent.Text.Trim()+"%");
            if (Session["LoginUserName"].ToString().Trim().ToUpper() == "ADMIN")
            {

                Strsql = " select sum(WO_VALUE)  from tbltimeregister where dateoffice='" + System.DateTime.Today.ToString("yyyy-MM-dd") + "' and paycode in " +
                        " (select paycode  from tblemployee where active='Y')";
            }
            else if (Session["LoginUserName"].ToString().Trim().ToUpper() == "H")
            {
                Strsql = " select SUM(WO_Value) from tbltimeregister where dateoffice='" + System.DateTime.Today.ToString("yyyy-MM-dd") + "'   and paycode in " +
                       " (select paycode  from tblemployee where active='Y' and Companycode='" + Session["LoginCompany"].ToString().Trim() + "' and  Departmentcode in (" + DataFilter.AuthDept.Trim() + ")  )";
            }
            else
            {
                Strsql = " select sum(WO_VALUE)  from tbltimeregister where dateoffice='" + System.DateTime.Today.ToString("yyyy-MM-dd") + "' and paycode in " +
                         " (select paycode  from tblemployee where active='Y' and Companycode='" + Session["LoginCompany"].ToString().Trim() + "' )";
            }
            ds = new DataSet();
            ds = Con.FillDataSet(Strsql);
            lblOff.Text = ds.Tables[0].Rows[0][0].ToString().Trim();
            DivOff.Style.Add("width", "" + lblOff.Text.Trim() + "%");
            if (Session["LoginUserName"].ToString().Trim().ToUpper() == "ADMIN")
            {
                Strsql = " select sum(HOLIDAY_VALUE)  from tbltimeregister where dateoffice='" + System.DateTime.Today.ToString("yyyy-MM-dd") + "' and paycode in " +
                       " (select paycode  from tblemployee where active='Y')";
            }
            else if (Session["LoginUserName"].ToString().Trim().ToUpper() == "H")
            {
                Strsql = " select SUM(HOLIDAY_VALUE) from tbltimeregister where dateoffice='" + System.DateTime.Today.ToString("yyyy-MM-dd") + "'   and paycode in " +
                       " (select paycode  from tblemployee where active='Y' and Companycode='" + Session["LoginCompany"].ToString().Trim() + "' and  Departmentcode in (" + DataFilter.AuthDept.Trim() + ")  )";
            }
            else
            {
                Strsql = " select sum(HOLIDAY_VALUE)  from tbltimeregister where dateoffice='" + System.DateTime.Today.ToString("yyyy-MM-dd") + "' and paycode in " +
                           " (select paycode  from tblemployee where active='Y' and Companycode='" + Session["LoginCompany"].ToString().Trim() + "')";
            }
            ds = new DataSet();
            ds = Con.FillDataSet(Strsql);
            lblHoliday.Text = ds.Tables[0].Rows[0][0].ToString().Trim();
            DivHoliday.Style.Add("width", "" + lblHoliday.Text.Trim() + "%");
            if (Session["LoginUserName"].ToString().Trim().ToUpper() == "ADMIN")
            {
                Strsql = " select sum(LEAVEAMOUNT)  from tbltimeregister where dateoffice='" + System.DateTime.Today.ToString("yyyy-MM-dd") + "' and paycode in " +
                      " (select paycode  from tblemployee where active='Y')";
            }
            else if (Session["LoginUserName"].ToString().Trim().ToUpper() == "H")
            {
                Strsql = " select SUM(LEAVEAMOUNT) from tbltimeregister where dateoffice='" + System.DateTime.Today.ToString("yyyy-MM-dd") + "'   and paycode in " +
                       " (select paycode  from tblemployee where active='Y' and Companycode='" + Session["LoginCompany"].ToString().Trim() + "' and  Departmentcode in (" + DataFilter.AuthDept.Trim() + ")  )";
            }
            else
            {

                Strsql = " select sum(LEAVEAMOUNT)  from tbltimeregister where dateoffice='" + System.DateTime.Today.ToString("yyyy-MM-dd") + "' and paycode in " +
                           " (select paycode  from tblemployee where active='Y' and Companycode='" + Session["LoginCompany"].ToString().Trim() + "')";
            }
            ds = new DataSet();
            ds = Con.FillDataSet(Strsql);
            lblLeave.Text = ds.Tables[0].Rows[0][0].ToString().Trim();
            DivHoliday.Style.Add("width", "" + lblLeave.Text.Trim() + "%");
            if (Session["LoginUserName"].ToString().Trim().ToUpper() == "ADMIN")
            {

                Strsql = " select count(PAYCODE)  from tbltimeregister where dateoffice='" + System.DateTime.Today.ToString("yyyy-MM-dd") + "' and LATEARRIVAL>0 and paycode in " +
                      " (select paycode  from tblemployee where active='Y')";
            }
            else if (Session["LoginUserName"].ToString().Trim().ToUpper() == "H")
            {
                Strsql = " select count(PAYCODE)  from tbltimeregister where dateoffice='" + System.DateTime.Today.ToString("yyyy-MM-dd") + "' and LATEARRIVAL>0    and paycode in " +
                       " (select paycode  from tblemployee where active='Y' and Companycode='" + Session["LoginCompany"].ToString().Trim() + "' and  Departmentcode in (" + DataFilter.AuthDept.Trim() + ")  )";
            }
            else
            {
                Strsql = " select count(PAYCODE)  from tbltimeregister where dateoffice='" + System.DateTime.Today.ToString("yyyy-MM-dd") + "' and LATEARRIVAL>0  and paycode in " +
                          " (select paycode  from tblemployee where active='Y' and Companycode='" + Session["LoginCompany"].ToString().Trim() + "')";
            }
            ds = new DataSet();
            ds = Con.FillDataSet(Strsql);
            lblLate.Text = ds.Tables[0].Rows[0][0].ToString().Trim();
            divLate.Style.Add("width", "" + lblLate.Text.Trim() + "%");
            if (Session["LoginUserName"].ToString().Trim().ToUpper() == "ADMIN")
            {
                Strsql = " select count (paycode) from leave_request where Stage1_approved is null or Stage2_approved is null and paycode in " +
                      " (select paycode  from tblemployee where active='Y')";
            }
            else if (Session["LoginUserName"].ToString().Trim().ToUpper() == "H")
            {
                Strsql = " select count (paycode) from leave_request where Stage1_approved is null or Stage2_approved is null and paycode in " +
                    " (select paycode  from tblemployee where active='Y' and Companycode='" + Session["LoginCompany"].ToString().Trim() + "' and  Departmentcode in (" + DataFilter.AuthDept.Trim() + "))";
            }
            else
            {
                Strsql = " select count (paycode) from leave_request where Stage1_approved is null or Stage2_approved is null and paycode in " +
                    " (select paycode  from tblemployee where active='Y' and Companycode='" + Session["LoginCompany"].ToString().Trim() + "')";
            }
            ds = new DataSet();
            ds = Con.FillDataSet(Strsql);
            lblPendingReq.Text = ds.Tables[0].Rows[0][0].ToString().Trim();

            //For Device


            if (Session["LoginUserName"].ToString().Trim().ToUpper() == "ADMIN")
            {
                Strsql = " select count(UserID) from userattendance where Convert(Varchar(10),Attdatetime,120)='" + System.DateTime.Today.ToString("yyyy-MM-dd") + "' and UserID!='Visitor'";
            }
            else
            {
                Strsql = " select count(UserID) from userattendance where Convert(Varchar(10),Attdatetime,120)='" + System.DateTime.Today.ToString("yyyy-MM-dd") + "' and DeviceID IN " +
                         " (select SerialNumber  from tblmachine where Companycode='" + Session["LoginCompany"].ToString().Trim() + "') and UserID!='Visitor' ";
            }
            ds = new DataSet();
            ds = Con.FillDataSet(Strsql);
            lblPunch.Text = ds.Tables[0].Rows[0][0].ToString().Trim();


            if (Session["LoginUserName"].ToString().Trim().ToUpper() == "ADMIN")
            {
                Strsql = " select count(UserID) from userattendance where Convert(Varchar(10),Attdatetime,120)='" + System.DateTime.Today.ToString("yyyy-MM-dd") + "' and MaskStatus='YES' and UserID!='Visitor' ";
            }
            else
            {
                Strsql = " select count(UserID) from userattendance where Convert(Varchar(10),Attdatetime,120)='" + System.DateTime.Today.ToString("yyyy-MM-dd") + "' and DeviceID IN " +
                         " (select SerialNumber  from tblmachine where Companycode='" + Session["LoginCompany"].ToString().Trim() + "') and MaskStatus='YES' and UserID!='Visitor' ";
            }
            ds = new DataSet();
            ds = Con.FillDataSet(Strsql);
            lblMask.Text = ds.Tables[0].Rows[0][0].ToString().Trim();


            if (Session["LoginUserName"].ToString().Trim().ToUpper() == "ADMIN")
            {
                Strsql = " select count(UserID) from userattendance where Convert(Varchar(10),Attdatetime,120)='" + System.DateTime.Today.ToString("yyyy-MM-dd") + "' and MaskStatus='NO' and UserID!='Visitor'";
            }
            else
            {
                Strsql = " select count(UserID) from userattendance where Convert(Varchar(10),Attdatetime,120)='" + System.DateTime.Today.ToString("yyyy-MM-dd") + "' and DeviceID IN " +
                         " (select SerialNumber  from tblmachine where Companycode='" + Session["LoginCompany"].ToString().Trim() + "') and MaskStatus='NO' and UserID!='Visitor' ";
            }
            ds = new DataSet();
            ds = Con.FillDataSet(Strsql);
            lblWMask.Text = ds.Tables[0].Rows[0][0].ToString().Trim();

            //if (Session["LoginUserName"].ToString().Trim().ToUpper() == "ADMIN")
            //{
            //    Strsql = " select count(*) from userattendance where Convert(Varchar(10),Attdatetime,120)='" + System.DateTime.Today.ToString("yyyy-MM-dd") + "' and UserID='Visitor' ";
            //}
            //else
            //{
            //    Strsql = " select count(*) from userattendance where Convert(Varchar(10),Attdatetime,120)='" + System.DateTime.Today.ToString("yyyy-MM-dd") + "' and DeviceID IN " +
            //             " (select SerialNumber  from tblmachine where Companycode='" + Session["LoginCompany"].ToString().Trim() + "') and UserID='Visitor' ";
            //}
            //ds = new DataSet();
            //ds = Con.FillDataSet(Strsql);
            //lblVisitor.Text = "";//ds.Tables[0].Rows[0][0].ToString().Trim();
        }
        catch (Exception Ex)
        {
            Error_Occured("BindDashboard", Ex.Message.Trim());
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        BindLocationGrid();
        GrdHC.DataBind();
    }
    public void BindChartData()
    {
        try
        {
            string ReportView = "";
           
            ReportView = ConfigurationSettings.AppSettings["ReportView_HeadID"].ToString();
            double HeadCount, Pre, Abs, Leave, Holiday, WeekOff;
            Strsql = " select isnull(count(tbltimeregister.paycode),0)'HeadCount',isnull(sum(tblTimeRegister.PRESENTVALUE),0) 'Present',isnull(sum(tblTimeRegister.ABSENTVALUE),0)'Absent', " +
                    " isnull(SUM(tblTimeRegister.WO_VALUE),0) 'Week Off',isnull(SUM(tblTimeRegister.HOLIDAY_VALUE),0)'Holiday',isnull(SUM(tblTimeRegister.LEAVEVALUE),0)'Leeave' from tblTimeRegister " +
                    " inner join TBLEMPLOYEE on TBLEMPLOYEE.PAYCODE=tblTimeRegister.PAYCODE where DateOFFICE=convert(varchar(10),GETDATE(),121) and TBLEMPLOYEE.ACTIVE='Y' ";

            if (Session["usertype"].ToString() == "H")  //Reproting Manager 
            {
                if (ReportView.ToString().Trim() == "Y")
                {

                    Strsql += "and headid in(select paycode from tblemployee where headid='" + Session["PAYCODE"].ToString() + "' ) or  headid='" + Session["PAYCODE"].ToString() + "' ";
                }
                else
                {
                    if (Session["Auth_Comp"] != null)
                    {
                        Strsql += " and tblemployee.companycode in (" + Session["Auth_Comp"].ToString() + ") ";
                    }
                    if (Session["Auth_Dept"] != null)
                    {
                        Strsql += " and tblemployee.departmentcode in (" + Session["Auth_Dept"].ToString() + ") ";
                    }
                }
            }
            else
            {
                if (Session["Auth_Comp"] != null)
                {
                    Strsql += " and tblemployee.companycode in (" + Session["Auth_Comp"].ToString() + ") ";
                }
                if (Session["Auth_Dept"] != null)
                {
                    Strsql += " and tblemployee.departmentcode in (" + Session["Auth_Dept"].ToString() + ") ";
                }
            } 
            
            DataSet DsC = new DataSet();
           // DataTable dt = new DataTable();
            DsC = Con.FillDataSet(Strsql);
            if (DsC.Tables[0].Rows.Count > 0)
            {
                HeadCount = Convert.ToDouble(DsC.Tables[0].Rows[0]["HeadCount"].ToString().Trim());
                Pre = Convert.ToDouble(DsC.Tables[0].Rows[0]["Present"].ToString().Trim());
                Abs = Convert.ToDouble(DsC.Tables[0].Rows[0]["Absent"].ToString().Trim());
                Leave = Convert.ToDouble(DsC.Tables[0].Rows[0]["Leeave"].ToString().Trim());
                WeekOff = Convert.ToDouble(DsC.Tables[0].Rows[0]["Week Off"].ToString().Trim());
                Holiday = Convert.ToDouble(DsC.Tables[0].Rows[0]["Holiday"].ToString().Trim());

                try
                {
                    Strsql = "";
                }
                catch
                {

                }


            }
        }
        catch (Exception )
        {
            
        }
    }


    public void BindLocationGrid()
    {
        try
        {
            GrdHC.Visible = true;
            Strsql = " select  SHIFTATTENDED,count(PAYCODE) 'Total Man Power',sum(presentvalue) 'Present',SUM(ABSENTVALUE) 'Absent',SUM(WO_VALUE) 'OFF' from tbltimeregister where  " +
                     " DateOFFICE='"+System.DateTime.Today.ToString("yyyy-MM-dd")+"' and ssn like '"+Session["LoginCompany"].ToString().Trim()+"_%' group by SHIFTATTENDED";
            ds = new DataSet();
            ds = Con.FillDataSet(Strsql);

            if (ds.Tables[0].Rows.Count > 0)
            {
                GrdHC.DataSource = ds;
                GrdHC.DataBind();
            }
            else
            {
                GrdHC.Visible = false;
            }
        }
        catch (Exception Ex)
        {
            GrdHC.Visible = false;
            Error_Occured("BindLocationGrid", Ex.Message);
        }
    }

    protected void WebChartControl1_BoundDataChanged(object sender, EventArgs e)
    {

    }
    protected void WebChartControl1_CustomCallback(object sender, CustomCallbackEventArgs e)
    {

    }
}