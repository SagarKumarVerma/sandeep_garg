﻿
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
public partial class EditUser : System.Web.UI.Page
{
    Class_Connection Con = new Class_Connection();
    string Strsql = null;
    DataSet ds = null;
    int result = 0;

    string Main = null;
    string DataProcess = null;
    string Admin = null;
    string Transaction = null;
    string Company = null;
    string Dept = null;
    string Section = null;
    string Grade = null;
    string Category = null;
    string Shift = null;
    string Employee = null;
    string Punch = null;
    string OsToOt = null;
    string shiftchange = null;
    string register_creation = null;
    string register_updation = null;
    string BDP = null;
    string Re_Processing = null;
    string Leaves = null;
    string leave_Master = null;
    string leave_App = null;
    string leave_Accural = null;
    string autoleave = null;
    string auth_comp = "", auth_dept = "";
    string CCode = "";

    string Reports = null;
    string TimeOfficeReport = null;
    string Timeofficesetup = null;
    string Userprivilge = null;
    string Verification = null;

    string Companycode = null;
    string DeptCode = null;
    string payrollreports = "";
    string PayRollManagement = "";
    string EmployeeSetup = "";
    string Arrear = "";
    string AdvanceLoan = "";
    string Form16 = "";
    string PayrollProcess = "";
    string PayrollFormula = "";
    string PayrollSetup = "";
    string PieceManagement = "";
    string RosterView = "";

    string EmpGrp = "";
    string EmpUpload = "";
    String Holiday = "";
    ErrorClass ec = new ErrorClass();

    string AddEmp = null;
    string EditEmp = null;
    string DelEmp = null;


    string AddCmp = null;
    string EditCmp = null;
    string DelCmp = null;

    string AddDep = null;
    string EditDep = null;
    string DelDep = null;


    string AddSec = null;
    string EditSec = null;
    string DelSec = null;

    string AddGrade = null;
    string EditGrade = null;
    string DelGrade = null;

    string AddCat = null;
    string EditCat = null;
    string DelCat = null;

    string AddShift = null;
    string EditShift = null;
    string DelShift = null;

    string AddGrp = null;
    string EditGrp = null;
    string DelGrp = null;


    string IsDesignation = null;
    string AddDesignation = null;
    string EditDesignation = null;
    string DeleteDesignation = null;
    string AddInActive = null;


    string DeviceMange = null;
    string BioDevice = null;
    string HTDevice = null;


    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    protected void Page_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();
        if (ex is HttpRequestValidationException)
        {
            string resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" +
                   "Form doesn't need to contain valid HTML, just anything with opening and closing angled brackets (<...>).<br />" +
                   btnGoBack +
                   "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
       
        //Page.ClientScript.RegisterStartupScript(Type.GetType("System.String"), "addScript1", "Hide('" + Panel2.ClientID + "','" + Panel3.ClientID + "','" + Panel4.ClientID + "','" + Panel5.ClientID + "','" + Panel7.ClientID + "','" + Panel8.ClientID + "','" + ddlUserType.ClientID + "','" + cmdSelection.ClientID + "','"+tabPayroll.ClientID+"');", true);
        if (!Page.IsPostBack)
        {
            if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
            {
                Request.Browser.Adapters.Clear();
            }
            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            if (Session["LoginUserType"] == null || Session["LoginUserType"].ToString().Trim()=="U")
            {
                Response.Redirect("Login.aspx");
            }
            bindCompany();
            bindDept();
            TRSEL.Visible = false;
           
            //if (Session["FormName"] != null)
            //{
            //    lblMsg.Text = Session["FormName"].ToString();
            //}
            //if (Session["UserPrevilegeVisible"] == null)
            //{
            //    Response.Redirect("PageNotFound.aspx");
            //}
            if (Request.QueryString["Value"] != null)
            {
                TRSEL.Visible = false;
                string P_Code = Request.QueryString["Value"].ToString();
                txtUserName.Text = P_Code.ToString();
                chklapp.Visible = false;
                bindUser(P_Code);
                bindUserRights(P_Code);
                Session["IsNewRecord"] = "N";
                
              
            }
        }   
    }
    protected void bindFields()
    {
        Strsql = "select * from tblMapping";
        ds = new DataSet();
        ds = Con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            lblPaycode.Text = ds.Tables[0].Rows[1]["Showas"].ToString().Trim();
            chkCompany.Text = ds.Tables[0].Rows[4]["Showas"].ToString().Trim();
            chkDept.Text = ds.Tables[0].Rows[5]["Showas"].ToString().Trim();
            chkCategory.Text = ds.Tables[0].Rows[6]["Showas"].ToString().Trim();
            chkSection.Text = ds.Tables[0].Rows[7]["Showas"].ToString().Trim();
            chkGrade.Text = ds.Tables[0].Rows[8]["Showas"].ToString().Trim();
            chkEmployee.Text = ds.Tables[0].Rows[11]["Showas"].ToString().Trim();
        }
    }
    protected void bindUser(string P_Code)
    {
        if(Session["LoginUserName"].ToString().Trim().ToUpper()=="ADMIN")
        {
            Strsql = "Select paycode,userdescriprion,usertype,LoginType from tbluser where user_r='" + P_Code.ToString() + "'";
        }
        else
        {
            Strsql = "Select paycode,userdescriprion,usertype,LoginType from tbluser where user_r='" + P_Code.ToString() + "' and companycode='"+Session["LoginCompany"].ToString().Trim()+"'";
        }
        ds = new DataSet();
        ds = Con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            txtPaycode.Text = ds.Tables[0].Rows[0]["Paycode"].ToString().Trim();
            txtDescription.Text = ds.Tables[0].Rows[0]["userdescriprion"].ToString().Trim();

            string LType = ds.Tables[0].Rows[0]["LoginType"].ToString().Trim();
            if (LType.Trim() == "N")
            {
                ddlLoginType.SelectedIndex = 0;
            }
            else if (LType.Trim() == "L")
            {
                ddlLoginType.SelectedIndex = 1;
            }
            else
            {
                ddlLoginType.SelectedIndex = 0;
            }
            
            string type = ds.Tables[0].Rows[0]["usertype"].ToString().Trim();

            if (type.Trim() == "A")
            {
                ddlUserType.SelectedIndex = 0;
            }
            else if (type.Trim() == "H")
            {
                ddlUserType.SelectedIndex = 1;
                TRSEL.Visible = true;
            }
            else if (type.Trim() == "U")
            {
                ddlUserType.SelectedIndex = 2;
            }
            else
            {
                ddlUserType.SelectedIndex = 0;
            }
           // ddlUserType.SelectedItem.Value = ds.Tables[0].Rows[0]["usertype"].ToString().Trim();
            
            
            if (type.ToString().Trim() == "A")
            {
                chkLeaveAccural.Visible = true;
                chkAuto_leaveAccural.Visible = true;
                chkAdmin.Visible = true;
                Panel8.Enabled = true;
                tabPayroll.Enabled = true;
                chkTimeOffSetup.Visible = true;
                chkUserPrivilge.Visible = true;
                chkVerification.Visible = true;
                txtPaycode.Visible = false;
                lblPaycode.Visible = false;
                TRSEL.Visible = false;
                chklapp.Visible = true;
            }
            if (type.ToString().Trim() == "H")
            {
                chkLeaveAccural.Visible = true;
                chkAuto_leaveAccural.Visible = true;
                chkAdmin.Visible = true;
                Panel8.Enabled = true;
                tabPayroll.Enabled = true;
                chkTimeOffSetup.Visible = true;
                chkUserPrivilge.Visible = false;
                chkVerification.Visible = true;
                txtPaycode.Visible = false;
                lblPaycode.Visible = false;
                TRSEL.Visible = true;
                chklapp.Visible = true;
                //Page.ClientScript.RegisterStartupScript(Type.GetType("System.String"), "addScript6", "Admin('" + chkAdmin.ClientID + "','" + chkTimeOffSetup.ClientID + "','" + chkUserPrivilge.ClientID + "','" + chkVerification.ClientID + "');", true);                    
            }
            else if (type.ToString().Trim() == "U")
            {
                chkLeaveAccural.Visible = false;
                chkAuto_leaveAccural.Visible = false;
                chkAdmin.Visible = false;
                Panel8.Enabled = false;
                chkTimeOffSetup.Visible = false;
                chkUserPrivilge.Visible = false;
                chkVerification.Visible = false;
                txtPaycode.Visible = true;
                lblPaycode.Visible = true;
                tabPayroll.Enabled = true;
                TRSEL.Visible = false;
                chklapp.Visible = false;
            }
            else if (type.ToString().Trim() == "T")
            {
                chkLeaveAccural.Visible = false;
                chkAuto_leaveAccural.Visible = false;
                chkAdmin.Visible = false;
                Panel8.Enabled = false;
                chkTimeOffSetup.Visible = false;
                chkUserPrivilge.Visible = false;
                chkVerification.Visible = false;
                txtPaycode.Visible = true;
                lblPaycode.Visible = true;
                tabPayroll.Enabled = true;
              //  cmdSelection.Visible = false;
                chklapp.Visible = false;
            }
        }
    }
    protected void bindUserRights(string P_Code)
    {
        Strsql = "select paycode,userdescriprion,usertype,case autoprocess when 'Y' then 'Yes' else 'No' end as AutoProcess,case dataprocess when 'Y' then 'Yes' else 'No' end as DataProcess," +
                        "case V_Transaction when 'Y' then 'Yes' else 'No' end as V_Transaction,case Admin when 'Y' then 'Yes' else 'No' end as Admin," +
                        "case Payroll when 'Y' then 'Yes' else 'No' end as Payroll,case Reports when 'Y' then 'Yes' else 'No' end as Reports,case Leave when 'Y' then 'Yes' else 'No' end as Leaves, " +
                        "case Department when 'Y' then 'Yes' else 'No' end as Department, " +
                        "case Holiday when 'Y' then 'Yes' else 'No' end as Holiday, " +
                        "case ManualUpload when 'Y' then 'Yes' else 'No' end as ManualUpload, " +
                        "case Visitor when 'Y' then 'Yes' else 'No' end as Visitor, " +
                        "case Section when 'Y' then 'Yes' else 'No' end as Section, " +
                        "case Grade when 'Y' then 'Yes' else 'No' end as Grade, " +
                        "case Category when 'Y' then 'Yes' else 'No' end as Category, " +
                        "case Shift when 'Y' then 'Yes' else 'No' end as Shift, " +
                        "case Employee when 'Y' then 'Yes' else 'No' end as Employee, " +
                        "case Manual_Attendance when 'Y' then 'Yes' else 'No' end as PunchEntry, " +
                        "case OstoOt when 'Y' then 'Yes' else 'No' end as OstoOt, " +
                        "case shiftchange when 'Y' then 'Yes' else 'No' end as shiftchange, " +
                        "case RegisterCreation when 'Y' then 'Yes' else 'No' end as Register_creation, " +
                        "case RegisterUpdation when 'Y' then 'Yes' else 'No' end as Register_updation, " +
                        "case BackDateProcess when 'Y' then 'Yes' else 'No' end as BDP, " +
                        "case ReProcess when 'Y' then 'Yes' else 'No' end as reProcessing, " +
                        "case LeaveMaster when 'Y' then 'Yes' else 'No' end as LeaveMaster, " +
                        "case LeaveApplication when 'Y' then 'Yes' else 'No' end as LeaveApplication, " +
                        "case LeaveAccural when 'Y' then 'Yes' else 'No' end as LeaveAccural, " +
                        "case LeaveAccuralAuto when 'Y' then 'Yes' else 'No' end as LeaveAccuralAuto, " +
                        "case TimeOfficeReport when 'Y' then 'Yes' else 'No' end as TimeOfficeReport, " +
                        "case Main when 'Y' then 'Yes' else 'No' end as Main, " +
                        "case Company when 'Y' then 'Yes' else 'No' end as Company, " +
                        "case TimeOfficeSetup when 'Y' then 'Yes' else 'No' end as TimeOfficeSetup, " +
                        "case UserPrevilege when 'Y' then 'Yes' else 'No' end as UserPrevilege, " +
                        "case PayrollReport when 'Y' then 'Yes' else 'No' end as PayrollReport, " +

                       "case Payroll when 'Y' then 'Yes' else 'No' end as Payroll, " +
                       "case EmployeeSetup when 'Y' then 'Yes' else 'No' end as EmployeeSetup, " +
                       "case ArearEntry when 'Y' then 'Yes' else 'No' end as ArearEntry, " +
                       "case Advance_Loan when 'Y' then 'Yes' else 'No' end as Advance_Loan, " +
                       "case Form16 when 'Y' then 'Yes' else 'No' end as Form16, " +
                       "case Form16Return when 'Y' then 'Yes' else 'No' end as Form16Return, " +
                       "case payrollFormula when 'Y' then 'Yes' else 'No' end as payrollFormula, " +
                       "case PayrollSetup when 'Y' then 'Yes' else 'No' end as PayrollSetup, " +
                       "case LoanAdjustment when 'Y' then 'Yes' else 'No' end as LoanAdjustment, " +
                       "case RosterView when 'Y' then 'Yes' else 'No' end as RosterView, " +
                        //
                        "case CompAdd when 'Y' then 'Yes' else 'No' end as CompAdd, " +
                        "case CompModi when 'Y' then 'Yes' else 'No' end as CompModi, " +
                        "case CompDel when 'Y' then 'Yes' else 'No' end as CompDel, " +
                        "case DeptAdd when 'Y' then 'Yes' else 'No' end as DeptAdd, " +
                        "case DeptModi when 'Y' then 'Yes' else 'No' end as DeptModi, " +
                        "case DeptDel when 'Y' then 'Yes' else 'No' end as DeptDel, " +
                        "case CatAdd when 'Y' then 'Yes' else 'No' end as CatAdd, " +
                        "case CatModi when 'Y' then 'Yes' else 'No' end as CatModi, " +
                        "case CatDel when 'Y' then 'Yes' else 'No' end as CatDel, " +
                        "case SecAdd when 'Y' then 'Yes' else 'No' end as SecAdd, " +
                        "case SecModi when 'Y' then 'Yes' else 'No' end as SecModi, " +
                        "case SecDel when 'Y' then 'Yes' else 'No' end as SecDel, " +
                        "case GrdAdd when 'Y' then 'Yes' else 'No' end as GrdAdd, " +
                        "case GrdModi when 'Y' then 'Yes' else 'No' end as GrdModi, " +
                        "case GrdDel when 'Y' then 'Yes' else 'No' end as GrdDel, " +
                        "case SftAdd when 'Y' then 'Yes' else 'No' end as SftAdd, " +
                        "case SftModi when 'Y' then 'Yes' else 'No' end as SftModi, " +
                        "case SftDel when 'Y' then 'Yes' else 'No' end as SftDel, " +
                        "case EmpAdd when 'Y' then 'Yes' else 'No' end as EmpAdd, " +
                        "case EmpModi when 'Y' then 'Yes' else 'No' end as EmpModi, " +
                        "case EmpDel when 'Y' then 'Yes' else 'No' end as EmpDel, " +
                        "case EMPGRPADD when 'Y' then 'Yes' else 'No' end as EMPGRPADD, " +
                        "case EMPGRPMODI when 'Y' then 'Yes' else 'No' end as EMPGRPMODI, " +
                        "case EMPGRPDEL when 'Y' then 'Yes' else 'No' end as EMPGRPDEL, " +
                        "case EMPGRPDEL when 'Y' then 'Yes' else 'No' end as DESIGADD, " +
                        "case EMPGRPDEL when 'Y' then 'Yes' else 'No' end as DESIGMODI, " +
                        "case EMPGRPDEL when 'Y' then 'Yes' else 'No' end as DESIGDEL, " +
                        "case EMPGRPDEL when 'Y' then 'Yes' else 'No' end as DESIGNATION, " +
                        "case DeviceMangement when 'Y' then 'Yes' else 'No' end as DeviceMangement, " +
                        "case BioDeviceManage when 'Y' then 'Yes' else 'No' end as BioDeviceManage, " +
                        "case HTDeviceManage when 'Y' then 'Yes' else 'No' end as HTDeviceManage, " +
                       //
                       "case Verification when 'Y' then 'Yes' else 'No' end as Verification,auth_comp,auth_dept,CompanyCode " +
                       "from tbluser where user_r='" + P_Code.ToString() + "'";

        ds = new DataSet();
        ds = Con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            Main = ds.Tables[0].Rows[0]["Main"].ToString().Trim();
            Transaction = ds.Tables[0].Rows[0]["V_Transaction"].ToString().Trim();
            Company = ds.Tables[0].Rows[0]["Company"].ToString().Trim();
            Dept = ds.Tables[0].Rows[0]["Department"].ToString().Trim();
            Section = ds.Tables[0].Rows[0]["Section"].ToString().Trim();
            Grade = ds.Tables[0].Rows[0]["Grade"].ToString().Trim();
            Category = ds.Tables[0].Rows[0]["Category"].ToString().Trim();
            Shift = ds.Tables[0].Rows[0]["Shift"].ToString().Trim();
            Employee = ds.Tables[0].Rows[0]["Employee"].ToString().Trim();
            DataProcess = ds.Tables[0].Rows[0]["DataProcess"].ToString().Trim();
            Punch = ds.Tables[0].Rows[0]["PunchEntry"].ToString().Trim();
            OsToOt = ds.Tables[0].Rows[0]["OstoOt"].ToString().Trim();
            shiftchange = ds.Tables[0].Rows[0]["shiftchange"].ToString().Trim();
            Leaves = ds.Tables[0].Rows[0]["Leaves"].ToString().Trim();
            leave_Master = ds.Tables[0].Rows[0]["LeaveMaster"].ToString().Trim();
            leave_App = ds.Tables[0].Rows[0]["LeaveApplication"].ToString().Trim();
            leave_Accural = ds.Tables[0].Rows[0]["LeaveAccural"].ToString().Trim();
            autoleave = ds.Tables[0].Rows[0]["LeaveAccuralAuto"].ToString().Trim();
            Reports = ds.Tables[0].Rows[0]["Reports"].ToString().Trim();
            TimeOfficeReport = ds.Tables[0].Rows[0]["TimeOfficeReport"].ToString().Trim();
            register_creation = ds.Tables[0].Rows[0]["Register_creation"].ToString().Trim();
            register_updation = ds.Tables[0].Rows[0]["Register_updation"].ToString().Trim();
            BDP = ds.Tables[0].Rows[0]["BDP"].ToString().Trim();
            Re_Processing = ds.Tables[0].Rows[0]["reProcessing"].ToString().Trim();
            Admin = ds.Tables[0].Rows[0]["Admin"].ToString().Trim();
            Timeofficesetup = ds.Tables[0].Rows[0]["TimeOfficeSetup"].ToString().Trim();
            Userprivilge = ds.Tables[0].Rows[0]["UserPrevilege"].ToString().Trim();
            Verification = ds.Tables[0].Rows[0]["Verification"].ToString().Trim();
            auth_comp = ds.Tables[0].Rows[0]["auth_comp"].ToString();
            auth_dept = ds.Tables[0].Rows[0]["auth_dept"].ToString();
            payrollreports = ds.Tables[0].Rows[0]["PayrollReport"].ToString().Trim();

            PayRollManagement = ds.Tables[0].Rows[0]["Payroll"].ToString().Trim();
            EmployeeSetup = ds.Tables[0].Rows[0]["EmployeeSetup"].ToString().Trim();
            Arrear = ds.Tables[0].Rows[0]["ArearEntry"].ToString().Trim();
            AdvanceLoan = ds.Tables[0].Rows[0]["Advance_Loan"].ToString().Trim();
            Form16 = ds.Tables[0].Rows[0]["Form16"].ToString().Trim();
            PayrollProcess = ds.Tables[0].Rows[0]["Form16Return"].ToString().Trim();
            PayrollFormula = ds.Tables[0].Rows[0]["payrollFormula"].ToString().Trim();
            PayrollSetup = ds.Tables[0].Rows[0]["PayrollSetup"].ToString().Trim();
            PieceManagement = ds.Tables[0].Rows[0]["LoanAdjustment"].ToString().Trim();
            RosterView = ds.Tables[0].Rows[0]["RosterView"].ToString().Trim();
            Session["CompanyCode"] = auth_comp;
            Session["DeptCode"] = auth_dept;

            Holiday = ds.Tables[0].Rows[0]["Holiday"].ToString().Trim();
            EmpGrp = ds.Tables[0].Rows[0]["Visitor"].ToString().Trim();
            EmpUpload = ds.Tables[0].Rows[0]["ManualUpload"].ToString().Trim();
            CCode = ds.Tables[0].Rows[0]["CompanyCode"].ToString().Trim();
            DeviceMange = ds.Tables[0].Rows[0]["DeviceMangement"].ToString().Trim();
            BioDevice = ds.Tables[0].Rows[0]["BioDeviceManage"].ToString().Trim();
            HTDevice = ds.Tables[0].Rows[0]["HTDeviceManage"].ToString().Trim();


            //Response.Write(Session["CompanyCode"].ToString());
            // Response.Write(Session["DeptCode"].ToString());
            Label1.Text = auth_comp.ToString();
            Label2.Text = auth_dept.ToString();

            if(auth_comp.Trim()!="")
            {
                GridLookup.Value = auth_comp.ToString();
            }

            if (auth_dept.Trim() != "")
            {
                 
                string[] SplitDep = auth_dept.Trim().Split(',');
                foreach (string sc in SplitDep)
                {
                    GDep.GridView.Selection.SelectRowByKey(sc);
                }
                   
            }
            AddCmp = ds.Tables[0].Rows[0]["CompAdd"].ToString().Trim();
            EditCmp = ds.Tables[0].Rows[0]["CompModi"].ToString().Trim();
            DelCmp = ds.Tables[0].Rows[0]["CompDel"].ToString().Trim();
            AddDep = ds.Tables[0].Rows[0]["DeptAdd"].ToString().Trim();
            EditDep = ds.Tables[0].Rows[0]["DeptModi"].ToString().Trim();
            DelDep = ds.Tables[0].Rows[0]["DeptDel"].ToString().Trim();
            AddSec = ds.Tables[0].Rows[0]["SecAdd"].ToString().Trim();
            EditSec = ds.Tables[0].Rows[0]["SecModi"].ToString().Trim();
            DelSec = ds.Tables[0].Rows[0]["SecDel"].ToString().Trim();
            AddGrade = ds.Tables[0].Rows[0]["GrdAdd"].ToString().Trim();
            EditGrade = ds.Tables[0].Rows[0]["GrdModi"].ToString().Trim();
            DelGrade = ds.Tables[0].Rows[0]["GrdDel"].ToString().Trim();
            AddCat = ds.Tables[0].Rows[0]["CatAdd"].ToString().Trim();
            EditCat = ds.Tables[0].Rows[0]["CatModi"].ToString().Trim();
            DelCat = ds.Tables[0].Rows[0]["CatDel"].ToString().Trim();
            AddShift = ds.Tables[0].Rows[0]["SftAdd"].ToString().Trim();
            EditShift = ds.Tables[0].Rows[0]["SftModi"].ToString().Trim();
            DelShift = ds.Tables[0].Rows[0]["SftDel"].ToString().Trim();
            AddGrp = ds.Tables[0].Rows[0]["EMPGRPADD"].ToString().Trim();
            EditGrp = ds.Tables[0].Rows[0]["EMPGRPMODI"].ToString().Trim();
            DelGrp = ds.Tables[0].Rows[0]["EMPGRPDEL"].ToString().Trim();
            AddEmp = ds.Tables[0].Rows[0]["EmpAdd"].ToString().Trim();
            EditEmp = ds.Tables[0].Rows[0]["EmpModi"].ToString().Trim();
            DelEmp = ds.Tables[0].Rows[0]["EmpDel"].ToString().Trim();
            IsDesignation = ds.Tables[0].Rows[0]["DESIGNATION"].ToString().Trim();
            AddDesignation = ds.Tables[0].Rows[0]["DESIGADD"].ToString().Trim();
            EditDesignation = ds.Tables[0].Rows[0]["DESIGMODI"].ToString().Trim();
            DeleteDesignation = ds.Tables[0].Rows[0]["DESIGDEL"].ToString().Trim();




            /******************************Device ********************************/




            if (DeviceMange == "Yes")
            {
                chkDevice.Checked = true;
            }
            else
            {
                chkDevice.Checked = false;
            }
            if (BioDevice == "Yes")
            {
                chkBio.Checked = true;
            }
            else
            {
                chkBio.Checked = false;
            }
            if (HTDevice == "Yes")
            {
                chkHT.Checked = true;
            }
            else
            {
                chkHT.Checked = false;
            }

            /******************************PayRoll ********************************/

            if (RosterView == "Yes")
            {
                chklapp.Checked = true;
            }
            else
            {
                chklapp.Checked = false;
            }

            if (PayRollManagement == "Yes")
            {
                chkPayrollManagement.Checked = true;
            }
            else
            {
                chkPayrollManagement.Checked = false;
            }

            if (EmployeeSetup == "Yes")
            {
                chkEmployeeSetup.Checked = true;
            }
            else
            {
                chkEmployeeSetup.Checked = false;
            }

            if (Arrear == "Yes")
            {
                chkArrear.Checked = true;
            }
            else
            {
                chkArrear.Checked = false;
            }

            if (AdvanceLoan == "Yes")
            {
                chkAdvanceLoan.Checked = true;
            }
            else
            {
                chkAdvanceLoan.Checked = false;
            }

            if (Form16 == "Yes")
            {
                chkForm16.Checked = true;
            }
            else
            {
                chkForm16.Checked = false;
            }

            if (PayrollProcess == "Yes")
            {
                chkPayrollProcess.Checked = true;
            }
            else
            {
                chkPayrollProcess.Checked = false;
            }

            if (PayrollSetup == "Yes")
            {
                chkPayrollSetup.Checked = true;
            }
            else
            {
                chkPayrollSetup.Checked = false;
            }


            if (PayrollFormula == "Yes")
            {
                chkPayrollFormula.Checked = true;
            }
            else
            {
                chkPayrollFormula.Checked = false;
            }
            if (PieceManagement == "Yes")
            {
                chkPieceManagement.Checked = true;
            }
            else
            {
                chkPieceManagement.Checked = false;
            }
            /**************************Main**************************/

            if (Main == "Yes")
            {
                chkMain.Checked = true;
            }
            else
            {
                chkMain.Checked = false;
            }
            if (Company == "Yes")
            {
                chkCompany.Checked = true;
            }
            else
            {
                chkCompany.Checked = false;
            }

            if (Dept == "Yes")
            {
                chkDept.Checked = true;
            }
            else
            {
                chkDept.Checked = false;
            }

            if (Section == "Yes")
            {
                chkSection.Checked = true;
            }
            else
            {
                chkSection.Checked = false;
            }

            if (Grade == "Yes")
            {
                chkGrade.Checked = true;
            }
            else
            {
                chkGrade.Checked = false;
            }

            if (Category == "Yes")
            {
                chkCategory.Checked = true;
            }
            else
            {
                chkCategory.Checked = false;
            }

            if (Shift == "Yes")
            {
                chkShift.Checked = true;
            }
            else
            {
                chkShift.Checked = false;
            }

            if (Employee == "Yes")
            {
                chkEmployee.Checked = true;
            }
            else
            {
                chkEmployee.Checked = false;
            }

            if (EmpGrp == "Yes")
            {
                chkEmpGrp.Checked = true;
            }
            else
            {
                chkEmpGrp.Checked = false;
            }

            if (EmpUpload == "Yes")
            {
                chkEmpUpload.Checked = true;
            }
            else
            {
                chkEmpUpload.Checked = false;
            }


            // New Section For G4S
            if (AddCmp == "Yes")
            {
                ChkAddComp.Checked = true;
            }
            else
            {
                ChkAddComp.Checked = false;
            }
            if (EditCmp == "Yes")
            {
                ChkEditComp.Checked = true;
            }
            else
            {
                ChkEditComp.Checked = false;
            }
            if (DelCmp == "Yes")
            {
                ChkDeleteComp.Checked = true;
            }
            else
            {
                ChkDeleteComp.Checked = false;
            }
            if (AddDep == "Yes")
            {
                ChkAddDep.Checked = true;
            }
            else
            {
                ChkAddDep.Checked = false;
            }
            if (EditDep == "Yes")
            {
                ChkEditDep.Checked = true;
            }
            else
            {
                ChkEditDep.Checked = false;
            }
            if (DelDep == "Yes")
            {
                ChkDeleteDep.Checked = true;
            }
            else
            {
                ChkDeleteDep.Checked = false;
            }
            if (AddSec == "Yes")
            {
                ChkAddSec.Checked = true;
            }
            else
            {
                ChkAddSec.Checked = false;
            }
            if (EditSec == "Yes")
            {
                ChkEditSec.Checked = true;
            }
            else
            {
                ChkEditSec.Checked = false;
            }
            if (DelSec == "Yes")
            {
                ChkDeleteSec.Checked = true;
            }
            else
            {
                ChkDeleteSec.Checked = false;
            }
            if (AddGrade == "Yes")
            {
                ChkAddGrade.Checked = true;
            }
            else
            {
                ChkAddGrade.Checked = false;
            }
            if (EditGrade == "Yes")
            {
                ChkEditGrade.Checked = true;
            }
            else
            {
                ChkEditGrade.Checked = false;
            }
            if (DelGrade == "Yes")
            {
                ChkDeleteGrade.Checked = true;
            }
            else
            {
                ChkDeleteGrade.Checked = false;
            }
            if (AddCat == "Yes")
            {
                ChkAddCat.Checked = true;
            }
            else
            {
                ChkAddCat.Checked = false;
            }
            if (EditCat == "Yes")
            {
                ChkEditCat.Checked = true;
            }
            else
            {
                ChkEditCat.Checked = false;
            }
            if (DelCat == "Yes")
            {
                ChkDeleteCat.Checked = true;
            }
            else
            {
                ChkDeleteCat.Checked = false;
            }
            if (AddShift == "Yes")
            {
                ChkAddShift.Checked = true;
            }
            else
            {
                ChkAddShift.Checked = false;
            }
            if (EditShift == "Yes")
            {
                ChkEditShift.Checked = true;
            }
            else
            {
                ChkEditShift.Checked = false;
            }
            if (DelShift == "Yes")
            {
                ChkDeleteShift.Checked = true;
            }
            else
            {
                ChkDeleteShift.Checked = false;
            }
            if (AddGrp == "Yes")
            {
                ChkAddGrp.Checked = true;
            }
            else
            {
                ChkAddGrp.Checked = false;
            }
            if (EditGrp == "Yes")
            {
                ChkEditGrp.Checked = true;
            }
            else
            {
                ChkEditGrp.Checked = false;
            }
            if (DelGrp == "Yes")
            {
                ChkDeleteGrp.Checked = true;
            }
            else
            {
                ChkDeleteGrp.Checked = false;
            }
            if (AddEmp == "Yes")
            {
                ChkAddEmp.Checked = true;
            }
            else
            {
                ChkAddEmp.Checked = false;
            }
            if (EditEmp == "Yes")
            {
                ChkEditEmp.Checked = true;
            }
            else
            {
                ChkEditEmp.Checked = false;
            }
            if (DelEmp == "Yes")
            {
                ChkDeleteEmp.Checked = true;
            }
            else
            {
                ChkDeleteEmp.Checked = false;
            }

            if (IsDesignation == "Yes")
            {
                chkDesignation.Checked = true;
            }
            else
            {
                chkDesignation.Checked = false;
            }

            if (AddDesignation == "Yes")
            {
                chkAddDesig.Checked = true;
            }
            else
            {
                chkAddDesig.Checked = false;
            }
            if (EditDesignation == "Yes")
            {
                ChkEditDesig.Checked = true;
            }
            else
            {
                ChkEditDesig.Checked = false;
            }
            if (DeleteDesignation == "Yes")
            {
                ChkDelDesig.Checked = true;
            }
            else
            {
                ChkDelDesig.Checked = false;
            }

            //



            /********************Transaction******************/
            if (Transaction == "Yes")
            {
                chkTransaction.Checked = true;
            }
            else
            {
                chkTransaction.Checked = false;
            }

            if (Punch == "Yes")
            {
                chkPunchEntry.Checked = true;
            }
            else
            {
                chkPunchEntry.Checked = false;
            }

            if (OsToOt == "Yes")
            {
                chkOStoOT.Checked = true;
            }
            else
            {
                chkOStoOT.Checked = false;
            }

            if (shiftchange == "Yes")
            {
                chkShiftChange.Checked = true;
            }
            else
            {
                chkShiftChange.Checked = false;
            }

            if (Holiday == "Yes")
            {
                chkHLD.Checked = true;
            }
            else
            {
                chkHLD.Checked = false;
            }


            /***************Data Process******************/

            if (DataProcess == "Yes")
            {
                chkDataProcess.Checked = true;
            }
            else
            {
                chkDataProcess.Checked = false;
            }

            if (register_creation == "Yes")
            {
                chkAtt_Creation.Checked = true;
            }
            else
            {
                chkAtt_Creation.Checked = false;
            }
            if (register_updation == "Yes")
            {
                chkAtt_Updation.Checked = true;
            }
            else
            {
                chkAtt_Updation.Checked = false;
            }
            if (BDP == "Yes")
            {
                chkBDP.Checked = true;
            }
            else
            {
                chkBDP.Checked = false;
            }
            if (Re_Processing == "Yes")
            {
                chkReProcessing.Checked = true;
            }
            else
            {
                chkReProcessing.Checked = false;
            }
            /*****************Leaves ******************/
            if (Leaves == "Yes")
            {
                chkLeaveManagement.Checked = true;
            }
            else
            {
                chkLeaveManagement.Checked = false;
            }
            if (leave_Master == "Yes")
            {
                chkLeaveMaster.Checked = true;
            }
            else
            {
                chkLeaveMaster.Checked = false;
            }
            if (leave_App == "Yes")
            {
                chkLeaveApplication.Checked = true;
            }
            else
            {
                chkLeaveApplication.Checked = false;
            }
            if (leave_Accural == "Yes")
            {
                chkLeaveAccural.Checked = true;
            }
            else
            {
                chkLeaveAccural.Checked = false;
            }
            if (autoleave == "Yes")
            {
                chkAuto_leaveAccural.Checked = true;
            }
            else
            {
                chkAuto_leaveAccural.Checked = false;
            }


            /*****************Reports***********************/
            if (Reports == "Yes")
            {
                chkReports.Checked = true;
            }
            else
            {
                chkReports.Checked = false;
            }
            if (TimeOfficeReport == "Yes")
            {
                chkTimeOffice.Checked = true;
            }
            else
            {
                chkTimeOffice.Checked = false;
            }

            if (payrollreports == "Yes")
            {
                chkPayroll.Checked = true;
            }
            else
            {
                chkPayroll.Checked = false;
            }

            /*****************Admin***********************/
            if (Admin == "Yes")
            {
                chkAdmin.Checked = true;
            }
            else
            {
                chkAdmin.Checked = false;
            }
            if (Timeofficesetup == "Yes")
            {
                chkTimeOffSetup.Checked = true;
            }
            else
            {
                chkTimeOffSetup.Checked = false;
            }
            if (Userprivilge == "Yes")
            {
                chkUserPrivilge.Checked = true;
            }
            else
            {
                chkUserPrivilge.Checked = false;
            }
            if (Verification == "Yes")
            {
                chkVerification.Checked = true;
            }
            else
            {
                chkVerification.Checked = false;
            }

        }
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        //Call The function to write the error log file 

        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            //ec.Write_Log(Session["ErrorFileName"].ToString(), DateTime.Now.Date, PageName, FunctionName, ErrorMsg);
        }
        catch (Exception errr)
        {
        }
    }  
    protected void cmdCreate_Click(object sender, EventArgs e)
    {


        /*int dcCount = 0;
        StrSql = "Select Departmentcode from tbldepartment";
        dsCou = cn.FillDataSet(StrSql);
        if (dsCou.Tables[0].Rows.Count > 0)
        {
            dcCount = Convert.ToInt32(dsCou.Tables[0].Rows.Count);
            dcCount = dcCount * 4 + 50;

            StrSql = "select * from sys.objects where name ='DF__tblUser__Auth_de__164452B1'";
            dsCou = cn.FillDataSet(StrSql);
            if (dsCou.Tables[0].Rows.Count > 0)
            {
                StrSql = "alter table tbluser drop constraint DF__tblUser__Auth_de__164452B1";
                cn.execute_NonQuery(StrSql);
            }

            StrSql = "alter table tbluser alter column Auth_Dept varchar(" + dcCount + ")";
            cn.execute_NonQuery(StrSql);
        }
        dcCount = 0;
        StrSql = "Select Companycode from tblCompany";
        dsCou = cn.FillDataSet(StrSql);
        if (dsCou.Tables[0].Rows.Count > 0)
        {
            dcCount = Convert.ToInt32(dsCou.Tables[0].Rows.Count);
            dcCount = dcCount * 4 + 50;

            StrSql = "select * from sys.objects where name ='DF__tblUser__auth_co__15502E78'";
            dsCou = cn.FillDataSet(StrSql);
            if (dsCou.Tables[0].Rows.Count > 0)
            {
                StrSql = "alter table tbluser drop constraint DF__tblUser__auth_co__15502E78";
                cn.execute_NonQuery(StrSql);
            }

            StrSql = "alter table tbluser alter column Auth_Comp varchar(" + dcCount + ")";
            cn.execute_NonQuery(StrSql);
        }*/

        if (chklapp.Checked)
        {
            RosterView = "Y";
        }
        else
        {
            RosterView = "N";
        }

        if (chkDevice.Checked)
        {
            DeviceMange = "Y";
            if (chkBio.Checked)
            {
                BioDevice = "Y";
            }
            else
            {
                BioDevice = "N";
            }
            if (chkHT.Checked)
            {
                HTDevice = "Y";
            }
            else
            {
                HTDevice = "N";
            }
        }
        else
        {
            DeviceMange = "N";
            BioDevice = "N";
            HTDevice = "N";
        }
       


        if (chkMain.Checked)
        {
            Main = "Y";
        }
        else
        {
            Main = "N";
        }
        if (chkCompany.Checked)
        {
            Company = "Y";

            if (ChkAddComp.Checked)
            {
                AddCmp = "Y";
            }
            else
            {
                AddCmp = "N";
            }
            if (ChkEditComp.Checked)
            {
                EditCmp = "Y";
            }
            else
            {
                EditCmp = "N";
            }
            if (ChkDeleteComp.Checked)
            {
                DelCmp = "Y";
            }
            else
            {
                DelCmp = "N";
            }


        }
        else
        {
            Company = "N";
            AddCmp = "N";
            EditCmp = "N";
            DelCmp = "N";
        }




        if (chkDept.Checked)
        {
            Dept = "Y";
            if (ChkAddDep.Checked)
            {
                AddDep = "Y";
            }
            else
            {
                AddDep = "N";
            }
            if (ChkEditDep.Checked)
            {
                EditDep = "Y";
            }
            else
            {
                EditDep = "N";
            }
            if (ChkDeleteDep.Checked)
            {
                DelDep = "Y";
            }
            else
            {
                DelDep = "N";
            }

        }
        else
        {
            Dept = "N";
            DelDep = "N";
            EditDep = "N";
            AddDep = "N";
        }

        if (chkSection.Checked)
        {
            Section = "Y";

            if (ChkAddSec.Checked)
            {
                AddSec = "Y";
            }
            else
            {
                AddSec = "N";
            }
            if (ChkEditSec.Checked)
            {
                EditSec = "Y";
            }
            else
            {
                EditSec = "N";
            }
            if (ChkDeleteSec.Checked)
            {
                DelSec = "Y";
            }
            else
            {
                DelSec = "N";
            }


        }
        else
        {
            Section = "N";
            AddSec = "N";
            EditSec = "N";
            DelSec = "N";

        }

        if (chkGrade.Checked)
        {
            Grade = "Y";

            if (ChkAddGrade.Checked)
            {
                AddGrade = "Y";
            }
            else
            {
                AddGrade = "N";
            }
            if (ChkEditGrade.Checked)
            {
                EditGrade = "Y";
            }
            else
            {
                EditGrade = "N";
            }
            if (ChkDeleteGrade.Checked)
            {
                DelGrade = "Y";
            }
            else
            {
                DelGrade = "N";
            }


        }
        else
        {
            Grade = "N";
            AddGrade = "N";
            EditGrade = "N";
            DelGrade = "N";
        }

        if (chkCategory.Checked)
        {
            Category = "Y";
            if (ChkAddCat.Checked)
            {
                AddCat = "Y";
            }
            else
            {
                AddCat = "N";
            }
            if (ChkEditCat.Checked)
            {
                EditCat = "Y";
            }
            else
            {
                EditCat = "N";
            }
            if (ChkDeleteCat.Checked)
            {
                DelCat = "Y";
            }
            else
            {
                DelCat = "N";
            }
        }
        else
        {
            Category = "N";
            AddCat = "N";
            EditCat = "N";
            DelCat = "N";
        }

        if (chkShift.Checked)
        {
            Shift = "Y";
            if (ChkAddShift.Checked)
            {
                AddShift = "Y";
            }
            else
            {
                AddShift = "N";
            }
            if (ChkEditShift.Checked)
            {
                EditShift = "Y";
            }
            else
            {
                EditShift = "N";
            }
            if (ChkDeleteShift.Checked)
            {
                DelShift = "Y";
            }
            else
            {
                DelShift = "N";
            }
        }
        else
        {
            Shift = "N";
            AddShift = "N";
            EditShift = "N";
            DelShift = "N";
        }

        if (chkEmployee.Checked)
        {
            Employee = "Y";
            if (ChkAddEmp.Checked)
            {
                AddEmp = "Y";
            }
            else
            {
                AddEmp = "N";
            }
            if (ChkEditEmp.Checked)
            {
                EditEmp = "Y";
            }
            else
            {
                EditEmp = "N";
            }
            if (ChkDeleteEmp.Checked)
            {
                DelEmp = "Y";
            }
            else
            {
                DelEmp = "N";
            }
        }
        else
        {
            Employee = "N";
            AddEmp = "N";
            EditEmp = "N";
            DelEmp = "N";
        }

        if (chkEmpGrp.Checked)
        {
            EmpGrp = "Y";
            if (ChkAddGrp.Checked)
            {
                AddGrp = "Y";
            }
            else
            {
                AddGrp = "N";
            }
            if (ChkEditGrp.Checked)
            {
                EditGrp = "Y";
            }
            else
            {
                EditGrp = "N";
            }
            if (ChkDeleteEmp.Checked)
            {
                DelGrp = "Y";
            }
            else
            {
                DelGrp = "N";
            }
        }
        else
        {
            EmpGrp = "N";
            AddGrp = "N";
            EditGrp = "N";
            DelGrp = "N";
        }


        if (chkDesignation.Checked)
        {
            IsDesignation = "Y";
            if (chkAddDesig.Checked)
            {
                AddDesignation = "Y";
            }
            else
            {
                AddDesignation = "N";
            }
            if (ChkEditDesig.Checked)
            {
                EditDesignation = "Y";
            }
            else
            {
                EditDesignation = "N";
            }
            if (ChkDelDesig.Checked)
            {
                DeleteDesignation = "Y";
            }
            else
            {
                DeleteDesignation = "N";
            }
        }
        else
        {
            IsDesignation = "N";
            AddDesignation = "N";
            EditDesignation = "N";
            DeleteDesignation = "N";
        }




        if (chkEmpUpload.Checked)
        {
            EmpUpload = "Y";
        }
        else
        {
            EmpUpload = "N";
        }





        /********************Transaction******************/
        if (chkTransaction.Checked)
        {
            Transaction = "Y";
        }
        else
        {
            Transaction = "N";
        }

        if (chkPunchEntry.Checked)
        {
            Punch = "Y";
        }
        else
        {
            Punch = "N";
        }

        if (chkOStoOT.Checked)
        {
            OsToOt = "Y";
        }
        else
        {
            OsToOt = "N";
        }

        if (chkShiftChange.Checked)
        {
            shiftchange = "Y";
        }
        else
        {
            shiftchange = "N";
        }
        if (chkHLD.Checked)
        {
            Holiday = "Y";
        }
        else
        {
            Holiday = "N";
        }

        /***************Data Process******************/

        if (chkDataProcess.Checked)
        {
            DataProcess = "Y";
        }
        else
        {
            DataProcess = "N";
        }

        if (chkAtt_Creation.Checked)
        {
            register_creation = "Y";
        }
        else
        {
            register_creation = "N";
        }
        if (chkAtt_Updation.Checked)
        {
            register_updation = "Y";
        }
        else
        {
            register_updation = "N";
        }
        if (chkBDP.Checked)
        {
            BDP = "Y";
        }
        else
        {
            BDP = "N";
        }
        if (chkReProcessing.Checked)
        {
            Re_Processing = "Y";
        }
        else
        {
            Re_Processing = "N";
        }
        /*****************Leaves ******************/
        if (chkLeaveManagement.Checked)
        {
            Leaves = "Y";
        }
        else
        {
            Leaves = "N";
        }
        if (chkLeaveMaster.Checked)
        {
            leave_Master = "Y";
        }
        else
        {
            leave_Master = "N";
        }
        if (chkLeaveApplication.Checked)
        {
            leave_App = "Y";
        }
        else
        {
            leave_App = "N";
        }

        if (chkLeaveAccural.Checked)
        {
            leave_Accural = "Y";
        }
        else
        {
            leave_Accural = "N";
        }
        if (chkAuto_leaveAccural.Checked)
        {
            autoleave = "Y";
        }
        else
        {
            autoleave = "N";
        }
        /*****************Reports***********************/
        if (chkReports.Checked)
        {
            Reports = "Y";
        }
        else
        {
            Reports = "N";
        }
        if (chkTimeOffice.Checked)
        {
            TimeOfficeReport = "Y";
        }
        else
        {
            TimeOfficeReport = "N";
        }
        if (chkPayroll.Checked)
        {
            payrollreports = "Y";
        }
        else
        {
            payrollreports = "N";
        }
        /*****************Admin***********************/
        if (chkAdmin.Checked)
        {
            Admin = "Y";
        }
        else
        {
            Admin = "N";
        }
        if (chkTimeOffSetup.Checked)
        {
            Timeofficesetup = "Y";
        }
        else
        {
            Timeofficesetup = "N";
        }
        if (chkUserPrivilge.Checked)
        {
            Userprivilge = "Y";
        }
        else
        {
            Userprivilge = "N";
        }
        if (chkVerification.Checked)
        {
            Verification = "Y";
        }
        else
        {
            Verification = "N";
        }






        /***************** Payroll ***********************/
        if (chkPayrollManagement.Checked)
        {
            PayRollManagement = "Y";
        }
        else
        {
            PayRollManagement = "N";
        }
        if (chkEmployeeSetup.Checked)
        {
            EmployeeSetup = "Y";
        }
        else
        {
            EmployeeSetup = "N";
        }
        if (chkArrear.Checked)
        {
            Arrear = "Y";
        }
        else
        {
            Arrear = "N";
        }
        if (chkAdvanceLoan.Checked)
        {
            AdvanceLoan = "Y";
        }
        else
        {
            AdvanceLoan = "N";
        }

        if (chkForm16.Checked)
        {
            Form16 = "Y";
        }
        else
        {
            Form16 = "N";
        }
        if (chkPayrollProcess.Checked)
        {
            PayrollProcess = "Y";
        }
        else
        {
            PayrollProcess = "N";
        }
        if (chkPayrollFormula.Checked)
        {
            PayrollFormula = "Y";
        }
        else
        {
            PayrollFormula = "N";
        }
        if (chkPayrollSetup.Checked)
        {
            PayrollSetup = "Y";
        }
        else
        {
            PayrollSetup = "N";
        }
        if (chkPieceManagement.Checked)
        {
            PieceManagement = "Y";
        }
        else
        {
            PieceManagement = "N";
        }
        if (Session["IsNewRecord"].ToString() == "Y")
        {
            try
            {

                Strsql = "Select * from tbluser where user_r='" + txtUserName.Text.ToString().Trim() + "'";
                DataSet dsUser = new DataSet();
                dsUser = Con.FillDataSet(Strsql);
                if (dsUser.Tables[0].Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('User Name Already Exist');", true);
                    return;
                }
                else
                {
                    string k = Session["LoginCompany"].ToString().Trim()
; string SSN = k.Trim() + "_" + k.Trim().Replace("'", " ");
                    Strsql = " insert into tbluser(USER_R,USERDESCRIPRION,PASSWORD,UserPrevilege,paycode,Admin,Usertype,TimeOfficeSetup,LoginType,AutoProcess,DataProcess,Main,V_Transaction,Reports,Leave,Company,Department,Section,Grade,Category, " +
                             " Shift,Employee,Manual_Attendance,OstoOt,ShiftChange,HoliDay,LeaveMaster,LeaveApplication,LeaveAccural,LeaveAccuralAuto,Verification,InstallationSetup,EmployeeSetup,ArearEntry,TimeOfficeReport,RegisterCreation,RegisterUpdation,BackDateProcess,isValid,companycode,SSN,auth_comp,ManualUpload,Visitor,CompAdd, " +
                             " CompModi,CompDel,DeptAdd,DeptModi,DeptDel,CatAdd,CatModi,CatDel,SecAdd,SecModi,SecDel,GrdAdd,GrdModi,GrdDel,SftAdd,SftModi,SftDel,EmpAdd,EmpModi,EmpDel,EMPGRPADD,EMPGRPMODI,EMPGRPDEL,DeviceMangement,BioDeviceManage,HTDeviceManage) " +
                             " values('" + txtUserName.Text.ToString().Trim().Replace("'", " ") + "','" + txtDescription.Text.ToString().Trim().Replace("'", " ") + "','" + txtPassword.Text.ToString().Trim().Replace("'", " ") + "','Y','" + k.Trim().Replace("'", " ") + "','Y','A','Y','L','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','" + k.Trim() + "','" + SSN.ToString() + "','" + k.Trim() + "','Y','Y', " +
                             " 'Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','"+ DeviceMange.Trim() + "','" + BioDevice.Trim() + "','" + HTDevice.Trim() + "')";
                    result = Con.execute_NonQuery(Strsql);
                    Strsql = "";
                    OleDbDataReader AuthDr;
                    OleDbCommand cmd;
                    string AuthComp = "", AuthDept = "", pay = "", qry1 = "", qry2 = "";
                    qry1 = "Select DepartmentCode from TblDepartment where CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' order by DepartmentCode ";
                    qry2 = "Select CompanyCode from Tblcompany where CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' order by CompanyCode";
                    AuthDr = Con.Execute_Reader(qry1);
                    while (AuthDr.Read())
                    {
                        AuthDept += AuthDr[0].ToString();
                        AuthDept = AuthDept.Trim() + ",";
                    }
                    if (AuthDept.Trim() != "")
                        AuthDept = AuthDept.Substring(0, AuthDept.Length - 1);
                    AuthDr.Close();

                    DeptCode = AuthDept.Trim();

                    // Finding Auth Company
                    AuthDr = Con.Execute_Reader(qry2);

                    while (AuthDr.Read())
                    {
                        AuthComp += AuthDr[0].ToString();
                        AuthComp = AuthComp.Trim() + ",";
                    }
                    if (AuthComp.Trim() != "")
                        AuthComp = AuthComp.Substring(0, AuthComp.Length - 1);
                    AuthDr.Close();

                    Companycode = AuthComp.Trim();

                    Strsql += " update tbluser set  DeviceMangement='"+ DeviceMange.Trim() +"',BioDeviceManage='"+BioDevice.Trim()+"',HTDeviceManage='"+ HTDevice.Trim() +"',  DataProcess='" + DataProcess.ToString().Trim() + "',Leave='" + Leaves.ToString().Trim() + "',Reports='" + Reports.ToString().Trim() + "',Company='" + Company.ToString().Trim() + "' , " +
                              " Department='" + Dept.ToString().Trim() + "',Section='" + Section.ToString().Trim() + "',Grade='" + Grade.ToString().Trim() + "',Category='" + Category.ToString().Trim() + "',Shift='" + Shift.ToString().Trim() + "', " +
                              " Employee='" + Employee.ToString().Trim() + "',Manual_Attendance='" + Punch.ToString().Trim() + "',OstoOt='" + OsToOt.ToString().Trim() + "',ShiftChange='" + shiftchange.ToString().Trim() + "', " +
                              " LeaveMaster='" + leave_Master.ToString().Trim() + "',LeaveApplication='" + leave_App.ToString().Trim() + "',LeaveAccural='" + leave_Accural.ToString().Trim() + "',LeaveAccuralAuto='" + autoleave.ToString().Trim() + "', " +
                              " RegisterCreation='" + register_creation.ToString().Trim() + "',RegisterUpdation='" + register_updation.ToString().Trim() + "', " +
                              " BackDateProcess='" + BDP.ToString().Trim() + "',ReProcess='" + Re_Processing.ToString().Trim() + "', " +
                              " TimeOfficeReport='" + TimeOfficeReport.ToString().Trim() + "', " +
                              " Admin='" + Admin.ToString().Trim() + "',TimeOfficeSetup='" + Timeofficesetup.ToString().Trim() + "', " +
                              " UserPrevilege='" + Userprivilge.ToString().Trim() + "',Verification='" + Verification.ToString().Trim() + "', " +
                              " auth_comp='" + Companycode.ToString() + "',Auth_dept='" + DeptCode.ToString() + "', " +
                              " Payroll='" + PayRollManagement.ToString() + "',EmployeeSetup='" + AddInActive.ToString() + "',LoginType='L', " +
                              " ArearEntry='" + Arrear.ToString() + "',Advance_Loan='" + AdvanceLoan.ToString() + "', " +
                              " Form16='" + Form16.ToString() + "',Form16Return='" + PayrollProcess.ToString() + "',payrollFormula='" + PayrollFormula.ToString() + "', " +
                              " PayrollSetup='" + PayrollSetup.ToString() + "',LoanAdjustment='" + PieceManagement.ToString() + "',PayrollReport='" + payrollreports.ToString() + "',RosterView='" + RosterView.ToString().Trim() + "', " +
                              " Holiday='" + Holiday.Trim() + "',ManualUpload='" + EmpUpload + "',Visitor='" + EmpGrp.Trim() + "' ,  " +
                              " CompAdd='" + AddCmp + "' ,CompModi='" + EditCmp + "' ,CompDel='" + DelCmp + "' ,DeptAdd='" + AddDep + "' ,DeptModi='" + EditDep + "' ,DeptDel='" + DelDep + "' ,CatAdd='" + AddCat + "' ,CatModi='" + EditCat + "' ,CatDel='" + DelCat + "' , " +
                              " SecAdd='" + AddSec + "' ,SecModi='" + EditSec + "' ,SecDel='" + DelSec + "' ,GrdAdd='" + AddGrade + "' ,GrdModi='" + EditGrade + "' ,GrdDel='" + DelGrade + "' ,SftAdd='" + AddShift + "' ,SftModi='" + EditShift + "' ,SftDel='" + DelShift + "' , " +
                              " EmpAdd='" + AddEmp + "' ,EmpModi='" + EditEmp + "' ,EmpDel='" + DelEmp + "' ,EMPGRPADD='" + AddGrp + "' ,EMPGRPMODI='" + EditGrp + "' ,EMPGRPDEL='" + DelGrp + "', DESIGNATION='" + IsDesignation + "',DESIGADD='" + AddDesignation + "',DESIGMODI='" + EditDesignation + "',DESIGDEL='" + DeleteDesignation + "'  " +
                              " where user_r='" + txtUserName.Text.ToString().Trim().Trim() + "' and CompanyCode='" + k.Trim() + "' ";

                    result = Con.execute_NonQuery(Strsql);


                    Response.Redirect("frmUser.aspx");

                }


            }
            catch (Exception Ex)
            {
                Error_Occured("AddNewuser", Ex.Message);
                Response.Redirect("frmUser.aspx");
            }




        }
        else if (Session["IsNewRecord"].ToString() == "N")
        {
            if (GridLookup.Text.ToString().Trim() != null && GridLookup.Text.ToString().Trim() != "")
            {
                Companycode = GridLookup.Text.ToString().Trim();
            }
            else
            {
                Companycode = Label1.Text.ToString();
            }

            //if (Session["DeptCodeSEL"] != null)
            if (GDep.Text.ToString().Trim() != null && GDep.Text.ToString().Trim() != "")
            {
                DeptCode = GDep.Text.ToString().Trim();
            }
            else
            {
                DeptCode = Label2.Text.ToString();
            }

            //  Response.Write(Companycode.ToString());
            //  Response.Write(DeptCode.ToString());

            if ((ddlUserType.SelectedItem.Value == "H") && ((DeptCode.ToString().Trim() == "") || (Companycode.ToString().Trim() == "")))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('For Department Head, Company and Department both selection required');", true);
                return;
            }
            if ((Session["LoginUserName"].ToString().Trim().Trim().ToUpper() == "ADMIN") && (ddlUserType.SelectedItem.Value == "A") && ((DeptCode.ToString().Trim() == "") || (Companycode.ToString().Trim() == "")))
            {
                OleDbDataReader AuthDr;
                OleDbCommand cmd;
                string AuthComp = "", AuthDept = "", pay = "", qry1 = "", qry2 = "";
                qry1 = "Select DepartmentCode from TblDepartment order by DepartmentCode ";
                qry2 = "Select CompanyCode from Tblcompany order by CompanyCode";
                AuthDr = Con.Execute_Reader(qry1);
                while (AuthDr.Read())
                {
                    AuthDept += AuthDr[0].ToString();
                    AuthDept = AuthDept.Trim() + ",";
                }
                if (AuthDept.Trim() != "")
                    AuthDept = AuthDept.Substring(0, AuthDept.Length - 1);
                AuthDr.Close();

                DeptCode = AuthDept.Trim();

                // Finding Auth Company
                AuthDr = Con.Execute_Reader(qry2);

                while (AuthDr.Read())
                {
                    AuthComp += AuthDr[0].ToString();
                    AuthComp = AuthComp.Trim() + ",";
                }
                if (AuthComp.Trim() != "")
                    AuthComp = AuthComp.Substring(0, AuthComp.Length - 1);
                AuthDr.Close();

                Companycode = AuthComp.Trim();
            }
            else if ((Session["LoginUserName"].ToString().Trim().Trim().ToUpper() != "ADMIN") && (ddlUserType.SelectedItem.Value == "A") && ((DeptCode.ToString().Trim() == "") || (Companycode.ToString().Trim() == "")))
            {
                OleDbDataReader AuthDr;
                OleDbCommand cmd;
                string AuthComp = "", AuthDept = "", pay = "", qry1 = "", qry2 = "";
                qry1 = "Select DepartmentCode from TblDepartment where CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' order by DepartmentCode ";
                qry2 = "Select CompanyCode from Tblcompany where CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' order by CompanyCode";
                AuthDr = Con.Execute_Reader(qry1);
                while (AuthDr.Read())
                {
                    AuthDept += AuthDr[0].ToString();
                    AuthDept = AuthDept.Trim() + ",";
                }
                if (AuthDept.Trim() != "")
                    AuthDept = AuthDept.Substring(0, AuthDept.Length - 1);
                AuthDr.Close();

                DeptCode = AuthDept.Trim();

                // Finding Auth Company
                AuthDr = Con.Execute_Reader(qry2);

                while (AuthDr.Read())
                {
                    AuthComp += AuthDr[0].ToString();
                    AuthComp = AuthComp.Trim() + ",";
                }
                if (AuthComp.Trim() != "")
                    AuthComp = AuthComp.Substring(0, AuthComp.Length - 1);
                AuthDr.Close();

                Companycode = AuthComp.Trim();
            }
            // string CCode = "";
            if (Session["LoginUserName"].ToString().Trim().Trim().ToUpper() == "ADMIN")
            {
                Strsql = "Select CompanyCode from tbluser where user_r='" + txtUserName.Text.ToString().Trim() + "'";
                DataSet dsU = new DataSet();
                dsU = Con.FillDataSet(Strsql);
                if (dsU.Tables[0].Rows.Count > 0)
                {
                    CCode = dsU.Tables[0].Rows[0][0].ToString().Trim().Trim();
                }


            }
            else
            {
                CCode = Session["LoginCompany"].ToString().Trim().Trim();
            }
            AddInActive = "N";
        UpdateRec:
            Strsql = "update tbluser set ";
            if (txtPassword.Text.Trim() == string.Empty)
            {
                Strsql += "USERDESCRIPRION='" + txtDescription.Text.Trim() + "',usertype='" + ddlUserType.SelectedItem.Value.ToString().Trim() + "', Main='" + Main.ToString().Trim() + "',V_Transaction='" + Transaction.ToString().Trim() + "', ";
            }
            else
            {
                Strsql += "password='" + txtPassword.Text.ToString().Trim() + "', USERDESCRIPRION='" + txtDescription.Text.Trim() + "',usertype='" + ddlUserType.SelectedItem.Value.ToString().Trim() + "', Main='" + Main.ToString().Trim() + "',V_Transaction='" + Transaction.ToString().Trim() + "', ";
            }

            Strsql += "   DeviceMangement='" + DeviceMange.Trim() + "',BioDeviceManage='" + BioDevice.Trim() + "',HTDeviceManage='" + HTDevice.Trim() + "', DataProcess='" + DataProcess.ToString().Trim() + "',Leave='" + Leaves.ToString().Trim() + "',Reports='" + Reports.ToString().Trim() + "',Company='" + Company.ToString().Trim() + "' , " +
                      " Department='" + Dept.ToString().Trim() + "',Section='" + Section.ToString().Trim() + "',Grade='" + Grade.ToString().Trim() + "',Category='" + Category.ToString().Trim() + "',Shift='" + Shift.ToString().Trim() + "', " +
                      " Employee='" + Employee.ToString().Trim() + "',Manual_Attendance='" + Punch.ToString().Trim() + "',OstoOt='" + OsToOt.ToString().Trim() + "',ShiftChange='" + shiftchange.ToString().Trim() + "', " +
                      " LeaveMaster='" + leave_Master.ToString().Trim() + "',LeaveApplication='" + leave_App.ToString().Trim() + "',LeaveAccural='" + leave_Accural.ToString().Trim() + "',LeaveAccuralAuto='" + autoleave.ToString().Trim() + "', " +
                      " RegisterCreation='" + register_creation.ToString().Trim() + "',RegisterUpdation='" + register_updation.ToString().Trim() + "', " +
                      " BackDateProcess='" + BDP.ToString().Trim() + "',ReProcess='" + Re_Processing.ToString().Trim() + "', " +
                      " TimeOfficeReport='" + TimeOfficeReport.ToString().Trim() + "', " +
                      " Admin='" + Admin.ToString().Trim() + "',TimeOfficeSetup='" + Timeofficesetup.ToString().Trim() + "', " +
                      " UserPrevilege='" + Userprivilge.ToString().Trim() + "',Verification='" + Verification.ToString().Trim() + "', " +
                      " auth_comp='" + Companycode.ToString() + "',Auth_dept='" + DeptCode.ToString() + "', " +
                      " Payroll='" + PayRollManagement.ToString() + "',EmployeeSetup='" + AddInActive.ToString() + "',LoginType='" + ddlLoginType.SelectedItem.Value.ToString() + "', " +
                      " ArearEntry='" + Arrear.ToString() + "',Advance_Loan='" + AdvanceLoan.ToString() + "', " +
                      " Form16='" + Form16.ToString() + "',Form16Return='" + PayrollProcess.ToString() + "',payrollFormula='" + PayrollFormula.ToString() + "', " +
                      " PayrollSetup='" + PayrollSetup.ToString() + "',LoanAdjustment='" + PieceManagement.ToString() + "',PayrollReport='" + payrollreports.ToString() + "',RosterView='" + RosterView.ToString().Trim() + "', " +
                      " Holiday='" + Holiday.Trim() + "',ManualUpload='" + EmpUpload + "',Visitor='" + EmpGrp.Trim() + "' ,  " +
                      " CompAdd='" + AddCmp + "' ,CompModi='" + EditCmp + "' ,CompDel='" + DelCmp + "' ,DeptAdd='" + AddDep + "' ,DeptModi='" + EditDep + "' ,DeptDel='" + DelDep + "' ,CatAdd='" + AddCat + "' ,CatModi='" + EditCat + "' ,CatDel='" + DelCat + "' , " +
                      " SecAdd='" + AddSec + "' ,SecModi='" + EditSec + "' ,SecDel='" + DelSec + "' ,GrdAdd='" + AddGrade + "' ,GrdModi='" + EditGrade + "' ,GrdDel='" + DelGrade + "' ,SftAdd='" + AddShift + "' ,SftModi='" + EditShift + "' ,SftDel='" + DelShift + "' , " +
                      " EmpAdd='" + AddEmp + "' ,EmpModi='" + EditEmp + "' ,EmpDel='" + DelEmp + "' ,EMPGRPADD='" + AddGrp + "' ,EMPGRPMODI='" + EditGrp + "' ,EMPGRPDEL='" + DelGrp + "', DESIGNATION='" + IsDesignation + "',DESIGADD='" + AddDesignation + "',DESIGMODI='" + EditDesignation + "',DESIGDEL='" + DeleteDesignation + "'  " +
                      " where user_r='" + txtUserName.Text.ToString().Trim().Trim() + "' and CompanyCode='" + CCode.Trim() + "' ";

            result = Con.execute_NonQuery(Strsql);
            if (result > 0)
            {
                UpdateHeadID(txtPaycode.Text.ToString().Trim(), DeptCode.Trim());
            }
            DataFilter.LoadCompany(Session["LoginUserName"].ToString().Trim());
            DataFilter.LoadDepartment(Session["LoginUserName"].ToString().Trim(), Session["LoginCompany"].ToString().Trim());
            Response.Redirect("frmUser.aspx");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Error in updating record');", true);
            return;
        }


        ////    if (Session["IsNewRecord"].ToString() == "Y")
        ////    {

        ////        //Strsql = "insert into tbluser(user_r,USERDESCRIPRION,password,OTCAL,UserType,Paycode,trainer,OTApproval,ISOA,CompAdd,Compmodi,compdel, " +
        ////        //    "DeptAdd,deptmodi,deptdel,catadd,catmodi,catdel,secadd,secmodi,secdel,grdadd,grdmodi,grddel,sftadd,sftmodi,sftdel,Empadd,EmpModi,Empdel, " +
        ////        //    "DataMaintenance) values " +
        ////        //    "('" + txtEmpCode.Text.Trim().ToString() + "','" + txtName.Text.ToString().Trim() + "', " +
        ////        //    " '" + txtEmpCode.Text.ToString() + "','N','U','" + txtEmpCode.Text.ToString() + "','N','N','N', " +
        ////        //    " 'Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','N')";
        ////        //result2 = cn.execute_NonQuery(Strsql2);
        ////    }
        ////    else if (Session["IsNewRecord"].ToString() == "N")
        ////    {
        ////        if (GridLookup.Text.ToString().Trim() != null && GridLookup.Text.ToString().Trim()!="")
        ////        {
        ////            Companycode = GridLookup.Text.ToString().Trim();
        ////        }
        ////        else
        ////        {
        ////            Companycode = Label1.Text.ToString();
        ////        }

        ////        //if (Session["DeptCodeSEL"] != null)
        ////        if (GDep.Text.ToString().Trim() != null && GDep.Text.ToString().Trim() != "")
        ////        {
        ////            DeptCode = GDep.Text.ToString().Trim();
        ////        }
        ////        else
        ////        {
        ////            DeptCode = Label2.Text.ToString();
        ////        }

        ////        //  Response.Write(Companycode.ToString());
        ////        //  Response.Write(DeptCode.ToString());

        ////        if ((ddlUserType.SelectedItem.Value == "H") && ((DeptCode.ToString().Trim() == "") || (Companycode.ToString().Trim() == "")))
        ////        {
        ////            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('For Department Head, Company and Department both selection required');", true);
        ////            return;
        ////        }
        ////        if ((Session["LoginUserName"].ToString().Trim().ToUpper() == "ADMIN") && (ddlUserType.SelectedItem.Value == "A") && ((DeptCode.ToString().Trim() == "") || (Companycode.ToString().Trim() == "")))
        ////        {
        ////            OleDbDataReader AuthDr;
        ////            OleDbCommand cmd;
        ////            string AuthComp = "", AuthDept = "", pay = "", qry1 = "", qry2 = "";
        ////            qry1 = "Select DepartmentCode from TblDepartment order by DepartmentCode ";
        ////            qry2 = "Select CompanyCode from Tblcompany order by CompanyCode";
        ////            AuthDr = Con.Execute_Reader(qry1);
        ////            while (AuthDr.Read())
        ////            {
        ////                AuthDept += AuthDr[0].ToString();
        ////                AuthDept = AuthDept.Trim() + ",";
        ////            }
        ////            if (AuthDept.Trim() != "")
        ////                AuthDept = AuthDept.Substring(0, AuthDept.Length - 1);
        ////            AuthDr.Close();

        ////            DeptCode = AuthDept.Trim();

        ////            // Finding Auth Company
        ////            AuthDr = Con.Execute_Reader(qry2);

        ////            while (AuthDr.Read())
        ////            {
        ////                AuthComp += AuthDr[0].ToString();
        ////                AuthComp = AuthComp.Trim() + ",";
        ////            }
        ////            if (AuthComp.Trim() != "")
        ////                AuthComp = AuthComp.Substring(0, AuthComp.Length - 1);
        ////            AuthDr.Close();

        ////            Companycode = AuthComp.Trim();
        ////        }
        ////        else if ((Session["LoginUserName"].ToString().Trim().ToUpper() != "ADMIN") && (ddlUserType.SelectedItem.Value == "A") && ((DeptCode.ToString().Trim() == "") || (Companycode.ToString().Trim() == "")))
        ////        {
        ////            OleDbDataReader AuthDr;
        ////            OleDbCommand cmd;
        ////            string AuthComp = "", AuthDept = "", pay = "", qry1 = "", qry2 = "";
        ////            qry1 = "Select DepartmentCode from TblDepartment where CompanyCode='"+ Session["LoginCompany"].ToString().Trim() +"' order by DepartmentCode ";
        ////            qry2 = "Select CompanyCode from Tblcompany where CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' order by CompanyCode";
        ////            AuthDr = Con.Execute_Reader(qry1);
        ////            while (AuthDr.Read())
        ////            {
        ////                AuthDept += AuthDr[0].ToString();
        ////                AuthDept = AuthDept.Trim() + ",";
        ////            }
        ////            if (AuthDept.Trim() != "")
        ////                AuthDept = AuthDept.Substring(0, AuthDept.Length - 1);
        ////            AuthDr.Close();

        ////            DeptCode = AuthDept.Trim();

        ////            // Finding Auth Company
        ////            AuthDr = Con.Execute_Reader(qry2);

        ////            while (AuthDr.Read())
        ////            {
        ////                AuthComp += AuthDr[0].ToString();
        ////                AuthComp = AuthComp.Trim() + ",";
        ////            }
        ////            if (AuthComp.Trim() != "")
        ////                AuthComp = AuthComp.Substring(0, AuthComp.Length - 1);
        ////            AuthDr.Close();

        ////            Companycode = AuthComp.Trim();
        ////        }
        ////       // string CCode = "";
        ////        if (Session["LoginUserName"].ToString().Trim().ToUpper()=="ADMIN")
        ////        {
        ////            Strsql = "Select CompanyCode from tbluser where user_r='" + txtUserName.Text.ToString().Trim() + "'";
        ////            DataSet dsU = new DataSet();
        ////            dsU = Con.FillDataSet(Strsql);
        ////            if (dsU.Tables[0].Rows.Count>0)
        ////            {
        ////                CCode = dsU.Tables[0].Rows[0][0].ToString().Trim().Trim();
        ////            }


        ////        }
        ////        else
        ////        {
        ////            CCode = Session["LoginCompany"].ToString().Trim();
        ////        }

        ////        Strsql = "update tbluser set ";
        ////        if (txtPassword.Text.Trim() == string.Empty)
        ////        {
        ////            Strsql += "USERDESCRIPRION='" + txtDescription.Text.Trim() + "',usertype='" + ddlUserType.SelectedItem.Value.ToString().Trim() + "', Main='" + Main.ToString().Trim() + "',V_Transaction='" + Transaction.ToString().Trim() + "', ";
        ////        }
        ////        else
        ////        {
        ////            Strsql += "password='" + txtPassword.Text.ToString().Trim() + "', USERDESCRIPRION='" + txtDescription.Text.Trim() + "',usertype='" + ddlUserType.SelectedItem.Value.ToString().Trim() + "', Main='" + Main.ToString().Trim() + "',V_Transaction='" + Transaction.ToString().Trim() + "', ";
        ////        }

        ////        Strsql += " DataProcess='" + DataProcess.ToString().Trim() + "',Leave='" + Leaves.ToString().Trim() + "',Reports='" + Reports.ToString().Trim() + "',Company='" + Company.ToString().Trim() + "' , " +
        ////                  " Department='" + Dept.ToString().Trim() + "',Section='" + Section.ToString().Trim() + "',Grade='" + Grade.ToString().Trim() + "',Category='" + Category.ToString().Trim() + "',Shift='" + Shift.ToString().Trim() + "', " +
        ////                  " Employee='" + Employee.ToString().Trim() + "',Manual_Attendance='" + Punch.ToString().Trim() + "',OstoOt='" + OsToOt.ToString().Trim() + "',ShiftChange='" + shiftchange.ToString().Trim() + "', " +
        ////                  " LeaveMaster='" + leave_Master.ToString().Trim() + "',LeaveApplication='" + leave_App.ToString().Trim() + "',LeaveAccural='" + leave_Accural.ToString().Trim() + "',LeaveAccuralAuto='" + autoleave.ToString().Trim() + "', " +
        ////                  " RegisterCreation='" + register_creation.ToString().Trim() + "',RegisterUpdation='" + register_updation.ToString().Trim() + "', " +
        ////                  " BackDateProcess='" + BDP.ToString().Trim() + "',ReProcess='" + Re_Processing.ToString().Trim() + "', " +
        ////                  " TimeOfficeReport='" + TimeOfficeReport.ToString().Trim() + "', " +
        ////                  " Admin='" + Admin.ToString().Trim() + "',TimeOfficeSetup='" + Timeofficesetup.ToString().Trim() + "', " +
        ////                  " UserPrevilege='" + Userprivilge.ToString().Trim() + "',Verification='" + Verification.ToString().Trim() + "', " +
        ////                  " auth_comp='" + Companycode.ToString() + "',Auth_dept='" + DeptCode.ToString() + "', " +
        ////                  " Payroll='" + PayRollManagement.ToString() + "',EmployeeSetup='" + EmployeeSetup.ToString() + "',LoginType='" + ddlLoginType.SelectedItem.Value.ToString() + "', " +
        ////                  " ArearEntry='" + Arrear.ToString() + "',Advance_Loan='" + AdvanceLoan.ToString() + "', " +
        ////                  " Form16='" + Form16.ToString() + "',Form16Return='" + PayrollProcess.ToString() + "',payrollFormula='" + PayrollFormula.ToString() + "', " +
        ////                  " PayrollSetup='" + PayrollSetup.ToString() + "',LoanAdjustment='" + PieceManagement.ToString() + "',PayrollReport='" + payrollreports.ToString() + "',RosterView='" + RosterView.ToString().Trim() + "', " +
        ////                  " Holiday='" + Holiday.Trim() + "',ManualUpload='" + EmpUpload + "',Visitor='" + EmpGrp.Trim() + "'   where user_r='" + txtUserName.Text.ToString().Trim().Trim() + "' and CompanyCode='" + CCode.Trim() + "' ";

        ////        result = Con.execute_NonQuery(Strsql);
        ////        if (result>0)
        ////        {
        ////            UpdateHeadID(txtPaycode.Text.ToString().Trim(), DeptCode.Trim());
        ////        }
        ////        DataFilter.LoadCompany(Session["LoginCompany"].ToString().Trim());
        ////        DataFilter.LoadDepartment(Session["LoginUserName"].ToString().Trim(), Session["LoginCompany"].ToString().Trim());
        ////        Response.Redirect("frmUser.aspx");
        ////    }
        ////else
        ////{
        ////    ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Error in updating record');", true);
        ////    return;
        ////}
    }

    private void UpdateHeadID(string PayCode ,string DepCode)

    {
        Error_Occured("UpdateHeadID", "HOD:--" + PayCode + "Dep:--" + DepCode);
        try
        {
            Strsql = " update tblemployee set headid='" + PayCode.ToString().Trim() + "' where companycode='" + Session["LoginCompany"].ToString().Trim() + "' " +
                     " and DepartmentCode in ("+DepCode+") ";
            Error_Occured("Query", Strsql);
            Con.execute_NonQuery(Strsql);
        }
        catch (Exception Ex)
        {
            Error_Occured("UpdateHeadID", Ex.Message);
        }
    }
  
    protected void ddlUserType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(ddlUserType.SelectedItem.Value.ToString().Trim()=="H")
        {
            TRSEL.Visible = true;
        }
        else
        {
            TRSEL.Visible = false;
        }

    }
    protected void bindCompany()
    {
        Strsql = "Select companycode,companycode+'-'+companyname as 'compDetails' from tblCompany where CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
        ds = new DataSet();
        ds = Con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            GridLookup.DataSource = ds.Tables[0];
            GridLookup.DataBind();
        }
    }
    protected void bindDept()
    {
        Strsql = "select LTRIM(RTRIM(departmentcode)) departmentcode,departmentcode+' - '+departmentname as 'deptDetails' from tbldepartment where CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
        ds = new DataSet();
        ds = Con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            GDep.DataSource = ds.Tables[0];
            GDep.DataBind();
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {

        bindCompany();
        bindDept();
    }
    
}