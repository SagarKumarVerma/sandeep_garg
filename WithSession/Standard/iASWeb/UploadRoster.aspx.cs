﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using FKWeb;
using Newtonsoft.Json.Linq;
using DevExpress.Web;
using System.Text.RegularExpressions;
using System.IO;
using Newtonsoft.Json;
using BioCloud;
using System.Data.Odbc;
using System.Collections;

public partial class UploadRoster : System.Web.UI.Page
{

    OleDbConnection MyConn = new OleDbConnection();
    OleDbCommand MyCmd = new OleDbCommand();
    Class_Connection cn = new Class_Connection();
    public string KeyValue = "";
    public string TableMain = null;
    public string TableName = null;
    public string ColumnName = null;
    const int GET_DEVICE_STATUS = 1;
    const int DEL_USER_INFO = 3;
    ErrorClass ec = new ErrorClass();
    SqlConnection msqlConn;
    string fileName = "";
    string UName, PhotoPath, Face;
    public ArrayList lstShifts = new ArrayList();

    DataSet Ds;
    string StrSql, Qry, change, Tmp;
    Class1 cls = new Class1();

    OdbcCommand ocmd;
    OdbcDataAdapter oDa;


    string Strsql = null;
    string Msg;
    DataSet ds = null;
    int result = 0;
    OleDbDataReader dr;


    string PayCode = null;
    DateTime dateofjoin;
    string Shift = null;
    string ShiftAttended = null;
    string ALTERNATEOFFDAYS = null;
    string firstoff = null;
    string secondofftype = null;
    string secondoff = null;
    string HALFDAYSHIFT = null;
    string SSN = null;
    DateTime selecteddate;
    DateTime FirstDate;
    DateTime LastDate;
    DateTime date;
    string ShiftType = null;
    string shiftpattern = null;
    string WOInclude = null;
    string[] words;
    int shiftcount = 0;

    string Status = "";
    int AbsentValue = 0;
    int WoVal = 0;
    int PresentValue = 0;
    string p = null;
    int shiftRemainDays = 0;

    string btnGoBack = "";
    string resMsg = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        lblStatus.Text = "";
        if (!IsPostBack)
        {
            lblStatus.Text = "";
            msqlConn = FKWebTools.GetDBPool();


        }
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        //Call The function to write the error log file 

        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }

    public static string[] ExcelSheetNames_First(String excelFile)
    {
        DataTable dt;
        string[] res = new string[0];
        //string connString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelFile + ";Extended Properties=HTML Import;";
        string connString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelFile + ";Extended Properties=Excel 8.0;";
        using (OleDbConnection objConn = new OleDbConnection(connString))
        {
            objConn.Open();
            dt =
            objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            if (dt == null)
            {
                return null;
            }
            res = new string[dt.Rows.Count];
            for (int i = 0; i < res.Length; i++)
            {
                string name = dt.Rows[i]["TABLE_NAME"].ToString();
                if (name[0] == '\'')
                {
                    //numeric sheetnames get single quotes around
                    //remove them here
                    if (Regex.IsMatch(name, @"^'\d\w+\$'$"))
                    {
                        name = name.Substring(1, name.Length - 2);
                    }
                }
                res[i] = name;
            }
            return res;
        }
    }
    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        try
        {
            lblStatus.Text = "";
            string str_FileLocation = "";
            string xConnStr = "";
            str_FileLocation = Server.MapPath("Roster");
            str_FileLocation = str_FileLocation + "\\" + fileName;
            int date = 0;
            date = Convert.ToInt32(System.DateTime.Now.Day);


            DataSet dsResult = new DataSet();
            string Cardno, istimelossallowed, isot, Permissablelate, permisearly, isautoshift, isoutwork, maxdaymin, isos, time, shortday, half, ishalfday, isshort;

            int l = 0;
            DataSet dscon = new DataSet();
            DataSet dsDept = new DataSet();
            Cardno = "";

            string AuthShifts = "";
            string Shift = "";
            string EmployeeGroup = "", EmployeeGroup1 = "";
            string SecondOffDay = "", weekdays = "";
            string ssql = "", Active = "", ComCode = "", Deptcode = "", CatID = "", Email = "", DivCode = "", Grade = "", GradeCode = "", LAStages = "", ShiftType = "", ISRoundClock = "", Inonly = "", IsPunchAll = "", Two = "", FirstOffDay = "", SecondOffType = "", UserType = "";
            string SSN = "";
            DateTime dob = System.DateTime.MinValue, doj = System.DateTime.MinValue;
            DataSet dsRecord = new DataSet();
            string Married = "";
            string Qualification = "";
            string Exp = "";

            string card, paycode, Name, FNAme, Company, Branch, Section, Department, catagory, Designation1, Sex, headid1, Email1;


            string BranchID = "";
            DataSet dspaycode = new DataSet();
            string[] d = new string[3];
            string[] dj = new string[3];
            string[] a = new string[3];

           
            if (File.Exists(str_FileLocation))
            {
                File.Delete(str_FileLocation);
            }
            ASPxUploadControl1.SaveAs(str_FileLocation);
            string[] f = ExcelSheetNames_First(str_FileLocation);
            if (txtDateFrom.Text.ToString().Trim()=="" || txtToDate.Text.ToString().Trim() == "")
            {
                lblStatus.Text = "Please Select Dates";
                return;
            }
            try
            {
                DateTime date1 = Convert.ToDateTime(txtDateFrom.Text.ToString().Trim());
                DateTime date2 = Convert.ToDateTime(txtToDate.Text.ToString().Trim());
                if (date1 > date2)
                {
                    lblStatus.Text = "From Date Is Greater Than To Date";
                    return;
                }


            }
            catch
            {
                lblStatus.Text = "Invalid Date";
                return;
            }



            if (fileName.ToString().Trim() == "")
            {
                lblStatus.Text = "Please Select File";
                return;
            }
            else
            {
                try
                {
                    if (string.IsNullOrEmpty(f[0].ToString().Trim()))
                    {

                        lblStatus.Text = "Excel File Is Not In Valid Format...";
                        return;

                    }
                    string RosterYear = "", RosterMonth = "", month = "", yearcheck = "", startday = "", lastday = ""; ;
                    DateTime DtLastDate;
                    string StrCurDate = "";
                    string StrLastDt = "";

                    month = txtDateFrom.Text.ToString().Substring(3, 2);
                    yearcheck = txtDateFrom.Text.ToString().Substring(6, 4);
                    startday = txtDateFrom.Text.ToString().Substring(0, 2);

                    string processDate = yearcheck + '-' + month + '-' + startday;

                    lastday = txtToDate.Text.ToString().Substring(0, 2);
                    RosterMonth = month.ToString();
                    RosterYear = yearcheck.ToString();
                    int SelMon = Convert.ToInt32(month.ToString());
                    DateTime CheckDate = System.DateTime.Today;



                    int q;
                    int lastdate = Convert.ToInt32(lastday);
                    int startdate = Convert.ToInt32(startday);

                     dscon = new DataSet();
                    DataSet dscon1 = new DataSet();
                    DateTime stdate = System.DateTime.MinValue;

                    dsRecord = new DataSet();

                    StrCurDate = txtDateFrom.Text.ToString();
                   
                    stdate = Convert.ToDateTime(StrCurDate.ToString());
                    DtLastDate = Convert.ToDateTime(txtToDate.Text.ToString());


                    StrSql = "select shift from tblshiftmaster";
                    DataSet dsShift = new DataSet();
                    dsShift = cn.FillDataSet(StrSql);
                    if (dsShift.Tables[0].Rows.Count > 0)
                    {
                        for (int sc = 0; sc < dsShift.Tables[0].Rows.Count; sc++)
                        {
                            lstShifts.Add(dsShift.Tables[0].Rows[sc][0].ToString().Trim().ToUpper());
                        }
                        lstShifts.Add("OFF");
                    }

                    string pay = "";
                    string exDate = "";
                    string p1 = "Select * FROM [" + f[0] + "] ";
                    dspaycode = new DataSet();

                    int exCol = 0;

                    //int exTotalCol = 0;
                    double exTotalCol = 0;
                    DateTime ExStartDate = DateTime.MinValue;
                    DateTime ExLastDate = DateTime.MinValue;
                    DateTime NextDate = DateTime.MinValue;
                    int ChkEmpExist = 0;
                    bool result = false;
                    int result1 = 0;
                    //int dDate = Convert.ToInt32(ddlDate.SelectedItem.Value);
                    int dDate = 0;
                    xConnStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + str_FileLocation + @";Extended Properties=""Excel 8.0;IMEX=1;HDR=YES;TypeGuessRows=0;ImportMixedTypes=Text""";
                    using (OleDbConnection connection = new OleDbConnection(xConnStr))
                    {
                        OleDbCommand command = new OleDbCommand("Select * FROM [" + f[0] + "] ", connection);
                        connection.Open();
                        using (OleDbDataReader dr1 = command.ExecuteReader())
                        {
                            DateTime frmdate = stdate;
                            DateTime todate = DtLastDate;
                            TimeSpan difference = todate - frmdate;
                            double dateno = difference.TotalDays;
                            double totaldays = dateno + 2;
                            int excellast = Convert.ToInt32(totaldays);
                            exTotalCol = totaldays;
                            while (dr1.Read())
                            {

                                pay = dr1[0].ToString();
                                exDate = dr1[2].ToString();


                                exCol = 2;
                                if ((pay.ToString().Trim() != "Employee Code") && (pay.ToString().Trim() != ""))
                                {
                                    ChkEmpExist = ChkEmp(pay);
                                    if (ChkEmpExist == 0)
                                    {
                                        continue;
                                    }
                                }
                                if ((exDate.ToString().Trim() != "") && (result == false))
                                {
                                    try
                                    {
                                        DtLastDate = stdate.AddDays(exTotalCol - 1);
                                        ExStartDate = stdate;
                                        ExLastDate = DtLastDate;
                                        result = true;
                                    }
                                    catch
                                    {
                                        change = "<script language='JavaScript'>alert('Invalid Date Format. Roster uploading aborted.')</script>";
                                        Page.RegisterStartupScript("frmchange", change);
                                        return;
                                    }
                                }
                                string t = "";

                                if ((pay.ToString().Trim() != "Employee Code") && (pay.ToString().Trim() != "") && (result == true))
                                {

                                    while ((stdate < DtLastDate) && (exCol < totaldays))
                                    {
                                        t = dr1[exCol].ToString();
                                        if (!lstShifts.Contains(dr1[exCol].ToString().Trim().ToUpper()))
                                        {
                                            change = "<script language='JavaScript'>alert('" + dr1[exCol].ToString() + " shift not found in TimeWatch System. Roster uploading aborted.')</script>";
                                            Page.RegisterStartupScript("frmchange", change);
                                            return;
                                        }
                                        stdate = stdate.AddDays(1);
                                        exCol = exCol + 1;
                                    }
                                    stdate = ExStartDate;
                                }
                            }
                            dr1.Close();
                        }

                        Shift = "";
                        string StartTime = " ", EndTime = " ";
                        using (OleDbDataReader dr1 = command.ExecuteReader())
                        {

                            exTotalCol = Convert.ToInt32(dr1.FieldCount.ToString()) - 1;
                            while (dr1.Read())
                            {
                                pay = dr1[0].ToString();

                                if ((pay.ToString().Trim() != "Employee Code") && (pay.ToString().Trim() != ""))
                                {

                                    StrSql = "select * from tblrosterCheck where paycode='" + pay.ToString().Trim() + "' and Month='" + RosterMonth.ToString().Trim() + "' and year='" + RosterYear.ToString().Trim() + "'";
                                    Ds = cn.FillDataSet(StrSql);
                                    if (Ds.Tables[0].Rows.Count > 0)
                                    {
                                        change = "<script language='JavaScript'>alert('Roster Already Uploaded For PayCode: " + pay.ToString().Trim() + "')</script>";
                                        Page.RegisterStartupScript("frmchange", change);
                                        return;
                                        //continue;

                                    }
                                }

                                stdate = ExStartDate;
                                DtLastDate = ExLastDate;
                                exCol = 2;
                                if (pay.ToString().Trim() == "")
                                {

                                    change = "<script language='JavaScript'>alert('PayCode Blank')</script>";
                                    Page.RegisterStartupScript("frmchange", change);
                                    return;
                                    //continue;
                                }
                                if ((pay.ToString().Trim() != "Employee Code") && (pay.ToString().Trim() != ""))
                                {
                                    ChkEmpExist = IsValidEmployee(pay);
                                    if (ChkEmpExist == 0)
                                    {

                                        change = "<script language='JavaScript'>alert('Invalid PayCode  : " + pay.ToString().Trim() + "')</script>";
                                        Page.RegisterStartupScript("frmchange", change);
                                        return;
                                        //  continue;
                                    }
                                    ChkEmpExist = ChkEmp(pay);
                                    if (ChkEmpExist == 0)
                                    {

                                        change = "<script language='JavaScript'>alert(' PayCode Not Available  : " + pay.ToString().Trim() + "')</script>";
                                        Page.RegisterStartupScript("frmchange", change);
                                        return;

                                        //  continue;
                                    }
                                    else
                                    {

                                    }
                                }

                                if ((pay.ToString().Trim() != "Employee Code") && (pay.ToString().ToUpper().Trim() != ""))
                                {
                                    while ((stdate < DtLastDate) && (exCol < exTotalCol + 1))
                                    {
                                        Shift = dr1[exCol].ToString().Trim();
                                        if (Shift != "")
                                        {
                                            if (Shift.ToUpper().ToString().Trim() != "OFF")
                                            {
                                                StrSql = "select shift, convert( char(5), starttime,108) StartTime, convert( char(5), EndTime,108)   EndTime  from tblshiftmaster where Shift ='" + Shift + "' ";
                                                Ds = cn.FillDataSet(StrSql);
                                                if (Ds.Tables[0].Rows.Count > 0)
                                                {
                                                    Shift = Ds.Tables[0].Rows[0][0].ToString().Trim();
                                                    StartTime = stdate.ToString("yyyy-MM-dd") + " " + Ds.Tables[0].Rows[0]["StartTime"].ToString().Trim();
                                                    EndTime = stdate.ToString("yyyy-MM-dd") + " " + Ds.Tables[0].Rows[0]["EndTime"].ToString().Trim();
                                                }
                                            }
                                            if (Shift.ToString().Trim().ToUpper() == "OFF")
                                            {
                                                StrSql = "update tbltimeregister set Status='WO', Wo_Value=1,shiftstarttime=null,shiftendtime=null, leavevalue=0, PresentValue=0, AbsentValue=0, Holiday_Value=0, WBR_Flag='1', shift='OFF', shiftattended='OFF',FLAG='1' where dateoffice='" + stdate.ToString("yyyy-MM-dd") + "' AND  paycode ='" + pay.ToString().Trim() + "' ";
                                                cn.execute_NonQuery(StrSql);

                                                if (stdate <= CheckDate)
                                                {
                                                    string sSql = "select CompanyCode,DepartmentCode from tblemployee where paycode='" + pay.ToString().Trim() + "'";
                                                  DataSet  dsEmp = new DataSet();
                                                    dsEmp = cn.FillDataSet(sSql);
                                                    string CCode = dsEmp.Tables[0].Rows[0]["CompanyCode"].ToString().Trim();

                                                    DataSet Rs = new DataSet();
                                                    string SetupID = "11";
                                                    try
                                                    {

                                                        using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString()))
                                                        {
                                                            MyCmd = new OleDbCommand("ProcessBackDate", MyConn);
                                                            MyCmd.CommandTimeout = 1800;
                                                            MyCmd.CommandType = CommandType.StoredProcedure;
                                                            MyCmd.Parameters.AddWithValue("@FromDate", stdate.ToString("yyyy-MM-dd"));
                                                            MyCmd.Parameters.AddWithValue("@ToDate", stdate.ToString("yyyy-MM-dd"));
                                                            MyCmd.Parameters.AddWithValue("@CompanyCode", CCode.ToString().Trim());
                                                            MyCmd.Parameters.AddWithValue("@DepartmentCode", dsEmp.Tables[0].Rows[0]["DepartmentCode"].ToString().Trim());
                                                            MyCmd.Parameters.AddWithValue("@PayCode", pay.ToString().Trim());
                                                            MyCmd.Parameters.AddWithValue("@SetupId", SetupID);
                                                            MyConn.Open();
                                                            MyCmd.ExecuteNonQuery();
                                                            MyConn.Close();
                                                        }
                                                    }
                                                    catch
                                                    {

                                                        continue;
                                                    }

                                                }

                                            }
                                            else
                                            {
                                                StrSql = "update tbltimeregister set WBR_Flag='1',shiftstarttime='" + StartTime + "', shiftendtime='" + EndTime + "',  shift='" + Shift + "', shiftattended='" + Shift + "', FLAG='1', Status='A', Wo_Value=0, leavevalue=0, PresentValue=0, AbsentValue=1, Holiday_Value=0 where dateoffice='" + stdate.ToString("yyyy-MM-dd") + "' AND  paycode ='" + pay.ToString().Trim() + "' ";
                                                cn.execute_NonQuery(StrSql);

                                                if (stdate <= CheckDate)
                                                {

                                                    string sSql = "select DepartmentCode,CompanyCode from tblemployee where paycode='" + pay.ToString().Trim() + "'";
                                                    DataSet  dsEmp = new DataSet();
                                                    dsEmp = cn.FillDataSet(sSql);
                                                    string CCode = dsEmp.Tables[0].Rows[0]["CompanyCode"].ToString().Trim();
                                                    DataSet Rs = new DataSet();
                                                    string SetupID = "11";
                                                    try
                                                    {
                                                        using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString()))
                                                        {
                                                            MyCmd = new OleDbCommand("ProcessBackDate", MyConn);
                                                            MyCmd.CommandTimeout = 1800;
                                                            MyCmd.CommandType = CommandType.StoredProcedure;
                                                            MyCmd.Parameters.AddWithValue("@FromDate", stdate.ToString("yyyy-MM-dd"));
                                                            MyCmd.Parameters.AddWithValue("@ToDate", stdate.ToString("yyyy-MM-dd"));
                                                            MyCmd.Parameters.AddWithValue("@CompanyCode", CCode.ToString().Trim());
                                                            MyCmd.Parameters.AddWithValue("@DepartmentCode", dsEmp.Tables[0].Rows[0]["DepartmentCode"].ToString().Trim());
                                                            MyCmd.Parameters.AddWithValue("@PayCode", pay.ToString().Trim());
                                                            MyCmd.Parameters.AddWithValue("@SetupId", SetupID);
                                                            MyConn.Open();
                                                            MyCmd.ExecuteNonQuery();
                                                            MyConn.Close();

                                                        }
                                                    }
                                                    catch
                                                    {

                                                        continue;
                                                    }

                                                }
                                            }
                                            stdate = stdate.AddDays(1);
                                            exCol = exCol + 1;
                                        }
                                    }

                                    StrSql = "Insert Into tblrosterCheck(Paycode,Month,year) values('" + pay.ToString().Trim() + "','" + RosterMonth.ToString().Trim() + "','" + RosterYear.ToString().Trim() + "')";
                                    cn.execute_NonQuery(StrSql);
                                }
                            }

                            dr1.Close();
                        }
                    }
                    change = "<script language='JavaScript'>alert('Roster Updated Successfully.')</script>";
                    Page.RegisterStartupScript("frmchange", change);
                    return;
                }
                catch (Exception Ex)
                {
                    lblStatus.Text = Ex.Message;
                    Error_Occured("Upload Function", Ex.Message);
                }
            }



        }
        catch (Exception Ex)
        {
            lblStatus.Text = Ex.Message;
            Error_Occured("ASPxButton1_Click", Ex.Message);
        }
    }

    protected int ChkEmp(string code)
    {
        int result = 0;
        StrSql = "select count(paycode) from tblemployee where paycode='" + code.ToString().Trim() + "' and active='Y'";
        result = cn.execute_Scalar(StrSql);
        return result;
    }
    protected int IsValidEmployee(string code)
    {
        int result = 0;
        DataSet dsp = new DataSet();
        StrSql = "";
        StrSql = "select paycode From tblEmployee where paycode='" + code.ToString().Trim() + "' ";
        if ((Session["Auth_Dept"] != null) && (Session["Auth_Comp"] != null))
        {
            StrSql += " and companycode in (" + Session["Auth_Comp"].ToString() + ") and DepartmentCode in (" + Session["Auth_Dept"].ToString() + ") ";
        }
        dsp = cn.FillDataSet(StrSql);
        if (dsp.Tables[0].Rows.Count > 0)
        {
            result = 1;
        }
        else
        {
            result = 0;
        }
        return result;
    }


    public byte[] StreamToByteArray(Stream input)
    {
        byte[] total_stream = new byte[0];
        byte[] stream_array = new byte[0];
        byte[] buffer = new byte[1024];

        int read = 0;
        while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
        {
            stream_array = new byte[total_stream.Length + read];
            total_stream.CopyTo(stream_array, 0);
            Array.Copy(buffer, 0, stream_array, total_stream.Length, read);
            total_stream = stream_array;
        }

        return total_stream;
    }
    protected void ASPxUploadControl1_FilesUploadComplete(object sender, FilesUploadCompleteEventArgs e)
    {
        System.Threading.Thread.Sleep(2000);

        ASPxUploadControl uploadControl = sender as ASPxUploadControl;

        if (uploadControl.UploadedFiles != null && uploadControl.UploadedFiles.Length > 0)
        {
            for (int i = 0; i < uploadControl.UploadedFiles.Length; i++)
            {
                UploadedFile file = uploadControl.UploadedFiles[i];
                if (file.FileName != "")
                {
                    fileName = file.FileName.ToString().Trim();//string.Format("{0}{1}", MapPath("~/UploadTemp/"), file.FileName);

                }
            }
        }

    }
}