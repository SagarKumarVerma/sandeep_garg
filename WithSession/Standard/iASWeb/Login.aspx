﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>


<%--<asp:Content ID="MainContent" ContentPlaceHolderID="MainContent" runat="server">--%>
      <script type = "text/javascript">
   function closeWindow()
   {
        window.open('','_self','');
        window.close();
   }    
</script>
<style>
div.relative {
  position: relative;
  width: 400px;
  height: 200px;
 
} 

div.absolute {
  position: absolute;
  top: 166px;
  right: 724px;
  width: 200px;
  height: 100px;

}
    .auto-style1 {
        width: 100%;
    }
    .auto-style2 {
        width: 50%;
    }
     .auto-style3 {
    }
     </style>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>iAS Web Login</title>
     <link rel="shortcut icon" href="Content/Images/favicon.ico" />
</head>
<body>
    <form id="form1" runat="server">
    

            
        <table class="auto-style1" style="background-image: url('Content/Images/logo.png'); background-repeat: no-repeat;">
            <tr>
                <td class="auto-style2">
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <div class="footer">
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td style="vertical-align: middle" align="center">
                    <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" HeaderText="Login" Theme="SoftOrange" Width="60%">
                        <HeaderStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Size="12pt" HorizontalAlign="Left" />
                        <PanelCollection>
<dx:PanelContent runat="server">
    <table class="dxflInternalEditorTable_SoftOrange">
              <tr>
            <td class="auto-style3">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style3" align="center">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Company Code *   ">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxTextBox ID="txtCCode" runat="server" Width="150px">
                    <%--<ValidationSettings Display="Dynamic" ErrorDisplayMode="Text" ErrorTextPosition="Bottom" ValidationGroup="LoginUserValidationGroup">
                        <RequiredField ErrorText="User Name is required." IsRequired="True" />
                    </ValidationSettings>--%>
                </dx:ASPxTextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCCode" ErrorMessage="*" ForeColor="Red" ValidationGroup="1"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style3" align="center">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="User Name *   ">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxTextBox ID="tbUserName" runat="server" Width="150px">
                    <%--<ValidationSettings Display="Dynamic" ErrorDisplayMode="Text" ErrorTextPosition="Bottom" ValidationGroup="LoginUserValidationGroup">
                        <RequiredField ErrorText="User Name is required." IsRequired="True" />
                    </ValidationSettings>--%>
                </dx:ASPxTextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="tbUserName" ErrorMessage="*" ForeColor="Red" ValidationGroup="1"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style3"  align="center">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Password *   ">
                </dx:ASPxLabel>
            </td>
            <td>
                <dx:ASPxTextBox ID="tbPassword" runat="server" Password="True" Width="150px">
                   <%-- <ValidationSettings Display="Dynamic" ErrorDisplayMode="Text" ErrorTextPosition="Bottom" ValidationGroup="LoginUserValidationGroup">
                        <RequiredField ErrorText="Password is required." IsRequired="True" />
                    </ValidationSettings>--%>
                </dx:ASPxTextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="tbPassword" ErrorMessage="*" ForeColor="Red" ValidationGroup="1"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td>
                <dx:ASPxButton ID="btnLogin" runat="server" OnClick="btnLogin_Click" Text="Log In" ValidationGroup="1">
                </dx:ASPxButton>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style3">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style3" colspan="3" align="center">
                <asp:Label ID="Label3" runat="server" ForeColor="Red" Font-Size="Small"></asp:Label>
            </td>
        </tr>
    </table>
                            </dx:PanelContent>
</PanelCollection>
                    </dx:ASPxRoundPanel>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            </div>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    

            
      <div class="accountHeader">

      </div>
      <br />
        
            </form>
</body>
</html>
<%-- 
</asp:Content>--%>
