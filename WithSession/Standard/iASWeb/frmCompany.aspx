﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="frmCompany.aspx.cs" Inherits="frmCompany" %>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">  
        function State_OnKeyUp(s, e) {  
            s.SetText(s.GetText().toUpperCase().trim());
        }  
    </script>  
    <%-- DXCOMMENT: Configure ASPxGridView control --%>
    <dx:ASPxGridView ID="GrdComp" runat="server" AutoGenerateColumns="False" 
    Width="100%" KeyFieldName="COMPANYCODE" OnCellEditorInitialize="GrdComp_CellEditorInitialize" OnRowValidating="GrdComp_RowValidating" OnRowUpdating="GrdComp_RowUpdating" OnRowInserting="GrdComp_RowInserting" OnRowDeleting="GrdComp_RowDeleting" OnPageIndexChanged="GrdComp_PageIndexChanged" OnCommandButtonInitialize="GrdComp_CommandButtonInitialize" OnInitNewRow="GrdComp_InitNewRow" OnRowDeleted="GrdComp_RowDeleted" OnStartRowEditing="GrdComp_StartRowEditing">
        <%-- DXCOMMENT: Configure ASPxGridView's columns in accordance with datasource fields --%>
        <ClientSideEvents EndCallback="function(s, e) {
	 if (s.cp_isSuccess) 
{ alert('Can Not Delete Record As It Is Already Assigned To Employee'); 
delete s.isSuccess; } 
}" />
        
        
        <SettingsAdaptivity AdaptivityMode="HideDataCells" />
        
         <SettingsPager>
                <PageSizeItemSettings Visible="true" Items="10, 20, 50,100" />
            </SettingsPager>
        <SettingsEditing Mode="PopupEditForm">
        </SettingsEditing>
        <Settings ShowFilterRow="True" ShowTitlePanel="True" />
        <SettingsBehavior ConfirmDelete="True" />
        <SettingsCommandButton>
            <ClearFilterButton Text=" ">
                <Image IconID="filter_clearfilter_16x16">
                </Image>
            </ClearFilterButton>
            <NewButton Text=" ">
                <Image IconID="actions_add_16x16">
                </Image>
            </NewButton>
            <UpdateButton ButtonType="Image" RenderMode="Image">
                <Image IconID="save_save_16x16office2013">
                </Image>
            </UpdateButton>
            <CancelButton ButtonType="Image" RenderMode="Image">
                <Image IconID="actions_cancel_16x16office2013">
                </Image>
            </CancelButton>
            <EditButton Text=" ">
                <Image IconID="actions_edit_16x16devav">
                </Image>
            </EditButton>
            <DeleteButton Text=" ">
                <Image IconID="actions_trash_16x16">
                </Image>
            </DeleteButton>
        </SettingsCommandButton>
         <SettingsPopup>
            <EditForm Modal="True" Width="800px" HorizontalAlign="WindowCenter" 
                PopupAnimationType="Slide" VerticalAlign="WindowCenter" />
        </SettingsPopup>
          <SettingsSearchPanel Visible="True" />
        <SettingsText PopupEditFormCaption="Company" Title="Company" />
        <Columns>
            <dx:GridViewCommandColumn ShowClearFilterButton="True" ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="COMPANYCODE" VisibleIndex="1" Caption="Code" >
                <PropertiesTextEdit MaxLength="5">
                   
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="COMPANYNAME" VisibleIndex="2" Caption="Company Name">
                 <PropertiesTextEdit MaxLength="50">
                    
                  </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="COMPANYADDRESS" VisibleIndex="3" Caption="Address">
                 <PropertiesTextEdit MaxLength="50">
                   
                  </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="SHORTNAME" VisibleIndex="4" Caption="Login Code">
                 <PropertiesTextEdit MaxLength="4">
                     <ClientSideEvents keyup="State_OnKeyUp" />
                  </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="PANNUM" VisibleIndex="5" Caption="GST No">
                 <PropertiesTextEdit MaxLength="16">
                     <ClientSideEvents keyup="State_OnKeyUp" />
                  </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
        </Columns>
        <Paddings Padding="0px" />
        <Border BorderWidth="0px" />
        <BorderBottom BorderWidth="1px" />
    </dx:ASPxGridView>


<%-- DXCOMMENT: Configure your datasource for ASPxGridView --%>



</asp:Content>
