﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="frmManual.aspx.cs" Inherits="frmManual" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <table align="center" style="width:100%" cellpadding="0" cellspacing="0" >
<tr>
<td style="text-align:center">Manual Punch</td>
</tr>
<tr>
<td align="center" colspan="6" class="tableHeaderCss" style="height:25px;">
    &nbsp;</td>
</tr>
     
<tr>
    <td align="center">
        <dx:ASPxFormLayout ID="formLayout" runat="server" AlignItemCaptionsInAllGroups="True" UseDefaultPaddings="False">
            <SettingsAdaptivity AdaptivityMode="SingleColumnWindowLimit" SwitchToSingleColumnAtWindowInnerWidth="200" />
            <Items>

                <dx:EmptyLayoutItem />
                <dx:LayoutGroup Caption="Employee & Date Selection" ColCount="3" Width="100%">
                    <Items>
                        <dx:LayoutItem Caption="PayCode">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                    <dx:ASPxComboBox ID="ddlPayCode" runat="server" ValueType="System.String" Width="200px" AutoPostBack="True" OnSelectedIndexChanged="ddlPayCode_SelectedIndexChanged" SelectedIndex="-1" NullText="NONE">
                                        <ClearButton DisplayMode="Always">
                                        </ClearButton>
                                    </dx:ASPxComboBox>
                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>

                        <dx:LayoutItem Caption="Employee Name">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                    <dx:ASPxLabel ID="lblName" runat="server" Text="Employee Name"></dx:ASPxLabel>
                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>
                        <dx:LayoutItem Caption="Department">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                                    <dx:ASPxLabel ID="lblLocation" runat="server" Text="Department"></dx:ASPxLabel>
                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>
                        <dx:LayoutItem Caption="Punch Date">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server">
                                    <dx:ASPxDateEdit runat="server" ID="TxtFromDate" Width="200px" AutoPostBack="True" EditFormatString="dd/MM/yyyy" OnDateChanged="TxtFromDate_DateChanged" OnCalendarDayCellPrepared="TxtFromDate_CalendarDayCellPrepared"></dx:ASPxDateEdit>

                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>
                        <dx:LayoutItem Caption="Punch Time">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server">
                                    <dx:ASPxTextBox ID="TxtPunchTime" runat="server" Width="50px">
                                        <MaskSettings Mask="HH:mm" />
                                    </dx:ASPxTextBox>

                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>
                        <dx:LayoutItem Caption="Is RTC">
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server">
                                    <dx:ASPxLabel ID="lblRTC" runat="server" Text="NA"></dx:ASPxLabel>

                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>
                    </Items>

                </dx:LayoutGroup>
                <dx:LayoutItem ShowCaption="False" CaptionSettings-HorizontalAlign="Right" Width="100%" HorizontalAlign="Right">
                    <LayoutItemNestedControlCollection>
                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True">
                            <dx:ASPxButton ID="btnPost" runat="server" Text="Post Punch" Width="100px" OnClick="btnPost_Click" />
                        </dx:LayoutItemNestedControlContainer>
                    </LayoutItemNestedControlCollection>

                    <CaptionSettings HorizontalAlign="Right"></CaptionSettings>
                </dx:LayoutItem>
               
            </Items>
        </dx:ASPxFormLayout>
             
               
                            <table style="width: 100%">
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="width: 68%; vertical-align: top;">
             
               
                            <dx:ASPxGridView ID="grdAtt" runat="server" Width="100%" Theme="SoftOrange" AutoGenerateColumns="False" OnPageIndexChanged="grdAtt_PageIndexChanged">
                                <Settings HorizontalScrollBarMode="Hidden" />
                                <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                                <Columns>
                                    <dx:GridViewDataTextColumn Caption="Late" VisibleIndex="8" FieldName="LateArrival">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Hours Worked" VisibleIndex="7" FieldName="Hours">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Status" VisibleIndex="6" FieldName="Status">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Out Time" VisibleIndex="5" FieldName="Out Time">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Out Date" VisibleIndex="4" FieldName="Out Date">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="In Time" VisibleIndex="3" FieldName="In Time">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="In Date" VisibleIndex="2" FieldName="In Date">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Shift" VisibleIndex="1" FieldName="ShiftAttended">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Date" VisibleIndex="0" FieldName="DateOffice">
                                    </dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:ASPxGridView>
                                    </td>
                                    <td style="width: 1%"/>
                                    <td style="vertical-align: top">
                         <dx:ASPxGridView ID="grdPunch" runat="server" AutoGenerateColumns="False" KeyFieldName="PDate" Width="100%" Theme="SoftOrange" OnRowDeleting="grdPunch_RowDeleting" OnPageIndexChanged="grdPunch_PageIndexChanged">
                                <Settings HorizontalScrollBarMode="Auto" />
                                <SettingsResizing ColumnResizeMode="Control" />
                                <SettingsCommandButton>
                                    <DeleteButton Text=" ">
                                        <Image IconID="actions_trash_16x16">
                                        </Image>
                                    </DeleteButton>
                                </SettingsCommandButton>
                                <SettingsDataSecurity AllowEdit="False" AllowInsert="False" />
                                <SettingsText ConfirmDelete="Yes" />
                                <Columns>
                                    <dx:GridViewCommandColumn ShowDeleteButton="True" ShowInCustomizationForm="True" VisibleIndex="0" Width="15%" Caption=" ">
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn FieldName="IsManual" ShowInCustomizationForm="True" VisibleIndex="5" Caption="Is Manual" Width="20%">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="punch" ShowInCustomizationForm="True" VisibleIndex="3" Caption="Time" Width="20%">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn FieldName="P_Day" ShowInCustomizationForm="True" VisibleIndex="4" Caption="Is Prv Day" Width="20%">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataDateColumn Caption="Date" FieldName="OfficePunch" VisibleIndex="2">
                                        <PropertiesDateEdit DisplayFormatInEditMode="True" DisplayFormatString="yyyy-MM-dd" EditFormat="Custom" EditFormatString="yyyy-MM-dd">
                                        </PropertiesDateEdit>
                                        <Settings AllowEllipsisInText="True" />
                                    </dx:GridViewDataDateColumn>
                                </Columns>
                            </dx:ASPxGridView>
                            
                
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
        </table>
                            
                
    </td>
    </tr>
        </table>
</asp:Content>

