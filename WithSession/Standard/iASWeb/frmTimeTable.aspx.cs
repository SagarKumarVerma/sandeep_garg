﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Configuration;
using System.Globalization;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using GlobalSettings;    

public partial class frmTimeTable : System.Web.UI.Page
{
    Class_Connection cn = new Class_Connection();
    //public string txtFromDateCID = "";//txtfromdate.Text.ToString();
    //public string txtToDateCID = "";// txttodate.Text.ToString();
    OleDbConnection Connection = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"]);

    public OleDbDataAdapter da;
    public OleDbDataReader dr;
    public OleDbDataReader drFlag;
    public DataSet ds;
    string strSql = "", strSql1, strFlag;
    string shift, team, change;
    string ReportView = "";
    string fd, td, pd;
    int StartTime;
    DateTime date1, date2;
    DateTime pdt;
    int i;
    int day;
    string Flag, Wbr, Paycode;
    DataSet ds1;

    static ArrayList ShiftArray = new ArrayList();


    ErrorClass ec = new ErrorClass();

    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    protected void Page_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();
        if (ex is HttpRequestValidationException)
        {
            string resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" +
                   "Form doesn't need to contain valid HTML, just anything with opening and closing angled brackets (<...>).<br />" +
                   btnGoBack +
                   "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
    }

    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        //Call The function to write the error log file 
        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        ec.Write_Log(PageName, FunctionName, ErrorMsg);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            if (Session["ShiftChangeVisible"] == null)
            {
               // Response.Redirect("PageNotFound.aspx");
            }
            if (!IsPostBack)
            {
                if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
                {
                    Request.Browser.Adapters.Clear();
                }
                //bindFields();
               // LblDept.Text = Global.getEmpInfo._Dept;
                Session["LastQuery"] = "";

                LblFrm.Text = "Roster Change";

                Response.Write("<script language='JavaScript'> function loadframe(){ }</script>");

                DateTime dt = System.DateTime.Now;
                int dn = Convert.ToInt32(dt.Day);
                SetDate();
                Fill_Shift();
                Fill_Dept();

                strSql = "";


                fd = txtfromdate.Text.ToString().Substring(6, 4) + "-" + txtfromdate.Text.ToString().Substring(3, 2) + "-" + txtfromdate.Text.ToString().Substring(0, 2);
                td = txttodate.Text.ToString().Substring(6, 4) + "-" + txttodate.Text.ToString().Substring(3, 2) + "-" + txttodate.Text.ToString().Substring(0, 2);

                if (Session["usertype"].ToString() == "T")  // If Team Leader 
                {
                    strSql = "select e.paycode,e.empname,  convert(char (10), t.dateoffice,103) as Dateoffice,convert(char (5), t.SHIFTSTARTTIME,108) as starttime,convert(char (5), t.SHIFTENDTIME ,108) as endtime,t.leavevalue ,isnull(t.flag,0) flag, t.shiftattended , t.Holiday_Value,t.Dateoffice ";
                    strSql += " from tblemployee e , tbltimeregister t where  active='Y' ";
                    strSql += " and e.Paycode in ( select Paycode from TblEmployee where teamId=  (select TeamId from tblEmployee where paycode= '" + Session["PAYCODE"].ToString().Trim() + "' )) ";
                    strSql += " and DateOffice between '" + fd.ToString() + " ' and '" + td.ToString() + "' and  e.paycode=t.paycode order by e.empname,Dateoffice";
                }

                else if (Session["usertype"].ToString() == "H")  //for HOD or any other User 
                {
                    ReportView = ConfigurationSettings.AppSettings["ReportView_HeadID"].ToString();
                    strSql = " select e.paycode,e.empname,  convert(char (10), t.dateoffice,103) as Dateoffice,convert(char (5), t.SHIFTSTARTTIME,108) as starttime,convert(char (5), t.SHIFTENDTIME ,108) as endtime,t.leavevalue , isnull(t.flag,0) flag , t.shiftattended , t.Holiday_Value,t.Dateoffice from ";
                    strSql += " tblemployee e , tbltimeregister t where   DateOffice between '" + fd.ToString() + " ' and '" + td.ToString() + "' and  e.paycode=t.paycode ";
                    strSql += " and e.Active='Y' ";
                    //And departmentcode ='" + DeptCombo.SelectedValue.ToString() + "' And  e.headid='" + Session["PAYCODE"].ToString().Trim() + "'   order by e.empname,dateoffice";
                    {
                        if (ReportView.ToString().Trim() == "Y")
                        {
                            strSql += " and e.headid = '" + Session["PAYCODE"].ToString().ToString().Trim() + "' ";
                        }
                        else
                        {
                            if (Session["Auth_Comp"] != null)
                            {
                                strSql += " and e.companycode in (" + DataFilter.AuthComp.Trim() + ") ";
                            }
                            if (Session["Auth_Dept"] != null)
                            {
                                strSql += " and e.Departmentcode in (" + DataFilter.AuthDept.Trim() + ") ";
                            }
                        }
                    }
                    strSql += " order by e.empname,dateoffice ";

                }
                else if (Session["usertype"].ToString() == "A")  //for Admin, HOD or any other User 
                {
                    strSql = " select e.paycode,e.empname,  convert(char (10), t.dateoffice,103) as Dateoffice,convert(char (5), t.SHIFTSTARTTIME,108) as starttime,convert(char (5), t.SHIFTENDTIME ,108) as endtime,t.leavevalue , isnull(t.flag,0) flag , t.shiftattended , t.Holiday_Value,t.Dateoffice from ";
                    strSql += " tblemployee e , tbltimeregister t where   DateOffice between '" + fd.ToString() + " ' and '" + td.ToString() + "' and  e.paycode=t.paycode ";
                    strSql += " and e.Active='Y' ";
                    if (DeptCombo.SelectedItem.Text.ToString().Trim() != "All")
                    {
                        strSql += " and e.departmentcode ='" + DeptCombo.SelectedItem.Value.ToString() + "' ";
                    }
                    if (Session["Auth_Comp"] != null)
                    {
                        strSql += " and e.companycode in (" + DataFilter.AuthComp.Trim() + ") ";
                    }
                    strSql += "order by e.empname,t.dateoffice";
                }
                else
               
                Session["LastQuery"] = strSql;
                fill_Table();
            }
        }

        catch (Exception er)
        {
            Error_Occured("Page_Load", er.Message);
        }
    }
    private void SetDate()
    {
        try
        {
        string d1, d2 = "";

        if (!IsPostBack)
        {
            OleDbDataReader dr;
            strSql = "SELECT convert(char(10), GETDATE(),120) ";
            dr = cn.ExecuteReader(strSql);
            dr.Read();
            d1 = dr[0].ToString();
            dr.Close();

        }
        else
        {
            d1 = txtfromdate.Text;
            d2 = txttodate.Text;
        }

        date1 = Convert.ToDateTime(d1);  //DateTime.ParseExact(d1.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
        txtfromdate.Text = date1.AddDays(1).ToString("dd/MM/yyyy");
        if (!IsPostBack)
            txttodate.Text = date1.AddDays(6).ToString("dd/MM/yyyy");
        else
        {
            date2 = Convert.ToDateTime(d2); //DateTime.ParseExact(d2.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            txttodate.Text = date2.AddDays(6).ToString("dd/MM/yyyy");
        }

        }
        catch (Exception er)
        {
            Error_Occured("SetDate", er.Message);
        }

    }
    protected void bindFields()
    {
        strSql = "select * from tblMapping";
        ds1 = new DataSet();
        ds1 = cn.FillDataSet(strSql);
        if (ds1.Tables[0].Rows.Count > 0)
        {
            LblDept.Text = ds1.Tables[0].Rows[7]["Showas"].ToString().Trim();
        }
    }
    private void Fill_Dept()
    {
        try
        {
        if (Session["usertype"].ToString().Trim() == "T")
        {
            LblDept.Visible = false;
            DeptCombo.Visible = false;
        }
        else if (Session["usertype"].ToString().Trim() == "H")
        {
            LblDept.Visible = true;
          //  LblDept.Text = Global.getEmpInfo._Dept;
            DeptCombo.Visible = true;
            strSql = " select d.departmentcode 'Code',d.departmentcode+'-'+d.departmentname 'Name' from tbldepartment d  join tblemployee e on e.departmentcode=d.departmentcode and e.paycode='" + Session["paycode"].ToString() + "' ";
            DataSet dsDept = new DataSet();
            dsDept = cn.FillDataSet(strSql);
            DeptCombo.DataSource = dsDept.Tables[0];
            DeptCombo.TextField = "Name";
            DeptCombo.ValueField = "Code";
            DeptCombo.DataBind();
            ViewState["dpt"] = DeptCombo.SelectedItem.Value.ToString();
        }
        else if (Session["usertype"].ToString().Trim() == "A")
        {
            LblDept.Visible = true;
           // LblDept.Text = Global.getEmpInfo._Dept;
            DeptCombo.Visible = true;
            strSql = " select d.departmentcode 'Code',d.departmentcode+'-'+d.departmentname 'Name' from tbldepartment d  ";
            if ((Session["Auth_Dept"] != null) && (Session["PAYCODE"].ToString().Trim().ToUpper() != "ADMIN"))
            {
                strSql += " where d.Departmentcode in (" + Session["Auth_Dept"].ToString().Trim() + ") ";
            }
            strSql += "order by Code";

            DataSet dsDept = new DataSet();
            dsDept = cn.FillDataSet(strSql);

            DeptCombo.DataSource = dsDept.Tables[0];
            DeptCombo.TextField = "Name";
            DeptCombo.ValueField = "Code";
            DeptCombo.DataBind();
            DeptCombo.SelectedIndex = -1;
            if (Session["PAYCODE"].ToString().Trim().ToUpper() == "ADMIN")
            {
                
            }
            ViewState["dpt"] = DeptCombo.SelectedItem.Value.ToString();
        }
        }

        catch (Exception er)
        {
            Error_Occured("Fill_Dept", er.Message);
        }
    }

    private void Fill_Shift()
    {
        try
        {

        
        string time;
        strSql = "select convert(char(5),starttime, 108), convert(char(5),endtime, 108), TblShiftMaster.Shift from  TblShiftMaster Where CompanyCode='"+Session["LoginCompany"].ToString().Trim().Trim()+"' ";
        strSql += " order by Shift";
        dr = cn.Execute_Reader(strSql);
        i = 0;
        ShiftCombo.Items.Clear();
        while (dr.Read())
        {
            time = dr[0].ToString() + " to " + dr[1].ToString() + "  " + dr[2].ToString();
            ShiftCombo.Items.Add(time);
            ShiftArray.Add(dr[2].ToString());
        }

        ShiftCombo.Items.Add("OFF");
        ShiftArray.Add("OFF");
        ShiftCombo.SelectedIndex = -1;
        dr.Close();
        }

        catch (Exception er)
        {
            Error_Occured("Fill_Shift", er.Message);
        }
    }
    private bool FindDays()  //Finding Roster Date For Single employee  
    {

        //try
        //{
        fd = txtfromdate.Text.ToString().Substring(6, 4) + "-" + txtfromdate.Text.ToString().Substring(3, 2) + "-" + txtfromdate.Text.ToString().Substring(0, 2);
        td = txttodate.Text.ToString().Substring(6, 4) + "-" + txttodate.Text.ToString().Substring(3, 2) + "-" + txttodate.Text.ToString().Substring(0, 2);

        strSql = "select datediff(dd, getdate(), '" + fd.ToString() + "') ";
        dr = cn.Execute_Reader(strSql);
        dr.Read();
        day = Convert.ToInt32(dr[0].ToString());
        dr.Close();

        if (day < 0)  //Trying to Change for Back Date
        {
            change = "<script language='JavaScript'>alert('You can not Update Roster for Back Date...')</script>";
            Page.RegisterStartupScript("frmchange", change);
            return true; // Roster can not be updated 
        }
        if (day == 0)  //Trying to Change for the Same Date 
        {
            // //Find Paycode for Employee for which the roster is to be changed
            Paycode = "";
            //foreach (DataGridItem dataGridItem in DataGrid1.Items)
            //{
            //    if (check)
            //    {
            //        Paycode = dataGridItem.Cells[1].Text;
            //    }
            //}


            //  // Check for Time Limit for 10 Hrs earlier for the new alloted shift 

            strSql = "select isnull(convert(char (2), t.SHIFTSTARTTIME,108),0) as starttime , convert(char (2), t.SHIFTENDTIME ,108) as endtime ,";
            strSql += " isnull(FLAG,0) FLAG, WBR_Flag from tblTimeRegister t where paycode ='" + Paycode.ToString() + "' and Dateoffice ='" + fd.ToString() + "'  ";
            dr = cn.Execute_Reader(strSql);
            dr.Read();
            StartTime = Convert.ToInt32(dr["starttime"].ToString());
            Flag = dr["flag"].ToString();

            dr.Close();

            if (Flag.Trim() == "1")  // it means going to update 2nd time 
            {
                strSql = "select isnull(convert(char (2), t.STARTTIME,108),0) as starttime  from tblshiftmaster t where shift='" + shift.ToString() + "'  ";
                dr = cn.Execute_Reader(strSql);
                dr.Read();
                int NewShift;
                NewShift = Convert.ToInt32(dr[0].ToString());
                dr.Close();
                // **
                int Hrs = 0;
                while (StartTime <= 24)
                {
                    if (StartTime == NewShift)
                    {
                        break;
                    }
                    if (StartTime == 24)
                    {
                        StartTime = 0;
                    }
                    StartTime++;
                    Hrs++;
                }
                // **
                if (Hrs < 10)
                {
                    change = "<script language='JavaScript'>alert('Should be at least 10 Hours gap between shifts...')</script>";
                    Page.RegisterStartupScript("frmchange", change);
                    return true; // Roster can not be updated 
                }
            }

        } //end of  if 

        return false;

        //}

        //catch (Exception er)
        //{
        //    Error_Occured("FindDays", er.Message);
        //    return false;
        //}
    }

    static int f = 1;
    private bool FindDate()
    {
        fd = txtfromdate.Text.ToString().Substring(6, 4) + "-" + txtfromdate.Text.ToString().Substring(3, 2) + "-" + txtfromdate.Text.ToString().Substring(0, 2);
        td = txttodate.Text.ToString().Substring(6, 4) + "-" + txttodate.Text.ToString().Substring(3, 2) + "-" + txttodate.Text.ToString().Substring(0, 2);

        if (day <= 0)
        {
            change = "<script language='JavaScript'>alert('You can not Update Roster for Back Date or for same date...')</script>";
            Page.RegisterStartupScript("frmchange", change);
            return true;
        }
        strSql = "select getdate() as Dt, DATEPART(HH,GETDATE() ) AS HOURS";
        dr = cn.Execute_Reader(strSql);
        dr.Read();
        DateTime dt = Convert.ToDateTime(dr[0].ToString());

        int hour = Convert.ToInt32(dr[1].ToString());
        dr.Close();
        return false;
        
    }
    public void fill_Table()
    {
        try
        {
        DataGrid1.Visible = true;
        DataGrid1.DataSource = null;
        DataGrid1.DataBind();
        if (Session["LastQuery"].ToString().Trim().Length > 0)  //Roster changed, and now use the old query 
            strSql = Session["LastQuery"].ToString();

        OleDbCommand comm = new OleDbCommand(strSql, Connection);
        OleDbDataAdapter adpater = new OleDbDataAdapter(strSql, Connection);
        DataSet ds = new DataSet();

        adpater.Fill(ds, "report");



            System.Data.DataTable dt = new DataTable();
            dt = ds.Tables.Add("shiftdate");
            strSql = "select * from tblMapping";
        ds1 = new DataSet();
        ds1 = cn.FillDataSet(strSql);
        string strpaycode = ds1.Tables[0].Rows[1]["Showas"].ToString().Trim();
        string strempname = ds1.Tables[0].Rows[2]["Showas"].ToString().Trim();

        dt.Columns.Add(strpaycode);
        dt.Columns.Add(strempname);




        DateTime date1 = new DateTime(int.Parse(fd.ToString().Substring(0, 4)), int.Parse(fd.ToString().Substring(5, 2)), int.Parse(fd.ToString().Substring(8, 2)));
        DateTime date2 = new DateTime(int.Parse(td.ToString().Substring(0, 4)), int.Parse(td.ToString().Substring(5, 2)), int.Parse(td.ToString().Substring(8, 2)));

            if (date2 < date1)
            {
                change = "<script language='JavaScript'>alert('From date should not be greater than To date...')</script>";
                Response.Write(change);
            }
        while (date1 <= date2)
        {

            string strdatecolumns = date1.ToString("dd/MM");  //ds.Tables["emp"].Rows[i][2].ToString();
            if (strdatecolumns == "")
                dt.Columns.Add("NULL");
            else
                dt.Columns.Add(strdatecolumns);
            date1 = date1.AddDays(1);
        }


        DataRow dr;
        i = 0;

        int j = 0;
        string strdate;

        while (j < ds.Tables["report"].Rows.Count)
        {
            dr = dt.NewRow();
            strpaycode = ds.Tables["report"].Rows[j][0].ToString();

            strempname = ds.Tables["report"].Rows[j][1].ToString();
            dr[0] = strpaycode;
            dr[1] = strempname;
            i = 2;
            while (strpaycode == ds.Tables["report"].Rows[j][0].ToString())
            {
                strdate = ds.Tables["report"].Rows[j][3].ToString() + " - " + ds.Tables["report"].Rows[j][4].ToString();
                //strdate = ds.Tables["report"].Rows[j][4].ToString();
                string s = ds.Tables["report"].Rows[j]["flag"].ToString();

                if (ds.Tables["report"].Rows[j]["flag"].ToString().Trim() == "0")
                {

                    if (ds.Tables["report"].Rows[j]["LeaveValue"].ToString().Trim() == "0")
                        dr[i] = "";
                    else if (ds.Tables["report"].Rows[j]["LeaveValue"].ToString().Trim() == "1")
                        dr[i] = "L";
                    if (ds.Tables["report"].Rows[j]["Holiday_value"].ToString().Trim() == "1")
                        dr[i] = "Hld";
                }
                else
                {
                    string SS = ds.Tables["report"].Rows[j]["shiftattended"].ToString().Trim();
                    if (ds.Tables["report"].Rows[j]["shiftattended"].ToString().Trim() == "OFF")
                        dr[i] = "OFF";
                    else if (Convert.ToDouble(ds.Tables["report"].Rows[j]["LeaveValue"].ToString().Trim()) > 0)
                        dr[i] = "L";
                    else
                        dr[i] = strdate.ToString();

                    if (ds.Tables["report"].Rows[j]["Holiday_value"].ToString().Trim() == "1")
                        dr[i] = "Hld";

                }


                i++;
                j++;
                if (j == ds.Tables["report"].Rows.Count)
                {
                    break;
                }
            }

            dt.Rows.Add(dr);
        }
        DataGrid1.DataSource = ds.Tables["shiftdate"];
        DataGrid1.DataBind();
        f = 1;
    }
        catch (Exception er)
        {
            Error_Occured("Fill_table", er.Message);
        }

    }

    protected void DeptCombo_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtfromdate.Text.ToString().Trim() == string.Empty || txttodate.Text.ToString().Trim() == string.Empty)
        {
            //change = "<script language='JavaScript'>alert('Please Select Date! ')</script>";
            //Page.RegisterStartupScript("frmchange", change);
            //return;
        }
        try
        {
        ViewState["dpt"] = DeptCombo.SelectedItem.Value.ToString();

        //Fill_Team();

        //ViewState["teamid"] = TeamCombo.SelectedValue.ToString();

        fd = txtfromdate.Text.ToString().Substring(6, 4) + "-" + txtfromdate.Text.ToString().Substring(3, 2) + "-" + txtfromdate.Text.ToString().Substring(0, 2);
        td = txttodate.Text.ToString().Substring(6, 4) + "-" + txttodate.Text.ToString().Substring(3, 2) + "-" + txttodate.Text.ToString().Substring(0, 2);
        strSql = " select  e.paycode ,e.empname ,  convert(char (10), t.dateoffice,103) as Dateoffice,convert(char (5), t.SHIFTSTARTTIME,108) as starttime,convert(char (5), t.SHIFTENDTIME ,108) as endtime,t.leavevalue ,isnull(t.flag,0) Flag, t.shiftattended , t.Holiday_Value,t.Dateoffice from ";
        strSql += " tblemployee e , tbltimeregister t where  DateOffice between '" + fd.ToString() + " ' and '" + td.ToString() + "' and  e.paycode=t.paycode ";

        strSql += " And e.Active='Y' ";
        if (DeptCombo.SelectedItem.Text.ToString().Trim() != "All")
        {
            //strSql += "and e.divisioncode= '" + DeptCombo.SelectedValue.ToString() + "' ";
            strSql += "and e.departmentcode= '" + DeptCombo.SelectedItem.Value.ToString() + "' ";
           
        }
        if (Session["Auth_Comp"] != null)
        {
            strSql += " and e.companycode in (" + DataFilter.AuthComp.Trim() + ") ";
        }

        strSql += "order by e.empname,t.dateoffice";
        Session["LastQuery"] = strSql; //***
        Error_Occured("sSql", strSql);
        fill_Table();

        }

        catch (Exception er)
        {
            Error_Occured("DeptCombo_SelectedIndexChanged", er.Message);
        }
    }
    protected void BtnShowRoster_Click(object sender, EventArgs e)
    {
        try
        {
            DataGrid1.DataSource = null;
            DataGrid1.DataBind();
            if (DeptCombo.SelectedIndex==-1)
            {
                string  Msg1 = "<script language='javascript'> alert('Select Department')</script>";
                Page.RegisterStartupScript("msg", Msg1);
                DeptCombo.Focus();
                return;
            }

            DateTime dateFrom;
            DateTime dateTo;
            try
            {
                dateFrom = Convert.ToDateTime(txtfromdate.Text.ToString()); // DateTime.ParseExact(txtfromdate.Value.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                dateTo = Convert.ToDateTime(txttodate.Text.ToString()); //DateTime.ParseExact(txttodate.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                
            }
            fd = txtfromdate.Text.ToString().Substring(6, 4) + "-" + txtfromdate.Text.ToString().Substring(3, 2) + "-" + txtfromdate.Text.ToString().Substring(0, 2);
            td = txttodate.Text.ToString().Substring(6, 4) + "-" + txttodate.Text.ToString().Substring(3, 2) + "-" + txttodate.Text.ToString().Substring(0, 2);

            if (Session["usertype"].ToString() == "T")  // If Team Leader 
            {
                strSql = "select e.paycode,e.empname,  convert(char (10), t.dateoffice,103) as Dateoffice,convert(char (5), t.SHIFTSTARTTIME,108) as starttime,convert(char (5), t.SHIFTENDTIME ,108) as endtime,t.leavevalue ,isnull(t.flag,0) flag, t.shiftattended , t.Holiday_Value,t.Dateoffice ";
                strSql += " from tblemployee e , tbltimeregister t where  active='Y' ";
                strSql += " and e.Paycode in ( select Paycode from TblEmployee where teamId=  (select TeamId from tblEmployee where paycode= '" + Session["PAYCODE"].ToString().Trim() + "' )) ";
                strSql += " and DateOffice between '" + fd.ToString() + " ' and '" + td.ToString() + "' and  e.paycode=t.paycode order by e.empname,dateoffice";
            }

            else if (Session["usertype"].ToString() == "H")  //for HOD or any other User 
            {
                ReportView = ConfigurationSettings.AppSettings["ReportView_HeadID"].ToString();
                strSql = " select e.paycode,e.empname,  convert(char (10), t.dateoffice,103) as Dateoffice,convert(char (5), t.SHIFTSTARTTIME,108) as starttime,convert(char (5), t.SHIFTENDTIME ,108) as endtime,t.leavevalue , isnull(t.flag,0) flag , t.shiftattended , t.Holiday_Value,t.Dateoffice from ";
                strSql += " tblemployee e , tbltimeregister t where   DateOffice between '" + fd.ToString() + " ' and '" + td.ToString() + "' and  e.paycode=t.paycode ";
                strSql += " and e.Active='Y' ";
                //And departmentcode ='" + DeptCombo.SelectedValue.ToString() + "' And  e.headid='" + Session["PAYCODE"].ToString().Trim() + "'   order by e.empname,dateoffice";
                {
                    if (ReportView.ToString().Trim() == "Y")
                    {
                        strSql += " and e.headid = '" + Session["PAYCODE"].ToString().ToString().Trim() + "' ";
                    }
                    else
                    {
                        if (Session["Auth_Comp"] != null)
                        {
                            strSql += " and e.companycode in (" + DataFilter.AuthComp + ") ";
                        }
                        if (Session["Auth_Dept"] != null)
                        {
                            strSql += " and e.Departmentcode in (" + DataFilter.AuthDept + ") ";
                        }
                    }
                }
                strSql += " order by e.empname,t.dateoffice ";
            }
            else if (Session["usertype"].ToString() == "A")  //for Admin, HOD or any other User 
            {

                strSql = " select e.paycode,e.empname,  convert(char (10), t.dateoffice,103) as Dateoffice,convert(char (5), t.SHIFTSTARTTIME,108) as starttime,convert(char (5), t.SHIFTENDTIME ,108) as endtime,t.leavevalue , isnull(t.flag,0) flag , t.shiftattended , t.Holiday_Value,t.Dateoffice from ";
                strSql += " tblemployee e , tbltimeregister t where   DateOffice between '" + fd.ToString() + " ' and '" + td.ToString() + "' and  e.paycode=t.paycode ";
                //strSql += " and e.Active='Y' and e.teamid ='" + team.ToString() + "' and departmentcode ='" + DeptCombo.SelectedValue.ToString() + "'    order by e.paycode";
                strSql += " and e.Active='Y' ";
                if (DeptCombo.SelectedItem.Text.ToString().Trim() != "All")
                {
                    strSql += " and e.departmentcode ='" + DeptCombo.SelectedItem.Value.ToString() + "' ";
                }
                if (Session["Auth_Comp"] != null)
                {
                    strSql += " and e.companycode in (" + DataFilter.AuthComp + ") ";
                }
                strSql += "order by e.empname,t.dateoffice";
            }
            else
                return;

            Session["LastQuery"] = strSql;
            fill_Table();
        }
        catch (Exception er)
        {
            Error_Occured("BtnShowRoster_Click", er.Message);
        }
    }
    protected void Btn_ChangeShift_Click(object sender, EventArgs e)
    {
        try
        {
            string shiftstrtime;
            string shiftendtime;
            string Msg = "";
            
            DateTime dateFrom;
            DateTime dateTo;
            try
            {
                dateFrom = Convert.ToDateTime(txtfromdate.Text.ToString()); // DateTime.ParseExact(txtfromdate.Value.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                dateTo = Convert.ToDateTime(txttodate.Text.ToString()); //DateTime.ParseExact(txttodate.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                //Msg = "<script lanaguage='javascript'>alert('Invalid date. ')</script>";
                //Page.RegisterStartupScript("msg", Msg);
                //return;
            }

            if (DeptCombo.SelectedIndex == -1 || ShiftCombo.SelectedIndex == -1)
            {
                string Msg1 = "<script language='javascript'> alert('Select Department/ Shft')</script>";
                Page.RegisterStartupScript("msg", Msg1);
                DeptCombo.Focus();
                return;
            }
            
            int i = 0;
            i = ShiftCombo.SelectedIndex;
           
            shift = ShiftCombo.SelectedItem.ToString().Substring(ShiftCombo.SelectedItem.ToString().Trim().Length - 3, 3);
            fd = txtfromdate.Text.ToString().Substring(6, 4) + "-" + txtfromdate.Text.ToString().Substring(3, 2) + "-" + txtfromdate.Text.ToString().Substring(0, 2);
            td = txttodate.Text.ToString().Substring(6, 4) + "-" + txttodate.Text.ToString().Substring(3, 2) + "-" + txttodate.Text.ToString().Substring(0, 2);

            string tid;

            if (ViewState["TID"] != null)
                tid = ViewState["TID"].ToString();
            else
                tid = "";

            if (Connection.State == 0)
                Connection.Open();

            string paycode;
            OleDbCommand cmd;
            string strpaycode;
            string sSql = "";
            sSql = "Select paycode from  tblemployee where departmentcode='"+DeptCombo.SelectedItem.Value.ToString().Trim()+"' and companycode='"+Session["LoginCompany"].ToString().Trim().Trim()+"'";
            DataSet DsEmp = new DataSet();
            DsEmp = cn.FillDataSet(sSql);


            if (DsEmp.Tables[0].Rows.Count > 0)
            {
                for (int Rec = 0; Rec < DsEmp.Tables[0].Rows.Count; Rec++)
                {


                    string ii = DDOff1.SelectedItem.Value.ToString();

                    strpaycode = DsEmp.Tables[0].Rows[Rec]["paycode"].ToString().Trim();

                    paycode = (strpaycode);

                    strSql = "Select paycode, convert(char (10), dateoffice,103) as dateoffice, isnull(flag,0) Flag from TblTimeRegister where Paycode ='" + paycode.ToString() + "' ";
                    strSql += " and LeaveValue =0 ";
                    strSql += " and DateOffice between '" + fd.ToString() + " ' and '" + td.ToString() + "' ";

                    OleDbDataReader dr;
                    cmd = new OleDbCommand(strSql, Connection);
                    dr = cmd.ExecuteReader();
                    cmd.Dispose();

                    while (dr.Read())
                    {

                        pd = dr["dateoffice"].ToString().Substring(6, 4) + "-" + dr["dateoffice"].ToString().Substring(3, 2) + "-" + dr["dateoffice"].ToString().Substring(0, 2);
                        shift = ShiftCombo.SelectedItem.ToString().Substring(ShiftCombo.SelectedItem.ToString().Trim().Length - 3, 3);
                        int off1, off2, dow;

                        DateTime mydt = Convert.ToDateTime(pd.ToString());
                        dow = Convert.ToInt32(mydt.DayOfWeek);

                        off1 = Convert.ToInt32(DDOff1.SelectedItem.Value.ToString());
                        off2 = Convert.ToInt32(DDOff2.SelectedItem.Value.ToString());

                        if (dow == off1 || dow == off2)
                        {
                            shift = "OFF";
                        }

                        {
                            shiftstrtime = " " + ShiftCombo.SelectedItem.Text.Substring(0, 5).ToString();
                            shiftendtime = " " + ShiftCombo.SelectedItem.Text.Substring(9, 5).ToString();

                            strSql = "Update TblTimeRegister set Shift= '" + shift + "' , flag='1' ";
                            strSql += ", ShiftAttended='" + shift + "',SHIFTSTARTTIME=DATEOFFICE + '" + shiftstrtime + "' ,SHIFTENDTIME=DATEOFFICE+ '" + shiftendtime + "'";
                            strSql += " ,WBR_Flag='1' ";

                            strSql += " Where  TbltimeRegister.Paycode ='" + paycode.ToString() + "' ";
                            strSql += " and DateOffice = '" + pd.ToString() + "' ";
                        }


                        cmd = new OleDbCommand(strSql, Connection);
                        cmd.Connection = Connection;
                        cmd.ExecuteNonQuery();
                    }
                    dr.Close();
                    cmd.Dispose();
                }
            }
           
            Connection.Close();
            BtnShowRoster_Click(sender, e);
           
        }
        catch (Exception er)
        {
            change = "<script language='JavaScript'>alert('" + er.Message.ToString() + "'.)</script>";
            Page.RegisterStartupScript("frmchange", change);
            return;
        }
    }
}