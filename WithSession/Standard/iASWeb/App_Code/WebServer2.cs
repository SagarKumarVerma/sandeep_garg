using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Configuration;


/// <summary>
/// Summary description for WebServer2
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class WebServer2 : System.Web.Services.WebService 
{
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;

    public WebServer2 () 
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
    [WebMethod]
    public string HelloWorld() 
    {
        return "Hello World";
    }
    [WebMethod]
    public DataSet getDepartment()
    {
        using (con = new SqlConnection(ConfigurationManager.ConnectionStrings["Con1"].ToString()))
        {
            da = new SqlDataAdapter("select * from tbldepartment", con);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;        
        }
    }
}

