﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using GlobalSettings;

public partial class frmLeavelevel : System.Web.UI.Page
{
    public string txtFromDateCID = "";
    public string txtToDateCID = "";


    public OleDbCommand cmd;
    public OleDbDataAdapter da;
    public OleDbDataReader dr;
    public DataSet ds;
    string strSql = "";
    string strlevelcode;
    string lblcode = "";
    int QryResult = 0;
    Class_Connection cn = new Class_Connection();
    ErrorClass ec = new ErrorClass();

    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    protected void Page_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();
        if (ex is HttpRequestValidationException)
        {
            string resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" +
                   "Form doesn't need to contain valid HTML, just anything with opening and closing angled brackets (<...>).<br />" +
                   btnGoBack +
                   "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
    }

    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        //Call The function to write the error log file 

        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        ec.Write_Log(PageName, FunctionName, ErrorMsg);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }

            if (!IsPostBack)
            {
                if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
                {
                    Request.Browser.Adapters.Clear();
                }
                Session["Updation"] = "FrmLeaveApprovalStages";
                LblFrmName.Text = "Level Updation Form ";
                LblSel.Text = "Stages";
                TxtHeadId1.Visible = false;
                RadPaycode1.Visible = false;
                RadName1.Visible = false;
                Fill_Dept();
                fill_grid();
                bindPaycode();
                bindheadid();

                if (Session["Updation"].ToString() == "FrmLevelChange")
                    fill_combo();
                else
                {
                    combo_Level.Items.Add("1");
                    combo_Level.Items.Add("2");
                }
                combo_Level_SelectedIndexChanged(sender, e);
            }

            if (RadEmpLevel.Checked)
            {
                Session["Updation"] = "FrmLevelChange";
                LblFrmName.Text = "Level Updation Form ";
            }

            if (RadLvApproval.Checked)
            {
                Session["Updation"] = "FrmLeaveApprovalStages";
                LblFrmName.Text = "Leave Approval Stages Updation Form ";
            }

            if (RdHeadId.Checked)
            {
                Session["Updation"] = "FrmHeadIdUpdate";
                LblFrmName.Text = "HeadId Updation Form ";
            }
        }
        catch (Exception er)
        {
            Error_Occured("Page_Load", er.Message);
        }
    }

    protected void bindheadid()
    {
        strSql = "select e.paycode +' - '+ e.empname 'EmpName1',e.paycode 'Pay' from tblemployee e join tbluser u on e.paycode=u.user_r where e.active='Y' and u.usertype='H' and e.companycode='" + Session["LoginCompany"].ToString().Trim().Trim() + "' order by e.paycode ";
        ds = new DataSet();
        ds = cn.FillDataSet(strSql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            TxtHeadId1.DataSource = ds.Tables[0];
            TxtHeadId1.DataTextField = "EmpName1";
            TxtHeadId1.DataValueField = "Pay";
            TxtHeadId1.DataBind();
            TxtHeadId1.Items.Insert(0, "-Select-");
        }
        else
        {
            TxtHeadId1.Items.Insert(0, "-Select-");
        }
    }

    protected void bindPaycode()
    {
        strSql = "select paycode +' - '+ empname'EmpName1',paycode,Empname from tblemployee where active='Y' and companycode='" + Session["LoginCompany"].ToString().Trim().Trim() + "' order by paycode ";
        ds = new DataSet();
        ds = cn.FillDataSet(strSql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlPaycode.DataSource = ds.Tables[0];
            ddlPaycode.DataTextField = "EmpName1";
            ddlPaycode.DataValueField = "paycode";
            ddlPaycode.DataBind();
            ddlPaycode.Items.Insert(0, "-Select-");
        }
        else
        {
            ddlPaycode.Items.Insert(0, "-Select-");
        }

    }

    private void Fill_Dept()
    {

        strSql = " Select DepartmentName,DepartmentCode from tblDepartment where companycode='"+Session["LoginCompany"].ToString().Trim().Trim()+"' Order by DepartmentName ";
        DataSet dsDept = new DataSet();
        dsDept = cn.FillDataSet(strSql);
        DdlDept.Items.Clear();
        DdlDept.DataSource = dsDept.Tables[0];
        DdlDept.DataTextField = "Departmentname";
        DdlDept.DataValueField = "DepartmentCode";
        DdlDept.DataBind();
        
    }

    public void fill_combo()
    {
        try
        {

        strSql = " select * from tbllevel order by levelcode ";
        ds = new DataSet();
        ds = cn.FillDataSet(strSql);
        combo_Level.DataSource = ds;
        combo_Level.DataTextField = "levelname";
        combo_Level.DataValueField = "levelcode";
        combo_Level.DataBind();

        }
        catch (Exception er)
        {
            Error_Occured("fill_combo", er.Message);
        }

    }


    public void fill_grid()
    {
        //try
        //{


        if (Session["Updation"].ToString() == "FrmLevelChange")
            strSql = "select Paycode,Empname,DepartmentName, Levelcode As 'Level' from tblemployee e, tbldepartment d ";

        if (Session["Updation"].ToString() == "FrmLeaveApprovalStages")
            strSql = "select Paycode,Empname, DepartmentName, LeaveApprovalStages As 'Approval Level' from tblemployee e, tbldepartment d";

        if (Session["Updation"].ToString() == "FrmHeadIdUpdate")
            strSql = "select e.Paycode,e.Empname,DepartmentName, e.HeadId + ' - '+ ltrim(h.empname) as HOD from tbldepartment d , tblemployee e left outer join tblemployee h on  e.headid = h.paycode  ";

        //strSql = "select e.paycode,e.empname,DepartmentName, e.HeadId from tblemployee e, tbldepartment d ";

        strSql += " WHERE e.Active='Y'  And  e.departmentcode= d.departmentcode ";

        if (DdlDept.SelectedIndex == 0)
            strSql += " And e.departmentcode IN (" + Session["Auth_Dept"].ToString() + ") ";
        else
            strSql += " And e.departmentcode ='" + DdlDept.SelectedValue.ToString() + "' ";

        if (Session["IsOA"].ToString() == "Y" && Session["ActualUserType"].ToString() == "T")
        {
            strSql += " UNION ";
            //*************

            if (Session["Updation"].ToString() == "FrmLevelChange")
                strSql += " select Paycode,Empname,DepartmentName,Levelcode As 'Level' from tblemployee e, tbldepartment d ";

            if (Session["Updation"].ToString() == "FrmLeaveApprovalStages")
                strSql += " select Paycode,Empname,DepartmentName, LeaveApprovalStages As 'Approval Level' from tblemployee e, tbldepartment d ";

            if (Session["Updation"].ToString() == "FrmHeadIdUpdate")
                strSql += " select Paycode,Empname, DepartmentName,HeadId from tblemployee e, tbldepartment d ";

            //*************
            strSql += " where e.departmentcode = ( select Departmentcode from tblemployee where paycode='" + Session["paycode"].ToString() + "' ) ";
            strSql += " And Teamid in (Select Teamid from tblemployee where paycode='" + Session["paycode"].ToString() + "' ) ";
        }

        strSql += " Order by DepartmentName, e.paycode ";
        Session["lastQry"] = strSql.ToString();
        ds = new DataSet();
        ds = cn.FillDataSet(strSql);
        //if (ds.Tables[0].Rows.Count > 0)
        //{
        DataGrid1.DataSource = ds;
        DataGrid1.DataBind();
        //}         
        //}
        //catch (Exception er)
        //{
        //    Error_Occured("fill_grid", er.Message);
        //}

    }



    protected void btn_Update_Click(object sender, EventArgs e)
    {
        string change;
        try
        {
            int found;
            lblcode = "";
            if (!RdHeadId.Checked)
            {
                if (ViewState["lblcode"].ToString() != null)
                    lblcode = combo_Level.SelectedValue.ToString();
                //ViewState["lblcode"] = combo_Level.SelectedValue.ToString();
            }
            else
            {
                if (TxtHeadId1.SelectedItem.Text == "-Select-")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Select HeadID from Combobox ');", true);
                    TxtHeadId1.Focus();
                    return;
                }
                lblcode = TxtHeadId1.SelectedItem.Value.ToString();
            }
            bool EmpSelected = false;
            foreach (DataGridItem dataGridItem in DataGrid1.Items)
            {

                bool check = ((CheckBox)dataGridItem.FindControl("chk_EmpCode")).Checked;
                if (check)
                {
                    EmpSelected = true;
                    break;
                }
            }
            if (!EmpSelected)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Select Employee for updation');", true);
                return;
            }


            //ViewState["lblcode"] = TxtHeadId.Text;

            strlevelcode = string.Empty;
            foreach (DataGridItem dataGridItem in DataGrid1.Items)
            {
                found = 1;
                bool check = ((CheckBox)dataGridItem.FindControl("chk_EmpCode")).Checked;
                if (check)
                {
                    strlevelcode = dataGridItem.Cells[1].Text;

                    if (Session["Updation"].ToString() == "FrmLevelChange")
                        strSql = "update tblemployee set levelcode='" + lblcode.ToString() + "' where paycode='" + strlevelcode.ToString() + "'";


                    if (Session["Updation"].ToString() == "FrmLeaveApprovalStages")
                        strSql = "update tblemployee set LeaveApprovalStages ='" + lblcode.ToString() + "' where paycode='" + strlevelcode.ToString() + "'";


                    if (Session["Updation"].ToString() == "FrmHeadIdUpdate")
                    {
                        found = FindHeadId();
                        if (found == 1)
                            strSql = "update tblemployee set Headid ='" + lblcode.ToString().ToUpper() + "' where paycode='" + strlevelcode.ToString() + "'";
                        else
                        {
                            change = "<script language='JavaScript'>alert('Invalid HeadId  !!!')</script>";
                            Page.RegisterStartupScript("frmchange", change);
                        }
                    }

                    if (found == 1)
                    {
                        QryResult = cn.execute_NonQuery(strSql);
                        //Search();

                    }

                }
                //else
                //{
                //    change = "<script language='JavaScript'>alert('Please Select Employee for Level Updation!!!')</script>";
                //    Page.RegisterStartupScript("frmchange", change);

                //}
            }
            fill_grid();
            // Search();
            //if (QryResult > 0)
            //{
            //    strSql = Session["lastQry"].ToString();
            //    ds = new DataSet();
            //    ds = cn.FillDataSet(strSql);
            //    DataGrid1.DataSource = ds;
            //    DataGrid1.DataBind();
            //}

        }
        catch (Exception er)
        {
            Error_Occured("btn_Update_Click", er.Message);
        }

    }




    protected void combo_Level_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["lblcode"] = combo_Level.SelectedValue.ToString();

    }

    protected void DataGrid1_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
    {

        DataGrid1.CurrentPageIndex = e.NewPageIndex;
        fill_grid();
    }


    public void Search()
    {
        //try
        //{
        if (Session["Updation"].ToString() == "FrmLevelChange")
            strSql = "select Paycode,Empname,Levelcode as 'Level' from tblemployee ";

        if (Session["Updation"].ToString() == "FrmLeaveApprovalStages")
            strSql = "select Paycode,Empname,LeaveApprovalStages As 'Approval Level'   from tblemployee ";

        if (Session["Updation"].ToString() == "FrmHeadIdUpdate")
            //strSql = "select paycode,empname, HeadId from tblemployee ";

            strSql = "select Tblemployee.Paycode,Tblemployee.Empname,Tblemployee.HeadId + ' - '+ ltrim(h.empname) as HOD from  " +
                    "tblemployee Tblemployee left outer join tblemployee h on  Tblemployee.headid = h.paycode  ";

        if (RadPaycode.Checked == true)
        {
            strSql += " WHERE Tblemployee.Active='Y' And Tblemployee.paycode ='" + ddlPaycode.SelectedItem.Value.ToString() + "' ";
            //strSql += " WHERE Tblemployee.Active='Y' And paycode ='" + TxtSearch.Text + "' ";
            strSql += " And Tblemployee.Departmentcode IN (" + Session["Auth_Dept"].ToString() + ") "; //order by paycode ";
        }

        if (RadName.Checked == true)
        {
            strSql += " WHERE Tblemployee.Active='Y' And tblemployee.paycode ='" + ddlPaycode.SelectedItem.Value.ToString() + "' ";
            //strSql += " WHERE Tblemployee.Active='Y' And Empname like '%" + TxtSearch.Text + "%' ";
            strSql += " And tblemployee.Departmentcode IN (" + Session["Auth_Dept"].ToString() + ") ";  //order by paycode ";
        }

        //*************

        if (Session["IsOA"].ToString() == "Y" && Session["ActualUserType"].ToString() == "T")
        {
            strSql += " UNION ";
            if (Session["Updation"].ToString() == "FrmLevelChange")
                strSql += " Select Paycode,Empname,Levelcode as 'Level' from tblemployee ";

            if (Session["Updation"].ToString() == "FrmLeaveApprovalStages")
                strSql += " select Paycode,Empname, LeaveApprovalStages As 'Approval Level' from tblemployee";

            if (Session["Updation"].ToString() == "FrmHeadIdUpdate")
                strSql += " select Paycode,Empname, HeadId from tblemployee";

            strSql += " where tblemployee.departmentcode = ( select Departmentcode from tblemployee where paycode='" + Session["paycode"].ToString() + "' ) ";
            strSql += " And tblemployee.Teamid in (Select Teamid from tblemployee where tblemployee.paycode='" + Session["paycode"].ToString() + "' ) ";

            if (RadPaycode.Checked == true)
                strSql += " and tblemployee.Active='Y' And tblemployee.paycode ='" + ddlPaycode.SelectedItem.Value.ToString() + "' ";
            //strSql += "  and active='Y' and paycode ='" + TxtSearch.Text + "'  ";
            else
                strSql += " and tblemployee.Active='Y' And tblemployee.paycode ='" + ddlPaycode.SelectedItem.Value.ToString() + "' ";
            //strSql += "  and active='Y' and Empname like '%" + TxtSearch.Text + "%' ";

        }
        //*************
        //Response.Write(strSql.ToString());
        //return;
        ds = new DataSet();
        ds = cn.FillDataSet(strSql);

        if (ds.Tables[0].Rows.Count <= 0)
        {
            string change;
            change = "<script language='JavaScript'>alert('Paycode/Name Not Found')</script>";
            Page.RegisterStartupScript("frmchange", change);
            DataGrid1.DataSource = null;
            DataGrid1.DataBind();
        }
        else
        {
            DataGrid1.DataSource = ds;
            DataGrid1.DataBind();
        }
        //}
        //catch (Exception er)
        //{
        //    Error_Occured("Search", er.Message);
        //}

    }


    protected void Button1_Click1(object sender, EventArgs e)
    {
        if (ddlPaycode.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Select Employee/Paycode from Combobox ');", true);
            ddlPaycode.Focus();
            return;
        }
        Search();
    }

    protected void RadPaycode_CheckedChanged(object sender, EventArgs e)
    {
        if (RadName.Checked)
        {
            strSql = "select empname +' - '+ paycode 'EmpName1',paycode,Empname from tblemployee where active='Y' and companycode='"+Session["LoginCompany"].ToString().Trim().Trim()+"' order by empname ";
        }
        else
        {
            strSql = "select paycode +' - '+ empname 'EmpName1',paycode,Empname from tblemployee where active='Y' and companycode='" + Session["LoginCompany"].ToString().Trim().Trim() + "' order by paycode ";
        }
        ds = new DataSet();
        ds = cn.FillDataSet(strSql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlPaycode.DataSource = ds.Tables[0];
            ddlPaycode.DataTextField = "EmpName1";
            ddlPaycode.DataValueField = "paycode";
            ddlPaycode.DataBind();
            ddlPaycode.Items.Insert(0, "-Select-");
        }
    }
    protected void RadPaycode1_CheckedChanged(object sender, EventArgs e)
    {
        if (RadName1.Checked)
        {
            strSql = "select e.empname +' - '+ e.paycode'EmpName1',e.paycode,e.Empname from tblemployee e join tbluser u on e.paycode=u.user_r where e.active='Y' and u.usertype='H' and e.companycode='" + Session["LoginCompany"].ToString().Trim().Trim() + "' order by e.paycode ";
            //   strSql = "select empname +' - '+ paycode 'EmpName1',paycode,Empname from tblemployee where active='Y' order by empname ";
        }
        else
        {

            strSql = "select e.paycode +' - '+ e.empname'EmpName1',e.paycode,e.Empname from tblemployee e join tbluser u on e.paycode=u.user_r where e.active='Y' and u.usertype='H' and e.companycode='" + Session["LoginCompany"].ToString().Trim().Trim() + "' order by e.paycode ";
            //strSql = "select paycode +' - '+ empname 'EmpName1',paycode,Empname from tblemployee where active='Y' order by paycode ";
        }
        DataSet ds1 = new DataSet();
        ds1 = cn.FillDataSet(strSql);
        if (ds1.Tables[0].Rows.Count > 0)
        {
            TxtHeadId1.DataSource = ds1.Tables[0];
            TxtHeadId1.DataTextField = "EmpName1";
            TxtHeadId1.DataValueField = "paycode";
            TxtHeadId1.DataBind();
            TxtHeadId1.Items.Insert(0, "-Select-");
        }

    }

    protected void RadName_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void chk_SelectAll_CheckedChanged1(object sender, EventArgs e)
    {
        try
        {

        for (int i = 0; i < DataGrid1.Items.Count; i++)
        {
            CheckBox cb = (CheckBox)DataGrid1.Items[i].FindControl("chk_EmpCode");
            //cb.Checked = true;
            cb.Checked = ((CheckBox)sender).Checked;
        }

        }
        catch (Exception er)
        {
            Error_Occured("chk_SelectAll_CheckedChanged1", er.Message);
        }
    }
    protected void RadLvApproval_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            combo_Level_SelectedIndexChanged(sender, e);
            LblSel.Text = "Stages";
            TxtHeadId1.Visible = false;
            combo_Level.Visible = true;
            RadPaycode1.Visible = false;
            RadName1.Visible = false;
            Session["Updation"] = "FrmLeaveApprovalStages";
            fill_grid();
            bindPaycode();
            combo_Level.Items.Clear();
            combo_Level.Items.Add("1");
            combo_Level.Items.Add("2");
        }
        catch (Exception er)
        {
            Error_Occured("RadLvApproval_CheckedChanged", er.Message);
        }
    }
    protected void RadEmpLevel_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            combo_Level_SelectedIndexChanged(sender, e);
            LblSel.Text = "Level ";
            TxtHeadId1.Visible = false;
            combo_Level.Visible = true;
            Session["Updation"] = "FrmLevelChange";
            fill_grid();
            fill_combo();
        }
        catch (Exception er)
        {
            Error_Occured("RadEmpLevel_CheckedChanged", er.Message);
        }
    }
    protected void RdHeadId_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            LblSel.Text = "HeadId ";
            TxtHeadId1.Visible = true;
            RadPaycode1.Visible = true;
            RadName1.Visible = true;
            TxtHeadId1.Items.Clear();
            combo_Level.Visible = false;
            Session["Updation"] = "FrmHeadIdUpdate";
            fill_grid();
            bindheadid();
        }
        catch (Exception er)
        {
            Error_Occured("RdHeadId_CheckedChanged", er.Message);
        }
    }
    private int FindHeadId()
    {
        try
        {
            OleDbDataReader drH;
            string qry = "select * from TblEmployee where PayCode='" + lblcode.ToString() + "' and companycode='" + Session["LoginCompany"].ToString().Trim().Trim() + "'";
            drH = cn.Execute_Reader(qry);
            if (drH.Read())
            {
                drH.Close();
                return 1;
            }
            else
            {
                drH.Close();
                return 0;
            }

        }
        catch (Exception er)
        {
            Error_Occured("FindHeadId", er.Message);
            return 0;
        }
    }
    protected void DdlDept_SelectedIndexChanged(object sender, EventArgs e)
    {
        fill_grid();
    }
    protected void cmdShowAll_Click(object sender, EventArgs e)
    {
        DdlDept.SelectedIndex = 0;
        ddlPaycode.SelectedIndex = 0;
        combo_Level.SelectedIndex = 0;
        fill_grid();
        if (DataGrid1.Items.Count <= 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('No record found');", true);
            return;
        }
    }
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        if (Session["usertype"].ToString() == "A")
        {
            Response.Redirect("dashboard.aspx");
        }
        else
        {
            Response.Redirect("HomePage.aspx");
        }

    }
}