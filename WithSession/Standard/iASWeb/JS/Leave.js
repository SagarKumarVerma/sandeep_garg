﻿//On load
function Onload(ctl00_ContentPlaceHolder1_td1)
{
    var textbox = document.getElementById('ctl00_ContentPlaceHolder1_td1');   
    textbox.style.display = 'none';     
}

function Onload_Modify(ctl00_ContentPlaceHolder1_hdIsAccrual,ctl00_ContentPlaceHolder1_hdFixed,ctl00_ContentPlaceHolder1_td1,
ctl00_ContentPlaceHolder1_trPresent,ctl00_ContentPlaceHolder1_trMaxAccuralLimit)
{
    var hid= document.getElementById('ctl00_ContentPlaceHolder1_hdIsAccrual');   
    var hidfix= document.getElementById('ctl00_ContentPlaceHolder1_hdFixed');      
    var textbox = document.getElementById('ctl00_ContentPlaceHolder1_td1');   
    var Present= document.getElementById('ctl00_ContentPlaceHolder1_trPresent');    
    var MaxAccural = document.getElementById('ctl00_ContentPlaceHolder1_trMaxAccuralLimit');
    
    //Is Accrual
    if(hid.value=='Y')    
    {
    textbox.style.display = '';     
    }
    else
    {
    textbox.style.display = 'none';     
    }
    //Fixed checked or not
    if(hidfix.value=='Y')    
    {
       Present.style.display='none';      
       MaxAccural.style.display='none';       
    }
    else
    {
       Present.style.display='';      
       MaxAccural.style.display='';       
    }   
}


//On Is Accrual click
function IsAccrual(ctl00_ContentPlaceHolder1_chkIsAccrual,ctl00_ContentPlaceHolder1_td1)
{   
    var textbox = document.getElementById('ctl00_ContentPlaceHolder1_td1');
    if(document.getElementById('ctl00_ContentPlaceHolder1_chkIsAccrual').checked==true)
    {
    textbox.style.display = '';     
    }
    else if(document.getElementById('ctl00_ContentPlaceHolder1_chkIsAccrual').checked==false)
    {
      textbox.style.display = 'none';
    }
}


//On Radio click
function SanctionLimit(ctl00_ContentPlaceHolder1_radCarried,ctl00_ContentPlaceHolder1_radFixed,
ctl00_ContentPlaceHolder1_trPresent,ctl00_ContentPlaceHolder1_trMaxAccuralLimit)
{   
    var Carried = document.getElementById('ctl00_ContentPlaceHolder1_radCarried');
    var Fixed = document.getElementById('ctl00_ContentPlaceHolder1_radFixed');
    var Present= document.getElementById('ctl00_ContentPlaceHolder1_trPresent');    
    var MaxAccural = document.getElementById('ctl00_ContentPlaceHolder1_trMaxAccuralLimit');
    
    if(Carried.checked && !Fixed.checked)
    {
       Present.style.display='';      
       MaxAccural.style.display='';       
    }
    else
    {
       Present.style.display='none';       
       MaxAccural.style.display='none';       
    }    
}


/*

*************************************Leave Accural**************************************

*/

function IsPaycodesame(ctl00_ContentPlaceHolder1_txtPaycodeFrom,ctl00_ContentPlaceHolder1_txtPaycodeTo,
ctl00_ContentPlaceHolder1_lblName,ctl00_ContentPlaceHolder1_lblDesignation,ctl00_ContentPlaceHolder1_lblDept,
ctl00_ContentPlaceHolder1_lblCard,ctl00_ContentPlaceHolder1_lblCompany,ctl00_ContentPlaceHolder1_lblCatagory)
{   
    var P_From = document.getElementById('ctl00_ContentPlaceHolder1_txtPaycodeFrom');
    var P_To = document.getElementById('ctl00_ContentPlaceHolder1_txtPaycodeTo');        
    if(P_From.value == P_To.value)
    {
      document.getElementById('ctl00_ContentPlaceHolder1_lblName').style.display='';       
      document.getElementById('ctl00_ContentPlaceHolder1_lblDesignation').style.display='';       
      document.getElementById('ctl00_ContentPlaceHolder1_lblDept').style.display='';       
      document.getElementById('ctl00_ContentPlaceHolder1_lblCard').style.display='';       
      document.getElementById('ctl00_ContentPlaceHolder1_lblCompany').style.display='';       
      document.getElementById('ctl00_ContentPlaceHolder1_lblCatagory').style.display='';       
    }
    else
    {
      document.getElementById('ctl00_ContentPlaceHolder1_lblName').style.display='none';       
      document.getElementById('ctl00_ContentPlaceHolder1_lblDesignation').style.display='none';       
      document.getElementById('ctl00_ContentPlaceHolder1_lblDept').style.display='none';       
      document.getElementById('ctl00_ContentPlaceHolder1_lblCard').style.display='none';       
      document.getElementById('ctl00_ContentPlaceHolder1_lblCompany').style.display='none';       
      document.getElementById('ctl00_ContentPlaceHolder1_lblCatagory').style.display='none';             
    }    
}




/*Daily Report selection */

function DailyReportSelection(ctl00_ContentPlaceHolder1_radcontlatearrival,ctl00_ContentPlaceHolder1_radContEarlyDept,
ctl00_ContentPlaceHolder1_radContAbsenteesim,ctl00_ContentPlaceHolder1_tdCal,ctl00_ContentPlaceHolder1_radLateArrival,ctl00_ContentPlaceHolder1_radDailyPerformance,
ctl00_ContentPlaceHolder1_radAbsenteesim,ctl00_ContentPlaceHolder1_radEarlyDeparture,ctl00_ContentPlaceHolder1_radAttendance,
ctl00_ContentPlaceHolder1_radTimeLoss,ctl00_ContentPlaceHolder1_radDepartment,ctl00_ContentPlaceHolder1_radOT,
ctl00_ContentPlaceHolder1_radEarlyArrival,ctl00_ContentPlaceHolder1_radshiftwise,ctl00_ContentPlaceHolder1_radPresent,
ctl00_ContentPlaceHolder1_radShiftChange,ctl00_ContentPlaceHolder1_radOverTimeSummary,ctl00_ContentPlaceHolder1_radMisPunch,
ctl00_ContentPlaceHolder1_radoutwork,ctl00_ContentPlaceHolder1_txtToDate,ctl00_ContentPlaceHolder1_imgCal,ctl00_ContentPlaceHolder1_lblto)
{   
    var lateArrival = document.getElementById('ctl00_ContentPlaceHolder1_radcontlatearrival');
    var EarlyDept = document.getElementById('ctl00_ContentPlaceHolder1_radContEarlyDept');
    var Absent= document.getElementById('ctl00_ContentPlaceHolder1_radContAbsenteesim');    
    var TdCal = document.getElementById('ctl00_ContentPlaceHolder1_tdCal');
    
    
    var radLateArrival= document.getElementById('ctl00_ContentPlaceHolder1_radLateArrival');    
    var radDailyPerformance= document.getElementById('ctl00_ContentPlaceHolder1_radDailyPerformance');    
    var radAbsent= document.getElementById('ctl00_ContentPlaceHolder1_radAbsenteesim');    
    var radEarlyDept= document.getElementById('ctl00_ContentPlaceHolder1_radEarlyDeparture');    
    var radAttendance= document.getElementById('ctl00_ContentPlaceHolder1_radAttendance');    
    var radTimeLoss= document.getElementById('ctl00_ContentPlaceHolder1_radTimeLoss');    
    var radDeptWise= document.getElementById('ctl00_ContentPlaceHolder1_radDepartment');    
    var radOT= document.getElementById('ctl00_ContentPlaceHolder1_radOT');    
    var radEarlyArrival= document.getElementById('ctl00_ContentPlaceHolder1_radEarlyArrival');    
    var radShiftwisePresent= document.getElementById('ctl00_ContentPlaceHolder1_radshiftwise');    
    var radPresent= document.getElementById('ctl00_ContentPlaceHolder1_radPresent');        
    var radShiftChange= document.getElementById('ctl00_ContentPlaceHolder1_radShiftChange');        
    var radOTSummary= document.getElementById('ctl00_ContentPlaceHolder1_radOverTimeSummary');    
    var radMisPunch= document.getElementById('ctl00_ContentPlaceHolder1_radMisPunch');    
    var radradoutwork= document.getElementById('ctl00_ContentPlaceHolder1_radoutwork');    
    var txtToDate= document.getElementById('ctl00_ContentPlaceHolder1_txtToDate');    
    var imgCal= document.getElementById('ctl00_ContentPlaceHolder1_imgCal');                 
    var lblto = document.getElementById('ctl00_ContentPlaceHolder1_lblto');   
    
    if(lateArrival.checked || EarlyDept.checked || Absent.checked)
    {    
    txtToDate.style.display='';            
    imgCal.style.display='';      
    lblto.style.display='';       
    }
    else if(radLateArrival.checked || radDailyPerformance.checked || radAbsent.checked || radEarlyDept.checked
    || radAttendance.checked || radTimeLoss.checked || radDeptWise.checked || radOT.checked ||
     radEarlyArrival.checked || radShiftwisePresent.checked || radPresent.checked || radShiftChange.checked 
     || radOTSummary.checked || radMisPunch.checked || radradoutwork.checked )
    {
       txtToDate.style.display='none';
       imgCal.style.display='none';
       lblto.style.display='none';
    }    
}
function OnloadReport(ctl00_ContentPlaceHolder1_txtToDate,ctl00_ContentPlaceHolder1_imgCal,ctl00_ContentPlaceHolder1_lblto)
{
    var textbox = document.getElementById('ctl00_ContentPlaceHolder1_txtToDate');   
    var textbox1 = document.getElementById('ctl00_ContentPlaceHolder1_imgCal');   
    var lblto = document.getElementById('ctl00_ContentPlaceHolder1_lblto');   
    textbox.style.display = 'none';     
    textbox1.style.display = 'none';     
    lblto.style.display = 'none';     
}


function isAlphabet(ctl00_ContentPlaceHolder1_txtDescription)
{
    var elem = document.getElementById('ctl00_ContentPlaceHolder1_txtDescription');   
	var alphaExp = /^[a-zA-Z]+$/;
	if(elem.value.match(alphaExp))
	{
		alert('yes');
		return true;
	}
	else
	{	
	    alert('no');		
		return false;
    }
}

