﻿function chkvalue(ctl00_ContentPlaceHolder1_ddlFirstWeeklyOff,ctl00_ContentPlaceHolder1_ddlSecondWeeklyOff,ctl00_ContentPlaceHolder1_ddlHalfDayShift,ctl00_ContentPlaceHolder1_chkSecondWeeklyOff)
{
var first=document.getElementById('ctl00_ContentPlaceHolder1_ddlFirstWeeklyOff');
var Text1= first.options[first.selectedIndex].text;
var ddlReport = document.getElementById('ctl00_ContentPlaceHolder1_ddlSecondWeeklyOff');
var Text = ddlReport.options[ddlReport.selectedIndex].text; 

if(Text1==Text)
{
    window.alert("First Weekly Off and Second Weekly Off may not be same");
    document.getElementById('ctl00_ContentPlaceHolder1_ddlSecondWeeklyOff').selectedIndex=0;
    document.getElementById('ctl00_ContentPlaceHolder1_ddlSecondWeeklyOff').focus();
    return false;
}
else if(Text=='None')
{
    document.getElementById('ctl00_ContentPlaceHolder1_chkSecondWeeklyOff').disabled=true;
    document.getElementById('ctl00_ContentPlaceHolder1_ddlSecondWeeklyOffType').disabled=true;
    document.getElementById('ctl00_ContentPlaceHolder1_ddlHalfDayShift').disabled=true;
    document.getElementById('ctl00_ContentPlaceHolder1_ddlHalfDayShift').value="";
    return false;
}
else if(Text!='None')
{
    document.getElementById('ctl00_ContentPlaceHolder1_chkSecondWeeklyOff').disabled=false; 
    document.getElementById('ctl00_ContentPlaceHolder1_ddlSecondWeeklyOffType').disabled=false;
    document.getElementById('ctl00_ContentPlaceHolder1_ddlHalfDayShift').disabled=false;
    document.getElementById('ctl00_ContentPlaceHolder1_ddlHalfDayShift').selectedIndex=0;
    return false;
}
}


//full day/half day

function SecondOffType(ctl00_ContentPlaceHolder1_ddlSecondWeeklyOffType,ctl00_ContentPlaceHolder1_ddlHalfDayShift)
{
 var ddlReport = document.getElementById('ctl00_ContentPlaceHolder1_ddlSecondWeeklyOffType');
 var Text = ddlReport.options[ddlReport.selectedIndex].text; 
 if(Text=='Full')
 {
    document.getElementById('ctl00_ContentPlaceHolder1_ddlHalfDayShift').disabled=true;
    document.getElementById('ctl00_ContentPlaceHolder1_ddlHalfDayShift').value="";
    return false;
 }
 else if(Text=='Half')
 { 
    document.getElementById('ctl00_ContentPlaceHolder1_ddlHalfDayShift').disabled=false;
    document.getElementById('ctl00_ContentPlaceHolder1_ddlHalfDayShift').selectedIndex=0;
    return false;
 } 
}

