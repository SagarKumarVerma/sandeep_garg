﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="frmTimeTable.aspx.cs" Inherits="frmTimeTable" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <script language="javascript" type="text/javascript">
        var ewin = null;
        function winopen() {
            ewin = window.open("frmselection.aspx", "eoffice", "width=600, height=260, left=100, top=100")
        }
        function winclose() {
            if (ewin && !ewin.closed) {
                ewin.close();
            }
        }
        function frameprint() {
            frames["report"].focus();
            frames["report"].print();
        }
        function change() {
            //document.getElementById('report').src="frmmonperformance.aspx?fromdate=2005-01-01&todate=2005-01-31";

        }
        function TABLE1_onclick() { }

        function IMG1_onclick() {

        }
    </script>
    <div>
        <table align="center" style="width: 100%" cellpadding="0" cellspacing="0">
            <tr>
                <td></td>
            </tr>

            <tr>
                <td align="center">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;" class="tableCss" align="center">
                        <tr>
                            <td style="width: 100%; vertical-align: top;">
                                <table border="0" id="TABLE1" runat="server" width="100%" cellpadding="0" cellspacing="0">
                                    <tr class="lgHeader1">
                                        <td runat="server" align="center" colspan="7" style="width: 850px; height: 25px" class="tableHeaderCss">

                                            <asp:Label ID="LblFrm" runat="server" CssClass="lblCss"></asp:Label></td>
                                    </tr>
                                    <tr class="lgHeader1" align="left">
                                        <td colspan="7" style="width: 100%; padding-left: 15px;">
                                            <table style="width: 600px;" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="height: 5px"></td>
                                                    <td align="left" style="height: 5px"></td>
                                                    <td style="height: 5px"></td>
                                                    <td style="height: 5px"></td>
                                                    <td align="left" style="height: 5px"></td>
                                                </tr>
                                                <tr>
                                                    <td class="text_new" style="width: 125px">
                                                        <asp:Label ID="LblDept" runat="server" Text="Department"></asp:Label></td>
                                                    <td style="width: 150px">
                                                        <dx:ASPxComboBox ID="DeptCombo" runat="server" ValueType="System.String" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="DeptCombo_SelectedIndexChanged" NullText="None" ></dx:ASPxComboBox>
                                                    </td>
                                                    <td style="width: 10px"></td>
                                                    <td class="text_new" style="width: 125px">Shift</td>
                                                    <td style="width: 125px">
                                                        <dx:ASPxComboBox ID="ShiftCombo" runat="server" ValueType="System.String" Width="150px" AutoPostBack="True" NullText="None"></dx:ASPxComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 5px;"></td>
                                                    <td style="height: 5px;"></td>
                                                    <td style="height: 5px;"></td>
                                                    <td style="height: 5px;"></td>
                                                    <td style="height: 5px;"></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 125px" class="text_new">From </td>
                                                    <td style="width: 150px" align="left">
                                  
                                                        <dx:ASPxDateEdit ID="txtfromdate" runat="server" Width="150px" EditFormatString="dd/MM/yyyy" ></dx:ASPxDateEdit>
                                                    </td>
                                                    <td class="text_new" style="width: 10px"></td>
                                                    <td style="width: 125px" class="text_new">To</td>
                                                    <td style="width: 125px" align="left">
                                                        
                                                        <dx:ASPxDateEdit ID="txttodate" runat="server" Width="150px" EditFormatString="dd/MM/yyyy" ></dx:ASPxDateEdit>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 5px;"></td>
                                                    <td style="height: 5px;"></td>
                                                    <td style="height: 5px;"></td>
                                                    <td style="height: 5px;"></td>
                                                    <td style="height: 5px;"></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 125px" class="text_new">First W.Off</td>
                                                    <td style="width: 150px" align="left">
                                                     
                                                        <dx:ASPxComboBox ID="DDOff1" runat="server" SelectedIndex="0" Width="150px">
                                                            <Items>
                                                                <dx:ListEditItem Selected="True" Text="Sunday" Value="0" />
                                                                 <dx:ListEditItem Text="Monday" Value="1" />
                                                                 <dx:ListEditItem  Text="Tuesday" Value="2" />
                                                                 <dx:ListEditItem  Text="Wednesday" Value="3" />
                                                                 <dx:ListEditItem Text="Thursday" Value="4" />
                                                                 <dx:ListEditItem  Text="Friday" Value="5" />
                                                                 <dx:ListEditItem  Text="Saturday" Value="6" />
                                                                 <dx:ListEditItem  Text="None" Value="7" />
                                                               
                                                            </Items>
                                                        </dx:ASPxComboBox>

                                                    </td>
                                                    <td style="width: 10px"></td>
                                                    <td style="width: 125px" class="text_new">Second W.Off</td>
                                                    <td style="width: 125px" align="left">
                                                      <dx:ASPxComboBox ID="DDOff2" runat="server" SelectedIndex="0" Width="150px">
                                                            <Items>
                                                                <dx:ListEditItem Selected="True" Text="Sunday" Value="0" />
                                                                 <dx:ListEditItem Text="Monday" Value="1" />
                                                                 <dx:ListEditItem  Text="Tuesday" Value="2" />
                                                                 <dx:ListEditItem  Text="Wednesday" Value="3" />
                                                                 <dx:ListEditItem Text="Thursday" Value="4" />
                                                                 <dx:ListEditItem  Text="Friday" Value="5" />
                                                                 <dx:ListEditItem  Text="Saturday" Value="6" />
                                                                 <dx:ListEditItem  Text="None" Value="7" />
                                                               
                                                            </Items>
                                                        </dx:ASPxComboBox>
                                                </tr>
                                                <tr>
                                                    <td style="height: 5px"></td>
                                                    <td align="left" style="height: 5px"></td>
                                                    <td style="height: 5px"></td>
                                                    <td style="height: 5px"></td>
                                                    <td align="left" style="height: 5px"></td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 5px" align="left">
                                                        <asp:Label ID="LblTeam" runat="server" Text="Team" CssClass="text_new"></asp:Label></td>
                                                    <td align="left" style="height: 5px">
                                                        &nbsp;</td>
                                                    <td style="height: 5px"></td>
                                                    <td style="height: 5px" align="left">
                                                        &nbsp;</td>
                                                    <td align="left" style="height: 5px">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td style="height: 5px"></td>
                                                    <td align="left" style="height: 5px"></td>
                                                    <td style="height: 5px"></td>
                                                    <td style="height: 5px"></td>
                                                    <td align="left" style="height: 5px"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr class="lgHeader1">
                                        <td id="Td2" runat="server" colspan="7" align="right">&nbsp;
                                            <%--<asp:Button ID="BtnShowRoster1" runat="server" CssClass="buttoncss" OnClick="BtnShowRoster_Click" Text="Show Roster" ValidationGroup="MKE" />--%>
                                            <dx:ASPxButton ID="BtnShowRoster" runat="server" Text="Show Roster" OnClick="BtnShowRoster_Click"></dx:ASPxButton>
                                           <%-- <asp:Button ID="Btn_ChangeShift" runat="server" CssClass="buttoncss" OnClick="Btn_ChangeShift_Click" Text="Change Shift" ValidationGroup="MKE" />--%>
                                            <dx:ASPxButton ID="Btn_ChangeShift" runat="server" Text="Change Shift" OnClick="Btn_ChangeShift_Click"></dx:ASPxButton>
                                           <%-- <asp:Button ID="Button1" runat="server" CausesValidation="False" CssClass="buttoncss" OnClick="Button1_Click" Text="Cancel" />--%>
                                             <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Cancel"></dx:ASPxButton>
                                            &nbsp;

                                        </td>
                                    </tr>
                                    <tr align="right" class="lgHeader1">
                                        <td align="left" colspan="7" style="width: 850px;" id="TD3" runat="server" visible="false" class="text_new">
                                            &nbsp;
                                    &nbsp;
                                        </td>
                                    </tr>
                                    <tr align="right">
                                      

                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                    </tr>

                                </table>
                                <div style="overflow: auto; width: 100%; text-align: left; height: 325px;">
                                   
                                    <dx:ASPxGridView ID="DataGrid1" runat="server"  Width="100%" >
                                        <SettingsPager Visible="False">
                                        </SettingsPager>
                                        <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
                                    </dx:ASPxGridView>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

