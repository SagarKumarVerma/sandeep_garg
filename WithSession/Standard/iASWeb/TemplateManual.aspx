﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="TemplateManual.aspx.cs" Inherits="TemplateManual" %>
<%@ Register assembly="DevExpress.Web.v17.1, Version=17.1.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
           <script type="text/javascript">
         function grid_SelectionChanged(s, e) {
             s.GetSelectedFieldValues("Biometric ID", GetSelectedFieldValuesCallback);
         }
         function GetSelectedFieldValuesCallback(values) {
         document.getElementById("selCount").innerHTML = grid.GetSelectedRowCount();
         }
    </script>
     <div style="border-style: solid; border-width: thin; border-color: inherit;">
    <table class="dxeBinImgCPnlSys">
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <dx:ASPxTextBox ID="mTransIdTxt" runat="server" Width="170px" Visible="false">
                </dx:ASPxTextBox>
            </td>
            
        </tr>
        <tr>
          <%--  <td align="left">
                 <table>
                    <tr>
                 
                          <div class="TopPadding">
           <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Selected Records:"></dx:ASPxLabel> <span id="selCount" style="font-weight: bold ; font-size:small ; font-family:Tahoma" >0</span>
        </div>
                <dx:ASPxTextBox ID="mUserID" runat="server" Width="170px" >
                </dx:ASPxTextBox>
                <dx:ASPxTextBox ID="mDeviceID" runat="server" Width="170px" >
                </dx:ASPxTextBox>
                        </tr>
                     </table>      
                </td>--%>
            <td align="center">
                <table>
                    <tr>
                              <td>
                            <dx:ASPxButton ID="ASPxButton3" runat="server" Text="Download" Theme="SoftOrange" OnClick="ASPxButton3_Click" >
                    <Image IconID="actions_download_16x16">
                    </Image>
                </dx:ASPxButton>
                        </td>
                        <td>
                            <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Upload" Theme="SoftOrange" OnClick="ASPxButton1_Click" style="height: 26px" Visible="true" >
                    <Image IconID="miscellaneous_publish_16x16">
                    </Image>
                </dx:ASPxButton>
                        </td>
                             <td>
                            <dx:ASPxButton ID="ASPxButton4" runat="server" Text="Upload To DB" Theme="SoftOrange" OnClick="ASPxButton4_Click" style="height: 26px" Visible="true" >
                    <Image IconID="miscellaneous_publish_16x16">
                    </Image>
                </dx:ASPxButton>
                        </td>
                         <td>
                            <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Delete" Theme="SoftOrange" Visible="true" >
                    <Image IconID="actions_trash_16x16">
                    </Image>
                </dx:ASPxButton>

                        </td>
                        <td>
                            <dx:ASPxCheckBox ID="chkAdmin" Text="Make Admin" runat="server" Theme="SoftOrange"></dx:ASPxCheckBox>
                        </td>
                    </tr>
                </table>

            

            </td>
           
        </tr>
        <tr>
              <td valign="top" >
          

   <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False" 
        EnableTheming="True" KeyFieldName="Biometric ID"
        Theme="SoftOrange" Width="100%" Font-Size="Small" OnPageIndexChanged="ASPxGridView1_PageIndexChanged" OnProcessColumnAutoFilter="ASPxGridView1_ProcessColumnAutoFilter" ClientInstanceName="grid"    >
       
        <SettingsPager EnableAdaptivity="True" PageSize="1000">
        </SettingsPager>
       
        <Settings ShowFilterRow="True" />
       
        <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
       
               <Columns>
            <dx:GridViewCommandColumn ShowInCustomizationForm="True" VisibleIndex="0" Width="8%" Caption=" " SelectAllCheckboxMode="Page" ShowSelectCheckbox="True" ShowClearFilterButton="True">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn Caption="User ID" FieldName="Biometric ID" 
                ShowInCustomizationForm="True" VisibleIndex="1">
                <Settings ShowFilterRowMenuLikeItem="True" />
                <SettingsHeaderFilter Mode="CheckedList">
                </SettingsHeaderFilter>
                <CellStyle ForeColor="Blue">
                </CellStyle>
            </dx:GridViewDataTextColumn>
           <%-- <dx:GridViewDataTextColumn Caption="User Name" FieldName="user_name" 
                ShowInCustomizationForm="True" VisibleIndex="2">
            </dx:GridViewDataTextColumn>--%>
             <dx:GridViewDataTextColumn Caption="Enroll Mode" FieldName="Enroll Mode" 
                ShowInCustomizationForm="True" VisibleIndex="3">
            </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Device" FieldName="Cloud ID" 
                ShowInCustomizationForm="True" VisibleIndex="4">
            </dx:GridViewDataTextColumn>
         
           
                   <dx:GridViewDataTextColumn Caption="Name" VisibleIndex="2" FieldName="NAME">
                   </dx:GridViewDataTextColumn>
         
           
        </Columns>
        <ClientSideEvents SelectionChanged="grid_SelectionChanged" />
    </dx:ASPxGridView>

            </td>
                     <td valign="top" >
                   <dx:ASPxGridView ID="ASPxGridView2" runat="server" AutoGenerateColumns="False" 
        EnableTheming="True" KeyFieldName="device_id" 
        Theme="SoftOrange" Width="100%" Font-Size="Small" OnPageIndexChanged="ASPxGridView2_PageIndexChanged" OnProcessColumnAutoFilter="ASPxGridView2_ProcessColumnAutoFilter"   >
       
                       <SettingsPager PageSize="50">
                       </SettingsPager>
       
                       <Settings ShowFilterRow="True" />
       
        <SettingsDataSecurity AllowDelete="False" AllowEdit="False" AllowInsert="False" />
       
        <Columns>
            <dx:GridViewCommandColumn ShowInCustomizationForm="True" VisibleIndex="0" Width="8%" Caption=" " SelectAllCheckboxMode="Page" ShowSelectCheckbox="True" ShowClearFilterButton="True">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn Caption="Device ID" FieldName="device_id" 
                ShowInCustomizationForm="True" VisibleIndex="1">
                <CellStyle ForeColor="Blue">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Device Name" FieldName="device_name" 
                ShowInCustomizationForm="True" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
             <dx:GridViewDataTextColumn Caption="Group" FieldName="GroupID" 
                ShowInCustomizationForm="True" VisibleIndex="2">
            </dx:GridViewDataTextColumn>
         
           
        </Columns>
    </dx:ASPxGridView>
            </td>
        </tr>
    </table>
    </div>
</asp:Content>

