﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Globalization;
using System.Threading;
using System.Data.OleDb;
using System.Text;
using System.IO;
using System.Drawing;
using System.Net.Mail;
using System.Net;


public partial class frmLeaveApplication : System.Web.UI.Page
{
    protected System.Web.UI.HtmlControls.HtmlGenericControl PageTitle;
    static public string title = "";
    Class_Connection cn = new Class_Connection();
    OleDbDataReader dr;
    OleDbConnection Connection = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"]);
    //public string txtFromDateCID = "";//txtfromdate.Text.ToString();
    //public string txtToDateCID = "";// txttodate.Text.ToString();
    OleDbConnection MyCon;
    OleDbTransaction oleTransaction;
    OleDbCommand MyCmd;

    string strsql;
    string change;
    string LeaveName;
    string FinYear, CurYear;
    int appno, DayCnt;
    int i;
    double days, LvDeducted;
    string HalfDay;
    Boolean AddLeave;
    DateTime date1, date2, dt;
    ArrayList LeaveCode = new ArrayList();
    Boolean present;

    ErrorClass ec = new ErrorClass();

    static DateTime dt_FinancialYear;
    string Strsql = null;
    DataSet ds = null;

    int L_YEAR;
    DataSet rsLeave = null;
    DataSet rsEmp = null;
    DataSet rsLedger = null;
    string sSql = "";
    string res1 = "";
    string d = "";
    DateTime dt1;
    string fname, dest, src;

    string strpaycode;
    string strdatefrom;
    string strleavecode;    
    
    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    protected void Page_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();
        if (ex is HttpRequestValidationException)
        {
            string resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" +
                   "Form doesn't need to contain valid HTML, just anything with opening and closing angled brackets (<...>).<br />" +
                   btnGoBack +
                   "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
        else if (ex is OleDbException)
        {
            string resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" + ex.Message.ToString() + ".<br />" +
                    "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
        else
        {
            string resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" + ex.Message.ToString() + ".<a href='Login.aspx' class='link'> Back to Login Page </a> <br />" +
                    "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        //Call The function to write the error log file 

        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        { }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        //BindGridInit();
        //grdrequest.DataBind();
        //fill_LeaveGrid();
        //LeaveGrid.DataBind();

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            
            bindPaycode();
            if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
            {
                Request.Browser.Adapters.Clear();
            }
            if ( Session["UserName"] == null && Session["Today"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            else
            {
                if (Session["FormName"] != null)
                {
                    lblMsg.Text = Session["FormName"].ToString();
                    txtpaycode.Focus();
                }
            }
           
        }
        if (Session["Today"] != null)
        {
            dt = Convert.ToDateTime(Session["Today"].ToString());
        }
        else
        {
            dt = System.DateTime.Now;
        }
        lblCOAgainst.Visible = false;
        if (!IsPostBack)
        {
            if (Session["usertype"].ToString().ToUpper() != "A")
                txtpaycode.Text = Session["PAYCODE"].ToString();

            dt_FinancialYear = DateTime.Now.Date;
            if (ConfigurationManager.AppSettings["isFinancialYear"].ToString() == "Y")
            {
                if (dt_FinancialYear.Month == 1 || dt_FinancialYear.Month == 2 || dt_FinancialYear.Month == 3)
                {
                    dt_FinancialYear = dt_FinancialYear.AddYears(-1);
                }
            }
            if (ConfigurationManager.AppSettings["isFinancialYear"].ToString() == "Y")
            {
                FinYear = Convert.ToString(dt_FinancialYear.Year) + "-04-01";
                CurYear = (dt_FinancialYear.Year + 1).ToString() + "-03-31";
            }
            else
            {
                FinYear = Convert.ToString(dt_FinancialYear.Year) + "-01-01";
                CurYear = Convert.ToString(dt_FinancialYear.Year) + "-12-31";
            }
            if (!IsPostBack)
            {
                if (Session["usertype"].ToString().ToUpper() != "U")
                    txtpaycode.Attributes.Add("onmousedown", "ShowPopup(this);");
            }
            bindFields();
        }
        //if (Session["usertype"].ToString() == "A" && txtpaycode.Text.Trim() == "" && Session["PaycodePassed"] == null)
       //     return;

        string strsql;
        DataSet ds = new DataSet();

        if (Connection.State == 0)
            Connection.Open();

        if (Session["usertype"].ToString() != "A")
            txtpaycode.Enabled = false;

        lblmessage.Text = "";
        if (!IsPostBack)
        {
            lblCOAgainst.Visible = false;
            ViewState["IsSL"] = "N";
            if (Session["PaycodePassed"] != null)
            {
                ViewState["paycode"] = Session["PaycodePassed"].ToString().Trim();
                Session["PaycodePassed"] = null;
                txtpaycode.Text = ViewState["paycode"].ToString();

                //txtpaycode_TextChanged(sender, e);
                
            }
            else
            {
                ViewState["paycode"] = Session["PAYCODE"].ToString();
                ViewState["SSN"] = Session["SSN"].ToString();
            }
            SetName();
            Fill_LeaveCombo();
            fill_LeaveGrid();
            
        }
        ViewState["td"] = txttodate.Text;
        ViewState["lf"] = "";

    }
    public void SetDate()
    {

        try
        {
            
        }
        catch (Exception er)
        {
            Error_Occured("SetDate", er.Message);
        }
    }
    protected void bindPaycode()
    {
       string ReportView = ConfigurationSettings.AppSettings["ReportView_HeadID"].ToString();
        Strsql = "Select paycode,rtrim(Empname) + ' - ' + paycode as Empname1  from tblemployee where Active='Y' ";
        if (Session["usertype"].ToString() == "A")
        {
            if (Session["Auth_Comp"] != null)
            {
                Strsql += " and companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
            }
            if (Session["Auth_Dept"] != null)
            {
                Strsql += " and Departmentcode in (" + Session["Auth_Dept"].ToString().Trim() + ") ";
            }
            Strsql += "  Order by EmpName ";
        }
        else if (Session["usertype"].ToString().Trim() == "H")
        {
            if (ReportView.ToString().Trim() == "Y")
            {
                Strsql += " and headid = '" + Session["PAYCODE"].ToString().ToString().Trim() + "' ";
            }
            else
            {
                if (Session["Auth_Comp"] != null)
                {
                    Strsql += " and companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
                }
                if (Session["Auth_Dept"] != null)
                {
                    Strsql += " and Departmentcode in (" + Session["Auth_Dept"].ToString().Trim() + ") ";
                }
            }
            Strsql += "  Order by EmpName ";
        }
        else
            Strsql = "Select paycode,rtrim(Empname) + ' - ' + paycode as Empname1  from tblemployee where paycode='" + Session["LoginPayCode"].ToString().Trim().Trim() + "' and Active='Y' ";


        ds = new DataSet();

        ds = cn.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlPayCode.DataSource = ds;
            ddlPayCode.TextField = "EmpName1";
            ddlPayCode.ValueField = "paycode";
            
            ddlPayCode.DataBind();
            ddlPayCode.SelectedIndex = -1;
          //  ddlPayCode.Items.Insert(0, "NONE");
        }
        else
        {
            ddlPayCode.Items.Add("NONE");
            ddlPayCode.SelectedIndex = 0;


        }
    }
    protected void ddlPayCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            LeaveGrid.Visible = true;
            txtpaycode.Text = ddlPayCode.SelectedItem.Value.ToString().Trim();
            try
            {
                ViewState["paycode"] = txtpaycode.Text;
                lblerror.Visible = false;
                dt_FinancialYear = DateTime.Now.Date;

                if(txtpaycode.Text.ToString().Trim().ToUpper()=="NONE")
                {
                    LblName.Text = "";
                    LeaveGrid.DataSource = null;
                    LeaveGrid.DataBind();
                    LeaveGrid.Visible = false;
                    return;
                }


                if (ConfigurationManager.AppSettings["isFinancialYear"].ToString() == "Y")
                {
                    if (dt_FinancialYear.Month == 1 || dt_FinancialYear.Month == 2 || dt_FinancialYear.Month == 3)
                    {
                        dt_FinancialYear = dt_FinancialYear.AddYears(-1);
                    }
                }
                
                if (Session["usertype"].ToString().ToUpper() == "A")
                {

                    string qry = "select EMPNAME, IsNull (Trainee,'N') Trainee, Isnull(TraineeLevel,0) TraineeLevel,EmpType,SSN from tblemployee WHERE PAYCODE='" + txtpaycode.Text.Trim().ToUpper() + "' and Companycode='"+Session["LoginCompany"].ToString().Trim()+"' ";
                    dr = cn.ExecuteReader(qry);
                    if (dr.Read())
                    {
                        Session["EmpName"] = dr[0].ToString();
                        Session["Trainee"] = dr[1].ToString();
                        Session["TraineeLevel"] = dr[2].ToString();
                        ViewState["SSN"] = dr[4].ToString();
                    }
                    else
                    {
                        dr.Close();
                        //string change = "<script language='JavaScript'>alert('Invalid Paycode, Please give a valid Paycode here...')</script>";
                        //Page.RegisterStartupScript("frmchange", change);
                        txtpaycode.Text = ViewState["paycode"].ToString();
                        txtpaycode.Focus();
                        return;
                    }
                    dr.Close();


                    strsql = "select count(*) from tblemployee where paycode ='" + ViewState["paycode"].ToString() + "' and CompanyCode='"+Session["LoginCompany"].ToString().Trim().Trim()+"'  ";
                    dr = cn.ExecuteReader(strsql);
                    dr.Read();
                    if (Convert.ToInt32(dr[0].ToString()) <= 0)
                    {
                        dr.Close();
                        string change = "<script language='JavaScript'>alert('Invalid Paycode, Please give a valid Paycode here...')</script>";
                        Page.RegisterStartupScript("frmchange", change);
                        txtpaycode.Text = ViewState["paycode"].ToString();
                        return;
                    }
                    dr.Close();
                }

                if (Session["usertype"].ToString() == "T")  //Team Leader 
                    strsql = "select * from tblemployee where teamid =(select teamid from tblemployee where paycode='" + Session["username"].ToString() + "' ) and paycode ='" + ViewState["paycode"].ToString() + "'  ";
                if (Session["Trainer"].ToString().ToUpper() == "Y")  //Trainer
                    strsql = "select * from tblemployee where paycode ='" + ViewState["paycode"].ToString() + "' and headid ='" + Session["username"].ToString() + "'  ";
                if (Session["usertype"].ToString() == "T" && Session["Trainer"].ToString().ToUpper() == "Y")   // user is both Trainer and Team Leader
                    strsql = "select * from tblemployee where paycode ='" + ViewState["paycode"].ToString() + "' and headid ='" + Session["username"].ToString() + "'  ";
                if (Session["usertype"].ToString() == "H") // for OA and HOD 
                    strsql = "select * from tblemployee where paycode ='" + ViewState["paycode"].ToString() + "' and headid='" + Session["LoginUserName"].ToString().Trim().Trim() + "'  and CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "' ";  //Departmentcode in (" + Session["Auth_Dept"].ToString() + " ) and paycode ='" + ViewState["paycode"].ToString() + "'  ";
                if (Session["IsOA"].ToString() == "Y")
                {
                    strsql = "Select * from tblemployee where Departmentcode in (" + Session["Auth_Dept"].ToString() + " ) ";
                    strsql += " Union select * from tblemployee where Departmentcode =(select departmentcode from tblemployee where paycode='" + Session["LoginUserName"].ToString().Trim().Trim() + "'  and CompanyCode='"+Session["LoginCompany"].ToString().Trim().Trim()+"'";
                    strsql += " And teamid = ( select teamid from tblemployee where paycode='" + Session["username"].ToString() + "'))";
                }
                if (Session["usertype"].ToString() == "A")
                {
                    strsql = "select * from tblemployee where paycode ='" + ViewState["paycode"].ToString() + "'   and CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "' "; 
                }

                dr = cn.ExecuteReader(strsql); 
                if (dr.Read())
                {
                    ViewState["SSN"] = dr["SSN"].ToString();
                    BindGrid();
                    Fill_LeaveCombo();
                    fill_LeaveGrid();
                    SetName();
                    Fill_Holiday();
                }
                else
                {
                    string change = "<script language='JavaScript'>alert('Invalid Paycode or not in your Department/Team...')</script>";
                    Page.RegisterStartupScript("frmchange", change);
                    ViewState["paycode"] = Session["username"].ToString();
                    txtpaycode.Text = ViewState["paycode"].ToString();
                    SetName();

                    BindGrid();
                    Fill_LeaveCombo();
                    fill_LeaveGrid();
                }
                dr.Close();
                

                RefreshMe();
                txtfromdate.Focus();
                //ScriptManager1.SetFocus(txtfromdate);
            }

            catch (Exception er)
            {
                Error_Occured("txtpaycode_TextChanged", er.Message);
            }
        }
        catch
        {

        }
    }
    private void RefreshMe()
    {
        TXTREMARKS.Text = "";
        DDLREASON.Enabled = true;
        txtfromdate.Enabled = true;
        txttodate.Enabled = true;
    }
    protected void bindFields()
    {
        Strsql = "select * from tblMapping";
        ds = new DataSet();
        ds = cn.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            lblEmpCode.Text = ds.Tables[0].Rows[1]["Showas"].ToString().Trim();
            lblEmpname.Text = ds.Tables[0].Rows[2]["Showas"].ToString().Trim();
        }
    }

    private void Fill_LeaveCombo()
    {
        try
        {
            OleDbDataReader dr;
            string Trainee, Tlevel;

            {
                strsql = " select sex,isnull(Ismarried,'N') from tblemployee where paycode = '" + ViewState["paycode"].ToString() + "' and CompanyCode='"+Session["LoginCompany"].ToString().Trim().Trim()+"' ";
                dr = cn.ExecuteReader(strsql);
                dr.Read();

                if (dr[0].ToString() == "M" && dr[1].ToString() == "Y")
                    strsql = " Select leavecode+' - ' +ltrim(leavedescription) as LeaveDesc,leavecode,leavefield from tblleavemaster where ShowOnWeb='Y' And LeaveCode <> 'MTL' AND LeaveCode<>'HLD' and CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "' ";
                else if (dr[0].ToString() == "F" && dr[1].ToString() == "Y")
                    strsql = " Select leavecode+' - ' +ltrim(leavedescription) as LeaveDesc,leavecode,leavefield from tblleavemaster where ShowOnWeb='Y' And LeaveCode <> 'PTL' AND LeaveCode<>'HLD' and CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "' ";
                else
                    strsql = " Select leavecode+' - ' +ltrim(leavedescription) as LeaveDesc,leavecode,leavefield from tblleavemaster where ShowOnWeb='Y' And LeaveCode <> 'PTL' and LeaveCode <> 'MTL' AND LeaveCode<>'HLD' and CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "'";
            }
            dr.Close();
            OleDbDataAdapter da = new OleDbDataAdapter(strsql, Connection);
            DataSet ds1 = new DataSet();

            txtfromdate.Text = DateTime.Now.ToString("dd") + "/" + DateTime.Now.Month.ToString("00") + "/" + DateTime.Now.Year.ToString("0000");
            txttodate.Text = DateTime.Now.ToString("dd") + "/" + DateTime.Now.Month.ToString("00") + "/" + DateTime.Now.Year.ToString("0000");

            DDLREASON.Items.Clear();

            da.Fill(ds1, "leavemaster");
            DDLREASON.DataSource = ds1;
            DDLREASON.TextField = "LeaveDesc";
            DDLREASON.ValueField = "leavefield";
            DDLREASON.DataBind();
            BindGrid();
            DDLREASON.SelectedIndex = 0;
            if (DDLREASON.Items.Count > 0)
            {
                ViewState["lt"] = DDLREASON.SelectedItem.Text;
                ViewState["lc"] = DDLREASON.SelectedItem.Value.ToString();
            }
        }
        catch (Exception er)
        {
            Error_Occured("Fill_LeaveCombo", er.Message);
        }
    }
    int application()
    {
        try
        {
            OleDbCommand comm = new OleDbCommand("select max(application_no) from leave_request", Connection);
            OleDbDataReader dr;
            int no;
            dr = comm.ExecuteReader();
            if (dr.Read())
            {
                if (dr[0].ToString() != "")
                {
                    no = int.Parse(dr[0].ToString());
                    dr.Close();
                    return no + 1;
                }
                else
                {
                    dr.Close();
                    return 1;
                }
            }
            else
            {
                dr.Close();
                return 1;
            }
        }
        catch (Exception er)
        {
            Error_Occured("application", er.Message);
            return 0;
        }
    }
    public void Fill_Holiday()
    {
        try
        {
            DateTime dt = System.DateTime.Now.Date;
            string ddt = dt.ToString("yyyy-MM-dd");
            if (ConfigurationManager.AppSettings["isFinancialYear"].ToString() == "Y")
            {
                FinYear = Convert.ToString(dt_FinancialYear.Year) + "-04-01";
                CurYear = (dt_FinancialYear.Year + 1).ToString() + "-03-31";
            }
            else
            {
                FinYear = Convert.ToString(dt_FinancialYear.Year) + "-01-01";
                CurYear = Convert.ToString(dt_FinancialYear.Year) + "-12-31";
            }

            string strsql1 = "SELECT distinct convert(char (10),Hdate,103) Hdate, Holiday, month(hdate) HolidayMonth, year(Hdate) Hyear FROM Holiday ";
            strsql1 += "  Where Hdate between '" + FinYear + "' And '" + CurYear + "' ";
            strsql1 += "   Order by HYear,HolidayMonth, Hdate ";

            OleDbDataAdapter hda = new OleDbDataAdapter(strsql1, Connection);
            DataSet ds = new DataSet();
            hda.Fill(ds, "holiday");

            DDType.DataSource = ds;
            DDType.DataTextField = "Holiday";
            DDType.DataValueField = "Hdate";
            DDType.DataBind();
        }
        catch (Exception er)
        {
            Error_Occured("Fill_Holiday", er.Message);
        }
    }
    #region Web Form Designer generated code
    override protected void OnInit(EventArgs e)
    {
        //
        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //
        InitializeComponent();
        base.OnInit(e);
    }

    private void BindGrid()
    {
        try
        {
            string strsql;
            DataSet ds = new DataSet();
            if (Connection.State == 0)
            {
                Connection.Open();
            }

            strsql = "SELECT application_no,CONVERT(CHAR(10),request_date,103) 'REQUEST_DATE',LEAVECODE,CONVERT(CHAR(10),LEAVE_FROM,103) 'FROM',"+
                    " CONVERT(CHAR(10),leave_TO,103) 'TO',datediff( day, leave_from, leave_to)+1 as Days, 'Leave Amount'= case when halfDay='N' then Leavedays "+
                    " when halfDay=null then Leavedays when halfDay='F' then convert(float,datediff( day, leave_from, leave_to)+1) /2 when halfDay='S' then convert"+
                    " (float, datediff( day, leave_from, leave_to)+1) /2  end , Duration = case when halfday='N' then 'Full Day' when halfday='F' then 'First half'"+
                    " When halfday='S' then 'Second Half' end, USERREMARKS,Stage1_Status=CASE WHEN Stage1_APPROVED='Y' THEN 'APPROVED' ELSE CASE WHEN  "+
                    " stage1_APPROVED='N' THEN 'Rejected' ELSE 'PENDING' END END,Stage1_Remarks=Stage1_Approval_Remarks,Stage2_Status=CASE WHEN Stage2_APPROVED='Y' "+
                    " THEN 'APPROVED' ELSE CASE WHEN stage2_APPROVED='N' THEN 'Rejected' ELSE 'PENDING' END END,Stage2_Remarks=Stage2_Approval_Remarks,CONVERT(CHAR(10),"+
                    " stage1_approval_Date,103) 'Approval/Rejection Date', Stage2_Approval_REMARKS HOD_REMARKS,stage1_approved lavel1   FROM LEAVE_REQUEST WHERE"+
                    " PAYCODE='" + ViewState["paycode"].ToString() + "'  and SSN='" + ViewState["SSN"].ToString() + "' order by Application_No Desc";
            OleDbDataAdapter adap = new OleDbDataAdapter(strsql, Connection);

            //Response.Write(strsql.ToString());
            adap.Fill(ds, "leave");
            grdrequest.DataSource = ds;
            grdrequest.DataBind();
        }
        catch (Exception er)
        {
            Error_Occured("BindGrid", er.Message);
        }
    }
    private void BindGridInit()
    {
        try
        {
            string strsql;
            DataSet ds = new DataSet();
            if (Connection.State == 0)
            {
                Connection.Open();
            }
            
            strsql = "SELECT application_no,CONVERT(CHAR(10),request_date,103) 'REQUEST_DATE',LEAVECODE,CONVERT(CHAR(10),LEAVE_FROM,103) 'FROM'," +
                    " CONVERT(CHAR(10),leave_TO,103) 'TO',datediff( day, leave_from, leave_to)+1 as Days, 'Leave Amount'= case when halfDay='N' then Leavedays " +
                    " when halfDay=null then Leavedays when halfDay='F' then convert(float,datediff( day, leave_from, leave_to)+1) /2 when halfDay='S' then convert" +
                    " (float, datediff( day, leave_from, leave_to)+1) /2  end , Duration = case when halfday='N' then 'Full Day' when halfday='F' then 'First half'" +
                    " When halfday='S' then 'Second Half' end, USERREMARKS,Stage1_Status=CASE WHEN Stage1_APPROVED='Y' THEN 'APPROVED' ELSE CASE WHEN  " +
                    " stage1_APPROVED='N' THEN 'Rejected' ELSE 'PENDING' END END,Stage1_Remarks=Stage1_Approval_Remarks,Stage2_Status=CASE WHEN Stage2_APPROVED='Y' " +
                    " THEN 'APPROVED' ELSE CASE WHEN stage2_APPROVED='N' THEN 'Rejected' ELSE 'PENDING' END END,Stage2_Remarks=Stage2_Approval_Remarks,CONVERT(CHAR(10)," +
                    " stage1_approval_Date,103) 'Approval/Rejection Date', Stage2_Approval_REMARKS HOD_REMARKS,stage1_approved lavel1   FROM LEAVE_REQUEST WHERE" +
                    " PAYCODE='" + txtpaycode.Text.ToString().Trim() + "' and SSN='" + ViewState["SSN"].ToString() + "' order by Application_No Desc";
            OleDbDataAdapter adap = new OleDbDataAdapter(strsql, Connection);

            //Response.Write(strsql.ToString());
            adap.Fill(ds, "leave");
            grdrequest.DataSource = ds;
            grdrequest.DataBind();
        }
        catch (Exception er)
        {
            Error_Occured("BindGrid", er.Message);
        }
    }
    private void updateVoucher(string voucher)
    {
        try
        {
            string strsql;
            string mvoucher;
            int maxvoucher;
            maxvoucher = int.Parse(voucher);
            mvoucher = maxvoucher.ToString("0000000000");



            strsql = "update tblvoucherno SET voucherno = '" + mvoucher.ToString() + "' where CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "' ";

            if (Connection.State == 0)
                Connection.Open();
            OleDbCommand cmd = new OleDbCommand(strsql, Connection);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
        }
        catch (Exception er)
        {
            Error_Occured("Update_TblLeaveLedger", er.Message);
        }
    }

    private void Update_TblLeaveLedger()
    {
        try
        {
            string strsql;
            string lcod;
            int Woffs;
            int Hlds;

            /*  ALREADY DEDUCTED ON BUTTON CLICK JUST UPDATE THE TBLLEAVELEDGER
        
            //Deduct WeeklyOff, Holidays from applied number of days if WeeklyOff is Not Include
            string IsOffInclude = "N", IsHldInclude = "N";
            strsql = "Select IsOffInclude,IsHolidayInclude from TblLeaveMaster where LeaveCode='" + LeaveName + "' ";
            dr = cn.ExecuteReader(strsql);
            if (dr.Read())
            {
                IsOffInclude = dr["IsOffInclude"].ToString();
                IsHldInclude = dr["IsHolidayInclude"].ToString();
            }
            dr.Close();

            if (IsOffInclude == "N")
            {
                strsql = "Select count(Wo_Value) as Wo_Value from tbltimeregister where dateoffice between '" + date1.Date.ToString("yyyy-MM-dd") + "' And  '" + date2.Date.ToString("yyyy-MM-dd") + "'";
                strsql += " And Wo_Value>0 And paycode ='" + ViewState["paycode"].ToString() + "' ";

                Woffs = 0;
                dr = cn.ExecuteReader(strsql);
                if (dr.Read())
                {
                    Woffs = int.Parse(dr[0].ToString());
                    days = days - Woffs;
                }
                dr.Close();

            }


            if (IsHldInclude == "N")
            {
                strsql = "Select count(Holiday_Value) as Holiday_Value from tbltimeregister where dateoffice between '" + date1.Date.ToString("yyyy-MM-dd") + "' And  '" + date2.Date.ToString("yyyy-MM-dd") + "'";
                strsql += " And Holiday_Value> 0 And paycode ='" + ViewState["paycode"].ToString() + "' ";

                Hlds = 0;

                dr = cn.ExecuteReader(strsql);
                if (dr.Read())
                {
                    Hlds = int.Parse(dr[0].ToString());
                    days = days - Hlds;
                }
                dr.Close();

            }
            // --------- Deduction End ---------
             
           */

            lcod = DDLREASON.SelectedItem.Value.ToString();
            //lcod = ViewState["lf"].ToString();

            strsql = "update TBLLEAVELEDGER SET " + lcod.ToString() + " = IsNull(" + lcod.ToString() + ",0) + " + days + "  ";
            strsql += " where  paycode='" + ViewState["paycode"].ToString() + "' and lyear='" + dt_FinancialYear.Year + "' and SSN='"+ViewState["SSN"].ToString().Trim()+"'";

            if (Connection.State == 0)
                Connection.Open();
            OleDbCommand cmd = new OleDbCommand(strsql, Connection);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
        }
        catch (Exception er)
        {
            Error_Occured("Update_TblLeaveLedger", er.Message);
        }
    }

    public void fill_LeaveGrid()
    {
        StringBuilder sb = new StringBuilder();
        string LvName, strsql, ColName = "";
        int Qry_Flag = 0;
        int cnt = 0;
        LvName = "";
        string qry;
        int DojYear, CurYear;
        int i;
        DojYear = System.DateTime.Now.Year;
        CurYear = System.DateTime.Now.Year;


        ////Opening Balance 
        strsql = "select LeaveField, LeaveCode from tblleavemaster where  upper(Leavecode)<>'MTL' and upper(LeaveCode)<>'PTL' and CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "'   ";
        qry = strsql;
        dr = cn.Execute_Reader(strsql);
        sb.Append("Select '' as Account, ");
        while (dr.Read())
        {
            LvName = dr["LeaveCode"].ToString().Trim();
            sb.Append("isnull(" + dr["LeaveField"].ToString().Trim());
            sb.Append("_Add , 0) As " + LvName + " ");
            sb.Append(",");
            Qry_Flag = 1;
        }
        dr.Close();

        if (Qry_Flag == 1)  //Checks if the Query has been Made or not 
        {

            ColName = sb.ToString().Substring(0, sb.Length - 1);
            ColName += " From tblleaveledger where paycode='" + ViewState["paycode"].ToString() + "' and Lyear='" + dt_FinancialYear.Year + "'   and SSN='" + ViewState["SSN"].ToString().Trim() + "'";

            strsql = ColName;
            DataSet ds = new DataSet();
            ds = cn.FillDataSet(strsql);

            //DataRow drow  = ds.Tables[0].NewRow();
            DataRow drow;

            if (ds.Tables[0].Rows.Count <= 0)
            {
                drow = ds.Tables[0].NewRow();
                ds.Tables[0].Rows.Add(drow);
            }
            drow = ds.Tables[0].NewRow();

            strsql = qry;
            sb.Remove(0, sb.Length);
            dr = cn.Execute_Reader(strsql);
            sb.Append("Select '' , ");
            cnt = 0;
            while (dr.Read())
            {
                cnt++;
                LvName = dr["LeaveCode"].ToString().Trim();
                sb.Append(" isnull(" + dr["LeaveField"].ToString().Trim());
                sb.Append(" , 0) As " + LvName + " ");
                sb.Append(",");
                Qry_Flag = 1;
            }
            dr.Close();
            ColName = sb.ToString().Substring(0, sb.Length - 1);
            ColName += " From tblleaveledger where paycode='" + ViewState["paycode"].ToString() + "' and Lyear='" + dt_FinancialYear.Year + "'  and SSN='" + ViewState["SSN"].ToString().Trim() + "'";
            strsql = ColName;

            dr = cn.Execute_Reader(strsql);
            if (dr.Read())
            {
                for (i = 0; i <= cnt; i++)
                {
                    drow[i] = dr[i].ToString();
                }
            }
            dr.Close();

            ds.Tables[0].Rows.Add(drow);

            //// current balance
            drow = ds.Tables[0].NewRow();
            strsql = qry;
            sb.Remove(0, sb.Length);
            dr = cn.Execute_Reader(strsql);
            sb.Append("Select '' , ");
            cnt = 0;
            while (dr.Read())
            {
                cnt++;
                LvName = dr["LeaveCode"].ToString().Trim();
                if (LvName.ToString().Trim() == "COF")
                {
                    //    sb.Append("abs(isnull(" + dr["LeaveField"].ToString().Trim());
                    //    sb.Append("_Add , 0) - isnull( ");
                    //    sb.Append(dr[0].ToString());
                    //    sb.Append(" , 0)) As " + LvName + " ");

                    sb.Append("0 As " + LvName + " ");
                }
                else
                {
                    sb.Append("isnull(" + dr["LeaveField"].ToString().Trim());
                    sb.Append("_Add , 0) - isnull( ");
                    sb.Append(dr[0].ToString());
                    sb.Append(" , 0) As " + LvName + " ");
                }
                sb.Append(",");
                Qry_Flag = 1;
            }
            dr.Close();
            ColName = sb.ToString().Substring(0, sb.Length - 1);
            ColName += " From tblleaveledger where paycode='" + ViewState["paycode"].ToString() + "' and Lyear='" + dt_FinancialYear.Year + "'  and SSN='" + ViewState["SSN"].ToString().Trim() + "'";
            strsql = ColName;
            //Response.Write(strsql.ToString());
            dr = cn.Execute_Reader(strsql);
            if (dr.Read())
            {
                for (i = 0; i <= cnt; i++)
                {
                    drow[i] = dr[i].ToString();
                }
            }
            dr.Close();

            ds.Tables[0].Rows.Add(drow);

            ds.Tables[0].Rows[0][0] = "Opening Balance";
            ds.Tables[0].Rows[1][0] = "Consumed Leaves";
            ds.Tables[0].Rows[2][0] = "Current Balance";

            LeaveGrid.DataSource = ds;
            LeaveGrid.DataBind();
        }
    }
    private void SetName()
    {
        try
        {
            string Qry = "select Empname,SSN from tblemployee where paycode='" + ViewState["paycode"].ToString() + "' and Companycode='"+Session["LoginCompany"].ToString().Trim().Trim()+"'";
            OleDbDataReader dr;
            if (Connection.State == 0)
                Connection.Open();
            OleDbCommand NameCmd = new OleDbCommand(Qry, Connection);
            dr = NameCmd.ExecuteReader();
            if (dr.Read())
                LblName.Text = dr[0].ToString();
                ViewState["SSN"] =dr[1].ToString();
            dr.Close();
            Connection.Close();
        }
        catch (Exception er)
        {
            Error_Occured("SetName", er.Message);
        }
    }
    private void InitializeComponent()
    {


    }
    #endregion

    private void ChkBackDate()
    {
        try
        {
            
            string PCode;
            DateTime dt = System.DateTime.Now.Date;
            string ddt = dt.ToString("yyyy-MM-dd");
            DataSet DsBackDate = new DataSet();
            PCode = txtpaycode.Text.Trim();

            date1 = DateTime.ParseExact(txtfromdate.Value.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            date2 = DateTime.ParseExact(txttodate.Value.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            date2 = date2.AddDays(1);
            strsql = "Select * from TblTimeRegister where paycode ='" + PCode.ToString() + "' and ";
            strsql += " DateOffice between '" + date2.ToString("yyyy-MM-dd") + "' and  '" + ddt.ToString() + "'  and SSN='" + ViewState["SSN"].ToString().Trim() + "' ";

            DsBackDate = cn.FillDataSet(strsql);
            DayCnt = 0;
            string Status;
            int PrsntValue;
            for (int xx = DsBackDate.Tables[0].Rows.Count - 1; xx >= 0; xx--)
            {
                
                PrsntValue = Convert.ToInt32(DsBackDate.Tables[0].Rows[xx]["PresentValue"].ToString());
                if (PrsntValue > 0)
                {
                    DayCnt++;
                }
            }
            
        }
        catch (Exception er)
        {
            Error_Occured("ChkBackDate", er.Message);
        }
    }
    protected void txtfromdate_DateChanged(object sender, EventArgs e)
    {
        txttodate.Text = txtfromdate.Text;
        txttodate.Focus();
        bindLeaveGrid();
    }
    protected void DDLREASON_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["lt"] = DDLREASON.SelectedItem.Text;
        ViewState["lc"] = DDLREASON.SelectedItem.Value.ToString();
         if (DDLREASON.SelectedItem.Text.Substring(0, 3).Trim().ToUpper() == "COF")
        {
            ViewState["IsSL"] = "N";
            txttodate.Text = txtfromdate.Text;
            txttodate.Enabled = true;
        }
        else
        {
            
            ViewState["IsSL"] = "N";
            txttodate.Enabled = true;
        }
        string s = DDLREASON.SelectedItem.Text.Substring(0, 3).Trim().ToUpper();
        
    }
    public void bindLeaveGrid()
    {
        StringBuilder sb = new StringBuilder();
        string LvName, strsql, ColName = "";
        int Qry_Flag = 0;
        int cnt = 0;
        LvName = "";
        string qry;
        int DojYear, CurYear;
        int i;
        DojYear = System.DateTime.Now.Year;
        CurYear = System.DateTime.Now.Year;
        try
        {
            date1 = DateTime.ParseExact(txttodate.Value.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
        }
        catch (Exception ex)
        {
            return;
        }
        dt_FinancialYear = date1;
        if (ConfigurationManager.AppSettings["isFinancialYear"].ToString() == "Y")
        {
            if (dt_FinancialYear.Month == 1 || dt_FinancialYear.Month == 2 || dt_FinancialYear.Month == 3)
            {
                dt_FinancialYear = dt_FinancialYear.AddYears(-1);
            }
        }
        strsql = "select LeaveField, LeaveCode from tblleavemaster where   upper(Leavecode)<>'MTL' and upper(LeaveCode)<>'PTL'  and CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "'  ";
        qry = strsql;
        dr = cn.Execute_Reader(strsql);
        sb.Append("Select '' as Account, ");
        while (dr.Read())
        {
            LvName = dr["LeaveCode"].ToString().Trim();
            sb.Append("isnull(" + dr["LeaveField"].ToString().Trim());
            sb.Append("_Add , 0) As " + LvName + " ");
            sb.Append(",");
            Qry_Flag = 1;
        }
        dr.Close();

        if (Qry_Flag == 1)  //Checks if the Query has been Made or not 
        {

            ColName = sb.ToString().Substring(0, sb.Length - 1);
            ColName += " From tblleaveledger where paycode='" + ViewState["paycode"].ToString() + "' and Lyear='" + dt_FinancialYear.Year + "' and SSN='" + ViewState["SSN"].ToString() + "' ";

            strsql = ColName;
            DataSet ds = new DataSet();
            ds = cn.FillDataSet(strsql);

            //DataRow drow  = ds.Tables[0].NewRow();
            DataRow drow;

            if (ds.Tables[0].Rows.Count <= 0)
            {
                drow = ds.Tables[0].NewRow();
                ds.Tables[0].Rows.Add(drow);
            }
            //// CONSUMED LEAVES 

            drow = ds.Tables[0].NewRow();

            strsql = qry;
            sb.Remove(0, sb.Length);
            dr = cn.Execute_Reader(strsql);
            sb.Append("Select '' , ");
            cnt = 0;
            while (dr.Read())
            {
                cnt++;
                LvName = dr["LeaveCode"].ToString().Trim();
                sb.Append(" isnull(" + dr["LeaveField"].ToString().Trim());
                sb.Append(" , 0) As " + LvName + " ");
                sb.Append(",");
                Qry_Flag = 1;
            }
            dr.Close();
            ColName = sb.ToString().Substring(0, sb.Length - 1);
            ColName += " From tblleaveledger where paycode='" + ViewState["paycode"].ToString() + "' and Lyear='" + dt_FinancialYear.Year + "' and SSN='" + ViewState["SSN"].ToString() + "' ";
            strsql = ColName;

            dr = cn.Execute_Reader(strsql);
            if (dr.Read())
            {
                for (i = 0; i <= cnt; i++)
                {
                    drow[i] = dr[i].ToString();
                }
            }
            dr.Close();

            ds.Tables[0].Rows.Add(drow);

            //// current balance
            drow = ds.Tables[0].NewRow();
            strsql = qry;
            sb.Remove(0, sb.Length);
            dr = cn.Execute_Reader(strsql);
            sb.Append("Select '' , ");
            cnt = 0;
            while (dr.Read())
            {
                cnt++;
                LvName = dr["LeaveCode"].ToString().Trim();
                if (LvName.ToString().Trim() == "COF")
                {
                    sb.Append("0 As " + LvName + " ");
                }
                else
                {
                    sb.Append("isnull(" + dr["LeaveField"].ToString().Trim());
                    sb.Append("_Add , 0) - isnull( ");
                    sb.Append(dr[0].ToString());
                    sb.Append(" , 0) As " + LvName + " ");
                }
                sb.Append(",");
                Qry_Flag = 1;
            }
            dr.Close();
            ColName = sb.ToString().Substring(0, sb.Length - 1);
            ColName += " From tblleaveledger where paycode='" + ViewState["paycode"].ToString() + "' and Lyear='" + dt_FinancialYear.Year + "' and SSN='" + ViewState["SSN"].ToString() + "'";
            strsql = ColName;
            //Response.Write(strsql.ToString());
            dr = cn.Execute_Reader(strsql);
            if (dr.Read())
            {
                for (i = 0; i <= cnt; i++)
                {
                    drow[i] = dr[i].ToString();
                }
            }
            dr.Close();

            ds.Tables[0].Rows.Add(drow);

            ds.Tables[0].Rows[0][0] = "Opening Balance";
            ds.Tables[0].Rows[1][0] = "Consumed Leaves";
            ds.Tables[0].Rows[2][0] = "Current Balance";

            LeaveGrid.DataSource = ds;
            LeaveGrid.DataBind();
        }
    }

    protected void Run_Procedure_New()
    {
        try
        {
            Module_UPD UPD = new Module_UPD();
            string lremarks = "";
            if (TXTREMARKS.Text.Contains("'"))
            {
                lremarks = TXTREMARKS.Text.Trim().ToString().Replace("'", "");
            }
            else
            {
                lremarks = TXTREMARKS.Text.Trim().ToString();
            }

            DataSet dsResult = new DataSet();
            DateTime dt1, dt2;
            string valid = "", isoff = "", ishld = "", lvtype = "", mvoucher = "";
            int voucher;
            dt1 = date1;
            dt2 = date2;
            string hday = "", Holiday = "", lvcodeTm = "";
            string FirstHalfLvCode = "";
            string LvType1 = "";
            string SecondHalfLvCode = "";
            string LvType2 = "";
            double lvamount = 0;
            double lvamount1 = 0;
            double lvamount2 = 0;
            string Pcode = "";
            Pcode = ViewState["paycode"].ToString().Trim();
            DataSet dsD = new DataSet();
            string ShiftAttended = "";
            string OldStatus = "";
            int result = 0;
            while (dt1 <= dt2)
            {
                valid = "Y";
                strsql = "select voucherno from tblvoucherno where CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "'  ";
                dr = cn.Execute_Reader(strsql);
                if (dr.Read())
                {
                    voucher = int.Parse(dr["voucherno"].ToString()) + 1;
                    mvoucher = voucher.ToString("0000000000");
                }
                else
                {
                    mvoucher = "0000000001";
                    strsql = "insert into tblvoucherno values ('" + mvoucher + "','"+Session["LoginCompany"].ToString().Trim().Trim()+"') ";
                    cn.execute_NonQuery(strsql);
                }
                dr.Close();
                strsql = "update tblvoucherno set voucherno='" + mvoucher + "' where CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "'";
                cn.execute_NonQuery(strsql);


                strsql = "select * from tblleavemaster where leavecode='" + LeaveName.ToString().Trim() + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "' ";
                dr = cn.ExecuteReader(strsql);

                if (dr.Read())
                {
                    isoff = dr["ISOFFINCLUDE"].ToString();
                    ishld = dr["ISHOLIDAYINCLUDE"].ToString();
                    lvtype = dr["LeaveType"].ToString();

                }
                dr.Close();
                strsql = "select * from holiday where hdate='" + dt1.ToString("yyyy-MM-dd") + "' and companycode=(select companycode from tblemployee where paycode='" + ViewState["paycode"].ToString().Trim() + "') " +
                                        " and departmentcode=(select departmentcode from tblemployee where paycode='" + ViewState["paycode"].ToString().Trim() + "')";
                dr = cn.ExecuteReader(strsql);
                if (dr.Read())
                {
                    Holiday = "Y";
                    if (ishld.ToString().Trim() == "Y")
                        valid = "Y";
                    else
                        valid = "N";
                }
                dr.Close();

                strsql = "select halfday,LeaveCode from leave_request where application_no='" + appno.ToString() + "'";
                dr = cn.Execute_Reader(strsql);
                dr.Read();

                hday = dr["halfday"].ToString().Trim();
                lvcodeTm = dr["LeaveCode"].ToString().Trim();
                dr.Close();


                strsql = "Select * From tblTimeRegister  Where Paycode ='" + Pcode.ToString() + "' And DateOffice='" + dt1.ToString("yyyy-MM-dd") + "' and SSN='" + ViewState["SSN"].ToString() + "'";
                dr = cn.ExecuteReader(strsql);
                if (dr.Read())
                {
                    OldStatus = dr["in1"].ToString();
                    ShiftAttended = dr["SHIFTATTENDED"].ToString();
                    if (dr["shift"].ToString() == "OFF")
                    {
                        if (isoff == "Y")
                        {
                            valid = "Y";
                        }
                        else
                        {
                            valid = "N";
                        }
                    }
                }
                dr.Close();
                if (valid == "Y")
                {
                    try
                    {
                        if (hday.ToString().Trim() == "N")
                        {
                            lvamount = 1;
                        }
                        else if (hday.ToString().Trim() == "F")
                        {
                            lvamount = 0.5;
                            lvamount1 = 0.5;
                        }
                        else
                        {
                            lvamount = 0.5;
                            lvamount2 = 0.5;
                        }
                        strsql = "";
                        if (hday.ToString().Trim() == "N")
                        {
                            FirstHalfLvCode = null;
                            LvType1 = null;
                            SecondHalfLvCode = null;
                            LvType2 = null;
                            lvamount = 1;

                        }
                        else if (hday.ToString().Trim() == "F")
                        {
                            strsql = "select isnull(SECONDHALFLEAVECODE,''),LeaveType2 from tbltimeregister where paycode='" + Pcode.ToString().Trim() + "' and dateoffice='" + dt1.ToString("yyyy-MM-dd") + "' and SSN='" + ViewState["SSN"].ToString() + "' ";
                            dsD = cn.FillDataSet(strsql);
                            if (dsD.Tables[0].Rows.Count > 0)
                            {
                                if (dsD.Tables[0].Rows[0][0].ToString().Trim() != "")
                                {
                                    FirstHalfLvCode = lvcodeTm.ToString().Trim();
                                    LvType1 = lvtype.ToString().Trim();
                                    SecondHalfLvCode = dsD.Tables[0].Rows[0][0].ToString().Trim();
                                    LvType2 = dsD.Tables[0].Rows[0][1].ToString().Trim();
                                    lvamount = 1;
                                }
                            }
                        }
                        else if (hday.ToString().Trim() == "S")
                        {

                            strsql = "select isnull(FIRSTHALFLEAVECODE,''),LeaveType1 from tbltimeregister where paycode='" + Pcode.ToString().Trim() + "' and dateoffice='" + dt1.ToString("yyyy-MM-dd") + "' and SSN='" + ViewState["SSN"].ToString() + "'";
                            dsD = cn.FillDataSet(strsql);
                            if (dsD.Tables[0].Rows.Count > 0)
                            {
                                if (dsD.Tables[0].Rows[0][0].ToString().Trim() != "")
                                {
                                    FirstHalfLvCode = dsD.Tables[0].Rows[0][0].ToString().Trim();
                                    LvType1 = dsD.Tables[0].Rows[0][1].ToString().Trim();
                                    SecondHalfLvCode = lvcodeTm.ToString().Trim();
                                    LvType2 = lvtype.ToString().Trim();
                                    lvamount = 1;
                                }
                            }
                        }
                        if (hday.ToString().Trim() == "N")
                        {
                            strsql = "Update tblTimeRegister Set LeaveCode='" + lvcodeTm.ToString() + "',LeaveType='" + lvtype.ToString() + "',LeaveAmount=" + lvamount + ",LeaveAprDate=getdate(),Voucher_No='" + mvoucher.ToString() + "',Reason='" + lremarks.ToString() + "' Where paycode='" + Pcode.ToString().Trim() + "' and dateoffice='" + dt1.ToString("yyyy-MM-dd") + "' and SSN='" + ViewState["SSN"].ToString() + "' ";
                        }
                        else if (hday.ToString().Trim() == "F")
                        {
                            strsql = "Update tblTimeRegister Set FIRSTHALFLEAVECODE='" + lvcodeTm.ToString() + "', LeaveCode='" + lvcodeTm.ToString() + "',LeaveType1='" + lvtype.ToString() + "',LeaveType='" + lvtype.ToString() + "',LeaveAmount=" + lvamount + ",LeaveAmount1=" + lvamount1 + ",LeaveAprDate=getdate(),Voucher_No='" + mvoucher.ToString() + "',Reason='" + lremarks.ToString() + "' Where paycode='" + Pcode.ToString().Trim() + "' and dateoffice='" + dt1.ToString("yyyy-MM-dd") + "'  and SSN='" + ViewState["SSN"].ToString() + "'";
                        }
                        else if (hday.ToString().Trim() == "S")
                        {
                            strsql = "Update tblTimeRegister Set SECONDHALFLEAVECODE='" + lvcodeTm.ToString() + "', LeaveCode='" + lvcodeTm.ToString() + "',LeaveType2='" + lvtype.ToString() + "',LeaveType='" + lvtype.ToString() + "',LeaveAmount=" + lvamount + ",LeaveAmount2=" + lvamount2 + ",LeaveAprDate=getdate(),Voucher_No='" + mvoucher.ToString() + "',Reason='" + lremarks.ToString() + "' Where paycode='" + Pcode.ToString().Trim() + "' and dateoffice='" + dt1.ToString("yyyy-MM-dd") + "' and SSN='" + ViewState["SSN"].ToString() + "' ";
                        }
                        result = cn.execute_NonQuery(strsql);
                        if (result > 0)
                        {

                            UPD.Upd(Pcode, dt1, lvamount, lvamount1, lvamount2, lvcodeTm, FirstHalfLvCode, SecondHalfLvCode, lvtype, LvType1, LvType2, ShiftAttended, Holiday, OldStatus);
                        }
                        dt1 = dt1.AddDays(1);
                    }
                    catch (Exception ex)
                    {
                        dt1 = dt1.AddDays(1);
                    }
                }
                else
                {
                    dt1 = dt1.AddDays(1);
                }
            }
        }
        catch (Exception er)
        {
            Error_Occured("Run_Procedure", er.Message);
        }
    }



    protected void cmdApply_Click(object sender, EventArgs e)
    {
        try
        {
            ViewState["lt"] = DDLREASON.SelectedItem.Text;
            ViewState["lc"] = DDLREASON.SelectedItem.Value.ToString();

            DateTime dateFrom;
            DateTime dateTo;
            string Msg = "";
            try
            {
                date1 = Convert.ToDateTime(txtfromdate.Value.ToString());// DateTime.ParseExact(txtfromdate.Value.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                date2 = Convert.ToDateTime(txttodate.Value.ToString()); ;// DateTime.ParseExact(txttodate.Value.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                Msg = "<script lanaguage='javascript'>alert('Invalid date format. ')</script>";
                Page.RegisterStartupScript("msg", Msg);
                return;
            }
            dt_FinancialYear = date1;
            if (ConfigurationManager.AppSettings["isFinancialYear"].ToString() == "Y")
            {
                if (dt_FinancialYear.Month == 1 || dt_FinancialYear.Month == 2 || dt_FinancialYear.Month == 3)
                {
                    dt_FinancialYear = dt_FinancialYear.AddYears(-1);
                }
                FinYear = Convert.ToString(dt_FinancialYear.Year) + "-04-01";
                CurYear = (dt_FinancialYear.Year + 1).ToString() + "-03-31";
                dateFrom = DateTime.ParseExact(FinYear.ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture);
                dateTo = DateTime.ParseExact(CurYear.ToString(), "yyyy-MM-dd", CultureInfo.InvariantCulture);

                if (dateFrom < date1 && dateTo < date2)
                {
                    Msg = "<script lanaguage='javascript'>alert('From Date and To Date must be in same financial year . ')</script>";
                    Page.RegisterStartupScript("msg", Msg);
                    return;
                }
            }

            if (date1 > date2)
            {
                Msg = "<script lanaguage='javascript'>alert('To Date Must Be Greater Than From Date. ')</script>";
                Page.RegisterStartupScript("msg", Msg);
                return;
            }
            if (date1.Year != date2.Year)
            {
                Msg = "<script lanaguage='javascript'>alert('From Date and To Date Must Be In Same Year. ')</script>";
                Page.RegisterStartupScript("msg", Msg);
                return;
            }
            if (ConfigurationManager.AppSettings["ApplyLastYearLeave"].ToString() == "Y")
            {
                DateTime newd = System.DateTime.MinValue;
                strsql = "select convert(varchar(10),dateadd(dd,-1,dateadd(yy,datediff(yy,0,getdate()),0)),103) ";
                ds = cn.FillDataSet(strsql);
                newd = DateTime.ParseExact(ds.Tables[0].Rows[0][0].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                if (newd > date2)
                {
                    Msg = "<script lanaguage='javascript'>alert('Can Not Apply Last Year Leave. ')</script>";
                    Page.RegisterStartupScript("msg", Msg);
                    return;
                }
            }
            if (DDLREASON.SelectedIndex < 0)
            {
                Msg = "<script language='javascript'> alert('No Data Found In Leave Type.')</script>";
                Page.RegisterStartupScript("msg", Msg);
                return;
            }

            OleDbDataReader dr;
            string qry = "";

            int i = Convert.ToInt16(Session["VALIDYEARS"].ToString().IndexOf(date1.Year.ToString("0000")));
            int j = Convert.ToInt16(Session["VALIDYEARS"].ToString().IndexOf(date2.Year.ToString("0000")));

            if (i < 0)
            {
                Msg = "<script lanaguage='javascript'>alert('Invalid year. Roster was not created for the specified Year...')</script>";
                Page.RegisterStartupScript("msg", Msg);
                txtfromdate.Focus();
                return;
            }
            if (j < 0)
            {
                Msg = "<script lanaguage='javascript'>alert('Invalid year. Roster was not created for the specified Year...')</script>";
                Page.RegisterStartupScript("msg", Msg);
                txttodate.Focus();
                return;
            }
            TimeSpan ts1 = date2 - date1;
            days = Convert.ToInt32(ts1.Days) + 1;
            if (radioday.SelectedIndex > 0)
            {
                days = days / 2;
                if (radioday.SelectedIndex == 1)
                    HalfDay = "F";
                else
                    HalfDay = "S";
            }
            else
            {
                HalfDay = "N";
            }

            qry = "Select isnull(smin,0) as Smin, isnull(smax,0) as Smax from tblleavemaster where leavefield='" + ViewState["lc"].ToString() + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "'  ";
            dr = cn.ExecuteReader(qry);
            double minLimit = 0, maxLimit = 0;

            if (dr.Read())
            {
                minLimit = Convert.ToDouble(dr[0].ToString());
                maxLimit = Convert.ToDouble(dr[1].ToString());
            }
            dr.Close();
            if (days < minLimit && maxLimit != 0)
            {
                change = "<script language='JavaScript'>alert('Please apply for Minimum " + minLimit.ToString() + " days for this Leave Type.')</script>";
                Page.RegisterStartupScript("frmchange", change);
                return;
            }
            if (days > maxLimit && maxLimit != 0)
            {
                change = "<script language='JavaScript'>alert('You can apply for Maximum " + maxLimit.ToString() + " days  in one go.')</script>";
                Page.RegisterStartupScript("frmchange", change);
                return;
            }
            qry = "Select isHolidayInclude,Leavetype,IsOffInclude from tblleavemaster where leavefield='" + ViewState["lc"].ToString() + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "'  ";

            dr = cn.ExecuteReader(qry);
            if (dr.Read())
            {
                OleDbDataReader BalDr;
                ViewState["LVType"] = dr["LeaveType"].ToString().Trim();
                if (dr["isHolidayInclude"].ToString() == "N")
                {
                    //Deduct Holidays if they comes inbetween date range
                    qry = "Select count(*) from tblTimeRegister where Holiday_Value>0 And PayCode='" + ViewState["paycode"].ToString() + "' And dateoffice between '" + date1.ToString("yyyy-MM-dd") + "' and '" + date2.ToString("yyyy-MM-dd") + "' and SSN='" + ViewState["SSN"].ToString() + "'  ";
                    BalDr = cn.ExecuteReader(qry);

                    if (BalDr.Read())
                        days = days - Convert.ToDouble(BalDr[0].ToString());

                    BalDr.Close();
                }
                if (dr["IsOffInclude"].ToString() == "N")
                {
                    qry = "Select count(*) from tblTimeRegister where Wo_Value>0 And PayCode='" + ViewState["paycode"].ToString() + "' And dateoffice between '" + date1.ToString("yyyy-MM-dd") + "' and '" + date2.ToString("yyyy-MM-dd") + "' and SSN='" + ViewState["SSN"].ToString() + "'  ";
                    BalDr = cn.ExecuteReader(qry);

                    if (BalDr.Read())
                    {
                        double bal = Convert.ToDouble(BalDr[0].ToString());
                        days = days - bal;
                    }
                    BalDr.Close();
                }
            }
            else
            {
                ViewState["LVType"] = "";
            }
            dr.Close();
            if (date1 > date2)
            {
                change = "<script language='JavaScript'>alert('Date Selection Not Valid.')</script>";
                txttodate.Focus();
                Page.RegisterStartupScript("frmchange", change);
                return;
            }


            //Leave Not Apply Next Finacial Year
            if (ConfigurationManager.AppSettings["isFinancialYear"].ToString() == "Y")
            {
                if (date1.Month <= 3 && date2.Month > 3)
                {
                    change = "<script language='JavaScript'>alert('Leave From and To date must be in same finacial year.')</script>";
                    Page.RegisterStartupScript("frmchange", change);
                    return;
                }
            }
            if (TXTREMARKS.Text.Trim() == "")
            {
                change = "<script language='JavaScript'>alert('Please give a valid Leave Reason.')</script>";
                Page.RegisterStartupScript("frmchange", change);
                TXTREMARKS.Focus();
                return;
            }
            int len_rem = 0;
            len_rem = Convert.ToInt32(TXTREMARKS.Text.ToString().Length);
            if (len_rem > 70)
            {
                change = "<script language='JavaScript'>alert('Maximum word limit for remarks is 70 characters.')</script>";
                Page.RegisterStartupScript("frmchange", change);
                TXTREMARKS.Focus();
                return;
            }

            string leaveRemarks = "";

            if (TXTREMARKS.Text.Contains("'"))
            {
                leaveRemarks = TXTREMARKS.Text.ToString().Trim().Replace("'", "");
            }
            else
            {
                leaveRemarks = TXTREMARKS.Text.ToString().Trim();
            }

            string str_Qry = "select *  from tblLeaveLedger where paycode = '" + txtpaycode.Text.Trim() + "'  and Lyear= '" + dt_FinancialYear.Year + "' and SSN='" + ViewState["SSN"].ToString() + "'";
            OleDbCommand cmd1 = new OleDbCommand(str_Qry, Connection);
            OleDbDataReader db_Dr;

            db_Dr = cmd1.ExecuteReader();

            if (!db_Dr.Read())
            {
                change = "<script language='JavaScript'>alert('No data found for this year in leave ledger.')</script>";
                Page.RegisterStartupScript("frmchange", change);
                return;
            }
            db_Dr.Close();
            qry = "Select LeaveField,LEAVECODE, isleaveaccrual from tblleavemaster where leavefield='" + ViewState["lc"].ToString() + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "'  ";
            //Response.Write(qry);
            OleDbCommand cmnd = new OleDbCommand(qry, Connection);

            OleDbDataReader drrac;
            OleDbDataReader MPdr;  //Data Reader for Maternity/Paternity Leave
            drrac = cmnd.ExecuteReader();

            drrac.Read();

            string LvField = drrac["LeaveField"].ToString();
            string cmd = "select isnull(" + LvField + ",0)  from tblLeaveLedger where paycode = '" + ViewState["paycode"].ToString() + "'  and Lyear= '" + dt_FinancialYear.Year + "'  and SSN='" + ViewState["SSN"].ToString() + "'";
            if (drrac["LEAVECODE"].ToString().Trim().ToUpper() == "MTL" || drrac["LEAVECODE"].ToString().Trim().ToUpper() == "PTL")
            {
                MPdr = cn.ExecuteReader(cmd);
                MPdr.Read();
                double day = Convert.ToDouble(MPdr[0].ToString());
                MPdr.Close();

                string lf = LvField.Trim().ToUpper();
                if (drrac["LEAVECODE"].ToString().Trim().ToUpper() == "MTL")   //Applied for Maternity Leave
                {
                    if (day > 0) // Already Taken this Leave  
                    {
                        change = "<script language='JavaScript'>alert('This Leave can be applied only once in a year.')</script>";
                        Page.RegisterStartupScript("frmchange", change);
                        return;
                    }
                    if (days > 84)
                    {
                        change = "<script language='JavaScript'>alert('Can not take more than 84 Leaves.')</script>";
                        Page.RegisterStartupScript("frmchange", change);
                        return;
                    }
                }
                else //Applied for Paternity Leave
                {
                    if (day > 0) // Already Taken this Leave  
                    {
                        change = "<script language='JavaScript'>alert('This Leave can be applied only once in a year...')</script>";
                        Page.RegisterStartupScript("frmchange", change);
                        return;
                    }
                    if (days > 3)
                    {
                        change = "<script language='JavaScript'>alert('Can not take more than 3 Leaves')</script>";
                        Page.RegisterStartupScript("frmchange", change);
                        return;
                    }
                }
            }

           
            drrac.Close();
            present = true;
            TimeSpan ts = new System.TimeSpan(date2.Ticks - date1.Ticks);
            LvDeducted = ts.TotalDays + 1;

            string leavecode = "";
            double Balance = 0;

            if (present == true)
            {
                string LvCode = "";

                string change;
                OleDbCommand comm1;
                OleDbDataReader dr1;
                if (RadioLeaveType.SelectedIndex == 0)  //leave selected 
                {
                    if (DDLREASON.Text != "")
                    {
                        
                        int xx;
                        LvCode = ViewState["lt"].ToString().Trim().Substring(0, 2);
                        xx = String.Compare(LvCode.Substring(0, 2), "CL");

                        strsql = "SELECT * FROM LEAVE_REQUEST WHERE ('" + date1.Date.ToString("yyyy-MM-dd") + "' Between LEAVE_FROM And LEAVE_To or '" + date2.Date.ToString("yyyy-MM-dd") + "' Between LEAVE_FROM And LEAVE_To or  LEAVE_FROM BETWEEN '" + date1.Date.ToString("yyyy-MM-dd") + "' AND '" + date2.Date.ToString("yyyy-MM-dd") + "' OR LEAVE_TO BETWEEN '" + date1.Date.ToString("yyyy-MM-dd") + "' AND '" + date2.Date.ToString("yyyy-MM-dd") + "') AND PAYCODE='" + ViewState["paycode"].ToString() + "' and SSN='" + ViewState["SSN"].ToString() + "' AND ((stage1_APPROVED IS NULL or stage1_APPROVED='Y') and (stage2_APPROVED IS NULL or stage2_APPROVED='Y')  ) ";

                        comm1 = new OleDbCommand(strsql, Connection);

                        dr1 = comm1.ExecuteReader();
                        while (dr1.Read())
                        {
                            if (dr1["HalfDay"].ToString().Trim().ToUpper() == "N")
                            {
                                change = "<script language='JavaScript'>alert('Leave already applied for this date range.')</script>";
                                Page.RegisterStartupScript("frmchange", change);
                                dr1.Close();
                                return;
                            }
                            if (dr1["HalfDay"].ToString().Trim().ToUpper() != "N" && radioday.SelectedIndex == 0)
                            {
                                change = "<script language='JavaScript'>alert('Leave already applied for this date range.')</script>";
                                Page.RegisterStartupScript("frmchange", change);
                                dr1.Close();
                                return;
                            }
                            if (dr1["HalfDay"].ToString().Trim().ToUpper() == "F" && radioday.SelectedIndex == 1)
                            {
                                //// check for half day
                                change = "<script language='JavaScript'>alert('First Half Leave already applied for this date range.')</script>";
                                Page.RegisterStartupScript("frmchange", change);
                                dr1.Close();
                                return;
                            }
                            if (dr1["HalfDay"].ToString().Trim().ToUpper() == "S" && radioday.SelectedIndex == 2)
                            {
                                //// check for half day
                                change = "<script language='JavaScript'>alert('Second Half Leave already applied for this date range.')</script>";
                                Page.RegisterStartupScript("frmchange", change);
                                dr1.Close();
                                return;
                            }
                        }

                        int voucher;
                        string mvoucher;
                        strsql = "select isnull(max(voucherno),0) voucherno from tblvoucherno where CompanyCode='"+Session["LoginCompany"].ToString().Trim().Trim()+"' ";
                        dr = cn.ExecuteReader(strsql);

                        if (dr.Read())
                        {
                            voucher = Convert.ToInt32(dr["voucherno"].ToString()) + 1;
                            mvoucher = voucher.ToString("0000000000");
                        }
                        else
                        {
                            mvoucher = "0000000001";
                            strsql = "insert into tblvoucherno values ('" + mvoucher + "','" + Session["LoginCompany"].ToString().Trim().Trim() + "') ";
                            cn.execute_NonQuery(strsql);
                        }
                        dr.Close();

                        //INSERT INTO LEAVE_REQUEST 
                        //oxford university

                        string reqid = Session["username"].ToString();
                        int req_len = Convert.ToInt32(reqid.ToString().Length);
                        if (req_len > 25)
                        {
                            reqid = reqid.Substring(0, 25);
                        }

                        double bal = 0;
                        DataSet dswo = new DataSet();
                        DateTime LWPfrom = System.DateTime.MinValue;
                        DateTime LWPTo = System.DateTime.MinValue;
                        string LWPString = "";
                        LeaveName = DDLREASON.SelectedItem.ToString().Substring(0, 3).Trim();
                        if (ViewState["LVType"].ToString().Trim() == "L")
                        {
                            string lcod = ViewState["lc"].ToString();
                            string lcod_add = ViewState["lc"].ToString() + "_ADD";
                            strsql = "select isnull(" + lcod_add + " ,0) - isnull( " + lcod + " , 0)  as Balance  FROM TBLLEAVELEDGER WHERE PAYCODE ='" + ViewState["paycode"].ToString() + "'  and Lyear= '" + dt_FinancialYear.Year + "' and SSN='" + ViewState["SSN"].ToString() + "'";
                            dr = cn.ExecuteReader(strsql);
                            if (dr.Read())
                                Balance = Convert.ToDouble(dr[0].ToString());
                            dr.Close();

                            //For jabong Customization open the below commented lines
                            /*if (EL_LWP == true)
                            {
                                if (Balance < days)
                                {
                                    bal = days - Balance;
                                    if (bal <= 2)
                                    {
                                        if (bal == 2)
                                        {
                                            strsql = "select Convert(varchar(10),dateoffice,103) 'dt' from tbltimeregister where dateoffice between '" + date1.ToString("yyyy-MM-dd") + "' and '" + date2.ToString("yyyy-MM-dd") + "' and status='WO' and shiftAttended='OFF' and PAYCODE ='" + ViewState["paycode"].ToString() + "' ";
                                            dswo = cn.FillDataSet(strsql);
                                            if (dswo.Tables[0].Rows.Count > 0)
                                            {
                                                LWPfrom = DateTime.ParseExact(dswo.Tables[0].Rows[0]["dt"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                LWPTo = DateTime.ParseExact(dswo.Tables[0].Rows[1]["dt"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                if (Session["usertype"].ToString().Trim() == "A")
                                                {
                                                    strsql = "update tbltimeregister set status='LWP',LeaveCode='LWP',absentvalue=1,wo_value=0,leaveamount=1,leavetype='A' where dateoffice between '" + LWPfrom.ToString("yyyy-MM-dd") + "' and '" + LWPTo.ToString("yyyy-MM-dd") + "' and PAYCODE ='" + ViewState["paycode"].ToString() + "' ";
                                                    cn.execute_NonQuery(strsql);
                                                }
                                            }

                                        }
                                        if (bal == 1)
                                        {
                                            strsql = "select top 1 Convert(varchar(10),dateoffice,103) 'dt' from tbltimeregister where dateoffice between '" + date1.ToString("yyyy-MM-dd") + "' and '" + date2.ToString("yyyy-MM-dd") + "' and status='WO' and shiftAttended='OFF' and PAYCODE ='" + ViewState["paycode"].ToString() + "' ";
                                            dswo = cn.FillDataSet(strsql);
                                            if (dswo.Tables[0].Rows.Count > 0)
                                            {
                                                LWPfrom = DateTime.ParseExact(dswo.Tables[0].Rows[0]["dt"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                                if (Session["usertype"].ToString().Trim() == "A")
                                                {
                                                    strsql = "update tbltimeregister set status='LWP',LeaveCode='LWP',absentvalue=1,wo_value=0,leaveamount=1,leavetype='A' where dateoffice = '" + LWPfrom.ToString("yyyy-MM-dd") + "'  and PAYCODE ='" + ViewState["paycode"].ToString() + "' ";
                                                    cn.execute_NonQuery(strsql);
                                                }
                                            }

                                        }
                                        days = days - bal;
                                    }
                                    
                                }
                                if (isfut==true)
                                {
                                    if (bal == 2)
                                    {   
                                        date1 = DateTime.ParseExact(txtfromdate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        date2 = DateTime.ParseExact(txttodate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }
                                    else if (bal == 1)
                                    {
                                        date1 = DateTime.ParseExact(txtfromdate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);                                        
                                        date2 = DateTime.ParseExact(txttodate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        date2 = date2.AddDays(1);
                                    }
                                }
                                else if (ispast  == true )
                                {
                                    if (bal == 2)
                                    {
                                        date1 = DateTime.ParseExact(txtfromdate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        date2 = DateTime.ParseExact(txttodate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }
                                    else if (bal == 1)
                                    {
                                        date1 = DateTime.ParseExact(txtfromdate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                        date1 = date1.AddDays(-1);
                                        date2 = DateTime.ParseExact(txttodate.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    }
                                }
                            }*/
                            if (Balance <= 0)
                            {
                                qry = "Select isHolidayInclude,IsOffInclude from tblleavemaster where leavefield='" + ViewState["lc"].ToString() + "' And LeaveType='P' and CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "'   ";
                                dr = cn.ExecuteReader(qry);
                                if (dr.Read())
                                { }
                                else
                                {
                                    change = "<script language='JavaScript'>alert('Leave Balance of " + DDLREASON.SelectedItem.Text.ToString().Trim() + " is not sufficient..')</script>";
                                    Page.RegisterStartupScript("frmchange", change);
                                    return;
                                }
                                dr.Close();
                            }
                            if (days > Balance)
                            {
                                qry = "Select isHolidayInclude,IsOffInclude from tblleavemaster where leavefield='" + ViewState["lc"].ToString() + "' And LeaveType='P'  and CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "'  ";
                                dr = cn.ExecuteReader(qry);
                                if (dr.Read())
                                { }
                                else
                                {
                                    change = "<script language='JavaScript'>alert('Not Enough Leave Balance..')</script>";
                                    Page.RegisterStartupScript("frmchange", change);
                                    return;
                                }
                                dr.Close();
                            }
                        }

                        // string strsql;
                        string fromMail = "";
                        string toMail = "";
                        dr1.Close();
                        string hid = "";
                        strsql = "select headid,e_mail1 from tblemployee where paycode='" + txtpaycode.Text.ToString() + "' and SSN='" + ViewState["SSN"].ToString() + "' ";
                        OleDbCommand comm = new OleDbCommand(strsql, Connection);
                        dr = comm.ExecuteReader();
                        if (date1 <= date2)
                        {
                            if (dr.Read())
                            {
                                hid = dr[0].ToString().Trim();
                                fromMail = dr[1].ToString();
                                strsql = "select empname,e_mail1 from tblemployee where paycode='" + hid.ToString().Trim() + "' and SSN='" + ViewState["SSN"].ToString() + "' ";
                                dr = cn.Execute_Reader(strsql);
                                if (dr.Read())
                                {
                                    toMail = dr[1].ToString().Trim();
                                    Session["tomail"] = toMail.ToString();
                                }
                            }
                            else
                            {
                                fromMail = "a@a.com";
                                toMail = "a@a.com";
                                Session["tomail"] = toMail.ToString();
                            }

                            dr.Close();
                            appno = application();
                            SetDate();

                            string Hod = "";
                            bool S1Approve = false, S2Approve = false, Run_Upd = false;
                            int LA_Stages = 1; //LEAVE APPROVAL STAGES  

                            if (Session["usertype"].ToString().ToUpper() == "A")
                            {
                                S2Approve = true;
                            }
                            else
                            {
                                strsql = "select headid from tblemployee where paycode ='" + ViewState["paycode"].ToString() + "' and SSN='" + ViewState["SSN"].ToString() + "'";
                                dr = cn.ExecuteReader(strsql);
                                while (dr.Read())
                                    Hod = dr[0].ToString();
                                dr.Close();
                                if (Hod.Trim() != "")
                                {
                                    if (Hod.ToString().Trim() == Session["PAYCODE"].ToString().Trim())
                                        S1Approve = true;
                                }
                                strsql = "select PAYCODE from tblemployee where PayCode = Headid";
                                dr = cn.ExecuteReader(strsql);
                                while (dr.Read())  //Find if he/she is Heaid of him/her self
                                {
                                    if (dr[0].ToString().Trim() == ViewState["paycode"].ToString().Trim())
                                        S2Approve = true;
                                }
                                dr.Close();

                                //CHECK FOR LEAVE APPROVAL STAGES FOR THE APPLIED PAYCODE
                                strsql = "select isnull(LeaveApprovalStages,0) 'LeaveApprovalStages' from TblEmployee where Paycode='" + ViewState["paycode"].ToString().Trim() + "' and SSN='" + ViewState["SSN"].ToString() + "' ";
                                dr = cn.ExecuteReader(strsql);

                                if (dr.Read())
                                {
                                    LA_Stages = Convert.ToInt32(dr[0].ToString());
                                }
                                if (LA_Stages == 0)
                                {
                                    change = "<script language='JavaScript'>alert('Leave Approval Stage is not set for this Employee.')</script>";
                                    Page.RegisterStartupScript("frmchange", change);
                                    dr.Close();
                                    return;
                                }
                                dr.Close();
                            }
                            // FIND OUT VOUCHER NUMBER 

                            strsql = "select isnull(max(voucherno),0) voucherno from tblvoucherno where CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "' ";
                            dr = cn.ExecuteReader(strsql);

                            if (dr.Read())
                            {
                                voucher = Convert.ToInt32(dr["voucherno"].ToString()) + 1;
                                mvoucher = voucher.ToString("0000000000");
                            }
                            else
                            {
                                mvoucher = "0000000001";
                            }
                            dr.Close();
                            dt = System.DateTime.Today;
                            if (S2Approve)  // approve at both levels and run upd for the number of days 
                            {
                                Run_Upd = true;
                                if (Session["usertype"].ToString().ToUpper() == "A")
                                {
                                    strsql = "insert into leave_request(application_no,VoucherNo,paycode,request_id,request_date,leave_from,leave_to,leavecode,halfday,leavedays,userremarks,chklbl1,Stage1_Approved, Stage1_Approval_Id, Stage1_Approval_Date, Stage1_Approval_Remarks, Stage2_Approved, Stage2_Approval_Id, Stage2_Approval_Date, Stage2_Approval_Remarks,SSN) values (";
                                    strsql = strsql + appno + ",'" + mvoucher + "', '" + ViewState["paycode"].ToString() + "','" + reqid.ToString() + "','" + dt.ToString("yyyy-MM-dd") + "','" + date1.Date.ToString("yyyy-MM-dd") + "','" + date2.Date.ToString("yyyy-MM-dd") + "', ";
                                    strsql += " '" + LeaveName.ToString() + "','" + HalfDay + "'," + days + ",'" + leaveRemarks.ToString() + "','N', 'Y','" + Session["PAYCODE"].ToString() + "', '" + dt.ToString("yyyy-MM-dd") + "', 'Applied And Approved by Admin','Y','" + Session["PAYCODE"].ToString() + "', '" + dt.ToString("yyyy-MM-dd") + "', 'Applied And Approved by Admin','" + ViewState["SSN"].ToString() + "') ";
                                }
                                else
                                {
                                    strsql = "insert into leave_request(application_no,VoucherNo,paycode,request_id,request_date,leave_from,leave_to,leavecode,halfday,leavedays,userremarks,chklbl1,Stage1_Approved, Stage1_Approval_Id, Stage1_Approval_Date, Stage1_Approval_Remarks, Stage2_Approved, Stage2_Approval_Id, Stage2_Approval_Date, Stage2_Approval_Remarks,SSN) values (";
                                    strsql = strsql + appno + ",'" + mvoucher + "', '" + ViewState["paycode"].ToString() + "','" + reqid.ToString() + "','" + dt.ToString("yyyy-MM-dd") + "','" + date1.Date.ToString("yyyy-MM-dd") + "','" + date2.Date.ToString("yyyy-MM-dd") + "', ";
                                    strsql += " '" + LeaveName.ToString() + "','" + HalfDay + "'," + days + ",'" + leaveRemarks.ToString() + "','N', 'Y','" + Session["PAYCODE"].ToString() + "', '" + dt.ToString("yyyy-MM-dd") + "', 'Approved','Y','" + Session["PAYCODE"].ToString() + "', '" + dt.ToString("yyyy-MM-dd") + "', 'Approved','" + ViewState["SSN"].ToString() + "') ";
                                }
                            }
                            else if (S1Approve && LA_Stages == 1)   // approve at both levels and run upd for the number of days 
                            {
                                Run_Upd = true;
                                strsql = "insert into leave_request(application_no,VoucherNo,paycode,request_id,request_date,leave_from,leave_to,leavecode,halfday,leavedays,userremarks,chklbl1,Stage1_Approved, Stage1_Approval_Id, Stage1_Approval_Date, Stage1_Approval_Remarks ,     Stage2_Approved, Stage2_Approval_Id, Stage2_Approval_Date, Stage2_Approval_Remarks,SSN) values (";
                                strsql = strsql + appno + ",'" + mvoucher + "', '" + ViewState["paycode"].ToString() + "','" + reqid.ToString() + "','" + dt.ToString("yyyy-MM-dd") + "','" + date1.Date.ToString("yyyy-MM-dd") + "','" + date2.Date.ToString("yyyy-MM-dd") + "', ";
                                strsql += " '" + LeaveName.ToString() + "','" + HalfDay + "'," + days + ",'" + leaveRemarks.ToString() + "','N', 'Y','" + Session["username"].ToString() + "', '" + dt.ToString("yyyy-MM-dd") + "', 'Approved','Y','" + Session["username"].ToString() + "', '" + dt.ToString("yyyy-MM-dd") + "', 'Approved' ,'" + ViewState["SSN"].ToString() + "') ";
                            }
                            else if (S1Approve)
                            {
                                strsql = "insert into leave_request(application_no,VoucherNo,paycode,request_id,request_date,leave_from,leave_to,leavecode,halfday,leavedays,userremarks,chklbl1,Stage1_Approved, Stage1_Approval_Id, Stage1_Approval_Date, Stage1_Approval_Remarks,SSN) values (";
                                strsql = strsql + appno + ",'" + mvoucher + "', '" + ViewState["paycode"].ToString() + "','" + reqid.ToString() + "','" + dt.ToString("yyyy-MM-dd") + "','" + date1.Date.ToString("yyyy-MM-dd") + "','" + date2.Date.ToString("yyyy-MM-dd") + "', ";
                                strsql += " '" + LeaveName.ToString() + "','" + HalfDay + "'," + days + ",'" + leaveRemarks.ToString() + "','N','Y','" + Session["username"].ToString() + "', '" + dt.ToString("yyyy-MM-dd") + "', 'Approved','" + ViewState["SSN"].ToString() + "' ) ";
                            }
                            else
                            {
                                strsql = "insert into leave_request(application_no,VoucherNo,paycode,request_id,request_date,leave_from,leave_to,leavecode,halfday,leavedays,userremarks,chklbl1,SSN) values (" + appno.ToString() + ",'" + mvoucher.ToString() + "', '" + ViewState["paycode"].ToString() + "','" + reqid.ToString() + "','" + dt.ToString("yyyy-MM-dd") + "','" + date1.Date.ToString("yyyy-MM-dd") + "','" + date2.Date.ToString("yyyy-MM-dd") + "','" + LeaveName.ToString() + "','" + HalfDay + "'," + days + ",'" + leaveRemarks.ToString() + "','N','" + ViewState["SSN"].ToString() + "')";
                            }

                            OleDbCommand leavecomm = new OleDbCommand(strsql, Connection);
                            leavecomm.ExecuteNonQuery();

                            //Log

                            try
                            {
                                ec.InsertLog("Leave Applied", "For " + ViewState["paycode"].ToString().Trim(), "", Session["LoginUserName"].ToString().Trim(), Session["LoginCompany"].ToString().Trim().Trim());
                            }
                            catch
                            {

                            }


                            // Update TblLeaveLedger
                            strsql = "update TBLLEAVELEDGER SET " + LvField.ToString() + " = IsNull(" + LvField.ToString() + ",0) + " + days + "  ";
                            strsql += " where  paycode='" + ViewState["paycode"].ToString() + "' and lyear='" + dt_FinancialYear.Year + "' and SSN='" + ViewState["SSN"].ToString() + "'";
                            cn.execute_NonQuery(strsql);
                            ViewState["LeaveDays"] = days;
                            try
                            {
                                if (ConfigurationManager.AppSettings["MailByDownloader"].ToString() == "Y")
                                {
                                    Set_Matter_ByDownloader(ViewState["paycode"].ToString().Trim());
                                }
                                else
                                {
                                    Set_Matter(ViewState["paycode"].ToString().Trim());
                                }

                                Set_Matter_Applicant(ViewState["paycode"].ToString().Trim());
                            }
                            catch
                            {

                            }
                            updateVoucher(mvoucher.ToString());
                            if (Run_Upd)
                            {
                                //Run_Procedure();
                                Run_Procedure_New();
                            }
                        }
                        else
                        {
                            lblcompdate.Visible = true;
                        }
                    }
                }
                else   // Holiday Selected 
                {
                    date1 = new DateTime(int.Parse(LblDt.Text.ToString().Substring(6, 4)), int.Parse(LblDt.Text.ToString().Substring(3, 2)), int.Parse(LblDt.Text.ToString().Substring(0, 2)));
                    //Checking for Holiday Balance 
                    string dat1, dat2;
                    dat1 = dt_FinancialYear.Year.ToString() + "-04-01";
                    dat2 = (dt_FinancialYear.AddYears(1)).Year.ToString() + "-03-31";

                    strsql = "select PAYCODE , HOLIDAYS, ";
                    strsql += " ( SELECT COUNT(*) FROM TBLEMPHOLIDAY where paycode ='" + ViewState["paycode"].ToString() + "' and Hdate Between '" + dat1 + "' And '" + dat2 + "' )  TAKEN from TblLeaveLedger where paycode ='" + ViewState["paycode"].ToString() + "' ";
                    strsql += " And Lyear='" + dt_FinancialYear.Year + "'";

                    comm1 = new OleDbCommand(strsql, Connection);
                    dr1 = comm1.ExecuteReader();

                    if (dr1.Read())
                    {
                        //HOLIDAY BALANCE REMOVED FOR SOME TIME 
                        if (Convert.ToInt32(dr1[1].ToString()) > Convert.ToInt32(dr1[2].ToString()))
                        {
                            strsql = "SELECT * FROM LEAVE_REQUEST WHERE '" + date1.Date.ToString("yyyy-MM-dd") + "' BETWEEN LEAVE_FROM AND LEAVE_TO AND PAYCODE='" + ViewState["paycode"].ToString() + "' AND (stage1_APPROVED IS NULL or stage1_APPROVED='Y') and (stage2_APPROVED IS NULL or stage2_APPROVED='Y') ";
                            comm1 = new OleDbCommand(strsql, Connection);
                            dr1 = comm1.ExecuteReader();
                            if (dr1.Read())
                            {
                                change = "<script language='JavaScript'>alert('Leave allready applied for this date range...')</script>";
                                Page.RegisterStartupScript("frmchange", change);
                                dr1.Close();
                            }
                            else
                            {
                                dr1.Close();
                                string fromMail = "";
                                string toMail = "";
                                dr1.Close();
                                string hid = "";
                                strsql = "select headid,e_mail1 from tblemployee where paycode='" + txtpaycode.Text.ToString() + "' ";
                                OleDbCommand comm = new OleDbCommand(strsql, Connection);
                                dr = comm.ExecuteReader();

                                if (dr.Read())
                                {
                                    hid = dr[0].ToString().Trim();
                                    fromMail = dr[1].ToString();
                                    strsql = "select empname,e_mail1 from tblemployee where paycode='" + hid.ToString().Trim() + "' ";
                                    dr = cn.Execute_Reader(strsql);
                                    if (dr.Read())
                                    {
                                        toMail = dr[1].ToString().Trim();
                                        Session["tomail"] = toMail.ToString();
                                    }
                                    appno = application();
                                    SetDate();

                                    LeaveName = "Hld";
                                    int voucher;
                                    string mvoucher;
                                    strsql = "select isnull(max(voucherno),0) voucherno from tblvoucherno";
                                    dr = cn.ExecuteReader(strsql);

                                    if (dr.Read())
                                    {
                                        voucher = Convert.ToInt32(dr["voucherno"].ToString()) + 1;
                                        mvoucher = voucher.ToString("0000000000");
                                    }
                                    else
                                    {
                                        mvoucher = "0000000001";
                                    }
                                    dr.Close();

                                    strsql = "insert into leave_request(application_no,VOUCHERNO,paycode,request_id,request_date,leave_from,leave_to,leavecode,halfday,leavedays,userremarks,chklbl1) values (";
                                    strsql = strsql + appno + ",'" + mvoucher + "','" + ViewState["paycode"].ToString() + "','" + Session["username"].ToString() + "','" + DateTime.Now.ToString("yyyy-MM-dd") + "','" + date1.Date.ToString("yyyy-MM-dd") + "','" + date2.Date.ToString("yyyy-MM-dd") + "','" + LeaveName.ToString() + "','" + HalfDay + "', 1 ,'" + leaveRemarks.ToString().Trim() + "','N')";

                                    OleDbCommand leavecomm = new OleDbCommand(strsql, Connection);
                                    leavecomm.ExecuteNonQuery();
                                    strsql = "insert into TblEmpHoliday (PayCode,Hdate) ";
                                    strsql += " values ( '" + ViewState["paycode"].ToString() + "', '" + date1.Date.ToString("yyyy-MM-dd") + "' )";
                                    leavecomm = new OleDbCommand(strsql, Connection);
                                    leavecomm.ExecuteNonQuery();
                                    try
                                    {
                                        Set_Matter(ViewState["paycode"].ToString().Trim());
                                    }
                                    catch
                                    {

                                    }
                                    
                                }
                                else
                                {
                                    dr.Close();
                                    change = "<script language='JavaScript'>alert('Email or Head Id Not Found')</script>";
                                    Page.RegisterStartupScript("frmchange", change);
                                }
                            }
                            
                        }
                        else
                        {
                            change = "<script language='JavaScript'>alert('Not enough Holiday Balance')</script>";
                            Page.RegisterStartupScript("frmchange", change);
                        }
                    }

                } 

                BindGrid();
                fill_LeaveGrid();
            }
            else
            {
                string change = "<script language='JavaScript'>alert('Employee should be present for Leave Apply...')</script>";
                Page.RegisterStartupScript("frmchange", change);
            }
            RefreshMe();
            
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('" + ex.Message.ToString() + "');", true);
            return;
        }
    }
    private void Set_Matter(string paycode)
    {
        try
        {
            string matter, txt, hid1, frommail, tomail, headname = "";
            hid1 = "";
            frommail = "";
            tomail = "";
            string pc = paycode.ToString();
            string un = "";
            string qry = "select Empname,headid,E_mail1 from tblemployee where paycode='" + pc.ToString() + "' and Active='Y'";
            OleDbDataReader dr;
            if (Connection.State == 0)
                Connection.Open();
            OleDbCommand cmdd = new OleDbCommand(qry, Connection);
            dr = cmdd.ExecuteReader();
            if (dr.Read())
            {
                un = dr[0].ToString();
                hid1 = dr[1].ToString().Trim();
                frommail = dr[2].ToString().Trim();
            }
            dr.Close();
            Connection.Close();
            string[] hname = new string[4];
            if (hid1.ToString().Trim() != "")
            {
                strsql = "select empname,e_mail1 from tblemployee where paycode='" + hid1.ToString().Trim() + "' ";
                dr = cn.Execute_Reader(strsql);
                if (dr.Read())
                {
                    headname = dr[0].ToString().Trim();
                    tomail = dr[1].ToString().Trim();
                }
                else
                    return;
            }
            hname = headname.ToString().Split(' ');
            headname = hname[0].ToString().Trim().ToUpper();

            string lt="";

           
            string message = "";
            StringBuilder sb = new System.Text.StringBuilder();

            sb.Append("<table class='taglist' width='100%' cellpadding='0' cellspacing='0'>");

            sb.Append("<tr >");
            sb.Append("<td style='height:5px'></td>");
            sb.Append("</tr>");

            sb.Append("<tr >");
            sb.Append("<td style='font-family:Verdana;font-size:11px;color:Black;'  colspan='2'><b>");
            sb.Append("Dear " + headname.ToString() + ", ");
            sb.Append("</b></td>");
            sb.Append("</tr >");

            sb.Append("<tr >");
            sb.Append("<td style='height:8px'></td>");
            sb.Append("</tr>");


            sb.Append("<tr >");
            sb.Append("<td style='font-family:Verdana;font-size:10px;color:Black;'  colspan='2'><b>");
            sb.Append("Kindly approve the leave request of your team member " + un.ToString() + " for " + lt.ToString() + " on " + txtfromdate.Text + " to " + txttodate.Text + "  for " + ViewState["LeaveDays"].ToString() + " days. Reason - " + TXTREMARKS.Text.ToString() + ". ");
            sb.Append("</b></td>");
            sb.Append("</tr>");

            sb.Append("<tr >");
            sb.Append("<td style='height:8px'></td>");
            sb.Append("</tr>");

            sb.Append("<tr >");
            sb.Append("<td style='font-family:Verdana;font-size:10px;color:Black;'  colspan='2'><b>");
            sb.Append("Please don’t reply to this e-mail. This is an auto generated e-mail. For any query, please reach out to hr@jabong.com.");
            sb.Append("</b></td>");
            sb.Append("</tr>");

            sb.Append("<tr >");
            sb.Append("<td style='height:8px'></td>");
            sb.Append("</tr>");

            sb.Append("<tr >");
            sb.Append("<td style='font-family:Verdana;font-size:10px;color:Sea Green;'  colspan='2'><b>");
            sb.Append("Regards,</br>Team - HR");
            sb.Append("</b></td>");
            sb.Append("</tr>");

            sb.Append("<tr >");
            sb.Append("<td style='height:8px'></td>");
            sb.Append("</tr>");

            sb.Append("<tr >");
            sb.Append("<td style='font-family:Verdana;font-size:10px;color:Black;' colspan='2'><b>");
            string link = ConfigurationManager.AppSettings["EmailLink"].ToString();
            sb.Append("To approve/Reject the same please click on the link " + link.ToString() + ".");
            sb.Append("</b></td>");
            sb.Append("</tr>");

            sb.Append("<tr >");
            sb.Append("<td style='height:8px'></td>");
            sb.Append("</tr>");

            sb.Append("</table>");
            message = sb.ToString();
            // Response.Write(message.ToString());
            // return;
            bool IsSent = false;
            if (!string.IsNullOrEmpty(tomail.ToString().Trim()))
            {
                if (ViewState["IsSL"].ToString() == "Y")
                {
                    IsSent = SendMail.SendEMailSL(tomail.ToString(), "Leave Application By " + un.Trim(), message, ViewState["uploadfile"].ToString());
                }
                else
                {
                    IsSent = SendMail.SendEMail(tomail.ToString(), "Leave Application By " + un.Trim(), message);
                }
                if (!IsSent)
                {
                    change = "<script language='javascript'>alert('could not sent mail')</script> ";
                    Page.RegisterStartupScript("msg", change);
                    return;
                }
            }
            else
            {
                change = "<script language='JavaScript'>alert('Head Email Addresses not found for this employee')</script>";
                Page.RegisterStartupScript("frmchange", change);
            }
        }
        catch (Exception er)
        {
            Error_Occured("Set_Matter12", er.Message);
        }
    }


    private void Set_Matter_ByDownloader(string paycode)
    {
        try
        {
            string[] sp2;
            string pc = paycode.ToString();
            string un = "", HOD = "", HODName = "", HODEMail = "", FinalDur = "", EmpName = "", Dur = "", FromMail = "", hid1 = "", frommail = "", tomail = "", headname = "";

            string qry = "select emp.paycode,emp.Empname 'Name',isnull(emp.E_Mail1,'') E_Mail1,emp.Headid, " +
                        "emp1.empname 'HODName',emp1.paycode,isnull(emp1.E_Mail1,'') HOD_Mail from tblemployee emp join tblemployee emp1 on emp.headid=emp1.paycode where emp.paycode='" + pc.ToString() + "'";
            OleDbDataReader dr;
            if (Connection.State == 0)
                Connection.Open();
            OleDbCommand cmdd = new OleDbCommand(qry, Connection);

            dr = cmdd.ExecuteReader();
            if (dr.Read())
            {
                un = dr[0].ToString();
                EmpName = dr["Name"].ToString().Trim();
                FromMail = dr["E_Mail1"].ToString().Trim();
                HOD = dr["Headid"].ToString().Trim();
                HODName = dr["HODName"].ToString().Trim();
                HODEMail = dr["HOD_Mail"].ToString().Trim();
            }
            dr.Close();
            Connection.Close();

            if (HOD.ToString().Trim() == "")
                return;

            sp2 = DDLREASON.SelectedItem.Text.Split('-');
            if (radioday.SelectedIndex == 0)
            {
                FinalDur = "N";
                Dur = "Full Day";
            }
            else if (radioday.SelectedIndex == 1)
            {
                FinalDur = "F";
                Dur = "First Half";
            }
            else
            {
                FinalDur = "S";
                Dur = "Second Half";
            }

            if (HODEMail.ToString().Trim() != "")
            {
                strsql = "insert into tblMailContent (AppNo,paycode,empname,FromMail,FromText,FromDate,ToDate,LeaveCode,LvStatus,Remarks,LvDuration,IsCalled,Code) values ('" + appno.ToString() + "', " +
                                   " '" + ViewState["paycode"].ToString() + "','" + EmpName.ToString().Trim() + "', " +
                                   " '" + HODEMail.ToString().Trim() + "','" + HODName.ToString().Trim() + "', " +
                                   " '" + date1.ToString("yyyy-MM-dd") + "','" + date2.ToString("yyyy-MM-dd") + "', " +
                                   " '" + sp2[1].ToString().Trim() + " ( " + sp2[0].ToString().Trim() + " - " + Dur.ToString().Trim() + "  ) '," +
                                   " 'P','" + TXTREMARKS.Text.ToString().Trim().Replace("'", "") + "','" + FinalDur.ToString().Trim() + "','N','" + sp2[0].ToString().Trim() + "')";
                cn.execute_NonQuery(strsql);
            }
        }
        catch (Exception er)
        {
            Error_Occured("Set_Matter12", er.Message);
        }
    }


    private void Set_Matter_Applicant(string paycode)
    {
        try
        {
            return;
            string matter, txt, tomail;
            tomail = "";
            string pc = paycode.ToString();
            string un = "";
            string qry = "select Empname,E_mail1 from tblemployee where paycode='" + pc.ToString() + "' and Active='Y'";
            OleDbDataReader dr;
            if (Connection.State == 0)
                Connection.Open();
            OleDbCommand cmdd = new OleDbCommand(qry, Connection);
            dr = cmdd.ExecuteReader();
            if (dr.Read())
            {
                un = dr[0].ToString();
                tomail = dr[1].ToString().Trim();
            }
            dr.Close();
            Connection.Close();

            string lt;

            
            string message = "";
            StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("<table class='taglist' width='100%' cellpadding='0' cellspacing='0'>");

            sb.Append("<tr >");
            sb.Append("<td style='height:5px'></td>");
            sb.Append("</tr>");

            sb.Append("<tr >");
            sb.Append("<td style='font-family:Verdana;font-size:11px;color:Black;'  colspan='2'><b>");
            sb.Append("Dear " + un.ToString().ToUpper() + ", ");
            sb.Append("</b></td>");
            sb.Append("</tr >");

            sb.Append("<tr >");
            sb.Append("<td style='height:8px'></td>");
            sb.Append("</tr>");

            sb.Append("<tr >");
            sb.Append("<td style='font-family:Verdana;font-size:10px;color:Black;'  colspan='2'><b>");
            sb.Append("You have applied " + lt.ToString() + " on " + txtfromdate.Text + " to " + txttodate.Text + "  for " + ViewState["LeaveDays"].ToString() + " days. Reason - " + TXTREMARKS.Text.ToString() + ". ");
            sb.Append("</b></td>");
            sb.Append("</tr>");

            sb.Append("<tr >");
            sb.Append("<td style='height:8px'></td>");
            sb.Append("</tr>");

            sb.Append("<tr >");
            sb.Append("<td style='font-family:Verdana;font-size:10px;color:Black;'  colspan='2'><b>");
            sb.Append("Please don’t reply to this e-mail. This is an auto generated e-mail. For any query, please reach out to hr@jabong.com.");
            sb.Append("</b></td>");
            sb.Append("</tr>");

            sb.Append("<tr >");
            sb.Append("<td style='height:8px'></td>");
            sb.Append("</tr>");

            sb.Append("<tr >");
            sb.Append("<td style='font-family:Verdana;font-size:10px;color:Sea Green;'  colspan='2'><b>");
            sb.Append("Regards,");
            sb.Append("</b></td>");
            sb.Append("</tr>");
            sb.Append("<tr >");
            sb.Append("<td style='font-family:Verdana;font-size:10px;color:Sea Green;'  colspan='2'><b>");
            sb.Append("Team - HR");
            sb.Append("</b></td>");
            sb.Append("</tr>");


            sb.Append("<tr >");
            sb.Append("<td style='height:8px'></td>");
            sb.Append("</tr>");

            sb.Append("</table>");
            message = sb.ToString();
            // Response.Write(message.ToString());
            // return;
            bool IsSent = false;
            if (!string.IsNullOrEmpty(tomail.ToString().Trim()))
            {
                if (ViewState["IsSL"].ToString() == "Y")
                {
                    IsSent = SendMail.SendEMailSL(tomail.ToString(), "Leave Application By " + un.Trim(), message, ViewState["uploadfile"].ToString());
                }
                else
                {
                    IsSent = SendMail.SendEMail(tomail.ToString(), "Leave Application By " + un.Trim(), message);
                }
                if (!IsSent)
                {
                    change = "<script language='javascript'>alert('could not sent mail')</script> ";
                    Page.RegisterStartupScript("msg", change);
                    return;
                }
            }
            else
            {
                change = "<script language='JavaScript'>alert('Email Addresses not found.')</script>";
                Page.RegisterStartupScript("frmchange", change);
            }
        }
        catch (Exception er)
        {
            Error_Occured("Set_Matter12", er.Message);
        }
    }
    private bool SendMails(string StrTo, string StrSubject, string StrMailMessage, string StrFrom, string Email_CC)
    {
        MailMessage mail = new MailMessage();
        mail.To.Add(StrTo);
        //mail.To.Add("yogesh@admantechnologies.com");
        if (StrFrom.ToString().Trim() == "")
            StrFrom = ConfigurationSettings.AppSettings["FromMail"].ToString();

        mail.From = new MailAddress(StrFrom);
        mail.Subject = StrSubject;
        string Body = StrMailMessage;
        mail.Body = Body;
        mail.IsBodyHtml = true;


        if (!string.IsNullOrEmpty(Email_CC))
            mail.CC.Add(Email_CC);

        /*if (HrMailId != "")
            mail.CC.Add(HrMailId);*/

        SmtpClient smtp = new SmtpClient();
        smtp.Host = ConfigurationManager.AppSettings["SmtpServer"].ToString();    //"192.168.1.3"; // smtp server address 
        smtp.Port = Convert.ToInt32(ConfigurationSettings.AppSettings["smtpport"].ToString());
        smtp.EnableSsl = true;
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
        try
        {
            smtp.Send(mail);
            return true;
        }
        catch (Exception exc)
        {
            //Response.Write(exc.Message.ToString() + "----" + exc.InnerException.ToString());
            return false;
        }
    }

    protected void DDType_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["HoliDayDate"] = DDType.SelectedValue.ToString().Substring(0, 10);
        LblDt.Text = ViewState["HoliDayDate"].ToString();
    }
    protected void RadioLeaveType_SelectedIndexChanged(object sender, EventArgs e)
    {
        TXTREMARKS.Text = "";
        if (RadioLeaveType.SelectedIndex == 1)
        {
            DDType.Visible = true;
            LblDt.Visible = true;
            txtfromdate.Enabled = false;
            txttodate.Enabled = false;
            DDLREASON.Enabled = false;
            DDType_SelectedIndexChanged(sender, e);
        }
        else
        {
            DDType.Visible = false;
            LblDt.Visible = false;
            txtfromdate.Enabled = true;
            txttodate.Enabled = true;
            DDLREASON.Enabled = true;
        }

    }


    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        if (Session["usertype"].ToString() == "A")
        {
            Response.Redirect("Home.aspx");
        }
        else
        {
            Response.Redirect("frmMain.aspx");
        }
    }
    protected void grdrequest_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        try
        {
            
            DateTime pastDate = new DateTime();
            DateTime LWPfrom = new DateTime();
            DateTime LWPTo = new DateTime();
            DateTime futDate = new DateTime();
            DataSet dsWeekname = new DataSet();
            DataSet dswo = new DataSet();
            string wkname = "";
            bool ispast = false;
            bool isfut = false;
            DateTime chkWk = System.DateTime.MinValue;
            bool iswk = false;
            string strsql;
            string lcod;
            string DelHoliday = "";
            OleDbDataReader dr;
            bool IsApproved = false;
            string S1, S2;
            string lvDur = "";
            lcod = DDLREASON.SelectedItem.Value.ToString();
            string applino = e.Keys[0].ToString().Trim();
            bool ismarkedLWP = false;
            double bal = 0;
            string lvfield = "";
            if (Connection.State == 0)
                Connection.Open();
            strsql = "select isnull(Leave_Request.stage1_approved,'') stage1_approved, isnull(Leave_Request.stage2_approved,'') stage2_approved, Leave_Request.paycode,Leave_Request.leave_from,Leave_Request.leavecode,Leave_Request.leavedays,tblleavemaster.leavefield,Leave_Request.leave_to,isnull(Leave_Request.LWPbal,0) 'LWPbal',Leave_Request.halfDay from leave_request,TblLeavemaster where leave_request.leaveCode=TblLeaveMaster.leavecode and  leave_request.application_no=" + applino;
            dr = cn.ExecuteReader(strsql);
            if (dr.Read())
            {
                bal = Convert.ToDouble(dr["LWPbal"].ToString());
                strpaycode = dr["paycode"].ToString().Trim();
                strdatefrom = dr["leave_from"].ToString().Trim();
                strleavecode = dr["leavecode"].ToString().Trim();
                date1 = DateTime.Parse(dr["leave_from"].ToString());
                if (DelHoliday.ToString().Trim().ToUpper() != "Y")
                    date2 = DateTime.Parse(dr["leave_to"].ToString());

                days = Convert.ToDouble(dr["leavedays"].ToString());
                S1 = dr["stage1_approved"].ToString().ToUpper().Trim();
                S2 = dr["stage2_approved"].ToString().ToUpper().Trim();
                lvDur = dr["halfDay"].ToString().ToUpper().Trim();
                if (S1 == "Y" && S2 == "Y")
                {
                    IsApproved = true;
                    ismarkedLWP = true;
                }
                else if (S1.ToString().Trim() == "" && S2.ToString().Trim() == "")
                {
                    IsApproved = true;
                    ismarkedLWP = false;
                }
                else
                {
                    IsApproved = false;
                    ismarkedLWP = false;
                }
                lvfield = dr["LeaveField"].ToString();
            }
            DateTime dt = DateTime.Parse(strdatefrom);
            strdatefrom = dt.ToString("yyyy-MM-dd");
            if (ConfigurationManager.AppSettings["isFinancialYear"].ToString() == "Y")
            {
                if (dt.Month == 1 || dt.Month == 2 || dt.Month == 3)
                {
                    dt = dt.AddYears(-1);
                }
            }
            if (strleavecode.ToString().Trim().ToUpper() != "HLD")
            {
                MyCon = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"]);
                if (MyCon.State == ConnectionState.Closed)
                    MyCon.Open();
                oleTransaction = MyCon.BeginTransaction();

                try
                {
                    if (IsApproved)
                    {
                        DeleteFromTimeRegister(applino);
                        strsql = " update TBLLEAVELEDGER SET " + lvfield.ToString() + " = " + lvfield.ToString() + " - " + days + "   ";
                        strsql += " where  paycode='" + ViewState["paycode"].ToString() + "' and lyear='" + dt.Year.ToString("0000") + "'";
                        cn.execute_NonQuery(strsql);
                    }
                    MyCmd = new OleDbCommand();
                    dr.Close();
                    strsql = "";

                    strsql = "delete from leave_request where application_no=" + applino;
                    cn.execute_NonQuery(strsql);

                    Strsql = "";
                    Strsql = "insert into tblFunctionCall(paycode,fromDate,CaptureData,FunctionName) values('" + ViewState["paycode"].ToString().Trim() + "','" + date1.ToString("yyyy-MM-dd") + "','N', 'BackDateProcess')";
                    cn.execute_NonQuery(Strsql);
                    oleTransaction.Commit();

                }
                catch (Exception ex)
                {
                    oleTransaction.Rollback();
                }
                finally
                {
                    oleTransaction.Dispose();
                    MyCon.Dispose();
                    MyCon.Close();
                }
                grdrequest.CancelEdit();
                e.Cancel = true;
                BindGrid();
                fill_LeaveGrid();
            }
            else
            {
                lblmessage.Text = "You cann't delete this leave application.";
                dr.Close();
            }

        }
        catch
        {

        }
    }
    private void DeleteFromTimeRegister(string applino)
    {
        //// Deleting approved leaves from timeregister  
        DateTime dt1, dt2;
        string IsHalfDay = "", LvCode = "";
        string payc = "";

        strsql = "select halfday,LeaveCode, convert ( char(10), Leave_from,120) Leave_from,  convert ( char(10),Leave_to,120) Leave_to,paycode from leave_Request where Application_No =" + applino;
        dr = cn.Execute_Reader(strsql);
        if (!dr.Read())
        {
            dr.Close();
            return;
        }
        else
        {
            IsHalfDay = dr["halfday"].ToString().Trim();
            LvCode = dr["LeaveCode"].ToString().Trim();
            dt1 = DateTime.ParseExact(dr["Leave_from"].ToString().Trim(), "yyyy-MM-dd", CultureInfo.InvariantCulture);
            dt2 = DateTime.ParseExact(dr["Leave_to"].ToString().Trim(), "yyyy-MM-dd", CultureInfo.InvariantCulture);
            payc = dr["paycode"].ToString().Trim();
        }
        dr.Close();
        strsql = "update tblTimeRegister set Reason='', Leavecode = Null, LEAVETYPE = Null, Voucher_No = Null, LEAVETYPE1 = Null, LEAVETYPE2 = Null, FIRSTHALFLEAVECODE = Null, SECONDHALFLEAVECODE = Null, leaveamount = 0, leaveamount1 = 0, leaveamount2 = 0,leavevalue=0,absentvalue=1,status='A',LeaveAprDate=null, stage1approvaldate=null,stage2approvaldate=null where LeaveAmount> 0 And Dateoffice Between '" + dt1.ToString("yyyy-MM-dd") + "' And '" + dt2.ToString("yyyy-MM-dd") + "' and paycode='" + payc.ToString() + "' ";
        i = cn.execute_NonQuery(strsql);

        if (i == 0)
            return;

        while (dt1 <= dt2)
        {
            if (Connection.State == 0)
                Connection.Open();

            OleDbCommand upd = new OleDbCommand("upd", Connection);
            upd.CommandType = CommandType.StoredProcedure;
            OleDbParameter myParm1 = upd.Parameters.Add("@paycode", OleDbType.Char, 10);
            myParm1.Value = ViewState["paycode"].ToString();
            OleDbParameter myParm2 = upd.Parameters.Add("@dateoffice", OleDbType.Date, 10);
            myParm2.Value = dt1.ToString("yyyy-MM-dd");
            upd.ExecuteNonQuery();
            Connection.Close();
            dt1 = dt1.AddDays(1);
        }
    }
    protected void txtfromdate_CalendarDayCellPrepared(object sender, DevExpress.Web.CalendarDayCellPreparedEventArgs e)
    {

        if (e.Date.DayOfWeek == DayOfWeek.Friday || e.Date.DayOfWeek == DayOfWeek.Saturday)
        {
            e.Cell.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
            e.Cell.ForeColor = System.Drawing.Color.Black;
        }
    }
    protected void txttodate_CalendarDayCellPrepared(object sender, DevExpress.Web.CalendarDayCellPreparedEventArgs e)
    {

        if (e.Date.DayOfWeek == DayOfWeek.Friday || e.Date.DayOfWeek == DayOfWeek.Saturday)
        {
            e.Cell.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
            e.Cell.ForeColor = System.Drawing.Color.Black;
        }
    }

    protected void grdrequest_CommandButtonInitialize(object sender, DevExpress.Web.ASPxGridViewCommandButtonEventArgs e)
    {
        try
        {
            if (Session["LoginUserType"].ToString().Trim().ToUpper() == "U")
            {

                if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Delete)
                {
                    e.Enabled = false;
                }
               
            }
        }
        catch
        {

        }
    }
}