﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Globalization;
using System.Threading;
using System.Data.OleDb;
using System.Configuration;
using System.Diagnostics;
using GlobalSettings;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;

using System.Text;
using System.IO;
using DevExpress.Web;
public partial class FrmLeaveSummary : System.Web.UI.Page
{

    OleDbConnection Connection = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"]);
    Class_Connection cn = new Class_Connection();
    ErrorClass ec = new ErrorClass();
    Setting_App Settings = new Setting_App();
    OleDbDataReader dr;
    public string txtFromDateCID = "";
    public string txtToDateCID = "";
    DateTime fd = System.DateTime.Today;
    string Pcode, StrSql;
    string Strsql = null;
    DataSet ds = null;
    string ReportView = "";
    string a = "Attendence Report";
    string strsql;
    OleDbConnection MyConn = new OleDbConnection();
    OleDbCommand MyCmd = new OleDbCommand();
    DateTime date1, date2;
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        //Call The function to write the error log file 
        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));

        ec.Write_Log(PageName, FunctionName, ErrorMsg);
    }
    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
            {
                Request.Browser.Adapters.Clear();
            }
        }

        try
        {
            // Put user code to initialize the page here
            if (Session["PAYCODE"] == null || Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            //txtFromDateCID = txtfromdate.ClientID;
            //txtToDateCID = txttodate.ClientID;

            if (Session["usertype"].ToString().Trim().ToUpper() == "U")
            {
                //TD1.Visible = false;
                //tdrad.Visible = false;
            }
            if (!IsPostBack)
            {
                // lblMsg.Text = "Attendance View";
                grdrequest.Visible = false;
                txtfromdate.Text = "01/" + DateTime.Now.Month.ToString("00") + "/" + DateTime.Now.Year.ToString();
                txttodate.Text = fd.ToString("dd/MM/yyyy");
                LblEmpName.Text = "";
                lblLocation.Text = "";
                Fill_Combo();
                


            }
        }
        catch (Exception er)
        {
            Error_Occured("Form Load", er.Message);
        }
    }

    private void Fill_Combo()
    {
        ReportView = "Y";// ConfigurationSettings.AppSettings["ReportView_HeadID"].ToString();
        try
        {
            StrSql = "";
            if (Session["usertype"].ToString() == "C")  //HOD
            {
                StrSql = "select paycode,rtrim(Empname) + ' - ' + paycode as Empname  from tblemployee where hod_code ='" + Session["PAYCODE"].ToString() + "'  and active='Y' order by empname";
            }
            else if (Session["usertype"].ToString() == "H")  //Reproting Manager 
            {
                if (ReportView.ToString().Trim() == "Y")
                {
                    StrSql = "";
                    StrSql = "select paycode,rtrim(Empname) + ' - ' + paycode as Empname  from tblemployee where headid in(select paycode from tblemployee where headid='" + Session["PAYCODE"].ToString() + "' ) or  headid='" + Session["PAYCODE"].ToString() + "' and active='Y' order by empname";
                }
                else
                {
                    StrSql = "";
                    StrSql = "Select paycode,rtrim(Empname) + ' - ' + paycode as Empname  from tblemployee where Active='Y'  ";
                    if (Session["Auth_Comp"] != null)
                    {
                        StrSql += " and tblemployee.companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
                    }
                    if (Session["Auth_Dept"] != null)
                    {
                        StrSql += " and tblemployee.Departmentcode in (" + Session["Auth_Dept"].ToString().Trim() + ") ";
                    }
                    StrSql += " order by empname";
                }
            }
            else if (Session["usertype"].ToString() == "A")  //Admin
            {
                StrSql = "Select paycode,rtrim(Empname) + ' - ' + paycode as Empname  from tblemployee where Active='Y'  ";
                if ((Session["usertype"].ToString() == "A") && (Session["Auth_Comp"] != null) && (Session["PAYCODE"].ToString().ToUpper().Trim() != "ADMIN"))
                {
                    StrSql += " and companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
                }
                if ((Session["usertype"].ToString() == "A") && (Session["Auth_Dept"] != null) && (Session["PAYCODE"].ToString().ToUpper().Trim() != "ADMIN"))
                {
                    StrSql += " and Departmentcode in (" + Session["Auth_Dept"].ToString().Trim() + ") ";
                }
                StrSql += " Order by EmpName ";
            }

            else if (Session["usertype"].ToString() == "U")  //Admin
            {
                StrSql = "Select paycode,rtrim(Empname) + ' - ' + paycode as Empname  from tblemployee where Active='Y' and CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "' and paycode='" + Session["LoginUserName"].ToString().Trim().Trim() + "'  ";

            }
            if (StrSql.Trim() == "")
                return;
            DataSet DsEmpName = new DataSet();
            DsEmpName = cn.FillDataSet(StrSql);
            EmpCombo.DataSource = DsEmpName;
            EmpCombo.TextField = "EmpName";
            EmpCombo.ValueField = "Paycode";
            EmpCombo.DataBind();
            EmpCombo.SelectedIndex = -1;
            if (Session["usertype"].ToString() != "A")  //Admin
            {
                //EmpCombo.Items.Insert(0, new ListItem(Session["UserName"].ToString().Trim() + " - " + Session["PAYCODE"].ToString(), Session["PAYCODE"].ToString()));
            }
            
        }
        catch (Exception er)
        {
            Error_Occured("Fill_Combo", er.Message);
        }
    }
    private void Fill_Grid()
    {
        try
        {
             string msg = "", change="";
            DateTime dateFrom = System.DateTime.MinValue;
            DateTime dateTo = System.DateTime.MinValue;
           
            try
                {
                    date1 = Convert.ToDateTime(txtfromdate.Text.ToString());   //DateTime.ParseExact(txtfromdate.Value.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                catch
                {

                }
                try
                {
                    date2 = Convert.ToDateTime(txttodate.Text.ToString());
                }
                catch
                {
                    
                }
                if (date1 > date1)
                {
                    SetDate();
                    change = "<script language='JavaScript'>alert('Date Selection Not Valid.')</script>";
                    Page.RegisterStartupScript("frmchange", change);
                    return;
                }
            DataSet ds = new DataSet();
            if (EmpCombo.SelectedIndex != -1)  //For a particular employee 
            {
                //strsql = " select Application_no 'Application No',convert( char(10), Request_Date,103) 'Applied on' ,Leave_Request.Paycode,rtrim(empname) Employee,  rtrim(Request_Id) Applicant, Leavecode Leave, ";
                //strsql += " convert( char(10), Leave_From,103) 'From', convert( char(10), Leave_to,103) 'To', LeaveDays Days , userremarks as Remarks, isnull(Stage2_Approved,'N') 'Status', Approved = case  Stage2_Approved   ";
                //strsql += "  when 'Y' then 'Approved' when 'N' then 'Rejected' else 'Pending' end from Leave_Request,  TblEmployee where Leave_Request.paycode= TblEmployee.paycode ";
                //strsql += " and Leave_Request.paycode='" + EmpCombo.SelectedItem.Value.ToString() + "'  ";
                strsql= "select Application_no 'Application No',convert( char(10), Request_Date,103) 'Applied on' ,Leave_Request.Paycode 'Employee Code',rtrim(empname) Employee, "+
                        " rtrim(Request_Id) Applicant, Leavecode Leave, convert( char(10), Leave_From, 103) 'From', convert(char(10), Leave_to, 103) 'To', LeaveDays Days, "+
                        " userremarks as 'User Remarks', isnull(Stage1_Approved, 'N') ' Stage 1 Status', 'Stage 1 Approved' = case  Stage1_Approved when 'Y' then 'Approved' when 'N' then 'Rejected' " +
                        " else 'Pending' end ,isnull(Stage1_approval_Remarks, '') 'Stage 1 Remarks',Stage1_approval_id 'Stage 1 Approver',convert(char(10), Stage1_approval_date, 103) 'Stage 1 Approved on', " +
                        " isnull(Stage2_approved, 'N') ' Stage 2 Status', 'Stage 2 Approved' = case  Stage2_approved when 'Y' then 'Approved' when 'N' then 'Rejected' else 'Pending' " +
                        " end ,isnull(Stage2_approval_Remarks, '') 'Stage 2 Remarks' ,Stage2_approval_id 'Stage 2 Approver',convert(char(10), Stage2_approval_date, 103) 'Stage 2 Approved on'from Leave_Request, TblEmployee " +
                        " where Leave_Request.paycode = TblEmployee.paycode  and Leave_Request.paycode = '" + EmpCombo.SelectedItem.Value.ToString() + "' ";


            }
            else if(EmpCombo.SelectedIndex == -1)  // All Employees
            {
                //strsql = "select Application_no 'Application No',convert( char(10), Request_Date,103) 'Applied on' ,Leave_Request.Paycode, rtrim(empname) Employee,  rtrim(Request_Id) Applicant, Leavecode Leave, ";
                //strsql += " convert( char(10), Leave_From,103) 'From', convert( char(10), Leave_to,103) 'To', LeaveDays Days , userremarks as Remarks, isnull(Stage2_Approved,'N') 'Status', Approved = case  Stage2_Approved ";
                //strsql += "  when 'Y' then 'Approved' when 'N' then 'Rejected' else 'Pending' end  ";
                //strsql += " from Leave_Request, TblEmployee where Leave_Request.paycode= TblEmployee.paycode ";
                strsql = "select Application_no 'Application No',convert( char(10), Request_Date,103) 'Applied on' ,Leave_Request.Paycode 'Employee Code', rtrim(empname) Employee," +
                         " rtrim(Request_Id) Applicant, Leavecode Leave, convert( char(10), Leave_From, 103) 'From', convert(char(10), Leave_to, 103) 'To', LeaveDays Days, " +
                         " userremarks as 'User Remarks', isnull(Stage1_Approved, 'N') ' Stage 1 Status', 'Stage 1 Approved' = case  Stage1_Approved when 'Y' then 'Approved' when 'N' then 'Rejected' else 'Pending' " +
                         " end ,isnull(Stage1_approval_Remarks, '') 'Stage 1 Remarks',Stage1_approval_id 'Stage 1 Approver',convert(char(10), Stage1_approval_date, 103) 'Stage 1 Approved ', isnull(Stage2_approved, 'N') 'Stage 2 Status', 'Stage 2 Approved' = case  Stage2_approved when 'Y' then 'Approved' when 'N' then 'Rejected' else 'Pending' " +
                         " end ,isnull(Stage2_approval_Remarks, '') 'Stage 2 Remarks' ,Stage2_approval_id 'Stage 2 Approver',convert(char(10), Stage2_approval_date, 103) 'Stage 2 Approved'  from Leave_Request, TblEmployee where Leave_Request.paycode = TblEmployee.paycode ";






                if (Session["usertype"].ToString() == "H")
                {
                    strsql += " And TblEmployee.paycode in  ( select paycode from tblemployee where headid in ( select Paycode from tblemployee where paycode ='" + Session["PAYCODE"].ToString() + "') or headid_2 in ( select Paycode from tblemployee where paycode ='" + Session["PAYCODE"].ToString() + "') ) ";

                }
                if ((Session["usertype"].ToString() == "A") && (Session["UserName"].ToString().ToUpper() != "ADMIN")) //Admin
                {
                    if (Session["Auth_Comp"] != null)
                    {
                        strsql += " and companycode in (" + Session["Auth_Comp"].ToString() + ")   ";
                    }
                    if (Session["Auth_Dept"] != null)
                    {
                        strsql += " and Departmentcode in (" + Session["Auth_Dept"].ToString() + ") ";
                    }
                }

            }
            strsql += " And leave_from between '" + date1.ToString("yyyy-MM-dd") + "' and '" + date2.ToString("yyyy-MM-dd") + "'  ";
            strsql += " order by  Leave_Request.Paycode, Request_Date  desc";
            ds = cn.FillDataSet(strsql);
           
            if (ds.Tables[0].Rows.Count > 0)
            {
                grdrequest.Visible = true;
                grdrequest.DataSource = ds.Tables[0];
                grdrequest.DataBind();
            }
            else
            {
                
                grdrequest.DataSource = "";
                grdrequest.DataBind();
                grdrequest.Visible = false;


            }
        }
        catch (Exception er)
        {
            Error_Occured("FillGrid", er.Message);
        }
    }

    private void SetDate()
    {
        try
        {
            date1 = DateTime.Now.Date.AddDays(-(DateTime.Now.Day - 1));
            txtfromdate.Text = date1.ToString("dd/MM/yyyy");

            date2 = System.DateTime.Now.Date;
            txttodate.Text = date2.ToString("dd/MM/yyyy");
        }
        catch (Exception er)
        {
            Error_Occured("SetDate", er.Message);
        }
    }

    protected void EmpCombo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string sSql = "";
        DataSet DsEmp = new DataSet();


        try
        {
            if (EmpCombo.SelectedIndex == -1 || EmpCombo.SelectedItem.Text.ToString().Trim().ToUpper() == "NONE")
            {
                LblEmpName.Text = "";
                lblLocation.Text = "";
                
                EmpCombo.Focus();
                return;
            }
            sSql = "select EMPNAME,tblDepartment.DepartmentName from tblemployee inner join tblDepartment on tblDepartment.DepartmentCode=tblemployee.DepartmentCode " +
                    "where tblemployee.PAYCODE='" + EmpCombo.SelectedItem.Value.ToString().Trim() + "' and tblemployee.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim().Trim() + "' " +
                    " and tblDepartment.CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "'";
            DsEmp = cn.FillDataSet(sSql);
            if (DsEmp.Tables[0].Rows.Count > 0)
            {
                LblEmpName.Text = DsEmp.Tables[0].Rows[0]["EMPNAME"].ToString().Trim();
                lblLocation.Text = DsEmp.Tables[0].Rows[0]["DepartmentName"].ToString().Trim();
            }



        }
        catch
        {
            LblEmpName.Text = "";
            lblLocation.Text = "";
            EmpCombo.Focus();
            return;
        }
    }

    protected void grdrequest_PageIndexChanged(object sender, EventArgs e)
    {
        int pageIndex = (sender as ASPxGridView).PageIndex;
        grdrequest.PageIndex = pageIndex;
        this.Fill_Grid();
    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        Fill_Grid();
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        //Fill_Grid();
        //grdrequest.DataBind();
    }
}