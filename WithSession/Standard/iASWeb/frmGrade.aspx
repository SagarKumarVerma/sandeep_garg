﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="frmGrade.aspx.cs" Inherits="frmGrade" %>
<asp:Content ID="Content" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">  
        function State_OnKeyUp(s, e) {  
            s.SetText(s.GetText().toUpperCase().trim());
        }  
    </script>  
    <%-- DXCOMMENT: Configure ASPxGridView control --%>
    <dx:ASPxGridView ID="GrdGrade" runat="server" AutoGenerateColumns="False" 
    Width="100%" KeyFieldName="GradeCode" OnCellEditorInitialize="GrdComp_CellEditorInitialize" OnRowValidating="GrdComp_RowValidating" OnRowUpdating="GrdComp_RowUpdating" OnRowInserting="GrdComp_RowInserting" OnRowDeleting="GrdComp_RowDeleting" OnPageIndexChanged="GrdComp_PageIndexChanged" OnInitNewRow="GrdGrade_InitNewRow" OnRowDeleted="GrdGrade_RowDeleted" OnStartRowEditing="GrdGrade_StartRowEditing" OnCommandButtonInitialize="GrdGrade_CommandButtonInitialize">
        <%-- DXCOMMENT: Configure ASPxGridView's columns in accordance with datasource fields --%>
         <ClientSideEvents EndCallback="function(s, e) {
	 if (s.cp_isSuccess) 
{ alert('Can Not Delete Record As It Is Already Assigned To Employee'); 
delete s.isSuccess; } 
}" />


        <SettingsAdaptivity AdaptivityMode="HideDataCells" />
        
         <SettingsPager>
                <PageSizeItemSettings Visible="true" Items="10, 20, 50,100" />
            </SettingsPager>
        <SettingsEditing Mode="PopupEditForm" EditFormColumnCount="1">
        </SettingsEditing>
        <Settings ShowFilterRow="True" ShowTitlePanel="True" />
        <SettingsBehavior ConfirmDelete="True" />
        <SettingsCommandButton>
            <ClearFilterButton Text=" ">
                <Image IconID="filter_clearfilter_16x16">
                </Image>
            </ClearFilterButton>
            <NewButton Text=" ">
                <Image IconID="actions_add_16x16">
                </Image>
            </NewButton>
            <UpdateButton ButtonType="Image" RenderMode="Image">
                <Image IconID="save_save_16x16office2013">
                </Image>
            </UpdateButton>
            <CancelButton ButtonType="Image" RenderMode="Image">
                <Image IconID="actions_cancel_16x16office2013">
                </Image>
            </CancelButton>
            <EditButton Text=" ">
                <Image IconID="actions_edit_16x16devav">
                </Image>
            </EditButton>
            <DeleteButton Text=" ">
                <Image IconID="actions_trash_16x16">
                </Image>
            </DeleteButton>
        </SettingsCommandButton>
         <SettingsPopup>
            <EditForm Modal="True" Width="500px" HorizontalAlign="WindowCenter" 
                PopupAnimationType="Slide" VerticalAlign="WindowCenter" />
        </SettingsPopup>  <SettingsSearchPanel Visible="True" />
        <SettingsText PopupEditFormCaption="Grade" Title="Grade" />
        <Columns>
            <dx:GridViewCommandColumn  Width="40px" ShowClearFilterButton="True" ShowDeleteButton="True" ShowEditButton="True" ShowNewButtonInHeader="True" VisibleIndex="0">
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="GradeCode" VisibleIndex="1" Caption="Code">
                <PropertiesTextEdit MaxLength="3">
                    
                    <%--<ValidationSettings Display="Dynamic">
                        <RequiredField IsRequired="false" />
                    </ValidationSettings>--%>
                </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="GradeName" VisibleIndex="2" Caption="Name">
                 <PropertiesTextEdit MaxLength="45">
                     
                  </PropertiesTextEdit>
            </dx:GridViewDataTextColumn>
          
        </Columns>
        <Paddings Padding="0px" />
        <Border BorderWidth="0px" />
        <BorderBottom BorderWidth="1px" />
    </dx:ASPxGridView>


<%-- DXCOMMENT: Configure your datasource for ASPxGridView --%>



</asp:Content>

