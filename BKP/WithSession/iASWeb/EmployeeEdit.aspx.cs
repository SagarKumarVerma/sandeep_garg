﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Text;
using System.IO;
using System.Globalization;
using System.Text.RegularExpressions;
using GlobalSettings;

public partial class EmployeeEdit : System.Web.UI.Page
{
    Class_Connection cn = new Class_Connection();
    OleDbConnection MyConn = new OleDbConnection();
    OleDbCommand MyCmd = new OleDbCommand();
    Setting_App Settings = new Setting_App();
    string Strsql = null;
    DataSet ds = null;
    DataSet ds1 = null;
    int result = 0;
    int result1 = 0;
    int result3 = 0;
    int result4 = 0;
    int result5 = 0; int result6 = 0; int result7 = 0;
    OleDbDataAdapter da = null;
    OleDbConnection connection = null;
    string Strsql1 = "";
    string Authrun = "";
    string str = "";
    string str1 = "";
    DateTime dob;
    string DB = "";
    string DL = "";
    string finalShifts = "";
    Byte[] imgByte = null;
    Byte[] imgByte1 = null;
    string PF1 = "";
    string BankCode = "";
    string BankAcc = "";
    string EmployeeImage = string.Empty;
    string FileName = "";
    string location = "";
    string guid = "";

    string FileName1 = "";
    string EmpSignatureLocation = "";
    string guid1 = "";

    string inonly = "";
    string isPunchAll = "";
    string Twopunch = "";
    string IsOutWork = "";

    string SecondWeeklyoff = "";
    string HalfDayShift = "";
    string SecondOffType = "";
    string FirstWeeklyoff = "";

    string Company = "";
    string Dept = "";
    string cat = "";
    string Grade = "";
    string Section = "";
    string EmpGroup = "";
    string HID = "";

    string Tmp;

    string Pcompany = " ";
    string Pdeptcode = " ";
    string Pcat = " ";
    string PDesignation = " ";
    string PDivisionCode = " ";
    string PGradeCode = " ";
    string PCard = "";
    string Active = "";
    string EmpName = "";
    string isLDAP = "";
    string btnGoBack = "";
    string resMsg = "";
    bool IsNewRecord = false;
     protected void Page_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();
        btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
        if (ex is HttpRequestValidationException)
        {
            resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" +
                   "Form doesn't need to contain valid HTML, just anything with opening and closing angled brackets (<...>).<br />" +
                   btnGoBack +
                   "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
        else if (ex is OleDbException)
        {
            resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" + ex.Message.ToString() + ".<br />" +
                    "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
        else
        {
            resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" + ex.Message.ToString() + ".<br />" +
                    "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
    }

    ErrorClass ec = new ErrorClass();

    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        //Call The function to write the error log file 

        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        //try
        //{
        ec.Write_Log(PageName, FunctionName, ErrorMsg);
        //}
        //catch (Exception ess)
        //{ }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
            {
                Request.Browser.Adapters.Clear();
            }
            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            txtCardNo.Attributes.Add("onBlur", "return empCode('" + txtCardNo.ClientID + "');");
            DataFilter.Loademployee(Session["LoginCompany"].ToString().Trim());
            //lookupComp.DataSource = DataFilter.CompanyNonAdmin;
            //lookupDept.DataSource = DataFilter.LocationNonAdmin;
            //lookupSec.DataSource = DataFilter.DivNonAdmin;
            //lookupGrade.DataSource = DataFilter.GradeNonAdmin;
            //lookupCat.DataSource = DataFilter.CatNonAdmin;
            //lookupPay.DataSource = DataFilter.EmpNonAdmin;
            bindCompany();
            bindPaycode();
            bindDepartment();
            bindSection();
            bindGrade();
            bindCategory();
            BindGroup();
            bindBank();
            ASPxPageControl1.ActiveTabIndex = 0;
            ddlBank.Attributes.Add("onChange", "return BankAc('" + ddlBank.ClientID + "','" + txtAC.ClientID + "')");
            ddlActive.Attributes.Add("onChange", "return Active('" + ddlActive.ClientID + "','" + lblLeavingDate.ClientID + "','" + lblReason.ClientID + "','" + txtLeavingDate.ClientID + "','" + txtLeavingreason.ClientID + "','" + tdleave.ClientID + "');");
            if (Request.QueryString["Value"] != null)
            {
                IsNewRecord = false;
                string EmployeeCode = Request.QueryString["Value"].ToString().Trim();
                ViewState["EmpCode"] = Request.QueryString["Value"].ToString().Trim();
                txtEmpCode.Text = EmployeeCode.ToString().Trim();
                //txtEmpCode.Enabled = false;                
                bindEmployee(EmployeeCode);
                Session["IsNewRec"] = "NO";

            }
            else
            {
                              
                Session["IsNewRec"] = "YES";

            }
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        bindCompany();
        bindPaycode();
        bindDepartment();
        bindSection();
        bindGrade();
        bindCategory();
        BindGroup();
        bindBank();
    }
    protected void bindCompany()
    {
        //Strsql = "select CompanyCode,convert(varchar(100),Companycode)+' - '+convert(varchar(100),Companyname) as 'Comid' from tblCompany";
        Strsql = "select CompanyCode,Companyname from tblCompany";
        //if ((Session["Auth_Comp"] != null || Session["Auth_Comp"] != string.Empty))
        //if ((Session["usertype"].ToString().Trim() == "A") && (Session["Auth_Dept"] != null))
        if(Session["LoginUserName"].ToString().Trim().ToUpper()=="ADMIN")
        {
            Strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim()+ ") ";
        }
        else
        {
            Strsql += " where companycode ='" + Session["LoginCompany"].ToString().Trim() + "' ";
        }
        //if (Session["usertype"].ToString().Trim() == "A" && Session["Auth_Comp"] != null)
        /*if ((Session["Auth_Comp"] != null) && (Session["PAYCODE"].ToString().Trim().ToUpper() != "ADMIN"))
        {`
            Strsql += " where companycode in (" + Session["Auth_Comp"].ToString() + ") ";
        }
        else if (Session["usertype"].ToString().Trim() == "H")
        {
            Strsql += " where companycode in (select companycode from tblemployee where paycode='"+Session["PAYCODE"].ToString().Trim()+"') ";
        }*/
        Error_Occured("Company", Strsql);
        ds = new DataSet();
        ds = cn.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            //ddlCompany.DataSource = ds.Tables[0];
            //ddlCompany.TextField = "Comid";
            //ddlCompany.ValueField = "CompanyCode";
            //ddlCompany.DataBind();
            //ddlCompany.SelectedIndex = 0;
            lookupComp.DataSource = ds.Tables[0];
            lookupComp.DataBind();
        }
    }
    protected void bindPaycode()
    {
        //Strsql = "select ltrim(rtrim(empname)) +' - '+ ltrim(rtrim(paycode)) 'EmpName1',ltrim(rtrim(paycode)) paycode,Empname from tblemployee where active='Y' and  CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
        Strsql = "select PAYCODE,EMPNAME from tblemployee where active='Y' and  CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
        ds = new DataSet();
        Error_Occured("PayCode", Strsql);
        ds = cn.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
           // ddlPaycode.DataSource = ds.Tables[0];
           // ddlPaycode.TextField = "EmpName1";
           // ddlPaycode.ValueField = "paycode";
           // ddlPaycode.DataBind();
           // ddlPaycode.SelectedIndex = 0;
           //// ddlPaycode.Items.Insert(0, "NONE");
            lookupPay.DataSource = ds.Tables[0];
            lookupPay.DataBind();
        }
    }
    protected void bindBank()
    {
        Strsql = "select BankCode,convert(varchar(100),BankCode)+' - '+convert(varchar(100),BankName) as 'Bankid' from tblBank where   CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
        ds = new DataSet();
        Error_Occured("Bank", Strsql);
        ds = cn.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlBank.DataSource = ds.Tables[0];
            ddlBank.TextField = "Bankid";
            ddlBank.ValueField = "BankCode";
            ddlBank.DataBind();
            ddlBank.SelectedIndex = 0;
        }
      
    }
    protected void bindDepartment()
    {
        //Strsql = "select DepartmentCode,convert(varchar(100),DepartmentCode)+' - '+convert(varchar(100),departmentname) as 'deptid' from tbldepartment";
        Strsql = "select DepartmentCode,departmentname from tbldepartment";
        //if ((Session["Auth_Dept"] != null && Session["Auth_Dept"] != string.Empty))
        //if ((Session["usertype"].ToString().Trim() == "A") && (Session["Auth_Dept"] != null))
        if (Session["LoginUserName"].ToString().Trim().ToUpper() == "ADMIN")
        {
            Strsql += " where DepartmentCode in (" + DataFilter.AuthDept.Trim() + ") and  CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' ";
        }
        else
        {
            Strsql += " where  CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' ";
        }
        Error_Occured("bindDepartment", Strsql);
        ds = new DataSet();
        ds = cn.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            //ddlDepartment.DataSource = ds.Tables[0];
            //ddlDepartment.TextField = "deptid";
            //ddlDepartment.ValueField = "DepartmentCode";
            //ddlDepartment.DataBind();
            //ddlDepartment.SelectedIndex = 0;
            lookupDept.DataSource = ds.Tables[0];
            lookupDept.DataBind();
        }
    }    
    protected void bindCategory()
    {
        //Strsql = "select CAT,convert(varchar(100),CAT)+' - '+convert(varchar(100),CATAGORYNAME) as 'Catid' from tblCatagory where   CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
        Strsql = "select CAT,CATAGORYNAME from tblCatagory where   CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
        ds = new DataSet();
        Error_Occured("Cat", Strsql);
        ds = cn.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            //ddlCategory.DataSource = ds.Tables[0];
            //ddlCategory.TextField = "Catid";
            //ddlCategory.ValueField = "CAT";
            //ddlCategory.DataBind();
            //ddlCategory.SelectedIndex = 0;
            lookupCat.DataSource = ds.Tables[0];
            lookupCat.DataBind();

        }
    }
    protected void bindSection()
    {
        //Strsql = "select DivisionCode,convert(varchar(100),DivisionCode)+' - '+convert(varchar(100),DivisionName) as 'Divid' from tblDivision where   CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
        Strsql = "select DivisionCode,DivisionName from tblDivision where   CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
        ds = new DataSet();
        Error_Occured("Section", Strsql);
        ds = cn.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            //ddlSection.DataSource = ds.Tables[0];
            //ddlSection.TextField = "Divid";
            //ddlSection.ValueField = "DivisionCode";
            //ddlSection.DataBind();
            //ddlSection.SelectedIndex = 0;
            lookupSec.DataSource = ds.Tables[0];
            lookupSec.DataBind();
        }
    }
    protected void bindGrade()
    {
        //Strsql = "select GradeCode,convert(varchar(100),GradeCode)+' - '+convert(varchar(100),GradeName) as 'tblGrade' from tblGrade where   CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
        Strsql = "select GradeCode,GradeName from tblGrade where   CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
        ds = new DataSet();
        Error_Occured("Grade", Strsql);
        ds = cn.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            //ddlGrade.DataSource = ds.Tables[0];
            //ddlGrade.TextField = "tblGrade";
            //ddlGrade.ValueField = "GradeCode";
            //ddlGrade.DataBind();
            //ddlGrade.SelectedIndex = 0;
            lookupGrade.DataSource = ds.Tables[0];
            lookupGrade.DataBind();
        }
    }
    protected void BindGroup()
    {
        //Strsql = "select GroupID,convert(varchar(100),GroupID)+' - '+convert(varchar(100),GroupName) as 'GroupIDText' from tblEmployeeGroupPolicy where   CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' order by GroupID ";
        Strsql = "select GroupID,GroupName from tblEmployeeGroupPolicy where   CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' order by GroupID ";
        ds = new DataSet();
        Error_Occured("Group", Strsql);
        ds = cn.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            //ddlGroup.DataSource = ds.Tables[0];
            //ddlGroup.TextField = "GroupIDText";
            //ddlGroup.ValueField = "GroupID";
            //ddlGroup.DataBind();
            //ddlGroup.SelectedIndex = 0;
            lookupGrp.DataSource = ds.Tables[0];
            //lookupGrp.DataBind();
        }
        else
        {
            lookupGrp.DataSource = null;
            lookupGrp.DataBind();
        }

    }

    protected void bindEmployee(string ECode)
    {
        DateTime doleaving;
        ASPxPageControl1.TabIndex = 0;
        Strsql = "select tblEmployee.*, convert(char(10), DateOfBirth , 121) DOB , convert(char(10), DateOfJoin , 121) DOJ , convert(char(10), LeavingDate , 121) LD  from tblEmployee where paycode='" + ECode.ToString().Trim() + "' and  CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' ";
        ds = new DataSet();
        Error_Occured("Employee", Strsql);
        ds = cn.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
           
            string Active = ds.Tables[0].Rows[0]["Active"].ToString().Trim();
            if (ds.Tables[0].Rows[0]["Active"].ToString().Trim() == "N")
            {
                ddlActive.SelectedIndex=1;
                Tmp = "";

                txtLeavingreason.Visible = true;
                txtLeavingDate.Visible = true;
                lblReason.Visible = true;
                lblLeavingDate.Visible = true;


                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["LD"].ToString().Trim()))
                {
                    Tmp = ds.Tables[0].Rows[0]["LD"].ToString().Trim().Substring(0, 10);
                    doleaving = DateTime.ParseExact(Tmp, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    txtLeavingDate.Text = doleaving.ToString("dd/MM/yyyy").Trim();
                    txtLeavingreason.Text = ds.Tables[0].Rows[0]["LeavingReason"].ToString().Trim();
                }
                else
                {
                    txtLeavingDate.Text = "";
                }
            }
            else
            {
               // ddlActive.SelectedItem.Text = "True";
                ddlActive.SelectedIndex = 0;
                txtLeavingreason.Visible = false;
                txtLeavingDate.Visible = false;
                lblReason.Visible = false;
                lblLeavingDate.Visible = false;
            }
            txtCardNo.Text = ds.Tables[0].Rows[0]["PRESENTCARDNO"].ToString().Trim();
            ViewState["CardNo"] = ds.Tables[0].Rows[0]["PRESENTCARDNO"].ToString().Trim();
            ViewState["SSN"] = ds.Tables[0].Rows[0]["SSN"].ToString().Trim();
            txtName.Text = ds.Tables[0].Rows[0]["EMPNAME"].ToString().Trim();
            txtGurName.Text = ds.Tables[0].Rows[0]["GUARDIANNAME"].ToString().Trim();






            Company = ds.Tables[0].Rows[0]["COMPANYCODE"].ToString();
            if (Company != string.Empty)
            {
                //ddlCompany.SelectedItem.Value = ds.Tables[0].Rows[0]["COMPANYCODE"].ToString();
                lookupComp.Value = Company;
            }
            BankCode = ds.Tables[0].Rows[0]["BankCODE"].ToString().Trim();
            if (BankCode != string.Empty)
            {
                ddlBank.SelectedItem.Value = BankCode;// ds.Tables[0].Rows[0]["BankCODE"].ToString();
                txtAC.Enabled = true;
            }
            else
            {
                txtAC.Enabled = false;
            }
            Dept = ds.Tables[0].Rows[0]["DepartmentCode"].ToString();           
            if (Dept != string.Empty)
            {
                //ddlDepartment.SelectedItem.Text = Dept;// ds.Tables[0].Rows[0]["DepartmentCode"].ToString();
                lookupDept.Value = Dept;
            }
            cat = ds.Tables[0].Rows[0]["CAT"].ToString();
            if (cat != string.Empty)
            {
                //ddlCategory.SelectedItem.Text = ds.Tables[0].Rows[0]["CAT"].ToString();
                lookupCat.Value = cat;
            }
            Section = ds.Tables[0].Rows[0]["DivisionCode"].ToString();
            if (Section != string.Empty)
            {
                //ddlSection.SelectedItem.Text = ds.Tables[0].Rows[0]["DivisionCode"].ToString();
                lookupSec.Value = Section;
            }
            Grade = ds.Tables[0].Rows[0]["GradeCode"].ToString();
            if (Grade != string.Empty)
            {
                //ddlGrade.SelectedItem.Text = ds.Tables[0].Rows[0]["GradeCode"].ToString();
                lookupGrade.Value =Grade;
            }
            EmpGroup = ds.Tables[0].Rows[0]["GroupID"].ToString();

            if (EmpGroup != string.Empty)
            {
                //ddlGroup.SelectedItem.Text = ds.Tables[0].Rows[0]["GroupID"].ToString();
                lookupGrp.Value =EmpGroup;
            }
            HID = ds.Tables[0].Rows[0]["headid"].ToString();
            if (HID != string.Empty)
            {
                //ddlGroup.SelectedItem.Text = ds.Tables[0].Rows[0]["GroupID"].ToString();
                lookupPay.Value = HID.Trim();
            }




            txtDesignation.Text = ds.Tables[0].Rows[0]["DESIGNATION"].ToString().Trim();
            if(Session["LoginUserName"].ToString().Trim().ToUpper()!="A")
            {
                txtDesignation.Enabled = false;
            }

            //Employee Image
            string img = ds.Tables[0].Rows[0]["Empphoto"].ToString().Trim();
            if (img == "NULL" || img == "")
            {
               // imgEmployee.ImageUrl = "~/Emp_Img/Default.gif";
                ViewState["EmpImg"] = "";
            }
            else
            {
                imgEmployee.ImageUrl = ds.Tables[0].Rows[0]["Empphoto"].ToString().Trim();
                ViewState["EmpImg"] = ds.Tables[0].Rows[0]["Empphoto"].ToString().Trim();
            }
            //Employee signature
            string imgSig = ds.Tables[0].Rows[0]["EMPSIGNATURE"].ToString().Trim();
            if (imgSig == "NULL" || imgSig == "")
            {
               // imgSignature.ImageUrl = "~/Emp_Img/Default.gif";
                ViewState["EmpImgSign"] = "";

            }
            else
            {
                imgSignature.ImageUrl = ds.Tables[0].Rows[0]["EMPSIGNATURE"].ToString().Trim();
                ViewState["EmpImgSign"] = ds.Tables[0].Rows[0]["EMPSIGNATURE"].ToString().Trim();
            }
             txtAC.Text = ds.Tables[0].Rows[0]["BankAcc"].ToString().Trim();

            DateTime dj = DateTime.Now.Date;

            Tmp = "";
            Tmp = ds.Tables[0].Rows[0]["DOJ"].ToString().Trim().Substring(0, 10);
            dj = DateTime.ParseExact(Tmp, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            txtDateOfjoin.Text = dj.ToString("dd/MM/yyyy").Trim();

            if (ds.Tables[0].Rows[0]["DateOFBIRTH"].ToString().Trim() != "")
            {
                Tmp = "";
                Tmp = ds.Tables[0].Rows[0]["DOB"].ToString().Trim().Substring(0, 10);
                DateTime dob = DateTime.ParseExact(Tmp, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                txtDOB.Text = dob.ToString("dd/MM/yyyy").Trim();
            }
            string IsMarried = ds.Tables[0].Rows[0]["ISMARRIED"].ToString();
            if (IsMarried.Trim() == "Y")
            {
                ddlMarried.SelectedIndex = 1;
            }
            else
            {
                ddlMarried.SelectedIndex = 0;
            }
            
            ddlBloodGroup.SelectedItem.Text = ds.Tables[0].Rows[0]["BLOODGROUP"].ToString().Trim();
            txtQualification.Text = ds.Tables[0].Rows[0]["QUALIFICATION"].ToString().Trim();
            txtExperience.Text = ds.Tables[0].Rows[0]["EXPERIENCE"].ToString().Trim();
            txtEmail.Text = ds.Tables[0].Rows[0]["E_MAIL1"].ToString().Trim();
            if (ds.Tables[0].Rows[0]["SEX"].ToString().Trim()=="F")
            {
               // radSex.SelectedItem.Value = ds.Tables[0].Rows[0]["SEX"].ToString().Trim();
                radSex.SelectedIndex = 1;
            }
            else
            {
                radSex.SelectedIndex = 0;
            }
           
            txtBusRoute.Text = ds.Tables[0].Rows[0]["BUS"].ToString().Trim();
            txtVehicle.Text = ds.Tables[0].Rows[0]["VehicleNo"].ToString().Trim();
            txtPAddress.Text = ds.Tables[0].Rows[0]["ADDRESS1"].ToString().Trim();
            txtPPhone.Text = ds.Tables[0].Rows[0]["TELEPHONE1"].ToString().Trim();
            txtPPinCode.Text = ds.Tables[0].Rows[0]["PINCODE1"].ToString().Trim();
            txtTAddress.Text = ds.Tables[0].Rows[0]["ADDRESS2"].ToString().Trim();
            txtTPhone.Text = ds.Tables[0].Rows[0]["TELEPHONE2"].ToString().Trim();
            txtTPinCode.Text = ds.Tables[0].Rows[0]["PINCODE2"].ToString().Trim();
            
            try
            {
                Strsql = "Select UserType From TblUser Where USer_R='" + ds.Tables[0].Rows[0]["PAYCODE"].ToString().Trim() + "' and SSN='" + ds.Tables[0].Rows[0]["SSN"].ToString().Trim() + "'";
                DataSet DsU = new DataSet();
                DsU = cn.FillDataSet(Strsql);
                if(DsU.Tables[0].Rows.Count>0)
                {
                    if (DsU.Tables[0].Rows[0][0].ToString().Trim() == "H")
                        ddlUType.SelectedIndex = 1;
                }
                else
                {
                    ddlUType.SelectedIndex = 0;
                }
            }
            catch
            {
                ddlUType.SelectedIndex = 0;
            }





        }

      
        }


    protected void LogCompare(DataSet DSES)
    {
        string MaxD = "", PMD = "", WFH = "", SRT = "", RTC = "N", Late = "", Early = "";
        string sSql = "", Active = "";

        hourToMinute hm = new hourToMinute();

        DateTime DateOfJoin = DateTime.Now.Date;
        DateTime DateOfBirth = DateTime.Now.Date;

        string TempDOJ = "";
        string CardNo = "";
        string TempDOB = "";
        TempDOJ = DSES.Tables[0].Rows[0]["dateofjoin"].ToString().Trim().Substring(0, 10);
        TempDOB = DSES.Tables[0].Rows[0]["dateofbirth"].ToString().Trim().Substring(0, 10);
        CardNo = DSES.Tables[0].Rows[0]["CARDNO"].ToString().Trim();

        //DateOfJoin = DateTime.ParseExact(TempDOJ, "yyyy-MM-dd", CultureInfo.InvariantCulture);


        if (txtCardNo.Text.ToString().Trim() != CardNo)
        {
            sSql = "INSERT INTO [dbo].[UserEditLog]([UserName],[LogDate],[LogDetails],[OldValue],[NewValue],[CompanyCode])values('" + Session["UserName"].ToString().Trim() + "',getdate(),'Biometric ID Updated For Code: " + ViewState["EmpCode"].ToString().Trim() + "','" + CardNo + "','"+txtCardNo.Text.ToString().Trim()+"','"+Session["LoginCompany"].ToString().Trim()+"')";
            cn.execute_NonQuery(sSql);
        }

        if (txtDateOfjoin.Text.ToString().Trim() != TempDOJ)
        {
            sSql = "INSERT INTO [dbo].[UserEditLog]([UserName],[LogDate],[LogDetails],[OldValue],[NewValue],[CompanyCode])values('" + Session["UserName"].ToString().Trim() + "',getdate(),'Date Of Join Updated For Code: " + ViewState["EmpCode"].ToString().Trim() + "','" + TempDOB + "','" + txtDOB.Text.ToString().Trim() + "','" + Session["LoginCompany"].ToString().Trim() + "')";
            cn.execute_NonQuery(sSql);
        }
        if (txtDOB.Text.ToString().Trim() != TempDOB)
        {
            sSql = "INSERT INTO [dbo].[UserEditLog]([UserName],[LogDate],[LogDetails],[OldValue],[NewValue],[CompanyCode])values('" + Session["UserName"].ToString().Trim() + "',getdate(),'Date Of Birth Updated For Code: " + ViewState["EmpCode"].ToString().Trim() + "','" + TempDOJ + "','" + txtDateOfjoin.Text.ToString().Trim() + "','" + Session["LoginCompany"].ToString().Trim() + "')";
            cn.execute_NonQuery(sSql);
        }

        if (lookupComp.Value.ToString().Trim() != DSES.Tables[0].Rows[0]["CompanyCode"].ToString().Trim())
        {

            sSql = "INSERT INTO [dbo].[UserEditLog]([UserName],[LogDate],[LogDetails],[OldValue],[NewValue],[CompanyCode])values('" + Session["UserName"].ToString().Trim() + "',getdate(),'Company Updated For Code: " + ViewState["EmpCode"].ToString().Trim() + "','" + DSES.Tables[0].Rows[0]["CompanyCode"].ToString().Trim() + "','" + lookupComp.Value.ToString().Trim() + "','" + Session["LoginCompany"].ToString().Trim() + "')";
            cn.execute_NonQuery(sSql);
        }

        if (lookupDept.Value.ToString().Trim() != DSES.Tables[0].Rows[0]["departmentcode"].ToString().Trim())
        {
            sSql = "INSERT INTO [dbo].[UserEditLog]([UserName],[LogDate],[LogDetails],[OldValue],[NewValue],[CompanyCode])values('" + Session["UserName"].ToString().Trim() + "',getdate(),'Department Updated For Code: " + ViewState["EmpCode"].ToString().Trim() + "','" + DSES.Tables[0].Rows[0]["departmentcode"].ToString().Trim() + "','" + lookupDept.Value.ToString().Trim() + "','" + Session["LoginCompany"].ToString().Trim() + "')";
            cn.execute_NonQuery(sSql);
        }
        if (lookupSec.Value.ToString().Trim() != DSES.Tables[0].Rows[0]["DivisionCode"].ToString().Trim())
        {
            sSql = "INSERT INTO [dbo].[UserEditLog]([UserName],[LogDate],[LogDetails],[OldValue],[NewValue],[CompanyCode])values('" + Session["UserName"].ToString().Trim() + "',getdate(),'Section Updated For Code: " + ViewState["EmpCode"].ToString().Trim() + "','" + DSES.Tables[0].Rows[0]["DivisionCode"].ToString().Trim() + "','" + lookupSec.Value.ToString().Trim() + "','" + Session["LoginCompany"].ToString().Trim() + "')";
            cn.execute_NonQuery(sSql);
        }
        if (lookupGrade.Value.ToString().Trim() != DSES.Tables[0].Rows[0]["GradeCode"].ToString().Trim())
        {
            sSql = "INSERT INTO [dbo].[UserEditLog]([UserName],[LogDate],[LogDetails],[OldValue],[NewValue],[CompanyCode])values('" + Session["UserName"].ToString().Trim() + "',getdate(),'Grade Updated For Code: " + ViewState["EmpCode"].ToString().Trim() + "','" + DSES.Tables[0].Rows[0]["GradeCode"].ToString().Trim() + "','" + lookupGrade.Value.ToString().Trim() + "','" + Session["LoginCompany"].ToString().Trim() + "')";
            cn.execute_NonQuery(sSql);
        }
        if (lookupCat.Value.ToString().Trim() != DSES.Tables[0].Rows[0]["CAT"].ToString().Trim())
        {
            sSql = "INSERT INTO [dbo].[UserEditLog]([UserName],[LogDate],[LogDetails],[OldValue],[NewValue],[CompanyCode])values('" + Session["UserName"].ToString().Trim() + "',getdate(),'Category Updated For Code: " + ViewState["EmpCode"].ToString().Trim() + "','" + DSES.Tables[0].Rows[0]["CAT"].ToString().Trim() + "','" + lookupCat.Value.ToString().Trim() + "','" + Session["LoginCompany"].ToString().Trim() + "')";
            cn.execute_NonQuery(sSql);
        }
        if (lookupGrp.Value.ToString().Trim() != DSES.Tables[0].Rows[0]["GroupID"].ToString().Trim())
        {
            sSql = "INSERT INTO [dbo].[UserEditLog]([UserName],[LogDate],[LogDetails],[OldValue],[NewValue],[CompanyCode])values('" + Session["UserName"].ToString().Trim() + "',getdate(),'Group Updated For Code: " + ViewState["EmpCode"].ToString().Trim() + "','" + DSES.Tables[0].Rows[0]["GroupID"].ToString().Trim() + "','" + lookupGrp.Value.ToString().Trim() + "','" + Session["LoginCompany"].ToString().Trim() + "')";
            cn.execute_NonQuery(sSql);
        }
        if (txtDesignation.Text.ToString().Trim() != DSES.Tables[0].Rows[0]["DESIGNATION"].ToString().Trim())
        {
            sSql = "INSERT INTO [dbo].[UserEditLog]([UserName],[LogDate],[LogDetails],[OldValue],[NewValue],[CompanyCode])values('" + Session["UserName"].ToString().Trim() + "',getdate(),'Designation Updated For Code: " + ViewState["EmpCode"].ToString().Trim() + "','" + DSES.Tables[0].Rows[0]["DESIGNATION"].ToString().Trim() + "','" + txtDesignation.Text.ToString().Trim() + "','" + Session["LoginCompany"].ToString().Trim() + "')";
            cn.execute_NonQuery(sSql);
        }
        //if (lookupPay.Value.ToString().Trim() != DSES.Tables[0].Rows[0]["headid"].ToString().Trim())
        //{
        //    sSql = "INSERT INTO [dbo].[UserEditLog]([UserName],[LogDate],[LogDetails],[OldValue],[NewValue],[CompanyCode])values('" + Session["UserName"].ToString().Trim() + "',getdate(),'Reporting Head Updated For Code: " + ViewState["EmpCode"].ToString().Trim() + "','" + DSES.Tables[0].Rows[0]["headid"].ToString().Trim() + "','" + lookupPay.Value.ToString().Trim() + "','" + Session["LoginCompany"].ToString().Trim() + "')";
        //    cn.execute_NonQuery(sSql);
        //}

    }

    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        try
        {
            lblstatus.Text = "";
            string change = "";
            int minyear = 1755;
            int maxyear = 9000;
            int year = 0;
            bool email = false;
            DataSet dsEcount = new DataSet();
            if (ddlActive.SelectedIndex > 0)
            {
                if (txtLeavingDate.Text.Length == 0)
                {
                    ASPxPageControl1.TabIndex = 0;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Leaving Date required.');", true);
                    change = "<script language='JavaScript'>alert('Leaving Date required.')</script>";
                    Page.RegisterStartupScript("frmchange", change);
                   // lblstatus.Text = "*Leaving Date Required";
                    tdleave.Visible = true;
                    lblLeavingDate.Visible = true;
                    txtLeavingDate.Visible = true;
                    lblReason.Visible = true;
                    txtLeavingreason.Visible = true;
                    txtLeavingDate.Focus();
                    return;
                }
            }
            else
            {
                lblLeavingDate.Visible = false;
                txtLeavingDate.Visible = false;
                lblReason.Visible = false;
                txtLeavingreason.Visible = false;
            }


            string EmpCardNumber = "";

            if (Session["IsNewRec"].ToString().Trim() == "YES")
            {
                ViewState["EmpCode"] = txtEmpCode.Text.ToString().Trim();
                ViewState["CardNo"] = txtCardNo.Text.ToString().Trim();
                ViewState["EmpImgSign"] = "";
                ViewState["EmpImg"] = "";
                ViewState["SSN"] = "";

                //int count = 0;
                //int UserLicence = Convert.ToInt32(Settings.UserLicence);

                //Strsql = "select count(paycode) from tblemployee where Active='Y' and CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"' ";
                //dsEcount = cn.FillDataSet(Strsql);
                //count = Convert.ToInt32(dsEcount.Tables[0].Rows[0][0]);
                //if (count >= UserLicence && UserLicence > 0)
                //{
                //    Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Maximum Users limit is " + UserLicence + ".');", true);
                //    return;
                //}
            }
            if (Session["IsNewRec"].ToString().Trim() == "NO")
            {
                DataSet DsTest = new DataSet();
                Strsql = "select tblemployeeshiftmaster.*,tblemployee.* from tblemployeeshiftmaster,tblemployee where tblemployeeshiftmaster.SSN=tblemployee.SSN and tblemployee.paycode='" + ViewState["EmpCode"].ToString().Trim() + "' and tblemployee.CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"'";
                DsTest = cn.FillDataSet(Strsql);
                if (DsTest.Tables[0].Rows.Count > 0)
                {
                    LogCompare(DsTest);
                }
            }

            if (ViewState["EmpCode"].ToString() != txtEmpCode.Text.ToString().Trim())
            {
                Strsql1 = "select count(paycode) from tblemployee where paycode='" + txtEmpCode.Text.Trim().ToString() + "' AND Companycode='"+Session["LoginCompany"].ToString().Trim()+"' ";
                result1 = cn.execute_Scalar(Strsql1);
                if (result1 > 0)
                {
                    txtEmpCode.Focus();
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Employee Code Already Exists.');", true);
                    change = "<script language='JavaScript'>alert('Employee Code Already Exists.')</script>";
                    Page.RegisterStartupScript("frmchange", change);
                    return;

                }

                string con = ConfigurationManager.AppSettings["ConnectionString"].ToString();
                OleDbConnection Conn = new OleDbConnection(con);
                Conn.Open();
                OleDbCommand cmd = new OleDbCommand();
                OleDbTransaction Trans = Conn.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = Trans;
                cmd.Connection = Conn;
                //try
                {
                    //string TempSSN= ddlCompany.SelectedItem.Value.ToString().Trim()+"_"+txtEmpCode.Text.ToString().Trim();
                    string TempSSN = lookupComp.Value.ToString().Trim() + "_" + txtEmpCode.Text.ToString().Trim();
                    Strsql = "Update tblEmployee set paycode = '" + txtEmpCode.Text.ToString().Trim() + "',SSN='"+TempSSN+"'    where SSN = '" + ViewState["SSN"].ToString().Trim() + "'";
                    cmd.CommandText = Strsql;
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                    //cmd.execute_NonQuery(Strsql);

                    Strsql = "Update tblTimeRegister set paycode = '" + txtEmpCode.Text + "' ,SSN='" + TempSSN + "' where SSN = '" + ViewState["SSN"].ToString().Trim() + "'";
                    cmd.CommandText = Strsql;
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();

                    Strsql = "Update tblemployeeshiftmaster set paycode = '" + txtEmpCode.Text + "' ,SSN='" + TempSSN + "' where SSN = '" + ViewState["SSN"].ToString().Trim() + "'";
                    cmd.CommandText = Strsql;
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();

                    Strsql = "Update tblleaveledger set paycode = '" + txtEmpCode.Text + "'  ,SSN='" + TempSSN + "' where SSN = '" + ViewState["SSN"].ToString().Trim() + "'";
                    cmd.CommandText = Strsql;
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();

                    Strsql = "Update leave_request set paycode = '" + txtEmpCode.Text + "'  ,SSN='" + TempSSN + "' where SSN = '" + ViewState["SSN"].ToString().Trim() + "'";
                    cmd.CommandText = Strsql;
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();

                    Strsql = "Update tbluser set paycode = '" + txtEmpCode.Text + "',user_r = '" + txtEmpCode.Text + "',SSN='" + TempSSN + "' where SSN = '" + ViewState["SSN"].ToString().Trim() + "'";
                    cmd.CommandText = Strsql;
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();

                    Strsql = "Update machinerawpunch set paycode = '" + txtEmpCode.Text + "',SSN='" + TempSSN + "'  where SSN = '" + ViewState["SSN"].ToString().Trim() + "'";
                    cmd.CommandText = Strsql;
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();

                    Trans.Commit();
                    ViewState["EmpCode"] = txtEmpCode.Text.ToString().Trim();
                    ViewState["SSN"] = TempSSN.Trim();
                }

            }
            if (ViewState["CardNo"].ToString() == txtCardNo.Text.ToString().Trim())
            {
                // EmpCardNumber = txtCardNo.Text.ToString().Trim();
                EmpCardNumber = string.Format("{0:00000000}", Convert.ToInt32(txtCardNo.Text));

            }
            else
            {
                Strsql1 = "select count(presentcardno) from tblemployee where presentcardno='" + txtCardNo.Text.Trim().ToString() + "' and SSN='" + ViewState["SSN"].ToString().Trim() + "' ";
                result1 = cn.execute_Scalar(Strsql1);
                if (result1 > 0)
                {
                   
                    txtCardNo.Focus();
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert(' "+lblCardNo.Text +" Already Exists');", true);
                    change = "<script language='JavaScript'>alert('" + lblCardNo.Text + " Already Exists.')</script>";
                    Page.RegisterStartupScript("frmchange", change);
                    return;
                }
                else
                {
                    EmpCardNumber = string.Format("{0:00000000}", Convert.ToInt32(txtCardNo.Text));
                }
            }
            DateTime DOJ;

            if (txtDateOfjoin.Text.Trim() == "")
            {
                int i = Convert.ToInt32(System.DateTime.Now.Day - 1);
                txtDateOfjoin.Text = System.DateTime.Now.AddDays(-i).ToString("dd/MM/yyyy");
            }
            try
            {
                DOJ = Convert.ToDateTime(txtDateOfjoin.Value);
                year = DOJ.Year;
                if ((year < minyear) || (year > maxyear))
                {
                    
                    txtDateOfjoin.Focus();
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Invalid Joining Date Year');", true);
                    change = "<script language='JavaScript'>alert('Invalid Joining Date Year.')</script>";
                    Page.RegisterStartupScript("frmchange", change);
                    return;
                }
            }
            catch
            {
                txtDateOfjoin.Focus();
                change = "<script language='JavaScript'>alert('Invalid Joining Date.')</script>";
                Page.RegisterStartupScript("frmchange", change);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Invalid Joining Date');", true);
                return;
            }

            //date of birth
            DateTime DOB;
            if (txtDOB.Text.Trim() == "")
            {
                DB = "1995-01-01";
            }
            else
            {
                try
                {
                    DOB = Convert.ToDateTime(txtDOB.Value);
                    DB = DOB.ToString("yyyy-MM-dd");
                    year = DOB.Year;
                    if ((year < minyear) || (year > maxyear))
                    {
                       
                        txtDOB.Focus();
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Invalid Birth Year');", true);
                        change = "<script language='JavaScript'>alert('Invalid Birth Date Year.')</script>";
                        Page.RegisterStartupScript("frmchange", change);
                        return;
                    }
                }
                catch
                {
                    
                    txtDOB.Focus();
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert(' Invalid Birth Date');", true);
                    change = "<script language='JavaScript'>alert('Invalid Birth Date.')</script>";
                    Page.RegisterStartupScript("frmchange", change);
                    return;
                }
            }

            DateTime DOL;
            if (ddlActive.SelectedItem.Value.ToString().Trim() == "N")
            {
                if (txtLeavingDate.Text.Trim() == "")
                {
                    DL = "null";
                }
                else
                {
                    try
                    {
                        DOL = DateTime.ParseExact(txtLeavingDate.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        Error_Occured("Save", DOL.ToString("dd/mm/yyyy"));
                        DL = DOL.ToString("yyyy-MM-dd");
                        year = DOL.Year;
                        if ((year < minyear) || (year > maxyear))
                        {
                           
                            txtLeavingDate.Focus();
                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Invalid Leaving Date Year');", true);
                            change = "<script language='JavaScript'>alert('Invalid Leaving Date Year.')</script>";
                            Page.RegisterStartupScript("frmchange", change);
                            return;
                        }
                    }
                    catch
                    {
                        Error_Occured("Save", txtLeavingDate.Text);
                        txtLeavingDate.Focus();
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert(' Invalid Leaving Date');", true);
                        change = "<script language='JavaScript'>alert('Invalid Leaving Date.')</script>";
                        Page.RegisterStartupScript("frmchange", change);
                        return;
                    }
                }
            }
            else
            {
                DL = "null";
            }
            //if (EmployeeImage!=string.Empty)
            if (imgEmployee.ImageUrl != string.Empty)
            {
                try
                {
                    string f = ViewState["EmpImg"].ToString().Trim();
                    if (f != string.Empty)
                    {
                        //string[] r = f.Split('/');
                        //f = r[1] + "\\" + r[2];
                        //FileInfo file = new FileInfo(Server.MapPath(f));
                        //file.Delete();
                    }
                }
                catch { }

                FileName = imgEmployee.ImageUrl.ToString().Replace(" ", "");
                //FileName = System.IO.Path.GetFileName(FileName);
                //guid = Guid.NewGuid().ToString().Trim();
                //guid = guid.Substring(0, 8);
                //string[] name = FileName.Split('.');
                //FileName = name[0] + "_" + guid + "." + name[1];
               // location = Server.MapPath("Emp_Img//") + FileName.ToString().Trim();
               // FileName = "~/Emp_Img/" + FileName.ToString().Trim();
                FileImage.ToolTip = FileName.ToString().Trim();
               // FileImage.PostedFile.SaveAs(FileName.ToString());
            }
            else if (ViewState["EmpImg"].ToString() != "")
            {
                FileName = ViewState["EmpImg"].ToString().Trim();
            }
            else
            {
                FileName = "";
            }

            // Employee Signature
            if (FileSignature.HasFile)
            {
                FileName1 = FileSignature.PostedFile.FileName.Replace(" ", "");
                FileName1 = System.IO.Path.GetFileName(FileName1);
                guid1 = Guid.NewGuid().ToString().Trim();
                guid1 = guid1.Substring(0, 8);
                string[] name1 = FileName1.Split('.');
                FileName1 = name1[0] + "_" + guid1 + "." + name1[1];
                EmpSignatureLocation = Server.MapPath("Emp_Signature//") + FileName1.ToString().Trim();
                FileName1 = "~/Emp_Signature/" + FileName1.ToString().Trim();
            }
            else if (ViewState["EmpImgSign"].ToString() != "")
            {
                FileName1 = ViewState["EmpImgSign"].ToString().Trim();
            }
            else
            {
                FileName1 = "";
            }
            string address1 = "";
            string address2 = "";
            int add1len = 0;
            int add2len = 0;
            add1len = Convert.ToInt32(txtPAddress.Text.ToString().Length);
            add2len = Convert.ToInt32(txtTAddress.Text.ToString().Length);

            if (add1len > 100)
            {
                address1 = txtPAddress.Text.ToString().Substring(0, 100);
            }
            else
            {
                address1 = txtPAddress.Text.ToString();
            }

            if (add2len > 100)
            {
                address2 = txtTAddress.Text.ToString().Substring(0, 100);
            }
            else
            {
                address2 = txtTAddress.Text.ToString();
            }


            //Master DropDown
           // string[] DepSle = ddlDepartment.SelectedItem.Text.ToString().Split('-');
            //string[] SecSel = ddlSection.SelectedItem.Text.ToString().Split('-');
            //string[] GrdSel = ddlGrade.SelectedItem.Text.ToString().Split('-');
            //string[] CatSel = ddlCategory.SelectedItem.Text.ToString().Split('-');
            //string[] EmpGrpSel = ddlGroup.SelectedItem.Text.ToString().Split('-');



           // Dept = DepSle[0].ToString().Trim();
            //Section = SecSel[0].ToString().Trim();
            //Grade = GrdSel[0].ToString().Trim();
            //cat = CatSel[0].ToString().Trim();
            //EmpGroup = EmpGrpSel[0].ToString().Trim();


            change="";
            try
            {
                Dept = lookupDept.Value.ToString();
            }
            catch
            {
                lookupDept.Focus();
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert(' Invalid Leaving Date');", true);
                change = "<script language='JavaScript'>alert('Select Department')</script>";
                Page.RegisterStartupScript("frmchange", change);
                return;
            }
            try
            {
                 Section = lookupSec.Value.ToString();
            }
           catch
            {
                lookupSec.Focus();
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert(' Invalid Leaving Date');", true);
                change = "<script language='JavaScript'>alert('Select Section')</script>";
                Page.RegisterStartupScript("frmchange", change);
                return;
            }
           
            try
            {
               
            Grade = lookupGrade.Value.ToString();
           
            }
            catch
            {
                lookupGrade.Focus();
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert(' Invalid Leaving Date');", true);
                change = "<script language='JavaScript'>alert('Select Grade')</script>";
                Page.RegisterStartupScript("frmchange", change);
                return;
            }
            try
            {
                cat = lookupCat.Value.ToString();
            }

            catch
            {
                lookupCat.Focus();
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert(' Invalid Leaving Date');", true);
                change = "<script language='JavaScript'>alert('Select Category')</script>";
                Page.RegisterStartupScript("frmchange", change);
                return;
            }

            try
            {
                EmpGroup = lookupGrp.Value.ToString();
            }
            catch
            {
                lookupGrp.Focus();
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert(' Invalid Leaving Date');", true);
                change = "<script language='JavaScript'>alert('Select Group')</script>";
                Page.RegisterStartupScript("frmchange", change);
                return;
            }


            if (EmpGroup.Trim() == string.Empty || Dept.Trim() == string.Empty || Section.Trim() == string.Empty || Grade.Trim() == string.Empty || cat.Trim() == string.Empty)
            {
                lookupDept.Focus();
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert(' Invalid Leaving Date');", true);
                change = "<script language='JavaScript'>alert('Select All Masters')</script>";
                Page.RegisterStartupScript("frmchange", change);
                return;
            }

            //EmployeeShiftMaster 


            string inonly = "N";
            string isPunchAll = "Y";
            string TimeLoss = "Y";
            string RoundClock = "N";
            string IsOt = "Y";
            string IsAuthShift = "N";
            string AuthShifts = "";
            string OS = "N";
            string ishalfday = "Y";
            string isshort = "Y";
            string Twopunch = "N";
            string days = "";
            string IsOutWork = "Y";
            string IsCoff = "O";
            string IsMIS = "P";
            int late = 10;
            int EarlyDept = 10;
            int MaxDayWork = 720;
            int PresentMarkingDuration = 240;
            int workinghourforhalfday = 300;
            int workhourforshort = 420;
            int HalfAfter = 60;
            int HalfBefore = 60;
            int ResignedAfter = 0;
            string FourPunch_Night = "N";
            string OTOutMinusShiftEndTime = "Y";
            string OTWrkgHrsMinusShiftHrs = "N";
            string OTEarlyComePlusLateDep = "N";
            string OTRound = "N";
            string OT_EARLYCOMING = "N";
            string PresentOn_HLDPresent = "N";
            string PresentOn_WOPresent = "N";
            string AbsentOn_WOAbsent = "N";
            string awa = "N";
            string AW = "N";
            string WA = "N";
            string Wo_Absent = "N";
            string EndTime_In = "05:00";
            string EndTime_RTCEmp = "05:00";
            int AUTOSHIFT_LOW = 240;
            int AUTOSHIFT_UP = 240;
            int DEDUCTHOLIDAYOT = 0;
            int DEDUCTWOOT = 0;
            string ISOTEARLYCOMING = "N";
            string OTMinus = "N";
            int OTEARLYDUR = 0;
            int OTLATECOMINGDUR = 0;
            int OTRESTRICTENDDUR = 0;
            int DUPLICATECHECKMIN = 0;
            int PREWO = 0;
            TimeLoss = "Y";
            RoundClock = "N";
            isshort = "Y";
            ishalfday = "Y";
            OS = "N";
            IsOt = "N";
            string   txtOTRate = "000.00";
            IsMIS = "H";
            IsCoff = "O";
            string    EnableAutoResign = "N";
            inonly = "N";
            isPunchAll = "Y";
            Twopunch = "N";
            IsOutWork = "N";
            IsAuthShift = "N";
            AuthShifts = "";
            FirstWeeklyoff = "SUN";
            SecondWeeklyoff = "NON";
            HalfDayShift = " ";
            SecondOffType = "";
            days = "";

               




                string HOD = "",UType;
                try
                {
                     //HOD = ddlPaycode.SelectedItem.Value.ToString().Trim();
                     HOD = lookupPay.Value.ToString().Trim();
                }
                catch
                {
                      HOD="";
                }
                try
                {
                    UType = ddlUType.SelectedItem.Value.ToString().Trim();
                }
                catch
                {
                    UType = "";
                }
           

            //Employee Shift Master End
            string IsMarried="N";
            try
            {
                IsMarried = ddlMarried.SelectedItem.Value.ToString().Trim();
            }
            catch
            {

            }
            string BloodGrp = "NA";
            try
            {
                BloodGrp = ddlBloodGroup.SelectedItem.Value.ToString().Trim();
            }
            catch
            {

            }


            using (OleDbConnection con = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString()))
            {
                con.Open();
                OleDbCommand cmd = new OleDbCommand();
                OleDbTransaction trans = con.BeginTransaction(IsolationLevel.ReadCommitted);
                cmd.Transaction = trans;

                string qry = "";
                int LogId = 0;
                DataSet dsLog = new DataSet();
                //string ESSN = ddlCompany.SelectedItem.Value.ToString().Trim() + "_" + txtEmpCode.Text.ToString().Trim();
                string ESSN = lookupComp.Value.ToString().Trim() + "_" + txtEmpCode.Text.ToString().Trim();
                string CardNo = string.Format("{0:00000000}", Convert.ToInt32(txtCardNo.Text));
                if (Session["IsNewRec"].ToString().Trim() != "YES")
                {
                    Strsql = "update tblEmployee set SSN='"+ESSN.Trim()+"', Active='" + ddlActive.SelectedItem.Value.ToString().Trim() + "',EmpName='" + txtName.Text.Trim().ToString().Trim().ToUpper() + "',  " +
                      "GUARDIANNAME='" + txtGurName.Text.ToString().Trim().ToUpper() + "', " +
                      "DateOFJOIN='" + DOJ.ToString("yyyy-MM-dd") + "',PRESENTCARDNO='" + CardNo.ToString().Trim() + "', " +
                      "COMPANYCODE='" + lookupComp.Value.ToString().Trim() + "',DEPARTMENTCODE='" + Dept.Trim() + "',CAT='" + cat.Trim() + "', " +
                      "SEX='" + radSex.SelectedItem.Value.ToString().Trim() + "',ISMARRIED='" + IsMarried.Trim() + "',BUS='" + txtBusRoute.Text.ToString().Trim() + "',QUALIFICATION='" + txtQualification.Text.ToString().Trim() + "' , " +
                      "EXPERIENCE ='" + txtExperience.Text.ToString().Trim() + "' ,DESIGNATION='" + txtDesignation.Text.ToString().Trim() + "',ADDRESS1='" + address1.ToString().ToString().Trim() + "', " +
                      "PINCODE1='" + txtPPinCode.Text.ToString().Trim() + "',TELEPHONE1='" + txtPPhone.Text.ToString().Trim() + "', " +
                      "E_MAIL1='" + txtEmail.Text.ToString().Trim() + "',ADDRESS2='" + address2.ToString().Trim() + "',PINCODE2='" + txtTPinCode.Text.ToString().Trim() + "', " +
                      "TELEPHONE2='" + txtTPhone.Text.ToString().Trim() + "'," +
                      "BLOODGROUP='"+ BloodGrp.ToString().Trim()+"', EMPPHOTO='" + FileName.ToString().Trim() + "', " +
                      "EMPSIGNATURE='" + FileName1.ToString().Trim() + "', DivisionCode='" + Section.Trim() + "', " +
                      "GradeCode='" + Grade.Trim() + "',VehicleNo= '" + txtVehicle.Text.ToString().Trim() + "', " +
                      "headid='" + HOD.Trim() + "', GroupID='"+EmpGroup.Trim()+"', " +
                      "bankCODE=isnull('" + BankCode.ToString().Trim() + "',null),BankAcc=isnull('" + BankAcc.ToString() + "',null), ";
                    if (txtDOB.Text.ToString() == "")
                        Strsql += "DateOFBIRTH=null, ";
                    else
                        Strsql += "DateOFBIRTH='" + DB.ToString() + "',";

                    if (ddlActive.SelectedIndex == 0)
                        Strsql += " LeavingReason='',Leavingdate=null ";
                    else
                        Strsql += " LeavingReason='" + txtLeavingreason.Text.ToString().Trim() + "',Leavingdate= '" + DL + "' ";

                    Strsql += " Where paycode='" + ViewState["EmpCode"].ToString().Trim() + "' and  COMPANYCODE='"+Session["LoginCompany"].ToString().Trim()+"' ";

                    result = cn.execute_NonQuery(Strsql);
                    UpdateTblEmpShiftMaster(ESSN);
                    {
                        trans.Commit();
                        if (ddlActive.SelectedItem.Value.ToString().Trim() == "N")
                        {
                            Strsql = "Delete from tbltimeregister where paycode='" + txtEmpCode.Text.ToString().Trim() + "' and dateoffice >= '" + DL + "' ";
                            result = cn.execute_NonQuery(Strsql);
                        }

                        if (FileImage.HasFile)
                        {
                            FileImage.PostedFile.SaveAs(location);
                        }
                        if (FileSignature.HasFile)
                        {
                            FileSignature.PostedFile.SaveAs(EmpSignatureLocation);
                        }
                        Strsql = "update tbluser set USERDESCRIPRION='"+txtName.Text.ToString().Trim()+"', Usertype='" + ddlUType.SelectedItem.Value.ToString().Trim() + "' where SSN='" + ESSN.Trim() + "'";
                        cn.execute_NonQuery(Strsql);


                        Response.Redirect("frmEmployee.aspx");
                    }
                }
                else
                {
                    Strsql = " insert into tblEmployee (SSN,Active,paycode,PRESENTCARDNO,EmpName,GUARDIANNAME,DateOFJOIN,DateOFBIRTH,COMPANYCODE,DEPARTMENTCODE,CAT,SEX,ISMARRIED,BUS," +
                            " QUALIFICATION,EXPERIENCE,DESIGNATION,ADDRESS1,PINCODE1,TELEPHONE1,E_MAIL1,ADDRESS2,PINCODE2,TELEPHONE2,BLOODGROUP,EMPPHOTO,EMPSIGNATURE," +
                            " DivisionCode,GradeCode,VehicleNo,headid,GroupID)values ('"+ESSN.Trim()+"','" + ddlActive.SelectedItem.Value.ToString().Trim() + "','" + txtEmpCode.Text.ToString().Trim() + "'," +
                            " '" + CardNo.ToString().Trim() + "','" + txtName.Text.ToString().Trim() + "','" + txtGurName.Text.ToString().Trim() + "','" + DOJ.ToString("yyyy-MM-dd") + "','"+DB.ToString()+"','" + lookupComp.Value.ToString().Trim() + "'," +
                            " '"+Dept.Trim()+"','"+cat.Trim()+"','"+radSex.SelectedItem.Value.ToString().Trim()+"','"+ddlMarried.SelectedItem.Value.ToString().Trim()+"',"+
                            " '" + txtBusRoute.Text.ToString().Trim() + "','" + txtQualification.Text.ToString().Trim() + "','" + txtExperience.Text.ToString().Trim() + "','" + txtDesignation.Text.ToString().Trim() + "',"+
                            " '" + txtPAddress.Text.ToString().Trim() + "','" + txtPPinCode.Text.ToString().Trim() + "','" + txtPPhone.Text.ToString().Trim() + "','" + txtEmail.Text.ToString().Trim() + "','" + txtTAddress.Text.ToString().Trim() + "'"+
                            " ,'" + txtTPinCode.Text.ToString().Trim() + "','" + txtTPhone.Text.ToString().Trim() + "','" + BloodGrp.ToString().Trim() + "','" + FileName.ToString().Trim() + "','" + FileName1.ToString().Trim() + "'"+
                            " ,'" + Section.Trim()+ "','" + Grade.Trim() + "','" + txtVehicle.Text.ToString().Trim() + "','" + HOD.Trim() + "','"+EmpGroup.Trim()+"')";

                    result = cn.execute_NonQuery(Strsql);

                    try
                    {
                        # region EmployeeShiftMaster Begin
                        string sSql = "";

                        string Shift = "GEN";
                        string ShiftType = "F";
                        DataSet DsEmpGrp = new DataSet();
                        try
                        {
                            //sSql = "Select * from tblEmployeeGroupPolicy where GroupID='" + ddlGroup.SelectedItem.Value.ToString().Trim() + "'";
                            sSql = "Select * from tblEmployeeGroupPolicy where GroupID='" + lookupGrp.Value.ToString().Trim() + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
                            DsEmpGrp = cn.FillDataSet(sSql);
                            if (DsEmpGrp.Tables[0].Rows.Count > 0)
                            {

                                ShiftType = DsEmpGrp.Tables[0].Rows[0]["SHIFTTYPE"].ToString().Trim();
                                Shift = DsEmpGrp.Tables[0].Rows[0]["SHIFT"].ToString().Trim();
                                late = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["PERMISLATEARRIVAL"].ToString().Trim());
                                EarlyDept = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["PERMISEARLYDEPRT"].ToString().Trim());
                                MaxDayWork = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["MAXDAYMIN"].ToString().Trim());
                                PresentMarkingDuration = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["TIME"].ToString().Trim());
                                workinghourforhalfday = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["HALF"].ToString().Trim());
                                workhourforshort = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["SHORT"].ToString().Trim());
                                HalfAfter = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["HLFAfter"].ToString().Trim());
                                HalfBefore = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["HLFBefore"].ToString().Trim());
                                ResignedAfter = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["ResignedAfter"].ToString().Trim());
                             string lblShiftPattern = DsEmpGrp.Tables[0].Rows[0]["SHIFTPATTERN"].ToString().Trim();
                                days = DsEmpGrp.Tables[0].Rows[0]["ALTERNATE_OFF_DAYS"].ToString().Trim();
                                TimeLoss = DsEmpGrp.Tables[0].Rows[0]["ISTIMELOSSALLOWED"].ToString().Trim();
                                RoundClock = DsEmpGrp.Tables[0].Rows[0]["ISROUNDTHECLOCKWORK"].ToString().Trim();
                                isshort = DsEmpGrp.Tables[0].Rows[0]["ISSHORT"].ToString().Trim();
                                if (isshort == "N")
                                {
                                    workhourforshort = 0;
                                }
                                ishalfday = DsEmpGrp.Tables[0].Rows[0]["ISHALFDAY"].ToString().Trim();
                                if (ishalfday == "N")
                                {
                                    workinghourforhalfday = 0;
                                }
                                OS = DsEmpGrp.Tables[0].Rows[0]["ISOS"].ToString().Trim();
                                IsOt = DsEmpGrp.Tables[0].Rows[0]["ISOT"].ToString().Trim();
                                if (IsOt == "N")
                                {
                                    txtOTRate = "000.00";
                                }
                                IsMIS = DsEmpGrp.Tables[0].Rows[0]["MIS"].ToString().Trim();
                                IsCoff = DsEmpGrp.Tables[0].Rows[0]["IsCOF"].ToString().Trim();
                                EnableAutoResign = DsEmpGrp.Tables[0].Rows[0]["EnableAutoResign"].ToString().Trim();
                                inonly = DsEmpGrp.Tables[0].Rows[0]["INONLY"].ToString().Trim();
                                isPunchAll = DsEmpGrp.Tables[0].Rows[0]["ISPUNCHALL"].ToString().Trim();
                                Twopunch = DsEmpGrp.Tables[0].Rows[0]["TWO"].ToString().Trim();
                                IsOutWork = DsEmpGrp.Tables[0].Rows[0]["ISOUTWORK"].ToString().Trim();
                                IsAuthShift = DsEmpGrp.Tables[0].Rows[0]["ISAUTOSHIFT"].ToString().Trim();
                                IsAuthShift = "N";
                               //AuthShifts = "";
                               
                                if (AuthShifts.ToString().Length > 50)
                                {
                                    AuthShifts = AuthShifts.Substring(0, 50);
                                }
                                FirstWeeklyoff = DsEmpGrp.Tables[0].Rows[0]["FIRSTOFFDAY"].ToString().Trim();
                                SecondWeeklyoff = DsEmpGrp.Tables[0].Rows[0]["SECONDOFFDAY"].ToString().Trim();
                                HalfDayShift = DsEmpGrp.Tables[0].Rows[0]["HALFDAYSHIFT"].ToString().Trim();
                                SecondOffType = DsEmpGrp.Tables[0].Rows[0]["SECONDOFFTYPE"].ToString().Trim();
                                days = DsEmpGrp.Tables[0].Rows[0]["ALTERNATE_OFF_DAYS"].ToString().Trim();


                                FourPunch_Night = DsEmpGrp.Tables[0].Rows[0]["NightShiftFourPunch"].ToString().Trim();
                                OTOutMinusShiftEndTime = DsEmpGrp.Tables[0].Rows[0]["ISOTOUTMINUSSHIFTENDTIME"].ToString().Trim();
                                OTWrkgHrsMinusShiftHrs = DsEmpGrp.Tables[0].Rows[0]["ISOTWRKGHRSMINUSSHIFTHRS"].ToString().Trim();
                                OTEarlyComePlusLateDep = DsEmpGrp.Tables[0].Rows[0]["ISOTEARLYCOMEPLUSLATEDEP"].ToString().Trim();

                                OTRound = DsEmpGrp.Tables[0].Rows[0]["OTROUND"].ToString().Trim();
                                OT_EARLYCOMING = DsEmpGrp.Tables[0].Rows[0]["ISOTEARLYCOMING"].ToString().Trim();
                                PresentOn_HLDPresent = DsEmpGrp.Tables[0].Rows[0]["ISPRESENTONHLDPRESENT"].ToString().Trim();
                                PresentOn_WOPresent = DsEmpGrp.Tables[0].Rows[0]["ISPRESENTONWOPRESENT"].ToString().Trim();
                                AbsentOn_WOAbsent = DsEmpGrp.Tables[0].Rows[0]["ISAUTOABSENT"].ToString().Trim();
                                awa = DsEmpGrp.Tables[0].Rows[0]["ISAWA"].ToString().Trim();
                                AW = DsEmpGrp.Tables[0].Rows[0]["ISAW"].ToString().Trim();
                                WA = DsEmpGrp.Tables[0].Rows[0]["ISWA"].ToString().Trim();
                                Wo_Absent = DsEmpGrp.Tables[0].Rows[0]["ISPREWO"].ToString().Trim();
                                EndTime_In = DsEmpGrp.Tables[0].Rows[0]["S_END"].ToString().Trim();
                                EndTime_RTCEmp = DsEmpGrp.Tables[0].Rows[0]["S_OUT"].ToString().Trim();
                                DEDUCTHOLIDAYOT = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["DEDUCTHOLIDAYOT"].ToString().Trim());
                                DEDUCTWOOT = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["DEDUCTWOOT"].ToString().Trim());
                                ISOTEARLYCOMING = DsEmpGrp.Tables[0].Rows[0]["ISOTEARLYCOMING"].ToString().Trim();
                                OTMinus = DsEmpGrp.Tables[0].Rows[0]["OTMinus"].ToString().Trim();
                                OTEARLYDUR = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["OTEARLYDUR"].ToString().Trim());
                                OTLATECOMINGDUR = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["OTLATECOMINGDUR"].ToString().Trim());
                                OTRESTRICTENDDUR = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["OTRESTRICTENDDUR"].ToString().Trim());
                                DUPLICATECHECKMIN = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["DUPLICATECHECKMIN"].ToString().Trim());
                                PREWO = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["PREWO"].ToString().Trim());
                                AUTOSHIFT_LOW = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["AUTOSHIFT_LOW"].ToString().Trim());
                                AUTOSHIFT_UP = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["AUTOSHIFT_UP"].ToString().Trim());

                                DateTime EndTime_InD = Convert.ToDateTime(EndTime_In);
                                DateTime EndTime_RTCEmpD = Convert.ToDateTime(EndTime_RTCEmp);





                                Strsql1 = "";
                                Strsql1 += "insert into tblEmployeeShiftMaster(Paycode,CARDNO,SHIFT,SHIFTTYPE,SHIFTPATTERN, " +
                                    "SHIFTREMAINDAYS,INONLY,ISPUNCHALL,ISTIMELOSSALLOWED,ALTERNATE_OFF_DAYS,CDAYS,ISROUNDTHECLOCKWORK,ISOT, " +
                                    "OTRATE,FIRSTOFFDAY,SECONDOFFTYPE,HALFDAYSHIFT,SECONDOFFDAY,PERMISLATEARRIVAL,PERMISEARLYDEPRT,ISAUTOSHIFT,IsOutWork,MAXDAYMIN," +
                                    "ISOS,AUTH_SHIFTS,[TIME],Short,Half,ISHALFDAY,ISSHORT,TWO,SSN,MIS,IsCOF,HLFAfter,HLFBefore,ResignedAfter,EnableAutoResign,[S_END],[S_OUT] " +
                                    ",[AUTOSHIFT_LOW],[AUTOSHIFT_UP],[ISPRESENTONWOPRESENT],[ISPRESENTONHLDPRESENT],[NightShiftFourPunch],[ISAUTOABSENT],[ISAWA],[ISWA],[ISAW],[ISPREWO],[ISOTOUTMINUSSHIFTENDTIME]" +
                                    ",[ISOTWRKGHRSMINUSSHIFTHRS],[ISOTEARLYCOMEPLUSLATEDEP],[DEDUCTHOLIDAYOT],[DEDUCTWOOT],[ISOTEARLYCOMING],[OTMinus],[OTROUND],[OTEARLYDUR],[OTLATECOMINGDUR],[OTRESTRICTENDDUR],[DUPLICATECHECKMIN]" +
                                    ",[PREWO])values ";
                                Strsql1 += "('" + txtEmpCode.Text.Trim().ToString().Trim() + "','" + CardNo.ToString().Trim() + "', ";
                                Strsql1 += " '"+Shift+"','"+ ShiftType+"', ";
                                Strsql1 += " '','', ";
                                Strsql1 += " '" + inonly.ToString().Trim() + "','" + isPunchAll.ToString().Trim() + "', ";
                                Strsql1 += " '" + TimeLoss.ToString().Trim() + "','" + days.ToString().Trim() + "','', ";
                                Strsql1 += " '" + RoundClock.ToString().Trim() + "','" + IsOt.ToString().Trim() + "' , ";
                                Strsql1 += " '" + txtOTRate.ToString().Trim() + "' , '" + FirstWeeklyoff.ToString().Trim() + "','" + SecondOffType.ToString().ToString().Trim() + "', ";
                                Strsql1 += " '" + HalfDayShift.ToString().Trim() + "','" + SecondWeeklyoff.ToString().Trim() + "','" + late.ToString().Trim() + "', ";
                                Strsql1 += " '" + EarlyDept.ToString().Trim() + "','" + IsAuthShift.ToString().Trim() + "','" + IsOutWork.ToString().Trim() + "','" + MaxDayWork.ToString().Trim() + "' , ";
                                Strsql1 += " '" + OS.ToString().Trim() + "','" + AuthShifts.ToString() + "', ";
                                Strsql1 += " '" + PresentMarkingDuration.ToString().Trim() + "','" + workhourforshort.ToString().Trim() + "', ";
                                Strsql1 += " '" + workinghourforhalfday.ToString().Trim() + "','" + ishalfday.ToString().Trim() + "','" + isshort.ToString().Trim() + "', ";
                                Strsql1 += " '" + Twopunch.ToString().Trim() + "','" + ESSN + "', '" + IsMIS + "','" + IsCoff + "','" + HalfAfter + "','" + HalfBefore + "','" + ResignedAfter + "','" + EnableAutoResign + "',";
                                Strsql1 += " '" + EndTime_InD.ToString("yyyy-MM-dd HH:mm") + "','" + EndTime_RTCEmpD.ToString("yyyy-MM-dd HH:mm") + "', '" + AUTOSHIFT_LOW + "','" + AUTOSHIFT_UP + "','" + PresentOn_WOPresent + "','" + PresentOn_HLDPresent + "','" + FourPunch_Night + "','" + AbsentOn_WOAbsent + "','" + awa + "',";
                                Strsql1 += " '" + WA.ToString().Trim() + "','" + AW.ToString().Trim() + "','" + Wo_Absent.ToString().Trim() + "','" + OTOutMinusShiftEndTime.ToString().Trim() + "',";
                                Strsql1 += " '" + OTWrkgHrsMinusShiftHrs.ToString().Trim() + "','" + OTEarlyComePlusLateDep.ToString().Trim() + "','" + DEDUCTHOLIDAYOT.ToString().Trim() + "','" + DEDUCTWOOT.ToString().Trim() + "',";
                                Strsql1 += " '" + ISOTEARLYCOMING.ToString().Trim() + "','" + OTMinus.ToString().Trim() + "','" + OTRound.ToString().Trim() + "','" + OTEARLYDUR.ToString().Trim() + "','" + OTLATECOMINGDUR.ToString().Trim() + "',";
                                Strsql1 += " '" + OTRESTRICTENDDUR.ToString().Trim() + "','" + DUPLICATECHECKMIN.ToString().Trim() + "','" + PREWO.ToString().Trim() + "')";
                                result1 = cn.execute_NonQuery(Strsql1);

                            }

                        }
                        catch
                        {

                            Strsql1 = "";
                            Strsql1 += "insert into tblEmployeeShiftMaster(Paycode,CARDNO,SHIFT,SHIFTTYPE,SHIFTPATTERN, " +
                                "SHIFTREMAINDAYS,INONLY,ISPUNCHALL,ISTIMELOSSALLOWED,ALTERNATE_OFF_DAYS,CDAYS,ISROUNDTHECLOCKWORK,ISOT, " +
                                "OTRATE,FIRSTOFFDAY,SECONDOFFTYPE,HALFDAYSHIFT,SECONDOFFDAY,PERMISLATEARRIVAL,PERMISEARLYDEPRT,ISAUTOSHIFT,IsOutWork,MAXDAYMIN," +
                                "ISOS,AUTH_SHIFTS,[TIME],Short,Half,ISHALFDAY,ISSHORT,TWO,SSN,MIS,IsCOF,HLFAfter,HLFBefore,ResignedAfter,EnableAutoResign,[S_END],[S_OUT] " +
                                ",[AUTOSHIFT_LOW],[AUTOSHIFT_UP],[ISPRESENTONWOPRESENT],[ISPRESENTONHLDPRESENT],[NightShiftFourPunch],[ISAUTOABSENT],[ISAWA],[ISWA],[ISAW],[ISPREWO],[ISOTOUTMINUSSHIFTENDTIME]" +
                                ",[ISOTWRKGHRSMINUSSHIFTHRS],[ISOTEARLYCOMEPLUSLATEDEP],[DEDUCTHOLIDAYOT],[DEDUCTWOOT],[ISOTEARLYCOMING],[OTMinus],[OTROUND],[OTEARLYDUR],[OTLATECOMINGDUR],[OTRESTRICTENDDUR],[DUPLICATECHECKMIN]" +
                                ",[PREWO])values ";
                            Strsql1 += "('" + txtEmpCode.Text.Trim().ToString().Trim() + "','" + CardNo.ToString().Trim() + "', ";
                            Strsql1 += " 'GEN','F', ";
                            Strsql1 += " '','', ";
                            Strsql1 += " '" + inonly.ToString().Trim() + "','" + isPunchAll.ToString().Trim() + "', ";
                            Strsql1 += " '" + TimeLoss.ToString().Trim() + "','" + days.ToString().Trim() + "','', ";
                            Strsql1 += " '" + RoundClock.ToString().Trim() + "','" + IsOt.ToString().Trim() + "' , ";
                            Strsql1 += " '" + txtOTRate.ToString().Trim() + "' , '" + FirstWeeklyoff.ToString().Trim() + "','" + SecondOffType.ToString().ToString().Trim() + "', ";
                            Strsql1 += " '" + HalfDayShift.ToString().Trim() + "','" + SecondWeeklyoff.ToString().Trim() + "','" + late.ToString().Trim() + "', ";
                            Strsql1 += " '" + EarlyDept.ToString().Trim() + "','" + IsAuthShift.ToString().Trim() + "','" + IsOutWork.ToString().Trim() + "','" + MaxDayWork.ToString().Trim() + "' , ";
                            Strsql1 += " '" + OS.ToString().Trim() + "','" + AuthShifts.ToString() + "', ";
                            Strsql1 += " '" + PresentMarkingDuration.ToString().Trim() + "','" + workhourforshort.ToString().Trim() + "', ";
                            Strsql1 += " '" + workinghourforhalfday.ToString().Trim() + "','" + ishalfday.ToString().Trim() + "','" + isshort.ToString().Trim() + "', ";
                            Strsql1 += " '" + Twopunch.ToString().Trim() + "','" + ESSN + "', '" + IsMIS + "','" + IsCoff + "','" + HalfAfter + "','" + HalfBefore + "','" + ResignedAfter + "','" + EnableAutoResign + "',";
                            Strsql1 += " '" + EndTime_In.ToString().Trim() + "','" + EndTime_RTCEmp + "', " + AUTOSHIFT_LOW + ",'" + AUTOSHIFT_UP + "','" + PresentOn_WOPresent + "','" + PresentOn_HLDPresent + "','" + FourPunch_Night + "','" + AbsentOn_WOAbsent + "','" + awa + "',";
                            Strsql1 += " '" + WA.ToString().Trim() + "','" + AW.ToString().Trim() + "','" + Wo_Absent.ToString().Trim() + "','" + OTOutMinusShiftEndTime.ToString().Trim() + "',";
                            Strsql1 += " '" + OTWrkgHrsMinusShiftHrs.ToString().Trim() + "','" + OTEarlyComePlusLateDep.ToString().Trim() + "','" + DEDUCTHOLIDAYOT.ToString().Trim() + "','" + DEDUCTWOOT.ToString().Trim() + "',";
                            Strsql1 += " '" + ISOTEARLYCOMING.ToString().Trim() + "','" + OTMinus.ToString().Trim() + "','" + OTRound.ToString().Trim() + "','" + OTEARLYDUR.ToString().Trim() + "','" + OTLATECOMINGDUR.ToString().Trim() + "',";
                            Strsql1 += " '" + OTRESTRICTENDDUR.ToString().Trim() + "','" + DUPLICATECHECKMIN.ToString().Trim() + "','" + PREWO.ToString().Trim() + "')";
                            result1 = cn.execute_NonQuery(Strsql1);
                        }

                        #endregion
                    }
                    catch
                    {

                    }



                  string  Strsql2 = "";
                    Strsql2 = "insert into tbluser(user_r,USERDESCRIPRION,password,UserType,OTCAL,Paycode,trainer,OTApproval,ISOA,CompAdd,Compmodi,compdel, " +
                        "DeptAdd,deptmodi,deptdel,catadd,catmodi,catdel,secadd,secmodi,secdel,grdadd,grdmodi,grddel,sftadd,sftmodi,sftdel,Empadd,EmpModi,Empdel, " +
                        "DataMaintenance,SSN,CompanyCode) values " +
                        "('" + txtEmpCode.Text.Trim().ToString() + "','" + txtName.Text.ToString().Trim() + "', " +
                        " '" + txtEmpCode.Text.ToString() + "','U','N','" + txtEmpCode.Text.ToString() + "','N','N','N', " +
                        " 'Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','N','" + ESSN + "','"+lookupComp.Value.ToString().Trim()+"')";
                   int result2 = cn.execute_NonQuery(Strsql2);
                    try
                    {
                        trans.Commit();

                        Strsql = "";
                        Strsql = "Insert into tblleaveledger (paycode,lyear,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13,L14,L15,L16,L17,L18,L19,L20,L01_Add,L02_Add,L03_Add,L04_Add,L05_Add,L06_Add,L07_Add,L08_Add,L09_Add,L10_Add,L11_Add,L12_Add,L13_Add,L14_Add,L15_Add,L16_Add,L17_Add,L18_Add,L19_Add,L20_Add,L01_CASH,L02_CASH,L03_CASH,L04_CASH,L05_CASH,L06_CASH,L07_CASH,L08_CASH,L09_CASH,L10_CASH,L11_CASH,L12_CASH,L13_CASH,L14_CASH,L15_CASH,L16_CASH,L17_CASH,L18_CASH,L19_CASH,L20_CASH,SSN) values('" + txtEmpCode.Text + "','" + DOJ.Year.ToString("0000") + "',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'" + ESSN + "')";
                        cn.execute_NonQuery(Strsql);
                        Strsql = "";
                        if (DOJ.Year<System.DateTime.Now.Year)
                        {
                            DateTime nowDate = DateTime.Now;
                            DateTime firstDayCurrentYear = new DateTime(nowDate.Year, 1, 1);
                            DOJ = firstDayCurrentYear;
                        }

                        try
                        {
                            using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString()))
                            {
                                MyCmd = new OleDbCommand("ProcessDutyRosterForCreate", MyConn);
                                MyCmd.CommandTimeout = 1800;
                                MyCmd.CommandType = CommandType.StoredProcedure;
                                MyCmd.Parameters.Add("@PayCode", SqlDbType.VarChar).Value = txtEmpCode.Text;
                                MyCmd.Parameters.Add("@FromDate", SqlDbType.VarChar).Value = DOJ.ToString("yyyy-MM-dd");
                                MyCmd.Parameters.Add("@WO_Include", SqlDbType.VarChar).Value = 'N';
                                MyCmd.Parameters.Add("@SSN", SqlDbType.VarChar).Value = ESSN;
                                MyConn.Open();
                                result = MyCmd.ExecuteNonQuery();
                                MyConn.Close();

                            }
                        }
                        catch
                        {


                        }


                        Strsql = "";

                        if (FileImage.HasFile)
                        {
                            FileImage.PostedFile.SaveAs(location);
                        }
                        if (FileSignature.HasFile)
                        {
                            FileSignature.PostedFile.SaveAs(EmpSignatureLocation);
                        }

                        Response.Redirect("frmEmployee.aspx");
                    }
                    catch
                    {
                        try
                        {
                            trans.Rollback();
                        }
                        catch { }
                    }
                    finally
                    {
                        trans.Dispose(); con.Dispose(); cmd.Dispose();
                    }
                    {
                        trans.Commit();
                        if (ddlActive.SelectedItem.Value.ToString().Trim() == "N")
                        {
                            Strsql = "Delete from tbltimeregister where paycode='" + txtEmpCode.Text.ToString().Trim() + "' and dateoffice >= '" + DL + "' ";
                            result = cn.execute_NonQuery(Strsql);
                        }

                        if (FileImage.HasFile)
                        {
                            FileImage.PostedFile.SaveAs(location);
                        }
                        if (FileSignature.HasFile)
                        {
                            FileSignature.PostedFile.SaveAs(EmpSignatureLocation);
                        }
                        Session["IsNewRec"] = null;
                        Response.Redirect("frmEmployee.aspx");
                    }
                }
               
                
            }
       
        }
        catch (Exception Ex)
        {
            Error_Occured("Save", Ex.Message);
        }
    }
    protected void UpdateTblEmpShiftMaster(string SSN)
    {
        try
        {

            string inonly = "N";
            string isPunchAll = "Y";
            string TimeLoss = "Y";
            string RoundClock = "N";
            string IsOt = "Y";
            string IsAuthShift = "N";
            string AuthShifts = "";
            string OS = "N";
            string ishalfday = "Y";
            string isshort = "Y";
            string Twopunch = "N";
            string days = "";
            string IsOutWork = "Y";
            string IsCoff = "O";
            string IsMIS = "P";
            int late = 10;
            int EarlyDept = 10;
            int MaxDayWork = 720;
            int PresentMarkingDuration = 240;
            int workinghourforhalfday = 300;
            int workhourforshort = 420;
            int HalfAfter = 60;
            int HalfBefore = 60;
            int ResignedAfter = 0;
            string FourPunch_Night = "N";
            string OTOutMinusShiftEndTime = "Y";
            string OTWrkgHrsMinusShiftHrs = "N";
            string OTEarlyComePlusLateDep = "N";
            string OTRound = "N";
            string OT_EARLYCOMING = "N";
            string PresentOn_HLDPresent = "N";
            string PresentOn_WOPresent = "N";
            string AbsentOn_WOAbsent = "N";
            string awa = "N";
            string AW = "N";
            string WA = "N";
            string Wo_Absent = "N";
            string EndTime_In = "05:00";
            string EndTime_RTCEmp = "05:00";
            int AUTOSHIFT_LOW = 240;
            int AUTOSHIFT_UP = 240;
            int DEDUCTHOLIDAYOT = 0;
            int DEDUCTWOOT = 0;
            string ISOTEARLYCOMING = "N";
            string OTMinus = "N";
            int OTEARLYDUR = 0;
            int OTLATECOMINGDUR = 0;
            int OTRESTRICTENDDUR = 0;
            int DUPLICATECHECKMIN = 0;
            int PREWO = 0;
            TimeLoss = "Y";
            RoundClock = "N";
            isshort = "Y";
            ishalfday = "Y";
            OS = "N";
            IsOt = "N";
            string txtOTRate = "000.00";
            IsMIS = "H";
            IsCoff = "O";
            string EnableAutoResign = "N";
            inonly = "N";
            isPunchAll = "Y";
            Twopunch = "N";
            IsOutWork = "N";
            IsAuthShift = "N";
            AuthShifts = "";
            FirstWeeklyoff = "SUN";
            SecondWeeklyoff = "NON";
            HalfDayShift = " ";
            SecondOffType = "";
            days = "";
            string Shift = "";
            string ShiftType = "";
            try
            {
                # region EmployeeShiftMaster Begin
                string sSql = "";
                DataSet DsEmpGrp = new DataSet();
                try
                {
                    //sSql = "Select * from tblEmployeeGroupPolicy where GroupID='" + ddlGroup.SelectedItem.Value.ToString().Trim() + "'";
                    sSql = "Select * from tblEmployeeGroupPolicy where GroupID='" + lookupGrp.Value.ToString().Trim() + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
                    DsEmpGrp = cn.FillDataSet(sSql);
                    if (DsEmpGrp.Tables[0].Rows.Count > 0)
                    {
                        Shift = DsEmpGrp.Tables[0].Rows[0]["SHIFT"].ToString().Trim();
                        ShiftType = DsEmpGrp.Tables[0].Rows[0]["SHIFTTYPE"].ToString().Trim();
                        late = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["PERMISLATEARRIVAL"].ToString().Trim());
                        EarlyDept = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["PERMISEARLYDEPRT"].ToString().Trim());
                        MaxDayWork = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["MAXDAYMIN"].ToString().Trim());
                        PresentMarkingDuration = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["TIME"].ToString().Trim());
                        workinghourforhalfday = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["HALF"].ToString().Trim());
                        workhourforshort = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["SHORT"].ToString().Trim());
                        HalfAfter = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["HLFAfter"].ToString().Trim());
                        HalfBefore = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["HLFBefore"].ToString().Trim());
                        ResignedAfter = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["ResignedAfter"].ToString().Trim());
                        string lblShiftPattern = DsEmpGrp.Tables[0].Rows[0]["SHIFTPATTERN"].ToString().Trim();
                        days = DsEmpGrp.Tables[0].Rows[0]["ALTERNATE_OFF_DAYS"].ToString().Trim();
                        TimeLoss = DsEmpGrp.Tables[0].Rows[0]["ISTIMELOSSALLOWED"].ToString().Trim();
                        RoundClock = DsEmpGrp.Tables[0].Rows[0]["ISROUNDTHECLOCKWORK"].ToString().Trim();
                        isshort = DsEmpGrp.Tables[0].Rows[0]["ISSHORT"].ToString().Trim();
                        if (isshort == "N")
                        {
                            workhourforshort = 0;
                        }
                        ishalfday = DsEmpGrp.Tables[0].Rows[0]["ISHALFDAY"].ToString().Trim();
                        if (ishalfday == "N")
                        {
                            workinghourforhalfday = 0;
                        }
                        OS = DsEmpGrp.Tables[0].Rows[0]["ISOS"].ToString().Trim();
                        IsOt = DsEmpGrp.Tables[0].Rows[0]["ISOT"].ToString().Trim();
                        if (IsOt == "N")
                        {
                            txtOTRate = "000.00";
                        }
                        IsMIS = DsEmpGrp.Tables[0].Rows[0]["MIS"].ToString().Trim();
                        IsCoff = DsEmpGrp.Tables[0].Rows[0]["IsCOF"].ToString().Trim();
                        EnableAutoResign = DsEmpGrp.Tables[0].Rows[0]["EnableAutoResign"].ToString().Trim();
                        inonly = DsEmpGrp.Tables[0].Rows[0]["INONLY"].ToString().Trim();
                        isPunchAll = DsEmpGrp.Tables[0].Rows[0]["ISPUNCHALL"].ToString().Trim();
                        Twopunch = DsEmpGrp.Tables[0].Rows[0]["TWO"].ToString().Trim();
                        IsOutWork = DsEmpGrp.Tables[0].Rows[0]["ISOUTWORK"].ToString().Trim();
                        IsAuthShift = DsEmpGrp.Tables[0].Rows[0]["ISAUTOSHIFT"].ToString().Trim();
                        IsAuthShift = "N";
                        AuthShifts = "";

                        if (AuthShifts.ToString().Length > 50)
                        {
                            AuthShifts = AuthShifts.Substring(0, 50);
                        }
                        FirstWeeklyoff = DsEmpGrp.Tables[0].Rows[0]["FIRSTOFFDAY"].ToString().Trim();
                        SecondWeeklyoff = DsEmpGrp.Tables[0].Rows[0]["SECONDOFFDAY"].ToString().Trim();
                        HalfDayShift = DsEmpGrp.Tables[0].Rows[0]["HALFDAYSHIFT"].ToString().Trim();
                        SecondOffType = DsEmpGrp.Tables[0].Rows[0]["SECONDOFFTYPE"].ToString().Trim();
                        days = DsEmpGrp.Tables[0].Rows[0]["ALTERNATE_OFF_DAYS"].ToString().Trim();


                        FourPunch_Night = DsEmpGrp.Tables[0].Rows[0]["NightShiftFourPunch"].ToString().Trim();
                        OTOutMinusShiftEndTime = DsEmpGrp.Tables[0].Rows[0]["ISOTOUTMINUSSHIFTENDTIME"].ToString().Trim();
                        OTWrkgHrsMinusShiftHrs = DsEmpGrp.Tables[0].Rows[0]["ISOTWRKGHRSMINUSSHIFTHRS"].ToString().Trim();
                        OTEarlyComePlusLateDep = DsEmpGrp.Tables[0].Rows[0]["ISOTEARLYCOMEPLUSLATEDEP"].ToString().Trim();

                        OTRound = DsEmpGrp.Tables[0].Rows[0]["OTROUND"].ToString().Trim();
                        OT_EARLYCOMING = DsEmpGrp.Tables[0].Rows[0]["ISOTEARLYCOMING"].ToString().Trim();
                        PresentOn_HLDPresent = DsEmpGrp.Tables[0].Rows[0]["ISPRESENTONHLDPRESENT"].ToString().Trim();
                        PresentOn_WOPresent = DsEmpGrp.Tables[0].Rows[0]["ISPRESENTONWOPRESENT"].ToString().Trim();
                        AbsentOn_WOAbsent = DsEmpGrp.Tables[0].Rows[0]["ISAUTOABSENT"].ToString().Trim();
                        awa = DsEmpGrp.Tables[0].Rows[0]["ISAWA"].ToString().Trim();
                        AW = DsEmpGrp.Tables[0].Rows[0]["ISAW"].ToString().Trim();
                        WA = DsEmpGrp.Tables[0].Rows[0]["ISWA"].ToString().Trim();
                        Wo_Absent = DsEmpGrp.Tables[0].Rows[0]["ISPREWO"].ToString().Trim();
                        EndTime_In = DsEmpGrp.Tables[0].Rows[0]["S_END"].ToString().Trim();
                        EndTime_RTCEmp = DsEmpGrp.Tables[0].Rows[0]["S_OUT"].ToString().Trim();
                        DEDUCTHOLIDAYOT = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["DEDUCTHOLIDAYOT"].ToString().Trim());
                        DEDUCTWOOT = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["DEDUCTWOOT"].ToString().Trim());
                        ISOTEARLYCOMING = DsEmpGrp.Tables[0].Rows[0]["ISOTEARLYCOMING"].ToString().Trim();
                        OTMinus = DsEmpGrp.Tables[0].Rows[0]["OTMinus"].ToString().Trim();
                        OTEARLYDUR = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["OTEARLYDUR"].ToString().Trim());
                        OTLATECOMINGDUR = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["OTLATECOMINGDUR"].ToString().Trim());
                        OTRESTRICTENDDUR = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["OTRESTRICTENDDUR"].ToString().Trim());
                        DUPLICATECHECKMIN = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["DUPLICATECHECKMIN"].ToString().Trim());
                        PREWO = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["PREWO"].ToString().Trim());
                        AUTOSHIFT_LOW = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["AUTOSHIFT_LOW"].ToString().Trim());
                        AUTOSHIFT_UP = Convert.ToInt32(DsEmpGrp.Tables[0].Rows[0]["AUTOSHIFT_UP"].ToString().Trim());

                        DateTime EndTime_InD = Convert.ToDateTime(EndTime_In);
                        DateTime EndTime_RTCEmpD = Convert.ToDateTime(EndTime_RTCEmp);





                        Strsql1 = "";
                        Strsql1 += "update tblEmployeeShiftMaster set CARDNO='"+txtCardNo.Text.ToString().Trim()+"',  SHIFT='" + Shift + "',SHIFTTYPE='" + ShiftType + "',SHIFTPATTERN='" + DsEmpGrp.Tables[0].Rows[0]["SHIFTPATTERN"].ToString().Trim() + "', " +
                            "SHIFTREMAINDAYS='" + DsEmpGrp.Tables[0].Rows[0]["SHIFTREMAINDAYS"].ToString().Trim() + "',INONLY='" + inonly + "',ISPUNCHALL='" + isPunchAll + "',ISTIMELOSSALLOWED='" + TimeLoss + "',ALTERNATE_OFF_DAYS='" + days + "',CDAYS='" + DsEmpGrp.Tables[0].Rows[0]["CDAYS"].ToString().Trim() + "',ISROUNDTHECLOCKWORK='" + RoundClock + "',ISOT='" + IsOt + "', " +
                            "OTRATE='" + txtOTRate + "',FIRSTOFFDAY='" + FirstWeeklyoff + "',SECONDOFFTYPE='" + SecondOffType + "',HALFDAYSHIFT='" + HalfDayShift + "',SECONDOFFDAY='" + SecondWeeklyoff + "',PERMISLATEARRIVAL='" + late + "',PERMISEARLYDEPRT='" + EarlyDept + "',ISAUTOSHIFT='" + IsAuthShift + "',IsOutWork='" + IsOutWork + "',MAXDAYMIN='" + MaxDayWork + "'," +
                            "ISOS='" + OS + "',AUTH_SHIFTS='" + AuthShifts + "',[TIME]='" + PresentMarkingDuration + "',Short='" + workhourforshort + "',Half='" + workinghourforhalfday + "',ISHALFDAY='" + ishalfday + "',ISSHORT='" + isshort + "',TWO='" + Twopunch + "',MIS='" + IsMIS + "',IsCOF='" + IsCoff + "',HLFAfter='" + HalfAfter + "',HLFBefore='" + HalfBefore + "',ResignedAfter='" + ResignedAfter + "',EnableAutoResign='" + EnableAutoResign + "',[S_END]='" + EndTime_InD.ToString("yyyy-MM-dd HH:mm") + "',[S_OUT]='" + EndTime_RTCEmpD.ToString("yyyy-MM-dd HH:mm") + "' " +
                            ",[AUTOSHIFT_LOW]='" + AUTOSHIFT_LOW + "',[AUTOSHIFT_UP]='" + AUTOSHIFT_UP + "',[ISPRESENTONWOPRESENT]='" + DsEmpGrp.Tables[0].Rows[0]["ISPRESENTONWOPRESENT"].ToString().Trim() + "',[ISPRESENTONHLDPRESENT]='" + DsEmpGrp.Tables[0].Rows[0]["ISPRESENTONHLDPRESENT"].ToString().Trim() + "',[NightShiftFourPunch]='" + FourPunch_Night + "',[ISAUTOABSENT]='" + AbsentOn_WOAbsent + "',[ISAWA]='" + awa + "',[ISWA]='" + WA + "',[ISAW]='" + AW + "',[ISPREWO]='" + Wo_Absent + "',[ISOTOUTMINUSSHIFTENDTIME]='" + OTOutMinusShiftEndTime + "'" +
                            ",[ISOTWRKGHRSMINUSSHIFTHRS]='" + OTWrkgHrsMinusShiftHrs + "',[ISOTEARLYCOMEPLUSLATEDEP]='" + OTEarlyComePlusLateDep + "',[DEDUCTHOLIDAYOT]='" + DEDUCTHOLIDAYOT + "',[DEDUCTWOOT]='" + DEDUCTWOOT + "',[ISOTEARLYCOMING]='" + ISOTEARLYCOMING + "',[OTMinus]='" + OTMinus + "',[OTROUND]='" + OTRound + "',[OTEARLYDUR]='" + OTEARLYDUR + "',[OTLATECOMINGDUR]='" + OTLATECOMINGDUR + "',[OTRESTRICTENDDUR]='" + OTRESTRICTENDDUR + "',[DUPLICATECHECKMIN]='" + DUPLICATECHECKMIN + "'" +
                            ",[PREWO]='" + PREWO + "' where SSN='" + SSN + "' ";
                        cn.execute_NonQuery(Strsql1);

                    }

                }
                catch
                {


                }

                #endregion
            }
            catch
            {

            }
        }
        catch
        {

        }
    }
    protected void ddlActive_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
           if (ddlActive.SelectedItem.Text.ToString().Trim().ToUpper()=="FALSE")
           {
               txtLeavingreason.Visible = true;
               txtLeavingDate.Visible = true;
               lblReason.Visible = true;
               lblLeavingDate.Visible = true;
           }
           else
           {
               txtLeavingreason.Visible = false;
               txtLeavingDate.Visible = false;
               lblReason.Visible = false;
               lblLeavingDate.Visible = false;
           }
        }
        catch
        {

        }
    }


    protected void txtDateOfjoin_CalendarDayCellPrepared(object sender, DevExpress.Web.CalendarDayCellPreparedEventArgs e)
    {

        if (e.Date.DayOfWeek == DayOfWeek.Sunday || e.Date.DayOfWeek == DayOfWeek.Saturday)
        {
            e.Cell.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
            e.Cell.ForeColor = System.Drawing.Color.Black;
        }
    }
    protected void txtDOB_CalendarDayCellPrepared(object sender, DevExpress.Web.CalendarDayCellPreparedEventArgs e)
    {

        if (e.Date.DayOfWeek == DayOfWeek.Sunday || e.Date.DayOfWeek == DayOfWeek.Saturday)
        {
            e.Cell.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
            e.Cell.ForeColor = System.Drawing.Color.Black;
        }
    }
    protected void SetImage_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            EmployeeImage = "";
            if (FileImage.HasFile)
            {
                int strlen = FileImage.FileName.Length;

                if (FileImage.FileName.ToLower().Substring(strlen - 4, 4) != ".jpg")
                {
                    string change = "<script language='JavaScript'>alert('Invalid File. Only Hpg File ')</script>";
                    Page.RegisterStartupScript("frmchange", change);
                    return;
                   
                }

                string savePath = Server.MapPath(".") + "\\Emp_Img\\";
                string filename = FileImage.FileName.ToString().Trim();
                filename = txtEmpCode.Text.ToString().Trim() + ".jpg";
                if (!System.IO.Directory.Exists(savePath))
                {
                    System.IO.Directory.CreateDirectory(savePath);
                }

                savePath = savePath +  filename;
                EmployeeImage = savePath;
                FileImage.PostedFile.SaveAs(savePath);
               imgEmployee.ImageUrl = "~/Emp_Img/" +  filename;
                //imgEmployee.ImageUrl = Server.MapPath(".") + "\\Emp_Img\\"+filename;
               
              
            }
        }
        catch
        {

        }
    }
}