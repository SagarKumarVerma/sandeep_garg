using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.IO;

/// <summary>
/// Summary description for Class_Print
/// </summary>
public class Class_Print
{
    PrinterHelper ph = new PrinterHelper();

	public Class_Print()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public void CreateReportFile(byte[] returnValue,string Format)
    {
        ErrorClass ec = new ErrorClass();
        ec.TrapError("Rpt_absent", "CreateReportFile", "**** starts *** ");
        
        
        
       


        //if (PrinterName.ToString().Trim() == "Printer_NotSet")
        //{
        //    ScriptManager.RegisterStartupScript(this.GetType(), "Alert", "alert('No default Printer found to print.');", true);
        //    HttpContext.Current.Response.Redirect("../SetPrinter.aspx");
        //    return;
        //}
        //if (PrinterName.ToString().Trim() == "Printer_NotFound")
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('No Printer found to print.');", true);
        //    HttpContext.Current.Response.Redirect("../SetPrinter.aspx");
        //    return;
        //}

        string Fname="";
        Fname = HttpContext.Current.Server.MapPath("~/") + "TmpRpts\\TimeWatch" + System.DateTime.Now.ToString("HHmmss") + "." +Format.ToString();
        //Fname = HttpContext.Current.Server.MapPath("~/") + "TmpRpts\\TimeWatch" + System.DateTime.Now.ToString("HHmmss") + ".xls";
        ec.TrapError("Rpt_absent", "CreateReportFile", "File to print:" + Fname );
 
        FileStream fs = new FileStream(Fname, FileMode.Create);

        fs.Write(returnValue, 0, returnValue.Length);
        fs.Close();

        ec.TrapError("Rpt_absent", "CreateReportFile", "passed 1");

        // Create New instance of FileInfo class to get the properties of the file being downloaded   
        FileInfo myfile = new FileInfo(Fname);     // Checking if file exists   
        if (myfile.Exists)
        {
            //System.Web.HttpContext.Current.Response.ClearContent();
            //System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + myfile.Name);
            //System.Web.HttpContext.Current.Response.AddHeader("Content-Length", myfile.Length.ToString());
            //System.Web.HttpContext.Current.Response.ContentType = myfile.Extension.ToLower();
            //System.Web.HttpContext.Current.Response.TransmitFile(myfile.FullName);
            //System.Web.HttpContext.Current.Response.End();

            ec.TrapError("Rpt_absent", "CreateReportFile", "Passed 2 ");

            //PrinterHelper.SetDefaultPrinter(PrinterName);
            //PrinterHelper.AddPrinterConnection(PrinterName);
            ec.TrapError("Rpt_absent", "CreateReportFile", "Passed 3 ");
            //ph.PrintFile(Fname, PrinterName, 1);
            ec.TrapError("Rpt_absent", "CreateReportFile", "Passed 4 ");
        }
        ec.TrapError("Rpt_absent", "CreateReportFile", "End**** ");

    }

    //SendStringToPrinter

}
