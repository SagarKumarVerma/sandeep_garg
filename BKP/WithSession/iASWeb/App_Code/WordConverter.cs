using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for WordConverter
/// </summary>
public class WordConverter
{
    
    
    
        /// <summary>
        /// Initializes a new instance of WordConverter



        private string[] lStrOneDigArray = { "ONE", "TWO", "THREE", "FOUR", "FIVE", "SIX", "SEVEN", "EIGHT", "NINE" };
        private string[] lStrTwoOneArray = {"TEN","ELEVEN","TWELVE","THIRTEEN","FOURTEEN",
                                                                           "FIFTEEN","SIXTEEN","SEVENTEEN","EIGHTEEN","NINTEEN"};
        private string[] lStrTwoDigArray = { "TWENTY", "THIRTY", "FOURTY", "FIFTY", "SIXTY", "SEVENTY", "EIGHTY", "NINTY" };

        private string OneDigit(int lIntValue)
        {
            try
            {
                return (this.lStrOneDigArray[lIntValue - 1]);
            }
            catch
            {
                return "";
            }
        }

        private string TwoDigit(int lIntValue)
        {
            try
            {
                if (lIntValue == 0) return "";
                int lIntFirstDig, lIntSecDig;
                lIntFirstDig = lIntValue / 10;
                lIntSecDig = lIntValue % 10;
                if (lIntFirstDig == 0)
                    return (this.lStrOneDigArray[lIntSecDig - 1]);
                if (lIntFirstDig == 1)
                    return this.lStrTwoOneArray[lIntSecDig];

                return (lIntSecDig == 0) ? this.lStrTwoDigArray[lIntFirstDig - 2] :
                                                  this.lStrTwoDigArray[lIntFirstDig - 2] + " " + this.lStrOneDigArray[lIntSecDig - 1];
            }
            catch
            {
                return "";
            }
        }

        private string ThreeDigit(int lIntValue)
        {
            try
            {
                if (lIntValue == 0) { return ""; }
                int lIntFirstDig, lIntSecDig;
                lIntFirstDig = lIntValue / 100;
                lIntSecDig = lIntValue % 100;
                return lIntFirstDig == 0 ? this.TwoDigit(lIntSecDig) : this.lStrOneDigArray[lIntFirstDig - 1] + " HUNDRED " + this.TwoDigit(lIntSecDig);
            }
            catch
            {
                return "";
            }
        }

        private string FourDigit(int lIntValue)
        {
            try
            {
                if (lIntValue == 0) { return ""; }
                int lIntFirstDig, lIntSecDig;
                lIntFirstDig = lIntValue / 1000;
                lIntSecDig = lIntValue % 1000;
                return lIntFirstDig == 0 ? this.ThreeDigit(lIntSecDig) : this.lStrOneDigArray[lIntFirstDig - 1] + " THOUSAND " + this.ThreeDigit(lIntSecDig);
            }
            catch
            {
                return "";
            }
        }

        private string FiveDigit(int lIntValue)
        {
            try
            {
                if (lIntValue == 0) { return ""; }
                int lIntFirstDig, lIntSecDig;
                lIntFirstDig = lIntValue / 1000;
                lIntSecDig = lIntValue % 1000;
                return lIntFirstDig == 0 ? this.ThreeDigit(lIntSecDig) : this.TwoDigit(lIntFirstDig) + " THOUSAND " + this.ThreeDigit(lIntSecDig);
            }
            catch
            {
                return "";
            }
        }
        private string SixDigit(int lIntValue)
        {
            try
            {
                if (lIntValue == 0) { return ""; }
                int lIntFirstDig, lIntSecDig;
                lIntFirstDig = lIntValue / 100000;
                lIntSecDig = lIntValue % 100000;
                return lIntFirstDig == 0 ? this.FiveDigit(lIntSecDig) : this.lStrOneDigArray[lIntFirstDig - 1] + " LAKH " + this.FiveDigit(lIntSecDig);
            }
            catch
            {
                return "";
            }
        }

        private string SevenDigit(int lIntValue)
        {
            try
            {
                if (lIntValue == 0) { return ""; }
                int lIntFirstDig, lIntSecDig;
                lIntFirstDig = lIntValue / 100000;
                lIntSecDig = lIntValue % 100000;
                return lIntFirstDig == 0 ? this.FiveDigit(lIntSecDig) : this.TwoDigit(lIntFirstDig) + " LAKH " + this.FiveDigit(lIntSecDig);
            }
            catch
            {
                return "";
            }
        }

        private string EightDigit(int lIntValue)
        {
            try
            {
                if (lIntValue == 0) { return ""; }
                int lIntFirstDig, lIntSecDig;
                lIntFirstDig = lIntValue / 10000000;
                lIntSecDig = lIntValue % 10000000;
                return lIntFirstDig == 0 ? this.SevenDigit(lIntSecDig) : this.lStrOneDigArray[lIntFirstDig - 1] + " CRORE " + this.SevenDigit(lIntSecDig);
            }
            catch
            {
                return "";
            }
        }

        private string NineDigit(int lIntValue)
        {
            try
            {
                if (lIntValue == 0) { return ""; }
                int lIntFirstDig, lIntSecDig;
                lIntFirstDig = lIntValue / 10000000;
                lIntSecDig = lIntValue % 10000000;
                return lIntFirstDig == 0 ? this.SevenDigit(lIntSecDig) : this.TwoDigit(lIntFirstDig) + " CRORE " + this.SevenDigit(lIntSecDig);
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// This Function Will Return String For the Given Positive Integer 
        /// Empty String is Returned For Long Integers  and Exceptions
        /// </summary>
        /// <param name="lIntValue">Integer Value to be Converted (Only interger Values are allowed)</param>
        /// <returns>Returns the corresponding words for the given value</returns>
        public string GetNumericWord(int lIntValue)
        {
            string rt = string.Empty;
            try
            {
                if (lIntValue == 0) rt = "ZERO";
                if (IsNumeric(lIntValue))
                {
                    int count, temp;
                    temp = lIntValue;
                    count = 0;
                    while (temp != 0)
                    {
                        temp = temp / 10;
                        count += 1;
                    }
                    if (count > 9) return "";
                    switch (count)
                    {
                        case 1: { return OneDigit(lIntValue); }
                        case 2: { return TwoDigit(lIntValue); }
                        case 3: { return ThreeDigit(lIntValue); }
                        case 4: { return FourDigit(lIntValue); }
                        case 5: { return FiveDigit(lIntValue); }
                        case 6: { return SixDigit(lIntValue); }
                        case 7: { return SevenDigit(lIntValue); }
                        case 8: { return EightDigit(lIntValue); }
                        case 9: { return NineDigit(lIntValue); }
                    }
                }
            }
            catch { }
            return rt;
        }
        /// <summary>
        /// This Function Will Return String For the Given string
        /// Empty String is Returned For Long Integers  and Exceptions
        /// </summary>
        /// <param name="number">String Value to be Converted (Only interger Values are allowed)</param>
        /// <returns>Returns the corresponding words for the given value</returns>
        public string GetNumericWord(string number)
        {
            return GetNumericWord(Int32.Parse(number));
        }
        private bool IsNumeric(object Expression)
        {
            // Variable to collect the Return value of the TryParse method.
            bool isNum;

            // Define variable to collect out parameter of the TryParse method. If the conversion fails, the out parameter is zero.
            double retNum;

            // The TryParse method converts a string in a specified style and culture-specific format to its double-precision floating point number equivalent.
            // The TryParse method does not generate an exception if the conversion fails. If the conversion passes, True is returned. If it does not, False is returned.
            isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }
    


	public WordConverter()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}
