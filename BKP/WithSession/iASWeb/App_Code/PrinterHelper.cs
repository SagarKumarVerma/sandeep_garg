using System;
using System.Drawing.Printing;
using System.Runtime.InteropServices;
using System.Diagnostics;


/// <summary>
/// Summary description for PrinterHelper
/// </summary>
public class PrinterHelper
{    
    static string prnData = string.Empty;
    //Add the printer connection for specified pName.
    [DllImport("winspool.drv")]  
    public static extern bool AddPrinterConnection(string pName); 
    //Set the added printer as default printer. 
    [DllImport("winspool.drv", CharSet = CharSet.Auto, SetLastError = true)]
    public static extern bool SetDefaultPrinter(string Name);

     public bool PrintFile(string fileName, string printerPath, int numCopies) 
     {
         
         ErrorClass ec = new ErrorClass();
         ec.TrapError("in PrintHelper Class", "PrintFile", "**** starts *** ");
      
        if (string.IsNullOrEmpty(fileName) || string.IsNullOrEmpty(printerPath))
         {             
             return false; 
         }
         ec.TrapError("in PrintHelper Class", "PrintFile", "Passed 1 ");
      
         Process objProcess = new Process();
         ec.TrapError("in PrintHelper Class", "PrintFile", "Passed 2 ");
         //try 
         //{             
             AddPrinterConnection(printerPath); 
             SetDefaultPrinter(printerPath);
             ec.TrapError("in PrintHelper Class", "PrintFile", "Passed 3 ");
             for (int intCount = 0; intCount < numCopies; intCount++) 
             {
                 ec.TrapError("in PrintHelper Class", "PrintFile", "in for Loop: " + intCount +1+3);
                 objProcess.StartInfo.FileName = fileName;    
                 objProcess.StartInfo.Verb = "print";       
                 objProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden; 
                 objProcess.StartInfo.UseShellExecute = true;
                 //return true;
                 ec.TrapError("in PrintHelper Class", "PrintFile", "Going to start:" + intCount + 1 + 3);
                 objProcess.Start();
                 objProcess.WaitForExit(10000);
                 ec.TrapError("in PrintHelper Class", "PrintFile", "Exit from Wait for Exit : " + intCount + 1 + 3);
                 objProcess.CloseMainWindow();

            }          
           
             ec.TrapError("in PrintHelper Class", "PrintFile", "End****" );
             return true;
         //}      
         //catch (Exception ex)          
         //{
         //    return false;         
         //}    
         //finally
         //{                     
             objProcess.Close();            
         //}        
       }

       public bool PrintFile1(string fileName, string printerPath)
       {
           // Check if the incomming strings are null or empty.
           if (string.IsNullOrEmpty(fileName) || string.IsNullOrEmpty(printerPath))
           {
               return false;
           }
           //Inst antiate the object of ProcessStartInfo.
           Process objProcess = new Process();

           try
           {
               //the command line to print PDF files without displaying the print dialog
               //AcroRd32.exe /t "C:\test.pdf" "\\servername\printername"            
               ProcessStartInfo starter = new ProcessStartInfo("AcroRd32.exe", "/t \"" + fileName + "\" \"" + printerPath + "\"");

               objProcess.StartInfo = starter;
               return true;
               objProcess.Start();

               //close Acrobat window
               objProcess.WaitForExit(10000);
               objProcess.CloseMainWindow();

               return true;
           }
           catch (Exception ex)
           {
               // Log the exception and return false as failure.
               return false;
           }
           finally
           {
               // Close the process.
               objProcess.Close();
           }
       }

       public bool SendStringToPrinter(string szString)
       {
           try
           {
               PrintDocument prnDocument;
               string printername;

               prnDocument = new PrintDocument();
               printername = Convert.ToString(prnDocument.PrinterSettings.PrinterName);
               if (string.IsNullOrEmpty(printername))
                   throw new Exception("No default printer is set.Printing failed!");
               prnData = szString;
               prnDocument.PrintPage += new PrintPageEventHandler(prnDoc_PrintPage);
               //
               return true;
               prnDocument.Print();
               return true;
           }
           catch (COMException comException)
           {
               return false;
           }
           catch (Exception sysException)
           {
               return false;
           }
       }

       static void prnDoc_PrintPage(object sender, PrintPageEventArgs e)
       {
           System.Drawing.Font fnt = new System.Drawing.Font(System.Drawing.FontFamily.GenericSerif, 10);
           e.Graphics.DrawString(prnData, fnt, System.Drawing.Brushes.Black, 0, 0);
       }
	public PrinterHelper()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}
