using System;
using System.Collections.Generic;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;

/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]

public class WebService : System.Web.Services.WebService
{
    Class_Connection con = new Class_Connection();
    public WebService()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    
    [WebMethod(EnableSession = true)]
    public string[] returnpaycode(string prefixText)
    {
        string Connection = ConfigurationManager.AppSettings["ConnectionString1"].ToString();
        int count = 10;

        SqlConnection con = new SqlConnection(Connection.ToString());
        //OleDbConnection con = new OleDbConnection(Connection.ToString());
        //string sql = "select * from tblemployee join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode Where tblemployee.Paycode like @prefixText and Departmentcode in (" + Context.Session["Auth_Dept"].ToString() + ") and companycode in (" + Context.Session["Auth_Comp"].ToString() + ")  ";
        string sql = "select * from tblemployee join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode Where tblemployee.Paycode like @prefixText and companycode in (" + Context.Session["Auth_Comp"].ToString() + ")  ";
        SqlDataAdapter da = new SqlDataAdapter(sql, con);
        //OleDbDataAdapter da = new OleDbDataAdapter(sql, con);
        da.SelectCommand.Parameters.Add("@prefixText", SqlDbType.VarChar, 50).Value = prefixText + "%";
        //da.SelectCommand.Parameters.Add("@prefixText", OleDbType.VarChar, 50).Value = prefixText + "%";
        DataTable dt = new DataTable();
        da.Fill(dt);
        string[] items = new string[dt.Rows.Count];
        int i = 0;
        foreach (DataRow dr in dt.Rows)
        {
            items.SetValue(dr["paycode"].ToString().Trim() + "-" + dr["EmpName"].ToString().Trim(), i);
            i++;
        }
        return items;
    }


    [WebMethod(EnableSession = true) ]
    public string[] returnEmpname(string prefixText,int count, string contextKey)
    {
        string Connection = ConfigurationManager.AppSettings["ConnectionString1"].ToString();
        count = 10;
        SqlConnection con = new SqlConnection(Connection.ToString());
        //OleDbConnection con = new OleDbConnection(Connection.ToString());
        //string sql = "select * from tblemployee join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode Where empname like @prefixText and Departmentcode in (" + Context.Session["Auth_Dept"].ToString() + ") and companycode in (" + Context.Session["Auth_Comp"].ToString() + ") ";
        string sql = "select * from tblemployee join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode Where empname like @prefixText  and companycode in (" + Context.Session["Auth_Comp"].ToString() + ") ";
        SqlDataAdapter da = new SqlDataAdapter(sql, con);
        //OleDbDataAdapter da = new OleDbDataAdapter(sql, con);
        da.SelectCommand.Parameters.Add("@prefixText", SqlDbType.VarChar, 50).Value = prefixText + "%";
       // da.SelectCommand.Parameters.Add("@prefixText", OleDbType.VarChar, 50).Value = prefixText + "%";
        DataTable dt = new DataTable();
        da.Fill(dt);
        string[] items = new string[dt.Rows.Count];
        int i = 0;
        foreach (DataRow dr in dt.Rows)
        {
            items.SetValue(dr["EmpName"].ToString().Trim() + "-" + dr["Paycode"].ToString().Trim(), i);
            i++;
        }
        return items;
    }


    [WebMethod(EnableSession = true)]
    public string[] returnController(string prefixText, int count, string contextKey)
    {
        string Connection = ConfigurationManager.AppSettings["ConnectionString1"].ToString();
        count = 10;
        SqlConnection con = new SqlConnection(Connection.ToString());
        //OleDbConnection con = new OleDbConnection(Connection.ToString());
        string sql = "select * from tblmachine Where ID_No like @prefixText";
        SqlDataAdapter da = new SqlDataAdapter(sql, con);
        //OleDbDataAdapter da = new OleDbDataAdapter(sql, con);
        da.SelectCommand.Parameters.Add("@prefixText", SqlDbType.VarChar, 50).Value = prefixText + "%";
        // da.SelectCommand.Parameters.Add("@prefixText", OleDbType.VarChar, 50).Value = prefixText + "%";
        DataTable dt = new DataTable();
        da.Fill(dt);
        string[] items = new string[dt.Rows.Count];
        int i = 0;
        foreach (DataRow dr in dt.Rows)
        {
            items.SetValue(dr["ID_No"].ToString().Trim() + "-" + dr["Location"].ToString().Trim(), i);
            i++;
        }
        return items;
    }

    [WebMethod(EnableSession = true)]
    public string[] returnCompany(string prefixText, int count, string contextKey)
    {
        string Connection = ConfigurationManager.AppSettings["ConnectionString1"].ToString();
        count = 10;
        SqlConnection con = new SqlConnection(Connection.ToString());
        //OleDbConnection con = new OleDbConnection(Connection.ToString());
        string sql = "select * from tblCompany Where Companycode like @prefixText";
        SqlDataAdapter da = new SqlDataAdapter(sql, con);
        //OleDbDataAdapter da = new OleDbDataAdapter(sql, con);
        da.SelectCommand.Parameters.Add("@prefixText", SqlDbType.VarChar, 50).Value = prefixText + "%";
        // da.SelectCommand.Parameters.Add("@prefixText", OleDbType.VarChar, 50).Value = prefixText + "%";
        DataTable dt = new DataTable();
        da.Fill(dt);
        string[] items = new string[dt.Rows.Count];
        int i = 0;
        foreach (DataRow dr in dt.Rows)
        {
            items.SetValue(dr["Companycode"].ToString().Trim() + "-" + dr["CompanyName"].ToString().Trim(), i);
            i++;
        }
        return items;
    }

    [WebMethod(EnableSession = true)]
    public string[] returnDepartment(string prefixText, int count, string contextKey)
    {
        string Connection = ConfigurationManager.AppSettings["ConnectionString1"].ToString();
        count = 10;
        SqlConnection con = new SqlConnection(Connection.ToString());
        //OleDbConnection con = new OleDbConnection(Connection.ToString());
        string sql = "select * from tblDepartment Where departmentcode like @prefixText";
        SqlDataAdapter da = new SqlDataAdapter(sql, con);
        //OleDbDataAdapter da = new OleDbDataAdapter(sql, con);
        da.SelectCommand.Parameters.Add("@prefixText", SqlDbType.VarChar, 50).Value = prefixText + "%";
        // da.SelectCommand.Parameters.Add("@prefixText", OleDbType.VarChar, 50).Value = prefixText + "%";
        DataTable dt = new DataTable();
        da.Fill(dt);
        string[] items = new string[dt.Rows.Count];
        int i = 0;
        foreach (DataRow dr in dt.Rows)
        {
            items.SetValue(dr["Departmentcode"].ToString().Trim() + "-" + dr["DepartmentName"].ToString().Trim(), i);
            i++;
        }
        return items;
    }

    [WebMethod(EnableSession = true)]
    public string[] returnDivision(string prefixText, int count, string contextKey)
    {
        string Connection = ConfigurationManager.AppSettings["ConnectionString1"].ToString();
        count = 10;
        SqlConnection con = new SqlConnection(Connection.ToString());
        //OleDbConnection con = new OleDbConnection(Connection.ToString());
        string sql = "select * from tblDivision Where divisioncode like @prefixText";
        SqlDataAdapter da = new SqlDataAdapter(sql, con);
        //OleDbDataAdapter da = new OleDbDataAdapter(sql, con);
        da.SelectCommand.Parameters.Add("@prefixText", SqlDbType.VarChar, 50).Value = prefixText + "%";
        // da.SelectCommand.Parameters.Add("@prefixText", OleDbType.VarChar, 50).Value = prefixText + "%";
        DataTable dt = new DataTable();
        da.Fill(dt);
        string[] items = new string[dt.Rows.Count];
        int i = 0;
        foreach (DataRow dr in dt.Rows)
        {
            items.SetValue(dr["Divisioncode"].ToString().Trim() + "-" + dr["DivisionName"].ToString().Trim(), i);
            i++;
        }
        return items;
    }

    [WebMethod]
    public string[] returnGrade(string prefixText, int count, string contextKey)
    {
        string Connection = ConfigurationManager.AppSettings["ConnectionString1"].ToString();
        count = 10;
        SqlConnection con = new SqlConnection(Connection.ToString());
        //OleDbConnection con = new OleDbConnection(Connection.ToString());
        string sql = "select * from tblGrade Where gradecode like @prefixText";
        SqlDataAdapter da = new SqlDataAdapter(sql, con);
        //OleDbDataAdapter da = new OleDbDataAdapter(sql, con);
        da.SelectCommand.Parameters.Add("@prefixText", SqlDbType.VarChar, 50).Value = prefixText + "%";
        // da.SelectCommand.Parameters.Add("@prefixText", OleDbType.VarChar, 50).Value = prefixText + "%";
        DataTable dt = new DataTable();
        da.Fill(dt);
        string[] items = new string[dt.Rows.Count];
        int i = 0;
        foreach (DataRow dr in dt.Rows)
        {
            items.SetValue(dr["Gradecode"].ToString().Trim() + "-" + dr["GradeName"].ToString().Trim(), i);
            i++;
        }
        return items;
    }
    [WebMethod]
    public string[] returnCat(string prefixText, int count, string contextKey)
    {
        string Connection = ConfigurationManager.AppSettings["ConnectionString1"].ToString();
        count = 10;
        SqlConnection con = new SqlConnection(Connection.ToString());
        //OleDbConnection con = new OleDbConnection(Connection.ToString());
        string sql = "select * from tblCatagory Where cat like @prefixText";
        //or catagoryname like @prefixText";
        SqlDataAdapter da = new SqlDataAdapter(sql, con);
        //OleDbDataAdapter da = new OleDbDataAdapter(sql, con);
        da.SelectCommand.Parameters.Add("@prefixText", SqlDbType.VarChar, 50).Value = prefixText + "%";
        // da.SelectCommand.Parameters.Add("@prefixText", OleDbType.VarChar, 50).Value = prefixText + "%";
        DataTable dt = new DataTable();
        da.Fill(dt);
        string[] items = new string[dt.Rows.Count];
        int i = 0;
        foreach (DataRow dr in dt.Rows)
        {
            items.SetValue(dr["cat"].ToString().Trim() + "-" + dr["catagoryName"].ToString().Trim(), i);
            i++;
        }
        return items;
    }

    [WebMethod]
    public string[] returnShift(string prefixText, int count, string contextKey)
    {
        string Connection = ConfigurationManager.AppSettings["ConnectionString1"].ToString();
        count = 10;
        SqlConnection con = new SqlConnection(Connection.ToString());
        //OleDbConnection con = new OleDbConnection(Connection.ToString());
        //string sql = "select * from tblShiftMaster Where shift like @prefixText";
        string sql = "select shift,convert(varchar(5),starttime,108)+'-'+ convert(varchar(5),endtime,108) 'ShiftTime' from tblshiftmaster Where shift like @prefixText";
        //or catagoryname like @prefixText";
        SqlDataAdapter da = new SqlDataAdapter(sql, con);
        //OleDbDataAdapter da = new OleDbDataAdapter(sql, con);
        da.SelectCommand.Parameters.Add("@prefixText", SqlDbType.VarChar, 50).Value = prefixText + "%";
        // da.SelectCommand.Parameters.Add("@prefixText", OleDbType.VarChar, 50).Value = prefixText + "%";
        DataTable dt = new DataTable(); 
        da.Fill(dt);
        string[] items = new string[dt.Rows.Count];
        int i = 0;
        foreach (DataRow dr in dt.Rows)
        {
            items.SetValue(dr["shift"].ToString().Trim() + "--" + dr["ShiftTime"].ToString().Trim(), i);
            i++;
        }
        return items;
    }


    [WebMethod]
    public string[] returnSetup(string prefixText, int count, string contextKey)
    {
        string Connection = ConfigurationManager.AppSettings["ConnectionString1"].ToString();
        count = 10;
        SqlConnection con = new SqlConnection(Connection.ToString());
        //OleDbConnection con = new OleDbConnection(Connection.ToString());
        //string sql = "select * from tblShiftMaster Where shift like @prefixText";
        string sql = "select * from tblsetup Where setupid like @prefixText";
        //or catagoryname like @prefixText";
        SqlDataAdapter da = new SqlDataAdapter(sql, con);
        //OleDbDataAdapter da = new OleDbDataAdapter(sql, con);
        da.SelectCommand.Parameters.Add("@prefixText", SqlDbType.VarChar, 50).Value = prefixText + "%";
        // da.SelectCommand.Parameters.Add("@prefixText", OleDbType.VarChar, 50).Value = prefixText + "%";
        DataTable dt = new DataTable();
        da.Fill(dt);
        string[] items = new string[dt.Rows.Count];
        int i = 0;
        foreach (DataRow dr in dt.Rows)
        {
            items.SetValue(dr["setupid"].ToString().Trim(), i);
            i++;
        }
        return items;
    }

    [WebMethod]
    public string[] returnuser(string prefixText, int count, string contextKey)
    {
        string Connection = ConfigurationManager.AppSettings["ConnectionString1"].ToString();
        count = 10;
        SqlConnection con = new SqlConnection(Connection.ToString());
        string sql = "select * from tbluser Where USERDESCRIPRION like @prefixText";        
        SqlDataAdapter da = new SqlDataAdapter(sql, con);        
        da.SelectCommand.Parameters.Add("@prefixText", SqlDbType.VarChar, 100).Value = prefixText + "%";
        DataTable dt = new DataTable();
        da.Fill(dt);
        string[] items = new string[dt.Rows.Count];
        int i = 0;
        foreach (DataRow dr in dt.Rows)
        {
            items.SetValue(dr["USERDESCRIPRION"].ToString().Trim(), i);
            i++;
        }
        return items;
    }   

}

