﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="FrmLeaveSummary.aspx.cs" Inherits="FrmLeaveSummary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <div align="left">
        <table align="center" style="width:100%" cellpadding="0" cellspacing="0" >
            <tr>
                <td style="height:25px"></td>
            </tr>
            <tr>
                <td align="center">
                    <table style="width:100%"cellpadding="0" cellspacing="0" class="tableCss">
                        <tr>
                            <td colspan="2" style="height:25px;width:100%" align="center" class="tableHeaderCss">
                                <asp:Label ID="LblFrmName" runat="server" Text="Leave Summary" CssClass="lblCss" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width: 100%; height: 15px; text-align: center"></td>
                        </tr>
                        <tr>
                            <td style="width: 45%" class="text_new" >Report Criteria
                           
                                <dx:ASPxRadioButton ID="RadAll" runat="server" Checked="True" Font-Bold="True" GroupName="G1"
                                Text="All Leaves" AutoPostBack="True" OnCheckedChanged="RadAll_CheckedChanged">
                                </dx:ASPxRadioButton>
                                <dx:ASPxRadioButton ID="RadDateWise" runat="server" Checked="True" Font-Bold="True" GroupName="G1"
                                Text="Leaves between" AutoPostBack="True" OnCheckedChanged="RadDateWise_CheckedChanged">
                                </dx:ASPxRadioButton>
                            </td>
                            <td style="width: 55%"  class="text_new">
                                <asp:Label ID="LblFromDate" runat="server" Text="From" Visible="False"></asp:Label>
                                <dx:ASPxDateEdit ID="txtfromdate" runat="server"  Width="71px" Visible="False" CssClass="TextBoxCss">
                                </dx:ASPxDateEdit>
                                    &nbsp;
                            
                                    <dx:ASPxLabel ID="LblToDate" runat="server" Text="Upto" Visible="False">
                                </dx:ASPxLabel>
                                <dx:ASPxDateEdit ID="txttodate" runat="server"  Width="71px" Visible="False" CssClass="TextBoxCss">
                                </dx:ASPxDateEdit>
                         
                                    &nbsp; &nbsp; &nbsp;
                                    <dx:ASPxButton ID="BtnShow" runat="server"  Text="Show" CssClass="buttoncss" OnClick="BtnShow_Click">
                                </dx:ASPxButton>
                                    &nbsp;
                                     <dx:ASPxButton ID="cmdBack" runat="server"  Text="Leave Approval" CssClass="buttoncss" OnClick="cmdBack_Click">
                                </dx:ASPxButton>
                                <dx:ASPxButton ID="Button3" runat="server"  Text="Cancel" CssClass="buttoncss" OnClick="Button3_Click">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                        <tr>
                            <td class="text_new" style="height: 15px" valign="top"></td>
                            <td class="text_new" style="height: 15px" valign="top"></td>
                        </tr>
                        <tr>
                            <td class="text_new" style="width: 364px" valign="top">
                                <dx:ASPxLabel ID="LblAll" runat="server" Text="Leave Report for Paycode(s)" Visible="False">
                                </dx:ASPxLabel>
              &nbsp;
                                <dx:ASPxRadioButton ID="RadShowAll" runat="server" Checked="True" Font-Bold="True" GroupName="G2"
                                Text="All" AutoPostBack="True" OnCheckedChanged="RadShowAll_CheckedChanged">
                                </dx:ASPxRadioButton>
              
              &nbsp;
                                <dx:ASPxRadioButton ID="RadShowParticular" runat="server" Checked="True" Font-Bold="True" GroupName="G2"
                                Text="Particular" AutoPostBack="True" OnCheckedChanged="RadShowParticular_CheckedChanged">
                                </dx:ASPxRadioButton>
                            </td>
                            <td  style="width: 550px" valign="top">
                                <table id="tdup" runat="server" cellpadding="0" cellspacing="0" style="width:458px" >
                                    <tr>
                                        <td class="text_new" style="width: 100px">Select By</td>
                                        <td style="width: 250px" valign="top" align="left">
                                            <dx:ASPxRadioButtonList ID="radioday" runat="server" EnableTheming="True" Height="19px" RepeatDirection="Horizontal" Style="top: 0px" Theme="SoftOrange">
                                                <Items>
                                                    <dx:ListEditItem Text="Name" Selected="true" Value="N" />
                                                    <dx:ListEditItem Text="PayCode" Value="P" />
                                                </Items>
                                            </dx:ASPxRadioButtonList>
                                        </td>
                                        <td style="width: 60px" valign="top"></td>
                                    </tr>
                                    <tr>
                                        <td style="width:100px;" class="text_new" >Select Paycode
          </td>
                                        <td style="width:250px" valign="top">
                                            <asp:DropDownList ID="ddlPaycode" runat="server" CssClass="DropDownCss" Width="258px" >
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 60px" valign="top">
                                            <asp:Button ID="cmdSearch" runat="server" CssClass="buttoncss" 
                  Text="Show" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="text_new" style="height: 10px" valign="top"></td>
                            <td style="height: 10px" valign="top"></td>
                        </tr>
                        <tr>
                            <td align="center" class="text_new" colspan="2" style="text-align: center"
              valign="top"></td>
                        </tr>
                        <tr>
                            <td  colspan="2" style="width: 746px;" align="right" class="text_new"></td>
                        </tr>
                        <tr>
                            <td align="right" class=" " colspan="2" style="width: 850px; height: 10px"></td>
                        </tr>
                        <tr>
                            <td  colspan="2" style="width: 100%;padding-left:5px">
                                <div style="overflow:auto; width:100%; height:350px" >
                                    <asp:GridView ID="GridView1" runat="server" Width="100%" CssClass="mGrid" >
                                        <EmptyDataTemplate>
                        No record Found                     
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

