﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using GlobalSettings;
using System.Collections.Generic;
using DevExpress.Web;
using System.Text;
public partial class frmEmployee : System.Web.UI.Page
{
    Class_Connection Con = new Class_Connection();
    string Strsql = null;
    DataSet ds = null;
    int result = 0;
    ErrorClass ec = new ErrorClass();
    bool isNewRecord = false;
    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    string Cat, CatName, IsLate, EveryInterval, Deductfrom, Leave1, Leave2;
    double LayDays = 0;
    double MaxLateDur = 0;
    double DeductDays = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            //if (Session["ComVisible"] == null)
            //{
            //    Response.Redirect("PageNotFound.aspx");
            //}
            BindData();
        }
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }

    protected void BindData()
    {

        try
        {
            if (Session["LoginUserName"].ToString().Trim().ToUpper() == "ADMIN")
            {
                Strsql = " SELECT TBLEMPLOYEE.ACTIVE, PAYCODE, EMPNAME, GUARDIANNAME, CONVERT(VARCHAR(12),DateOFBIRTH,106) 'DateOFBIRTH', CONVERT(VARCHAR(12),DATEOFJOIN,106) 'DateOFJOIN', PRESENTCARDNO, tblcompany.COMPANYNAME, tblDivision.DivisionName, " +
                  " tblCatagory.CATAGORYNAME,tblDepartment.DEPARTMENTNAME,tblGrade.GradeName,SEX, ISMARRIED,  DESIGNATION FROM TBLEMPLOYEE inner join TBLCOMPANY on " +
                  " TBLEMPLOYEE.COMPANYCODE=tblCompany.COMPANYCODE inner join tblDepartment on tblDepartment.DEPARTMENTCODE= TBLEMPLOYEE.DepartmentCode inner join tblDivision on " +
                  " tblDivision.DivisionCode=TBLEMPLOYEE.DivisionCode inner join tblGrade on  tblGrade.GradeCode=TBLEMPLOYEE.GradeCode inner join tblCatagory on tblCatagory.CAT=TBLEMPLOYEE.CAT order by PAYCODE ";
            }
            else if (Session["LoginUserName"].ToString().Trim().ToUpper().ToUpper() == "H")
            {

                Strsql = " SELECT TBLEMPLOYEE.ACTIVE, PAYCODE, EMPNAME, GUARDIANNAME, CONVERT(VARCHAR(12),DateOFBIRTH,106) 'DateOFBIRTH', CONVERT(VARCHAR(12),DATEOFJOIN,106) 'DateOFJOIN', PRESENTCARDNO, tblcompany.COMPANYNAME, tblDivision.DivisionName, " +
                  " tblCatagory.CATAGORYNAME,tblDepartment.DEPARTMENTNAME,tblGrade.GradeName,SEX, ISMARRIED,  DESIGNATION FROM TBLEMPLOYEE inner join TBLCOMPANY on " +
                  " TBLEMPLOYEE.COMPANYCODE=tblCompany.COMPANYCODE inner join tblDepartment on tblDepartment.DEPARTMENTCODE= TBLEMPLOYEE.DepartmentCode inner join tblDivision on " +
                  " tblDivision.DivisionCode=TBLEMPLOYEE.DivisionCode inner join tblGrade on  tblGrade.GradeCode=TBLEMPLOYEE.GradeCode inner join tblCatagory on tblCatagory.CAT=TBLEMPLOYEE.CAT " +
                  " and tblemployee.DEPARTMENTCODE in(" + Session["Auth_Dept"].ToString().Trim() + ") and tblemployee.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "' AND   tblCompany.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "'	AND tblDepartment.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "' " +
                  " AND tblDivision.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "'	AND tblGrade.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "'	AND tblCatagory.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "' order by PAYCODE ";
            }
            else
            {
                Strsql = " SELECT TBLEMPLOYEE.ACTIVE, PAYCODE, EMPNAME, GUARDIANNAME, CONVERT(VARCHAR(12),DateOFBIRTH,106) 'DateOFBIRTH', CONVERT(VARCHAR(12),DATEOFJOIN,106) 'DateOFJOIN', PRESENTCARDNO, tblcompany.COMPANYNAME, tblDivision.DivisionName, " +
                  " tblCatagory.CATAGORYNAME,tblDepartment.DEPARTMENTNAME,tblGrade.GradeName,SEX, ISMARRIED,  DESIGNATION FROM TBLEMPLOYEE inner join TBLCOMPANY on " +
                  " TBLEMPLOYEE.COMPANYCODE=tblCompany.COMPANYCODE inner join tblDepartment on tblDepartment.DEPARTMENTCODE= TBLEMPLOYEE.DepartmentCode inner join tblDivision on " +
                  " tblDivision.DivisionCode=TBLEMPLOYEE.DivisionCode inner join tblGrade on  tblGrade.GradeCode=TBLEMPLOYEE.GradeCode inner join tblCatagory on tblCatagory.CAT=TBLEMPLOYEE.CAT "+
                  " and tblemployee.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "' AND   tblCompany.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "'	AND tblDepartment.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "' " +
                  " AND tblDivision.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "'	AND tblGrade.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "'	AND tblCatagory.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "' order by PAYCODE ";
            }

            ds = new DataSet();
            ds = Con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                GrdEmp.DataSource = ds.Tables[0];
                GrdEmp.DataBind();
            }
        }
        catch (Exception Ex)
        {
            
            Error_Occured("BindData",Ex.Message);
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        BindData();
        GrdEmp.DataBind();
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        Session["IsNewRec"] = "YES";
        Response.Redirect("EmployeeEdit.aspx");
    }
    protected void GrdEmp_PageIndexChanged(object sender, EventArgs e)
    {
        int pageIndex = (sender as ASPxGridView).PageIndex;
        GrdEmp.PageIndex = pageIndex;
        this. BindData();
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            string strsql = string.Empty;
            if (Session["LoginUserName"].ToString().Trim().ToUpper() == "ADMIN")
            {
                Strsql = " SELECT TBLEMPLOYEE.ACTIVE, PAYCODE, EMPNAME,E_Mail1, GUARDIANNAME, CONVERT(VARCHAR(12),DateOFBIRTH,106) 'DateOFBIRTH', CONVERT(VARCHAR(12),DATEOFJOIN,106) 'DateOFJOIN', PRESENTCARDNO, tblcompany.COMPANYNAME, tblDivision.DivisionName, " +
                  " tblCatagory.CATAGORYNAME,tblDepartment.DEPARTMENTNAME,tblGrade.GradeName,SEX, ISMARRIED,  DESIGNATION FROM TBLEMPLOYEE inner join TBLCOMPANY on " +
                  " TBLEMPLOYEE.COMPANYCODE=tblCompany.COMPANYCODE inner join tblDepartment on tblDepartment.DEPARTMENTCODE= TBLEMPLOYEE.DepartmentCode inner join tblDivision on " +
                  " tblDivision.DivisionCode=TBLEMPLOYEE.DivisionCode inner join tblGrade on  tblGrade.GradeCode=TBLEMPLOYEE.GradeCode inner join tblCatagory on tblCatagory.CAT=TBLEMPLOYEE.CAT order by PAYCODE ";
            }
            else if (Session["LoginUserName"].ToString().Trim().ToUpper().ToUpper() == "H")
            {

                Strsql = " SELECT TBLEMPLOYEE.ACTIVE, PAYCODE,PRESENTCARDNO,E_Mail1, EMPNAME, GUARDIANNAME, CONVERT(VARCHAR(12),DateOFBIRTH,106) 'DateOFBIRTH', CONVERT(VARCHAR(12),DATEOFJOIN,106) 'DateOFJOIN',  tblcompany.COMPANYNAME, tblDivision.DivisionName, " +
                  " tblCatagory.CATAGORYNAME,tblDepartment.DEPARTMENTNAME,tblGrade.GradeName,SEX, ISMARRIED,  DESIGNATION FROM TBLEMPLOYEE inner join TBLCOMPANY on " +
                  " TBLEMPLOYEE.COMPANYCODE=tblCompany.COMPANYCODE inner join tblDepartment on tblDepartment.DEPARTMENTCODE= TBLEMPLOYEE.DepartmentCode inner join tblDivision on " +
                  " tblDivision.DivisionCode=TBLEMPLOYEE.DivisionCode inner join tblGrade on  tblGrade.GradeCode=TBLEMPLOYEE.GradeCode inner join tblCatagory on tblCatagory.CAT=TBLEMPLOYEE.CAT " +
                  " and tblemployee.DEPARTMENTCODE in(" + Session["Auth_Dept"].ToString().Trim() + ") and tblemployee.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "' AND   tblCompany.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "'	AND tblDepartment.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "' " +
                  " AND tblDivision.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "'	AND tblGrade.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "'	AND tblCatagory.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "' order by PAYCODE ";
            }
            else
            {
                Strsql = " SELECT TBLEMPLOYEE.ACTIVE, PAYCODE, EMPNAME,E_Mail1, GUARDIANNAME, CONVERT(VARCHAR(12),DateOFBIRTH,106) 'DateOFBIRTH', CONVERT(VARCHAR(12),DATEOFJOIN,106) 'DateOFJOIN', PRESENTCARDNO, tblcompany.COMPANYNAME, tblDivision.DivisionName, " +
                  " tblCatagory.CATAGORYNAME,tblDepartment.DEPARTMENTNAME,tblGrade.GradeName,SEX, ISMARRIED,  DESIGNATION FROM TBLEMPLOYEE inner join TBLCOMPANY on " +
                  " TBLEMPLOYEE.COMPANYCODE=tblCompany.COMPANYCODE inner join tblDepartment on tblDepartment.DEPARTMENTCODE= TBLEMPLOYEE.DepartmentCode inner join tblDivision on " +
                  " tblDivision.DivisionCode=TBLEMPLOYEE.DivisionCode inner join tblGrade on  tblGrade.GradeCode=TBLEMPLOYEE.GradeCode inner join tblCatagory on tblCatagory.CAT=TBLEMPLOYEE.CAT " +
                  " and tblemployee.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "' AND   tblCompany.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "'	AND tblDepartment.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "' " +
                  " AND tblDivision.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "'	AND tblGrade.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "'	AND tblCatagory.COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "' order by PAYCODE ";
            }
            ds = new DataSet();
            ds = Con.FillDataSet(Strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    try
                    {
                        string Tmp = "";
                        int x, i;
                        System.Data.DataTable dTbl = new System.Data.DataTable();
                        DataRow dRow;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);

                        dTbl.Columns.Add("Sr.No");
                        dTbl.Columns.Add("Active");
                        dTbl.Columns.Add("Employee Code");
                        dTbl.Columns.Add("Employee Name");
                        dTbl.Columns.Add("Guardian Name");
                        dTbl.Columns.Add("Date Of Birth");
                        dTbl.Columns.Add("Date Of Join");
                        dTbl.Columns.Add("Biometric ID");
                        dTbl.Columns.Add("Company");
                        dTbl.Columns.Add("Department");
                        dTbl.Columns.Add("Section");
                        dTbl.Columns.Add("Grade");
                        dTbl.Columns.Add("Category");
                        dTbl.Columns.Add("Gender");
                        dTbl.Columns.Add("Married");
                        dTbl.Columns.Add("Designation");
                        dTbl.Columns.Add("Email Id");

                        dTbl.Rows[0][0] = "Sr.No";
                        dTbl.Rows[0][1] = "Active";
                        dTbl.Rows[0][2] = "Employee Code";
                        dTbl.Rows[0][3] = "Employee Name";
                        dTbl.Rows[0][4] = "Guardian Name";
                        dTbl.Rows[0][5] = "Date Of Birth";
                        dTbl.Rows[0][6] = "Date Of Join";
                        dTbl.Rows[0][7] = "Biometric ID";
                        dTbl.Rows[0][8] = "Company";
                        dTbl.Rows[0][9] = "Department";
                        dTbl.Rows[0][10] = "Section";
                        dTbl.Rows[0][11] = "Grade";
                        dTbl.Rows[0][12] = "Category";
                        dTbl.Rows[0][13] = "Gender";
                        dTbl.Rows[0][14] = "Married";
                        dTbl.Rows[0][15] = "Designation";
                    dTbl.Rows[0][16] = "Email Id";

                    int ct = 1;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                            {

                                dTbl.Rows[ct][0] = ct.ToString();
                                dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1]["ACTIVE"].ToString().Trim();
                                dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1]["PAYCODE"].ToString().Trim();
                                dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1]["EMPNAME"].ToString().Trim();
                                dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1]["GUARDIANNAME"].ToString().Trim();
                                dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1]["DateOFBIRTH"].ToString().Trim();
                                dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1]["DateOFJOIN"].ToString().Trim();
                                dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1]["PRESENTCARDNO"].ToString().Trim();
                                dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1]["COMPANYNAME"].ToString().Trim();
                                dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1]["DEPARTMENTNAME"].ToString().Trim();
                                dTbl.Rows[ct][10] = ds.Tables[0].Rows[i1]["DivisionName"].ToString().Trim();
                                dTbl.Rows[ct][11] = ds.Tables[0].Rows[i1]["GradeName"].ToString().Trim();
                                dTbl.Rows[ct][12] = ds.Tables[0].Rows[i1]["CATAGORYNAME"].ToString().Trim();
                                dTbl.Rows[ct][13] = ds.Tables[0].Rows[i1]["SEX"].ToString().Trim();
                                dTbl.Rows[ct][14] = ds.Tables[0].Rows[i1]["ISMARRIED"].ToString().Trim();
                                dTbl.Rows[ct][15] = ds.Tables[0].Rows[i1]["DESIGNATION"].ToString().Trim();
                            dTbl.Rows[ct][16] = ds.Tables[0].Rows[i1]["E_Mail1"].ToString().Trim();

                            dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                ct = ct + 1;
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        bool isEmpty;
                        for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                        {

                            isEmpty = true;
                            for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                            {
                                if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                {
                                    isEmpty = false;
                                    break;
                                }
                            }

                            if (isEmpty == true)
                            {
                                dTbl.Rows.RemoveAt(i2);
                                i2--;
                            }
                        }
                        string companyname = "";
                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = Con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }

                        int colCount = Convert.ToInt32(dTbl.Columns.Count);

                        string msg = "Employee Master Data";
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                        x = colCount;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                    if (Tmp.Substring(0, 1) == "0")
                                    {
                                        if (Tmp.Contains(":"))
                                        {
                                            Tmp = Tmp;
                                        }
                                        else
                                        {
                                            Tmp = Tmp;
                                        }
                                    }
                                }
                                if ((i == 1) || (i == 2))
                                {
                                    if (Tmp.ToString().Trim() != "")
                                    {
                                      sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        
                                    }
                                }
                                else if (((i == 3) || (i == 4)) && (Tmp.ToString().Trim() != ""))
                                {
                                    sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                else
                                    sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=EmployeeData.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/EmployeeData.xls";
                        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                        Response.Write(sb.ToString());
                        Response.End();
                    }
                    catch (Exception ex)
                    {
                        //Response.Write(ex.Message.ToString());
                    }
                }


        }
        catch (Exception Ex)
        {

            Error_Occured("Export", Ex.Message);
        }
    }
    protected void btlDel_Click(object sender, EventArgs e)
    {
        try
        {

            List<object> keys = GrdEmp.GetSelectedFieldValues(new string[] { GrdEmp.KeyFieldName });
            if (keys.Count == 0)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Please Select Atleast One Employee')</script>");
                return;
            }

            for (int i = 0; i < GrdEmp.VisibleRowCount; i++)
            {
                if (GrdEmp.Selection.IsRowSelected(i) == true)
                {
                    string PayCode = GrdEmp.GetRowValues(i, "PAYCODE").ToString().Trim();

                    try
                    {

                        Strsql = "Delete from tblemployee where Paycode='" + PayCode + "' ";
                        Con.execute_NonQuery(Strsql);
                        Strsql = "Delete from tbluser where user_r='" + PayCode + "' ";
                        Con.execute_NonQuery(Strsql);
                        Strsql = "Delete from tblemployeeShiftMaster where Paycode='" + PayCode + "' ";
                        Con.execute_NonQuery(Strsql);
                        Strsql = "Delete from tblTimeRegister where Paycode='" + PayCode + "' ";
                        Con.execute_NonQuery(Strsql);
                        Strsql = "Delete from tblLeaveLedger where Paycode='" + PayCode + "' ";
                        Con.execute_NonQuery(Strsql);
                        Strsql = "Delete from MachineRawPunch where PAYCODE ='" + PayCode + "' ";
                        Con.execute_NonQuery(Strsql);
                       


                    }
                    catch
                    {
                        continue;
                    }



                }
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Employee Deleted Successfully');document.location='frmEmployee.aspx'", true);
        }
        catch (Exception Ex)
        {

            Error_Occured("Delete", Ex.Message);
        }
    }
}