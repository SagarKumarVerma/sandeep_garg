﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using FKWeb;
using Newtonsoft.Json.Linq;
using DevExpress.Web;
using System.Collections;
using System.Net;
using Newtonsoft.Json;
public partial class DeviceCommands : System.Web.UI.Page
{
    OleDbConnection MyConn = new OleDbConnection();
    OleDbCommand MyCmd = new OleDbCommand();
    Class_Connection cn = new Class_Connection();
    public string KeyValue = "";
    public string TableMain = null;
    public string TableName = null;
    public string ColumnName = null;
    const int GET_DEVICE_STATUS = 1;
    string sSql = string.Empty;
    DataSet ds = null;
    SqlConnection msqlConn;
    ErrorClass ec = new ErrorClass();
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["TimeWatchConnectionString"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetCommands();
        }
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        //Call The function to write the error log file 

        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        GetCommands();
        ASPxGridView1.DataBind();
    }
    protected void GetCommands()
    {
        try
        {
            DataTable DT = new DataTable();
            DT.Columns.Add("TransactionID");
            DT.Columns.Add("DeviceID");
            DT.Columns.Add("DeviceName");
            DT.Columns.Add("CommandCode");
            DT.Columns.Add("Result");
            DT.Columns.Add("Status");
            DT.Columns.Add("UpdateTime");

            sSql = "Select tbl_fkcmd_trans.trans_id,tbl_fkcmd_trans.device_id 'device_id',tbl_fkcmd_trans.cmd_code,tbl_fkcmd_trans.return_code,tbl_fkcmd_trans.status,tbl_fkcmd_trans.update_time, " +
                "tbl_fkdevice_status.device_name from tbl_fkcmd_trans inner join tbl_fkdevice_status on tbl_fkcmd_trans.device_id=tbl_fkdevice_status.device_id";
            DataSet DsCmd = new DataSet();
            DsCmd = cn.FillDataSet(sSql);
            if (DsCmd.Tables[0].Rows.Count > 0)
            {
                for (int B = 0; B < DsCmd.Tables[0].Rows.Count; B++)
                {
                    DT.Rows.Add(DsCmd.Tables[0].Rows[B]["trans_id"].ToString().Trim(), DsCmd.Tables[0].Rows[B]["device_id"].ToString().Trim(), DsCmd.Tables[0].Rows[B]["device_name"].ToString().Trim(),
                        DsCmd.Tables[0].Rows[B]["cmd_code"].ToString().Trim(), DsCmd.Tables[0].Rows[B]["return_code"].ToString().Trim(), DsCmd.Tables[0].Rows[B]["status"].ToString().Trim(),
                        DsCmd.Tables[0].Rows[B]["update_time"].ToString().Trim());
                }
            }
            string sSqlZK = "select DeviceCommands.Dev_Cmd_ID,DeviceCommands.TransferToDevice,DeviceCommands.CommandContent,DeviceCommands.Executed," +
                "DeviceCommands.CreatedOn,tblMachine.DeviceName from DeviceCommands inner join tblMachine on tblMachine.SerialNumber=DeviceCommands.TransferToDevice";

            DataSet DsCmdZ = new DataSet();
            DsCmdZ = cn.FillDataSet(sSqlZK);
            if (DsCmdZ.Tables[0].Rows.Count > 0)
            {
                for (int Z = 0; Z < DsCmdZ.Tables[0].Rows.Count; Z++)
                {
                    DT.Rows.Add(DsCmdZ.Tables[0].Rows[Z]["Dev_Cmd_ID"].ToString().Trim(), DsCmdZ.Tables[0].Rows[Z]["TransferToDevice"].ToString().Trim(), DsCmdZ.Tables[0].Rows[Z]["DeviceName"].ToString().Trim(),
                        DsCmdZ.Tables[0].Rows[Z]["CommandContent"].ToString().Trim(), "", DsCmdZ.Tables[0].Rows[Z]["Executed"].ToString().Trim(),
                        DsCmdZ.Tables[0].Rows[Z]["CreatedOn"].ToString().Trim());
                }
            }

            if (DT.Rows.Count > 0)
            {
                ASPxGridView1.DataSource = DT;
                ASPxGridView1.KeyFieldName = "DeviceID";
                ASPxGridView1.DataBind();
            }
        }
        catch (Exception Exc)
        {
            Error_Occured("GetCommands", Exc.Message);

        }


    }

    protected void ASPxGridView1_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        if (e.DataColumn.Caption == "Discription")
        {
            string statementType = "switch";
            statementType = Convert.ToString(e.GetValue("CommandCode"));
            switch (statementType.Trim())
            {
                case "SET_TIME":
                    e.Cell.Text = "Sync Time ";
                    break;
                case "GET_LOG_DATA":
                    e.Cell.Text = "Get Logs From Device";
                    break;
                case "DELETE_USER":
                    e.Cell.Text = "Delete User From Device";
                    break;
                case "UPDATE_FIRMWARE":
                    e.Cell.Text = "Update Firmware Of Device";
                    break;
                case "SET_WEB_SERVER_INFO":
                    e.Cell.Text = "Set Device Server Settings";
                    break;
                case "GET_USER_INFO":
                    e.Cell.Text = "Get User Data From Device";
                    break;
                case "GET_DEVICE_STATUS":
                    e.Cell.Text = "Get Device User Count Status";
                    break;
                case "CLEAR_ENROLL_DATA":
                    e.Cell.Text = "Clear Device Enroll Data";
                    break;
                case "CLEAR_LOG_DATA":
                    e.Cell.Text = "Clear Device Log Data";
                    break;
                case "SET_FK_NAME":
                    e.Cell.Text = "Set Device Name";
                    break;
                case "RESET_FK":
                    e.Cell.Text = "Reset Device";
                    break;
                case "SET_USER_INFO":
                    e.Cell.Text = "Set User Data To Device";
                    break;
                case "flagupduserinfo":
                    e.Cell.Text = "Set User Details To Device";
                    break;
                case "flagsetfp":
                    e.Cell.Text = "Set FP Data To Device";
                    break;
                case "flagsetface":
                    e.Cell.Text = "Set Face Data To Device";
                    break;
                case "flagadmin":
                    e.Cell.Text = "Set Admin To Device";
                    break;
                case "flagdeluserinfo":
                    e.Cell.Text = "Delete User Data From Device";
                    break;
                case "ENABLEUSER":
                    e.Cell.Text = "Enable User Data In Device";
                    break;
                case "DISABLEUSER":
                    e.Cell.Text = "Disable User Data In Device";
                    break;
                case "CLEARADMIN":
                    e.Cell.Text = "Clear Admin From Device";
                    break;
                case "flagclearLog":
                    e.Cell.Text = "Clear Log From Device";
                    break;
                case "flagclearData":
                    e.Cell.Text = "Clear All Data From Device";
                    break;
                case "ATTLOG":
                    e.Cell.Text = "Get Attendance Data From Device";
                    break;
                case "flagremoteenroll":
                    e.Cell.Text = "Remote Enroll Finger";
                    break;
                case "flagremoteenrollFace":
                    e.Cell.Text = "Remote Enroll Finger";
                    break;
                default:
                    e.Cell.Text = "NA";
                    break;
            }
        }
    }

    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        try
        {
            sSql = "delete from tbl_fkcmd_trans where return_code is not null  ";
            cn.execute_NonQuery(sSql);

            sSql = "delete from DeviceCommands where Executed=0  ";
            cn.execute_NonQuery(sSql);
        }
        catch (Exception Exc)
        {
            Error_Occured("DeleteCommand", Exc.Message);

        }
    }

    protected void ASPxComboBox1_SelectedIndexChanged(object sender, EventArgs e)
    {
        ASPxGridView1.DataSource = null;
        ASPxGridView1.DataBind();
        if (ASPxComboBox1.SelectedIndex == 0)
        {
            try
            {
                DataTable DT = new DataTable();
                DT.Columns.Add("TransactionID");
                DT.Columns.Add("DeviceID");
                DT.Columns.Add("DeviceName");
                DT.Columns.Add("CommandCode");
                DT.Columns.Add("Result");
                DT.Columns.Add("Status");
                DT.Columns.Add("UpdateTime");

                sSql = "Select tbl_fkcmd_trans.trans_id,tbl_fkcmd_trans.device_id 'device_id',tbl_fkcmd_trans.cmd_code,tbl_fkcmd_trans.return_code,tbl_fkcmd_trans.status,tbl_fkcmd_trans.update_time, " +
                    "tbl_fkdevice_status.device_name from tbl_fkcmd_trans inner join tbl_fkdevice_status on tbl_fkcmd_trans.device_id=tbl_fkdevice_status.device_id";
                DataSet DsCmd = new DataSet();
                DsCmd = cn.FillDataSet(sSql);
                if (DsCmd.Tables[0].Rows.Count > 0)
                {
                    for (int B = 0; B < DsCmd.Tables[0].Rows.Count; B++)
                    {
                        DT.Rows.Add(DsCmd.Tables[0].Rows[B]["trans_id"].ToString().Trim(), DsCmd.Tables[0].Rows[B]["device_id"].ToString().Trim(), DsCmd.Tables[0].Rows[B]["device_name"].ToString().Trim(),
                            DsCmd.Tables[0].Rows[B]["cmd_code"].ToString().Trim(), DsCmd.Tables[0].Rows[B]["return_code"].ToString().Trim(), DsCmd.Tables[0].Rows[B]["status"].ToString().Trim(),
                            DsCmd.Tables[0].Rows[B]["update_time"].ToString().Trim());
                    }
                }
                string sSqlZK = "select DeviceCommands.Dev_Cmd_ID,DeviceCommands.TransferToDevice,DeviceCommands.CommandContent,DeviceCommands.Executed," +
                    "DeviceCommands.CreatedOn,tblMachine.DeviceName from DeviceCommands inner join tblMachine on tblMachine.SerialNumber=DeviceCommands.TransferToDevice";

                DataSet DsCmdZ = new DataSet();
                DsCmdZ = cn.FillDataSet(sSqlZK);
                if (DsCmdZ.Tables[0].Rows.Count > 0)
                {
                    for (int Z = 0; Z < DsCmdZ.Tables[0].Rows.Count; Z++)
                    {
                        DT.Rows.Add(DsCmdZ.Tables[0].Rows[Z]["Dev_Cmd_ID"].ToString().Trim(), DsCmdZ.Tables[0].Rows[Z]["TransferToDevice"].ToString().Trim(), DsCmdZ.Tables[0].Rows[Z]["DeviceName"].ToString().Trim(),
                            DsCmdZ.Tables[0].Rows[Z]["CommandContent"].ToString().Trim(), "", DsCmdZ.Tables[0].Rows[Z]["Executed"].ToString().Trim(),
                            DsCmdZ.Tables[0].Rows[Z]["CreatedOn"].ToString().Trim());
                    }
                }

                if (DT.Rows.Count > 0)
                {
                    ASPxGridView1.DataSource = DT;
                    ASPxGridView1.KeyFieldName = "DeviceID";
                    ASPxGridView1.DataBind();
                }
            }
            catch (Exception Exc)
            {
                Error_Occured("GetCommands", Exc.Message);

            }
        }
        else if (ASPxComboBox1.SelectedIndex == 1)
        {
            try
            {
                DataTable DT = new DataTable();
                DT.Columns.Add("TransactionID");
                DT.Columns.Add("DeviceID");
                DT.Columns.Add("DeviceName");
                DT.Columns.Add("CommandCode");
                DT.Columns.Add("Result");
                DT.Columns.Add("Status");
                DT.Columns.Add("UpdateTime");

                sSql = "Select tbl_fkcmd_trans.trans_id,tbl_fkcmd_trans.device_id 'device_id',tbl_fkcmd_trans.cmd_code,tbl_fkcmd_trans.return_code,tbl_fkcmd_trans.status,tbl_fkcmd_trans.update_time, " +
                    "tbl_fkdevice_status.device_name from tbl_fkcmd_trans inner join tbl_fkdevice_status on tbl_fkcmd_trans.device_id=tbl_fkdevice_status.device_id where tbl_fkcmd_trans.return_code is not null ";
                DataSet DsCmd = new DataSet();
                DsCmd = cn.FillDataSet(sSql);
                if (DsCmd.Tables[0].Rows.Count > 0)
                {
                    for (int B = 0; B < DsCmd.Tables[0].Rows.Count; B++)
                    {
                        DT.Rows.Add(DsCmd.Tables[0].Rows[B]["trans_id"].ToString().Trim(), DsCmd.Tables[0].Rows[B]["device_id"].ToString().Trim(), DsCmd.Tables[0].Rows[B]["device_name"].ToString().Trim(),
                            DsCmd.Tables[0].Rows[B]["cmd_code"].ToString().Trim(), DsCmd.Tables[0].Rows[B]["return_code"].ToString().Trim(), DsCmd.Tables[0].Rows[B]["status"].ToString().Trim(),
                            DsCmd.Tables[0].Rows[B]["update_time"].ToString().Trim());
                    }
                }
                string sSqlZK = "select DeviceCommands.Dev_Cmd_ID,DeviceCommands.TransferToDevice,DeviceCommands.CommandContent,DeviceCommands.Executed," +
                    "DeviceCommands.CreatedOn,tblMachine.DeviceName from DeviceCommands inner join tblMachine on tblMachine.SerialNumber=DeviceCommands.TransferToDevice where Executed=1 ";

                DataSet DsCmdZ = new DataSet();
                DsCmdZ = cn.FillDataSet(sSqlZK);
                if (DsCmdZ.Tables[0].Rows.Count > 0)
                {
                    for (int Z = 0; Z < DsCmdZ.Tables[0].Rows.Count; Z++)
                    {
                        DT.Rows.Add(DsCmdZ.Tables[0].Rows[Z]["Dev_Cmd_ID"].ToString().Trim(), DsCmdZ.Tables[0].Rows[Z]["TransferToDevice"].ToString().Trim(), DsCmdZ.Tables[0].Rows[Z]["DeviceName"].ToString().Trim(),
                            DsCmdZ.Tables[0].Rows[Z]["CommandContent"].ToString().Trim(), "", DsCmdZ.Tables[0].Rows[Z]["Executed"].ToString().Trim(),
                            DsCmdZ.Tables[0].Rows[Z]["CreatedOn"].ToString().Trim());
                    }
                }

                if (DT.Rows.Count > 0)
                {
                    ASPxGridView1.DataSource = DT;
                    ASPxGridView1.KeyFieldName = "DeviceID";
                    ASPxGridView1.DataBind();
                }
            }
            catch (Exception Exc)
            {
                Error_Occured("GetCommands", Exc.Message);

            }
        }
        else if (ASPxComboBox1.SelectedIndex == 2)
        {
            try
            {
                DataTable DT = new DataTable();
                DT.Columns.Add("TransactionID");
                DT.Columns.Add("DeviceID");
                DT.Columns.Add("DeviceName");
                DT.Columns.Add("CommandCode");
                DT.Columns.Add("Result");
                DT.Columns.Add("Status");
                DT.Columns.Add("UpdateTime");

                sSql = "Select tbl_fkcmd_trans.trans_id,tbl_fkcmd_trans.device_id 'device_id',tbl_fkcmd_trans.cmd_code,tbl_fkcmd_trans.return_code,tbl_fkcmd_trans.status,tbl_fkcmd_trans.update_time, " +
                    "tbl_fkdevice_status.device_name from tbl_fkcmd_trans inner join tbl_fkdevice_status on tbl_fkcmd_trans.device_id=tbl_fkdevice_status.device_id where tbl_fkcmd_trans.return_code is null";
                DataSet DsCmd = new DataSet();
                DsCmd = cn.FillDataSet(sSql);
                if (DsCmd.Tables[0].Rows.Count > 0)
                {
                    for (int B = 0; B < DsCmd.Tables[0].Rows.Count; B++)
                    {
                        DT.Rows.Add(DsCmd.Tables[0].Rows[B]["trans_id"].ToString().Trim(), DsCmd.Tables[0].Rows[B]["device_id"].ToString().Trim(), DsCmd.Tables[0].Rows[B]["device_name"].ToString().Trim(),
                            DsCmd.Tables[0].Rows[B]["cmd_code"].ToString().Trim(), DsCmd.Tables[0].Rows[B]["return_code"].ToString().Trim(), DsCmd.Tables[0].Rows[B]["status"].ToString().Trim(),
                            DsCmd.Tables[0].Rows[B]["update_time"].ToString().Trim());
                    }
                }
                string sSqlZK = "select DeviceCommands.Dev_Cmd_ID,DeviceCommands.TransferToDevice,DeviceCommands.CommandContent,DeviceCommands.Executed," +
                    "DeviceCommands.CreatedOn,tblMachine.DeviceName from DeviceCommands inner join tblMachine on tblMachine.SerialNumber=DeviceCommands.TransferToDevice Where Executed=0";

                DataSet DsCmdZ = new DataSet();
                DsCmdZ = cn.FillDataSet(sSqlZK);
                if (DsCmdZ.Tables[0].Rows.Count > 0)
                {
                    for (int Z = 0; Z < DsCmdZ.Tables[0].Rows.Count; Z++)
                    {
                        DT.Rows.Add(DsCmdZ.Tables[0].Rows[Z]["Dev_Cmd_ID"].ToString().Trim(), DsCmdZ.Tables[0].Rows[Z]["TransferToDevice"].ToString().Trim(), DsCmdZ.Tables[0].Rows[Z]["DeviceName"].ToString().Trim(),
                            DsCmdZ.Tables[0].Rows[Z]["CommandContent"].ToString().Trim(), "", DsCmdZ.Tables[0].Rows[Z]["Executed"].ToString().Trim(),
                            DsCmdZ.Tables[0].Rows[Z]["CreatedOn"].ToString().Trim());
                    }
                }

                if (DT.Rows.Count > 0)
                {
                    ASPxGridView1.DataSource = DT;
                    ASPxGridView1.KeyFieldName = "DeviceID";
                    ASPxGridView1.DataBind();
                }
            }
            catch (Exception Exc)
            {
                Error_Occured("GetCommands", Exc.Message);

            }
        }
        else
        {
            try
            {
                DataTable DT = new DataTable();
                DT.Columns.Add("TransactionID");
                DT.Columns.Add("DeviceID");
                DT.Columns.Add("DeviceName");
                DT.Columns.Add("CommandCode");
                DT.Columns.Add("Result");
                DT.Columns.Add("Status");
                DT.Columns.Add("UpdateTime");

                sSql = "Select tbl_fkcmd_trans.trans_id,tbl_fkcmd_trans.device_id 'device_id',tbl_fkcmd_trans.cmd_code,tbl_fkcmd_trans.return_code,tbl_fkcmd_trans.status,tbl_fkcmd_trans.update_time, " +
                    "tbl_fkdevice_status.device_name from tbl_fkcmd_trans inner join tbl_fkdevice_status on tbl_fkcmd_trans.device_id=tbl_fkdevice_status.device_id";
                DataSet DsCmd = new DataSet();
                DsCmd = cn.FillDataSet(sSql);
                if (DsCmd.Tables[0].Rows.Count > 0)
                {
                    for (int B = 0; B < DsCmd.Tables[0].Rows.Count; B++)
                    {
                        DT.Rows.Add(DsCmd.Tables[0].Rows[B]["trans_id"].ToString().Trim(), DsCmd.Tables[0].Rows[B]["device_id"].ToString().Trim(), DsCmd.Tables[0].Rows[B]["device_name"].ToString().Trim(),
                            DsCmd.Tables[0].Rows[B]["cmd_code"].ToString().Trim(), DsCmd.Tables[0].Rows[B]["return_code"].ToString().Trim(), DsCmd.Tables[0].Rows[B]["status"].ToString().Trim(),
                            DsCmd.Tables[0].Rows[B]["update_time"].ToString().Trim());
                    }
                }
                string sSqlZK = "select DeviceCommands.Dev_Cmd_ID,DeviceCommands.TransferToDevice,DeviceCommands.CommandContent,DeviceCommands.Executed," +
                    "DeviceCommands.CreatedOn,tblMachine.DeviceName from DeviceCommands inner join tblMachine on tblMachine.SerialNumber=DeviceCommands.TransferToDevice";

                DataSet DsCmdZ = new DataSet();
                DsCmdZ = cn.FillDataSet(sSqlZK);
                if (DsCmdZ.Tables[0].Rows.Count > 0)
                {
                    for (int Z = 0; Z < DsCmdZ.Tables[0].Rows.Count; Z++)
                    {
                        DT.Rows.Add(DsCmdZ.Tables[0].Rows[Z]["Dev_Cmd_ID"].ToString().Trim(), DsCmdZ.Tables[0].Rows[Z]["TransferToDevice"].ToString().Trim(), DsCmdZ.Tables[0].Rows[Z]["DeviceName"].ToString().Trim(),
                            DsCmdZ.Tables[0].Rows[Z]["CommandContent"].ToString().Trim(), "", DsCmdZ.Tables[0].Rows[Z]["Executed"].ToString().Trim(),
                            DsCmdZ.Tables[0].Rows[Z]["CreatedOn"].ToString().Trim());
                    }
                }

                if (DT.Rows.Count > 0)
                {
                    ASPxGridView1.DataSource = DT;
                    ASPxGridView1.KeyFieldName = "DeviceID";
                    ASPxGridView1.DataBind();
                }
            }
            catch (Exception Exc)
            {
                Error_Occured("GetCommands", Exc.Message);

            }
        }
    }
}