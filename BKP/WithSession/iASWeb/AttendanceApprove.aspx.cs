﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Configuration;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Web.Mail;
using System.Globalization;
using System.IO;
using System.Collections.Generic;

public partial class AttendanceApprove : System.Web.UI.Page
{
    string strsql = "";
    string txttodate, txtfromdate;
    string headEmail = "";
    string empEmail = "";
    DateTime date1, date2;
    OleDbDataReader LTDr;
    Class_Connection cn = new Class_Connection();
    OleDbConnection MyConn = new OleDbConnection();
    OleDbCommand MyCmd = new OleDbCommand();
    OleDbConnection Connection = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"]);
    int days;
    bool finalLevel;
    string sSql = "";
    string Pcode, Duration, vno, remarks, Approval_Status, Stage, Reason, RejectedRemarks;

    static DateTime dt_FinancialYear;

    OleDbConnection conn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString());
    OleDbCommand comm;
    string ErrorFileName = ConfigurationManager.AppSettings["ErrorFileName"].ToString();
    ErrorClass ec = new ErrorClass();

    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    protected void Page_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();
        if (ex is HttpRequestValidationException)
        {
            string resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" +
                   "Form doesn't need to contain valid HTML, just anything with opening and closing angled brackets (<...>).<br />" +
                   btnGoBack +
                   "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
        else if (ex is OleDbException)
        {
            string resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" + ex.Message.ToString() + ".<br />" +
                    "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
        else
        {
            string resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" + ex.Message.ToString() + ".<a href='Default.aspx' class='link'> Back to Login Page </a> <br />" +
                    "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        //Call The function to write the error log file 

        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            //ec.Write_Log(Session["ErrorFileName"].ToString(), DateTime.Now.Date, PageName, FunctionName, ErrorMsg);
        }
        catch (Exception errr)
        {
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            // Put user code to initialize the page here

            if (Session["UserName"] == null && Session["PAYCODE"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            
            if (conn.State == 0)
                conn.Open();
            if (!IsPostBack)
            {
                if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
                {
                    Request.Browser.Adapters.Clear();
                }
                fill_grid();
            }
        }
        catch (Exception er)
        {
            Error_Occured("Page Load", er.Message);
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        fill_grid();
        DataGrid1.DataBind();
    }
    public void fill_grid()
    {
        try
        {
            string ReportView = "";
            ReportView = ConfigurationSettings.AppSettings["ReportView_HeadID"].ToString();
            string Tmp = Session["PAYCODE"].ToString();
            if (Session["usertype"].ToString().ToUpper() == "A")
            {

                strsql = " select paycode,application_no, CONVERT(CHAR(10), request_date, 103) 'REQUEST DATE',CONVERT(CHAR(20), InTime, 113) 'Punch Time',UserRemarks 'Remarks', Stage1_Status = CASE WHEN " +
                        " Stage1_approval = 'Y' THEN 'APPROVED' ELSE CASE WHEN Stage1_approval = 'N' THEN 'Rejected' ELSE 'PENDING' END END, Stage1_Remarks = Stage1_Remarks1, " +
                        " Stage2_Status = CASE WHEN Stage2_approval = 'Y'THEN 'APPROVED' ELSE CASE WHEN Stage2_approval = 'N' THEN 'Rejected' ELSE 'PENDING'  END END," +
                        " Stage2_Remarks = Stage2_Remarks1, CONVERT(CHAR(10), stage1_approvalDate, 103) 'Approval/Rejection Date'  FROM Att_request WHERE " +
                        " Stage1_approval is null order by Application_No Desc";
            }
            else if (Session["usertype"].ToString().ToUpper() == "H")
            {

                strsql = " select paycode,application_no, CONVERT(CHAR(10), request_date, 103) 'REQUEST DATE',CONVERT(CHAR(20), InTime, 113) 'Punch Time',UserRemarks 'Remarks', Stage1_Status = CASE WHEN " +
                         " Stage1_approval = 'Y' THEN 'APPROVED' ELSE CASE WHEN Stage1_approval = 'N' THEN 'Rejected' ELSE 'PENDING' END END, Stage1_Remarks = Stage1_Remarks1, " +
                         " Stage2_Status = CASE WHEN Stage2_approval = 'Y'THEN 'APPROVED' ELSE CASE WHEN Stage2_approval = 'N' THEN 'Rejected' ELSE 'PENDING'  END END," +
                         " Stage2_Remarks = Stage2_Remarks1, CONVERT(CHAR(10), stage1_approvalDate, 103) 'Approval/Rejection Date'  FROM Att_request WHERE " +
                         " Stage1_approval is null and paycode in (select paycode from tblemployee where headid='" + Session["LoginPayCode"].ToString().Trim() + "') order by Application_No Desc";


            }

            else
                return;

            OleDbDataAdapter adapter = new OleDbDataAdapter(strsql, conn);
            DataSet ds = new DataSet();
            adapter.Fill(ds);

            DataGrid1.DataSource = ds;
            DataGrid1.DataBind();
        }
        catch (Exception Ex)
        {
            Error_Occured("fillgrid", Ex.Message);
        }

    }
    protected void btnApprove_Click(object sender, EventArgs e)
    {
        try
        {
            string code = "", Strsql;
            string pdate = "";
            DateTime ApplyDate;
            List<object> keys = DataGrid1.GetSelectedFieldValues(new string[] { DataGrid1.KeyFieldName });
            if (keys.Count == 0)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Please Select Any Application To Approve/Reject')</script>");
                return;
            }


            //if error occurs while approval, Please increase the length of REASON field of TBLTIMEREGISTER  
            ViewState["Approval_Status"] = "Approved";
            DataSet ds = new DataSet();
            OleDbDataAdapter adpter = new OleDbDataAdapter("", conn);
            string Tmp;
            string Pcode = "", CardNo = "", CompanyCode = "", DepartmentCode = "", SSN;
            DateTime Attdate = System.DateTime.MinValue;
            OleDbDataReader dr;
            for (int DR = 0; DR < DataGrid1.VisibleRowCount; DR++)
            {
                if (DataGrid1.Selection.IsRowSelected(DR) == true)
                {

                    Tmp = DataGrid1.GetRowValues(DR, "application_no").ToString().Trim();
                    Pcode = DataGrid1.GetRowValues(DR, "paycode").ToString().Trim();
                    txtfromdate = DataGrid1.GetRowValues(DR, "Punch Time").ToString().Trim();
                   
                    ApplyDate = Convert.ToDateTime(txtfromdate);
                    sSql = "Select presentCardno,companycode,departmentcode,ssn from tblemployee where Active='Y' And paycode ='" + Pcode + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' ";
                    dr = cn.ExecuteReader(sSql);
                    if (dr.Read())
                        CardNo = dr[0].ToString().Trim();
                    CompanyCode = dr[1].ToString().Trim();
                    DepartmentCode = dr[2].ToString().Trim();
                    SSN = dr[3].ToString().Trim();
                    dr.Close();
                    if (CardNo.Trim() == "")
                        return;
                    string SetupID = "1";

                    DataSet Rs = new DataSet();
                    sSql = " Select setupid from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tblsetup)";
                    Rs = cn.FillDataSet(sSql);
                    if (Rs.Tables[0].Rows.Count > 0)
                    {
                        SetupID = Rs.Tables[0].Rows[0][0].ToString().Trim();
                    }
                    try
                    {

                        Strsql = "Insert into MachineRawPunch  (CARDNO,PAYCODE,ISMANUAL,P_Day,OFFICEPUNCH,CompanyCode,SSN)  VALUES('" + CardNo + "','" + Pcode + "','Y','N','" + ApplyDate.ToString("yyyy-MM-dd HH:mm") + "','" + CompanyCode + "','" + SSN + "') ";
                       int result = cn.execute_NonQuery(Strsql);

                        if (result > 0)
                        {

                            Strsql = "update Att_request Set stage1_approval='Y',stage2_approval='Y',stage1_approvalID='"+ Session["LoginUserName"].ToString().Trim() + "',stage1_ApprovalDate=getdate() ,stage1_Remarks1='Approved' " +
                                " ,stage2_approvalID='" + Session["LoginUserName"].ToString().Trim() + "',stage2_ApprovalDate=getdate(),stage2_Remarks1='Approved' where application_no='" + Tmp + "' ";

                            cn.execute_NonQuery(Strsql);
                            Strsql = "Insert into tblLog_ManualPunch (Paycode,PunchDate,LoggedUser,LoggedDate)  VALUES('" + Pcode + "','" + ApplyDate.ToString("yyyy-MM-dd HH:mm") + "','" + Session["PAYCODE"].ToString().Trim() + "',getdate() )";
                            result = cn.execute_NonQuery(Strsql);

                            try
                            {
                                sSql = "INSERT INTO [dbo].[UserEditLog]([UserName],[LogDate],[LogDetails],[OldValue],[NewValue],[CompanyCode])values('" + Session["UserName"].ToString().Trim() + "',getdate(),'Manual Punch Posted For Code: " + Pcode + "','Punch Date: " + ApplyDate.ToString("dd/MM/yyyy") + "','Punch Time: " + ApplyDate.ToString("yyyy-MM-dd HH:mm") + "','" + Session["LoginCompany"].ToString().Trim() + "')";
                                cn.execute_NonQuery(sSql);

                            }
                            catch
                            {

                            }
                          try


                                {
                                    using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString()))
                                    {

                                        MyCmd = new OleDbCommand("ProcessBackDate", MyConn);
                                        MyCmd.CommandTimeout = 1800;
                                        MyCmd.CommandType = CommandType.StoredProcedure;
                                        MyCmd.Parameters.Add("@FromDate", SqlDbType.VarChar).Value = ApplyDate.AddDays(-1).ToString("yyyy-MM-dd");
                                        MyCmd.Parameters.Add("@ToDate", SqlDbType.VarChar).Value = System.DateTime.Now.ToString("yyyy-MM-dd");
                                        MyCmd.Parameters.Add("@CompanyCode", SqlDbType.VarChar).Value = CompanyCode;
                                        MyCmd.Parameters.Add("@DepartmentCode", SqlDbType.VarChar).Value = DepartmentCode;
                                        MyCmd.Parameters.Add("@PayCode", SqlDbType.VarChar).Value = Pcode;
                                        MyCmd.Parameters.Add("@SetupId", SqlDbType.VarChar).Value = SetupID;
                                        MyConn.Open();
                                        result = MyCmd.ExecuteNonQuery();
                                        MyConn.Close();

                                    }
                                }
                            catch (Exception Ex)
                            {
                                Error_Occured("SP", Ex.Message);
                            }
                            fill_grid();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Request Approved Successfully');", true);

                            result = 0;
                           
                        }

                    }
                    catch (Exception Ex)
                    {
                        Error_Occured("MRP", Ex.Message);
                    }




                }
            }
        }
        catch (Exception Ex)
        {
            Error_Occured("Approve", Ex.Message);
        }
    }

    protected void btncan_Click(object sender, EventArgs e)
    {
        Response.Redirect("Main.aspx");
    }

    protected void btnReject_Click(object sender, EventArgs e)
    {
        try
        {
            string code = "", Strsql;
            string pdate = "";
            DateTime ApplyDate;
            List<object> keys = DataGrid1.GetSelectedFieldValues(new string[] { DataGrid1.KeyFieldName });
            if (keys.Count == 0)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Please Select Any Application To Approve/Reject')</script>");
                return;
            }


            //if error occurs while approval, Please increase the length of REASON field of TBLTIMEREGISTER  
            ViewState["Approval_Status"] = "Approved";
            DataSet ds = new DataSet();
            OleDbDataAdapter adpter = new OleDbDataAdapter("", conn);
            string Tmp;
            string Pcode = "", CardNo = "", CompanyCode = "", DepartmentCode = "", SSN;
            DateTime Attdate = System.DateTime.MinValue;
            OleDbDataReader dr;
            for (int DR = 0; DR < DataGrid1.VisibleRowCount; DR++)
            {
                if (DataGrid1.Selection.IsRowSelected(DR) == true)
                {

                    Tmp = DataGrid1.GetRowValues(DR, "application_no").ToString().Trim();
                    Pcode = DataGrid1.GetRowValues(DR, "paycode").ToString().Trim();
                    txtfromdate = DataGrid1.GetRowValues(DR, "Punch Time").ToString().Trim();

                    ApplyDate = Convert.ToDateTime(txtfromdate);

                    Strsql = "update Att_request Set stage1_approval='Y',stage2_approval='Y',stage1_approvalID='" + Session["LoginUserName"].ToString().Trim() + "',stage1_ApprovalDate=getdate() ,stage1_Remarks1='Approved' " +
                               " ,stage2_approvalID='" + Session["LoginUserName"].ToString().Trim() + "',stage2_ApprovalDate=getdate(),stage2_Remarks1='Approved' where application_no='" + Tmp + "' ";

                    fill_grid();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Request Rejected Successfully');", true);

                }
            }
        }
        catch (Exception Ex)
        {
            Error_Occured("Reject", Ex.Message);
        }
    }
}