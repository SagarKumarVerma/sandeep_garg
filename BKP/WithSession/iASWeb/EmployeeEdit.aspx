﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="EmployeeEdit.aspx.cs" Inherits="EmployeeEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <script language="javascript" type="text/javascript" src="JS/validation_Employee.js"></script>
    <script language="javascript" type="text/javascript">
        function ismaxlength(Obj, MaxLen) {
            if (Obj.value.length > MaxLen) {
                alert('Maximum allowed address length is: ' + MaxLen);
                return false;
            }
            else
                return true;
        }
       
    </script>
    <div align="center">
        <table align="center" style="width: 1000px" cellpadding="0" cellspacing="0">
            <tr>
                <td style="height: 25px"></td>
            </tr>
            <tr>
                <td align="center">
                    <table cellpadding="0" cellspacing="0" style="text-align:left;width:850px" class="tableCss">
                        <tr>
                            <td align="center" class="tableHeaderCss" style="width:25px;width:850px">
                                <asp:Label ID="lblMsg" runat="server" CssClass="lblCss" Text="Employee"  ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="height:5px">&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="height:5px">
                                <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="0" EnableTheming="True" Theme="SoftOrange" Width="100%">
                                    <TabPages>
                                        <dx:TabPage Name="OD" Text="Office Details">
                                            <ActiveTabStyle Border-BorderColor="#666666" Border-BorderStyle="Solid">
                                            </ActiveTabStyle>
                                            <ContentCollection>
                                                <dx:ContentControl runat="server">
                                                    <table align="center" class="dxflInternalEditorTable" width="100%">
                                                        <tr>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Active" Theme="SoftOrange">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxComboBox ID="ddlActive" runat="server" EnableTheming="True" SelectedIndex="0" Theme="SoftOrange" OnSelectedIndexChanged="ddlActive_SelectedIndexChanged" AutoPostBack="True">
                                                                    <Items>
                                                                        <dx:ListEditItem Selected="True" Text="True" Value="Y" />
                                                                        <dx:ListEditItem Text="False" Value="N" />
                                                                    </Items>
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Photo">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px">
                                                                <asp:FileUpload ID="FileImage" runat="server" Width="300px" ToolTip="Only jpg File" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px">
                                                                &nbsp;</td>
                                                            <td style="padding: 5px">
                                                                &nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">
                                                                &nbsp;</td>
                                                            <td style="padding: 5px">
                                                                <asp:ImageButton ID="SetImage0" runat="server" Height="25px" ImageUrl="~/Images/Icons/IconSave.bmp" OnClick="SetImage_Click" ToolTip="Upload" Width="25px" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="lblEmpCode" runat="server" Theme="SoftOrange" Text="Employee Code*">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxTextBox ID="txtEmpCode" runat="server" Width="170px" MaxLength="10">
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px" rowspan="5">
                                                                <dx:ASPxImage ID="imgEmployee" runat="server" ShowLoadingImage="true" Height="200px" Width="200px" ImageUrl="~/Images/User.png" ClientInstanceName="UserPhoto">
                                                                    <Border BorderStyle="Solid" BorderWidth="2px" BorderColor="#54ABE1" />
                                                                </dx:ASPxImage>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="lblCardNo" runat="server" Theme="SoftOrange" Text="Biometric ID*">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxTextBox ID="txtCardNo" runat="server" Width="170px" MaxLength="8" onkeypress="fncInputNumericValuesOnly('txtCardNo')">
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="lblName" runat="server" Text="Employee Name*" Theme="SoftOrange">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxTextBox ID="txtName" runat="server" Width="170px" MaxLength="25">
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="lblFather" runat="server" Theme="SoftOrange" Text="Guardian Name">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxTextBox ID="txtGurName" runat="server" Width="170px">
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px; height: 35px;">
                                                                <dx:ASPxLabel ID="lblCompany" runat="server" Theme="SoftOrange" Text="Company">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px; height: 35px;">
                                                                <%--<dx:ASPxComboBox ID="ddlCompany" runat="server" Theme="SoftOrange">
                                                                </dx:ASPxComboBox>--%>
                                                                <dx:ASPxGridLookup ID="lookupComp" runat="server" KeyFieldName="CompanyCode" AutoGenerateColumns="False" MultiTextSeparator="-">
                                                                    <GridViewProperties>
                                                                        <SettingsBehavior AllowFocusedRow="True" AllowSelectSingleRowOnly="True" />
                                                                        <Settings GroupFormat="{0}-{1}" ShowFilterRow="True" />
                                                                    </GridViewProperties>
                                                                    <Columns>
                                                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="CompanyCode" ShowInCustomizationForm="True" VisibleIndex="0">
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="Name" FieldName="Companyname" ShowInCustomizationForm="True" VisibleIndex="1">
                                                                        </dx:GridViewDataTextColumn>
                                                                    </Columns>
                                                                    <ValidationSettings ValidationGroup="MK">
                                                                        <RequiredField IsRequired="True" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxGridLookup>
                                                            </td>
                                                            <td style="padding: 5px; height: 35px;"></td>
                                                            <td style="padding: 5px; height: 35px;"></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="lblDepartment" runat="server" Theme="SoftOrange" Text="Department">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxGridLookup ID="lookupDept" runat="server" KeyFieldName="DepartmentCode" MultiTextSeparator=" ">
                                                                    <GridViewProperties>
                                                                        <SettingsBehavior AllowFocusedRow="True" AllowSelectSingleRowOnly="True" />
                                                                        <Settings GroupFormat="{0}  {1}" ShowFilterRow="True" />
                                                                    </GridViewProperties>
                                                                    <Columns>
                                                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="DepartmentCode" ShowInCustomizationForm="True" VisibleIndex="0">
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="Name" FieldName="departmentname" ShowInCustomizationForm="True" VisibleIndex="1">
                                                                        </dx:GridViewDataTextColumn>
                                                                    </Columns>
                                                                    <ValidationSettings ValidationGroup="MK">
                                                                        <RequiredField IsRequired="True" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxGridLookup>
                                                               <%-- <dx:ASPxComboBox ID="ddlDepartment" runat="server" Theme="SoftOrange">
                                                                </dx:ASPxComboBox>--%>
                                                            </td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="Signature" Theme="SoftOrange" Visible="False">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxUploadControl ID="FileSignature" runat="server" UploadMode="Auto" Width="280px" Visible="False">
                                                                    <AdvancedModeSettings EnableFileList="True" EnableMultiSelect="True">
                                                                    </AdvancedModeSettings>
                                                                </dx:ASPxUploadControl>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="lblCatagory" runat="server" Text="Category" Theme="SoftOrange">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px">
                                                              <%--  <dx:ASPxComboBox ID="ddlCategory" runat="server" Theme="SoftOrange">
                                                                </dx:ASPxComboBox>--%>
                                                                 <dx:ASPxGridLookup ID="lookupCat" runat="server" KeyFieldName="CAT" MultiTextSeparator=" ">
                                                                    <GridViewProperties>
                                                                        <SettingsBehavior AllowFocusedRow="True" AllowSelectSingleRowOnly="True" />
                                                                        <Settings GroupFormat="{0}  {1}" ShowFilterRow="True" />
                                                                    </GridViewProperties>
                                                                     <Columns>
                                                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="CAT" ShowInCustomizationForm="True" VisibleIndex="0">
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="Name" FieldName="CATAGORYNAME" ShowInCustomizationForm="True" VisibleIndex="1">
                                                                        </dx:GridViewDataTextColumn>
                                                                    </Columns>
                                                                     <ValidationSettings ValidationGroup="MK">
                                                                        <RequiredField IsRequired="True" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxGridLookup>
                                                            </td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px" rowspan="2">
                                                                <dx:ASPxImage ID="imgSignature" runat="server" Height="40px" ImageAlign="Middle" ShowLoadingImage="True" Width="200px" Visible="False">
                                                                    <Border BorderStyle="Solid" BorderWidth="1px" />
                                                                </dx:ASPxImage>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="lblSection" runat="server" Theme="SoftOrange" Text="Section">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px">
                                                               <%-- <dx:ASPxComboBox ID="ddlSection" runat="server" Theme="SoftOrange">
                                                                </dx:ASPxComboBox>--%>
                                                                <dx:ASPxGridLookup ID="lookupSec" runat="server" KeyFieldName="DivisionCode" MultiTextSeparator=" ">
                                                                    <GridViewProperties>
                                                                        <SettingsBehavior AllowFocusedRow="True" AllowSelectSingleRowOnly="True" />
                                                                        <Settings GroupFormat="{0}  {1}" ShowFilterRow="True" />
                                                                    </GridViewProperties>
                                                                     <Columns>
                                                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="DivisionCode" ShowInCustomizationForm="True" VisibleIndex="0">
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="Name" FieldName="DivisionName" ShowInCustomizationForm="True" VisibleIndex="1">
                                                                        </dx:GridViewDataTextColumn>
                                                                    </Columns>
                                                                    <ValidationSettings ValidationGroup="MK">
                                                                        <RequiredField IsRequired="True" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxGridLookup>
                                                            </td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="lblGrade" runat="server" Theme="SoftOrange" Text="Sub Contractor">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px">
                                                              <%--  <dx:ASPxComboBox ID="ddlGrade" runat="server" Theme="SoftOrange">
                                                                </dx:ASPxComboBox>--%>
                                                                <dx:ASPxGridLookup ID="lookupGrade" runat="server" KeyFieldName="GradeCode" MultiTextSeparator=" ">
                                                                    <GridViewProperties>
                                                                        <SettingsBehavior AllowFocusedRow="True" AllowSelectSingleRowOnly="True" />
                                                                        <Settings GroupFormat="{0}  {1}" ShowFilterRow="True" />
                                                                    </GridViewProperties>
                                                                    <Columns>
                                                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="GradeCode" ShowInCustomizationForm="True" VisibleIndex="0">
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="Name" FieldName="GradeName" ShowInCustomizationForm="True" VisibleIndex="1">
                                                                        </dx:GridViewDataTextColumn>
                                                                    </Columns>
                                                                    <ValidationSettings ValidationGroup="MK">
                                                                        <RequiredField IsRequired="True" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxGridLookup>
                                                            </td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="User Type" Theme="SoftOrange">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxComboBox ID="ddlUType" runat="server" EnableTheming="True" SelectedIndex="0" Theme="SoftOrange">
                                                                    <Items>
                                                                        <dx:ListEditItem Selected="True" Text="User" Value="U" />
                                                                        <dx:ListEditItem Text="Manager" Value="H" />
                                                                    </Items>
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="lblEmployeeGroup" runat="server" Theme="SoftOrange" Text="Employee Group">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px">
                                                                <%--<dx:ASPxComboBox ID="ddlGroup" runat="server" Theme="SoftOrange">
                                                                </dx:ASPxComboBox>--%>
                                                                <dx:ASPxGridLookup ID="lookupGrp" runat="server" KeyFieldName="GroupID" MultiTextSeparator=" ">
                                                                    <GridViewProperties>
                                                                        <SettingsBehavior AllowFocusedRow="True" AllowSelectSingleRowOnly="True" />
                                                                        <Settings GroupFormat="{0}  {1}" ShowFilterRow="True" />
                                                                    </GridViewProperties>
                                                                    <Columns>
                                                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="GroupID" ShowInCustomizationForm="True" VisibleIndex="0">
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="Name" FieldName="GroupName" ShowInCustomizationForm="True" VisibleIndex="1">
                                                                        </dx:GridViewDataTextColumn>
                                                                    </Columns>
                                                                    <ValidationSettings ValidationGroup="MK">
                                                                        <RequiredField IsRequired="True" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxGridLookup>
                                                            </td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="ASPxLabel24" runat="server" Text="HOD" Theme="SoftOrange">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px">
                                                                <%--<dx:ASPxComboBox ID="lookupPay" runat="server" Theme="SoftOrange">
                                                                </dx:ASPxComboBox>--%>
                                                                <dx:ASPxGridLookup ID="lookupPay" runat="server" KeyFieldName="PAYCODE" MultiTextSeparator=" ">
                                                                    <GridViewProperties>
                                                                        <SettingsBehavior AllowFocusedRow="True" AllowSelectSingleRowOnly="True" />
                                                                        <Settings GroupFormat="{0}  {1}" ShowFilterRow="True" />
                                                                    </GridViewProperties>
                                                                    <Columns>
                                                                        <dx:GridViewDataTextColumn Caption="Code" FieldName="PAYCODE" ShowInCustomizationForm="True" VisibleIndex="0">
                                                                        </dx:GridViewDataTextColumn>
                                                                        <dx:GridViewDataTextColumn Caption="Name" FieldName="EMPNAME" ShowInCustomizationForm="True" VisibleIndex="1">
                                                                        </dx:GridViewDataTextColumn>
                                                                    </Columns>
                                                                </dx:ASPxGridLookup>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="lblDesignation" runat="server" Text="Designation" Theme="SoftOrange">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxTextBox ID="txtDesignation" runat="server" Width="170px">
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">
                                                                <asp:HiddenField ID="hfimage" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="lblLeavingDate" runat="server" Text="Leaving Date" Theme="SoftOrange" Visible="false">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px" runat="server" id="tdleave">
                                                                <dx:ASPxDateEdit ID="txtLeavingDate" runat="server" EditFormatString="dd/MM/yyyy" Visible="false">
                                                                </dx:ASPxDateEdit>
                                                            </td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">
                                                                <asp:HiddenField ID="hfEmpSig" runat="server" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="lblReason" runat="server" Theme="SoftOrange" Text="Resson" Visible="false" >
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxTextBox ID="txtLeavingreason" runat="server" Width="170px" Visible="false">
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">
                                                                &nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </dx:ContentControl>
                                            </ContentCollection>
                                        </dx:TabPage>
                                        <dx:TabPage Name="PD" Text="Personal Details">
                                            <ActiveTabStyle Border-BorderColor="#666666" Border-BorderStyle="Solid">
                                            </ActiveTabStyle>
                                            <ContentCollection>
                                                <dx:ContentControl runat="server">
                                                    <table align="center" class="dxflInternalEditorTable" width="100%">
                                                        <tr>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Date Of Join*" Theme="SoftOrange">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxDateEdit ID="txtDateOfjoin" runat="server" EditFormat="Custom" EditFormatString="dd/MM/yyyy" OnCalendarDayCellPrepared="txtDateOfjoin_CalendarDayCellPrepared">
                                                                    <ValidationSettings ValidationGroup="MK">
                                                                        <RequiredField IsRequired="True" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxDateEdit>
                                                            </td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px"></td>
                                                            <td style="padding: 5px">
                                                                <asp:Label  ID="Label5" runat="server" style="background-image: url(http://localhost:2313/Images/bg_nav.gif); text-align: left; padding-left: 10px; color: White; font-family: Verdana; font-size: 12px; height: 20px" Width="211px">Permanent Address</asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Theme="SoftOrange" Text="Date Of Birth">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxDateEdit ID="txtDOB" runat="server" EditFormat="Custom" EditFormatString="dd/MM/yyyy" OnCalendarDayCellPrepared="txtDOB_CalendarDayCellPrepared">
                                                                    <ValidationSettings ValidationGroup="MK">
                                                                        <RequiredField IsRequired="True" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxDateEdit>
                                                            </td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px" rowspan="5">
                                                                <table style="width: 100%">
                                                                    <tr>
                                                                        <td style="height: 28px" valign="top">
                                                                            <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Address" Theme="SoftOrange">
                                                                            </dx:ASPxLabel>
                                                                        </td>
                                                                        <td >
                                                                            <dx:ASPxMemo ID="txtPAddress" runat="server" Height="71px" Theme="SoftOrange" Width="170px">
                                                                            </dx:ASPxMemo>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="height: 28px">
                                                                            <dx:ASPxLabel ID="ASPxLabel22" runat="server" Text="PIN Code" Theme="SoftOrange">
                                                                            </dx:ASPxLabel>
                                                                        </td>
                                                                        <td style="height: 28px">
                                                                            <dx:ASPxTextBox ID="txtPPinCode" runat="server" Width="170px" MaxLength="6" onkeypress="fncInputNumericValuesOnly('txtPPinCode')">
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <dx:ASPxLabel ID="ASPxLabel23" runat="server" Text="Mobile No" Theme="SoftOrange">
                                                                            </dx:ASPxLabel>
                                                                        </td>
                                                                        <td>
                                                                            <dx:ASPxTextBox ID="txtPPhone" runat="server" Width="170px" MaxLength="10" onkeypress="fncInputNumericValuesOnly('txtPPhone')">
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>&nbsp;</td>
                                                                        <td>&nbsp;</td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="ASPxLabel8" runat="server" Theme="SoftOrange" Text="Married">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxComboBox ID="ddlMarried" runat="server" EnableTheming="True" SelectedIndex="0" Theme="SoftOrange">
                                                                    <Items>
                                                                        <dx:ListEditItem Selected="true" Text="No" Value="N" />
                                                                        <dx:ListEditItem Text="Yes" Value="Y" />
                                                                    </Items>
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="Gender" Theme="SoftOrange">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxRadioButtonList ID="radSex" runat="server" RepeatDirection="Horizontal" SelectedIndex="0" Theme="SoftOrange">
                                                                    <Items>
                                                                        <dx:ListEditItem  Text="Male" Value="M" />
                                                                        <dx:ListEditItem Text="Female" Value="F" />
                                                                    </Items>
                                                                    <Border BorderStyle="None" />
                                                                </dx:ASPxRadioButtonList>
                                                            </td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="ASPxLabel10" runat="server" Theme="SoftOrange" Text="Email">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxTextBox ID="txtEmail" runat="server" Width="170px">
                                                                    <ValidationSettings SetFocusOnError="True" ValidationGroup="MK">
                                                                        <RegularExpression ErrorText="*Invalid Email Id" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                                                                    </ValidationSettings>
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px; height: 35px;">
                                                                <dx:ASPxLabel ID="ASPxLabel11" runat="server" Theme="SoftOrange" Text="Blood Group">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px; height: 35px;">
                                                                <dx:ASPxComboBox ID="ddlBloodGroup" runat="server" EnableTheming="True" SelectedIndex="0" Theme="SoftOrange">
                                                                    <Items>
                                                                        <dx:ListEditItem Selected="True" Text="NA" Value="NA" />
                                                                        <dx:ListEditItem Text="O+" Value="O+" />
                                                                        <dx:ListEditItem Text="O-" Value="O-" />
                                                                        <dx:ListEditItem Text="A+" Value="A+" />
                                                                        <dx:ListEditItem Text="A-" Value="A-" />
                                                                        <dx:ListEditItem Text="B+" Value="B+" />
                                                                        <dx:ListEditItem Text="B-" Value="B-" />
                                                                    </Items>
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                            <td style="padding: 5px; height: 35px;"></td>
                                                            <td style="padding: 5px; height: 35px;"></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="ASPxLabel12" runat="server" Theme="SoftOrange" Text="Qualification">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxTextBox ID="txtQualification" runat="server" Width="170px">
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">
                                                                <asp:Label ID="Label6" runat="server" style="background-image: url(http://localhost:2313/Images/bg_nav.gif); text-align: left; padding-left: 10px; color: White; font-family: Verdana; font-size: 12px; height: 20px" Width="211px">Temporary Address</asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="ASPxLabel14" runat="server" Text="Experience" Theme="SoftOrange">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxTextBox ID="txtExperience" runat="server" Width="170px">
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px" rowspan="2">
                                                                <table style="width: 100%">
                                                                    <tr>
                                                                        <td style="height: 28px" valign="top">
                                                                            <dx:ASPxLabel ID="ASPxLabel13" runat="server" Text="Address" Theme="SoftOrange">
                                                                            </dx:ASPxLabel>
                                                                        </td>
                                                                        <td >
                                                                            <dx:ASPxMemo ID="txtTAddress" runat="server" Height="71px" Theme="SoftOrange" Width="170px">
                                                                            </dx:ASPxMemo>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="height: 28px">
                                                                            <dx:ASPxLabel ID="ASPxLabel17" runat="server" Text="PIN Code" Theme="SoftOrange">
                                                                            </dx:ASPxLabel>
                                                                        </td>
                                                                        <td style="height: 28px">
                                                                            <dx:ASPxTextBox ID="txtTPinCode" runat="server" Width="170px" MaxLength="6" onkeypress="fncInputNumericValuesOnly('txtTPinCode')">
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <dx:ASPxLabel ID="ASPxLabel20" runat="server" Text="Mobile No" Theme="SoftOrange">
                                                                            </dx:ASPxLabel>
                                                                        </td>
                                                                        <td>
                                                                            <dx:ASPxTextBox ID="txtTPhone" runat="server" Width="170px" MaxLength="10" onkeypress="fncInputNumericValuesOnly('txtTPhone')">
                                                                            </dx:ASPxTextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>&nbsp;</td>
                                                                        <td>&nbsp;</td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="ASPxLabel15" runat="server" Theme="SoftOrange" Text="Bank">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxComboBox ID="ddlBank" runat="server" Theme="SoftOrange">
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="ASPxLabel16" runat="server" Theme="SoftOrange" Text="Account No">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxTextBox ID="txtAC" runat="server" Width="170px">
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="ASPxLabel18" runat="server" Theme="SoftOrange" Text="UID">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxTextBox ID="txtVehicle" runat="server" onkeypress="fncInputNumericValuesOnly('txtVehicle')" MaxLength="12" Width="170px" >
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxLabel ID="ASPxLabel19" runat="server" Text="PAN No" Theme="SoftOrange">
                                                                </dx:ASPxLabel>
                                                            </td>
                                                            <td style="padding: 5px">
                                                                <dx:ASPxTextBox ID="txtBusRoute" runat="server" Width="170px">
                                                                </dx:ASPxTextBox>
                                                            </td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                            <td style="padding: 5px">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </dx:ContentControl>
                                            </ContentCollection>
                                        </dx:TabPage>
                                    </TabPages>
                                </dx:ASPxPageControl>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="padding: 5px">
                                <dx:ASPxLabel ID="lblstatus" runat="server" Text="" ForeColor="Red">
                                </dx:ASPxLabel>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="padding: 5px">
                                <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Save" OnClick="ASPxButton1_Click" ValidationGroup="MK">
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <script type="text/javascript">
        function UpdateUploadButton() {
            var isAnyFileSelected = false;
            for (var i = 0; i < uploadControl.GetFileInputCount() ; i++) {
                if (uploadControl.GetText(i) != "") { isAnyFileSelected = true; break; }
            }
            btnUploadViaPostback.SetEnabled(isAnyFileSelected);
            btnUploadViaCallback.SetEnabled(isAnyFileSelected);
        }
    </script>
</asp:Content>

