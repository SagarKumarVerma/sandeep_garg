﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using GlobalSettings;
using System.Collections.Generic;

public partial class LevelUpdation : System.Web.UI.Page
{

    public string txtFromDateCID = "";
    public string txtToDateCID = "";


    public OleDbCommand cmd;
    public OleDbDataAdapter da;
    public OleDbDataReader dr;
    public DataSet ds;
    string strSql = "";
    string strlevelcode;
    string lblcode = "";
    int QryResult = 0;
    Class_Connection cn = new Class_Connection();
    ErrorClass ec = new ErrorClass();

    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    protected void Page_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();
        if (ex is HttpRequestValidationException)
        {
            string resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" +
                   "Form doesn't need to contain valid HTML, just anything with opening and closing angled brackets (<...>).<br />" +
                   btnGoBack +
                   "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
    }

    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        //Call The function to write the error log file 

        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        ec.Write_Log(PageName, FunctionName, ErrorMsg);
    }

    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserName"] == null)
        {
            Session.Abandon();
            Response.Redirect("Login.aspx");
        }

        if (!IsPostBack)
        {
            if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
            {
                Request.Browser.Adapters.Clear();
            }
            Session["Updation"] = "FrmLeaveApprovalStages";
           
            LblSel.Text = "Stages";
            TxtHeadId1.Visible = false;
            Fill_Dept();
            fill_grid();
            bindheadid();

            if (Session["Updation"].ToString() == "FrmLevelChange")
                fill_combo();
            else
            {
                combo_Level.SelectedIndex = 0;
                combo_Level_SelectedIndexChanged(sender, e);
            }
            
        }

       

        if (RadLvApproval.Checked)
        {
            Session["Updation"] = "FrmLeaveApprovalStages";
            
        }

        if (RdHeadId.Checked)
        {
            Session["Updation"] = "FrmHeadIdUpdate";
            
        }

       
    }

    protected void bindheadid()
    {
        strSql = "select e.paycode +' - '+ e.empname 'EmpName1',e.paycode 'Pay' from tblemployee e join tbluser u on e.paycode=u.user_r where e.active='Y' and u.usertype='H' AND e.CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' order by e.paycode ";
        ds = new DataSet();
        ds = cn.FillDataSet(strSql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            TxtHeadId1.DataSource = ds.Tables[0];
            TxtHeadId1.TextField = "EmpName1";
            TxtHeadId1.ValueField = "Pay";
            TxtHeadId1.DataBind();
            TxtHeadId1.SelectedIndex = -1;
        }
        else
        {
            TxtHeadId1.DataSource = null;
            TxtHeadId1.DataBind();
        }
    }

   

    private void Fill_Dept()
    {

        strSql = " Select DepartmentName,DepartmentCode from tblDepartment Where CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"' Order by DepartmentName ";
        DataSet dsDept = new DataSet();
        dsDept = cn.FillDataSet(strSql);
        DdlDept.Items.Clear();
        DdlDept.DataSource = dsDept.Tables[0];
        DdlDept.TextField = "Departmentname";
        DdlDept.ValueField = "DepartmentCode";
        DdlDept.DataBind();
       // DdlDept.SelectedIndex = -1;
       // DdlDept.Items.Insert(0, new ListItem("All", "All"));
    }

    public void fill_combo()
    {
        try
        {

        strSql = " select * from tbllevel order by levelcode ";
        ds = new DataSet();
        ds = cn.FillDataSet(strSql);
        combo_Level.DataSource = ds;
        combo_Level.TextField = "levelname";
        combo_Level.ValueField = "levelcode";
        combo_Level.DataBind();

        }
        catch (Exception er)
        {
            Error_Occured("fill_combo", er.Message);
        }

    }


    public void fill_grid()
    {
        try
        {


        if (Session["Updation"].ToString() == "FrmLevelChange")
            strSql = "select Paycode,Empname,DepartmentName, Levelcode As 'Level' from tblemployee e, tbldepartment d ";

        if (Session["Updation"].ToString() == "FrmLeaveApprovalStages")
            strSql = "select Paycode,Empname, DepartmentName, LeaveApprovalStages As 'Level' from tblemployee e, tbldepartment d";

        if (Session["Updation"].ToString() == "FrmHeadIdUpdate")
            strSql = "select e.Paycode,e.Empname,DepartmentName, e.HeadId + ' - '+ ltrim(h.empname) as Level from tbldepartment d , tblemployee e left outer join tblemployee h on  e.headid = h.paycode  ";

        //strSql = "select e.paycode,e.empname,DepartmentName, e.HeadId from tblemployee e, tbldepartment d ";

        strSql += " WHERE e.Active='Y'  And  e.departmentcode= d.departmentcode ";

        if (DdlDept.SelectedIndex == -1)
            strSql += " And e.departmentcode IN (" + DataFilter.AuthDept.Trim() + ") ";
        else
            strSql += " And e.departmentcode ='" + DdlDept.SelectedItem.Value.ToString() + "' ";

        if (Session["IsOA"].ToString() == "Y" && Session["ActualUserType"].ToString() == "T")
        {
            strSql += " UNION ";
            //*************

            if (Session["Updation"].ToString() == "FrmLevelChange")
                strSql += " select Paycode,Empname,DepartmentName,Levelcode As 'Level' from tblemployee e, tbldepartment d ";

            if (Session["Updation"].ToString() == "FrmLeaveApprovalStages")
                strSql += " select Paycode,Empname,DepartmentName, LeaveApprovalStages As 'Approval Level' from tblemployee e, tbldepartment d ";

            if (Session["Updation"].ToString() == "FrmHeadIdUpdate")
                strSql += " select Paycode,Empname, DepartmentName,HeadId from tblemployee e, tbldepartment d ";

            //*************
            strSql += " where e.departmentcode = ( select Departmentcode from tblemployee where paycode='" + Session["paycode"].ToString() + "' ) ";
            strSql += " And Teamid in (Select Teamid from tblemployee where paycode='" + Session["paycode"].ToString() + "' ) ";
        }

        strSql += " Order by DepartmentName, e.paycode ";
        Session["lastQry"] = strSql.ToString();
        ds = new DataSet();
        ds = cn.FillDataSet(strSql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            DataGrid1.DataSource = ds;
            DataGrid1.DataBind();
        } 
        else
        {
            DataGrid1.DataSource = null;
            DataGrid1.DataBind();
            
        }
        }
        catch (Exception er)
        {
            Error_Occured("fill_grid", er.Message);
        }

    }


    protected void combo_Level_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["lblcode"] = combo_Level.SelectedItem.Value.ToString();
    }
    protected void DdlDept_SelectedIndexChanged(object sender, EventArgs e)
    {
        fill_grid();
    }
    protected void RadLvApproval_CheckedChanged(object sender, EventArgs e)
    {
        try
        {

            if (RadLvApproval.Checked)
            {
                combo_Level_SelectedIndexChanged(sender, e);
                LblSel.Text = "Stages";
                TxtHeadId1.Visible = false;
                combo_Level.Visible = true;
                Session["Updation"] = "FrmLeaveApprovalStages";
                fill_grid();
            }
           
            
        }
        catch (Exception er)
        {
            Error_Occured("RadLvApproval_CheckedChanged", er.Message);
        }
    }
    protected void RdHeadId_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            if (RdHeadId.Checked)
            {
                LblSel.Text = "HeadId ";
                TxtHeadId1.Visible = true;
                TxtHeadId1.Items.Clear();
                combo_Level.Visible = false;
                Session["Updation"] = "FrmHeadIdUpdate";
                fill_grid();
                bindheadid();
            }

        }
        catch (Exception er)
        {
            Error_Occured("RdHeadId_CheckedChanged", er.Message);
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            fill_grid();
            DataGrid1.DataBind();
        }

        
    }
    private int FindHeadId()
    {
        try
        {
            OleDbDataReader drH;
            string qry = "select * from TblEmployee where PayCode='" + lblcode.ToString() + "' and CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"'";
            drH = cn.Execute_Reader(qry);
            if (drH.Read())
            {
                drH.Close();
                return 1;
            }
            else
            {
                drH.Close();
                return 0;
            }

        }
        catch (Exception er)
        {
            Error_Occured("FindHeadId", er.Message);
            return 0;
        }
    }
    protected void btn_Update_Click(object sender, EventArgs e)
    {
        string change;
        
        try
        {
            List<object> KeyUser = DataGrid1.GetSelectedFieldValues(new string[] { DataGrid1.KeyFieldName });
            if (KeyUser.Count == 0 )
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Please Select Atleast Employee')</script>");
                return;
            }
            int found;
            lblcode = "";
            if (!RdHeadId.Checked)
            {
                //if (ViewState["lblcode"].ToString() != null)
                    try
                    {
                        lblcode = combo_Level.SelectedItem.Value.ToString();
                    }
                   catch
                    {
                        ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Please Select Approval Stage')</script>");
                        return;
                    }

            }
            else
            {
                if (TxtHeadId1.SelectedItem.Text.ToString().Trim()== "NONE")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Select HeadID From Combobox ');", true);
                    TxtHeadId1.Focus();
                    return;
                }
                lblcode = TxtHeadId1.SelectedItem.Value.ToString();
            }
            strlevelcode = string.Empty;
            for (int i = 0; i < DataGrid1.VisibleRowCount; i++)
            {
                found = 1;
                if (DataGrid1.Selection.IsRowSelected(i) == true)
                {
                    strlevelcode = DataGrid1.GetRowValues(i, "Paycode").ToString().Trim(); 

                    if (Session["Updation"].ToString() == "FrmLevelChange")
                        strSql = "update tblemployee set levelcode='" + lblcode.ToString() + "' where paycode='" + strlevelcode.ToString() + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' ";


                    if (Session["Updation"].ToString() == "FrmLeaveApprovalStages")
                        strSql = "update tblemployee set LeaveApprovalStages ='" + lblcode.ToString() + "' where paycode='" + strlevelcode.ToString() + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";


                    if (Session["Updation"].ToString() == "FrmHeadIdUpdate")
                    {
                        found = FindHeadId();
                        if (found == 1)
                            strSql = "update tblemployee set Headid ='" + lblcode.ToString().ToUpper() + "' where paycode='" + strlevelcode.ToString() + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
                        else
                        {
                            change = "<script language='JavaScript'>alert('Invalid HeadId  !!!')</script>";
                            Page.RegisterStartupScript("frmchange", change);
                        }
                    }

                    if (found == 1)
                    {
                        QryResult = cn.execute_NonQuery(strSql);
                        //Search();

                    }

                }
            }
           // fill_grid();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Updated Successfully');document.location='LevelUpdation.aspx'", true);
        }
        catch (Exception er)
        {
            Error_Occured("btn_Update_Click", er.Message);
        }
    }
    protected void DataGrid1_HtmlDataCellPrepared(object sender, DevExpress.Web.ASPxGridViewTableDataCellEventArgs e)
    {
        try
        {
            //if (e.DataColumn.Caption == "Leave Approval Stage")
            //{
            //    if (RadLvApproval.Checked)
            //    {
            //        e.DataColumn.Caption = "Leave Approval Stage";
            //    }
            //    else if(RdHeadId.Checked)
            //    {
            //        e.DataColumn.Caption = " Reporting Head ";
            //    }
            //    else
            //    {
            //        e.DataColumn.Caption = "Leave Approval Stage";
            //    }

            //}

        }
        catch
        {

        }
    }
    protected void DataGrid1_Init(object sender, EventArgs e)
    {
        fill_grid();
        DataGrid1.DataBind();
    }
}