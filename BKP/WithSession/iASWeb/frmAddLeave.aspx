﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="frmAddLeave.aspx.cs" Inherits="frmAddLeave" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
  <div style="center">
      <dx:ASPxRoundPanel ID="pnlLEave" runat="server" ShowCollapseButton="false" Width="800px" Theme="Office2010Silver" ShowHeader="False">



          <PanelCollection>
<dx:PanelContent runat="server">
    <table class="dxflInternalEditorTable" style="padding: 5px" Width="500px" >
        <tr>
           <td style="padding: 5px">
                <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Leave Field">
                </dx:ASPxLabel>
            </td>
             <td style="padding: 5px">
                <dx:ASPxTextBox ID="txtLeaveField" runat="server" Width="70px" Enabled="false">
                </dx:ASPxTextBox>
            </td>
             <td style="padding: 5px">&nbsp;</td>
             <td style="padding: 5px">&nbsp;</td>
        </tr>
        <tr>
                <td style="padding: 5px">
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Leave Code">
                </dx:ASPxLabel>
            </td>
             <td style="padding: 5px">
                <dx:ASPxTextBox ID="txtLeaveCode" runat="server" Width="70px" MaxLength="3" >
                </dx:ASPxTextBox>
            </td>
             <td style="padding: 5px">&nbsp;</td>
             <td style="padding: 5px">&nbsp;</td>
        </tr>
        <tr>
                <td style="padding: 5px">
                <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="Leave Discription">
                </dx:ASPxLabel>
            </td>
             <td style="padding: 5px">
                <dx:ASPxTextBox ID="txtDescription" runat="server" Width="150px"  MaxLength="50" >
                </dx:ASPxTextBox>
            </td>
             <td style="padding: 5px" colspan="2" rowspan="5">
                 <table width="500px" id="LA" runat="server" visible="false">
                     <tr>
                         <td colspan="4">
                             <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="Senction Leave">
                             </dx:ASPxLabel>
                         </td>
                        
                     </tr>
                     <tr>
                          <td style="padding: 5px">
                             <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="Min">
                             </dx:ASPxLabel>
                         </td>
                          <td style="padding: 5px; width: 146px;">
                             <dx:ASPxTextBox ID="txtMin" runat="server" Width="70px">
                             </dx:ASPxTextBox>
                         </td>
                          <td style="padding: 5px">
                             <dx:ASPxLabel ID="ASPxLabel10" runat="server" Text="Max">
                             </dx:ASPxLabel>
                         </td>
                          <td style="padding: 5px">
                             <dx:ASPxTextBox ID="txtMax" runat="server" Width="70px">
                             </dx:ASPxTextBox>
                         </td>
                     </tr>
                     <tr>
                          <td style="padding: 5px">&nbsp;</td>
                          <td style="padding: 5px; width: 146px;">
                              <dx:ASPxRadioButton ID="radFixed"  GroupName="a" Text="Fixed"  Checked="True" runat="server">
                              </dx:ASPxRadioButton>
                          </td>
                          <td style="padding: 5px">
                              <dx:ASPxRadioButton ID="radCarried" GroupName="a" Text="Carried" runat="server">
                              </dx:ASPxRadioButton>
                          </td>
                          <td style="padding: 5px">&nbsp;</td>
                     </tr>
                     <tr>
                          <td style="padding: 5px">
                              <dx:ASPxLabel ID="ASPxLabel11" runat="server" Text="Present">
                              </dx:ASPxLabel>
                          </td>
                          <td style="padding: 5px; width: 146px;">
                              <dx:ASPxTextBox ID="txtPresent" runat="server" Width="70px">
                              </dx:ASPxTextBox>
                          </td>
                          <td style="padding: 5px">
                              <dx:ASPxLabel ID="ASPxLabel12" runat="server" Text="Leave">
                              </dx:ASPxLabel>
                          </td>
                          <td style="padding: 5px">
                              <dx:ASPxTextBox ID="txtLeave" runat="server" Width="70px">
                              </dx:ASPxTextBox>
                          </td>
                     </tr>
                      <tr>
                          <td style="padding: 5px">
                              <dx:ASPxLabel ID="ASPxLabel13" runat="server" Text="Max Leave Accural">
                              </dx:ASPxLabel>
                          </td>
                          <td style="padding: 5px; width: 146px;">
                              <dx:ASPxTextBox ID="txtMaxAccuralLimit" runat="server" Width="70px">
                              </dx:ASPxTextBox>
                          </td>
                          <td style="padding: 5px">
                              &nbsp;</td>
                          <td style="padding: 5px">
                              &nbsp;</td>
                     </tr>
                 </table>
                </td>
        </tr>
    <tr>
                <td style="padding: 5px">
                <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="Visible For Apply">
                </dx:ASPxLabel>
            </td>
             <td style="padding: 5px">
                 <dx:ASPxCheckBox ID="ChkShowOnWeb" runat="server" CheckState="Unchecked">
                 </dx:ASPxCheckBox>
            </td>
        </tr>
        <tr>
                <td style="padding: 5px">
                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Is Off Include">
                </dx:ASPxLabel>
            </td>
             <td style="padding: 5px">
                 <dx:ASPxCheckBox ID="chkWeeklyOff" runat="server" CheckState="Unchecked">
                 </dx:ASPxCheckBox>
            </td>
        </tr>
        <tr>
                <td style="padding: 5px">
                <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="Is Holiday Include">
                </dx:ASPxLabel>
            </td>
             <td style="padding: 5px">
                 <dx:ASPxCheckBox ID="chkHolidays" runat="server" CheckState="Unchecked">
                 </dx:ASPxCheckBox>
            </td>
        </tr>
        <tr>
                <td style="padding: 5px">
                <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="Is Accural">
                </dx:ASPxLabel>
            </td>
             <td style="padding: 5px">
                 <dx:ASPxCheckBox ID="chkIsAccrual" runat="server" AutoPostBack="True" CheckState="Unchecked" OnCheckedChanged="chkIsAccrual_CheckedChanged">
                 </dx:ASPxCheckBox>
            </td>
        </tr>
        <tr>
                <td style="padding: 5px">
                    <dx:ASPxLabel ID="ASPxLabel14" runat="server" Text="Leave Type">
                    </dx:ASPxLabel>
                </td>
             <td style="padding: 5px">
                 <dx:ASPxComboBox ID="ddlLeaveType" runat="server" SelectedIndex="0">
                     <Items>
                         <dx:ListEditItem Text="Leave" Value="L" />
                         <dx:ListEditItem Text="Present" Value="P" />
                         <dx:ListEditItem Text="Absent" Value="A" />
                     </Items>
                 </dx:ASPxComboBox>
                </td>
             <td style="padding: 5px">&nbsp;</td>
             <td style="padding: 5px">&nbsp;</td>
        </tr>
        <tr>
                <td style="padding: 5px">
                    &nbsp;</td>
             <td style="padding: 5px">
                 <asp:HiddenField ID="hdFixed" runat="server" />
                </td>
             <td style="padding: 5px">&nbsp;</td>
             <td style="padding: 5px">
                 <asp:HiddenField ID="hdIsAccrual" runat="server" />
                </td>
        </tr>
        <tr>
                <td style="padding: 5px" colspan="4">
                    
                </td>
            
        </tr>
        <tr>
                <td style="padding: 5px">
                    <dx:ASPxButton ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click">
                    </dx:ASPxButton>
                </td>
             <td style="padding: 5px">
                 &nbsp;</td>
             <td style="padding: 5px">&nbsp;</td>
             <td style="padding: 5px" align="right">
                 &nbsp;</td>
        </tr>
      
        
    </table>
              </dx:PanelContent>
</PanelCollection>



      </dx:ASPxRoundPanel>
  </div>
</asp:Content>

