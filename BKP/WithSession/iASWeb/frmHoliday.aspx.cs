﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using GlobalSettings;
using System.Collections.Generic;
using DevExpress.Web;
using System.Text.RegularExpressions;
public partial class frmHoliday : System.Web.UI.Page
{
        Class_Connection Con = new Class_Connection();
        string Strsql = null;
        DataSet ds = null;
        int result = 0;
        ErrorClass ec = new ErrorClass();
        string DName,DHOD,DEmail;
        bool isNewRecord = false;
        OleDbConnection MyConn = new OleDbConnection();
        OleDbCommand MyCmd = new OleDbCommand();
    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    protected void Page_Load(object sender, EventArgs e)
    {
         if (!Page.IsPostBack)
        {

            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            //if (Session["ComVisible"] == null)
            //{
            //    Response.Redirect("PageNotFound.aspx");
            //}
            BindData();
        }
    }

        private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }
        protected void BindData()
        {
            try
            {
                if (Session["LoginUserName"].ToString().Trim().ToUpper() == "ADMIN")
                {
                    Strsql = "select distinct convert(varchar(100),hdate,103) as 'Date',Holiday ,COMPANYCODE,DEPARTMENTCODE from holiday  where " +
                           "datepart(yyyy,hdate)=datepart(yyyy,getdate()) and   CompanyCode in ('" + Session["Auth_Comp"].ToString().Trim() + "') ";
                }
                else
                {
                    Strsql = "select distinct convert(varchar(100),hdate,103) as 'Date',Holiday ,COMPANYCODE,DEPARTMENTCODE from holiday  where " +
                        "datepart(yyyy,hdate)=datepart(yyyy,getdate()) and   CompanyCode in ("+Session["Auth_Comp"].ToString().Trim()+") ";
                
                }

                ds = new DataSet();
                ds = Con.FillDataSet(Strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GrdHoliday.DataSource = ds.Tables[0];
                    GrdHoliday.DataBind();
                }
            }
            catch (Exception E)
            {
                Error_Occured("BindData", E.Message);
            }


        }
        protected void Page_Init(object sender, EventArgs e)
        {
            BindData();
            GrdHoliday.DataBind();
        }
        private void AddError(Dictionary<GridViewColumn, string> errors, GridViewColumn column, string errorText)
        {
            if (!errors.ContainsKey(column))
                errors[column] = errorText;
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("Holiday.aspx");
        }
        protected void GrdHoliday_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            string strSql;
            DataSet Ds = new DataSet();
            try
            {
                string HolidayDate = Convert.ToString(e.Keys["Date"]);


                strSql = "select convert(char(10),hdate,121) HldDate ,convert(char(10),AdjustmentHoliday,121) AdjDate, * from holiday WHERE HDate= convert(datetime,'" + HolidayDate.ToString() + "',103) and CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"'  ";
                Ds = Con.FillDataSet(strSql);
                strSql = "Delete from Holiday where hdate =convert(datetime,'" + HolidayDate.ToString() + "',103) ";
                result = Con.execute_NonQuery(strSql);
                if (result > 0)
                {
                    // for calling vbfunction in case of holiday deletion
                    for (int i = 0; i < Ds.Tables[0].Rows.Count; i++)
                    {
                        string hDate, AdjDate, Tmp, CompCode, DeptCode;

                        hDate = Ds.Tables[0].Rows[i]["HldDate"].ToString().Trim();
                        AdjDate = Ds.Tables[0].Rows[i]["AdjDate"].ToString().Trim();
                        CompCode = Ds.Tables[0].Rows[i]["CompanyCode"].ToString().Trim();
                        DeptCode = Ds.Tables[0].Rows[i]["DepartmentCode"].ToString().Trim();
                        if (AdjDate == "")
                            AdjDate = hDate;
                        //strSql = "insert into tblFunctionCall (FromDate, ToDate, Companycode, DepartmentCode, FunctionName )  " +
                        //            " Values ('" + hDate + "', '" + AdjDate + "','" + CompCode + "','" + DeptCode + "', 'DeleteHolidays' ) ";
                        //cn.execute_NonQuery(strSql); 

                        string SetupID = "";
                        DataSet Rs = new DataSet();
                        string sSql = " Select setupid from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tblsetup)";
                        Rs = Con.FillDataSet(sSql);
                        if (Rs.Tables[0].Rows.Count > 0)
                        {
                            SetupID = Rs.Tables[0].Rows[0][0].ToString().Trim();
                        }
                        try
                        {
                            using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString()))
                            {
                                MyCmd = new OleDbCommand("ProcessDeleteHolidays", MyConn);
                                MyCmd.CommandTimeout = 1800;
                                MyCmd.CommandType = CommandType.StoredProcedure;
                                MyCmd.Parameters.Add("@MinDate", SqlDbType.VarChar).Value = hDate;
                                MyCmd.Parameters.Add("@MaxDate", SqlDbType.VarChar).Value = AdjDate;
                                MyCmd.Parameters.Add("@CompanyCode", SqlDbType.VarChar).Value = CompCode;
                                MyCmd.Parameters.Add("@DepartmentCode", SqlDbType.VarChar).Value = DeptCode;
                                MyCmd.Parameters.Add("@SetupId", SqlDbType.VarChar).Value = SetupID;
                                MyConn.Open();
                                result = MyCmd.ExecuteNonQuery();
                                MyConn.Close();

                            }
                        }
                        catch
                        {
                            
                            
                        }
                    }
                }

                GrdHoliday.CancelEdit();
                e.Cancel = true;
                BindData();
            }
            catch
            {

            }
        }
}