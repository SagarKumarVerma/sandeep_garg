﻿using System.Drawing.Printing;
using System.Management;
using System.Runtime.InteropServices;
using System;
using System.Data;
using System.Data.OleDb;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Globalization;
using System.Data.SqlClient;
using System.Web.Caching;
using System.DirectoryServices;
using System.DirectoryServices.ActiveDirectory;
using System.Text;
using System.IO;
using System.Collections;
using System.Web.Configuration;
using System.Management;
using System.Net.NetworkInformation;
public partial class Login : System.Web.UI.Page
{
    [DllImport("ADVAPI32.dll", EntryPoint = "LogonUserW", SetLastError = true, CharSet = CharSet.Auto)]
    public static extern bool LogonUser(string lpszUsername, string lpszDomain, string lpszPassword, int dwLogonType, int dwLogonProvider, ref IntPtr phToken);

    Class_Connection cn = new Class_Connection();
    ErrorClass ec = new ErrorClass();
    OleDbConnection MyConn = new OleDbConnection();
    Setting_App Settings = new Setting_App();
    OleDbCommand MyCmd = new OleDbCommand();
    OleDbDataReader myDr;
    string StrSql, strMsg, Tmp;
    string isLDAP = "";
    string change = "";
    string MACAddress = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        Label3.Visible = false;
        Session["PRINTER"] = "";
        Session["SERVER_PATH"] = Server.MapPath(".");
        //LinkButton LnkLogOut = this.Master.FindControl("LnkLogOut") as LinkButton;
        //if (LnkLogOut != null)
        //    LnkLogOut.Visible = false;



        getUsers();
        txtCCode.Focus();
        this.Title = "TimeWatch Login";
        if (!Page.IsPostBack)
        {
            Session["DifferentUser"] = "N";
          
        }
        if (ConfigurationManager.AppSettings["ForgetPwdLink"].ToString().Trim() == "Y")
        {
           
        }
        else
        {
            
        }
    }
    public static string GetDomainName(string usernameDomain)
    {
        if (string.IsNullOrEmpty(usernameDomain))
        {
            throw (new ArgumentException("Argument can't be null.", "usernameDomain"));
        }
        if (usernameDomain.Contains("\\"))
        {
            int index = usernameDomain.IndexOf("\\");
            return usernameDomain.Substring(0, index);
        }
        else if (usernameDomain.Contains("@"))
        {
            int index = usernameDomain.IndexOf("@");
            return usernameDomain.Substring(index + 1);
        }
        else
        {
            return "";
        }
    }

    protected void getUsers()
    {
        string strsql = "";
        DataSet dsAtt = new DataSet();
        string isRun = "";

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='GroupID' ";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add GroupID varchar(500)";
            cn.execute_NonQuery(strsql);

        }


        strsql = "select * from sysobjects where name='Att_request' ";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "CREATE TABLE Att_request(application_no bigint primary key identity(1,1),paycode varchar(10),Request_date datetime NULL,datetime NULL ,Request_for datetime NULL,UserRemarks varchar(100) ,stage1_approval char(1),stage1_approvalID varchar(10),stage1_ApprovalDate datetime NULL,stage1_Remarks1 varchar(100),stage2_approval char(1),stage2_approvalID varchar(10),stage2_ApprovalDate datetime NULL,stage2_Remarks1 varchar(100),RequestBy varchar(100))  ";
            cn.execute_NonQuery(strsql);
        }


        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='DeviceCommands' and col.name='Priority' ";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table DeviceCommands add Priority varchar(50)";
            cn.execute_NonQuery(strsql);
            
        }


        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='ClientDeviceGroup' and col.name='DeviceType' ";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table ClientDeviceGroup add DeviceType varchar(50)";
            cn.execute_NonQuery(strsql);

        }
        

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='LoginType' ";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add LoginType char(1)";
            cn.execute_NonQuery(strsql);
            strsql = "update tbluser set LoginType=case when user_r='ADMIN' then 'L' else 'N' end ";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblMachine' and col.name='IsAuthorized' ";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblMachine add IsAuthorized char(1)";
            cn.execute_NonQuery(strsql);
            
        }


        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='IsValid' ";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add IsValid char(1)";
            cn.execute_NonQuery(strsql);
            strsql = "update tbluser set IsValid='Y' ";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblEmployee' and col.name='headid_2' ";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblemployee add headid_2 varchar(10)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbl_fkdevice_status' and col.name='CompanyCode' ";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbl_fkdevice_status add CompanyCode varchar(5)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from tbluser where User_r='ADMIN'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "insert into tbluser(USER_R,USERDESCRIPRION,PASSWORD,UserPrevilege,paycode,Admin,Usertype,TimeOfficeSetup,LoginType,AutoProcess,DataProcess,Main,V_Transaction,Reports,Leave,Company,Department,Section,Grade,Category, " +
                    "Shift,Employee,Manual_Attendance,OstoOt,ShiftChange,HoliDay,LeaveMaster,LeaveApplication,LeaveAccural,LeaveAccuralAuto,Verification,InstallationSetup,EmployeeSetup,ArearEntry,TimeOfficeReport,RegisterCreation,RegisterUpdation,BackDateProcess,isValid,SSN,COMPANYCODE) " +
                    " values('ADMIN','ADMIN','ADMIN','Y','C001','Y','A','Y','L','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','C001','C001')";
            cn.execute_NonQuery(strsql);
        }
        else
        {
            strsql = "update tbluser set USERDESCRIPRION='ADMIN',LoginType='L' where ltrim(rtrim(upper(User_r)))='ADMIN'";
            cn.execute_NonQuery(strsql);

        }

        try
        {
            strsql = "update tbluser set Visitor='Y',ManualUpload='Y' where USERTYPE='A'";
            cn.execute_NonQuery(strsql);
        }
        catch
        {

        }




        strsql = "select tblemployee.paycode 'Paycode',tblemployee.empname 'Name',tblemployee.ssn 'ssn',tblemployee.CompanyCode 'CC' from tblemployee left join tbluser on tblemployee.paycode=tbluser.user_r where tbluser.user_r is null";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count > 0)
        {
            for (int i = 0; i < dsAtt.Tables[0].Rows.Count; i++)
            {
                strsql = "Insert into tbluser(User_r,USERDESCRIPRION,password,usertype,UserPrevilege,SSN,CompanyCode)values('" + dsAtt.Tables[0].Rows[i]["Paycode"].ToString().Trim() + "','" + dsAtt.Tables[0].Rows[i]["Name"].ToString().Trim() + "','" + dsAtt.Tables[0].Rows[i]["Paycode"].ToString().Trim() + "','U','N','" + dsAtt.Tables[0].Rows[i]["ssn"].ToString().Trim() + "','" + dsAtt.Tables[0].Rows[i]["CC"].ToString().Trim() + "')";
                try
                {
                    cn.execute_NonQuery(strsql);
                }
                catch
                {
                    continue;
                }
            }
        }

        isRun = ConfigurationSettings.AppSettings["RunScript"].ToString();
        if (isRun.ToString().Trim().ToUpper() == "N")
            return;

        StrSql = "alter table tbltimeregister alter column reason varchar(2000)";
        cn.execute_NonQuery(StrSql);

        strsql = "select * from sysobjects where xtype='U' and name='tblrosterCheck'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "CREATE TABLE[dbo].[tblrosterCheck] ([Paycode][char](10) NULL,	[Month] [varchar] (5) NULL,	[year] [char](10) NULL) ON[PRIMARY]";

            cn.execute_NonQuery(strsql);
        }


        strsql = "select * from sysobjects where xtype='U' and name='leave_request'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "CREATE TABLE leave_request( " +
                    "application_no numeric(18, 0) NOT NULL,paycode char(10),request_id char(20),request_date datetime NULL, " +
                    "leave_from datetime NULL,leave_to datetime NULL,leavecode char(3) ,halfday char(1) ,leavedays float NULL, " +
                    "userremarks varchar(100),Stage1_approved char(1),Stage1_approval_id char(20),Stage1_approval_date datetime NULL, " +
                    "Stage1_approval_Remarks varchar(2000),Stage2_approved char(10) ,Stage2_approval_id char(10),Stage2_approval_date datetime NULL, " +
                    "Stage2_Approval_Remarks varchar(2000),hod_remarks varchar(30),chklbl1 char(1),voucherno char(10),Stage3_approved char(1), " +
                    "Stage3_approval_id char(10),Stage3_approval_date datetime NULL,Stage3_approval_Remarks varchar(2000)) ";
            cn.execute_NonQuery(strsql);
        }



        StrSql = "alter table leave_request alter column request_id varchar(100)";
        cn.execute_NonQuery(StrSql);

        StrSql = "alter table leave_request alter column userremarks varchar(200)";
        cn.execute_NonQuery(StrSql);

        StrSql = "select * from syscolumns col join sysobjects obj on obj.id=col.id where obj.name='tblEmployee' and col.name='rockwellid'";
        DataSet dsCount = new DataSet();
        dsCount = cn.FillDataSet(StrSql);
        if (dsCount.Tables[0].Rows.Count > 0)
        {
            StrSql = "alter table tblemployee alter column rockwellid char(20)";
            cn.execute_NonQuery(StrSql);
        }
        StrSql = "select * from syscolumns col join sysobjects obj on obj.id=col.id where obj.name='tbltimeregister' and col.name='AbsentMail'";
        dsCount = cn.FillDataSet(StrSql);
        if (dsCount.Tables[0].Rows.Count > 0)
        {
            StrSql = "alter table tbltimeregister drop column absentmail";
            cn.execute_NonQuery(StrSql);
        }


        string Permissablelate, permisearly, maxdaymin, time, half, shortday, paycode;
        string AuthShifts = "", HALFDAYSHIFT = "";
        string Shift = "";
        string SecondOffDay = "", weekdays = "";
        string Cardno, istimelossallowed, isot, isautoshift, isoutwork, isos, ishalfday, isshort;
        string ShiftType = "", ISRoundClock = "", Inonly = "", IsPunchAll = "", Two = "", FirstOffDay = "", SecondOffType = "";
        DataSet ds = new DataSet();
        Permissablelate = "";
        permisearly = "";
        maxdaymin = "";
        time = "";
        half = "";
        shortday = "";
        StrSql = "select PermisLateArr,PermisEarlyDep,PREWO,Time1,MaxWrkDuration,Time,Half,Short from tblsetup " +
                   "where setupid in (select max(convert(int,setupid)) from tblsetup)";
        ds = cn.FillDataSet(StrSql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            Permissablelate = ds.Tables[0].Rows[0]["PermisLateArr"].ToString().Trim();
            permisearly = ds.Tables[0].Rows[0]["PermisEarlyDep"].ToString().Trim();
            maxdaymin = ds.Tables[0].Rows[0]["MaxWrkDuration"].ToString().Trim();
            time = ds.Tables[0].Rows[0]["Time"].ToString().Trim();
            half = ds.Tables[0].Rows[0]["Half"].ToString().Trim();
            shortday = ds.Tables[0].Rows[0]["Short"].ToString().Trim();
        }
        Shift = "G";
        ShiftType = "F";
        ISRoundClock = "Y";
        Inonly = "N";
        IsPunchAll = "Y";
        Two = "N";
        FirstOffDay = "SUN";
        istimelossallowed = "Y";
        isot = "N";
        isautoshift = "Y";
        isoutwork = "Y";
        isos = "Y";
        ishalfday = "Y";
        isshort = "N";
        HALFDAYSHIFT = " ";
        SecondOffDay = "SAT";
        weekdays = "12345";
        SecondOffType = "F";
        AuthShifts = "";
        DateTime doj = System.DateTime.MinValue;
        //strsql = "select tblemployee.paycode,tblemployee.PresentcardNo,tblemployeeshiftmaster.paycode,convert(varchar(10),isnull(tblemployee.dateofjoin,getdate()),126) 'doj' from tblemployee tblemployee  left join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode where tblemployeeshiftmaster.paycode is null";
        //dsAtt = cn.FillDataSet(strsql);
        //if (dsAtt.Tables[0].Rows.Count > 0)
        //{
        //    for (int i = 0; i < dsAtt.Tables[0].Rows.Count; i++)
        //    {
        //        paycode = dsAtt.Tables[0].Rows[i][0].ToString().Trim();
        //        Cardno = dsAtt.Tables[0].Rows[i][1].ToString().Trim();
        //        doj = DateTime.ParseExact(dsAtt.Tables[0].Rows[i][3].ToString().Trim(), "yyyy-MM-dd", CultureInfo.InvariantCulture);
        //        strsql = "insert into tblemployeeshiftmaster(Paycode,cardno,shift,shifttype,isroundtheclockwork,inonly,ispunchall,two,firstoffday,secondoffday, secondofftype,istimelossallowed , isot ,PERMISLATEARRIVAL, PERMISEARLYDEPRT, isautoshift, isoutwork, maxdaymin,isos,time,short,half,ishalfday,isshort,ALTERNATE_OFF_DAYS,shiftRemainDays,CDays,otrate,auth_shifts,HALFDAYSHIFT) values ('" + paycode.ToString() + "','" + Cardno.ToString() + "', " +
        //                 " '" + Shift.ToString() + "','" + ShiftType.ToString() + "','" + ISRoundClock.ToString() + "','" + Inonly.ToString() + "', " +
        //                 " '" + IsPunchAll.ToString() + "','" + Two.ToString() + "','" + FirstOffDay.ToString() + "','" + SecondOffDay.ToString() + "'," +
        //                 " '" + SecondOffType.ToString() + "','" + istimelossallowed.ToString() + "','" + isot.ToString() + "'," + Permissablelate.ToString() + "," + permisearly.ToString() + ", " +
        //                 " '" + isautoshift.ToString() + "','" + isoutwork.ToString() + "'," + maxdaymin.ToString() + ",'" + isos.ToString() + "'," + time.ToString() + "," + shortday.ToString() + "," + half.ToString() + ", " +
        //                 " '" + ishalfday.ToString() + "','" + isshort.ToString() + "','" + weekdays.ToString() + "',0,0,0,'" + AuthShifts.ToString() + "','" + HALFDAYSHIFT.ToString() + "')";
        //        try
        //        {
        //            cn.execute_NonQuery(strsql);
        //            StrSql = "insert into tblFunctionCall(paycode,fromDate,FunctionName) values('" + paycode.ToString().Trim() + "','" + doj.ToString("yyyy-MM-dd") + "','CreateDutyRoster')";
        //            cn.execute_NonQuery(StrSql);
        //        }
        //        catch
        //        {
        //            continue;
        //        }
        //    }
        //}



        strsql = "select * from sysobjects where type='P' and name='checkpassword' ";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "create proc checkpassword " +
                        "@user varchar(1000), " +
                        "@password varchar(100), " +
                        "@newpassword varchar(100) " +
                        "as " +
                        "if exists(select * from tbluser where user_r=@user and password=@password) " +
                        "begin " +
                        "update tbluser set password=@newpassword where user_r=@user and password=@password " +
                        "end";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='TblFunctionCall' ";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "CREATE TABLE [dbo].[TblFunctionCall]( " +
                    "[paycode] [char](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " +
                    "[FromDate] [datetime] NULL, " +
                    "[ToDate] [datetime] NULL, " +
                    "[FunctionName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " +
                    "[CompanyCode] [char](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " +
                    "[DepartmentCode] [char](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " +
                    "[UsbFileName] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL, " +
                    "[CaptureData] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL DEFAULT ('N'), " +
                    "[Called] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL DEFAULT ('N'), " +
                    "[ID] [bigint] IDENTITY(1,1) NOT NULL, " +
                    "PRIMARY KEY CLUSTERED  " +
                    "( [ID] ASC)WITH (PAD_INDEX  = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]) ON [PRIMARY] ";

            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects where name='tblMsg' ";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "Create table tblMsg(id bigint primary key identity(1,1), Message char(100))";
            cn.execute_NonQuery(strsql);

            cn.execute_NonQuery("insert into tblMsg(Message)values('OnLine Time Attendance Management')");
            cn.execute_NonQuery("insert into tblMsg(Message)values('OnLine Leave Management')");
            cn.execute_NonQuery("insert into tblMsg(Message)values('OnLine Report Generation')");
            cn.execute_NonQuery("insert into tblMsg(Message)values(' ')");
            cn.execute_NonQuery("insert into tblMsg(Message)values(' ')");
            cn.execute_NonQuery("insert into tblMsg(Message)values(' ')");
        }

        strsql = "select * from sysobjects where name='Att_request' ";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "CREATE TABLE Att_request(application_no bigint primary key identity(1,1),paycode varchar(10),Request_date datetime NULL,InTime varchar(10) ,Request_for datetime NULL,UserRemarks varchar(100) ,stage1_approval char(1),stage1_approvalID varchar(10),stage1_ApprovalDate datetime NULL,stage1_Remarks1 varchar(100),stage2_approval char(1),stage2_approvalID varchar(10),stage2_ApprovalDate datetime NULL,stage2_Remarks1 varchar(100))  ";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblmachine' and col.name='commkey'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblmachine add commkey bigint";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblemployee' and col.name='logid'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblemployee add logid char(10)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects where name='tblEmployeeTemp' ";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "select * into tblEmployeeTemp from tblemployee where 1=0";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects where name='tblLog' ";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "CREATE TABLE tblLog(LogId int NULL,UserId char(10),LogDate datetime NULL,MTable varchar(30),Task varchar(200))";
            cn.execute_NonQuery(strsql);
        }
        strsql = "select * from sysobjects where name='tblLog_ManualPunch' ";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "CREATE TABLE tblLog_ManualPunch(LogId bigint identity(1,1),Paycode char(10),PunchDate datetime NULL,LoggedUser varchar(10),LoggedDate datetime)";
            cn.execute_NonQuery(strsql);
        }




        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='leave_request' and col.name='LWPBal'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table leave_request add LWPBal float";
            cn.execute_NonQuery(strsql);
        }
        StrSql = "select * from syscolumns col join sysobjects obj on obj.id=col.id where obj.name='tblEmployee' and col.name='rockwellid'";
        dsCount = cn.FillDataSet(StrSql);
        if (dsCount.Tables[0].Rows.Count == 0)
        {
            StrSql = "alter table tblemployee add rockwellid char(20)";
            cn.execute_NonQuery(StrSql);
        }
        else
        {
            StrSql = "alter table tblemployee alter column rockwellid char(20)";
            cn.execute_NonQuery(StrSql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblUser' and col.name='Approver_Visible'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add Approver_Visible char(1) default('N')";
            cn.execute_NonQuery(strsql);

            strsql = "update tbluser set Approver_Visible ='N' ";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblUser' and col.name='RosterView'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add RosterView char(1) default('N')";
            cn.execute_NonQuery(strsql);

            strsql = "update tbluser set RosterView ='N' ";
            cn.execute_NonQuery(strsql);
        }



        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='trainer'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add trainer char(1)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='Attrition'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add Attrition char(1)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='OTApproval'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add OTApproval char(1)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='IsOA'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add IsOA char(1)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='CompAdd'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add CompAdd char(1)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='CompModi'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add CompModi char(1)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='CompDel'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add CompDel char(1)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='DeptAdd'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add DeptAdd char(1)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='DeptModi'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add DeptModi char(1)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='DeptDel'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add DeptDel char(1)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='CatAdd'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add CatAdd char(1)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='CatModi'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add CatModi char(1)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='CatDel'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add CatDel char(1)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='SecAdd'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add SecAdd char(1)";
            cn.execute_NonQuery(strsql);
        }
        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='SecModi'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add SecModi char(1)";
            cn.execute_NonQuery(strsql);
        }
        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='SecDel'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add SecDel char(1)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='GrdAdd'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add GrdAdd char(1)";
            cn.execute_NonQuery(strsql);
        }
        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='GrdModi'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add GrdModi char(1)";
            cn.execute_NonQuery(strsql);
        }
        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='GrdDel'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add GrdDel char(1)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='SftAdd'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add SftAdd char(1)";
            cn.execute_NonQuery(strsql);
        }
        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='SftModi'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add SftModi char(1)";
            cn.execute_NonQuery(strsql);
        }
        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='SftDel'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add SftDel char(1)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='EmpAdd'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add EmpAdd char(1)";
            cn.execute_NonQuery(strsql);
        }
        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='EmpModi'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add EmpModi char(1)";
            cn.execute_NonQuery(strsql);
        }
        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='EmpDel'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add EmpDel char(1)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='DataMaintenance'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add DataMaintenance char(1)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='LeaveApproval'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add LeaveApproval char(1)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='IsAutoLogin'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbluser add IsAutoLogin char(1)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbltimeregister' and col.name='ApprovedOT'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbltimeregister add ApprovedOT float";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbltimeregister' and col.name='WBR_Flag'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbltimeregister  add WBR_Flag char(1)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbltimeregister' and col.name='Stage1OTApprovedBy'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbltimeregister add Stage1OTApprovedBy char(10)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbltimeregister' and col.name='Stage2OTApprovedBy'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbltimeregister add Stage2OTApprovedBy char(10)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbltimeregister' and col.name='Stage1ApprovalDate'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbltimeregister add Stage1ApprovalDate datetime";
            cn.execute_NonQuery(strsql);
        }
        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbltimeregister' and col.name='Stage2ApprovalDate'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tbltimeregister add Stage2ApprovalDate datetime";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblmachine' and col.name='isfp'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblmachine add isfp char(1)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblmachine' and col.name='MACHINELOCATION'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblmachine add MACHINELOCATION char(50)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblemployee' and col.name='headid'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblemployee add headid varchar(10)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblemployee' and col.name='Reporting1'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblemployee add Reporting1 varchar(10)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblemployee' and col.name='Reporting2'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblemployee add Reporting2 varchar(10)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblemployee' and col.name='DESPANSARYCODE'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblemployee add DESPANSARYCODE char(3)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblemployee' and col.name='IsFixedShift'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblemployee add IsFixedShift char(1)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblemployee' and col.name='LeaveApprovalstages'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblemployee add LeaveApprovalstages nvarchar(2)";
            cn.execute_NonQuery(strsql);

            strsql = "update tblemployee set LeaveApprovalstages=1";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblemployee' and col.name='involuntary'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblemployee add involuntary nvarchar(2)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblemployee' and col.name='EmpBioData'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblemployee add EmpBioData varchar(200)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblemployee' and col.name='dateofexp'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblemployee add dateofexp datetime";
            cn.execute_NonQuery(strsql);
        }
        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblemployee' and col.name='Disable'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblemployee add Disable char(1)";
            cn.execute_NonQuery(strsql);
        }
        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblemployee' and col.name='Group_Index'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblemployee add Group_Index char(1)";
            cn.execute_NonQuery(strsql);
        }
        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblemployee' and col.name='OnRoll'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblemployee add OnRoll char(1)";
            cn.execute_NonQuery(strsql);
        }
        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblemployee' and col.name='teamid'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblemployee add teamid char(5)";
            cn.execute_NonQuery(strsql);
        }
        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblemployee' and col.name='PSCODE'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblemployee add PSCODE char(5) ";
            cn.execute_NonQuery(strsql);
        }
        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblemployee' and col.name='SUBAREACODE'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblemployee add SUBAREACODE char(10)";
            cn.execute_NonQuery(strsql);
        }
        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblemployee' and col.name='SECTIONID'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblemployee add SECTIONID char(10)";
            cn.execute_NonQuery(strsql);
        }
        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblemployee' and col.name='POSITIONCODE'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblemployee add POSITIONCODE char(10)";
            cn.execute_NonQuery(strsql);
        }
        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblemployee' and col.name='POSITIONCODE'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblemployee add POSITIONCODE char(10)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblemployee' and col.name='trainer'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblemployee add trainer char(1)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblemployee' and col.name='trainee'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblemployee add trainee char(1)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblemployee' and col.name='TraineeLevel'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblemployee add TraineeLevel char(1)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='leave_request' and col.name='Stage2_approved'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table leave_request add Stage2_approved char(10)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='leave_request' and col.name='Stage2_approval_id'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table leave_request add Stage2_approval_id char(20)";
            cn.execute_NonQuery(strsql);
        }
        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='leave_request' and col.name='Stage2_approval_date'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table leave_request add Stage2_approval_date datetime";
            cn.execute_NonQuery(strsql);
        }
        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='leave_request' and col.name='Stage2_Approval_Remarks'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table leave_request add Stage2_Approval_Remarks varchar(2000)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='leave_request' and col.name='Stage3_approved'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table leave_request add Stage3_approved char(10)";
            cn.execute_NonQuery(strsql);
        }
        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='leave_request' and col.name='Stage3_approval_id'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table leave_request add Stage3_approval_id char(20)";
            cn.execute_NonQuery(strsql);
        }
        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='leave_request' and col.name='Stage3_approval_date'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table leave_request add Stage3_approval_date datetime";
            cn.execute_NonQuery(strsql);
        }
        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='leave_request' and col.name='Stage3_Approval_Remarks'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table leave_request add Stage3_Approval_Remarks varchar(2000)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='leave_request' and col.name='VoucherNo'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table leave_request add VoucherNo char(10)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tblleavemaster' and col.name='showonweb'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "alter table tblleavemaster add Showonweb char(1)";
            cn.execute_NonQuery(strsql);

            strsql = "update tblleavemaster set showonweb='Y' ";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects where xtype='U' and name='TblEMPHOLIDAY'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "CREATE TABLE [dbo].[TblEMPHOLIDAY]([paycode] [char](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,	[hdate] [smalldatetime] NULL) ";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from sysobjects where xtype='U' and name='TblTeam'";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "CREATE TABLE [dbo].[TblTeam](	[TeamId] [char](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,	[DepartmentCode] [char](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,	[TeamName] [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,	[TeamLeader] [char](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL)";
            cn.execute_NonQuery(strsql);
        }

        strsql = "select * from tblshiftmaster";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "insert into tblshiftmaster(SHIFT,STARTTIME,ENDTIME,LUNCHTIME,LUNCHDURATION,LUNCHENDTIME,ORDERINF12,OTSTARTAFTER,OTDEDUCTHRS,LUNCHDEDUCTION,SHIFTPOSITION,SHIFTDURATION,OTDEDUCTAFTER,CompanyCode)values('GEN','1899-12-30 09:00:00.000','1899-12-30 18:00:00.000','1899-12-30 00:00:00.000',0,'1899-12-30 00:00:00.000',NULL,0,	0,	0,	'DAY',540,	0,'C001')";
            cn.execute_NonQuery(strsql);
        }
        strsql = "select * from tblsetup";
        dsAtt = cn.FillDataSet(strsql);
        if (dsAtt.Tables[0].Rows.Count == 0)
        {
            strsql = "Insert into tblSetup(SETUPID,PERMISLATEARR,PERMISEARLYDEP,DUPLICATECHECKMIN,ISOVERSTAY,S_END,S_OUT,ISOTOUTMINUSSHIFTENDTIME,ISOTWRKGHRSMINUSSHIFTHRS,ISOTEARLYCOMEPLUSLATEDEP,ISOTALL,ISOTEARLYCOMING,OTEARLYDUR,OTLATECOMINGDUR,OTRESTRICTENDDUR,OTEARLYDEPARTUREDUR,DEDUCTWOOT,DEDUCTHOLIDAYOT,ISPRESENTONWOPRESENT,ISPRESENTONHLDPRESENT,MAXWRKDURATION,TIME1,CHECKLATE,CHECKEARLY,TIME,HALF,SHORT,ISAUTOABSENT,AUTOSHIFT_LOW,AUTOSHIFT_UP,ISAUTOSHIFT,ISHALFDAY,ISSHORT,TWO,WOINCLUDE,IsOutWork,NightShiftFourPunch,LinesPerPage,SkipAfterDepartment,meals_rate,INOUT,SMART,PREWO,ISAWA,ISPREWO,CompanyCode) values('1',10,10,5,'N','Mar 21 2000  5:00','Mar 21 2000  5:00','N','Y','N','N','N',0,0,0,0,0,0,'N','N',    1440,1020,240,240,240,300,120,'N',240,240,'N','0','0','N','N','N','N',58,'Y',15.5,'N','N',3,'N','N','C001')";
            cn.execute_NonQuery(strsql);
        }
    }
    public static string GetUsername(string usernameDomain)
    {
        if (string.IsNullOrEmpty(usernameDomain))
        {
            throw (new ArgumentException("Argument can't be null.", "usernameDomain"));
        }
        if (usernameDomain.Contains("\\"))
        {
            int index = usernameDomain.IndexOf("\\");
            return usernameDomain.Substring(index + 1);
        }
        else if (usernameDomain.Contains("@"))
        {
            int index = usernameDomain.IndexOf("@");
            return usernameDomain.Substring(0, index);
        }
        else
        {
            return usernameDomain;
        }
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {

        ViewState["user"] = tbUserName.Text;             //  username
        ViewState["password"] = tbPassword.Text;     //  password     
        LoginFun();
       
       // Response.Redirect("frmCompany.aspx");
    }
  
    private void SetAuthCompanyDepartment()
    {
        //setting auth.dept 
        OleDbDataReader AuthDr;
        OleDbCommand cmd;
        string qry1, qry2, sss;
        string AuthComp = "", AuthDept = "";
        Session["Auth_Dept"] = "";
        Session["Auth_Comp"] = "";
       // if (Session["PAYCODE"].ToString().ToUpper().Trim() == "ADMIN" && Session["usertype"].ToString().ToUpper().Trim() == "A")
        DataFilter.AuthDept = "";
        DataFilter.AuthComp = "";

        if (Session["LoginCompany"].ToString().ToUpper().Trim() == "C001")
        {
            qry1 = "Select DepartmentCode from TblDepartment  order by DepartmentCode ";
            qry2 = "Select CompanyCode from Tblcompany order by CompanyCode";
        }
        else if (Session["usertype"].ToString().ToUpper().Trim() == "A")
        {
            qry1 = "Select DepartmentCode from TblDepartment where  CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "' order by DepartmentCode ";
            qry2 = "Select CompanyCode from Tblcompany where   CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "' order by CompanyCode";
        }
        else
        {
            qry1 = "Select Auth_Dept from TblUser where User_R='" + Session["PAYCODE"].ToString() + "' and  CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "' ";
            qry2 = "Select Auth_Comp from TblUser where User_R='" + Session["PAYCODE"].ToString() + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "' ";
        }
        AuthDr = cn.Execute_Reader(qry1);
        while (AuthDr.Read())
        {
            AuthDept += AuthDr[0].ToString();
            AuthDept = AuthDept.Trim() + ",";
        }
        if (AuthDept.Trim() != "")
        {
            AuthDept = AuthDept.Substring(0, AuthDept.Length - 1);
        }
        AuthDr.Close();

        string[] SplitDept = AuthDept.ToString().Trim().Split(',');
        DataFilter.AuthDept = "";
        AuthDept = "";
        foreach (string sd in SplitDept)
            AuthDept += "'" + sd.Trim() + "',";
        AuthDept = AuthDept.TrimEnd(',');
        Session["Auth_Dept"] = AuthDept;// AuthDept.Trim().Substring(0, AuthDept.Trim().Length - 1);
        sss = Session["Auth_Dept"].ToString();
        DataFilter.AuthDept = Session["Auth_Dept"].ToString();
        // Finding Auth Company
        AuthDr = cn.Execute_Reader(qry2);

        while (AuthDr.Read())
        {
            AuthComp += AuthDr[0].ToString();
            AuthComp = AuthComp.Trim() + ",";
        }
        if (AuthComp.Trim() != "")
            AuthComp = AuthComp.Substring(0, AuthComp.Length - 1);
        AuthDr.Close();

        string[] SplitComp = AuthComp.ToString().Trim().Split(',');
        AuthComp = "";
        foreach (string sc in SplitComp)
            AuthComp += "'" + sc.Trim() + "',";

        AuthComp = AuthComp.TrimEnd(',');
        Session["Auth_Comp"] = AuthComp;//AuthComp.Trim().Substring(0, AuthComp.Trim().Length - 1);
        sss = Session["Auth_Dept"].ToString();
        DataFilter.AuthComp = Session["Auth_Comp"].ToString();
    }
    private void LoginFun()
    {
        string username = "";
        string password = "";
        string CCode = "";
        string change = "";
        int result1 = 0;
        try
        {

            try
            {
                //DataFilter.LoginCompany = "";
                //DataFilter.LoginUserName = "";
                //DataFilter.LoginUserType = "";
                //DataFilter.LoginPayCode = "";
                DataFilter.ClientID = 0;


                Session["LoginCompany"] = "";
                Session["LoginUserName"] = "";
                Session["LoginUserType"] = "";
                Session["LoginPayCode"] = "";
            }
            catch
            {

            }



            username = tbUserName.Text.ToString().Trim();
            password = tbPassword.Text.ToString().Trim();
            CCode = txtCCode.Text.ToString().Trim();
            bool res;
            res = cn.chkvalid(username);
            if (!res)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('You have enter some invalid inputs.')", true);
                return;
            }
            res = cn.chkvalid(password);
            if (!res)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('You have enter some invalid inputs.')", true);
                return;
            }
            res = cn.chkvalid(CCode);
            if (!res)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('You have enter some invalid inputs.')", true);
                return;
            }
            if (Settings.isDemo.ToString().Trim().ToUpper() == "Y")
            {
                DateTime currentdate = System.DateTime.Now;
                string cDate = Settings.ExpireDate.ToString();
                DateTime dt = DateTime.ParseExact(cDate.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                if (dt > currentdate)
                {
                    StrSql = "update tbluser set IsValid='Y' where user_r='Admin'";
                    cn.execute_NonQuery(StrSql);
                }
                result1 = checkValidDate();
                if (result1 == 1)
                {
                    //set UnCommented the below line for Demo.Change ExpireDate at homepage.aspx VadlidCheck() function.
                    Session.Abandon();
                    Response.Redirect("error.htm");
                    return;
                }
            }
            else
            {
                StrSql = "update tbluser set IsValid='Y' where user_r='"+CCode+"'";
                cn.execute_NonQuery(StrSql);
            }
            StrSql = "select convert(char(10), getdate(),103)";
            myDr = cn.ExecuteReader(StrSql);
            myDr.Read();
            Session["Today"] = DateTime.ParseExact(myDr[0].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            string its = Session["Today"].ToString();
            myDr.Close();
            try
            {
                if (tbUserName.Text.Trim().ToUpper() == "ADMIN")
                    AddTables();
            }
            catch (Exception ex)
            {
            }

            OleDbDataReader dr;
            OleDbDataReader dr1;
            Session["G_Reader"] = "TW";

            username = tbUserName.Text.ToString().Trim();
            password = tbPassword.Text.ToString().Trim();

            DataSet dsEmp = new DataSet();

            StrSql = "select isnull(LoginType,'N') from TblUser Where User_R='" + tbUserName.Text.Trim() + "' and rtrim(ltrim(password))='" + tbPassword.Text.Trim() + "' ";
            dsEmp = cn.FillDataSet(StrSql);
            if (dsEmp.Tables[0].Rows.Count > 0)
            {
                if (tbUserName.Text.Trim().ToUpper()!="ADMIN")
                {
                    StrSql = "select * from TblUser";
                    if (dsEmp.Tables[0].Rows[0][0].ToString().Trim() == "N")
                        StrSql += " , TblEmployee where TblUser.user_r = TblEmployee.paycode And TblEmployee.active ='Y' And ";
                    else
                        StrSql += " Where ";
                    StrSql += " User_R='" + tbUserName.Text.Trim() + "' and rtrim(ltrim(password))='" + tbPassword.Text.Trim() + "' and TblUser.CompanyCode='" + txtCCode.Text.ToString().Trim() + "' ";

                }
                else
                {
                    StrSql = "select * from TblUser";
                    StrSql += " Where ";
                    StrSql += " User_R='" + tbUserName.Text.Trim() + "' and rtrim(ltrim(password))='" + tbPassword.Text.Trim() + "' and TblUser.CompanyCode='C001' ";
                }
               
            }
            else
            {
                Label3.Visible = true;
                Label3.Text = "Invalid Username or Password.";
                //string Msg = "<script language='javascript'>alert('Invalid Username or Password.')</script>";
                //Page.RegisterStartupScript("Msg", Msg);
                //tbUserName.Text = "";
                tbPassword.Text = "";
                txtCCode.Focus();
                return;
            }


            DataSet UserDs = new DataSet();
            UserDs = cn.FillDataSet(StrSql);
            if (UserDs.Tables[0].Rows.Count > 0)
            {
                FormsAuthentication.SetAuthCookie(tbUserName.Text, false);
                Session["LoggedUser"] = tbUserName.Text.Trim().ToUpper();
                if (dsEmp.Tables[0].Rows[0][0].ToString().Trim() == "L")
                {
                    Session["UserName"] = UserDs.Tables[0].Rows[0]["userdescriprion"].ToString().Trim();
                    Session["usertype"] = UserDs.Tables[0].Rows[0]["usertype"].ToString().Trim();
                }
                else
                {
                    Session["UserName"] = UserDs.Tables[0].Rows[0]["EmpName"].ToString().Trim();
                    Session["usertype"] = UserDs.Tables[0].Rows[0]["usertype"].ToString().Trim();
                }
                
                Session["PAYCODE"] = UserDs.Tables[0].Rows[0]["User_R"].ToString().Trim().ToUpper();
                Session["Trainer"] = UserDs.Tables[0].Rows[0]["Trainer"].ToString().Trim();
                Session["Attrition"] = UserDs.Tables[0].Rows[0]["Attrition"].ToString().Trim();
                Session["OTApproval"] = UserDs.Tables[0].Rows[0]["OTApproval"].ToString();
                Session["IsOA"] = UserDs.Tables[0].Rows[0]["IsOA"].ToString();
                Session["ActualUserType"] = UserDs.Tables[0].Rows[0]["usertype"].ToString();
                Session["RosterUpload"] = UserDs.Tables[0].Rows[0]["RosterView"].ToString();
                Session["UserDs"] = UserDs;
                Session["PaycodePassed"] = null;
                Session["TVSTATUS"] = "";
                Session["TVSTATUS1"] = "";
                Session["VALIDYEARS"] = "";
                Session["SERVER_DATE"] = cn.ServerDate();
                Session["Pay_Duplicate"] = "N";
                Session["LeaveApplicationVisible"] = "Y";
                Session["Dept_Selection"] = null;
                Session["Com_Selection"] = null;
                try
                {
                    Session["SSN"] = UserDs.Tables[0].Rows[0]["SSN"].ToString().Trim(); 
                }
                catch
                {
                    Session["SSN"] = null;
                }


                //DataFilter.LoginCompany = UserDs.Tables[0].Rows[0]["CompanyCode"].ToString();
                //DataFilter.LoginUserName = Session["LoggedUser"].ToString().Trim();
                //DataFilter.LoginUserType = Session["usertype"].ToString().Trim();
                //DataFilter.LoginPayCode = Session["PAYCODE"].ToString().Trim();
                Session["LoginCompany"] = UserDs.Tables[0].Rows[0]["CompanyCode"].ToString();
                Session["CompanyCode"] = UserDs.Tables[0].Rows[0]["CompanyCode"].ToString();
                Session["LoginUserName"] = Session["LoggedUser"].ToString().Trim();
                Session["LoginUserType"] = Session["usertype"].ToString().Trim();
                Session["LoginPayCode"] = Session["PAYCODE"].ToString().Trim();


                SetAuthCompanyDepartment();
                setValidYears();
                ValidYears();
                GetClientID(UserDs.Tables[0].Rows[0]["CompanyCode"].ToString().Trim());
                //LeaveField();
                Session["V"] = true;
               
                DataFilter.GetClientID(Session["LoginUserName"].ToString().Trim(),Session["LoginCompany"].ToString().Trim());
                DataFilter.LoadCompany(Session["LoginUserName"].ToString().Trim());
                DataFilter.LoadDepartment(Session["LoginUserName"].ToString().Trim(), Session["LoginCompany"].ToString().Trim());
                DataFilter.LoadGgrade(Session["LoginCompany"].ToString().Trim());
                DataFilter.LoadCat(Session["LoginCompany"].ToString().Trim());
                DataFilter.LoadDivision(Session["LoginCompany"].ToString().Trim());
                DataFilter.Loademployee(Session["LoginCompany"].ToString().Trim());
                DataFilter.Loadshift(Session["LoginCompany"].ToString().Trim());
                if (Session["usertype"].ToString() == "A")
                {
                    Response.Redirect("Home.aspx");
                }
                else
                {
                    Response.Redirect("frmMain.aspx");
                }
                //Response.Redirect("Test.aspx");                
            }
            else
            {
                //tbPassword.Text = "";
                //change = "<script language='JavaScript'>alert('Invalid Username / Password.')</script>";
                //Page.RegisterStartupScript("frmchange", change);
                Label3.Visible = true;
                Label3.Text = "Invalid Username or Password.";
                //string Msg = "<script language='javascript'>alert('Invalid Username or Password.')</script>";
                //Page.RegisterStartupScript("Msg", Msg);
                //tbUserName.Text = "";
                tbPassword.Text = "";
                txtCCode.Focus();
                return;
            }

        }
        catch
        {
            //change = "<script language='JavaScript'>alert('Invalid Username / Password.')</script>";
            //Page.RegisterStartupScript("frmchange", change);
            Label3.Visible = true;
            Label3.Text = "Invalid Username or Password.";
            //string Msg = "<script language='javascript'>alert('Invalid Username or Password.')</script>";
            //Page.RegisterStartupScript("Msg", Msg);
            //tbUserName.Text = "";
            tbPassword.Text = "";
            txtCCode.Focus();
            return;

        }
    }
    private void GetClientID(string CompCode)
    {
        string ClientID = "";
        string sSql = "select ID,CompanyCode from tblcompany where companycode='" + CompCode.Trim() + "'";
        DataSet DsC = new DataSet();
        DsC = cn.FillDataSet(sSql);
        if (DsC.Tables[0].Rows.Count > 0)
        {
            Session["ClientID"] = DsC.Tables[0].Rows[0]["ID"].ToString().Trim();
            Session["CompanyCode"] = DsC.Tables[0].Rows[0]["CompanyCode"].ToString().Trim();
            DataFilter.LoginClientID = Convert.ToInt32( DsC.Tables[0].Rows[0]["ID"].ToString().Trim());

        }




    }
    private void ValidYears()
    {
        StrSql = "Select Distinct yr from Tblyears order by yr ";
        //StrSql = "select distinct(lyear) from tblleaveledger";
        myDr = cn.Execute_Reader(StrSql);
        Tmp = "";
        while (myDr.Read())
        {
            Tmp = Tmp + "'" + myDr[0].ToString().Trim() + "',";
        }
        myDr.Close();
        if (Tmp.Length > 0)
        {
            Tmp = Tmp.Remove(Tmp.Length - 1, 1);
        }
        else
        {
            Tmp = System.DateTime.Now.Year.ToString();
        }
        Session["VALIDYEARS"] = Tmp;
    }
    private void GetDefaultPrinter()
    {
        //string strQuery = "SELECT * FROM Win32_Printer";
        //ObjectQuery objectQuery = new ObjectQuery(strQuery);
        //ManagementObjectSearcher query = new ManagementObjectSearcher(objectQuery);
        //ManagementObjectCollection queryCollection = query.Get();
        //string g;
        //foreach (ManagementObject managementObject in queryCollection)
        //{
        //    PropertyDataCollection propertyDataCollection = managementObject.Properties;
        //    if ((bool)managementObject["Default"]) // DEFAULT PRINTER                
        //    {
        //        g = managementObject["Name"].ToString();
        //        g = managementObject["Name"].ToString();
        //        Session["PRINTER"] = g;
        //        LblTemp.Text = g;
        //        //g = managementObject["Location"].ToString();                
        //    }
        //}
    }
    protected int checkValidDate()
    {
        int result = 0;
        DataSet dsEmp = new DataSet();
        StrSql = "select * from sysobjects obj join syscolumns col on obj.id=col.id and obj.name='tbluser' and col.name='IsValid' ";
        dsEmp = cn.FillDataSet(StrSql);
        if (dsEmp.Tables[0].Rows.Count == 0)
        {
            StrSql = "alter table tbluser add IsValid char(1)";
            cn.execute_NonQuery(StrSql);
            StrSql = "update tbluser set IsValid='Y' where user_r='Admin'";
            cn.execute_NonQuery(StrSql);
        }

        StrSql = "select IsValid from tbluser where user_r='Admin'";
        DataSet dsValid = new DataSet();
        dsValid = cn.FillDataSet(StrSql);
        string val = dsValid.Tables[0].Rows[0][0].ToString();
        if (val.ToString() == "N")
        {
            result = 1;
            return result;
        }
        else
        {
            result = 2;
            return result;
        }
    }
    protected void setValidYears()
    {
        DataSet dsyear = new DataSet();
        int year = System.DateTime.Now.Year;
        int nextyear = year + 1;
        StrSql = "select * from sysobjects where xtype='U' and name='tblyears'";
        dsyear = cn.FillDataSet(StrSql);
        if (dsyear.Tables[0].Rows.Count == 0)
        {
            StrSql = "create table tblyears(yr bigint)";
            cn.execute_NonQuery(StrSql);
            year = year - 1;
            StrSql = "insert into tblyears(yr) values (" + year.ToString() + ")";
            cn.execute_NonQuery(StrSql);
        }
        year = System.DateTime.Now.Year;
        StrSql = "select * from tblyears where yr=" + year.ToString() + " ";
        dsyear = cn.FillDataSet(StrSql);
        if (dsyear.Tables[0].Rows.Count == 0)
        {
            StrSql = "insert into tblyears(yr) values (" + year + ")";
            cn.execute_NonQuery(StrSql);
        }
        StrSql = "select * from tblyears where yr=" + nextyear.ToString() + " ";
        dsyear = cn.FillDataSet(StrSql);
        if (dsyear.Tables[0].Rows.Count == 0)
        {
            StrSql = "insert into tblyears(yr) values (" + nextyear + ")";
            cn.execute_NonQuery(StrSql);
        }
    }
    private void AddTables()
    {
        try
        {
            DataSet dsCount = new DataSet();
            StrSql = "select * from syscolumns col join sysobjects obj on obj.id=col.id where obj.name='leave_request' and col.name='LWPbal'";
            dsCount = cn.FillDataSet(StrSql);
            if (dsCount.Tables[0].Rows.Count == 0)
            {
                StrSql = "alter table leave_request add LWPbal bigint";
                cn.execute_NonQuery(StrSql);
            }

            StrSql = "select * from sysobjects where name='TblMapping' And Type='U' ";
            myDr = cn.Execute_Reader(StrSql);
            if (!myDr.Read())
            {
                CreateMapTable();
            }
            else
            {
                myDr.Close();
                StrSql = "select * from TblMapping";
                DataSet DsMap = new DataSet();
                DsMap = cn.FillDataSet(StrSql);
                if (DsMap.Tables[0].Rows.Count < 12)
                {
                    cn.execute_NonQuery("drop table tblMapping");
                    CreateMapTable();
                }
            }
                     
        }
        catch (Exception ex)
        { }
        return;
        try
        {
            StrSql = "alter table tbluser add LeaveApproval char(1) default 'N' ";
            cn.execute_NonQuery(StrSql);
        }
        catch (Exception ex)
        { }
        try
        {
            StrSql = "alter table tbluser add IsAutoLogin char(1) default 'N' ";
            cn.execute_NonQuery(StrSql);
        }
        catch (Exception ex)
        { }
    }
    private void CreateMapTable()
    {
        StrSql = "Create table tblMapping (id bigint primary key identity(1,1), FieldName char(100), ShowAs char(20) ) ";
        cn.execute_NonQuery(StrSql);
        cn.execute_NonQuery("insert into tblMapping(FieldName,Showas)values('CardNo','CardNo')");
        cn.execute_NonQuery("insert into tblMapping(FieldName,Showas)values('EmpCode','PayCode')");
        cn.execute_NonQuery("insert into tblMapping(FieldName,Showas)values('Name','Name')");
        cn.execute_NonQuery("insert into tblMapping(FieldName,Showas)values('FName','Guardian Name')");
        cn.execute_NonQuery("insert into tblMapping(FieldName,Showas)values('Company/School/University Name','Company')");
        cn.execute_NonQuery("insert into tblMapping(FieldName,Showas)values('Department','Department')");
        cn.execute_NonQuery("insert into tblMapping(FieldName,Showas)values('Catagory','Catagory')");
        cn.execute_NonQuery("insert into tblMapping(FieldName,Showas)values('Section','Section')");
        cn.execute_NonQuery("insert into tblMapping(FieldName,Showas)values('Grade','Grade')");
        cn.execute_NonQuery("insert into tblMapping(FieldName,Showas)values('Designation','Designation')");
        cn.execute_NonQuery("insert into tblMapping(FieldName,Showas)values('Branch','Branch')");
        cn.execute_NonQuery("insert into tblMapping(FieldName,Showas)values('Employee','Employee')");



    }
}