﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="frmLeaveMaster.aspx.cs" Inherits="frmLeaveMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
      <script type="text/javascript">  
        function State_OnKeyUp(s, e) {  
            s.SetText(s.GetText().toUpperCase().trim());
        }  
    </script>  
    <%-- DXCOMMENT: Configure ASPxGridView control --%>
 <div>
        <table style="width:100%; border:solid;">
            <tr>
                <td align="center"></td>
            </tr>
            <tr>
                <td>
                    <dx:ASPxGridView ID="grdLeave" runat="server" AutoGenerateColumns="False" Width="100%" KeyFieldName="Leavefield" ClientInstanceName="grdLeave"  >
                        <Settings ShowFilterRow="True" ShowTitlePanel="True"/>
                        <ClientSideEvents CustomButtonClick="function(s, e) {

var obj=grdLeave.GetRowKey(e.visibleIndex)

	window.location = &quot;frmAddLeave.aspx?Value=&quot; + obj;
}"  />
                        <SettingsAdaptivity AdaptivityMode="HideDataCells" />
                        <SettingsPager>
                            <PageSizeItemSettings Visible="true" Items="10, 20, 50,100" />
                        </SettingsPager>
                        <SettingsEditing Mode="PopupEditForm">
                        </SettingsEditing>
                        <Settings ShowFilterRow="True" />
                        <SettingsBehavior ConfirmDelete="True" />
                        <SettingsDataSecurity AllowEdit="False" AllowInsert="False" />
                        <SettingsPopup>
                            <EditForm Modal="True" Width="800px" HorizontalAlign="WindowCenter" 
                PopupAnimationType="Slide" VerticalAlign="WindowCenter" />
                        </SettingsPopup>
                        <SettingsSearchPanel Visible="True" />
                        <SettingsText PopupEditFormCaption="Employee" Title="Employee" />
                        <Columns>
                            <dx:GridViewCommandColumn ShowNewButton="true" ShowEditButton="true" VisibleIndex="1" ButtonRenderMode="Image" ShowDeleteButton="True">
                                <CustomButtons >
                                    <dx:GridViewCommandColumnCustomButton ID="Edit"  >
                                        <Image ToolTip="Edit" IconID="actions_edit_16x16devav"/>
                                    </dx:GridViewCommandColumnCustomButton>
                                </CustomButtons>
                            </dx:GridViewCommandColumn>
                            <dx:GridViewCommandColumn SelectAllCheckboxMode="Page" ShowClearFilterButton="True" ShowSelectCheckbox="True" VisibleIndex="0" Caption=" ">
                            </dx:GridViewCommandColumn>
                            <dx:GridViewDataTextColumn FieldName="Leavefield" VisibleIndex="2" Caption="Leave Field">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="leavecode" VisibleIndex="3" Caption="Leave Code">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="leavedescription" VisibleIndex="4" Caption="Leave Discription">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Show on Web" VisibleIndex="5" Caption="Visible For Apply">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Is Accrual" VisibleIndex="6" Caption="Is Accural">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Off Include" VisibleIndex="7" Caption="Off Include">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="Holiday Include" VisibleIndex="8" Caption="Holiday Include">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn FieldName="LeaveType" VisibleIndex="9" Caption="Leave Type">
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:ASPxGridView>
                </td>
            </tr>
            <tr>
                <td>
                    <dx:ASPxButton ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click">
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

