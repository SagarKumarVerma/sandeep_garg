﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="frmMonthlyReport.aspx.cs" Inherits="frmMonthlyReport" %>
<%@ Register Src="UserControl/Selection.ascx" TagName="Selection" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
      <div align="center">
          <asp:ScriptManager ID="ScriptManager1" runat="server">
          </asp:ScriptManager>
          <table align="center" style="width:100%" cellpadding="0" cellspacing="0" >
              <tr>
                  <td ></td>
              </tr>
              <tr>
                  <td colspan="3" style="height:25px;width:100%;" align="center" class="tableHeaderCss">
                      <asp:Label ID="lblMsg" runat="server" Text="Monthly Attendance Report" CssClass="lblCss" ></asp:Label>
                  </td>
              </tr>
              <tr>
                  <td align="center">
                      <table  style="width:880px;text-align:left" >
                          <tr>
                              <td style="height: 5px; width: 250px;"></td>
                              <td style="width: 250px;"></td>
                              <td style="width: 250px;"></td>
                          </tr>
                          <tr>
                              <td style="width: 250px; height: 5px;">
                                  <%--<cc1:maskededitextender ID="MaskedEditExtender5" runat="server" AcceptNegative="Left"
                    DisplayMoney="Left" ErrorTooltipEnabled="True" Mask="99/99/9999" MaskType="Date"
                    MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                    TargetControlID="txtDateFrom">
                                  </cc1:maskededitextender>--%>
                              </td>
                              <td style="width: 250px">
                                 <%-- <cc1:maskededitextender ID="MaskedEditExtender1" runat="server" AcceptNegative="Left"
                    DisplayMoney="Left" ErrorTooltipEnabled="True" Mask="99/99/9999" MaskType="Date"
                    MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                    TargetControlID="txtToDate">
                                  </cc1:maskededitextender>--%>
                              </td>
                              <td style="width: 250px"></td>
                          </tr>
                          <tr>
                              <td style="width: 250px; height: 5px;">&nbsp;
				                  <asp:Label ID="Label1" runat="server" CssClass="text" Text="From" style="left: 0px; top: 1px"></asp:Label>
                &nbsp;&nbsp;
                                  <asp:TextBox ID="txtDateFrom" runat="server" TabIndex="17" CssClass="TextBoxCss_Msmall" MaxLength="1" style="text-align:justify" ValidationGroup="MKE" AutoPostBack="True" OnTextChanged="txtDateFrom_TextChanged"></asp:TextBox>
                                  <asp:ImageButton ID="ImgBntCalc" TabIndex="18" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" CausesValidation="False" />
                                  <asp:RequiredFieldValidator ID="NReq" runat="server" ControlToValidate="txtDateFrom"
                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />Date is required."
                    SetFocusOnError="True" ValidationGroup="MKE"></asp:RequiredFieldValidator>
                                  <cc1:validatorcalloutextender
                        ID="NReqE" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="NReq">
                                  </cc1:validatorcalloutextender>
                              </td>
                              <td style="width: 250px" valign="top">&nbsp;&nbsp;<asp:Label ID="Label2" runat="server" CssClass="text" Text="To   "></asp:Label>
				&nbsp;
                                  <asp:TextBox ID="txtToDate" runat="server" TabIndex="17" CssClass="TextBoxCss_Msmall" MaxLength="1" style="text-align:justify" ValidationGroup="MKE"></asp:TextBox>
                                  <asp:ImageButton ID="imgCal" TabIndex="18" runat="server" ImageUrl="~/images/Calendar_scheduleHS.png" CausesValidation="False" />
                                  <asp:RequiredFieldValidator
                ID="NReq2" runat="server" ControlToValidate="txtToDate" Display="None"
                ErrorMessage="<b>Required Field Missing</b><br />Date is required." SetFocusOnError="True"
                ValidationGroup="MKE"></asp:RequiredFieldValidator>
                                  <cc1:validatorcalloutextender
                        ID="NReqE2" runat="server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="NReq2">
                                  </cc1:validatorcalloutextender>
                              </td>
                              <td style="width: 250px" class="text" valign="top">Sort by &nbsp;<asp:DropDownList ID="ddlSorting" runat="server" CssClass="DropDownCss">
                                  <asp:ListItem Selected="True" Value="tblemployee.paycode">Paycode</asp:ListItem>
                                  <asp:ListItem Value="tblemployee.presentcardno">Card Number</asp:ListItem>
                                  <asp:ListItem Value="tblemployee.EmpName">Employee Name</asp:ListItem>
                                  <asp:ListItem Value="tbldepartment.Departmentcode,tblemployee.paycode">Department + Paycode</asp:ListItem>
                                  <asp:ListItem Value="tbldepartment.Departmentcode,tblemployee.presentcardno">Departmentcode + Card Number</asp:ListItem>
                                  <asp:ListItem Value="tbldepartment.Departmentcode,tblemployee.empname">Department + Name</asp:ListItem>
                                  <asp:ListItem Value="tbldivision.divisioncode,tblemployee.paycode">Section+Paycode</asp:ListItem>
                                  <asp:ListItem Value="tbldivision.divisioncode,tblemployee.presentcardno">Section + Card Number</asp:ListItem>
                                  <asp:ListItem Value="tbldivision.divisioncode,tblemployee.empname">Section + Name</asp:ListItem>
                                  <asp:ListItem Value="tblcatagory.Cat,tblemployee.paycode">Catagory + Paycode</asp:ListItem>
                                  <asp:ListItem Value="tblcatagory.Cat,tblemployee.presentcardno">Catagory + Card Number</asp:ListItem>
                                  <asp:ListItem Value="tblcatagory.Cat,tblemployee.empname">Catagory + Name</asp:ListItem>
                                  </asp:DropDownList>
                              </td>
                          </tr>
                          <tr>
                              <td style="width: 250px; height: 5px;">
                                  <cc1:calendarextender ID="calendarButtonExtender" runat="server" TargetControlID="txtDateFrom" PopupButtonID="ImgBntCalc" cssclass="cal_Theme1" Format="dd/MM/yyyy"  />
        <%--<cc1:MaskedEditValidator ID="MaskedEditValidator5" runat="server" ControlExtender="MaskedEditExtender5"
            ControlToValidate="txtDateFrom" Display="Dynamic" EmptyValueBlurredText="Enter From Date"
            InvalidValueBlurredMessage="Date is invalid" InvalidValueMessage="Date is invalid"
            TooltipMessage="Enter From Date" ValidationGroup="MKE"></cc1:MaskedEditValidator>--%></td>
                              <td style="width:250px">
                                  <cc1:calendarextender ID="CalendarExtender1" runat="server" TargetControlID="txtToDate" cssclass="cal_Theme1"
            PopupButtonID="imgCal" Format="dd/MM/yyyy"  />
        <%--<cc1:MaskedEditValidator ID="MaskedEditValidator1" runat="server" ControlExtender="MaskedEditExtender5"
            ControlToValidate="txtToDate" Display="Dynamic" EmptyValueBlurredText="Enter From Date"
            InvalidValueBlurredMessage="Date is invalid" InvalidValueMessage="Date is invalid"
            TooltipMessage="Enter To Date" ValidationGroup="MKE"></cc1:MaskedEditValidator>--%></td>
                              <td style="width: 250px"></td>
                          </tr>
                          <tr>
                              <td style="height: 5px; width: 250px;"></td>
                              <td style="width: 250px"></td>
                              <td style="width: 250px"></td>
                          </tr>
                          <tr>
                              <td style="width: 250px; height: 5px;">
                                  <asp:RadioButton ID="radEmployeeWisePerformance" runat="server" Checked="True" GroupName="a"
                    Text="Employee Wise Performance" CssClass="Radiocss" />
                              </td>
                              <td style="width: 250px">
                                  <asp:RadioButton ID="radEmployeewiseAttendance" runat="server" GroupName="a" Text="Employee Wise Attendance" CssClass="Radiocss" />
                              </td>
                              <td style="width: 250px">
                                  <asp:RadioButton ID="radDepartmentWise" runat="server" GroupName="a" Text="Department Wise Attendance" CssClass="Radiocss" />
                              </td>
                          </tr>
                          <tr>
                              <td style="height: 5px; width: 250px;"></td>
                              <td style="width: 250px"></td>
                              <td style="width: 250px"></td>
                          </tr>
                          <tr>
                              <td style="width: 250px; height: 5px;">
                                  <asp:RadioButton ID="radMonthlyLate" runat="server" GroupName="a" Text="Late Arrival Register" CssClass="Radiocss" />
                              </td>
                              <td style="width: 250px">
                                  <asp:RadioButton ID="radEarlyDeparture" runat="server" GroupName="a" Text="Early Departure Register" CssClass="Radiocss" />
                              </td>
                              <td style="width: 250px">
                                  <asp:RadioButton ID="radAbsent" runat="server" GroupName="a" Text="Absenteeism Register" CssClass="Radiocss" />
                              </td>
                          </tr>
                          <tr>
                              <td style="height: 5px; width: 250px;"></td>
                              <td style="width: 250px"></td>
                              <td style="width: 250px"></td>
                          </tr>
                          <tr>
                              <td style="width: 250px; height: 5px;">
                                  <asp:RadioButton ID="radshiftwise" runat="server" GroupName="a" Text="Shift Wise Attendance" CssClass="Radiocss" />
                              </td>
                              <td style="width: 250px">
                                  <asp:RadioButton ID="radMontlyOT" runat="server" GroupName="a" Text="Over Time Register" CssClass="Radiocss" />
                              </td>
                              <td style="width: 250px">
                                  <asp:RadioButton ID="radOT" runat="server" GroupName="a" Text="Over Time Summary" CssClass="Radiocss" />
                              </td>
                          </tr>
                          <tr>
                              <td style="height: 5px; width: 250px;"></td>
                              <td style="width: 250px"></td>
                              <td style="width: 250px"></td>
                          </tr>
                          <tr>
                              <td style="width: 250px; height: 5px;">
                                  <asp:RadioButton ID="radOverStay" runat="server" GroupName="a" Text="Over Stay Register" CssClass="Radiocss" />
                              </td>
                              <td style="width: 250px">
                                  <asp:RadioButton ID="radShiftSchedule" runat="server" GroupName="a" Text="Shift Schedule" CssClass="Radiocss" />
                              </td>
                              <td style="width: 250px">
                                  <asp:RadioButton ID="radEarlyDeptSummary" runat="server" GroupName="a" Text="Early Departure Summary" CssClass="Radiocss" />
                              </td>
                          </tr>
                          <tr>
                              <td style="height: 5px; width: 250px;"></td>
                              <td style="width: 250px"></td>
                              <td style="width: 250px"></td>
                          </tr>
                          <tr>
                              <td style="width: 250px; height: 5px;">
                                  <asp:RadioButton ID="radLateArrivalSummary" runat="server" GroupName="a" Text="Late Arrival Summary" CssClass="Radiocss" />
                              </td>
                              <td style="width: 250px">
                                  <asp:RadioButton ID="radLate_OverStay" runat="server" GroupName="a" Text="Late and Over Stay" CssClass="Radiocss" />
                              </td>
                              <td style="width: 250px">
                                  <asp:RadioButton ID="radTotalTimeLoss" runat="server" GroupName="a" Text="Total Loss and Over Stay" CssClass="Radiocss" />
                              </td>
                          </tr>
                          <tr>
                              <td style="height: 5px; width: 250px;"></td>
                              <td style="width: 250px"></td>
                              <td style="width: 250px"></td>
                          </tr>
                          <tr>
                              <td style="width: 250px; height: 5px;" valign="top">
                                  <asp:RadioButton ID="radEmpAttendance_Percentage" runat="server" GroupName="a" Text="Employee Wise Attendance Percentage" CssClass="Radiocss" />
                              </td>
                              <td style="width: 250px" valign="top">
                                  <asp:RadioButton ID="radDeptWiseAttendance_Percentagte" runat="server" GroupName="a" Text="Department Wise Attendance Percentage" CssClass="Radiocss" Width="280px" />
                              </td>
                              <td style="width: 250px" valign="top">
                                  <asp:RadioButton ID="radPerformanceRegister" runat="server"  GroupName="a"
                    Text="Performance Register" CssClass="Radiocss" />
                              </td>
                          </tr>
                          <tr>
                              <td style="height: 5px; width: 250px;" valign="top"></td>
                              <td style="width: 250px" valign="top"></td>
                              <td style="width: 250px" valign="top"></td>
                          </tr>
                          <tr>
                              <td style="width: 250px; height: 5px;">
                                  <asp:RadioButton ID="radMusterRoll" runat="server" GroupName="a" Text="Muster Roll" CssClass="Radiocss" />
                              </td>
                              <td style="width: 250px">
                                  <asp:RadioButton ID="radManualPunch" runat="server" CssClass="Radiocss" GroupName="a"
                    Text="Manual Punch Report" />
                              </td>
                              <td style="width: 250px">
                                  <asp:RadioButton ID="radLogReport" runat="server" CssClass="Radiocss" GroupName="a"
                    Text="Log Report" Visible="False" />
                              </td>
                          </tr>
                          <tr>
                              <td style="width: 250px; height: 5px"></td>
                              <td style="width: 250px"></td>
                              <td style="width: 250px"></td>
                          </tr>
                          <tr>
                              <td style="width: 250px; height: 5px">
                                  <asp:RadioButton ID="radShiftChangeDepartment" runat="server" GroupName="a" Text="Shift Change Summary Department" CssClass="Radiocss" Font-Bold="False" Font-Size="8pt" Visible="False" />
                              </td>
                              <td style="width: 250px">
                                  <asp:RadioButton ID="radTotalAbsent" runat="server" GroupName="a" Text="Total Absent " CssClass="Radiocss" Font-Bold="False" Font-Size="8pt" Visible="False" />
                              </td>
                              <td style="width: 250px">
                                  <asp:RadioButton ID="radlvarrear" runat="server" GroupName="a" Text="Leave Arrear Report" CssClass="Radiocss" Font-Bold="False" Font-Size="8pt" Visible="False" />
                              </td>
                          </tr>
                          <tr>
                              <td style="width: 250px; height: 5px"></td>
                              <td style="width: 250px">
                                  <asp:RadioButton ID="radNightShift" runat="server" GroupName="a" Text="Employee Night Attendance" CssClass="Radiocss" Font-Bold="False" Font-Size="8pt" Visible="False" />
                              </td>
                              <td style="width: 250px">
                                  <asp:RadioButton ID="radShiftChangeSBU" runat="server" GroupName="a" Text="Shift Change Summary SBU" CssClass="Radiocss" Font-Bold="False" Font-Size="8pt" Visible="False" />
                              </td>
                          </tr>
                          <tr>
                              <td style="width: 250px; height: 5px">
                                  <asp:RadioButton ID="radReg_arrear" runat="server" GroupName="a" Text="Regularization Arrear Report" CssClass="Radiocss" Font-Bold="False" Font-Size="8pt" Visible="False" />
                              </td>
                              <td style="width: 250px">
                                  <asp:RadioButton ID="radCoff" runat="server" GroupName="a" Text="Comp Off Report" CssClass="Radiocss" Font-Bold="False" Font-Size="8pt" Visible="False" />
                              </td>
                              <td style="width: 250px">
                                  <asp:RadioButton ID="radStatusChange" runat="server" GroupName="a" Text="Status Change Report" CssClass="Radiocss" Font-Bold="False" Font-Size="8pt" Visible="False" />
                              </td>
                          </tr>
                          <tr>
                              <td style="width: 250px; height: 5px">
                                  <asp:RadioButton ID="RadAtt_Leave" runat="server" GroupName="a" Text="Attendance and Leave" CssClass="Radiocss" Font-Size="9pt" ForeColor="SteelBlue" Visible="False" />
                              </td>
                              <td style="width: 250px"></td>
                              <td style="width: 250px"></td>
                          </tr>
                          <tr>
                              <td style="width: 250px; height: 5px">
                                  <asp:RadioButton ID="radAttendance1" runat="server" GroupName="a" Text="Customized Attendance" CssClass="Radiocss" Font-Size="9pt" ForeColor="SteelBlue" Font-Bold="True" Visible="True" />
                              </td>
                              <td style="width: 250px">
                                  <asp:RadioButton ID="radAttendanceDetails" runat="server" GroupName="a" Text="Attendance and Leave" CssClass="Radiocss" Font-Size="9pt" ForeColor="SteelBlue" Visible="False" />
                              </td>
                              <td style="width: 250px">
                                  <asp:RadioButton ID="radAttend_Leave" runat="server" GroupName="a" Text="Attendnce-PTT" CssClass="Radiocss" Font-Size="9pt" ForeColor="SteelBlue" Visible="False" />
                              </td>
                          </tr>
                          <tr>
                              <td style="width: 250px; height: 5px">
                                  <asp:RadioButton ID="radMonthlyAttendance_New" runat="server" GroupName="a" Text="New Monthly Attendnce" CssClass="Radiocss" Font-Size="9pt" ForeColor="SteelBlue" Font-Bold="true" Visible="False" />
                              </td>
                              <td style="width: 250px">
                                  <asp:RadioButton ID="radMonthlyReport" runat="server" GroupName="a" Text="New Report" CssClass="Radiocss" Visible="False" />
                              </td>
                              <td style="width: 250px"></td>
                          </tr>
                          <tr>
                              <td colspan="3" style="text-align: right" id="TDATT" runat="server"  visible="false" >
                                  <table>
                                      <tr>
                                          <td></td>
                                          <td></td>
                                      </tr>
                                      <tr>
                                          <td colspan="2">
                                              <asp:GridView ID="GridView1" runat="server">
                                              </asp:GridView>
                                          </td>
                                      </tr>
                                  </table>
                              </td>
                          </tr>
                          <tr>
                              <td colspan="3" style="width: 250px; height: 5px"></td>
                          </tr>
                          <tr>
                              <td colspan="3" style="height: 5px; width: 850px;"></td>
                          </tr>
                          <tr>
                              <td colspan="3" align="right" style="width: 880px; height: 5px">
                                  <asp:RadioButtonList ID="RadExport" runat="server" CssClass="Radiocss" RepeatDirection="Horizontal"
					   Width="337px">
                                      <asp:ListItem Selected="True" >Excel</asp:ListItem>
                                      <asp:ListItem Enabled="false" >PDF</asp:ListItem>
                                  </asp:RadioButtonList>
                              </td>
                          </tr>
                          <tr>
                              <td style="width: 250px; height: 5px;"></td>
                              <td style="width: 250px;">&nbsp;</td>
                              <td style="width: 250px;" align="right">
                                  <asp:Button ID="cmdSelection" runat="server" CssClass="buttoncss" Text=" Selection" />
				&nbsp;<asp:Button ID="cmdGenerate" runat="server" OnClick="cmdGenerate_Click" Text="Generate" ValidationGroup="MKE" CssClass="buttoncss" />
                                  <asp:Button ID="cmdClose" runat="server" Text="Close" OnClick="cmdClose_Click1" CssClass="buttoncss" />
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td colspan="3" style="width: 250px; height: 5px"></td>
                          </tr>
                        <%--  <tr>
                              <td colspan="3" style="height: 5px; width: 880px;">
                                  <asp:Panel ID="Panel1" runat="server" Style="display: none;width:723px" CssClass="modalPopup">
                                      <asp:Panel ID="Panel3" runat="server" Style="cursor: move;background-color:#ffffff;border:solid 1px Gray;color:Black">
                                          <table style="width:100%" cellpadding="0" cellspacing="0">
                                              <tr>
                                                  <td align="right" style="width: 721px; background-color: #ffffff">
                                                      <asp:Image ID="imgClose" runat="server" ImageUrl="~/Images/Icons/IconClose.bmp" BackColor="Silver" Height="25px" ToolTip="Close" Width="20px" style="cursor:default" />
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td style="width: 721px">
                                                      <uc1:Selection ID="Selection1" runat="server" />
                                                  </td>
                                              </tr>
                                          </table>
                                      </asp:Panel>
                                      <div>
                                      </div>
                                  </asp:Panel>
                                  <cc1:modalpopupextender ID="ModalPopupExtender" CancelControlID="imgClose" runat="server" BackgroundCssClass="modalBackground"
					   DropShadow="true" PopupControlID="Panel1" TargetControlID="cmdSelection">
                                  </cc1:modalpopupextender>
                              </td>
                          </tr>--%>
                           
                          <tr>
                              <td colspan="3" style="width: 250px; height: 5px; top: 1104px; left: 2px;" class="text" valign="top"></td>
                          </tr>
                      </table>
                  </td>
              </tr>
          </table>
      </div>
</asp:Content>

