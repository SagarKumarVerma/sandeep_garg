﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Globalization;
using System.Text;
using System.IO;
using System.Diagnostics;
using GlobalSettings;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text;

public partial class frmReportDaily : System.Web.UI.Page
{
    Class_Connection con = new Class_Connection();
    string datefrom = null;
    string dateTo = null;
    string strsql = null;
    string Strsql = null;
    DataSet ds = null;
    string OpenPopupPage = "";
    string Selection = "";
    string status = null;
    string leave = null;
    DateTime toDate;
    DateTime FromDate;
    DateTime FromDateNepal;
    TimeSpan ts = TimeSpan.Zero;
    int day = 0;
    string btnGoBack = "";
    string resMsg = "";
    ErrorClass ec = new ErrorClass();
    protected void Page_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();
        btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
        if (ex is HttpRequestValidationException)
        {
            resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" +
                   "Form doesn't need to contain valid HTML, just anything with opening and closing angled brackets (<...>).<br />" +
                   btnGoBack +
                   "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
        else if (ex is OleDbException)
        {
            resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" + ex.Message.ToString() + ".<br />" +
                    "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
        else
        {
            resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" + ex.Message.ToString() + ".<br />" +
                    "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
       
        if(!IsPostBack)
        {
            txtDateFrom.Text = System.DateTime.Today.ToString("dd/MM/yyyy");
            txtToDate.Text = System.DateTime.Today.ToString("dd/MM/yyyy");
            //LookupCompany.DataSource = DataFilter.CompanyNonAdmin;
            //LookupCompany.DataBind();
            //LookUpDept.DataSource = DataFilter.LocationNonAdmin;
            //LookUpDept.DataBind();
            //lookupSec.DataSource = DataFilter.DivNonAdmin;
            //lookupSec.DataBind();
            //lookupGrade.DataSource = DataFilter.GradeNonAdmin;
            //lookupGrade.DataBind();
            //lookupCat.DataSource = DataFilter.CatNonAdmin;
            //lookupCat.DataBind();
            //LookEmp.DataSource = DataFilter.EmpNonAdmin;
            //LookEmp.DataBind();
            //bindShift();
            //LookUpShift.DataSource = DataFilter.ShiftNonAdmin;
            //LookUpShift.DataBind();
            Session["IsNepali"] = "N";
            Session["IsNepali"] = "N";
            bindShift();
            bindCompany();
            bindPaycode();
            bindDepartment();
            bindSection();
            bindGrade();
            bindCategory();
        }

    }
    protected void bindCompany()
    {
        try
        {
            Strsql = "select CompanyCode,Companyname from tblCompany";
            if (Session["LoginUserName"].ToString().Trim().Trim().ToUpper() == "ADMIN")
            {
                Strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
            }
            else
            {
                Strsql += " where companycode ='" + Session["LoginCompany"].ToString().Trim().Trim() + "' ";
            }

            ds = new DataSet();
            ds = con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                LookupCompany.DataSource = ds.Tables[0];
                LookupCompany.DataBind();
            }
        }
        catch (Exception Err)
        {

            Error_Occured("bindCompany", Err.Message);
        }
    }
    protected void bindPaycode()
    {
        try
        {
            Strsql = "select PAYCODE,EMPNAME from tblemployee where active='Y' and  CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "'";
            ds = new DataSet();
            ds = con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                LookEmp.DataSource = ds.Tables[0];
                LookEmp.DataBind();
            }
        }
        catch (Exception Err)
        {

            Error_Occured("bindPaycode", Err.Message);
        }
    }
    protected void bindShift()
    {
        Strsql = "select Shift,Shift +'-'+ convert(char(5),starttime, 108) +'-'+ convert(char(5),endtime, 108) 'ShiftTime' from  TblShiftMaster where companycode in(" + Session["Auth_Comp"].ToString().Trim() + ") ";
        ds = new DataSet();
        ds = con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            LookUpShift.DataSource = ds.Tables[0];
            LookUpShift.Value = "Shift";
            LookUpShift.Text = "ShiftTime";
            LookUpShift.DataBind();
        }
    }
    protected void bindDepartment()
    {
        try
        {
            Strsql = "select DepartmentCode,departmentname from tbldepartment";
            if (Session["LoginUserName"].ToString().Trim().Trim().ToUpper() == "ADMIN")
            {
                Strsql += " where DepartmentCode in (" + Session["Auth_Dept"].ToString().Trim() + ") and  CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "' ";
            }
            else
            {
                Strsql += " where  CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "' ";
            }
            ds = new DataSet();
            ds = con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                LookUpDept.DataSource = ds.Tables[0];
                LookUpDept.DataBind();
            }
        }
        catch (Exception Err)
        {

            Error_Occured("bindDepartment", Err.Message);
        }
    }
    protected void bindCategory()
    {
        try
        {
            Strsql = "select CAT,CATAGORYNAME from tblCatagory where   CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "'";
            ds = new DataSet();
            ds = con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lookupCat.DataSource = ds.Tables[0];
                lookupCat.DataBind();

            }
        }
        catch (Exception Err)
        {

            Error_Occured("bindCategory", Err.Message);
        }
    }
    protected void bindSection()
    {
        try
        {
            Strsql = "select DivisionCode,DivisionName from tblDivision where   CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "'";
            ds = new DataSet();
            ds = con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lookupSec.DataSource = ds.Tables[0];
                lookupSec.DataBind();
            }
        }
        catch (Exception Err)
        {

            Error_Occured("bindSection", Err.Message);
        }
    }
    protected void bindGrade()
    {
        try
        {
            Strsql = "select GradeCode,GradeName from tblGrade where   CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "'";
            ds = new DataSet();
            ds = con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lookupGrade.DataSource = ds.Tables[0];
                lookupGrade.DataBind();
            }
        }
        catch (Exception Err)
        {

            Error_Occured("bindGrade", Err.Message);
        }
    }


    //ErrorClass ec = new ErrorClass();

   
    protected void Page_Init(object sender, EventArgs e)
    {
        //LookupCompany.DataSource = DataFilter.CompanyNonAdmin;
        //LookupCompany.DataBind();
        //LookUpDept.DataSource = DataFilter.LocationNonAdmin;
        //LookUpDept.DataBind();
        //lookupSec.DataSource = DataFilter.DivNonAdmin;
        //lookupSec.DataBind();
        //lookupGrade.DataSource = DataFilter.GradeNonAdmin;
        //lookupGrade.DataBind();
        //lookupCat.DataSource = DataFilter.CatNonAdmin;
        //lookupCat.DataBind();
        //LookEmp.DataSource = DataFilter.EmpNonAdmin;
        //LookEmp.DataBind();
        bindShift();
        bindCompany();
        bindPaycode();
        bindDepartment();
        bindSection();
        bindGrade();
        bindCategory();

    }

    protected void fire(object sender, EventArgs e)
    {
        try
        {
            FromDate = Convert.ToDateTime(txtDateFrom.Value);  //DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            toDate = FromDate.AddMonths(1).AddDays(-1);
            txtToDate.Text = toDate.ToString("dd/MM/yyyy");
           
            if (radContAbsent1.Checked || radcontlatearrival1.Checked || radContEarlyDept1.Checked)
            {
                txtToDate.Visible = false;
                lblto.Visible = false;
                txtDateFrom.Enabled = false;
                Tday.Visible = true;
                txtDays.Text = "";
            }
            else if (radhris.Checked)
            {
                txtToDate.Visible = true;
                lblto.Visible = true;
            }
            else
            {
                txtToDate.Visible = false;
             //   imgCal.Visible = false;
                lblto.Visible = false;
                Tday.Visible = false;
                txtDateFrom.Enabled = true;
            }
        }
        catch
        {
        }
    }
    protected void GetSelection()
    {
        try
        {
            strsql = "";
            string Company = "";
            string Employee = "";
            string Dept = "";
            string category = "";
            string Shift = "";
            string Division = "";
            string Grade = "";

            if (LookupCompany.Text.ToString().Trim() != string.Empty || LookupCompany.Text.ToString().Trim() != "")
            {


                string[] SplitComp = LookupCompany.Text.ToString().Trim().Split(',');
                Company = "";
                foreach (string sc in SplitComp)
                    Company += "'" + sc.Trim() + "',";

                Company = Company.TrimEnd(',');

                
                //Company = LookupCompany.Text.ToString().Trim();
            }
            if (LookUpDept.Text.ToString().Trim() != string.Empty || LookUpDept.Text.ToString().Trim() != "")
            {
                string[] SplitDep = LookUpDept.Text.ToString().Trim().Split(',');
                Dept = "";
                foreach (string sc in SplitDep)
                    Dept += "'" + sc.Trim() + "',";

                Dept = Dept.TrimEnd(',');
            }
            if (lookupSec.Text.ToString().Trim() != string.Empty || lookupSec.Text.ToString().Trim() != "")
            {

                string[] SplitSec = lookupSec.Text.ToString().Trim().Split(',');
                Division = "";
                foreach (string sc in SplitSec)
                    Division += "'" + sc.Trim() + "',";

                Division = Division.TrimEnd(',');
                
                
                
                //Division = lookupSec.Text.ToString().Trim();
            }
            if (lookupGrade.Text.ToString().Trim() != string.Empty || lookupGrade.Text.ToString().Trim() != "")
            {

                string[] SplitGr = lookupGrade.Text.ToString().Trim().Split(',');
                Grade = "";
                foreach (string sc in SplitGr)
                    Grade += "'" + sc.Trim() + "',";

                Grade = Grade.TrimEnd(',');
                
                
                
               // Grade = lookupGrade.Text.ToString().Trim();
            }
            if (lookupCat.Text.ToString().Trim() != string.Empty || lookupCat.Text.ToString().Trim() != "")
            {


                string[] SplitCAt = lookupCat.Text.ToString().Trim().Split(',');
                category = "";
                foreach (string sc in SplitCAt)
                    category += "'" + sc.Trim() + "',";

                category = category.TrimEnd(',');
                
                //category = lookupCat.Text.ToString().Trim();
            }
            if (LookUpShift.Text.ToString().Trim() != string.Empty || LookUpShift.Text.ToString().Trim() != "")
            {

                string[] SplitShift = LookUpShift.Text.ToString().Trim().Split(',');
                Shift = "";
                foreach (string sc in SplitShift)
                    Shift += "'" + sc.Trim() + "',";

                Shift = Shift.TrimEnd(',');
                
                //Shift = LookUpShift.Text.ToString().Trim();
            }
            if (LookEmp.Text.ToString().Trim() != string.Empty || LookEmp.Text.ToString().Trim() != "")
            {
                string[] SplitEmp = LookEmp.Text.ToString().Trim().Split(',');
                Employee = "";
                foreach (string sc in SplitEmp)
                    Employee += "'" + sc.Trim() + "',";

                Employee = Employee.TrimEnd(',');
                
                
                //Employee = LookEmp.Text.ToString().Trim();
            }
            strsql = " and 1=1 ";
            if (Company != "")
            {
                strsql += "and tblemployee.Companycode in (" + Company.ToString().Trim() + ") ";
            }
            if (Employee != "")
            {
                strsql += "and tblemployee.paycode in (" + Employee.ToString().Trim() + ")  ";
            }
            if (Dept != "")
            {
                strsql += "and tblemployee.DepartmentCode in (" + Dept.ToString().Trim() + ")  ";
            }
            else
            {
                strsql += "and tblemployee.DepartmentCode in (" + Session["Auth_Dept"].ToString().Trim() + ")  ";
            }
            if (category != "")
            {
                strsql += "and tblemployee.Cat in (" + category.ToString().Trim() + ")  ";
            }
            if (Shift != "")
            {
                strsql += "and tbltimeregister.shiftattended in (" + Shift.ToString().Trim() + ")  ";
            }
            if (Division != "")
            {
                strsql += "and tblemployee.Divisioncode in (" + Division.ToString().Trim() + ")  ";
            }
            if (Grade != "")
            {
                strsql += "and tblemployee.Gradecode in (" + Grade.ToString().Trim() + ")";
            }
            Session["SelectionQuery"] = strsql;
        }
        catch
        {
            Session["SelectionQuery"] = "";
            Session["SelectionQuery"] = null;
        }
    }
    protected void btnGenerate_Click(object sender, EventArgs e)
    {
        string ReportView = "";
        DataSet dsEmpCount = new DataSet();
        string EmpCodes = "", CompanySelection = "";
        TimeSpan ts = toDate.Subtract(FromDate);
        int day = ts.Days;
        ReportView = ConfigurationSettings.AppSettings["ReportView_HeadID"].ToString();
        if (Session["VALIDYEARS"] == null)
        {
            Response.Redirect("Default.aspx");
        }
       // Session["ExportInto"] = RadExport.SelectedIndex;
        string msg;
        if (txtDateFrom.Text.ToString().Trim().Length < 10)
        {
            msg = "<script language='javascript'> alert('Enter a valid Punch Date...')</script>";
            Page.RegisterStartupScript("msg", msg);
            txtDateFrom.Focus();
            return;
        }
        if (txtToDate.Text.ToString().Trim().Length < 10)
        {
            msg = "<script language='javascript'> alert('Enter a valid Punch Date...')</script>";
            Page.RegisterStartupScript("msg", msg);
            txtToDate.Focus();
            return;
        } 
        try
        {


            if (Session["SelectionQuery"] != null)
            {
                Selection = Session["SelectionQuery"].ToString();
            }
            else
            {
                if (Session["usertype"].ToString().Trim() == "A")
                {
                    if (Session["Auth_Comp"] != null)
                    {
                        Selection += " and tblemployee.companycode in (" + Session["Auth_Comp"].ToString() + ") ";
                    }
                    if (Session["Auth_Dept"] != null)
                    {
                        Selection += " and tblemployee.Departmentcode in (" + Session["Auth_Dept"].ToString() + ") ";
                    }
                }
                if (Session["usertype"].ToString().Trim() == "H")
                {
                    if (ReportView.ToString().Trim() == "Y")
                    {
                        strsql = "select paycode from tblemployee where headid='" + Session["PAYCODE"].ToString().Trim() + "'";
                        dsEmpCount = con.FillDataSet(strsql);
                        if (dsEmpCount.Tables[0].Rows.Count > 0)
                        {
                            for (int count = 0; count < dsEmpCount.Tables[0].Rows.Count; count++)
                            {
                                EmpCodes += ",'" + dsEmpCount.Tables[0].Rows[count]["paycode"].ToString().Trim() + "'";
                            }
                            if (!string.IsNullOrEmpty(EmpCodes))
                            {
                                EmpCodes = EmpCodes.Substring(1);
                            }
                            Selection += " and tblemployee.paycode in (" + EmpCodes.ToString() + ") ";
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('No employee assigned under you.');", true);
                            return;
                        }
                    }
                    else
                    {
                        if (Session["Auth_Comp"] != null)
                        {
                            Selection += " and tblemployee.companycode in (" + Session["Auth_Comp"].ToString() + ") ";
                        }
                        if (Session["Auth_Dept"] != null)
                        {
                            Selection += " and tblemployee.Departmentcode in (" + Session["Auth_Dept"].ToString() + ") ";
                        }
                    }
                }
            }
            CompanySelection = "and tblcompany.companycode in(" + Session["Auth_Comp"].ToString() + ") " +
                          "and tbldepartment.companycode in(" + Session["Auth_Comp"].ToString() + ") " +
                          "and tblcatagory.companycode in(" + Session["Auth_Comp"].ToString() + ") " +
                          "and tbldivision.companycode in (" + Session["Auth_Comp"].ToString() + ") " +
                          "and tblGrade.companycode in(" + Session["Auth_Comp"].ToString() + ") ";
            if ((!radoutwork.Checked) && (!radManualPunch.Checked) && (!radmachinepunch.Checked))
            {
                CompanySelection += "and tblShiftmaster.companycode in(" + Session["Auth_Comp"].ToString() + ") ";
            }
            Generate();
        }
        catch
        {

        }
    }
    protected string GetAttendanceLocation(string CardNo, string PunchTime)
    {
        string Location = "NA";
        DataSet DsLoc = new DataSet();
        DateTime AttTime = System.DateTime.MinValue;
        string sSql = "";
        try
        {
            if (PunchTime.Trim() == "")
            {
                return Location;
            }
            AttTime = Convert.ToDateTime(PunchTime);
            //if (Devices.ToString().Trim() == "")
            //{
                sSql = "select UA.DeviceID,TM.branch from UserAttendance UA inner join tblMachine TM on tm.SerialNumber=UA.DeviceID where ua.UserID= " +
                       "(select cast(presentcardno as int) from tblemployee where SSN='" + CardNo + "' ) and ua.AttDateTime='" + AttTime.ToString("yyyy-MM-dd HH:mm:ss") + "'";
          //  }
            //else
            //{
            //    sSql = "select UA.DeviceID,TM.branch from UserAttendance UA inner join tblMachine TM on tm.SerialNumber=UA.DeviceID where ua.UserID= " +
            //           "(select cast(presentcardno as int) from tblemployee where PAYCODE='" + CardNo + "' ) and ua.AttDateTime='" + AttTime.ToString("yyyy-MM-dd HH:mm:ss") + "' and " +
            //           "UA.DeviceID in (" + Devices.ToString().Trim() + ")";
            //}





            DsLoc = con.FillDataSet(sSql);
            if (DsLoc.Tables[0].Rows.Count > 0)
            {
                Location = DsLoc.Tables[0].Rows[0]["branch"].ToString();
            }
            else
            {
                Location = "NA";
            }

        }
        catch
        {
            Location = "Not Found";

        }
        return Location;
    }

    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }
    protected void Generate()
    {
        Session["SelectionQuery"] = "";
        Session["SelectionQuery"] = null;
        string ReportView = "";
        string SortOrder = "";
        ReportView = ConfigurationSettings.AppSettings["ReportView_HeadID"].ToString();
        if (Session["VALIDYEARS"] == null)
        {
            Response.Redirect("Default.aspx");
        }
        try
        {
            SortOrder = ddlSorting.SelectedItem.Value.ToString().Trim();
        }
        catch
        {
            SortOrder = "Tblemployee.PayCode";
        }

       


       // Session["ExportInto"] = RadExport.SelectedIndex;
        string msg;
        if (txtDateFrom.Text.ToString().Trim().Length < 10)
        {
            msg = "<script language='javascript'> alert('Enter a valid Punch Date...')</script>";
            Page.RegisterStartupScript("msg", msg);
            txtDateFrom.Focus();
            return;
        }
        if (txtToDate.Text.ToString().Trim().Length < 10)
        {
            msg = "<script language='javascript'> alert('Enter a valid Punch Date...')</script>";
            Page.RegisterStartupScript("msg", msg);
            txtToDate.Focus();
            return;
        }
        try
        {

            if (Session["IsNepali"].ToString().Trim() == "Y")
            {
                //string FromDate1 = "", ToDate = "";
                //DateTime dtFrom = System.DateTime.MinValue;
                //DateTime dtTo = System.DateTime.MinValue;
                //int MonthTo = NC.GetNepaliMonthIndex(ddlMonthTo.SelectedItem.Text.ToString());
                //int MonthFrom = NC.GetNepaliMonthIndex(ddlMonthFrom.SelectedItem.Text.ToString());
                //FromDate1 = ddlYearFrom.SelectedValue + "/" + MonthFrom + "/" + ddlDateFrom.SelectedValue;
                //ToDate = ddlYearTo.SelectedValue + "/" + MonthTo + "/" + ddlDateTo.SelectedValue;
                //dtFrom = Convert.ToDateTime(FromDate1);
                //dtTo = Convert.ToDateTime(ToDate);
                //FromDateNepal = dtFrom;
                //toDateNepal = dtTo;
                //dtFrom = NC.ToAD(dtFrom);
                //dtTo = NC.ToAD(dtTo);
                //FromDate = dtFrom;
                //toDate = dtTo;


            }

            else
            {
                //FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                //toDate = DateTime.ParseExact(txtToDate.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                FromDate = Convert.ToDateTime(txtDateFrom.Value);
                toDate = Convert.ToDateTime(txtToDate.Value);
            }



        }
        catch
        {
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('Invalid Date Format');", true);
            return;
        }
        int ii = Convert.ToInt16(Session["VALIDYEARS"].ToString().IndexOf(FromDate.Year.ToString("0000")));
        int j = Convert.ToInt16(Session["VALIDYEARS"].ToString().IndexOf(toDate.Year.ToString("0000")));
        if (ii < 0)
        {
            msg = "<script lanaguage='javascript'>alert('Invalid year. Roster was not created for the specified Year...')</script>";
            Page.RegisterStartupScript("msg", msg);
            txtDateFrom.Focus();
            return;
        }
        if (j < 0)
        {
            msg = "<script lanaguage='javascript'>alert('Invalid year. Roster was not created for the specified Year...')</script>";
            Page.RegisterStartupScript("msg", msg);
            txtToDate.Focus();
            return;
        }
        UpdateTimeRegister();
        OpenPopupPage = "";
        if (toDate < FromDate)
        {
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('To Date must be greater than From Date');", true);
            return;
        }
        DataSet dsEmpCount = new DataSet();
        string EmpCodes = "", CompanySelection = "";
        TimeSpan ts = toDate.Subtract(FromDate);
        int day = ts.Days;
        //if (day > 30)
        //{ 
        //    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('Difference is more than one Month');", true);
        //    return; 
        //}
        GetSelection();
        Selection = "";
       
        if (Session["SelectionQuery"] != null)
        {
            Selection = Session["SelectionQuery"].ToString();
        }
        else
        {
            if (Session["usertype"].ToString().Trim() == "A")
            {
                if (Session["Auth_Comp"] != null)
                {
                    Selection += " and tblemployee.companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
                }
                if (Session["Auth_Dept"] != null)
                {
                    Selection += " and tblemployee.Departmentcode in (" + Session["Auth_Dept"].ToString().Trim() + ") ";
                }
            }
            if (Session["usertype"].ToString().Trim() == "H")
            {
                if (ReportView.ToString().Trim() == "Y")
                {
                    strsql = "select paycode from tblemployee where headid='" + Session["PAYCODE"].ToString().Trim() + "'";
                    dsEmpCount = con.FillDataSet(strsql);
                    if (dsEmpCount.Tables[0].Rows.Count > 0)
                    {
                        for (int count = 0; count < dsEmpCount.Tables[0].Rows.Count; count++)
                        {
                            EmpCodes += ",'" + dsEmpCount.Tables[0].Rows[count]["paycode"].ToString().Trim() + "'";
                        }
                        if (!string.IsNullOrEmpty(EmpCodes))
                        {
                            EmpCodes = EmpCodes.Substring(1);
                        }
                        Selection += " and tblemployee.paycode in (" + EmpCodes.ToString() + ") ";
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('No employee assigned under you.');", true);
                        return;
                    }
                }
                else
                {
                    if (Session["Auth_Comp"] != null)
                    {
                        Selection += " and tblemployee.companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
                    }
                    if (Session["Auth_Dept"] != null)
                    {
                        Selection += " and tblemployee.Departmentcode in (" + Session["Auth_Dept"].ToString().Trim() + ") ";
                    }
                }
            }
        }

        CompanySelection = "and tblcompany.companycode in(" + Session["Auth_Comp"].ToString().Trim() + ") " +
                            "and tbldepartment.companycode in(" + Session["Auth_Comp"].ToString().Trim() + ") " +
                            "and tblcatagory.companycode in(" + Session["Auth_Comp"].ToString().Trim() + ") " +
                            "and tbldivision.companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") " +
                            "and tblGrade.companycode in(" + Session["Auth_Comp"].ToString().Trim() + ") ";
        if ((!radoutwork.Checked) && (!radManualPunch.Checked) && (!radmachinepunch.Checked) && (!radDepartment.Checked))
        {
            CompanySelection += "and tblShiftmaster.companycode in(" + Session["Auth_Comp"].ToString().Trim() + ") ";
        }

        try
        {



            if (radManualPunch.Checked)
            {
                strsql = " select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblemployee.Paycode,tblemployee.EmpName,tblemployee.PresentCardno,CONVERT(VARCHAR(20), machinerawpunch.OfficePunch, 100)   'OfficePunch' " +
                          " from tblemployee join tblcompany  on tblemployee.companycode=tblcompany.companycode join " +
                          " tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN join tbldepartment on " +
                          " tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                          " join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                          " join machinerawpunch machinerawpunch on tblemployee.SSN=machinerawpunch.SSN  " +
                          " join tblgrade on tblemployee.gradecode=tblgrade.gradecode " +
                          " where (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "'  or tblemployee.LeavingDate is null) AND  1=1  and  1=1  " +
                          " and machinerawpunch.ismanual='Y' and (machinerawpunch.paycode is not null or machinerawpunch.paycode!='')  " +
                          " and convert(varchar(10),machinerawpunch.officepunch,120)='" + FromDate.ToString("yyyy-MM-dd") + "' ";

                if (Selection.ToString() != "")
                {
                    strsql += " " + Selection.ToString() + " ";
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " order by " + SortOrder.Trim() + " ";
                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (radExcel.Checked)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("PDate");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "Manual Punch Time";

                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][8].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][10].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][9].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][11].ToString();

                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                 strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            //msg = "Manual Punch Report on " + FromDate.ToString("dd-MM-yyyy") + " ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {

                                msg = "Manual Punch Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Manual Punch Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            }

                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if (((i == 3) || (i == 4)) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=ManualPunch.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/ManualPunch.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("PDate");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "Manual Punch Time";

                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][8].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][10].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][9].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][11].ToString();

                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            //msg = "Manual Punch Report on " + FromDate.ToString("dd-MM-yyyy") + " ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {

                                msg = "Manual Punch Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Manual Punch Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            }

                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if (((i == 3) || (i == 4)) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "Manual.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('No record found');", true);
                    return;
                }
            }
            if (radhris.Checked)
            {
                try
                {
                    strsql = " Select UserAttendance.UserID,UserAttendance.DeviceID,UserAttendance.AttDateTime,UserAttendance.MaskStatus,UserAttendance.IsAbnomal,UserAttendance.CTemp,UserAttendance.FTemp,isnull(tblemployee.EMPNAME,'Not Registered') 'Name' From UserAttendance WITH (NOLOCK) Left join tblemployee on tblemployee.paycode= UserAttendance.UserID  " +
                                    " Where convert(varchar(10),AttDateTime,120) between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and  UserAttendance.deviceid in (Select Serialnumber from tblmachine where companycode='" + Session["LoginCompany"].ToString().Trim() + "') and  UserAttendance.UserID!='VISITOR' and (FTemp!='' or CTemp!='') ";
                    ds = con.FillDataSet(strsql);
                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("User Id");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Device ID");
                            dTbl.Columns.Add("AttDateTime");
                            dTbl.Columns.Add("Mask Status");
                            dTbl.Columns.Add("Is Abnormal");
                            dTbl.Columns.Add("Temprature In Celsius");
                            dTbl.Columns.Add("Temprature In Fahrenheit");




                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "User Id";
                            dTbl.Rows[0][2] = "Name";
                            dTbl.Rows[0][3] = "Device ID";
                            dTbl.Rows[0][4] = "Att Date Time";
                            dTbl.Rows[0][5] = "Mask Status";
                            dTbl.Rows[0][6] = "Is Abnormal";
                            dTbl.Rows[0][7] = "Temprature In Celsius";
                            dTbl.Rows[0][8] = "Temprature In Fahrenheit";




                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1]["UserID"].ToString().Trim();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1]["Name"].ToString().Trim();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1]["DeviceID"].ToString().Trim();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1]["AttDateTime"].ToString().Trim();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1]["MaskStatus"].ToString().Trim();
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1]["IsAbnomal"].ToString().Trim();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1]["CTemp"].ToString().Trim();
                                    double Ftemp = 0;
                                    try
                                    {
                                        Ftemp = (Convert.ToDouble(ds.Tables[0].Rows[i1]["CTemp"].ToString().Trim()) * 9) / 5 + 32; ;
                                    }
                                    catch
                                    {
                                        Ftemp = 0;
                                    }
                                    dTbl.Rows[ct][8] = Ftemp.ToString().Trim(); //ds.Tables[0].Rows[i1]["FTemp"].ToString().Trim();
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {

                                msg = "Temprature Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Temprature Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            }





                            //msg = "Machine Punch Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if (((i == 3) || (i == 4)) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:120px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();

                            if (radExcel.Checked)
                            {
                                Response.Clear();
                                Response.AddHeader("content-disposition", "attachment;filename=Temprature.xls");
                                Response.Charset = "";
                                Response.Cache.SetCacheability(HttpCacheability.Private);
                                Response.ContentType = "application/Temprature.xls";
                                System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                                System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                                Response.Write(sb.ToString());
                                Response.End();
                            }
                            else
                            {
                                Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                                string pdffilename = "Temprature.pdf";
                                PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                                pdfDocument.Open();
                                String htmlText = sb.ToString();
                                StringReader str = new StringReader(htmlText);
                                HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                                htmlworker.Parse(str);
                                pdfWriter.CloseStream = false;
                                pdfDocument.Close();
                                //Download Pdf  
                                Response.Buffer = true;
                                Response.ContentType = "application/pdf";
                                Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                                Response.Write(pdfDocument);
                                Response.Flush();
                                Response.End();
                            }

                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('No record found');", true);
                        return;
                    }
                }
                catch (Exception eX)
                {

                    Error_Occured("HRIS", eX.Message);
                }
            }

            ///Phone Excel///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ///
            if(radEditLog.Checked)
            {
                strsql = "Select * From UserEditLog Where convert(varchar(10),LogDate,120) = '" + FromDate.ToString("yyyy-MM-dd") + "' and  CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"' ";
                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {

                    try
                    {
                        string Tmp = "";
                        int x, i;
                        System.Data.DataTable dTbl = new System.Data.DataTable();
                        DataRow dRow;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);

                        dTbl.Columns.Add("SNo");
                        dTbl.Columns.Add("Log Date");
                        dTbl.Columns.Add("User Name");
                        dTbl.Columns.Add("Log Details");
                        dTbl.Columns.Add("Old Value");
                        dTbl.Columns.Add("New Value");

                        dTbl.Rows[0][0] = "SNo";
                        dTbl.Rows[0][1] = "Log Date";
                        dTbl.Rows[0][2] = "User Name";
                        dTbl.Rows[0][3] = "Log Details";
                        dTbl.Rows[0][4] = "Old Value";
                        dTbl.Rows[0][5] = "New Value";

                        int ct = 1;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                            {

                                dTbl.Rows[ct][0] = ct.ToString();
                                dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1]["LogDate"].ToString();
                                dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1]["UserName"].ToString();
                                dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1]["LogDetails"].ToString();
                                dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1]["OldValue"].ToString();
                                dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1]["NewValue"].ToString();

                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                ct = ct + 1;
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        bool isEmpty;
                        for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                        {

                            isEmpty = true;
                            for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                            {
                                if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                {
                                    isEmpty = false;
                                    break;
                                }
                            }

                            if (isEmpty == true)
                            {
                                dTbl.Rows.RemoveAt(i2);
                                i2--;
                            }
                        }
                        string companyname = "";
                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }

                        int colCount = Convert.ToInt32(dTbl.Columns.Count);

                        if (Session["IsNepali"].ToString().Trim() == "Y")
                        {

                            msg = "Edit Log Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                        }
                        else
                        {
                            msg = "Edit Log Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                        }





                        //msg = "Machine Punch Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                        x = colCount;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                    if (Tmp.Substring(0, 1) == "0")
                                    {
                                        if (Tmp.Contains(":"))
                                        {
                                            Tmp = Tmp;
                                        }
                                        else
                                        {
                                            Tmp = Tmp;
                                        }
                                    }
                                }
                                if ((i == 1) || (i == 2))
                                {
                                    if (Tmp.ToString().Trim() != "")
                                    {
                                        //if (Tmp.ToString().Substring(0, 1) == "0")
                                        // {                                              
                                        sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        //}
                                        //else
                                        //{
                                        //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                        // }
                                        // }
                                    }
                                }
                                else if (((i == 3) || (i == 4)) && (Tmp.ToString().Trim() != ""))
                                {
                                    sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                else
                                    sb.Append("<td style='width:120px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();

                        if (radExcel.Checked)
                        {
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=EditLog.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/EditLog.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        else
                        {
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "EditLog.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                       
                    }
                    catch (Exception ex)
                    {
                        //Response.Write(ex.Message.ToString());
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('No record found');", true);
                    return;
                }
            }
          

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            if (radmachinepunch.Checked)
            {
                strsql = " select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblemployee.Paycode,tblemployee.EmpName,tblemployee.PresentCardno,CONVERT(VARCHAR(20), machinerawpunch.OfficePunch, 100)   'OfficePunch',tblmachine.branch " +
                          " from tblemployee join tblcompany  on tblemployee.companycode=tblcompany.companycode join " +
                          " tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN join tbldepartment on " +
                          " tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                          " join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                          " join machinerawpunch machinerawpunch on tblemployee.SSN=machinerawpunch.SSN  " +
                          " join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblmachine on cast(machinerawpunch.MC_no as int)= cast(tblmachine.id_no as int) " +
                          " where (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "'  or tblemployee.LeavingDate is null) AND  1=1  and  1=1  " +
                          " and (machinerawpunch.ismanual='N'or machinerawpunch.ismanual is null) and (machinerawpunch.paycode is not null or machinerawpunch.paycode!='')  " +
                          " and convert(varchar(10),machinerawpunch.officepunch,120) = '" + FromDate.ToString("yyyy-MM-dd") + "' ";

                if (Selection.ToString() != "")
                {
                    strsql += " " + Selection.ToString() + " ";
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " order by " + SortOrder.Trim() + " ";
                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {

                    try
                    {
                        string Tmp = "";
                        int x, i;
                        System.Data.DataTable dTbl = new System.Data.DataTable();
                        DataRow dRow;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);

                        dTbl.Columns.Add("SNo");
                        dTbl.Columns.Add("PayCode");
                        dTbl.Columns.Add("CardNo");
                        dTbl.Columns.Add("Name");
                        dTbl.Columns.Add("PDate");
                        dTbl.Columns.Add("Location");

                        dTbl.Rows[0][0] = "SNo";
                        dTbl.Rows[0][1] = "PayCode";
                        dTbl.Rows[0][2] = "Card No";
                        dTbl.Rows[0][3] = "Employee Name";
                        dTbl.Rows[0][4] = "Punch Time";
                        dTbl.Rows[0][5] = "Location";

                        int ct = 1;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                            {

                                dTbl.Rows[ct][0] = ct.ToString();
                                dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][8].ToString();
                                dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][10].ToString();
                                dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][9].ToString();
                                dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][11].ToString();
                                dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][12].ToString();

                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                ct = ct + 1;
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        bool isEmpty;
                        for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                        {

                            isEmpty = true;
                            for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                            {
                                if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                {
                                    isEmpty = false;
                                    break;
                                }
                            }

                            if (isEmpty == true)
                            {
                                dTbl.Rows.RemoveAt(i2);
                                i2--;
                            }
                        }
                        string companyname = "";
                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                             strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }

                        int colCount = Convert.ToInt32(dTbl.Columns.Count);

                        if (Session["IsNepali"].ToString().Trim() == "Y")
                        {

                            msg = "Machine Punch Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                        }
                        else
                        {
                            msg = "Machine Punch Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                        }





                        //msg = "Machine Punch Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                        x = colCount;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                    if (Tmp.Substring(0, 1) == "0")
                                    {
                                        if (Tmp.Contains(":"))
                                        {
                                            Tmp = Tmp;
                                        }
                                        else
                                        {
                                            Tmp = Tmp;
                                        }
                                    }
                                }
                                if ((i == 1) || (i == 2))
                                {
                                    if (Tmp.ToString().Trim() != "")
                                    {
                                        //if (Tmp.ToString().Substring(0, 1) == "0")
                                        // {                                              
                                        sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        //}
                                        //else
                                        //{
                                        //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                        // }
                                        // }
                                    }
                                }
                                else if (((i == 3) || (i == 4)) && (Tmp.ToString().Trim() != ""))
                                {
                                    sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                else
                                    sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();


                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=MachinePunch.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/MachinePunch.xls";
                        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                        Response.Write(sb.ToString());
                        Response.End();
                    }
                    catch (Exception ex)
                    {
                        //Response.Write(ex.Message.ToString());
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('No record found');", true);
                    return;
                }
            }
            if (radLateArrival.Checked)
            {

                strsql = " select tblemployee.paycode 'PayCode',tblemployee.Bus 'Bus',tblemployee.empname 'Name',tblemployee.presentcardno 'Card Number', " +
                        " tblcompany.companyname as 'CompanyName',tbltimeregister.shiftattended 'Shift', " +
                        " tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                        " Convert(varchar(5),tbltimeregister.shiftstarttime,108) 'Shift Start',convert(varchar(12),tbltimeregister.dateoffice,103) 'ForDate' ," +
                        " convert(char(5),tbltimeregister.in1,108) 'In' ," +
                        " substring(convert(varchar(20),dateadd(minute,tbltimeregister.latearrival,0),108),0,6) 'shift late' ," +
                        " case when latearrival between 0 and 10 then '**' else '' end as '>(0.01)', " +
                        " case when latearrival between 11 and 30 then '**' else '' end as '>(0.10)', " +
                        " case when latearrival between 31 and 60 then '**' else '' end as '>(0.30)', " +
                        " case when latearrival > 60 then '**' else '' end as '>(0.60)' " +
                        " from tblemployee join tblcompany  on tblemployee.companycode=tblcompany.companycode join tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN " +
                        " join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                        " join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                        " join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                        " join tblgrade on tblemployee.gradecode=tblgrade.gradecode  " +
                        " join tbltimeregister  on tblemployee.SSN=tbltimeregister.SSN join tblshiftmaster on tbltimeregister.Shift= tblshiftmaster.Shift where tbltimeregister.dateoffice='" + FromDate.ToString("yyyy-MM-dd") + "' " +
                        " And  (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "'  or tblemployee.LeavingDate is null) AND  1=1  and  1=1 " +
                        " and tbltimeregister.latearrival > 0  ";
                if (Selection.ToString() != "")
                {
                    strsql += " " + Selection.ToString() + " ";
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " order by " + SortOrder.Trim() + " ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (radExcel.Checked)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Shift");
                            dTbl.Columns.Add("Start");
                            dTbl.Columns.Add("IN");
                            dTbl.Columns.Add("ShiftLate");
                            dTbl.Columns.Add(">(0.01)");
                            dTbl.Columns.Add(">(0.10)");
                            dTbl.Columns.Add(">(0.30)");
                            dTbl.Columns.Add(">(0.60)");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "Shift";
                            dTbl.Rows[0][5] = "Start";
                            dTbl.Rows[0][6] = "IN";
                            dTbl.Rows[0][7] = "Shift Late";
                            dTbl.Rows[0][8] = ">(0.01)";
                            dTbl.Rows[0][9] = ">(0.10)";
                            dTbl.Rows[0][10] = ">(0.30)";
                            dTbl.Rows[0][11] = ">(0.60)";


                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][3].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][5].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][10].ToString();
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][12].ToString();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][13].ToString();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][14].ToString();
                                    dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1][15].ToString();
                                    dTbl.Rows[ct][10] = ds.Tables[0].Rows[i1][16].ToString();
                                    dTbl.Rows[ct][11] = ds.Tables[0].Rows[i1][17].ToString();



                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                 strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            // msg = "Late Arrival Report on " + FromDate.ToString("dd-MM-yyyy") + " ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {

                                msg = "Late Arrival Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Late Arrival Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            }



                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=LateArrival.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/LateArrival.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    if (radPDF.Checked)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Shift");
                            dTbl.Columns.Add("Start");
                            dTbl.Columns.Add("IN");
                            dTbl.Columns.Add("ShiftLate");
                            dTbl.Columns.Add(">(0.01)");
                            dTbl.Columns.Add(">(0.10)");
                            dTbl.Columns.Add(">(0.30)");
                            dTbl.Columns.Add(">(0.60)");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][0] = "PayCode";
                            dTbl.Rows[0][1] = "Card No";
                            dTbl.Rows[0][2] = "Name";
                            dTbl.Rows[0][4] = "Shift";
                            dTbl.Rows[0][5] = "Start";
                            dTbl.Rows[0][6] = "IN";
                            dTbl.Rows[0][7] = "Shift Late";
                            dTbl.Rows[0][8] = ">(0.01)";
                            dTbl.Rows[0][9] = ">(0.10)";
                            dTbl.Rows[0][10] = ">(0.30)";
                            dTbl.Rows[0][11] = ">(0.60)";


                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][3].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][5].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][10].ToString();
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][12].ToString();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][13].ToString();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][14].ToString();
                                    dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1][15].ToString();
                                    dTbl.Rows[ct][10] = ds.Tables[0].Rows[i1][16].ToString();
                                    dTbl.Rows[ct][11] = ds.Tables[0].Rows[i1][17].ToString();



                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            // msg = "Late Arrival Report on " + FromDate.ToString("dd-MM-yyyy") + " ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {

                                msg = "Late Arrival Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Late Arrival Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            }



                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();

                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "Late.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    //Response.Redirect("Reports/DailyReports/Rpt_LateArrival.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('No record found');", true);
                    return;
                }
            }
            //Daily Performance
            else if (radDailyPerformance.Checked)
            {
                string in1manual = "", in2manual = "", out1manual = "", Out2manual = "",INLocation="NA",OutLocation="NA";;
                /*strsql = "select tblemployee.paycode 'PayCode',tblemployee.empname 'Name','HOD'=(select empname from tblemployee tblemp where tblemp.paycode=tblemployee.headid), " +
                        " tblcompany.companyname as 'CompanyName',tbltimeregister.shiftattended 'Shift',Convert(varchar(5),tbltimeregister.shiftstarttime,108) 'Shift Start', " +
                        " tbldepartment.DEPARTMENTName 'departmentName',tblcatagory.CATagoryname 'CatagoryName',tblgrade.gradename 'GradeName', " +
                        " Convert(varchar(5),tbltimeregister.shiftendtime,108) 'Shift End',  " +
                        " tbltimeregister.status 'Status' ," +
                        " Convert(varchar(5),tbltimeregister.IN1,108) 'In', " +
                        " Convert(varchar(5),tbltimeregister.Out2,108) 'Out', " +
                        " case when tbltimeregister.hoursworked=0 then null else substring(convert(varchar(20),dateadd(minute,tbltimeregister.hoursworked,0),108),0,6) end 'Hours Worked' , " +
                        " convert(varchar(12),tbltimeregister.dateoffice,103) 'ForDate',tblemployee.companycode,Convert(varchar(5),dateadd(minute,tbltimeregister.latearrival,0),108) 'latearrival',Convert(varchar(5),dateadd(minute,tbltimeregister.earlydeparture,0),108) 'shift_early' " +
                        " from tblemployee join tblcompany on tblemployee.companycode=tblcompany.companycode join tblemployeeshiftmaster on tblemployee .paycode=tblemployeeshiftmaster.paycode " +
                        " join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                        " join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                        " join tblgrade on tblemployee.gradecode=tblgrade.gradecode " +
                        " join tbltimeregister on tblemployee.paycode=tbltimeregister.paycode where tbltimeregister.dateoffice='" + FromDate.ToString("yyyy-MM-dd") + "' " +
                        " And  (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "'  or tblemployee.LeavingDate is null) AND 1=1  and  1=1 ";
                        if (Selection.ToString() != "")
                        {
                            strsql += " " + Selection.ToString() + " ";
                        }
                        strsql += " order by " + SortOrder.Trim() + " ";                   */
                /////////////
                SortOrder = "tblemployee.paycode";
                strsql = "select distinct tblemployee.paycode ,tblemployee.DESIGNATION,tblemployee.SSN, tblCatagory.CatagoryName, tblDivision.DivisionName,tblTimeRegister.Shift, tblEmployee.Divisioncode, " +
                    "tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,tbltimeregister.PayCode,tblEmployee.PresentCardNo 'Card Number'," +
                    "tblEmployee.EmpName 'Name',tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.ShiftAttended, " +
                "Convert(varchar(5),tbltimeregister.ShiftStartTime,108) 'Start'," +
                "Convert(varchar(5),tbltimeregister.In1,108) 'In'," +
                "Convert(varchar(5),tbltimeregister.Out1,108) 'lunch Start Time'," +
                "Convert(varchar(5),tbltimeregister.In2,108) 'lunch End Time'," +
                "Convert(varchar(5),tbltimeregister.Out2,108) 'Out'," +
                " case when tbltimeregister.hoursworked=0 then null else substring(convert(varchar(20),dateadd(minute,tbltimeregister.hoursworked,0),108),0,6) end 'Hours Worked' ,  " +
                "tbltimeregister.Status," +
                "convert(varchar(12),tbltimeregister.dateoffice,103) 'ForDate'," +
                "Convert(varchar(5),dateadd(minute,tbltimeregister.latearrival,0),108) 'shift late'," +
                "Convert(varchar(5),dateadd(minute,tbltimeregister.earlydeparture,0),108) 'shift early',  " +
                "Convert(varchar(5),dateadd(minute,tbltimeregister.EarlyArrival,0),108) 'Early arrival', " +
                "Convert(varchar(5),dateadd(minute,tbltimeregister.ExcLunchHours,0),108) 'Excess of lunch hours'  " +
                ",Convert(varchar(5),dateadd(minute,tbltimeregister.OtDuration,0),108) 'OT' ,tbltimeregister.OtAmount 'OTAmount',tbltimeregister.OsDuration 'Over Stay', " +
                "tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual from tblCatagory," +
                "tblDivision,tbltimeregister,tblEmployee,tblCompany,tblDepartment,tblgrade,tblshiftmaster Where tblGrade.GradeCode = tblEmployee.GradeCode And " +
                "tblCatagory.Cat = tblEmployee.Cat  And  " +
                "tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregister.SSN = tblEmployee.SSN And " +
                "tbltimeregister.DateOffice = '" + FromDate.ToString("yyyy-MM-dd") + "' And tblEmployee.CompanyCode = tblCompany.CompanyCode And " +
                "tblEmployee.DepartmentCode = tblDepartment.DepartmentCode And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "'  or " +
                "tblemployee.LeavingDate is null) AND  1=1  and  1=1 ";
                if (Selection.ToString() != "")
                {
                    strsql += " " + Selection.ToString() + " ";
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " order by " + SortOrder.Trim() + " ";
                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (radExcel.Checked)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            //// create table 
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            //dTbl.Columns.Add("Paycode");
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("Department");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Designation");
                            dTbl.Columns.Add("Category");
                            dTbl.Columns.Add("Shift");
                            dTbl.Columns.Add("Start");
                            dTbl.Columns.Add("IN");
                            dTbl.Columns.Add("LunchIn");
                            dTbl.Columns.Add("LunchOut");
                            dTbl.Columns.Add("OUT");
                            dTbl.Columns.Add("HoursWorked");
                            dTbl.Columns.Add("Status");
                            dTbl.Columns.Add("EarlyArrival");
                            dTbl.Columns.Add("ShiftLate");
                            dTbl.Columns.Add("ShiftEarly");
                            dTbl.Columns.Add("OT");
                            dTbl.Columns.Add("OTAmount");
                            dTbl.Columns.Add("OverStay");
                            dTbl.Columns.Add("Manual");
                            //dTbl.Columns.Add("In Location");
                            //dTbl.Columns.Add("Out Location");

                            dTbl.Rows[0][0] = "PayCode";
                            dTbl.Rows[0][1] = "Department";
                            dTbl.Rows[0][2] = "Name";
                            dTbl.Rows[0][3] = "Designation";
                            dTbl.Rows[0][4] = "Category";
                            dTbl.Rows[0][5] = "Shift";
                            dTbl.Rows[0][6] = "Start";
                            dTbl.Rows[0][7] = "IN";
                            dTbl.Rows[0][8] = "Lunch In";
                            dTbl.Rows[0][9] = "Lunch Out";
                            dTbl.Rows[0][10] = "OUT";
                            dTbl.Rows[0][11] = "Hours Worked";
                            dTbl.Rows[0][12] = "Status";
                            dTbl.Rows[0][13] = "Early Arrival";
                            dTbl.Rows[0][14] = "Shift Late";
                            dTbl.Rows[0][15] = "Shift Early";
                            dTbl.Rows[0][16] = "OT";
                            dTbl.Rows[0][17] = "OT Amount";
                            dTbl.Rows[0][18] = "OverStay";
                            dTbl.Rows[0][19] = "Manual";
                            //dTbl.Rows[0][20] = "In Location";
                            //dTbl.Rows[0][21] = "Out Location";



                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {


                                    //INLocation = GetAttendanceLocation(ds.Tables[0].Rows[i1]["SSN"].ToString().Trim(), ds.Tables[0].Rows[i1]["In"].ToString());
                                    //OutLocation = GetAttendanceLocation(ds.Tables[0].Rows[i1]["SSN"].ToString().Trim(), ds.Tables[0].Rows[i1]["Out"].ToString());
                                    //if (INLocation == "NA" && OutLocation == "NA")
                                    //{
                                    // //   goto NextRecord;
                                    //}
                                    dTbl.Rows[ct][0] = ds.Tables[0].Rows[i1]["paycode"].ToString().Trim();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1]["DepartmentName"].ToString().Trim();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1]["Name"].ToString().Trim();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1]["designation"].ToString().Trim();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1]["CatagoryName"].ToString().Trim();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1]["ShiftAttended"].ToString().Trim();
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1]["Start"].ToString().Trim();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1]["In"].ToString().Trim();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1]["lunch Start Time"].ToString().Trim();
                                    dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1]["lunch End Time"].ToString().Trim();
                                    dTbl.Rows[ct][10] = ds.Tables[0].Rows[i1]["Out"].ToString().Trim();
                                    dTbl.Rows[ct][11] = ds.Tables[0].Rows[i1]["Hours Worked"].ToString().Trim();
                                    dTbl.Rows[ct][12] = ds.Tables[0].Rows[i1]["Status"].ToString().Trim();
                                    dTbl.Rows[ct][13] = ds.Tables[0].Rows[i1]["Early arrival"].ToString().Trim();
                                    dTbl.Rows[ct][14] = ds.Tables[0].Rows[i1]["shift late"].ToString().Trim();
                                    dTbl.Rows[ct][15] = ds.Tables[0].Rows[i1]["shift early"].ToString().Trim();
                                    dTbl.Rows[ct][16] = ds.Tables[0].Rows[i1]["OT"].ToString().Trim();
                                    dTbl.Rows[ct][17] = ds.Tables[0].Rows[i1]["OTAmount"].ToString().Trim();
                                    dTbl.Rows[ct][18] = ds.Tables[0].Rows[i1]["Over Stay"].ToString().Trim();
                                    if ((ds.Tables[0].Rows[i1]["In1Mannual"].ToString() == "Y") || (ds.Tables[0].Rows[i1]["In2Mannual"].ToString() == "Y") || (ds.Tables[0].Rows[i1]["Out1Mannual"].ToString() == "Y") || (ds.Tables[0].Rows[i1]["Out2Mannual"].ToString() == "Y"))
                                    {
                                        dTbl.Rows[ct][19] = "Y";
                                    }
                                    else
                                    {
                                        dTbl.Rows[ct][19] = "N";
                                    }
                                    //dTbl.Rows[ct][20] = INLocation.Trim();
                                    //dTbl.Rows[ct][21] = OutLocation.Trim();
                                NextRecord:

                                    //dTbl.Rows[ct][0] = ds.Tables[0].Rows[i1]["paycode"].ToString();
                                    //dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][9].ToString();
                                    //dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][10].ToString();
                                    //dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][13].ToString();
                                    //dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][14].ToString();
                                    //dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][15].ToString(); 
                                    //dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][16].ToString();
                                    //dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][17].ToString();
                                    //dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][18].ToString();
                                    //dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1][19].ToString();
                                    //dTbl.Rows[ct][10] = ds.Tables[0].Rows[i1][20].ToString();
                                    //dTbl.Rows[ct][11] = ds.Tables[0].Rows[i1][24].ToString();
                                    //dTbl.Rows[ct][12] = ds.Tables[0].Rows[i1][22].ToString();
                                    //dTbl.Rows[ct][13] = ds.Tables[0].Rows[i1][23].ToString();
                                    //dTbl.Rows[ct][14] = ds.Tables[0].Rows[i1][26].ToString();
                                    //dTbl.Rows[ct][15] = ds.Tables[0].Rows[i1][27].ToString();
                                    //dTbl.Rows[ct][16] = ds.Tables[0].Rows[i1][28].ToString();
                                    //if ((ds.Tables[0].Rows[i1][29].ToString() == "Y") || (ds.Tables[0].Rows[i1][30].ToString() == "Y") || (ds.Tables[0].Rows[i1][31].ToString() == "Y") || (ds.Tables[0].Rows[i1][32].ToString() == "Y"))
                                    //{
                                    //    dTbl.Rows[ct][17] = "Y";
                                    //}
                                    //else
                                    //{
                                    //    dTbl.Rows[ct][17] = "N";
                                    //}
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);
                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {

                                msg = "Daily Performance Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Daily Performance Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            }



                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 0) || (i == 1))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:100px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 2) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:100px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=DailyPerformance.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/DailyPerformance.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    if(radPDF.Checked)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            //// create table 
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            //dTbl.Columns.Add("Paycode");
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("Department");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Designation");
                            dTbl.Columns.Add("Category");
                            dTbl.Columns.Add("Shift");
                            dTbl.Columns.Add("Start");
                            dTbl.Columns.Add("IN");
                            dTbl.Columns.Add("LunchIn");
                            dTbl.Columns.Add("LunchOut");
                            dTbl.Columns.Add("OUT");
                            dTbl.Columns.Add("HoursWorked");
                            dTbl.Columns.Add("Status");
                            dTbl.Columns.Add("EarlyArrival");
                            dTbl.Columns.Add("ShiftLate");
                            dTbl.Columns.Add("ShiftEarly");
                            dTbl.Columns.Add("OT");
                            dTbl.Columns.Add("OTAmount");
                            dTbl.Columns.Add("OverStay");
                            dTbl.Columns.Add("Manual");
                            dTbl.Columns.Add("In Location");
                            dTbl.Columns.Add("Out Location");

                            dTbl.Rows[0][0] = "PayCode";
                            dTbl.Rows[0][1] = "Department";
                            dTbl.Rows[0][2] = "Name";
                            dTbl.Rows[0][3] = "Designation";
                            dTbl.Rows[0][4] = "Category";
                            dTbl.Rows[0][5] = "Shift";
                            dTbl.Rows[0][6] = "Start";
                            dTbl.Rows[0][7] = "IN";
                            dTbl.Rows[0][8] = "Lunch In";
                            dTbl.Rows[0][9] = "Lunch Out";
                            dTbl.Rows[0][10] = "OUT";
                            dTbl.Rows[0][11] = "Hours Worked";
                            dTbl.Rows[0][12] = "Status";
                            dTbl.Rows[0][13] = "Early Arrival";
                            dTbl.Rows[0][14] = "Shift Late";
                            dTbl.Rows[0][15] = "Shift Early";
                            dTbl.Rows[0][16] = "OT";
                            dTbl.Rows[0][17] = "OT Amount";
                            dTbl.Rows[0][18] = "OverStay";
                            dTbl.Rows[0][19] = "Manual";
                            dTbl.Rows[0][20] = "In Location";
                            dTbl.Rows[0][21] = "Out Location";



                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {


                                    INLocation = GetAttendanceLocation(ds.Tables[0].Rows[i1]["SSN"].ToString().Trim(), ds.Tables[0].Rows[i1]["In"].ToString());
                                    OutLocation = GetAttendanceLocation(ds.Tables[0].Rows[i1]["SSN"].ToString().Trim(), ds.Tables[0].Rows[i1]["Out"].ToString());
                                    //if (INLocation == "NA" && OutLocation == "NA")
                                    //{
                                    // //   goto NextRecord;
                                    //}
                                    dTbl.Rows[ct][0] = ds.Tables[0].Rows[i1]["paycode"].ToString().Trim();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1]["DepartmentName"].ToString().Trim();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1]["Name"].ToString().Trim();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1]["designation"].ToString().Trim();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1]["CatagoryName"].ToString().Trim();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1]["Shift"].ToString().Trim();
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1]["Start"].ToString().Trim();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1]["In"].ToString().Trim();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1]["lunch Start Time"].ToString().Trim();
                                    dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1]["lunch End Time"].ToString().Trim();
                                    dTbl.Rows[ct][10] = ds.Tables[0].Rows[i1]["Out"].ToString().Trim();
                                    dTbl.Rows[ct][11] = ds.Tables[0].Rows[i1]["Hours Worked"].ToString().Trim();
                                    dTbl.Rows[ct][12] = ds.Tables[0].Rows[i1]["Status"].ToString().Trim();
                                    dTbl.Rows[ct][13] = ds.Tables[0].Rows[i1]["Early arrival"].ToString().Trim();
                                    dTbl.Rows[ct][14] = ds.Tables[0].Rows[i1]["shift late"].ToString().Trim();
                                    dTbl.Rows[ct][15] = ds.Tables[0].Rows[i1]["shift early"].ToString().Trim();
                                    dTbl.Rows[ct][16] = ds.Tables[0].Rows[i1]["OT"].ToString().Trim();
                                    dTbl.Rows[ct][17] = ds.Tables[0].Rows[i1]["OTAmount"].ToString().Trim();
                                    dTbl.Rows[ct][18] = ds.Tables[0].Rows[i1]["Over Stay"].ToString().Trim();
                                    if ((ds.Tables[0].Rows[i1]["In1Mannual"].ToString() == "Y") || (ds.Tables[0].Rows[i1]["In2Mannual"].ToString() == "Y") || (ds.Tables[0].Rows[i1]["Out1Mannual"].ToString() == "Y") || (ds.Tables[0].Rows[i1]["Out2Mannual"].ToString() == "Y"))
                                    {
                                        dTbl.Rows[ct][19] = "Y";
                                    }
                                    else
                                    {
                                        dTbl.Rows[ct][19] = "N";
                                    }
                                    dTbl.Rows[ct][20] = INLocation.Trim();
                                    dTbl.Rows[ct][21] = OutLocation.Trim();
                                NextRecord:

                                    //dTbl.Rows[ct][0] = ds.Tables[0].Rows[i1]["paycode"].ToString();
                                    //dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][9].ToString();
                                    //dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][10].ToString();
                                    //dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][13].ToString();
                                    //dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][14].ToString();
                                    //dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][15].ToString(); 
                                    //dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][16].ToString();
                                    //dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][17].ToString();
                                    //dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][18].ToString();
                                    //dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1][19].ToString();
                                    //dTbl.Rows[ct][10] = ds.Tables[0].Rows[i1][20].ToString();
                                    //dTbl.Rows[ct][11] = ds.Tables[0].Rows[i1][24].ToString();
                                    //dTbl.Rows[ct][12] = ds.Tables[0].Rows[i1][22].ToString();
                                    //dTbl.Rows[ct][13] = ds.Tables[0].Rows[i1][23].ToString();
                                    //dTbl.Rows[ct][14] = ds.Tables[0].Rows[i1][26].ToString();
                                    //dTbl.Rows[ct][15] = ds.Tables[0].Rows[i1][27].ToString();
                                    //dTbl.Rows[ct][16] = ds.Tables[0].Rows[i1][28].ToString();
                                    //if ((ds.Tables[0].Rows[i1][29].ToString() == "Y") || (ds.Tables[0].Rows[i1][30].ToString() == "Y") || (ds.Tables[0].Rows[i1][31].ToString() == "Y") || (ds.Tables[0].Rows[i1][32].ToString() == "Y"))
                                    //{
                                    //    dTbl.Rows[ct][17] = "Y";
                                    //}
                                    //else
                                    //{
                                    //    dTbl.Rows[ct][17] = "N";
                                    //}
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);
                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {

                                msg = "Daily Performance Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Daily Performance Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            }



                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 0) || (i == 1))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:100px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 2) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:100px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "DailyPerformance.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('No record found');", true);
                    return;
                }
            }
            //Cont. late arrival
            else if (radcontlatearrival.Checked)
            {
                if (toDate < FromDate)
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('To Date must be greater than From Date');", true);
                    return;
                }
                ts = toDate.Subtract(FromDate);
                day = ts.Days;
                if (day > 30)
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('Difference is more than one Month');", true);
                    toDate = FromDate.AddMonths(1).AddDays(-1);
                    txtToDate.Text = toDate.ToString("dd/MM/yyyy");
                }

                strsql = "select tblemployee.presentcardno 'CardNumber', tblemployee.paycode 'PayCode',tblemployee.empname 'Name', " +
                            " tbldepartment.DEPARTMENTCODE 'departmentcode',tbldepartment.DEPARTMENTName 'department',tblcatagory.CAT 'catagorycode',tblcatagory.CatagoryName Catagory, " +
                            " tblDivision.DivisionCode 'DivisionCode',tblcompany.companyname as 'CompanyName',convert(varchar(12),tbltimeregister.dateoffice,103) 'DATEOFFICE' , " +
                            " substring(convert(varchar(20),dateadd(minute,tbltimeregister.latearrival,0),108),0,6) 'LateArrival' " +
                            " FROM tblemployee, tblcompany, tbldepartment, tblDivision ,tblcatagory, tbltimeregister,tblemployeeshiftmaster " +
                            " Where tblemployee.paycode = tbltimeregister.paycode And tblemployee.CompanyCode = TblCompany.CompanyCode " +
                            " And tblemployee.Departmentcode = tbldepartment.Departmentcode  " +
                            " And tblemployee.DivisionCode = tblDivision.DivisionCode " +
                            " And tblemployee.cat = tblcatagory.cat " +
                            " And tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                            " And lateArrival > 0 And tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' And '" + toDate.ToString("yyyy-MM-dd") + "' " +
                            " And  (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1 " +
                            " And tblemployee.paycode in ( select paycode from tbltimeregister Where lateArrival > 0 and tbltimeregister.dateoffice " +
                            " between '" + FromDate.ToString("yyyy-MM-dd") + "' And '" + toDate.ToString("yyyy-MM-dd") + "' group by paycode Having count(*) >= " + (day + 1) + "  ) ";

                if (Selection.ToString() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " order by " + SortOrder.Trim() + " ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string PCode;
                    int count = 0;
                    j = 0;
                    PCode = ds.Tables[0].Rows[j]["Paycode"].ToString().Trim();
                    while (j < ds.Tables[0].Rows.Count)
                    {
                        while (j < ds.Tables[0].Rows.Count && PCode == ds.Tables[0].Rows[j]["Paycode"].ToString().Trim())
                        {
                            count++;
                            if (count > 1)
                            {
                                ds.Tables[0].Rows[j]["Cardnumber"] = "";
                                ds.Tables[0].Rows[j]["Paycode"] = "";
                                ds.Tables[0].Rows[j]["name"] = "";
                                ds.Tables[0].Rows[j]["departmentCode"] = "";
                                ds.Tables[0].Rows[j]["Catagorycode"] = "";
                                ds.Tables[0].Rows[j]["divisioncode"] = "";
                                ds.Tables[0].Rows[j]["catagory"] = "";
                                ds.Tables[0].Rows[j]["companyname"] = "";
                                ds.Tables[0].Rows[j]["Department"] = "";
                            }
                            j++;
                        }
                        if (j < ds.Tables[0].Rows.Count)
                            PCode = ds.Tables[0].Rows[j]["Paycode"].ToString().Trim();
                        count = 0;
                    }
                   
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('No record found');", true);
                    return;
                }
            }
            //Cont. Absent
            else if (radContAbsenteesim.Checked)
            {
                if (toDate < FromDate)
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('To Date must be greater than From Date');", true);
                    return;
                }
                ts = toDate.Subtract(FromDate);
                day = ts.Days;
                if (day > 30)
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('Difference is more than one Month');", true);
                    toDate = FromDate.AddMonths(1).AddDays(-1);
                    txtToDate.Text = toDate.ToString("dd/MM/yyyy");
                }
                strsql = "select tblemployee.presentcardno 'CardNumber', tblemployee.paycode 'PayCode',tblemployee.empname 'Name', " +
                            " tbldepartment.DEPARTMENTCODE 'departmentcode',tbldepartment.DEPARTMENTName 'department',tblcatagory.CAT 'catagorycode',tblcatagory.CatagoryName Catagory, " +
                            " tblDivision.DivisionCode 'DivisionCode',tblcompany.companyname as 'CompanyName',convert(varchar(12),tbltimeregister.dateoffice,103) 'DATEOFFICE', " +
                            " tbltimeregister.status 'Absent' " +
                            " FROM tblemployee, tblcompany, tbldepartment, tblDivision ,tblcatagory, tbltimeregister,tblemployeeshiftmaster  " +
                            " Where tblemployee.paycode = tbltimeregister.paycode And tblemployee.CompanyCode = TblCompany.CompanyCode " +
                            " And tblemployee.Departmentcode = tbldepartment.Departmentcode  " +
                            " And tblemployee.DivisionCode = tblDivision.DivisionCode " +
                            " And tblemployee.cat = tblcatagory.cat " +
                            " And tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                            " And absentvalue > 0 And tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' And '" + toDate.ToString("yyyy-MM-dd") + "' " +
                            " And  (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1 " +
                            " And tblemployee.paycode in ( select paycode from tbltimeregister Where absentvalue > 0 and tbltimeregister.dateoffice " +
                            " between '" + FromDate.ToString("yyyy-MM-dd") + "' And '" + toDate.ToString("yyyy-MM-dd") + "' " +
                            " group by paycode Having count(*) >= 1) ";
                if (Selection.ToString() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " order by " + SortOrder.Trim() + " ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string PCode;
                    int count = 0;
                    j = 0;
                    PCode = ds.Tables[0].Rows[j]["Paycode"].ToString().Trim();
                    while (j < ds.Tables[0].Rows.Count)
                    {
                        while (j < ds.Tables[0].Rows.Count && PCode == ds.Tables[0].Rows[j]["Paycode"].ToString().Trim())
                        {
                            count++;
                            if (count > 1)
                            {
                                ds.Tables[0].Rows[j]["Cardnumber"] = "";
                                ds.Tables[0].Rows[j]["Paycode"] = "";
                                ds.Tables[0].Rows[j]["name"] = "";
                                ds.Tables[0].Rows[j]["departmentCode"] = "";
                                ds.Tables[0].Rows[j]["Catagorycode"] = "";
                                ds.Tables[0].Rows[j]["divisioncode"] = "";
                                ds.Tables[0].Rows[j]["catagory"] = "";
                                ds.Tables[0].Rows[j]["companyname"] = "";
                                ds.Tables[0].Rows[j]["Department"] = "";
                            }
                            j++;
                        }
                        if (j < ds.Tables[0].Rows.Count)
                            PCode = ds.Tables[0].Rows[j]["Paycode"].ToString().Trim();
                        count = 0;
                    }
                   
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('No record found');", true);
                    return;
                }
            }



            else if (radAbsenteesim.Checked)
            {

                strsql = "Select tblCatagory.Catagoryname,tblcatagory.CAT 'catagorycode', tblDivision.DivisionName,tblDivision.DivisionCode 'DivisionCode', tblTimeregister.Paycode 'PayCode',tblTimeregister.reason 'Reason', " +
                           "tblTimeregister.Shift,TblEmployee.EmpName 'Name', tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode,tblcompany.companyname 'Company Name', " +
                            "tblEmployee.Cat 'catagorycode', TblEmployee.PresentCardno 'presentcardno',tblemployee.presentcardno 'Card Number', TblDepartment.DepartmentCode 'departmentcode', tblDepartment.DepartmentName, " +
                           "tblTimeregister.Status 'Absent',convert(varchar(12),tbltimeregister.dateoffice,103) 'ForDate', " +
                           "tblTimeregister.LeaveType 'OT/Leave' From  tblCatagory, tblDivision, tblTimeregister, TblEmployee,tblemployeeshiftmaster, " +
                           "tblDepartment,tblCompany,tblgrade,tblshiftmaster  Where tblGrade.GradeCode = tblEmployee.GradeCode And tbltimeregister.Shift= tblshiftmaster.Shift and tblEmployee.CAT = tblCatagory.cat And tblemployee.SSN=tblemployeeshiftmaster.SSN and " +
                           "tblEmployee.DivisionCode = tblDivision.DivisionCode And (tblTimeregister.AbsentValue > 0 Or tblTimeregister.LeaveType " +
                           "In ('A', 'P', 'L')) And  tblEmployee.companycode=tblCompany.companycode and " +
                           "tblEmployee.DepartmentCode = tblDepartment.DepartmentCode AND tbltimeregister.DateOffice = '" + FromDate.ToString("yyyy-MM-dd") + "' " +
                           "and tblEmployee.SSN = tblTimeregister.SSN And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or  " +
                           "tblemployee.LeavingDate is null) And  1=1  and  1=1 ";
                if (Selection.ToString() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " order by " + SortOrder.Trim() + " ";


                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        status = ds.Tables[0].Rows[i]["Absent"].ToString().Trim();
                        if (status.ToString().Trim() != "A")
                        {
                            ds.Tables[0].Rows[i]["OT/Leave"] = ds.Tables[0].Rows[i]["Absent"].ToString();
                            ds.Tables[0].Rows[i]["Absent"] = "";
                        }
                    }
                    if (radExcel.Checked)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Absent");
                            dTbl.Columns.Add("LeaveOT");
                            dTbl.Columns.Add("Remarks");


                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][4] = "Absent";
                            dTbl.Rows[0][5] = "Leave/OT";
                            dTbl.Rows[0][6] = "Remarks";



                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][4].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][13].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][7].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][17].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][19].ToString(); ;
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][5].ToString();


                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                 strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            // msg = "Absence Report on " + FromDate.ToString("dd-MM-yyyy") + " ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {

                                msg = " Absence Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Absence  Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            }


                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else if (((i == 3) || (i == 6)) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=Absent.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/Absent.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Absent");
                            dTbl.Columns.Add("LeaveOT");
                            dTbl.Columns.Add("Remarks");


                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][4] = "Absent";
                            dTbl.Rows[0][5] = "Leave/OT";
                            dTbl.Rows[0][6] = "Remarks";



                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][4].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][13].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][7].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][17].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][19].ToString(); ;
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][5].ToString();


                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            // msg = "Absence Report on " + FromDate.ToString("dd-MM-yyyy") + " ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {

                                msg = " Absence Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Absence  Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            }


                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        }
                                    }
                                    else if (((i == 3) || (i == 6)) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "Absent.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    // Response.Redirect("Reports/DailyReports/Rpt_Absent.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('No record found');", true);
                    return;
                }
            }


            //Early Departure
            else if (radEarlyDeparture.Checked)
            {

                strsql = " select tblemployee.paycode 'PayCode',tblemployee.empname 'Name',tblemployee.presentcardno 'Card Number', " +
                        " tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                        " tblcompany.companyname as 'Company Name',Convert(varchar(5),tbltimeregister.shiftendtime,108) 'Shift End', " +
                        " tblemployeeshiftmaster.ISROUNDTHECLOCKWORK 'RTC',tbltimeregister.SHIFTATTENDED 'Shift Attended', " +
                        " convert(varchar(12),tbltimeregister.dateoffice,103) 'ForDate',Convert(varchar(5),tbltimeregister.out2,108) 'Out', " +
                        " tbltimeregister.status 'Status' ,Cast(tbltimeregister.earlydeparture / 60 as Varchar) +':' +Cast(tbltimeregister.earlydeparture  % 60 as Varchar)'Shift Early', " +
                        " case when tbltimeregister.Otduration = 0 then null else substring(convert(varchar(20),dateadd(minute,tbltimeregister.Otduration,0),108),0,6)  end 'OT', " +
                        " case when tbltimeregister.osduration = 0 then null else substring(convert(varchar(20),dateadd(minute,tbltimeregister.osduration,0),108),0,6)  end 'Over Stay' " +
                        " from tblcalander,tblemployee join tblcompany on tblemployee.companycode=tblcompany.companycode join tblemployeeshiftmaster on " +
                        " tblemployee.SSN=tblemployeeshiftmaster.SSN join tbltimeregister on tblemployee.SSN=tbltimeregister.SSN " +
                        " join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                        " join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                        " join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                        " join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblshiftmaster on tbltimeregister.Shift= tblshiftmaster.Shift " +
                        " where tbltimeregister.dateoffice='" + FromDate.ToString("yyyy-MM-dd") + "' and tbltimeregister.earlydeparture > 0 " +
                        " And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) " +
                        "AND tblcalander.mDate = '" + FromDate.ToString("yyyy-MM-dd") + "' and 1=1  and  1=1 ";
                if (Selection.ToString() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " order by " + SortOrder.Trim() + " ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (radExcel.Checked)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("RTC");
                            dTbl.Columns.Add("Shift");
                            dTbl.Columns.Add("End");
                            dTbl.Columns.Add("Out");
                            dTbl.Columns.Add("ShiftEarly");
                            dTbl.Columns.Add("Status");
                            dTbl.Columns.Add("OT");
                            dTbl.Columns.Add("OverStay");
                            dTbl.Columns.Add("Manual");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "RTC";
                            dTbl.Rows[0][5] = "Shift";
                            dTbl.Rows[0][6] = "End";
                            dTbl.Rows[0][7] = "Out";
                            dTbl.Rows[0][8] = "Shift Early";
                            dTbl.Rows[0][9] = "Status";
                            dTbl.Rows[0][10] = "OT";
                            dTbl.Rows[0][11] = "Over Stay";
                            dTbl.Rows[0][12] = "Manual";


                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {
                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][9].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][10].ToString();
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][8].ToString();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][12].ToString();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][14].ToString();
                                    dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1][13].ToString(); ;
                                    dTbl.Rows[ct][10] = ds.Tables[0].Rows[i1][15].ToString();
                                    dTbl.Rows[ct][11] = ds.Tables[0].Rows[i1][16].ToString();
                                    dTbl.Rows[ct][12] = ds.Tables[0].Rows[i1][16].ToString();

                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                 strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            //  msg = "EarlyDeparture Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {

                                msg = " Early Departure Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Early Departure  Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            }


                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=EarlyDeparture.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/EarlyDeparture.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {

                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("RTC");
                            dTbl.Columns.Add("Shift");
                            dTbl.Columns.Add("End");
                            dTbl.Columns.Add("Out");
                            dTbl.Columns.Add("ShiftEarly");
                            dTbl.Columns.Add("Status");
                            dTbl.Columns.Add("OT");
                            dTbl.Columns.Add("OverStay");
                            dTbl.Columns.Add("Manual");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "RTC";
                            dTbl.Rows[0][5] = "Shift";
                            dTbl.Rows[0][6] = "End";
                            dTbl.Rows[0][7] = "Out";
                            dTbl.Rows[0][8] = "Shift Early";
                            dTbl.Rows[0][9] = "Status";
                            dTbl.Rows[0][10] = "OT";
                            dTbl.Rows[0][11] = "Over Stay";
                            dTbl.Rows[0][12] = "Manual";


                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {
                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][9].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][10].ToString();
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][8].ToString();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][12].ToString();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][14].ToString();
                                    dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1][13].ToString(); ;
                                    dTbl.Rows[ct][10] = ds.Tables[0].Rows[i1][15].ToString();
                                    dTbl.Rows[ct][11] = ds.Tables[0].Rows[i1][16].ToString();
                                    dTbl.Rows[ct][12] = ds.Tables[0].Rows[i1][16].ToString();

                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            //  msg = "EarlyDeparture Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {

                                msg = " Early Departure Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Early Departure  Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            }


                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "EarlyDeparture.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('No record found');", true);
                    return;
                }
            }
            //Cont. Early Departure
            else if (radContEarlyDept.Checked)
            {
                if (toDate < FromDate)
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('To Date must be greater than From Date');", true);
                    return;
                }
                ts = toDate.Subtract(FromDate);
                day = ts.Days;
                if (day > 30)
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('Difference is more than one Month');", true);
                    toDate = FromDate.AddMonths(1).AddDays(-1);
                    txtToDate.Text = toDate.ToString("dd/MM/yyyy");
                }
                strsql = "select tblemployee.presentcardno 'CardNumber', tblemployee.paycode 'PayCode',tblemployee.empname 'Name', " +
                    " tbldepartment.DEPARTMENTCODE 'departmentcode',tbldepartment.DEPARTMENTName 'department',tblcatagory.CAT 'catagorycode',tblcatagory.CatagoryName Catagory, " +
                    " tblDivision.DivisionCode 'DivisionCode',tblcompany.companyname as 'CompanyName',convert(varchar(12),tbltimeregister.dateoffice,103) 'DATEOFFICE' , " +
                    " substring(convert(varchar(20),dateadd(minute,tbltimeregister.earlydeparture,0),108),0,6) 'Early' " +
                    " FROM tblemployee, tblcompany, tbldepartment, tblDivision ,tblcatagory, tbltimeregister,tblemployeeshiftmaster " +
                    " Where tblemployee.paycode = tbltimeregister.paycode And tblemployee.CompanyCode = TblCompany.CompanyCode " +
                    " And tblemployee.Departmentcode = tbldepartment.Departmentcode  " +
                    " And tblemployee.DivisionCode = tblDivision.DivisionCode " +
                    " And tblemployee.cat = tblcatagory.cat " +
                    " And tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                    " And Earlydeparture > 0 And tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' And '" + toDate.ToString("yyyy-MM-dd") + "' " +
                    " And  (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1 " +
                    " And tblemployee.paycode in ( select paycode from tbltimeregister Where earlydeparture > 0 and tbltimeregister.dateoffice " +
                    " between '" + FromDate.ToString("yyyy-MM-dd") + "' And '" + toDate.ToString("yyyy-MM-dd") + "' group by paycode Having count(*) >= " + (day + 1) + "  ) ";

                if (Selection.ToString() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " order by " + SortOrder.Trim() + " ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string PCode;
                    int count = 0;
                    j = 0;
                    PCode = ds.Tables[0].Rows[j]["Paycode"].ToString().Trim();
                    while (j < ds.Tables[0].Rows.Count)
                    {
                        while (j < ds.Tables[0].Rows.Count && PCode == ds.Tables[0].Rows[j]["Paycode"].ToString().Trim())
                        {
                            count++;
                            if (count > 1)
                            {
                                ds.Tables[0].Rows[j]["Cardnumber"] = "";
                                ds.Tables[0].Rows[j]["Paycode"] = "";
                                ds.Tables[0].Rows[j]["name"] = "";
                                ds.Tables[0].Rows[j]["departmentCode"] = "";
                                ds.Tables[0].Rows[j]["Catagorycode"] = "";
                                ds.Tables[0].Rows[j]["divisioncode"] = "";
                                ds.Tables[0].Rows[j]["catagory"] = "";
                                ds.Tables[0].Rows[j]["companyname"] = "";
                                ds.Tables[0].Rows[j]["Department"] = "";
                            }
                            j++;
                        }
                        if (j < ds.Tables[0].Rows.Count)
                            PCode = ds.Tables[0].Rows[j]["Paycode"].ToString().Trim();
                        count = 0;
                    }
                    
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('No record found');", true);
                    return;
                }
            }
            //Attendance Report
            else if (radAttendance.Checked)
            {
                strsql = "select tblemployee.paycode 'EmpCode',tblemployee.paycode 'paycode',tblemployee.empname 'Employee Name',tblemployee.presentcardno 'CardNo',tblcompany.companyname as 'CompanyName',tbltimeregister.shiftattended 'Shift', " +
                        "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                        "Convert(varchar(5),tbltimeregister.shiftstarttime,108) 'Start',convert(varchar(12),tbltimeregister.dateoffice,103) 'ForDate', " +
                        "Cast(tbltimeregister.latearrival / 60 as Varchar) +':' +Cast(tbltimeregister.latearrival % 60 as Varchar)  'shift late', " +
                        "tbltimeregister.status 'Status',Convert(varchar(5),tbltimeregister.in1,108) as 'In' " +
                        "from tblemployee join tblcompany on tblemployee.companycode=tblcompany.companycode join tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN " +
                        "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                        "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                        "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                        "join tbltimeregister on tblemployee.SSN=tbltimeregister.SSN " +
                        " join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblshiftmaster on tbltimeregister.Shift= tblshiftmaster.Shift " +
                        "where tbltimeregister.dateoffice='" + FromDate.ToString("yyyy-MM-dd") + "'  " +
                        "and (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) and  1=1  and  1=1  ";
                if (Selection.ToString() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " order by " + SortOrder.Trim() + " ";



                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        status = ds.Tables[0].Rows[i]["Status"].ToString().Trim();
                        if (status.ToString().ToUpper().Trim() == "MIS")
                        {
                            ds.Tables[0].Rows[i]["Status"] = "P";
                        }
                    }

                    if (radExcel.Checked)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Shift");
                            dTbl.Columns.Add("Start");
                            dTbl.Columns.Add("IN");
                            dTbl.Columns.Add("ShiftLate");
                            dTbl.Columns.Add("Status");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "Shift";
                            dTbl.Rows[0][5] = "Start";
                            dTbl.Rows[0][6] = "IN";
                            dTbl.Rows[0][7] = "Shift Late";
                            dTbl.Rows[0][8] = "Status";


                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][3].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][5].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][10].ToString(); ;
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][14].ToString();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][12].ToString();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][13].ToString();

                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                 strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            //  msg = "Attendance Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {

                                msg = " Attendnce Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Attendance  Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            }


                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=Attendance.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/Attendance.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Shift");
                            dTbl.Columns.Add("Start");
                            dTbl.Columns.Add("IN");
                            dTbl.Columns.Add("ShiftLate");
                            dTbl.Columns.Add("Status");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "Shift";
                            dTbl.Rows[0][5] = "Start";
                            dTbl.Rows[0][6] = "IN";
                            dTbl.Rows[0][7] = "Shift Late";
                            dTbl.Rows[0][8] = "Status";


                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][3].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][5].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][10].ToString(); ;
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][14].ToString();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][12].ToString();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][13].ToString();

                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            //  msg = "Attendance Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {

                                msg = " Attendnce Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Attendance  Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            }


                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "Attendance.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('No record found');", true);
                    return;
                }
            }
            //Time Loss
            else if (radTimeLoss.Checked)
            {

                strsql = "select tblemployee.paycode 'paycode',tblemployee.paycode 'EmpCode',tblemployee.empname 'Employee Name',tblemployee.presentcardno 'CardNo',tblcompany.companyname as 'CompanyName',tbltimeregister.shiftattended 'Shift', " +
                            "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                            "Convert(varchar(5),tbltimeregister.shiftstarttime,108) 'Start',Convert(varchar(5),tbltimeregister.shiftendtime,108) 'End',tbltimeregister.status 'Status',convert(varchar(12),tbltimeregister.dateoffice,103) 'ForDate', " +
                            "case when tbltimeregister.earlyarrival=0 then null else substring(convert(varchar(20),dateadd(minute,tbltimeregister.earlyarrival,0),108),0,6) end 'Early Arrival', " +
                            "case when tbltimeregister.latearrival=0 then null else substring(convert(varchar(20),dateadd(minute,tbltimeregister.latearrival,0),108),0,6) end 'shift late', " +
                            "case when tbltimeregister.earlydeparture=0 then null else substring(convert(varchar(20),dateadd(minute,tbltimeregister.earlydeparture,0),108),0,6) end 'Shift Early', " +
                            "case when tbltimeregister.LUNCHLATEARRIVAL =0 then null else substring(convert(varchar(20),dateadd(minute,tbltimeregister.LUNCHLATEARRIVAL ,0),108),0,6) end 'Lunch Late', " +
                            "case when tbltimeregister.LUNCHEARLYDEPARTURE =0 then null else substring(convert(varchar(20),dateadd(minute,tbltimeregister.LUNCHEARLYDEPARTURE ,0),108),0,6) end 'lunch Early', " +
                            "case when tbltimeregister.EXCLUNCHHOURS=0 then null else substring(convert(varchar(20),dateadd(minute,tbltimeregister.EXCLUNCHHOURS,0),108),0,6) end 'Excess lunch', " +
                            "case when tbltimeregister.OTDURATION=0 then null else substring(convert(varchar(20),dateadd(minute,tbltimeregister.OTDURATION,0),108),0,6) end 'OT', " +
                            "case when tbltimeregister.TOTALLOSSHRS=0 then null else substring(convert(varchar(20),dateadd(minute,tbltimeregister.TOTALLOSSHRS,0),108),0,6) end 'Loss Hours', " +
                            "case when tbltimeregister.OSDURATION=0 then null else substring(convert(varchar(20),dateadd(minute,tbltimeregister.OSDURATION,0),108),0,6) end 'Over Stay' " +
                            "from tblemployee join tblcompany on tblemployee.companycode=tblcompany.companycode join tblemployeeshiftmaster  on tblemployee.SSN=tblemployeeshiftmaster.SSN " +
                            "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                            "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                            "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                            "join tbltimeregister on tblemployee.SSN=tbltimeregister.SSN " +
                            " join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblshiftmaster on tbltimeregister.Shift= tblshiftmaster.Shift " +
                            " where tbltimeregister.dateoffice='" + FromDate.ToString("yyyy-MM-dd") + "'  " +
                            "and tbltimeregister.TOTALLOSSHRS > 0" +
                            " And  (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "'  or tblemployee.LeavingDate is null) AND  1=1  and  1=1 ";
                if (Selection.ToString() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " order by " + SortOrder.Trim() + " ";


                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (radExcel.Checked)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            //// create table 
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            //dTbl.Columns.Add("Paycode");
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Shift");
                            dTbl.Columns.Add("Start");
                            dTbl.Columns.Add("End");
                            dTbl.Columns.Add("Status");
                            dTbl.Columns.Add("EarlyArrival");
                            dTbl.Columns.Add("ShiftLate");
                            dTbl.Columns.Add("ShiftEarly");
                            dTbl.Columns.Add("LunchLate");
                            dTbl.Columns.Add("LunchEarly");
                            dTbl.Columns.Add("ExcessLunch");
                            dTbl.Columns.Add("OT");
                            dTbl.Columns.Add("LossHours");
                            dTbl.Columns.Add("OverStay");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "Shift";
                            dTbl.Rows[0][5] = "Start";
                            dTbl.Rows[0][6] = "End";
                            dTbl.Rows[0][7] = "Status";
                            dTbl.Rows[0][8] = "Early Arrival";
                            dTbl.Rows[0][9] = "Shift Late";
                            dTbl.Rows[0][10] = "Shift Early";
                            dTbl.Rows[0][11] = "Lunch Late";
                            dTbl.Rows[0][12] = "Lunch Early";
                            dTbl.Rows[0][13] = "Excess Lunch";
                            dTbl.Rows[0][14] = "OT";
                            dTbl.Rows[0][15] = "Loss Hours";
                            dTbl.Rows[0][16] = "Over Stay";


                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][3].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][5].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][10].ToString();
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][11].ToString();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][12].ToString();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][14].ToString();
                                    dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1][15].ToString();
                                    dTbl.Rows[ct][10] = ds.Tables[0].Rows[i1][16].ToString();
                                    dTbl.Rows[ct][11] = ds.Tables[0].Rows[i1][17].ToString();
                                    dTbl.Rows[ct][12] = ds.Tables[0].Rows[i1][18].ToString();
                                    dTbl.Rows[ct][13] = ds.Tables[0].Rows[i1][19].ToString();
                                    dTbl.Rows[ct][14] = ds.Tables[0].Rows[i1][20].ToString();
                                    dTbl.Rows[ct][15] = ds.Tables[0].Rows[i1][21].ToString();
                                    dTbl.Rows[ct][16] = ds.Tables[0].Rows[i1][22].ToString();

                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                 strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            // msg = "Time Loss Report on " + FromDate.ToString("dd-MM-yyyy") + " ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {

                                msg = " TimeLoss Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "TimeLoss  Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            }
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=TimeLoss.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/TimeLoss.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            //// create table 
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            //dTbl.Columns.Add("Paycode");
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Shift");
                            dTbl.Columns.Add("Start");
                            dTbl.Columns.Add("End");
                            dTbl.Columns.Add("Status");
                            dTbl.Columns.Add("EarlyArrival");
                            dTbl.Columns.Add("ShiftLate");
                            dTbl.Columns.Add("ShiftEarly");
                            dTbl.Columns.Add("LunchLate");
                            dTbl.Columns.Add("LunchEarly");
                            dTbl.Columns.Add("ExcessLunch");
                            dTbl.Columns.Add("OT");
                            dTbl.Columns.Add("LossHours");
                            dTbl.Columns.Add("OverStay");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "Shift";
                            dTbl.Rows[0][5] = "Start";
                            dTbl.Rows[0][6] = "End";
                            dTbl.Rows[0][7] = "Status";
                            dTbl.Rows[0][8] = "Early Arrival";
                            dTbl.Rows[0][9] = "Shift Late";
                            dTbl.Rows[0][10] = "Shift Early";
                            dTbl.Rows[0][11] = "Lunch Late";
                            dTbl.Rows[0][12] = "Lunch Early";
                            dTbl.Rows[0][13] = "Excess Lunch";
                            dTbl.Rows[0][14] = "OT";
                            dTbl.Rows[0][15] = "Loss Hours";
                            dTbl.Rows[0][16] = "Over Stay";


                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][3].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][5].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][10].ToString();
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][11].ToString();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][12].ToString();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][14].ToString();
                                    dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1][15].ToString();
                                    dTbl.Rows[ct][10] = ds.Tables[0].Rows[i1][16].ToString();
                                    dTbl.Rows[ct][11] = ds.Tables[0].Rows[i1][17].ToString();
                                    dTbl.Rows[ct][12] = ds.Tables[0].Rows[i1][18].ToString();
                                    dTbl.Rows[ct][13] = ds.Tables[0].Rows[i1][19].ToString();
                                    dTbl.Rows[ct][14] = ds.Tables[0].Rows[i1][20].ToString();
                                    dTbl.Rows[ct][15] = ds.Tables[0].Rows[i1][21].ToString();
                                    dTbl.Rows[ct][16] = ds.Tables[0].Rows[i1][22].ToString();

                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            // msg = "Time Loss Report on " + FromDate.ToString("dd-MM-yyyy") + " ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {

                                msg = " TimeLoss Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "TimeLoss  Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            }
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "TimeLoss.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    //Response.Redirect("Reports/DailyReports/Rpt_TimeLoss.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('No record found');", true);
                    return;
                }
            }
            //Department wise
            else if (radDepartment.Checked)
            {

                strsql = "Select TblDepartment.DepartmentCode,TblDepartment.DepartmentName,count(tblemployee.paycode) 'Total Employee',sum(tblTimeregister.PresentValue)'Present',sum(tblTimeregister.AbsentValue)'Absent',sum(tbltimeregister.LeaveValue)'Leave',tblcompany.companyname 'CompanyName', " +
                        "sum(tbltimeregister.outworkduration)'OnDuty', sum(tblTimeregister.Wo_Value)'WeeklyOff' from tblemployeeshiftmaster, tblcatagory, tblDivision,tblTimeregister ,tblEmployee ,tblDepartment,TBLCOMPANY,tblgrade  Where tblGrade.GradeCode = tblEmployee.GradeCode  And tblEmployee.Companycode = tblCompany.Companycode AND tblemployee.paycode=tblemployeeshiftmaster.paycode and " +
                        "tblEmployee.CAT = tblCatagory.cat And tblEmployee.DivisionCode = tblDivision.DivisionCode And tblTimeregister.PayCode = TblEmployee.PayCode And  TblEmployee.DepartmentCode = TblDepartment.DepartmentCode AND " +
                        "tbltimeregister.DateOffice ='" + FromDate.ToString("yyyy-MM-dd") + "'  And  (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1 ";

                if (Selection.ToString() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " group by TblDepartment.DepartmentCode,TblDepartment.DepartmentName,tblcompany.companyname order by tbldepartment.departmentcode";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    try
                    {
                        string Tmp = "";
                        int x, i;
                        System.Data.DataTable dTbl = new System.Data.DataTable();
                        DataRow dRow;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);

                        dTbl.Columns.Add("SNo");
                        dTbl.Columns.Add("Company");
                        dTbl.Columns.Add("Code");
                        dTbl.Columns.Add("Department Name");
                        dTbl.Columns.Add("Total Employee");
                        dTbl.Columns.Add("Present");
                        dTbl.Columns.Add("On Duty");
                        dTbl.Columns.Add("Absent");
                        dTbl.Columns.Add("Leave");
                        dTbl.Columns.Add("Weeok Off");
                        //dTbl.Columns.Add("OTAmount");

                        dTbl.Rows[0][0] = "SNo";
                        dTbl.Rows[0][1] = "Company";
                        dTbl.Rows[0][2] = "Code";
                        dTbl.Rows[0][3] = "Department";
                        dTbl.Rows[0][4] = "Total Employee";
                        dTbl.Rows[0][5] = "Present";
                        dTbl.Rows[0][6] = "On Duty";
                        dTbl.Rows[0][7] = "Absent";
                        dTbl.Rows[0][8] = "Leave";
                        dTbl.Rows[0][9] = "Week Off";
                        //dTbl.Rows[0][10] = "OT Amount";


                        int ct = 1;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                            {

                                dTbl.Rows[ct][0] = ct.ToString();
                                dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1]["CompanyName"].ToString();
                                dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1]["DepartmentCode"].ToString();
                                dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1]["DepartmentName"].ToString();
                                dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1]["Total Employee"].ToString();
                                dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1]["Present"].ToString(); ;
                                dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1]["OnDuty"].ToString();
                                dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1]["Absent"].ToString();
                                dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1]["Leave"].ToString();
                                dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1]["WeeklyOff"].ToString();
                                // dTbl.Rows[ct][10] = ds.Tables[0].Rows[i1][15].ToString();

                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                ct = ct + 1;
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        bool isEmpty;
                        for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                        {

                            isEmpty = true;
                            for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                            {
                                if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                {
                                    isEmpty = false;
                                    break;
                                }
                            }

                            if (isEmpty == true)
                            {
                                dTbl.Rows.RemoveAt(i2);
                                i2--;
                            }
                        }
                        string companyname = "";
                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" +Session["LoginCompany"].ToString().Trim() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }

                        int colCount = Convert.ToInt32(dTbl.Columns.Count);

                        msg = "Department Summary For " + FromDate.ToString("dd-MM-yyyy");
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                        x = colCount;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                    if (Tmp.Substring(0, 1) == "0")
                                    {
                                        if (Tmp.Contains(":"))
                                        {
                                            Tmp = Tmp;
                                        }
                                        else
                                        {
                                            Tmp = Tmp;
                                        }
                                    }
                                }
                                if ((i == 1) || (i == 2))
                                {
                                    if (Tmp.ToString().Trim() != "")
                                    {
                                        //if (Tmp.ToString().Substring(0, 1) == "0")
                                        // {                                              
                                        sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:95px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        //}
                                        //else
                                        //{
                                        //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                        // }
                                        // }
                                    }
                                }
                                else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                {
                                    sb.Append("<td style='width:220px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                else
                                    sb.Append("<td style='width:80px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        if (radExcel.Checked)
                        {
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=DepartmentSummary.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/DepartmentSummary.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        else
                        {
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "DepartmentSummary.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                   
                    }
                    catch (Exception ex)
                    {
                        //Response.Write(ex.Message.ToString());
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('No record found');", true);
                    return;
                }
            }
            //OT 
            else if (radOT.Checked)
            {

                strsql = "select tblcompany.companyname'CompanyName',tblemployee.paycode 'paycode',tblemployee.presentcardno 'CardNumber',tblemployee.empname 'Name', " +
                            "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                            "tbltimeregister.paycode 'EmpCode', " +
                            "case when tbltimeregister.otduration=0 then null else substring(convert(varchar(20),dateadd(minute,tbltimeregister.otduration,0),108),0,6) end 'OTDuration', " +
                            "case when tbltimeregister.otduration=0 then null else cast(otamount as decimal(10,2)) end 'OTAmount' " +
                            "from tbltimeregister join tblemployee on tbltimeregister.SSN=tblemployee.SSN " +
                            "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                            "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                            "join tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN " +
                            "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                            "join tblcompany on tblemployee.COMPANYCODE=tblcompany.COMPANYCODE " +
                            " join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblshiftmaster on tbltimeregister.Shift= tblshiftmaster.Shift " +
                            " where tbltimeregister.otduration > 0  " +
                            "and tbltimeregister.dateoffice='" + FromDate.ToString("yyyy-MM-dd") + "' " +
                            " And  (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "'  or tblemployee.LeavingDate is null) AND  1=1  and  1=1 ";
                if (Selection.ToString() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " order by " + SortOrder.Trim() + " ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (radExcel.Checked)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("OT");
                            dTbl.Columns.Add("OTAmount");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "OT";
                            dTbl.Rows[0][5] = "OT Amount";

                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][3].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][9].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][10].ToString();

                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                 strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            // msg = "OT Report on " + FromDate.ToString("dd-MM-yyyy") + " ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {

                                msg = " OT Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "OT  Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            }
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:70px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=OTReport.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/OTReport.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("OT");
                            dTbl.Columns.Add("OTAmount");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "OT";
                            dTbl.Rows[0][5] = "OT Amount";

                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][3].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][9].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][10].ToString();

                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }
                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            // msg = "OT Report on " + FromDate.ToString("dd-MM-yyyy") + " ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {

                                msg = " OT Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "OT  Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            }
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:70px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "OT.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    //Response.Redirect("Reports/DailyReports/Rpt_OverTime.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('No record found');", true);
                    return;
                }
            }
            //Early Arrival
            else if (radEarlyArrival.Checked)
            {
                strsql = "select tblemployee.paycode 'PayCode',tblemployee.empname 'Name',tblemployee.presentcardno 'Card Number',tblcompany.companyname as 'CompanyName', " +
                            "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                            "tbltimeregister.shiftattended 'Shift',Convert(varchar(5),tbltimeregister.shiftstarttime,108) 'Shift Start',convert(char(5),tbltimeregister.in1,108) 'In', " +
                            "substring(convert(varchar(20),dateadd(minute,tbltimeregister.earlyarrival,0),108),0,6)  'shift Early', " +
                            "case when tbltimeregister.earlyarrival between 0 and 10 then '**' else '' end as '>(0.01)', " +
                            "case when tbltimeregister.earlyarrival between 11 and 30 then '**' else '' end as '>(0.10)', " +
                            "case when tbltimeregister.earlyarrival between 31 and 60 then '**' else '' end as '>(0.30)', " +
                            "case when tbltimeregister.earlyarrival > 60 then '**' else '' end as '>(0.60)' " +
                            "from tblemployee  join tblcompany  on tblemployee.companycode=tblcompany.companycode join tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                            "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                            "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                            "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                            "join tbltimeregister on tblemployee.SSN=tbltimeregister.SSN " +
                            "join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblshiftmaster on tbltimeregister.Shift= tblshiftmaster.Shift " +
                            " where tbltimeregister.dateoffice='" + FromDate.ToString("yyyy-MM-dd") + "' and tbltimeregister.earlyarrival > 0  " +
                            " And  (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "'  or tblemployee.LeavingDate is null) AND  1=1  and  1=1 ";
                if (Selection.ToString() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " order by " + SortOrder.Trim() + " ";


                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (radExcel.Checked)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Shift");
                            dTbl.Columns.Add("Start");
                            dTbl.Columns.Add("IN");
                            dTbl.Columns.Add("ShiftEarly");
                            dTbl.Columns.Add(">(0.01)");
                            dTbl.Columns.Add(">(0.10)");
                            dTbl.Columns.Add(">(0.30)");
                            dTbl.Columns.Add(">(0.60)");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "Shift";
                            dTbl.Rows[0][5] = "Start";
                            dTbl.Rows[0][6] = "IN";
                            dTbl.Rows[0][7] = "Shift Early";
                            dTbl.Rows[0][8] = ">(0.01)";
                            dTbl.Rows[0][9] = ">(0.10)";
                            dTbl.Rows[0][10] = ">(0.30)";
                            dTbl.Rows[0][11] = ">(0.60)";


                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][8].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][9].ToString();
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][10].ToString();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][11].ToString();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][12].ToString();
                                    dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1][13].ToString();
                                    dTbl.Rows[ct][10] = ds.Tables[0].Rows[i1][14].ToString();
                                    dTbl.Rows[ct][11] = ds.Tables[0].Rows[i1][15].ToString();



                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                 strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            //  msg = "Early Arrival Report on " + FromDate.ToString("dd-MM-yyyy") + " ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {

                                msg = " Early Arrival Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Early Arrival  Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            }
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=EarlyArrival.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/EarlyArrival.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Shift");
                            dTbl.Columns.Add("Start");
                            dTbl.Columns.Add("IN");
                            dTbl.Columns.Add("ShiftEarly");
                            dTbl.Columns.Add(">(0.01)");
                            dTbl.Columns.Add(">(0.10)");
                            dTbl.Columns.Add(">(0.30)");
                            dTbl.Columns.Add(">(0.60)");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "Shift";
                            dTbl.Rows[0][5] = "Start";
                            dTbl.Rows[0][6] = "IN";
                            dTbl.Rows[0][7] = "Shift Early";
                            dTbl.Rows[0][8] = ">(0.01)";
                            dTbl.Rows[0][9] = ">(0.10)";
                            dTbl.Rows[0][10] = ">(0.30)";
                            dTbl.Rows[0][11] = ">(0.60)";


                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][8].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][9].ToString();
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][10].ToString();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][11].ToString();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][12].ToString();
                                    dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1][13].ToString();
                                    dTbl.Rows[ct][10] = ds.Tables[0].Rows[i1][14].ToString();
                                    dTbl.Rows[ct][11] = ds.Tables[0].Rows[i1][15].ToString();



                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            //  msg = "Early Arrival Report on " + FromDate.ToString("dd-MM-yyyy") + " ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {

                                msg = " Early Arrival Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Early Arrival  Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            }
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "EarlyArrival.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        } 
                    }
                    //Response.Redirect("Reports/DailyReports/Rpt_EarlyArrival.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('No record found');", true);
                    return;
                }
            }
            //Shift Wise
            else if (radshiftwise.Checked)
            {

                //strsql = "select  distinct tbltimeregister.shift 'Shift',tblemployee.paycode from tblCatagory,tblDivision,tbltimeregister,tblEmployee,tblCompany,tblDepartment,tblgrade,tblshiftmaster " +
                //        "where tblGrade.GradeCode = tblEmployee.GradeCode And tblCatagory.Cat = tblEmployee.Cat And  tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregister.SSN= tblEmployee.SSN and tbltimeregister.dateoffice='" + FromDate.ToString("yyyy-MM-dd") + "' ";
                //if (Selection.ToString() != "")
                //{
                //    strsql += Selection.ToString();
                //}
                //strsql += " " + CompanySelection.ToString() + " ";
                //strsql += " order by shift ";


                strsql = "select tblemployee.paycode 'PayCode',tblemployee.empname 'Name',tblemployee.presentcardno 'CardNumber', " +
                                        " tblcompany.companyname as 'CompanyName', " +
                                        " tbltimeregister.shiftattended 'Shift', Convert(varchar(5),tbltimeregister.shiftstarttime,108) 'ShiftStart',tbltimeregister.status 'Status', " +
                                        " Convert(varchar(5),tbltimeregister.in1,108) 'In',substring(convert(varchar(20), " +
                                        " dateadd(minute,tbltimeregister.latearrival ,0),108),0,6) 'shiftlate' " +
                                        " from tblemployee tblemployee join tbltimeregister tbltimeregister on tblemployee.paycode=tbltimeregister.paycode join tblcompany tblcompany on " +
                                        " tblemployee.companycode=tblcompany.companycode " +
                                        " join  tblemployeeshiftmaster on tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                                        "where tbltimeregister.dateoffice=convert(datetime,convert(char(10),'" + FromDate.ToString() + "',120),120) ";
                if (Selection.ToString() != "")
                {
                    strsql += Selection.ToString();
                }
              //  strsql += " " + CompanySelection.ToString() + " ";
                strsql += " order by shift ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    try
                    {
                        string Tmp = "";
                        int x, i;
                        System.Data.DataTable dTbl = new System.Data.DataTable();
                        DataRow dRow;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);

                        dTbl.Columns.Add("SNo");
                        dTbl.Columns.Add("PayCode");
                        dTbl.Columns.Add("Name");
                        dTbl.Columns.Add("Shift");
                        dTbl.Columns.Add("Start");
                        dTbl.Columns.Add("In Time");
                        dTbl.Columns.Add("Status");
                        dTbl.Columns.Add("Late");


                        dTbl.Rows[0][0] = "SNo";
                        dTbl.Rows[0][1] = "PayCode";
                        dTbl.Rows[0][2] = "Name";
                        dTbl.Rows[0][3] = "Shift";
                        dTbl.Rows[0][4] = "Start";
                        dTbl.Rows[0][5] = "In Time";
                        dTbl.Rows[0][6] = "Status";
                        dTbl.Rows[0][7] = "Late";

                        int ct = 1;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                            {

                                dTbl.Rows[ct][0] = ct.ToString();
                                dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1]["PayCode"].ToString().Trim();
                                dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1]["Name"].ToString().Trim();
                                dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1]["Shift"].ToString().Trim();
                                dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1]["ShiftStart"].ToString().Trim();
                                dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1]["In"].ToString().Trim();
                                dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1]["Status"].ToString().Trim();
                                dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1]["shiftlate"].ToString().Trim();

                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                ct = ct + 1;
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }
                        bool isEmpty;
                        for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                        {

                            isEmpty = true;
                            for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                            {
                                if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                {
                                    isEmpty = false;
                                    break;
                                }
                            }

                            if (isEmpty == true)
                            {
                                dTbl.Rows.RemoveAt(i2);
                                i2--;
                            }
                        }
                        string companyname = "";
                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }

                        int colCount = Convert.ToInt32(dTbl.Columns.Count);

                        // msg = "OT Report on " + FromDate.ToString("dd-MM-yyyy") + " ";

                        if (Session["IsNepali"].ToString().Trim() == "Y")
                        {

                            msg = " Shift Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                        }
                        else
                        {
                            msg = "Shift  Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                        }
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                        x = colCount;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                    if (Tmp.Substring(0, 1) == "0")
                                    {
                                        if (Tmp.Contains(":"))
                                        {
                                            Tmp = Tmp;
                                        }
                                        else
                                        {
                                            Tmp = Tmp;
                                        }
                                    }
                                }
                                if ((i == 1) || (i == 2))
                                {
                                    if (Tmp.ToString().Trim() != "")
                                    {
                                        //if (Tmp.ToString().Substring(0, 1) == "0")
                                        // {                                              
                                        sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        //}
                                        //else
                                        //{
                                        //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                        // }
                                        // }
                                    }
                                }
                                else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                {
                                    sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                else
                                    sb.Append("<td style='width:70px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();

                        if (radPDF.Checked)
                        {
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "Shift.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        else
                        {
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=Shift.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/Shift.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                    }
                    catch (Exception ex)
                    {
                        //Response.Write(ex.Message.ToString());
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('No record found');", true);
                    return;
                }
            }
            //Shift Change
            else if (radShiftChange.Checked)
            {

                strsql = "select tblemployee.paycode 'PayCode',tblemployee.empname 'Name',tblemployee.presentcardno 'CardNumber',tblcompany.companyname as 'CompanyName',tbltimeregister.shift 'ActualShift', " +
                            "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                            "tbltimeregister.shiftattended 'Shift',Convert(varchar(5),tbltimeregister.shiftstarttime,108) 'ShiftStart',Convert(varchar(5),tbltimeregister.in1,108) 'In', " +
                            "Convert(varchar(5),tbltimeregister.out2,108) 'Out', " +
                            "case when tbltimeregister.HOURSWORKED = 0 then null else substring(convert(varchar(20),dateadd(minute,tbltimeregister.HOURSWORKED,0),108),0,6)  end 'HourWorked', " +
                            "tbltimeregister.status 'Status', " +
                            "case when tbltimeregister.earlyarrival = 0 then null else substring(convert(varchar(20),dateadd(minute,tbltimeregister.earlyarrival,0),108),0,6)  end 'shiftEarly', " +
                            "case when tbltimeregister.latearrival = 0 then null else substring(convert(varchar(20),dateadd(minute,tbltimeregister.latearrival,0),108),0,6)  end 'shiftlate' " +
                            "from tblemployee join tbltimeregister on tblemployee.SSN=tbltimeregister.SSN join tblcompany  " +
                            "on tblemployee.companycode=tblcompany.companycode " +
                            "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                            "join tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN " +
                            "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                            "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                            "join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblshiftmaster on tbltimeregister.Shift= tblshiftmaster.Shift " +
                            "where tbltimeregister.shift!=tbltimeregister.shiftattended and tbltimeregister.dateoffice='" + FromDate.ToString("yyyy-MM-dd") + "' " +
                            "And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) and  1=1  and  1=1 ";
                if (Selection.ToString() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " order by " + SortOrder.Trim() + " ";


                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                   
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('No record found');", true);
                    return;
                }
            }
            //Out Work Register
            else if (radoutwork.Checked)
            {
                strsql = "select tblemployee.paycode 'Paycode',tblemployee.presentcardno 'CardNo',tblemployee.empname 'Name',tbloutworkregister.dateoffice 'Date', " +
                        "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                         "tblcompany.companyname 'CompanyName', " +
                         "Convert(varchar(5),tbloutworkregister.IN1,108)'IN1', " +
                         "Convert(varchar(5),tbloutworkregister.out1,108) 'Out1'," +
                         "Convert(varchar(5),tbloutworkregister.In2,108) 'IN2', " +
                         "Convert(varchar(5),tbloutworkregister.out2,108)'Out2'," +
                         "Convert(varchar(5),tbloutworkregister.In3,108) 'IN3', " +
                         "Convert(varchar(5),tbloutworkregister.out3,108) 'Out3', " +
                         "Convert(varchar(5),tbloutworkregister.In4,108) 'IN4', " +
                         "Convert(varchar(5),tbloutworkregister.out4,108)'Out4', " +
                         "Convert(varchar(5),tbloutworkregister.In5,108)  'IN5', " +
                         "Convert(varchar(5),tbloutworkregister.out5,108)'out5', " +
                         "Convert(varchar(5),tbloutworkregister.In6,108)  'IN6', " +
                         "Convert(varchar(5),tbloutworkregister.out6,108) 'Out', " +
                         "Convert(varchar(5),tbloutworkregister.In7,108)  'IN7', " +
                         "Convert(varchar(5),tbloutworkregister.out7,108) 'Out7'," +
                         "Convert(varchar(5),tbloutworkregister.In8,108)  'IN8', " +
                         "Convert(varchar(5),tbloutworkregister.out8,108) 'Out8', " +
                         "Convert(varchar(5),tbloutworkregister.In9,108)  'IN9', " +
                         "Convert(varchar(5),tbloutworkregister.out9,108) 'Out9', " +
                         "Convert(varchar(5),tbloutworkregister.In10,108)  'IN10', " +
                         "Convert(varchar(5),tbloutworkregister.out10,108) 'Out10', " +
                         "case when tbloutworkregister.outworkduration = 0 then null else substring(convert(varchar(20),dateadd(minute,tbloutworkregister.outworkduration,0),108),0,6) end 'OutWork' " +
                         "from tblemployee join tbloutworkregister on tblemployee.SSN=tbloutworkregister.SSN join tblcompany on tblemployee.companycode=tblcompany.companycode " +
                         "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                         "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                         "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                         " join tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN " +
                         " join tblgrade on tblemployee.gradecode=tblgrade.gradecode " +
                         " where " +
                         " tbloutworkregister.dateoffice='" + FromDate.ToString("yyyy-MM-dd") + "' " +
                         " And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) and  1=1  and  1=1 ";
                if (Selection.ToString() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " order by " + SortOrder.Trim() + " ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                   
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('No record found');", true);
                    return;
                }
            }
            //Mis Punch
            else if (radMisPunch.Checked)
            {
                strsql = "select tblemployee.paycode 'PayCode',tblemployee.empname 'Name',tblemployee.presentcardno 'CardNumber', " +
                        "tblcompany.companyname as 'CompanyName',tbltimeregister.shift 'Shift', tbltimeregister.status 'Status', " +
                        "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                        "Convert(varchar(5),tbltimeregister.in1,108) 'In',Convert(varchar(5),tbltimeregister.out2,108) 'Out' , " +
                        "Convert(varchar(5),tbltimeregister.in2,108) 'LunchIn',Convert(varchar(5),tbltimeregister.out1,108) 'Lunchout', " +
                        "case when tbltimeregister.latearrival = 0 then null else substring(convert(varchar(20),dateadd(minute,tbltimeregister.latearrival,0),108),0,6)  end 'Late', " +
                        "case when tbltimeregister.hoursworked= 0 then null else substring(convert(varchar(20),dateadd(minute,tbltimeregister.hoursworked,0),108),0,6) end 'Hourworked', " +
                        "case when tbltimeregister.earlydeparture = 0 then null else substring(convert(varchar(20),dateadd(minute,tbltimeregister.earlydeparture,0),108),0,6) end 'Early', " +
                        "case when tbltimeregister.otduration = 0 then null else substring(convert(varchar(20),dateadd(minute,tbltimeregister.otduration,0),108),0,6)  end 'OT'  from " +
                        "tblcalander,tblemployee join tbltimeregister on tblemployee.SSN=tbltimeregister.SSN join tblcompany on " +
                        "tblemployee.companycode=tblcompany.companycode " +
                        "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                        "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                        "join tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN " +
                        "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                        "join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblshiftmaster on tbltimeregister.Shift= tblshiftmaster.Shift " +
                        "where tbltimeregister.dateoffice='" + FromDate.ToString("yyyy-MM-dd") + "' " +
                        "and tblCalander.mDate = '" + FromDate.ToString("yyyy-MM-dd") + "' AND tbltimeregister.Status = 'MIS' " +
                        "And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) AND  1=1  and  1=1  ";
                if (Selection.ToString() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " order by " + SortOrder.Trim() + " ";

                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (radExcel.Checked)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            //// create table 
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            //dTbl.Columns.Add("Paycode");
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Shift");
                            dTbl.Columns.Add("IN");
                            dTbl.Columns.Add("LunchIn");
                            dTbl.Columns.Add("LunchOut");
                            dTbl.Columns.Add("OUT");
                            dTbl.Columns.Add("HoursWorked");
                            dTbl.Columns.Add("ShiftLate");
                            dTbl.Columns.Add("ShiftEarly");
                            dTbl.Columns.Add("Status");
                            dTbl.Columns.Add("OT");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "Shift";
                            dTbl.Rows[0][5] = "IN";
                            dTbl.Rows[0][6] = "Lunch In";
                            dTbl.Rows[0][7] = "Lunch Out";
                            dTbl.Rows[0][8] = "OUT";
                            dTbl.Rows[0][9] = "Hours Worked";
                            dTbl.Rows[0][10] = "Shift Late";
                            dTbl.Rows[0][11] = "Shift Early";
                            dTbl.Rows[0][12] = "Status";
                            dTbl.Rows[0][13] = "OT";


                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][4].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][10].ToString(); ;
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][12].ToString();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][13].ToString();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][11].ToString();
                                    dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1][15].ToString();
                                    dTbl.Rows[ct][10] = ds.Tables[0].Rows[i1][14].ToString();
                                    dTbl.Rows[ct][11] = ds.Tables[0].Rows[i1][16].ToString();
                                    dTbl.Rows[ct][12] = ds.Tables[0].Rows[i1][5].ToString();
                                    dTbl.Rows[ct][13] = ds.Tables[0].Rows[i1][17].ToString();

                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                 strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            // msg = "MIS Punch Report on " + FromDate.ToString("dd-MM-yyyy") + " ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {

                                msg = " MIS Punch Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "MIS Punch  Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            }
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=MISPunch.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/MISPunch.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            //// create table 
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            //dTbl.Columns.Add("Paycode");
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Shift");
                            dTbl.Columns.Add("IN");
                            dTbl.Columns.Add("LunchIn");
                            dTbl.Columns.Add("LunchOut");
                            dTbl.Columns.Add("OUT");
                            dTbl.Columns.Add("HoursWorked");
                            dTbl.Columns.Add("ShiftLate");
                            dTbl.Columns.Add("ShiftEarly");
                            dTbl.Columns.Add("Status");
                            dTbl.Columns.Add("OT");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "Shift";
                            dTbl.Rows[0][5] = "IN";
                            dTbl.Rows[0][6] = "Lunch In";
                            dTbl.Rows[0][7] = "Lunch Out";
                            dTbl.Rows[0][8] = "OUT";
                            dTbl.Rows[0][9] = "Hours Worked";
                            dTbl.Rows[0][10] = "Shift Late";
                            dTbl.Rows[0][11] = "Shift Early";
                            dTbl.Rows[0][12] = "Status";
                            dTbl.Rows[0][13] = "OT";


                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][4].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][10].ToString(); ;
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][12].ToString();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][13].ToString();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][11].ToString();
                                    dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1][15].ToString();
                                    dTbl.Rows[ct][10] = ds.Tables[0].Rows[i1][14].ToString();
                                    dTbl.Rows[ct][11] = ds.Tables[0].Rows[i1][16].ToString();
                                    dTbl.Rows[ct][12] = ds.Tables[0].Rows[i1][5].ToString();
                                    dTbl.Rows[ct][13] = ds.Tables[0].Rows[i1][17].ToString();

                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            // msg = "MIS Punch Report on " + FromDate.ToString("dd-MM-yyyy") + " ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {

                                msg = " MIS Punch Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "MIS Punch  Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            }
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "MIS.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    //Response.Redirect("Reports/DailyReports/Rpt_MisPunch.aspx");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('No record found');", true);
                    return;
                }
            }
            //Present
            else if (radPresent.Checked)
            {
                strsql = "select tblemployee.paycode 'PayCode',tblemployee.empname 'Name',tblemployee.presentcardno 'CardNumber', " +
                        "tblemployee.presentcardno 'presentcardno',tbldepartment.DEPARTMENTCODE 'departmentcode',tblcatagory.CAT 'catagorycode',tblDivision.DivisionCode 'DivisionCode', " +
                        "tblcompany.companyname as 'CompanyName',tbltimeregister.shift 'ActualShift', tbltimeregister.shiftattended 'Shift', " +
                        "Convert(varchar(5),tbltimeregister.shiftstarttime,108) 'Start',Convert(varchar(5),tbltimeregister.in1,108) 'In', " +
                        "Convert(varchar(5),tbltimeregister.out2,108) 'Out', " +
                        "case when tbltimeregister.HOURSWORKED = 0 then null else substring(convert(varchar(20),dateadd(minute,tbltimeregister.HOURSWORKED,0),108),0,6)  end 'HourWorked', " +
                        "case when tbltimeregister.status = 'MIS' then 'P' when tbltimeregister.status = 'P' then 'P' else tbltimeregister.status end 'Status', " +
                        "case when tbltimeregister.earlyarrival = 0 then null else substring(convert(varchar(20),dateadd(minute,tbltimeregister.earlyarrival,0),108),0,6)  end 'shiftEarly', " +
                        "case when tbltimeregister.latearrival = 0 then null else substring(convert(varchar(20),dateadd(minute,tbltimeregister.latearrival,0),108),0,6)  end 'shiftlate' " +
                        "from tblemployee join tbltimeregister on tblemployee.SSN=tbltimeregister.SSN " +
                        "join tbldepartment on tblemployee.departmentcode=tbldepartment.DEPARTMENTCODE  " +
                        "join tblcatagory on tblemployee.CAT=tblcatagory.CAT  " +
                        "join tbldivision on tblemployee.divisioncode=tbldivision.divisioncode " +
                        "join tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN " +
                        "join tblcompany  on tblemployee.companycode=tblcompany.companycode " +
                        " join tblgrade on tblemployee.gradecode=tblgrade.gradecode join tblshiftmaster on tbltimeregister.Shift= tblshiftmaster.Shift " +
                        "where  tbltimeregister.presentvalue > 0 and tbltimeregister.in1 is not null and " +
                        "tbltimeregister.dateoffice='" + FromDate.ToString("yyyy-MM-dd") + "' And " +
                        "(tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "' or tblemployee.LeavingDate is null) and  1=1  and  1=1  ";
                if (Selection.ToString() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " order by " + SortOrder.Trim() + " ";


                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (radExcel.Checked)
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Shift");
                            dTbl.Columns.Add("Start");
                            dTbl.Columns.Add("IN");
                            dTbl.Columns.Add("ShiftLate");
                            dTbl.Columns.Add("Status");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "Shift";
                            dTbl.Rows[0][5] = "Start";
                            dTbl.Rows[0][6] = "IN";
                            dTbl.Rows[0][7] = "Shift Late";
                            dTbl.Rows[0][8] = "Status";


                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][9].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][10].ToString(); ;
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][11].ToString();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][16].ToString();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][14].ToString();

                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                 strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            // msg = "Present Report on " + FromDate.ToString("dd-MM-yyyy") + " ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {

                                msg = " Present Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Present  Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            }
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=Present.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/Present.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        }
                    }
                    else
                    {
                        try
                        {
                            string Tmp = "";
                            int x, i;
                            System.Data.DataTable dTbl = new System.Data.DataTable();
                            DataRow dRow;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Columns.Add("SNo");
                            dTbl.Columns.Add("PayCode");
                            dTbl.Columns.Add("CardNo");
                            dTbl.Columns.Add("Name");
                            dTbl.Columns.Add("Shift");
                            dTbl.Columns.Add("Start");
                            dTbl.Columns.Add("IN");
                            dTbl.Columns.Add("ShiftLate");
                            dTbl.Columns.Add("Status");

                            dTbl.Rows[0][0] = "SNo";
                            dTbl.Rows[0][1] = "PayCode";
                            dTbl.Rows[0][2] = "Card No";
                            dTbl.Rows[0][3] = "Employee Name";
                            dTbl.Rows[0][4] = "Shift";
                            dTbl.Rows[0][5] = "Start";
                            dTbl.Rows[0][6] = "IN";
                            dTbl.Rows[0][7] = "Shift Late";
                            dTbl.Rows[0][8] = "Status";


                            int ct = 1;
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {

                                    dTbl.Rows[ct][0] = ct.ToString();
                                    dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][0].ToString();
                                    dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][2].ToString();
                                    dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][1].ToString();
                                    dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][9].ToString();
                                    dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][10].ToString(); ;
                                    dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][11].ToString();
                                    dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][16].ToString();
                                    dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][14].ToString();

                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                                return;
                            }

                            bool isEmpty;
                            for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                            {

                                isEmpty = true;
                                for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                                {
                                    if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                    {
                                        isEmpty = false;
                                        break;
                                    }
                                }

                                if (isEmpty == true)
                                {
                                    dTbl.Rows.RemoveAt(i2);
                                    i2--;
                                }
                            }
                            string companyname = "";
                            strsql = "select companyname from tblcompany";
                            if (Session["Com_Selection"] != null)
                            {
                                strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                            }
                            else if (Session["Auth_Comp"] != null)
                            {
                                strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                            }
                            DataSet dsCom = new DataSet();
                            dsCom = con.FillDataSet(strsql);
                            if (dsCom.Tables[0].Rows.Count > 0)
                            {
                                for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                                {
                                    companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                                }
                            }
                            if (!string.IsNullOrEmpty(companyname.ToString()))
                            {
                                companyname = companyname.Substring(1);
                            }

                            int colCount = Convert.ToInt32(dTbl.Columns.Count);

                            // msg = "Present Report on " + FromDate.ToString("dd-MM-yyyy") + " ";

                            if (Session["IsNepali"].ToString().Trim() == "Y")
                            {

                                msg = " Present Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                            }
                            else
                            {
                                msg = "Present  Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                            }
                            StringBuilder sb = new StringBuilder();
                            sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                            sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                            x = colCount;
                            int n, m;
                            string mm = "";
                            foreach (DataRow tr in dTbl.Rows)
                            {
                                sb.Append("<tr>");
                                for (i = 0; i < x; i++)
                                {
                                    Tmp = tr[i].ToString().Trim();
                                    mm = "";
                                    if (Tmp != "")
                                    {
                                        for (n = 0, m = 1; n < Tmp.Length; n++)
                                        {
                                            mm = mm + "0";
                                        }
                                        if (Tmp.Substring(0, 1) == "0")
                                        {
                                            if (Tmp.Contains(":"))
                                            {
                                                Tmp = Tmp;
                                            }
                                            else
                                            {
                                                Tmp = Tmp;
                                            }
                                        }
                                    }
                                    if ((i == 1) || (i == 2))
                                    {
                                        if (Tmp.ToString().Trim() != "")
                                        {
                                            //if (Tmp.ToString().Substring(0, 1) == "0")
                                            // {                                              
                                            sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                            //}
                                            //else
                                            //{
                                            //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                            // }
                                            // }
                                        }
                                    }
                                    else if ((i == 3) && (Tmp.ToString().Trim() != ""))
                                    {
                                        sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                                }
                                sb.Append("</tr>");
                            }
                            sb.Append("</table>");
                            string tbl = sb.ToString();
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "Present.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        catch (Exception ex)
                        {
                            //Response.Write(ex.Message.ToString());
                        } 
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('No record found');", true);
                    return;
                }
            }
            else if (radOverTimeSummary.Checked)
            {

                strsql = "Select TblDepartment.DepartmentCode,TblDepartment.DepartmentName, " +
                            "Case When (cast(sum(tblTimeregister.otduration) as int))/60 = 1 Then " +
                            "Convert(nvarchar(10),(cast(sum(tblTimeregister.otduration) as int))/60) +':' Else Convert(nvarchar(10),(cast(sum(tblTimeregister.otduration) as int))/60) +':' End  " +
                            " +''+  Case When (cast(sum(tblTimeregister.otduration) as int))%60 = 1 Then Convert(nvarchar(10),(cast(sum(tblTimeregister.otduration) as int))%60) Else Convert(nvarchar(10),(cast(sum(tblTimeregister.otduration) as int))%60) End 'OtDuration', " +
                            "convert(decimal(10,2),sum(tblTimeregister.otamount))'OtAmount',tblcompany.companyname 'CompanyName' from  tblcatagory, tblDivision,tblTimeregister ,tblEmployee ,tblDepartment,TBLCOMPANY,tblgrade,tblshiftmaster  " +
                            "Where tblGrade.GradeCode = tblEmployee.GradeCode And tbltimeregister.Shift= tblshiftmaster.Shift And tblEmployee.Companycode = tblCompany.Companycode AND tblEmployee.CAT = tblCatagory.cat And tblEmployee.DivisionCode = tblDivision.DivisionCode And tblTimeregister.SSN = TblEmployee.SSN " +
                            "And  TblEmployee.DepartmentCode = TblDepartment.DepartmentCode AND tbltimeregister.DateOffice ='" + FromDate.ToString("yyyy-MM-dd") + "'  And  (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "'  or tblemployee.LeavingDate is null) AND  1=1  " +
                            "and  1=1  ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += Selection.ToString();
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " group by TblDepartment.DepartmentCode,TblDepartment.DepartmentName,tblcompany.companyname order by tbldepartment.departmentname";
                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                   
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('No record found');", true);
                    return;
                }

            }
            else if (radContAbsent1.Checked)
            {
                if (radExcel.Checked)
                {
                    try
                    {
                        int mlngStartingNoOfDays = 0;
                        if (txtDays.Text.ToString() == "")
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('Please Enter Number of days.');", true);
                            txtDays.Focus();
                            txtToDate.Visible = true;
                           // imgCal.Visible = true;
                            lblto.Visible = true;
                            Tday.Visible = true;
                            return;
                        }
                        else
                        {
                            mlngStartingNoOfDays = Convert.ToInt32(txtDays.Text);
                        }


                        string field1 = "", field2 = "", tablename = "";
                        string Tmp = "";
                        int x, i;
                        //// create table 
                        System.Data.DataTable dTbl = new System.Data.DataTable();
                        DataRow dRow;

                        dTbl.Columns.Add("SNo");
                        dTbl.Columns.Add("Paycode");
                        dTbl.Columns.Add("SBU");
                        dTbl.Columns.Add("Name");
                        dTbl.Columns.Add("Absent Since");
                        dTbl.Columns.Add("Days");
                        dTbl.Columns.Add("Department");
                        dTbl.Columns.Add("HOD");





                        int ct = 0;
                        strsql = "";
                        strsql = " select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, " +
                                    " tblEmployee.Cat,tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.DateOffice, " +
                                    " tbltimeregister.AbsentValue,tblEmployee.Bus ,'HOD'=(select empname from tblemployee tblemp where tblemp.paycode=tblemployee.headid) from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCompany,tblDepartment,tblgrade,tblshiftmaster Where tblGrade.GradeCode = tblEmployee.GradeCode And tblCatagory.Cat = tblEmployee.Cat " +
                                    " And tbltimeregister.Shift= tblshiftmaster.Shift And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.AbsentValue = 1 and " +
                                    " tbltimeregister.DateOffice = '" + FromDate.ToString("yyyy-MM-dd") + "' And  " +
                                    " tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode And tblemployee.LeavingDate is null  AND  1=1  and  1=1 ";
                        if (Selection.ToString().Trim() != "")
                        {
                            strsql = strsql + Selection.ToString();
                        }
                        strsql += " " + CompanySelection.ToString() + " ";
                        strsql += " order by " + ddlSorting.SelectedItem.Value + " ";

                        //Response.Write(strsql.ToString());
                        ct = 0;
                        DataSet dsResult = new DataSet();
                        DataSet dsResult1 = new DataSet();
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        dTbl.Rows[ct]["SNo"] = "SNo";
                        dTbl.Rows[ct]["Paycode"] = "PayCode";
                        dTbl.Rows[ct]["SBU"] = Global.getEmpInfo._Category;
                        dTbl.Rows[ct]["Name"] = "Employee Name";
                        dTbl.Rows[ct]["Absent Since"] = "Absent Since";
                        dTbl.Rows[ct]["Days"] = "Days";
                        dTbl.Rows[ct]["Department"] = Global.getEmpInfo._Dept;
                        dTbl.Rows[ct]["HOD"] = "HOD";




                        ct = ct + 1;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        dsResult = con.FillDataSet(strsql);
                        bool mcontinuse = false;
                        int lngNoOfDaysForPrint = 0;
                        string dtmDateForPrint = "";
                        int mCount = 0;


                        if (dsResult.Tables[0].Rows.Count > 0)
                        {
                            for (int i1 = 0; i1 < dsResult.Tables[0].Rows.Count; i1++)
                            {
                                lngNoOfDaysForPrint = 0;
                                strsql = "select * from tbltimeregister Where  DateOffice <= getdate() " +
                                    "  and paycode='" + dsResult.Tables[0].Rows[i1]["Paycode"].ToString().Trim() + "' order by dateoffice ";
                                dsResult1 = con.FillDataSet(strsql);
                                if (dsResult1.Tables[0].Rows.Count > 0)
                                {
                                    for (int abs = Convert.ToInt32(dsResult1.Tables[0].Rows.Count) - 1; abs >= 0; abs--)
                                    {
                                        if ((Convert.ToDouble(dsResult1.Tables[0].Rows[abs]["ABSENTVALUE"]) > 0) || (Convert.ToDouble(dsResult1.Tables[0].Rows[abs]["WO_VALUE"]) > 0))
                                        {
                                            dtmDateForPrint = Convert.ToDateTime(dsResult1.Tables[0].Rows[abs]["DATEOFFICE"]).ToString("dd/MM/yyyy");
                                            mcontinuse = true;
                                        }
                                        else
                                        {
                                            mcontinuse = false;
                                        }

                                        if (mcontinuse)
                                        {
                                            lngNoOfDaysForPrint = lngNoOfDaysForPrint + 1;
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                }

                                if (lngNoOfDaysForPrint >= mlngStartingNoOfDays)
                                {
                                    mCount = mCount + 1;
                                    dTbl.Rows[ct]["SNo"] = mCount.ToString();
                                    dTbl.Rows[ct]["Paycode"] = dsResult.Tables[0].Rows[i1]["Paycode"].ToString();
                                    dTbl.Rows[ct]["Name"] = dsResult.Tables[0].Rows[i1]["EmpName"].ToString();
                                    dTbl.Rows[ct]["Department"] = dsResult.Tables[0].Rows[i1]["DepartmentName"].ToString();
                                    dTbl.Rows[ct]["SBU"] = dsResult.Tables[0].Rows[i1]["CatagoryName"].ToString();
                                    dTbl.Rows[ct]["Absent Since"] = dtmDateForPrint.ToString();
                                    dTbl.Rows[ct]["Days"] = lngNoOfDaysForPrint.ToString();
                                    dTbl.Rows[ct]["HOD"] = dsResult.Tables[0].Rows[i1]["HOD"].ToString();
                                    ct = ct + 1;
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        if (dTbl.Rows.Count == 2)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        bool isEmpty;
                        for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                        {

                            isEmpty = true;
                            for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                            {
                                if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                {
                                    isEmpty = false;
                                    break;
                                }
                            }

                            if (isEmpty == true)
                            {
                                dTbl.Rows.RemoveAt(i2);
                                i2--;
                            }
                        }

                        string companyname = "";

                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                             strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }
                        int count1 = Convert.ToInt32(dTbl.Columns.Count);
                        // msg = " ABSENCE FROM DUTY FROM :" + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";

                        if (Session["IsNepali"].ToString().Trim() == "Y")
                        {

                            msg = " ABSENCE FROM DUTY Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                        }
                        else
                        {
                            msg = "ABSENCE FROM DUTY  Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                        }

                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: left'></td></tr> ");

                        x = dTbl.Columns.Count;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                }
                                if (Convert.ToInt32(Tmp.ToString().Length) >= 15)
                                    //sb.Append("<td style='width:180px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:180px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                else
                                    //sb.Append("<td style='width:75px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:75px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));



                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=ContAbsence.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/ContAbsence.xls";
                        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                        Response.Write(sb.ToString());
                        Response.End();
                    }
                    catch
                    {

                    }
                }
                else
                {
                    try
                    {
                        int mlngStartingNoOfDays = 0;
                        if (txtDays.Text.ToString() == "")
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('Please Enter Number of days.');", true);
                            txtDays.Focus();
                            txtToDate.Visible = true;
                            // imgCal.Visible = true;
                            lblto.Visible = true;
                            Tday.Visible = true;
                            return;
                        }
                        else
                        {
                            mlngStartingNoOfDays = Convert.ToInt32(txtDays.Text);
                        }


                        string field1 = "", field2 = "", tablename = "";
                        string Tmp = "";
                        int x, i;
                        //// create table 
                        System.Data.DataTable dTbl = new System.Data.DataTable();
                        DataRow dRow;

                        dTbl.Columns.Add("SNo");
                        dTbl.Columns.Add("Paycode");
                        dTbl.Columns.Add("SBU");
                        dTbl.Columns.Add("Name");
                        dTbl.Columns.Add("Absent Since");
                        dTbl.Columns.Add("Days");
                        dTbl.Columns.Add("Department");
                        dTbl.Columns.Add("HOD");





                        int ct = 0;
                        strsql = "";
                        strsql = " select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, " +
                                    " tblEmployee.Cat,tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.DateOffice, " +
                                    " tbltimeregister.AbsentValue,tblEmployee.Bus ,'HOD'=(select empname from tblemployee tblemp where tblemp.paycode=tblemployee.headid) from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCompany,tblDepartment,tblgrade,tblshiftmaster Where tblGrade.GradeCode = tblEmployee.GradeCode And tblCatagory.Cat = tblEmployee.Cat " +
                                    " And tbltimeregister.Shift= tblshiftmaster.Shift And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.AbsentValue = 1 and " +
                                    " tbltimeregister.DateOffice = '" + FromDate.ToString("yyyy-MM-dd") + "' And  " +
                                    " tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode And tblemployee.LeavingDate is null  AND  1=1  and  1=1 ";
                        if (Selection.ToString().Trim() != "")
                        {
                            strsql = strsql + Selection.ToString();
                        }
                        strsql += " " + CompanySelection.ToString() + " ";
                        strsql += " order by " + ddlSorting.SelectedItem.Value + " ";

                        //Response.Write(strsql.ToString());
                        ct = 0;
                        DataSet dsResult = new DataSet();
                        DataSet dsResult1 = new DataSet();
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        dTbl.Rows[ct]["SNo"] = "SNo";
                        dTbl.Rows[ct]["Paycode"] = "PayCode";
                        dTbl.Rows[ct]["SBU"] = Global.getEmpInfo._Category;
                        dTbl.Rows[ct]["Name"] = "Employee Name";
                        dTbl.Rows[ct]["Absent Since"] = "Absent Since";
                        dTbl.Rows[ct]["Days"] = "Days";
                        dTbl.Rows[ct]["Department"] = Global.getEmpInfo._Dept;
                        dTbl.Rows[ct]["HOD"] = "HOD";




                        ct = ct + 1;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        dsResult = con.FillDataSet(strsql);
                        bool mcontinuse = false;
                        int lngNoOfDaysForPrint = 0;
                        string dtmDateForPrint = "";
                        int mCount = 0;


                        if (dsResult.Tables[0].Rows.Count > 0)
                        {
                            for (int i1 = 0; i1 < dsResult.Tables[0].Rows.Count; i1++)
                            {
                                lngNoOfDaysForPrint = 0;
                                strsql = "select * from tbltimeregister Where  DateOffice <= getdate() " +
                                    "  and paycode='" + dsResult.Tables[0].Rows[i1]["Paycode"].ToString().Trim() + "' order by dateoffice ";
                                dsResult1 = con.FillDataSet(strsql);
                                if (dsResult1.Tables[0].Rows.Count > 0)
                                {
                                    for (int abs = Convert.ToInt32(dsResult1.Tables[0].Rows.Count) - 1; abs >= 0; abs--)
                                    {
                                        if ((Convert.ToDouble(dsResult1.Tables[0].Rows[abs]["ABSENTVALUE"]) > 0) || (Convert.ToDouble(dsResult1.Tables[0].Rows[abs]["WO_VALUE"]) > 0))
                                        {
                                            dtmDateForPrint = Convert.ToDateTime(dsResult1.Tables[0].Rows[abs]["DATEOFFICE"]).ToString("dd/MM/yyyy");
                                            mcontinuse = true;
                                        }
                                        else
                                        {
                                            mcontinuse = false;
                                        }

                                        if (mcontinuse)
                                        {
                                            lngNoOfDaysForPrint = lngNoOfDaysForPrint + 1;
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                }

                                if (lngNoOfDaysForPrint >= mlngStartingNoOfDays)
                                {
                                    mCount = mCount + 1;
                                    dTbl.Rows[ct]["SNo"] = mCount.ToString();
                                    dTbl.Rows[ct]["Paycode"] = dsResult.Tables[0].Rows[i1]["Paycode"].ToString();
                                    dTbl.Rows[ct]["Name"] = dsResult.Tables[0].Rows[i1]["EmpName"].ToString();
                                    dTbl.Rows[ct]["Department"] = dsResult.Tables[0].Rows[i1]["DepartmentName"].ToString();
                                    dTbl.Rows[ct]["SBU"] = dsResult.Tables[0].Rows[i1]["CatagoryName"].ToString();
                                    dTbl.Rows[ct]["Absent Since"] = dtmDateForPrint.ToString();
                                    dTbl.Rows[ct]["Days"] = lngNoOfDaysForPrint.ToString();
                                    dTbl.Rows[ct]["HOD"] = dsResult.Tables[0].Rows[i1]["HOD"].ToString();
                                    ct = ct + 1;
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        if (dTbl.Rows.Count == 2)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        bool isEmpty;
                        for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                        {

                            isEmpty = true;
                            for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                            {
                                if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                {
                                    isEmpty = false;
                                    break;
                                }
                            }

                            if (isEmpty == true)
                            {
                                dTbl.Rows.RemoveAt(i2);
                                i2--;
                            }
                        }

                        string companyname = "";

                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }
                        int count1 = Convert.ToInt32(dTbl.Columns.Count);
                        // msg = " ABSENCE FROM DUTY FROM :" + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";

                        if (Session["IsNepali"].ToString().Trim() == "Y")
                        {

                            msg = " ABSENCE FROM DUTY Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                        }
                        else
                        {
                            msg = "ABSENCE FROM DUTY  Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                        }

                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: left'></td></tr> ");

                        x = dTbl.Columns.Count;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                }
                                if (Convert.ToInt32(Tmp.ToString().Length) >= 15)
                                    //sb.Append("<td style='width:180px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:180px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                else
                                    //sb.Append("<td style='width:75px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:75px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));



                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                        string pdffilename = "ContAbs.pdf";
                        PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                        pdfDocument.Open();
                        String htmlText = sb.ToString();
                        StringReader str = new StringReader(htmlText);
                        HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                        htmlworker.Parse(str);
                        pdfWriter.CloseStream = false;
                        pdfDocument.Close();
                        //Download Pdf  
                        Response.Buffer = true;
                        Response.ContentType = "application/pdf";
                        Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.Write(pdfDocument);
                        Response.Flush();
                        Response.End();
                    }
                    catch
                    {

                    }
                }

            }
            else if (radcontlatearrival1.Checked)
            {
                if (radExcel.Checked)
                {
                    try
                    {
                        int mlngStartingNoOfDays = 0;
                        if (txtDays.Text.ToString() == "")
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('Please Enter Number of days.');", true);
                            txtDays.Focus();
                            txtToDate.Visible = true;
                           /// imgCal.Visible = true;
                            lblto.Visible = true;
                           // tddays.Visible = true;
                            return;
                        }
                        else
                        {
                            mlngStartingNoOfDays = Convert.ToInt32(txtDays.Text);
                        }


                        string field1 = "", field2 = "", tablename = "";
                        string Tmp = "";
                        int x, i;
                        //// create table 
                        System.Data.DataTable dTbl = new System.Data.DataTable();
                        DataRow dRow;

                        dTbl.Columns.Add("SNo");
                        dTbl.Columns.Add("Paycode");
                        dTbl.Columns.Add("PresentCardNo");
                        dTbl.Columns.Add("Name");
                        dTbl.Columns.Add("Absent Since");
                        dTbl.Columns.Add("Days");
                        dTbl.Columns.Add("Department");
                        dTbl.Columns.Add("HOD");





                        int ct = 0;
                        strsql = "";
                        strsql = " select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, " +
                                    " tblEmployee.Cat,tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.DateOffice, " +
                                    " tbltimeregister.LateArrival,tblEmployee.Bus ,'HOD'=(select empname from tblemployee tblemp where tblemp.paycode=tblemployee.headid) from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCompany,tblDepartment,tblgrade,tblshiftmaster Where tblGrade.GradeCode = tblEmployee.GradeCode And tblCatagory.Cat = tblEmployee.Cat " +
                                    " And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.Shift= tblshiftmaster.Shift and " +
                                    " tbltimeregister.DateOffice = '" + FromDate.ToString("yyyy-MM-dd") + "' And  " +
                                    " tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode And tblemployee.LeavingDate is null  AND  1=1  and  1=1 ";
                        if (Selection.ToString().Trim() != "")
                        {
                            strsql = strsql + Selection.ToString();
                        }
                        strsql += " " + CompanySelection.ToString() + " ";
                        strsql += " order by " + ddlSorting.SelectedItem.Value + " ";

                        //Response.Write(strsql.ToString());
                        ct = 0;
                        DataSet dsResult = new DataSet();
                        DataSet dsResult1 = new DataSet();
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        dTbl.Rows[ct]["SNo"] = "SNo";
                        dTbl.Rows[ct]["Paycode"] = "PayCode";
                        dTbl.Rows[ct]["PresentCardNo"] = "Card No";
                        dTbl.Rows[ct]["Name"] = "Employee Name";
                        dTbl.Rows[ct]["Absent Since"] = "Late Since";
                        dTbl.Rows[ct]["Days"] = "Days";
                        dTbl.Rows[ct]["Department"] = Global.getEmpInfo._Dept;
                        dTbl.Rows[ct]["HOD"] = "HOD";




                        ct = ct + 1;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        dsResult = con.FillDataSet(strsql);
                        bool mcontinuse = false;
                        int lngNoOfDaysForPrint = 0;
                        string dtmDateForPrint = "";
                        int mCount = 0;


                        if (dsResult.Tables[0].Rows.Count > 0)
                        {
                            for (int i1 = 0; i1 < dsResult.Tables[0].Rows.Count; i1++)
                            {
                                lngNoOfDaysForPrint = 0;
                                strsql = "select * from tbltimeregister Where  DateOffice <= getdate() " +
                                    "  and paycode='" + dsResult.Tables[0].Rows[i1]["Paycode"].ToString().Trim() + "' order by dateoffice ";
                                dsResult1 = con.FillDataSet(strsql);
                                if (dsResult1.Tables[0].Rows.Count > 0)
                                {
                                    for (int abs = Convert.ToInt32(dsResult1.Tables[0].Rows.Count) - 1; abs >= 0; abs--)
                                    {
                                        if ((Convert.ToDouble(dsResult1.Tables[0].Rows[abs]["LateArrival"]) > 0))
                                        {
                                            dtmDateForPrint = Convert.ToDateTime(dsResult1.Tables[0].Rows[abs]["DATEOFFICE"]).ToString("dd/MM/yyyy");
                                            mcontinuse = true;
                                        }
                                        else
                                        {
                                            mcontinuse = false;
                                        }

                                        if (mcontinuse)
                                        {
                                            lngNoOfDaysForPrint = lngNoOfDaysForPrint + 1;
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                }

                                if (lngNoOfDaysForPrint >= mlngStartingNoOfDays)
                                {
                                    mCount = mCount + 1;
                                    dTbl.Rows[ct]["SNo"] = mCount.ToString();
                                    dTbl.Rows[ct]["Paycode"] = dsResult.Tables[0].Rows[i1]["Paycode"].ToString();
                                    dTbl.Rows[ct]["Name"] = dsResult.Tables[0].Rows[i1]["EmpName"].ToString();
                                    dTbl.Rows[ct]["Department"] = dsResult.Tables[0].Rows[i1]["DepartmentName"].ToString();
                                    dTbl.Rows[ct]["PresentCardNo"] = dsResult.Tables[0].Rows[i1]["PresentCardNo"].ToString();
                                    dTbl.Rows[ct]["Absent Since"] = dtmDateForPrint.ToString();
                                    dTbl.Rows[ct]["Days"] = lngNoOfDaysForPrint.ToString();
                                    dTbl.Rows[ct]["HOD"] = dsResult.Tables[0].Rows[i1]["HOD"].ToString();
                                    ct = ct + 1;
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        if (dTbl.Rows.Count == 2)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        bool isEmpty;
                        for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                        {

                            isEmpty = true;
                            for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                            {
                                if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                {
                                    isEmpty = false;
                                    break;
                                }
                            }

                            if (isEmpty == true)
                            {
                                dTbl.Rows.RemoveAt(i2);
                                i2--;
                            }
                        }

                        string companyname = "";

                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                             strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }
                        int count1 = Convert.ToInt32(dTbl.Columns.Count);
                        //  msg = " LATE FROM DUTY FROM :" + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";

                        if (Session["IsNepali"].ToString().Trim() == "Y")
                        {

                            msg = " LATE FROM DUTY Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                        }
                        else
                        {
                            msg = "LATE FROM DUTY  Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                        }

                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: left'></td></tr> ");

                        x = dTbl.Columns.Count;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                }
                                if (Convert.ToInt32(Tmp.ToString().Length) >= 15)
                                    //sb.Append("<td style='width:180px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:180px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                else
                                    //sb.Append("<td style='width:75px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:75px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));



                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=ContLate.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/ContLate.xls";
                        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                        Response.Write(sb.ToString());
                        Response.End();
                    }
                    catch
                    {

                    }
                }
                else
                {
                    try
                    {
                        int mlngStartingNoOfDays = 0;
                        if (txtDays.Text.ToString() == "")
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('Please Enter Number of days.');", true);
                            txtDays.Focus();
                            txtToDate.Visible = true;
                            /// imgCal.Visible = true;
                            lblto.Visible = true;
                            // tddays.Visible = true;
                            return;
                        }
                        else
                        {
                            mlngStartingNoOfDays = Convert.ToInt32(txtDays.Text);
                        }


                        string field1 = "", field2 = "", tablename = "";
                        string Tmp = "";
                        int x, i;
                        //// create table 
                        System.Data.DataTable dTbl = new System.Data.DataTable();
                        DataRow dRow;

                        dTbl.Columns.Add("SNo");
                        dTbl.Columns.Add("Paycode");
                        dTbl.Columns.Add("PresentCardNo");
                        dTbl.Columns.Add("Name");
                        dTbl.Columns.Add("Absent Since");
                        dTbl.Columns.Add("Days");
                        dTbl.Columns.Add("Department");
                        dTbl.Columns.Add("HOD");





                        int ct = 0;
                        strsql = "";
                        strsql = " select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, " +
                                    " tblEmployee.Cat,tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.DateOffice, " +
                                    " tbltimeregister.LateArrival,tblEmployee.Bus ,'HOD'=(select empname from tblemployee tblemp where tblemp.paycode=tblemployee.headid) from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCompany,tblDepartment,tblgrade,tblshiftmaster Where tblGrade.GradeCode = tblEmployee.GradeCode And tblCatagory.Cat = tblEmployee.Cat " +
                                    " And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.Shift= tblshiftmaster.Shift and " +
                                    " tbltimeregister.DateOffice = '" + FromDate.ToString("yyyy-MM-dd") + "' And  " +
                                    " tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode And tblemployee.LeavingDate is null  AND  1=1  and  1=1 ";
                        if (Selection.ToString().Trim() != "")
                        {
                            strsql = strsql + Selection.ToString();
                        }
                        strsql += " " + CompanySelection.ToString() + " ";
                        strsql += " order by " + ddlSorting.SelectedItem.Value + " ";

                        //Response.Write(strsql.ToString());
                        ct = 0;
                        DataSet dsResult = new DataSet();
                        DataSet dsResult1 = new DataSet();
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        dTbl.Rows[ct]["SNo"] = "SNo";
                        dTbl.Rows[ct]["Paycode"] = "PayCode";
                        dTbl.Rows[ct]["PresentCardNo"] = "Card No";
                        dTbl.Rows[ct]["Name"] = "Employee Name";
                        dTbl.Rows[ct]["Absent Since"] = "Late Since";
                        dTbl.Rows[ct]["Days"] = "Days";
                        dTbl.Rows[ct]["Department"] = Global.getEmpInfo._Dept;
                        dTbl.Rows[ct]["HOD"] = "HOD";




                        ct = ct + 1;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        dsResult = con.FillDataSet(strsql);
                        bool mcontinuse = false;
                        int lngNoOfDaysForPrint = 0;
                        string dtmDateForPrint = "";
                        int mCount = 0;


                        if (dsResult.Tables[0].Rows.Count > 0)
                        {
                            for (int i1 = 0; i1 < dsResult.Tables[0].Rows.Count; i1++)
                            {
                                lngNoOfDaysForPrint = 0;
                                strsql = "select * from tbltimeregister Where  DateOffice <= getdate() " +
                                    "  and paycode='" + dsResult.Tables[0].Rows[i1]["Paycode"].ToString().Trim() + "' order by dateoffice ";
                                dsResult1 = con.FillDataSet(strsql);
                                if (dsResult1.Tables[0].Rows.Count > 0)
                                {
                                    for (int abs = Convert.ToInt32(dsResult1.Tables[0].Rows.Count) - 1; abs >= 0; abs--)
                                    {
                                        if ((Convert.ToDouble(dsResult1.Tables[0].Rows[abs]["LateArrival"]) > 0))
                                        {
                                            dtmDateForPrint = Convert.ToDateTime(dsResult1.Tables[0].Rows[abs]["DATEOFFICE"]).ToString("dd/MM/yyyy");
                                            mcontinuse = true;
                                        }
                                        else
                                        {
                                            mcontinuse = false;
                                        }

                                        if (mcontinuse)
                                        {
                                            lngNoOfDaysForPrint = lngNoOfDaysForPrint + 1;
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                }

                                if (lngNoOfDaysForPrint >= mlngStartingNoOfDays)
                                {
                                    mCount = mCount + 1;
                                    dTbl.Rows[ct]["SNo"] = mCount.ToString();
                                    dTbl.Rows[ct]["Paycode"] = dsResult.Tables[0].Rows[i1]["Paycode"].ToString();
                                    dTbl.Rows[ct]["Name"] = dsResult.Tables[0].Rows[i1]["EmpName"].ToString();
                                    dTbl.Rows[ct]["Department"] = dsResult.Tables[0].Rows[i1]["DepartmentName"].ToString();
                                    dTbl.Rows[ct]["PresentCardNo"] = dsResult.Tables[0].Rows[i1]["PresentCardNo"].ToString();
                                    dTbl.Rows[ct]["Absent Since"] = dtmDateForPrint.ToString();
                                    dTbl.Rows[ct]["Days"] = lngNoOfDaysForPrint.ToString();
                                    dTbl.Rows[ct]["HOD"] = dsResult.Tables[0].Rows[i1]["HOD"].ToString();
                                    ct = ct + 1;
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        if (dTbl.Rows.Count == 2)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        bool isEmpty;
                        for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                        {

                            isEmpty = true;
                            for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                            {
                                if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                {
                                    isEmpty = false;
                                    break;
                                }
                            }

                            if (isEmpty == true)
                            {
                                dTbl.Rows.RemoveAt(i2);
                                i2--;
                            }
                        }

                        string companyname = "";

                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }
                        int count1 = Convert.ToInt32(dTbl.Columns.Count);
                        //  msg = " LATE FROM DUTY FROM :" + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";

                        if (Session["IsNepali"].ToString().Trim() == "Y")
                        {

                            msg = " LATE FROM DUTY Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                        }
                        else
                        {
                            msg = "LATE FROM DUTY  Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                        }

                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: left'></td></tr> ");

                        x = dTbl.Columns.Count;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                }
                                if (Convert.ToInt32(Tmp.ToString().Length) >= 15)
                                    //sb.Append("<td style='width:180px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:180px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                else
                                    //sb.Append("<td style='width:75px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:75px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));



                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                        string pdffilename = "ContLate.pdf";
                        PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                        pdfDocument.Open();
                        String htmlText = sb.ToString();
                        StringReader str = new StringReader(htmlText);
                        HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                        htmlworker.Parse(str);
                        pdfWriter.CloseStream = false;
                        pdfDocument.Close();
                        //Download Pdf  
                        Response.Buffer = true;
                        Response.ContentType = "application/pdf";
                        Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.Write(pdfDocument);
                        Response.Flush();
                        Response.End();
                    }
                    catch
                    {

                    }
                }

            }
            else if (radContEarlyDept1.Checked)
            {
                if (radExcel.Checked)
                {
                    try
                    {
                        int mlngStartingNoOfDays = 0;
                        if (txtDays.Text.ToString() == "")
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('Please Enter Number of days.');", true);
                            txtDays.Focus();
                            txtToDate.Visible = true;
                           // imgCal.Visible = true;
                            lblto.Visible = true;
                           // tddays.Visible = true;
                            return;
                        }
                        else
                        {
                            mlngStartingNoOfDays = Convert.ToInt32(txtDays.Text);
                        }


                        string field1 = "", field2 = "", tablename = "";
                        string Tmp = "";
                        int x, i;
                        //// create table 
                        System.Data.DataTable dTbl = new System.Data.DataTable();
                        DataRow dRow;

                        dTbl.Columns.Add("SNo");
                        dTbl.Columns.Add("Paycode");
                        dTbl.Columns.Add("PresentCardNo");
                        dTbl.Columns.Add("Name");
                        dTbl.Columns.Add("Absent Since");
                        dTbl.Columns.Add("Days");
                        dTbl.Columns.Add("Department");
                        dTbl.Columns.Add("HOD");





                        int ct = 0;
                        strsql = "";
                        strsql = " select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, " +
                                    " tblEmployee.Cat,tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.DateOffice, " +
                                    " tbltimeregister.earlydeparture,tblEmployee.Bus ,'HOD'=(select empname from tblemployee tblemp where tblemp.paycode=tblemployee.headid) from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCompany,tblDepartment,tblgrade,tblshiftmaster  Where tblGrade.GradeCode = tblEmployee.GradeCode And tblCatagory.Cat = tblEmployee.Cat " +
                                    " And tbltimeregister.Shift= tblshiftmaster.Shift And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregister.PayCode = tblEmployee.PayCode And  " +
                                    " tbltimeregister.DateOffice = '" + FromDate.ToString("yyyy-MM-dd") + "' And  " +
                                    " tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode And tblemployee.LeavingDate is null  AND  1=1  and  1=1 ";
                        if (Selection.ToString().Trim() != "")
                        {
                            strsql = strsql + Selection.ToString();
                        }
                        strsql += " " + CompanySelection.ToString() + " ";
                        strsql += " order by " + SortOrder.Trim() + " ";

                        //Response.Write(strsql.ToString());
                        ct = 0;
                        DataSet dsResult = new DataSet();
                        DataSet dsResult1 = new DataSet();
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        dTbl.Rows[ct]["SNo"] = "SNo";
                        dTbl.Rows[ct]["Paycode"] = "PayCode";
                        dTbl.Rows[ct]["PresentCardNo"] = "Card No";
                        dTbl.Rows[ct]["Name"] = "Employee Name";
                        dTbl.Rows[ct]["Absent Since"] = "Early Since";
                        dTbl.Rows[ct]["Days"] = "Days";
                        dTbl.Rows[ct]["Department"] = Global.getEmpInfo._Dept;
                        dTbl.Rows[ct]["HOD"] = "HOD";




                        ct = ct + 1;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        dsResult = con.FillDataSet(strsql);
                        bool mcontinuse = false;
                        int lngNoOfDaysForPrint = 0;
                        string dtmDateForPrint = "";
                        int mCount = 0;


                        if (dsResult.Tables[0].Rows.Count > 0)
                        {
                            for (int i1 = 0; i1 < dsResult.Tables[0].Rows.Count; i1++)
                            {
                                lngNoOfDaysForPrint = 0;
                                strsql = "select * from tbltimeregister Where  DateOffice <= getdate() " +
                                    "  and paycode='" + dsResult.Tables[0].Rows[i1]["Paycode"].ToString().Trim() + "' order by dateoffice ";
                                dsResult1 = con.FillDataSet(strsql);
                                if (dsResult1.Tables[0].Rows.Count > 0)
                                {
                                    for (int abs = Convert.ToInt32(dsResult1.Tables[0].Rows.Count) - 1; abs >= 0; abs--)
                                    {
                                        if ((Convert.ToDouble(dsResult1.Tables[0].Rows[abs]["earlydeparture"]) > 0))
                                        {
                                            dtmDateForPrint = Convert.ToDateTime(dsResult1.Tables[0].Rows[abs]["DATEOFFICE"]).ToString("dd/MM/yyyy");
                                            mcontinuse = true;
                                        }
                                        else
                                        {
                                            mcontinuse = false;
                                        }

                                        if (mcontinuse)
                                        {
                                            lngNoOfDaysForPrint = lngNoOfDaysForPrint + 1;
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                }

                                if (lngNoOfDaysForPrint >= mlngStartingNoOfDays)
                                {
                                    mCount = mCount + 1;
                                    dTbl.Rows[ct]["SNo"] = mCount.ToString();
                                    dTbl.Rows[ct]["Paycode"] = dsResult.Tables[0].Rows[i1]["Paycode"].ToString();
                                    dTbl.Rows[ct]["Name"] = dsResult.Tables[0].Rows[i1]["EmpName"].ToString();
                                    dTbl.Rows[ct]["Department"] = dsResult.Tables[0].Rows[i1]["DepartmentName"].ToString();
                                    dTbl.Rows[ct]["PresentCardNo"] = dsResult.Tables[0].Rows[i1]["PresentCardNo"].ToString();
                                    dTbl.Rows[ct]["Absent Since"] = dtmDateForPrint.ToString();
                                    dTbl.Rows[ct]["Days"] = lngNoOfDaysForPrint.ToString();
                                    dTbl.Rows[ct]["HOD"] = dsResult.Tables[0].Rows[i1]["HOD"].ToString();
                                    ct = ct + 1;
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        if (dTbl.Rows.Count == 2)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        bool isEmpty;
                        for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                        {

                            isEmpty = true;
                            for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                            {
                                if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                {
                                    isEmpty = false;
                                    break;
                                }
                            }

                            if (isEmpty == true)
                            {
                                dTbl.Rows.RemoveAt(i2);
                                i2--;
                            }
                        }

                        string companyname = "";

                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                             strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }
                        int count1 = Convert.ToInt32(dTbl.Columns.Count);
                        //  msg = " LATE FROM DUTY FROM :" + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";

                        if (Session["IsNepali"].ToString().Trim() == "Y")
                        {

                            msg = " LATE FROM DUTY Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                        }
                        else
                        {
                            msg = "LATE FROM DUTY  Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                        }

                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: left'></td></tr> ");

                        x = dTbl.Columns.Count;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                }
                                if (Convert.ToInt32(Tmp.ToString().Length) >= 15)
                                    //sb.Append("<td style='width:180px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:180px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                else
                                    //sb.Append("<td style='width:75px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:75px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));



                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=ContEarlyDeparture.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/ContEarlyDeparture.xls";
                        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                        Response.Write(sb.ToString());
                        Response.End();
                    }
                    catch
                    {

                    }
                }
                else
                {
                    try
                    {
                        int mlngStartingNoOfDays = 0;
                        if (txtDays.Text.ToString() == "")
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('Please Enter Number of days.');", true);
                            txtDays.Focus();
                            txtToDate.Visible = true;
                            // imgCal.Visible = true;
                            lblto.Visible = true;
                            // tddays.Visible = true;
                            return;
                        }
                        else
                        {
                            mlngStartingNoOfDays = Convert.ToInt32(txtDays.Text);
                        }


                        string field1 = "", field2 = "", tablename = "";
                        string Tmp = "";
                        int x, i;
                        //// create table 
                        System.Data.DataTable dTbl = new System.Data.DataTable();
                        DataRow dRow;

                        dTbl.Columns.Add("SNo");
                        dTbl.Columns.Add("Paycode");
                        dTbl.Columns.Add("PresentCardNo");
                        dTbl.Columns.Add("Name");
                        dTbl.Columns.Add("Absent Since");
                        dTbl.Columns.Add("Days");
                        dTbl.Columns.Add("Department");
                        dTbl.Columns.Add("HOD");





                        int ct = 0;
                        strsql = "";
                        strsql = " select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, " +
                                    " tblEmployee.Cat,tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.DateOffice, " +
                                    " tbltimeregister.earlydeparture,tblEmployee.Bus ,'HOD'=(select empname from tblemployee tblemp where tblemp.paycode=tblemployee.headid) from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCompany,tblDepartment,tblgrade,tblshiftmaster  Where tblGrade.GradeCode = tblEmployee.GradeCode And tblCatagory.Cat = tblEmployee.Cat " +
                                    " And tbltimeregister.Shift= tblshiftmaster.Shift And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregister.PayCode = tblEmployee.PayCode And  " +
                                    " tbltimeregister.DateOffice = '" + FromDate.ToString("yyyy-MM-dd") + "' And  " +
                                    " tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode And tblemployee.LeavingDate is null  AND  1=1  and  1=1 ";
                        if (Selection.ToString().Trim() != "")
                        {
                            strsql = strsql + Selection.ToString();
                        }
                        strsql += " " + CompanySelection.ToString() + " ";
                        strsql += " order by " + SortOrder.Trim() + " ";

                        //Response.Write(strsql.ToString());
                        ct = 0;
                        DataSet dsResult = new DataSet();
                        DataSet dsResult1 = new DataSet();
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        dTbl.Rows[ct]["SNo"] = "SNo";
                        dTbl.Rows[ct]["Paycode"] = "PayCode";
                        dTbl.Rows[ct]["PresentCardNo"] = "Card No";
                        dTbl.Rows[ct]["Name"] = "Employee Name";
                        dTbl.Rows[ct]["Absent Since"] = "Early Since";
                        dTbl.Rows[ct]["Days"] = "Days";
                        dTbl.Rows[ct]["Department"] = Global.getEmpInfo._Dept;
                        dTbl.Rows[ct]["HOD"] = "HOD";




                        ct = ct + 1;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        dsResult = con.FillDataSet(strsql);
                        bool mcontinuse = false;
                        int lngNoOfDaysForPrint = 0;
                        string dtmDateForPrint = "";
                        int mCount = 0;


                        if (dsResult.Tables[0].Rows.Count > 0)
                        {
                            for (int i1 = 0; i1 < dsResult.Tables[0].Rows.Count; i1++)
                            {
                                lngNoOfDaysForPrint = 0;
                                strsql = "select * from tbltimeregister Where  DateOffice <= getdate() " +
                                    "  and paycode='" + dsResult.Tables[0].Rows[i1]["Paycode"].ToString().Trim() + "' order by dateoffice ";
                                dsResult1 = con.FillDataSet(strsql);
                                if (dsResult1.Tables[0].Rows.Count > 0)
                                {
                                    for (int abs = Convert.ToInt32(dsResult1.Tables[0].Rows.Count) - 1; abs >= 0; abs--)
                                    {
                                        if ((Convert.ToDouble(dsResult1.Tables[0].Rows[abs]["earlydeparture"]) > 0))
                                        {
                                            dtmDateForPrint = Convert.ToDateTime(dsResult1.Tables[0].Rows[abs]["DATEOFFICE"]).ToString("dd/MM/yyyy");
                                            mcontinuse = true;
                                        }
                                        else
                                        {
                                            mcontinuse = false;
                                        }

                                        if (mcontinuse)
                                        {
                                            lngNoOfDaysForPrint = lngNoOfDaysForPrint + 1;
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                }

                                if (lngNoOfDaysForPrint >= mlngStartingNoOfDays)
                                {
                                    mCount = mCount + 1;
                                    dTbl.Rows[ct]["SNo"] = mCount.ToString();
                                    dTbl.Rows[ct]["Paycode"] = dsResult.Tables[0].Rows[i1]["Paycode"].ToString();
                                    dTbl.Rows[ct]["Name"] = dsResult.Tables[0].Rows[i1]["EmpName"].ToString();
                                    dTbl.Rows[ct]["Department"] = dsResult.Tables[0].Rows[i1]["DepartmentName"].ToString();
                                    dTbl.Rows[ct]["PresentCardNo"] = dsResult.Tables[0].Rows[i1]["PresentCardNo"].ToString();
                                    dTbl.Rows[ct]["Absent Since"] = dtmDateForPrint.ToString();
                                    dTbl.Rows[ct]["Days"] = lngNoOfDaysForPrint.ToString();
                                    dTbl.Rows[ct]["HOD"] = dsResult.Tables[0].Rows[i1]["HOD"].ToString();
                                    ct = ct + 1;
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        if (dTbl.Rows.Count == 2)
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        bool isEmpty;
                        for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                        {

                            isEmpty = true;
                            for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                            {
                                if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                {
                                    isEmpty = false;
                                    break;
                                }
                            }

                            if (isEmpty == true)
                            {
                                dTbl.Rows.RemoveAt(i2);
                                i2--;
                            }
                        }

                        string companyname = "";

                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }
                        int count1 = Convert.ToInt32(dTbl.Columns.Count);
                        //  msg = " LATE FROM DUTY FROM :" + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";

                        if (Session["IsNepali"].ToString().Trim() == "Y")
                        {

                            msg = " LATE FROM DUTY Report on " + FromDateNepal.ToString("dd-MM-yyyy") + " ";
                        }
                        else
                        {
                            msg = "LATE FROM DUTY  Report on " + FromDate.ToString("dd-MM-yyyy") + " ";
                        }

                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + count1 + "' style='text-align: left'></td></tr> ");

                        x = dTbl.Columns.Count;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                }
                                if (Convert.ToInt32(Tmp.ToString().Length) >= 15)
                                    //sb.Append("<td style='width:180px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:180px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                else
                                    //sb.Append("<td style='width:75px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:75px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));



                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                        string pdffilename = "ContEarlyDept.pdf";
                        PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                        pdfDocument.Open();
                        String htmlText = sb.ToString();
                        StringReader str = new StringReader(htmlText);
                        HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                        htmlworker.Parse(str);
                        pdfWriter.CloseStream = false;
                        pdfDocument.Close();
                        //Download Pdf  
                        Response.Buffer = true;
                        Response.ContentType = "application/pdf";
                        Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.Write(pdfDocument);
                        Response.Flush();
                        Response.End();
                    }
                    catch
                    {

                    }
                }

            }
            else if (radTextPunch.Checked)
            {
                if (Session["usertype"].ToString().ToString().ToUpper() != "A")
                {
                    strsql = "select paycode from tblemployee where headid='" + Session["PAYCODE"].ToString().Trim() + "'";
                    dsEmpCount = con.FillDataSet(strsql);
                    if (dsEmpCount.Tables[0].Rows.Count == 0)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('No Employee found Under ID=" + Session["PAYCODE"].ToString().ToString() + "');", true);
                        return;
                    }
                    else
                    {
                        for (int count = 0; count < dsEmpCount.Tables[0].Rows.Count; count++)
                        {
                            EmpCodes += ",'" + dsEmpCount.Tables[0].Rows[count]["paycode"].ToString().Trim() + "'";
                        }
                        if (!string.IsNullOrEmpty(EmpCodes))
                        {
                            EmpCodes = EmpCodes.Substring(1);
                        }
                        strsql = " select REPLACE(CONVERT(VARCHAR(10), dateoffice, 101), '/', '') AS [dateoffice],CAST(tblTimeRegister.PayCode AS VARCHAR(10)) PayCode,CAST(tbltimeregister.shiftattended AS VARCHAR(3)) shiftattended, " +
                        " isnull(Convert(varchar(5),tblTimeRegister.In1,108),'00:00') 'IN1', " +
                        " isnull(Convert(varchar(5),tblTimeRegister.Out1,108),'00:00') 'Out1' , " +
                        " isnull(Convert(varchar(5),tblTimeRegister.In2,108),'00:00') 'In2', " +
                        " isnull(Convert(varchar(5),tblTimeRegister.Out2,108),'00:00') 'Out2' " +
                        " from tblTimeRegister tblTimeRegister join tblemployee tblemployee on tblemployee.paycode=tbltimeregister.paycode " +
                        " where (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "'  or tblemployee.LeavingDate is null) " +
                        " and tblemployee.paycode in ( " + EmpCodes.ToString() + " )" +
                        " and tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "' and in1 is not null order by " + SortOrder.Trim() + " ";
                    }
                }
                else
                {
                    if (Selection.ToString().Trim() == "")
                    {
                        strsql = " select REPLACE(CONVERT(VARCHAR(10), dateoffice, 101), '/', '') AS [dateoffice],CAST(tblTimeRegister.PayCode AS VARCHAR(10)) PayCode,CAST(tbltimeregister.shiftattended AS VARCHAR(3)) shiftattended, " +
                        " isnull(Convert(varchar(5),tblTimeRegister.In1,108),'00:00') 'IN1', " +
                        " isnull(Convert(varchar(5),tblTimeRegister.Out1,108),'00:00') 'Out1' , " +
                        " isnull(Convert(varchar(5),tblTimeRegister.In2,108),'00:00') 'In2', " +
                        " isnull(Convert(varchar(5),tblTimeRegister.Out2,108),'00:00') 'Out2' " +
                        " from tblTimeRegister tblTimeRegister join tblemployee tblemployee on tblemployee.paycode=tbltimeregister.paycode " +
                        " where (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "'  or tblemployee.LeavingDate is null) " +
                        " and tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "' and in1 is not null order by " + SortOrder.Trim() + " ";

                    }
                    else
                    {
                        strsql = " select REPLACE(CONVERT(VARCHAR(10), dateoffice, 101), '/', '') AS [dateoffice],CAST(tblTimeRegister.PayCode AS VARCHAR(10)) PayCode,CAST(tbltimeregister.shiftattended AS VARCHAR(3)) shiftattended, " +
                        " isnull(Convert(varchar(5),tblTimeRegister.In1,108),'00:00') 'IN1', " +
                        " isnull(Convert(varchar(5),tblTimeRegister.Out1,108),'00:00') 'Out1' , " +
                        " isnull(Convert(varchar(5),tblTimeRegister.In2,108),'00:00') 'In2', " +
                        " isnull(Convert(varchar(5),tblTimeRegister.Out2,108),'00:00') 'Out2' " +
                        " from tblTimeRegister tblTimeRegister join tblemployee tblemployee on tblemployee.paycode=tbltimeregister.paycode " +
                        " where (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "'  or tblemployee.LeavingDate is null) " +
                        " " + Selection.ToString() + " " +
                        " and tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "' and in1 is not null order by " + SortOrder.Trim() + " ";
                    }
                }


                string filename = Server.MapPath("~\\Reports\\Sifa\\Sifafile") + System.DateTime.Now.ToString("HHmmss") + ".txt";
                string filename1 = "Sifafile" + System.DateTime.Now.ToString("HHmmss") + ".txt";
                FileInfo file = new FileInfo(filename);
                if (file.Exists)
                {
                    file.Delete();
                }
                FileStream fs = new FileStream(filename.ToString(), FileMode.CreateNew, FileAccess.ReadWrite, FileShare.ReadWrite);
                StreamWriter sw = new StreamWriter(fs);

                /*strsql = " select REPLACE(CONVERT(VARCHAR(10), dateoffice, 101), '/', '') AS [dateoffice],CAST(tblTimeRegister.PayCode AS VARCHAR(10)) PayCode,CAST(tbltimeregister.shiftattended AS VARCHAR(3)) shiftattended, " +
                        " isnull(Convert(varchar(5),tblTimeRegister.In1,108),'00:00') 'IN1', " +
                        " isnull(Convert(varchar(5),tblTimeRegister.Out1,108),'00:00') 'Out1' , " +
                        " isnull(Convert(varchar(5),tblTimeRegister.In2,108),'00:00') 'In2', " +
                        " isnull(Convert(varchar(5),tblTimeRegister.Out2,108),'00:00') 'Out2' " +
                        " from tblTimeRegister tblTimeRegister join tblemployee tblemployee on tblemployee.paycode=tbltimeregister.paycode "+
                        " where (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "'  or tblemployee.LeavingDate is null) "+
                        " and tblTimeRegister.DateOffice Between '"+FromDate.ToString("yyyy-MM-dd")+"' AND '"+toDate.ToString("yyyy-MM-dd")+"' and in1 is not null order by "+SortOrder.Trim() +" ";
                */


                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int pc = 0; pc < ds.Tables[0].Rows.Count; pc++)
                    {
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[pc]["in1"].ToString().Trim()))
                        {
                            sw.WriteLine(string.Format("{0,-20}", (ds.Tables[0].Rows[pc]["paycode"].ToString())) + "\t" + ds.Tables[0].Rows[pc]["dateoffice"].ToString() + "\t" + ds.Tables[0].Rows[pc]["ShiftAttended"].ToString() + "\t" + ds.Tables[0].Rows[pc]["IN1"].ToString() + "\t" + ds.Tables[0].Rows[pc]["Out1"].ToString() + "\t" + ds.Tables[0].Rows[pc]["In2"].ToString() + "\t" + ds.Tables[0].Rows[pc]["Out2"].ToString());
                        }
                    }
                    sw.Flush();
                    sw.Close();
                    fs.Close();

                    Response.Clear();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("Content-Disposition", "attachment; filename=" + filename1.ToString() + " ");
                    Response.WriteFile(filename);
                    Response.End();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('No Record Found.');", true);
                    return;
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('" + ex.Message.ToString() + "');", true);
            return;
        }
        //Response.Redirect("FrmDailyReports.aspx");
    }
    protected void UpdateTimeRegister()
    {
        FromDate = Convert.ToDateTime(txtDateFrom.Value);   //DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
        toDate = Convert.ToDateTime(txtToDate.Value); // DateTime.ParseExact(txtToDate.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
        Module_UPD UPD = new Module_UPD();
        string str = "";
        DateTime dtFrom = System.DateTime.MinValue;
        DateTime dtTo = System.DateTime.MinValue;
        DataSet dslv = new DataSet();
        string lvcode = "";
        string lvtype = "";
        double lvamount = 0;
        DateTime lvapr = DateTime.MinValue;
        string reason = "";
        string vno = "";
        string hday = "";
        double lvamount1 = 0;
        double lvamount2 = 0;
        string Pcode = "";
        string lvcodeTm = "";
        DateTime doffice = System.DateTime.MinValue;
        DataSet ds = new DataSet();
        DataSet drlvType = new DataSet();
        DataSet dsCount = new DataSet();
        string LvType1 = "";
        string LvType2 = "";
        string FirstHalfLvCode = "";
        string SecondHalfLvCode = "";
        string ShiftAttended = "";
        string Holiday = "";
        DataSet dsD = new DataSet();
        int result = 0;
        string StrSql = "";
        string OldStatus = "";
        string isOffInclude = "";
        string isHldInclude = "";
        string Status = "";
        try
        {
            StrSql = "select l.voucherno,t.paycode,convert(varchar(10),t.dateoffice,103), " +
                    " l.leavecode,  l.leavedays,l.halfday,l.userremarks,convert(varchar(10),isnull(stage1_approval_date,getdate()),103),t.leaveamount1,t.leaveamount2,t.status,l.leavecode,t.presentvalue,t.absentvalue,T.LEAVEVALUE,t.in1,t.Shiftattended,lm.isoffinclude,lm.ISHOLIDAYINCLUDE,t.Status  " +
                    " from tbltimeregister t join leave_request l 	on t.paycode=l.paycode join tblleavemaster lm on lm.leavecode=l.leavecode and t.dateoffice between l.leave_from and l.leave_to  " +
                    " join tblemployee e on e.paycode=t.paycode	where t.status not like '%'+rtrim(l.leavecode)+'%' and l.stage1_approved='Y' and l.stage2_approved='Y' " +
                    " and dateoffice ='" + FromDate.ToString("yyyy-MM-dd") + "' and l.SSN like '" + Session["Custcomp"] + "_%' order by t.paycode";

            ds = con.FillDataSet(StrSql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    vno = ds.Tables[0].Rows[i][0].ToString().Trim();
                    Pcode = ds.Tables[0].Rows[i][1].ToString().Trim();
                    doffice = DateTime.ParseExact(ds.Tables[0].Rows[i][2].ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    lvcode = ds.Tables[0].Rows[i][3].ToString().Trim();
                    lvcodeTm = ds.Tables[0].Rows[i][3].ToString().Trim();
                    lvamount = Convert.ToDouble(ds.Tables[0].Rows[0][4].ToString());
                    hday = ds.Tables[0].Rows[i][5].ToString().Trim();
                    reason = ds.Tables[0].Rows[i][6].ToString().Trim().Replace("'", "");
                    lvapr = DateTime.ParseExact(ds.Tables[0].Rows[i][7].ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    OldStatus = ds.Tables[0].Rows[i]["in1"].ToString().Trim();
                    ShiftAttended = ds.Tables[0].Rows[i]["Shiftattended"].ToString().Trim();
                    isOffInclude = ds.Tables[0].Rows[i]["isoffinclude"].ToString().Trim();
                    isHldInclude = ds.Tables[0].Rows[i]["ISHOLIDAYINCLUDE"].ToString().Trim();
                    Status = ds.Tables[0].Rows[i]["Status"].ToString().Trim();
                    dtFrom = doffice;
                    str = "select * from tblleavemaster where leavecode='" + lvcode.ToString().ToUpper() + "' ";
                    drlvType = con.FillDataSet(str);
                    if (drlvType.Tables[0].Rows.Count > 0)
                    {
                        lvtype = drlvType.Tables[0].Rows[0][6].ToString().Trim();
                    }
                    if (isOffInclude.ToString().Trim().ToUpper() == "N" && hday.ToString().Trim().ToUpper() == "N" && ShiftAttended.ToString().Trim().ToUpper() == "OFF")
                    {
                        continue;
                    }
                    if (isHldInclude.ToString().Trim().ToUpper() == "N" && hday.ToString().Trim().ToUpper() == "N" && Status.ToString().Trim().ToUpper() == "HLD")
                    {
                        continue;
                    }
                    try
                    {
                        if (hday.ToString().Trim() == "N")
                        {
                            lvamount = 1;
                            lvcodeTm = lvcode;
                        }
                        else if (hday.ToString().Trim() == "F")
                        {
                            lvamount = 0.5;
                            lvamount1 = 0.5;
                            lvcodeTm = lvcode;
                        }
                        else
                        {
                            lvamount = 0.5;
                            lvamount2 = 0.5;
                            lvcodeTm = lvcode;
                        }
                        StrSql = "";
                        StrSql = "select * from holiday where hdate='" + dtFrom.ToString("yyyy-MM-dd") + "' and companycode=(select companycode from tblemployee where paycode='" + Pcode.ToString().Trim() + "') " +
                                "and departmentcode=(select departmentcode from tblemployee where paycode='" + Pcode.ToString().Trim() + "')";
                        dsD = con.FillDataSet(StrSql);
                        if (dsD.Tables[0].Rows.Count > 0)
                        {
                            Holiday = "Y";
                        }
                        StrSql = "";
                        /*StrSql = "Select * From tblTimeRegister  Where Paycode = '" + Pcode.ToString().Trim() + "'" + " And DateOffice='" + dtFrom.ToString("yyyy-MM-dd") + "'";
                        dsD = con.FillDataSet(StrSql);
                        if (dsD.Tables[0].Rows.Count > 0)
                        {
                            ShiftAttended = dsD.Tables[0].Rows[0]["SHIFTATTENDED"].ToString().Trim();
                        }*/
                        StrSql = "";
                        if (hday.ToString().Trim() == "N")
                        {
                            FirstHalfLvCode = null;
                            LvType1 = null;
                            SecondHalfLvCode = null;
                            LvType2 = null;
                            lvamount = 1;
                        }
                        else if (hday.ToString().Trim() == "F")
                        {
                            StrSql = "select isnull(SECONDHALFLEAVECODE,''),LeaveType2 from tbltimeregister where paycode='" + Pcode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                            dsD = con.FillDataSet(StrSql);
                            if (dsD.Tables[0].Rows.Count > 0)
                            {
                                if (dsD.Tables[0].Rows[0][0].ToString().Trim() != "")
                                {
                                    FirstHalfLvCode = lvcode.ToString().Trim();
                                    LvType1 = lvtype.ToString().Trim();
                                    SecondHalfLvCode = dsD.Tables[0].Rows[0][0].ToString().Trim();
                                    LvType2 = dsD.Tables[0].Rows[0][1].ToString().Trim();
                                    lvamount = 1;
                                }
                            }
                        }
                        else if (hday.ToString().Trim() == "S")
                        {
                            StrSql = "select isnull(FIRSTHALFLEAVECODE,''),LeaveType1 from tbltimeregister where paycode='" + Pcode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                            dsD = con.FillDataSet(StrSql);
                            if (dsD.Tables[0].Rows.Count > 0)
                            {
                                if (dsD.Tables[0].Rows[0][0].ToString().Trim() != "")
                                {
                                    FirstHalfLvCode = dsD.Tables[0].Rows[0][0].ToString().Trim();
                                    LvType1 = dsD.Tables[0].Rows[0][1].ToString().Trim();
                                    SecondHalfLvCode = lvcode.ToString().Trim();
                                    LvType2 = lvtype.ToString().Trim();
                                    lvamount = 1;
                                }
                            }
                        }
                        if (hday.ToString().Trim() == "N")
                        {
                            StrSql = "Update tblTimeRegister Set LeaveCode='" + lvcodeTm.ToString() + "',LeaveType='" + lvtype.ToString() + "',LeaveAmount=" + lvamount + ",LeaveAprDate=getdate(),Voucher_No='" + vno.ToString() + "',Reason='" + reason.ToString() + "' Where paycode='" + Pcode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                        }
                        else if (hday.ToString().Trim() == "F")
                        {
                            StrSql = "Update tblTimeRegister Set FIRSTHALFLEAVECODE='" + lvcodeTm.ToString() + "', LeaveCode='" + lvcodeTm.ToString() + "',LeaveType1='" + lvtype.ToString() + "',LeaveType='" + lvtype.ToString() + "',LeaveAmount=" + lvamount + ",LeaveAmount1=" + lvamount1 + ",LeaveAprDate=getdate(),Voucher_No='" + vno.ToString() + "',Reason='" + reason.ToString() + "' Where paycode='" + Pcode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                        }
                        else if (hday.ToString().Trim() == "S")
                        {
                            StrSql = "Update tblTimeRegister Set SECONDHALFLEAVECODE='" + lvcodeTm.ToString() + "', LeaveCode='" + lvcodeTm.ToString() + "',LeaveType2='" + lvtype.ToString() + "',LeaveType='" + lvtype.ToString() + "',LeaveAmount=" + lvamount + ",LeaveAmount2=" + lvamount2 + ",LeaveAprDate=getdate(),Voucher_No='" + vno.ToString() + "',Reason='" + reason.ToString() + "' Where paycode='" + Pcode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                        }
                        result = con.execute_NonQuery(StrSql);
                        if (result > 0)
                        {
                            UPD.Upd(Pcode, dtFrom, lvamount, lvamount1, lvamount2, lvcode, FirstHalfLvCode, SecondHalfLvCode, lvtype, LvType1, LvType2, ShiftAttended, Holiday, OldStatus);
                        }
                    }
                    catch
                    {
                        continue;
                    }
                }
            }
        }
        catch
        {

        }
    }
    protected void txtDateFrom_CalendarDayCellPrepared(object sender, DevExpress.Web.CalendarDayCellPreparedEventArgs e)
    {

        if (e.Date.DayOfWeek == DayOfWeek.Sunday || e.Date.DayOfWeek == DayOfWeek.Saturday)
        {
            e.Cell.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
            e.Cell.ForeColor = System.Drawing.Color.Black;
        }
    }
    protected void txtToDate_CalendarDayCellPrepared(object sender, DevExpress.Web.CalendarDayCellPreparedEventArgs e)
    {
        if (e.Date.DayOfWeek == DayOfWeek.Sunday || e.Date.DayOfWeek == DayOfWeek.Saturday)
        {
            e.Cell.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
            e.Cell.ForeColor = System.Drawing.Color.Black;
        }
    }
}