﻿function Main(ctl00_ContentPlaceHolder1_Tabs_Panel1_chkMain,ctl00_ContentPlaceHolder1_Tabs_Panel2_chkCompany,
ctl00_ContentPlaceHolder1_Tabs_Panel2_chkDept,ctl00_ContentPlaceHolder1_Tabs_Panel2_chkSection,
ctl00_ContentPlaceHolder1_Tabs_Panel2_chkGrade,ctl00_ContentPlaceHolder1_Tabs_Panel2_chkCategory,ctl00_ContentPlaceHolder1_Tabs_Panel2_chkShift,ctl00_ContentPlaceHolder1_Tabs_Panel3_chkEmployee)
{
    if(document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel1_chkMain').checked==true)
    {       
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel2_chkCompany').disabled=false;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel2_chkDept').disabled=false;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel2_chkSection').disabled=false;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel2_chkGrade').disabled=false;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel2_chkCategory').disabled=false;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel2_chkShift').disabled=false;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel2_chkEmployee').disabled=false;
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel2').style.color='green';
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel2').disabled=false;
    }
    else if(document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel1_chkMain').checked==false)
    {    
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel2_chkCompany').disabled=true;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel2_chkDept').disabled=true;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel2_chkSection').disabled=true;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel2_chkGrade').disabled=true;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel2_chkCategory').disabled=true;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel2_chkShift').disabled=true;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel2_chkEmployee').disabled=true;
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel2').style.color='green';
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel2').disabled=true;
    }
}

function Transaction(ctl00_ContentPlaceHolder1_Tabs_Panel1_chkTransaction,ctl00_ContentPlaceHolder1_Tabs_Panel3_chkPunchEntry,
ctl00_ContentPlaceHolder1_Tabs_Panel3_chkOStoOT,ctl00_ContentPlaceHolder1_Tabs_Panel3_chkShiftChange)
{
    if(document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel1_chkTransaction').checked==true)
    {       
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel3_chkPunchEntry').disabled=false;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel3_chkOStoOT').disabled=false;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel3_chkShiftChange').disabled=false;
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel3').style.color='green';
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel3').disabled=false;
    }
    else if(document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel1_chkTransaction').checked==false)
    {    
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel3_chkPunchEntry').disabled=true; 
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel3_chkOStoOT').disabled=true; 
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel3_chkShiftChange').disabled=true;    
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel3').style.color='green';
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel3').disabled=true;
    }
}

function DataProcess(ctl00_ContentPlaceHolder1_Tabs_Panel1_chkDataProcess,ctl00_ContentPlaceHolder1_Tabs_Panel4_chkAtt_Creation,
ctl00_ContentPlaceHolder1_Tabs_Panel4_chkAtt_Updation,ctl00_ContentPlaceHolder1_Tabs_Panel4_chkBDP,
ctl00_ContentPlaceHolder1_Tabs_Panel4_chkReProcessing)
{
    if(document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel1_chkDataProcess').checked==true)
    {       
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel4_chkAtt_Creation').disabled=false;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel4_chkAtt_Updation').disabled=false;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel4_chkBDP').disabled=false;
    //document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel4_chkReProcessing').disabled=false;
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel4').style.color='green';
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel4').disabled=false;
    }
    else if(document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel1_chkDataProcess').checked==false)
    {    
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel4_chkAtt_Creation').disabled=true;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel4_chkAtt_Updation').disabled=true;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel4_chkBDP').disabled=true;
    //document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel4_chkReProcessing').disabled=true;
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel4').style.color='green';
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel4').disabled=true;
    }
}



function Leave(ctl00_ContentPlaceHolder1_Tabs_Panel1_chkLeaveManagement,ctl00_ContentPlaceHolder1_Tabs_Panel5_chkLeaveMaster,
ctl00_ContentPlaceHolder1_Tabs_Panel5_chkLeaveApplication)
{
    if(document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel1_chkLeaveManagement').checked==true)
    {       
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel5_chkLeaveMaster').disabled=false;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel5_chkLeaveApplication').disabled=false;    
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel5').style.color='green';
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel5').disabled=false;
    }
    else if(document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel1_chkLeaveManagement').checked==false)
    {    
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel5_chkLeaveMaster').disabled=true;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel5_chkLeaveApplication').disabled=true;    
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel5').style.color='green';
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel5').disabled=true;
    }
}

function Reports(ctl00_ContentPlaceHolder1_Tabs_Panel1_chkReports,ctl00_ContentPlaceHolder1_Tabs_Panel7_chkTimeOffice,ctl00_ContentPlaceHolder1_Tabs_Panel7_chkPayroll,ctl00_ContentPlaceHolder1_Tabs_Panel7_chkVisitorManagement)
{

    if(document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel1_chkReports').checked==true)
    {       
    
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel7_chkTimeOffice').disabled=false;
    //document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel7_chkPayroll').disabled=false;
    //document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel7_chkVisitorManagement').disabled=false;
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel7').style.color='green';
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel7').disabled=false;
    }
    else if(document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel1_chkReports').checked==false)
    {
    
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel7_chkTimeOffice').disabled=true;    
    //document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel7_chkPayroll').disabled=true;
    //document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel7_chkVisitorManagement').disabled=true;
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel7').style.color='green';
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel7').disabled=true;
    }
}

function Admin(ctl00_ContentPlaceHolder1_Tabs_Panel1_chkAdmin,ctl00_ContentPlaceHolder1_Tabs_Panel8_chkTimeOffSetup,
ctl00_ContentPlaceHolder1_Tabs_Panel8_chkVerification)
{
    if(document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel1_chkAdmin').checked==true)
    { 
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel8_chkTimeOffSetup').disabled=false;
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel8').style.color='green';
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel8_chkVerification').disabled=false;
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel8').disabled=false;
    }
    else if(document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel1_chkAdmin').checked==false)
    {  
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel8_chkTimeOffSetup').disabled=true;
   // document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel8_chkUserPrivilge').disabled=true;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel8_chkVerification').disabled=true;
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel8').disabled=true;
    }
}

function Hide(__tab_ctl00_ContentPlaceHolder1_Tabs_Panel2,__tab_ctl00_ContentPlaceHolder1_Tabs_Panel3,
__tab_ctl00_ContentPlaceHolder1_Tabs_Panel4,__tab_ctl00_ContentPlaceHolder1_Tabs_Panel5,
__tab_ctl00_ContentPlaceHolder1_Tabs_Panel7,__tab_ctl00_ContentPlaceHolder1_Tabs_Panel8,ctl00_ContentPlaceHolder1_ddlUserType,ctl00_ContentPlaceHolder1_cmdSelection,
__tab_ctl00_ContentPlaceHolder1_Tabs_tabPayroll)
{
    var first=document.getElementById('ctl00_ContentPlaceHolder1_ddlUserType');
    var Text1= first.options[first.selectedIndex].text;
    if(Text1=='Head of Department')
    {
    document.getElementById('ctl00_ContentPlaceHolder1_cmdSelection').style.display='block';
    }
    else
    {
    document.getElementById('ctl00_ContentPlaceHolder1_cmdSelection').style.display='none';                  
    }
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel2').disabled=true;                
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel3').disabled=true;    
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel4').disabled=true;                
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel5').disabled=true;    
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel7').disabled=true;                
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel8').disabled=true;                                  
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_tabPayroll').disabled=true;
}

/*User type change*/
function userType(ctl00_ContentPlaceHolder1_ddlUserType,ctl00_ContentPlaceHolder1_Tabs_Panel1_tdAdmin,ctl00_ContentPlaceHolder1_Tabs_Panel8,ctl00_ContentPlaceHolder1_Tabs_Panel5_tdleaveAccural,
ctl00_ContentPlaceHolder1_Tabs_Panel5_tdauto_leave,ctl00_ContentPlaceHolder1_Tabs_Panel8_tduserprivilge,
ctl00_ContentPlaceHolder1_Tabs_Panel8_tdTimeOffSetup,ctl00_ContentPlaceHolder1_Tabs_Panel8_tdVerification,
ctl00_ContentPlaceHolder1_cmdSelection)
{
    var first=document.getElementById('ctl00_ContentPlaceHolder1_ddlUserType');
    var Text1= first.options[first.selectedIndex].text;    
    if(Text1=='Administrator')
    {       
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel5_tdleaveAccural').style.display='';
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel5_tdauto_leave').style.display='';    
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel8_tduserprivilge').style.display='';        
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel8_tdTimeOffSetup').style.display='';        
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel8_tdVerification').style.display='';            
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel1_tdAdmin').style.display='none';
    document.getElementById('ctl00_ContentPlaceHolder1_txtPaycode').style.display='none';
    document.getElementById('ctl00_ContentPlaceHolder1_lblPaycode').style.display='none';    
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel8').disabled=true;
    document.getElementById('ctl00_ContentPlaceHolder1_cmdSelection').style.display='block';
    }
    else if(Text1=='Head of Department')
    {        
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel5_tdleaveAccural').style.display='';
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel5_tdauto_leave').style.display='';
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel8_tduserprivilge').style.display='none';    
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel8_tdVerification').style.display='';    
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel8_tdTimeOffSetup').style.display='';        
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel8').disabled=true;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel1_tdAdmin').style.display='none';         
    document.getElementById('ctl00_ContentPlaceHolder1_txtPaycode').style.display='none';
    document.getElementById('ctl00_ContentPlaceHolder1_lblPaycode').style.display='none';
    document.getElementById('ctl00_ContentPlaceHolder1_cmdSelection').style.display='block';
    
    }
    else if(Text1=='User')
    {    
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel5_tdleaveAccural').style.display='none';
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel5_tdauto_leave').style.display='none';
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel8_tduserprivilge').style.display='none';
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel8_tdTimeOffSetup').style.display='none';        
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel8_tdVerification').style.display='none';        
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel1_tdAdmin').style.display='none';     
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel8').disabled=true;
    document.getElementById('ctl00_ContentPlaceHolder1_txtPaycode').style.display='';
    document.getElementById('ctl00_ContentPlaceHolder1_lblPaycode').style.display='';
    document.getElementById('ctl00_ContentPlaceHolder1_cmdSelection').style.display='none';
    }
}


/*for usertype modification*/
function userType_modify(ctl00_ContentPlaceHolder1_ddlUserType,ctl00_ContentPlaceHolder1_Tabs_Panel1_tdAdmin,
ctl00_ContentPlaceHolder1_Tabs_Panel8,ctl00_ContentPlaceHolder1_Tabs_Panel5_tdleaveAccural,
ctl00_ContentPlaceHolder1_Tabs_Panel5_tdauto_leave,ctl00_ContentPlaceHolder1_Tabs_Panel8_tduserprivilge,
ctl00_ContentPlaceHolder1_cmdSelection,ctl00_ContentPlaceHolder1_txtPaycode,ctl00_ContentPlaceHolder1_lblPaycode)
{
    var first=document.getElementById('ctl00_ContentPlaceHolder1_ddlUserType');
    var Text1= first.options[first.selectedIndex].text;    
    if(Text1=='Administrator')
    {       
    //window.alert('user2222222');
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel5_tdleaveAccural').style.display='';
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel5_tdauto_leave').style.display='';    
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel8_tduserprivilge').style.display='';        
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel1_tdAdmin').style.display='';       
    document.getElementById('ctl00_ContentPlaceHolder1_txtPaycode').style.display='none';
    document.getElementById('ctl00_ContentPlaceHolder1_lblPaycode').style.display='none';    
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel8').disabled=false;
    document.getElementById('ctl00_ContentPlaceHolder1_cmdSelection').style.display='none';
    }
    else if(Text1=='Head of Department')
    {
    //window.alert('user111');        
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel5_tdleaveAccural').style.display='';
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel5_tdauto_leave').style.display='';
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel8_tduserprivilge').style.display='none';    
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel8').disabled=false;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel1_tdAdmin').style.display='';         
    document.getElementById('ctl00_ContentPlaceHolder1_txtPaycode').style.display='none';
    document.getElementById('ctl00_ContentPlaceHolder1_lblPaycode').style.display='block';
    document.getElementById('ctl00_ContentPlaceHolder1_cmdSelection').style.display='block';
    
    }
    else if(Text1=='User')
    {    
    //window.alert('user');    
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel5_tdleaveAccural').style.display='none';
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel5_tdauto_leave').style.display='none';
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel8_tduserprivilge').style.display='none';    
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel1_tdAdmin').style.display='none';     
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel8').disabled=true;
    document.getElementById('ctl00_ContentPlaceHolder1_txtPaycode').style.display='block';
    document.getElementById('ctl00_ContentPlaceHolder1_lblPaycode').style.display='block';
    document.getElementById('ctl00_ContentPlaceHolder1_cmdSelection').style.display='none';
    
    }
}



/*Hide controls of user privilge page on load*/
function HideControls(ctl00_ContentPlaceHolder1_txtUserName,__tab_ctl00_ContentPlaceHolder1_Tabs_Panel2,__tab_ctl00_ContentPlaceHolder1_Tabs_Panel3,
__tab_ctl00_ContentPlaceHolder1_Tabs_Panel4,__tab_ctl00_ContentPlaceHolder1_Tabs_Panel5,
__tab_ctl00_ContentPlaceHolder1_Tabs_Panel7,__tab_ctl00_ContentPlaceHolder1_Tabs_Panel8,ctl00_ContentPlaceHolder1_cmdSelection,__tab_ctl00_ContentPlaceHolder1_Tabs_tabPayroll)
{
    document.getElementById('ctl00_ContentPlaceHolder1_txtUserName').focus();                    
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel2').disabled=true;                
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel3').disabled=true;    
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel4').disabled=true;                
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel5').disabled=true;    
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel7').disabled=true;                
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_Panel8').disabled=true;                   
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_tabPayroll').disabled=true;                   
    document.getElementById('ctl00_ContentPlaceHolder1_cmdSelection').style.display='none';              
}

//Payroll
function PayrollManagement(ctl00_ContentPlaceHolder1_Tabs_Panel1_chkPayrollManagement,ctl00_ContentPlaceHolder1_Tabs_tabPayroll_chkEmployeeSetup,
ctl00_ContentPlaceHolder1_Tabs_tabPayroll_chkArrear,ctl00_ContentPlaceHolder1_Tabs_tabPayroll_chkAdvanceLoan,ctl00_ContentPlaceHolder1_Tabs_tabPayroll_chkForm16,
ctl00_ContentPlaceHolder1_Tabs_tabPayroll_chkPayrollProcess,ctl00_ContentPlaceHolder1_Tabs_tabPayroll_chkPayrollFormula,ctl00_ContentPlaceHolder1_Tabs_tabPayroll_chkPayrollSetup,
ctl00_ContentPlaceHolder1_Tabs_tabPayroll_chkPieceManagement)
{
    if(document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel1_chkPayrollManagement').checked==true)
    {
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_tabPayroll_chkEmployeeSetup').disabled=false;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_tabPayroll_chkArrear').disabled=false;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_tabPayroll_chkAdvanceLoan').disabled=false;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_tabPayroll_chkForm16').disabled=false;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_tabPayroll_chkPayrollProcess').disabled=false;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_tabPayroll_chkPayrollFormula').disabled=false;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_tabPayroll_chkPayrollSetup').disabled=false;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_tabPayroll_chkPieceManagement').disabled=false;    
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_tabPayroll').style.color='green';   
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_tabPayroll').disabled=false;
    }
    else if(document.getElementById('ctl00_ContentPlaceHolder1_Tabs_Panel1_chkPayrollManagement').checked==false)
    {  
    
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_tabPayroll_chkEmployeeSetup').disabled=true;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_tabPayroll_chkArrear').disabled=true;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_tabPayroll_chkAdvanceLoan').disabled=true;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_tabPayroll_chkForm16').disabled=true;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_tabPayroll_chkPayrollProcess').disabled=true;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_tabPayroll_chkPayrollFormula').disabled=true;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_tabPayroll_chkPayrollSetup').disabled=true;
    document.getElementById('ctl00_ContentPlaceHolder1_Tabs_tabPayroll_chkPieceManagement').disabled=true;    
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_tabPayroll').style.color='green';   
    document.getElementById('__tab_ctl00_ContentPlaceHolder1_Tabs_tabPayroll').disabled=true;    
    }
}