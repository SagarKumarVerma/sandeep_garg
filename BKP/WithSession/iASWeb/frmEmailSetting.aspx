﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="frmEmailSetting.aspx.cs" Inherits="frmEmailSetting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent"  Runat="Server">
      <script language="javascript" type="text/javascript" src="JS/validation_Employee.js"></script>
      <style>


    div.relative {
  position: relative;
  width: 400px;
  height: 200px;
 
} 

div.absolute {
  position: absolute;
  top: 166px;
  right: 724px;
  width: 200px;
  height: 100px;

}
        </style>
      <div id="Frame" style="align-content:center;border:solid;border-width:medium ">
          <table  align="center" width="500px" >
              <tr>
                  <td style="width:auto;padding:5px" colspan="2" align="left">
                      <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="Email Setup" Font-Bold="true" Font-Size="Medium">
                      </dx:ASPxLabel>
                  </td>
              </tr>
              <tr>
                  <td style="width:auto;padding:5px">
                      <dx:ASPxLabel ID="ASPxLabel8" runat="server" Text="SMTP Server:">
                      </dx:ASPxLabel>
                  </td>
                  <td style="width:auto;padding:5px">
                      <dx:ASPxTextBox ID="txtSmtp" runat="server" Width="170px">
                          <ValidationSettings SetFocusOnError="True" ValidationGroup="MK">
                              <RequiredField IsRequired="True" />
                          </ValidationSettings>
                      </dx:ASPxTextBox>
                  </tr>
              <tr>
                  <td style="width:auto;padding:5px">
                      <dx:ASPxLabel ID="ASPxLabel2" runat="server" Text="SMTP Port:">
                      </dx:ASPxLabel>
                  </td>
                  <td style="width:auto;padding:5px">
                      <dx:ASPxTextBox ID="txtPort" runat="server" Width="170px" onkeypress= "fncInputNumericValuesOnly('txtPort')">
                          <ValidationSettings SetFocusOnError="True" ValidationGroup="MK">
                              <RequiredField IsRequired="True" />
                          </ValidationSettings>
                      </dx:ASPxTextBox>
                  </tr>
              <tr>
                  <td style="width:auto;padding:5px">
                      <dx:ASPxLabel ID="ASPxLabel3" runat="server" Text="Email ID:">
                      </dx:ASPxLabel>
                  </td>
                  <td style="width:auto;padding:5px">
                      <dx:ASPxTextBox ID="txtEmail" runat="server" Width="170px">
                          <ValidationSettings SetFocusOnError="True" ValidationGroup="MK">
                              <RegularExpression ErrorText="*Invalid Email Id" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                              <RequiredField IsRequired="True" />
                          </ValidationSettings>
                      </dx:ASPxTextBox>
                  </tr>
              <tr>
                  <td style="width:auto;padding:5px">
                      <dx:ASPxLabel ID="ASPxLabel4" runat="server" Text="User Name:">
                      </dx:ASPxLabel>
                  </td>
                  <td style="width:auto;padding:5px">
                      <dx:ASPxTextBox ID="txtUserName" runat="server" Width="170px">
                          <ValidationSettings SetFocusOnError="True" ValidationGroup="MK">
                              <RequiredField IsRequired="True" />
                          </ValidationSettings>
                      </dx:ASPxTextBox>
                  </tr>
              <tr>
                  <td style="width:auto;padding:5px">
                      <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="Password:">
                      </dx:ASPxLabel>
                  </td>
                  <td style="width:auto;padding:5px">
                      <dx:ASPxTextBox ID="txtPassword" runat="server" Width="170px" Password="True">
                          <ValidationSettings SetFocusOnError="True" ValidationGroup="MK">
                              <RequiredField IsRequired="True" />
                          </ValidationSettings>
                      </dx:ASPxTextBox>
                  </tr>
              <tr>
                  <td style="width:auto;padding:5px">
                      <dx:ASPxLabel ID="ASPxLabel6" runat="server" Text="Display Name:">
                      </dx:ASPxLabel>
                  </td>
                  <td style="width:auto;padding:5px">
                      <dx:ASPxTextBox ID="txtDisplayName" runat="server" Width="170px">
                          <ValidationSettings SetFocusOnError="True">
                              <RequiredField IsRequired="True" />
                          </ValidationSettings>
                      </dx:ASPxTextBox>
                  </tr>
              <tr>
                  <td style="width:auto;padding:5px">
                      <dx:ASPxLabel ID="ASPxLabel9" runat="server" Text="Test Email:">
                      </dx:ASPxLabel>
                  </td>
                  <td style="width:auto;padding:5px">
                      <dx:ASPxTextBox ID="txtEmailTo" runat="server" Width="170px">
                          <ValidationSettings ValidationGroup="MK">
                              <RegularExpression ErrorText="*Invalid Email ID" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" />
                          </ValidationSettings>
                      </dx:ASPxTextBox>
                  </tr>
              <tr>
                  <td style="width:auto;padding:5px">
                      <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="Enable SSL:">
                      </dx:ASPxLabel>
                  </td>
                  <td style="width:auto;padding:5px">
                      <dx:ASPxCheckBox ID="chkssl" runat="server">
                      </dx:ASPxCheckBox>
                  </td>
              </tr>
              <tr>
                  <td style="width:auto;padding:5px"  align="center">&nbsp;</td>
                  <td style="padding:5px"  align="left">
                      <dx:ASPxButton ID="ASPxButton2" runat="server" Text="Test Mail" OnClick="ASPxButton2_Click" >
                      </dx:ASPxButton>
                      <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Save" OnClick="ASPxButton1_Click" ValidationGroup="MK">
                      </dx:ASPxButton>
                  </td>
              </tr>
          </table>
      </div>
</asp:Content>

