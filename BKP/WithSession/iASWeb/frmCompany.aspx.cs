﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using GlobalSettings;
using System.Collections.Generic;
using DevExpress.Web;

public partial class frmCompany : System.Web.UI.Page
{
    Class_Connection Con = new Class_Connection();
    string Strsql = null;
    DataSet ds = null;
    int result = 0;
    ErrorClass ec = new ErrorClass();
    string Cname, CAdd, CSort, CPan;
    bool isNewRecord = false;


    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {

            if ( Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            //if (Session["ComVisible"] == null)
            //{
            //    Response.Redirect("PageNotFound.aspx");
            //}
            BindData();
        }
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }

    protected void BindData()
    {
        try
        {
            if (Session["LoginUserName"].ToString().Trim().ToUpper() == "ADMIN")
            {
                Strsql = "Select * from tblCompany ";
            }
            else
            {
                Strsql = "Select * from tblCompany where COMPANYCODE='" + Session["LoginCompany"].ToString().Trim() + "' ";
            }

            ds = new DataSet();
            ds = Con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                GrdComp.DataSource = ds.Tables[0];
                GrdComp.DataBind();
            }
        }
        catch(Exception Ex)
        {
            Error_Occured("BindData", Ex.Message);
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        BindData();
        GrdComp.DataBind();
    }
    private void AddError(Dictionary<GridViewColumn, string> errors, GridViewColumn column, string errorText)
    {
        if (!errors.ContainsKey(column))
            errors[column] = errorText;
    }


    protected void GrdComp_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
    {
        string CCode = Convert.ToString(e.NewValues["COMPANYCODE"]);
        string CName = Convert.ToString(e.NewValues["COMPANYNAME"]);
        string SName = Convert.ToString(e.NewValues["SHORTNAME"]);
       // string k = e.Keys["COMPANYCODE"].ToString();
        if(CCode.ToString().Trim()==string.Empty)
        {
            AddError(e.Errors, GrdComp.Columns[0], "Input Company Code");

            e.RowError = "*Input Company Code";
        }
        if (SName.ToString().Trim() == string.Empty)
        {
            AddError(e.Errors, GrdComp.Columns[0], "Input Login Code");

            e.RowError = "*Input Login Code";
        } 
        if (SName.ToString().Trim().ToUpper() != CCode.Trim().ToUpper())
        {
            AddError(e.Errors, GrdComp.Columns[0], "Company Copde & Login Code Must Be Same");

            e.RowError = "*Company Code & Login Code Must Be Same";
        }

         if (CName.ToString().Trim() == string.Empty)
        {
            AddError(e.Errors, GrdComp.Columns[0], "Input Company Name");
            e.RowError = "*Input Company Name";
        }
        if (isNewRecord==true)
        {
            Strsql = "Select * from tblCompany where COMPANYCODE='" + CCode.Trim() + "'  ";
            DataSet DsIP = new DataSet();
            DsIP = Con.FillDataSet(Strsql);
            if (DsIP.Tables[0].Rows.Count > 0)
            {
                AddError(e.Errors, GrdComp.Columns[0], "Duplicate Company Code");
                e.RowError = "*Duplicate Company Code";
            }
            Strsql = "Select * from tblCompany where SHORTNAME='" + SName.Trim() + "'  ";
            DsIP = new DataSet();
            DsIP = Con.FillDataSet(Strsql);
            if (DsIP.Tables[0].Rows.Count > 0)
            {
                AddError(e.Errors, GrdComp.Columns[0], "Duplicate Login Code");
                e.RowError = "*Duplicate Login Code";
            }
        }
        
        
        
        
    }
    protected void GrdComp_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.Grid.IsNewRowEditing == false)
        {
            if (e.Column.FieldName == "COMPANYCODE" )
            {
                e.Editor.ReadOnly = true;

            }
            if (e.Column.FieldName == "SHORTNAME")
            {
                e.Editor.ReadOnly = true;

            }   
           
        }
        else
        {
            isNewRecord = true;
            e.Editor.ReadOnly = false;
        }

    }
    protected void GrdComp_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        try
        {
            string k = e.Keys["COMPANYCODE"].ToString();
            try
            {
                CAdd = e.NewValues["COMPANYADDRESS"].ToString();
            }
            catch
            {
                CAdd = "";
            }
            try
            {
                CSort = e.NewValues["SHORTNAME"].ToString();
            }
            catch
            {
                CSort = "";
            }
            try
            {
                CPan = e.NewValues["PANNUM"].ToString();
            }
            catch
            {
                CPan = "";
            }

            try
            {
                Cname = e.NewValues["COMPANYNAME"].ToString();
            }
            catch
            {
                Cname = "";
            }
           
            Strsql = "update TBLCOMPANY set COMPANYADDRESS='" + CAdd.Trim().ToUpper() + "',SHORTNAME='" + CSort.Trim().ToUpper() + "', "+
                " PANNUM='" + CPan.Trim().ToUpper() + "',COMPANYNAME='" + Cname.Trim().ToUpper() + "' where COMPANYCODE='" + k.Trim() + "'  ";
            Con.execute_NonQuery(Strsql);

            e.Cancel = true;
            GrdComp.CancelEdit();
            BindData();
        }
        catch (Exception Ex)
        {
            Error_Occured("GrdComp_RowUpdating", Ex.Message);
        }
    }
    protected void GrdComp_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        try
        {
            string k = e.NewValues["COMPANYCODE"].ToString();
            CAdd = e.NewValues["COMPANYADDRESS"].ToString();
            CSort = e.NewValues["SHORTNAME"].ToString();
           
            Cname = e.NewValues["COMPANYNAME"].ToString();

            try
            {
                CPan = e.NewValues["PANNUM"].ToString();
            }
            catch
            {
                CPan = "";
            }

            Strsql = "insert into TBLCOMPANY (COMPANYCODE,COMPANYADDRESS,SHORTNAME,PANNUM,COMPANYNAME)values ('" + k.Trim() + "','" + CAdd.Trim() + "','" + CSort.Trim() + "', "+
            "'" + CPan.Trim() + "','" + Cname.Trim() + "')  ";
            Con.execute_NonQuery(Strsql);
           


                #region AddUser
                    string SSN = k.Trim() + "_" + CSort.Trim().Replace("'", " ");
                    Strsql = "insert into tbluser(USER_R,USERDESCRIPRION,PASSWORD,UserPrevilege,paycode,Admin,Usertype,TimeOfficeSetup,LoginType,AutoProcess,DataProcess,Main,V_Transaction,Reports,Leave,Company,Department,Section,Grade,Category, " +
                            "Shift,Employee,Manual_Attendance,OstoOt,ShiftChange,HoliDay,LeaveMaster,LeaveApplication,LeaveAccural,LeaveAccuralAuto,Verification,InstallationSetup,EmployeeSetup,ArearEntry,TimeOfficeReport,RegisterCreation,RegisterUpdation,BackDateProcess,isValid,companycode,SSN,auth_comp,ManualUpload,Visitor) " +
                            " values('" + CSort.Trim().Replace("'", " ") + "','" + CSort.Trim().Replace("'", " ") + "','" + CSort.Trim().Replace("'", " ") + "','Y','" + CSort.Trim().Replace("'", " ") + "','Y','A','Y','L','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','Y','" + k.Trim() + "','" + SSN.ToString() + "','" + k.Trim() + "','Y','Y')";
                    result = Con.execute_NonQuery(Strsql);

                    Strsql = "insert into tblshiftmaster(SHIFT,STARTTIME,ENDTIME,LUNCHTIME,LUNCHDURATION,LUNCHENDTIME,ORDERINF12,OTSTARTAFTER,OTDEDUCTHRS,LUNCHDEDUCTION,SHIFTPOSITION,SHIFTDURATION,OTDEDUCTAFTER,Companycode)values('GEN','1899-12-30 09:00:00.000','1899-12-30 18:00:00.000','1899-12-30 00:00:00.000',0,'1899-12-30 00:00:00.000',NULL,0,	0,	0,	'DAY',540,	0,'" + k.Trim() + "')";
                    Con.execute_NonQuery(Strsql);

                    try
                    {
                        string SetupID = "1";
                        Strsql = "Select setupid+1 from tblSetup WHERE CONVERT(INT,SETUPID)=(SELECT MAX(Convert(int,Setupid)) FROM TBLSETUP) ";
                        ds = Con.FillDataSet(Strsql);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            SetupID = ds.Tables[0].Rows[0][0].ToString();
                        }
                            
                        Strsql = "Insert into tblSetup(SETUPID,PERMISLATEARR,PERMISEARLYDEP,DUPLICATECHECKMIN,ISOVERSTAY,S_END,S_OUT,ISOTOUTMINUSSHIFTENDTIME,ISOTWRKGHRSMINUSSHIFTHRS,ISOTEARLYCOMEPLUSLATEDEP,ISOTALL,ISOTEARLYCOMING,OTEARLYDUR,OTLATECOMINGDUR,OTRESTRICTENDDUR,OTEARLYDEPARTUREDUR,DEDUCTWOOT,DEDUCTHOLIDAYOT,ISPRESENTONWOPRESENT,ISPRESENTONHLDPRESENT,MAXWRKDURATION,TIME1,CHECKLATE,CHECKEARLY,TIME,HALF,SHORT,ISAUTOABSENT,AUTOSHIFT_LOW,AUTOSHIFT_UP,ISAUTOSHIFT,ISHALFDAY,ISSHORT,TWO,WOINCLUDE,IsOutWork,NightShiftFourPunch,LinesPerPage,SkipAfterDepartment,meals_rate,INOUT,SMART,PREWO,ISAWA,ISPREWO,CompanyCode) values('" + SetupID + "',10,10,5,'N','Mar 21 2000  5:00','Mar 21 2000  5:00','N','Y','N','N','N',0,0,0,0,0,0,'N','N',    1440,1020,240,240,240,300,120,'N',240,240,'N','0','0','N','N','N','N',58,'Y',15.5,'N','N',3,'N','N','" + k.Trim() + "')";
                        Con.execute_NonQuery(Strsql);

                    }
                    catch
                    {

                    }
                   


                #endregion




            e.Cancel = true;
            GrdComp.CancelEdit();
            BindData();
        }
        catch (Exception Ex)
        {
            Error_Occured("GrdComp_RowInserting", Ex.Message);
            e.Cancel = true;
            GrdComp.CancelEdit();
            BindData();
        }
    }
    protected void GrdComp_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        try
        {
            string IsMaster = Convert.ToString(e.Keys["COMPANYCODE"]);
            bool Validate = false;
            Validate = ValidData(IsMaster);
            if (Validate == true)
            {
                //ClientScript.RegisterStartupScript(Page.GetType(), "validation", "<script language='javascript'>alert('Company Already Assigned To Employee')</script>");
                //return;
               
            }
            else
            {
                Strsql = "Delete From TBLCOMPANY where COMPANYCODE='" + IsMaster.Trim() + "'  ";
                Con.execute_NonQuery(Strsql);
            }

            GrdComp.CancelEdit();
            e.Cancel = true;
            BindData();
        }
        catch (Exception Ex)
        {
            Error_Occured("GrdComp_RowDeleting", Ex.Message);
        }
    }
    private bool ValidData(string CCode)
    {
        bool Check = false;
        try
        {
            Strsql = "Select * from tblemployee where COMPANYCODE='" + CCode + "' ";
            DataSet DsCheck = new DataSet();
            DsCheck = Con.FillDataSet(Strsql);
            if (DsCheck.Tables[0].Rows.Count > 0)
            {
                Check = true;

            }

        }
        catch (Exception Ex)
        {
            Error_Occured("ValidData", Ex.Message);
        }

        return Check;
    }
    protected void GrdComp_PageIndexChanged(object sender, EventArgs e)
    {
        int pageIndex = (sender as ASPxGridView).PageIndex;
        GrdComp.PageIndex = pageIndex;
        this.BindData();  

    }
    protected void GrdComp_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
    {
        //int MaxComp=1;
        //Strsql="Select * from tblcompany";
        //DataSet Dsc = new DataSet();
        //Dsc = Con.FillDataSet(Strsql);
        //if (Dsc.Tables[0].Rows.Count>0)
        //{
        //    MaxComp = Dsc.Tables[0].Rows.Count;
        //}
        //if (MaxComp>5)
        //{
        //    if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.New)
        //    {
        //        e.Enabled = false;
        //    }
        //    return;
        //}
        
        if (Session["LoginUserName"].ToString().Trim().ToUpper() != "ADMIN")
        {

            if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.New)
            {
                e.Enabled = false;
            }
            if (e.ButtonType == DevExpress.Web.ColumnCommandButtonType.Delete)
            {
                e.Enabled = false;
            }

        }
    }
}