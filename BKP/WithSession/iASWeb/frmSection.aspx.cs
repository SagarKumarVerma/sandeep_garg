﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using GlobalSettings;
using System.Collections.Generic;
using DevExpress.Web;

public partial class frmSection : System.Web.UI.Page
{
    Class_Connection Con = new Class_Connection();
    string Strsql = null;
    DataSet ds = null;
    int result = 0;
    ErrorClass ec = new ErrorClass();
    string Cname, CAdd, CSort, CPan;
    bool isNewRecord = false;


    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            //if (Session["ComVisible"] == null)
            //{
            //    Response.Redirect("PageNotFound.aspx");
            //}
            BindData();
        }
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }

    protected void BindData()
    {
        try
        {
           if(Session["LoginUserName"].ToString().Trim().ToUpper()=="ADMIN")
           {
               Strsql = "Select DivisionCode,DivisionName from tblDivision order by 1";
           }
           else
           {
               Strsql = "Select DivisionCode,DivisionName from tblDivision where CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"'  order by 1";
           }
            
            ds = new DataSet();
            ds = Con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                GrdSec.DataSource = ds.Tables[0];
                GrdSec.DataBind();
            }
        }
        catch (Exception Ex)
        {
            Error_Occured("BindData", Ex.Message);
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        BindData();
        GrdSec.DataBind();
    }
    private void AddError(Dictionary<GridViewColumn, string> errors, GridViewColumn column, string errorText)
    {
        if (!errors.ContainsKey(column))
            errors[column] = errorText;
    }


    protected void GrdComp_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
    {
        string CCode = Convert.ToString(e.NewValues["DivisionCode"]);
        string CName = Convert.ToString(e.NewValues["DivisionName"]);
        // string k = e.Keys["COMPANYCODE"].ToString();
        if (CCode.ToString().Trim() == string.Empty)
        {
            AddError(e.Errors, GrdSec.Columns[0], "Input Code");

            e.RowError = "*Input Code";
        }
        if (CName.ToString().Trim() == string.Empty)
        {
            AddError(e.Errors, GrdSec.Columns[0], "Input Name");
            e.RowError = "*Input Name";
        }
        if (isNewRecord == true)
        {
            Strsql = "Select * from tblDivision where DivisionCode='" + CCode + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'   ";
            DataSet DsIP = new DataSet();
            DsIP = Con.FillDataSet(Strsql);
            if (DsIP.Tables[0].Rows.Count > 0)
            {
                AddError(e.Errors, GrdSec.Columns[0], "Duplicate Code");
                e.RowError = "*Duplicate Code";
            }
        }




    }
    protected void GrdComp_CellEditorInitialize(object sender, ASPxGridViewEditorEventArgs e)
    {
        if (e.Column.Grid.IsNewRowEditing == false)
        {
            if (e.Column.FieldName == "DivisionCode")
            {
                e.Editor.ReadOnly = true;

            }
            else
            {
                e.Editor.ReadOnly = false;
            }
        }
        else
        {
            isNewRecord = true;
            e.Editor.ReadOnly = false;
            
        }

    }
    protected void GrdComp_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
    {
        try
        {
            string k = e.Keys["DivisionCode"].ToString();
            CAdd = e.NewValues["DivisionName"].ToString();

            Strsql = "update tblDivision set DivisionName='" + CAdd.Trim().ToUpper() + "' where DivisionCode='" + k.Trim() + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'  ";
            Con.execute_NonQuery(Strsql);

            e.Cancel = true;
            GrdSec.CancelEdit();
            BindData();
            DataFilter.LoadDivision(Session["LoginCompany"].ToString().Trim());
        }
        catch (Exception Ex)
        {

        }
    }
    protected void GrdComp_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
    {
        try
        {
            string k = e.NewValues["DivisionCode"].ToString();
            CAdd = e.NewValues["DivisionName"].ToString();
            Strsql = "insert into tblDivision (DivisionCode,DivisionName,CompanyCode)values ('" + k.Trim() + "','" + CAdd.Trim() + "','"+Session["LoginCompany"].ToString().Trim()+"')  ";
            Con.execute_NonQuery(Strsql);
            e.Cancel = true;
            GrdSec.CancelEdit();
            BindData();
            DataFilter.LoadDivision(Session["LoginCompany"].ToString().Trim());
        }
        catch (Exception Ex)
        {

            Error_Occured("GrdComp_RowInserting", Ex.Message);
        }
    }
    protected void GrdComp_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
    {
        try
        {
            string IsMaster = Convert.ToString(e.Keys["DivisionCode"]);
            bool Validate = false;
            Validate = ValidData(IsMaster);
            if (Validate == true)
            {
                GrdSec.CancelEdit();
                e.Cancel = true;
                BindData();
                return;

            }
            else
            {
                Strsql = "Delete From tblDivision where DivisionCode='" + IsMaster.Trim() + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'  ";
                Con.execute_NonQuery(Strsql);
            }
            e.Cancel = true;
            GrdSec.CancelEdit();
            DataFilter.LoadDivision(Session["LoginCompany"].ToString().Trim());
            
            BindData();
        }
        catch (Exception Ex)
        {

            Error_Occured("GrdComp_RowDeleting", Ex.Message);
        }
    }
    private bool ValidData(string CCode)
    {
        bool Check = false;
        try
        {
            Strsql = "Select * from tblemployee where DivisionCode='" + CCode + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' ";
            DataSet DsCheck = new DataSet();
            DsCheck = Con.FillDataSet(Strsql);
            if (DsCheck.Tables[0].Rows.Count > 0)
            {
                Check = true;

            }

        }
        catch (Exception Ex)
        {
            Check = false;
            Error_Occured("ValidData", Ex.Message);
        }

        return Check;
    }
    protected void GrdComp_PageIndexChanged(object sender, EventArgs e)
    {
        int pageIndex = (sender as ASPxGridView).PageIndex;
        GrdSec.PageIndex = pageIndex;
        this.BindData();

    }
}