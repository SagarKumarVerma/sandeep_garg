﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="frmShift.aspx.cs" Inherits="frmShift" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:HiddenField ID="HdnRowId" runat="server" />
    <script type="text/javascript">
        //function ShowLoginWindow() {
        //    pcLogin.Show();
        //}
        //function ShowCreateAccountWindow() {
        //    pcCreateAccount.Show();
        //    tbUsername.Focus();
        //}
        function OnDetailsClick(keyValue) {
            document.getElementById('MainPane_Content_MainContent_HdnRowId').value = keyValue;
            //alert(document.getElementById('MainPane_Content_MainContent_HdnRowId').value);          
            pcLogin.Show();
            //pcLogin.PerformCallback(keyValue);
        }
    </script>
     <dx:ASPxButton ID="ASPxButton1" runat="server" Text="Add" AutoPostBack="False" >
        <ClientSideEvents Click="function(s, e) {
	OnDetailsClick('');
}" />
    </dx:ASPxButton>
    <dx:ASPxCallbackPanel ID="ASPxCallbackPanel2" runat="server" ClientInstanceName="ASPxCallbackPanel2" Width="100%">
        <PanelCollection>
            <dx:PanelContent runat="server">
                <dx:ASPxGridView ID="grdShift" runat="server" AutoGenerateColumns="False" 
    Width="100%" KeyFieldName="Shift" OnRowUpdating="grdShift_RowUpdating" OnStartRowEditing="grdShift_StartRowEditing" OnInitNewRow="grdShift_InitNewRow" OnContextMenuItemClick="grdShift_ContextMenuItemClick" OnToolbarItemClick="grdShift_ToolbarItemClick" ClientInstanceName="grdShift" OnRowDeleting="grdShift_RowDeleting">
                    <%-- DXCOMMENT: Configure ASPxGridView's columns in accordance with datasource fields --%>
                    <ClientSideEvents CustomButtonClick="function(s, e) {
	//alert(grdShift.GetRowKey(e.visibleIndex));
	OnDetailsClick(grdShift.GetRowKey(e.visibleIndex));
}" />
                    <SettingsAdaptivity AdaptivityMode="HideDataCells" />
                    <SettingsPager>
                        <PageSizeItemSettings Visible="true" Items="10, 20, 50,100" />
                    </SettingsPager>
                      <SettingsSearchPanel Visible="True" />
                    <SettingsEditing Mode="PopupEditForm">
                    </SettingsEditing>
                    <Settings ShowFilterRow="True" ShowTitlePanel="True" />
                    <SettingsBehavior ConfirmDelete="True" />
                    <SettingsCommandButton>
                        <ClearFilterButton Text=" ">
                            <Image IconID="filter_clearfilter_16x16">
                            </Image>
                        </ClearFilterButton>
                        <NewButton Text=" ">
                            <Image IconID="actions_add_16x16">
                            </Image>
                        </NewButton>
                        <UpdateButton ButtonType="Image" RenderMode="Image">
                            <Image IconID="save_save_16x16office2013">
                            </Image>
                        </UpdateButton>
                        <CancelButton ButtonType="Image" RenderMode="Image">
                            <Image IconID="actions_cancel_16x16office2013">
                            </Image>
                        </CancelButton>
                        <EditButton Text=" ">
                            <Image IconID="actions_edit_16x16devav">
                            </Image>
                        </EditButton>
                        <DeleteButton Text=" ">
                            <Image IconID="actions_trash_16x16">
                            </Image>
                        </DeleteButton>
                    </SettingsCommandButton>
                    <SettingsPopup>
                        <EditForm Modal="True" Width="800px" HorizontalAlign="WindowCenter" 
                PopupAnimationType="Slide" VerticalAlign="WindowCenter" />
                    </SettingsPopup>
                    <SettingsText PopupEditFormCaption="SHIFT" Title="SHIFT" />
                    <Columns>
                        <dx:GridViewCommandColumn  Width="40px" ShowClearFilterButton="True" ShowDeleteButton="True" ShowNewButtonInHeader="false" VisibleIndex="0" Caption=" ">
                        </dx:GridViewCommandColumn>
                        <dx:GridViewCommandColumn Caption=" " VisibleIndex="1" CellStyle-HorizontalAlign="Left" Width="7%" ButtonRenderMode="Image" ButtonType="Image">
                            <CustomButtons>
                                <dx:GridViewCommandColumnCustomButton ID="Edit">
                                    <Image IconID="actions_edit_16x16devav" />
                                </dx:GridViewCommandColumnCustomButton>
                            </CustomButtons>
                            <CellStyle HorizontalAlign="Left">
                            </CellStyle>
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn FieldName="Shift" VisibleIndex="3" Caption="Shift Code">
                            <PropertiesTextEdit MaxLength="3">
                            </PropertiesTextEdit>
                        </dx:GridViewDataTextColumn>
                         <dx:GridViewDataTextColumn FieldName="ShiftDiscription" VisibleIndex="4" Caption="Shift Discription">
                            
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Start Time" VisibleIndex="5" Caption="Start Time">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="End Time" VisibleIndex="6" Caption="End Time">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="Lunch Time" VisibleIndex="7" Caption="Lunch Start Time">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="lunchduration" VisibleIndex="8" Caption="Lunch Duration">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="LunchEnd Time" VisibleIndex="9" Caption="Lunch End Time">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="OTDEDUCTHRS" VisibleIndex="10" Caption="OT Deduct Hours">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="OTStartafter" VisibleIndex="11" Caption="OT Start After">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="LUNCHDEDUCTION" VisibleIndex="12" Caption="Lunch Deduction">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="SHIFTPOSITION" VisibleIndex="13" Caption="Shift Position">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="shiftduration" VisibleIndex="14" Caption="Shift Duration">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn FieldName="otdeductafter" VisibleIndex="15" Caption="OT Deduct After">
                        </dx:GridViewDataTextColumn>
                    </Columns>
                    <Paddings Padding="0px" />
                    <Border BorderWidth="0px" />
                    <BorderBottom BorderWidth="1px" />
                </dx:ASPxGridView>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxCallbackPanel>
   
    <div>
        <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" ClientInstanceName="ASPxCallbackPanel1" Width="200px">
            <PanelCollection>
                <dx:PanelContent runat="server">
                    <dx:ASPxPopupControl ID="pcLogin" runat="server" AllowDragging="True" AllowResize="True" ClientInstanceName="pcLogin" CloseAction="CloseButton" CloseAnimationType="Fade" CloseOnEscape="True" ContentUrl="~/ShiftEdit.aspx" EnableViewState="False" HeaderText="Shift" Height="550px" Modal="True" PopupAnimationType="Fade" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="Middle" ScrollBars="Auto" Target="_parent" Theme="SoftOrange" Width="500px">
                        <ClientSideEvents Closing="function(s, e) {
                            document.getElementById('MainPane_Content_MainContent_HdnRowId').value='';
	window.location = &quot;frmShift.aspx&quot;;
}" />
                        <ContentCollection>
                            <dx:PopupControlContentControl runat="server">
                            </dx:PopupControlContentControl>
                        </ContentCollection>
                    </dx:ASPxPopupControl>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxCallbackPanel>
    </div>
</asp:Content>

