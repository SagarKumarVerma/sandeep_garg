﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Globalization;
using System.Text;
using System.IO;
using System.Diagnostics;
using GlobalSettings;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text;


public partial class FrmReportLeave : System.Web.UI.Page
{
    DataSet ds;
    string strsql, Tmp, msg;
    Class_Connection con = new Class_Connection();
    DateTime DtFromDate, DtToDate, Dt;

    string datefrom = null;
    string dateTo = null;
    string Strsql = null;

    string OpenPopupPage = "";
    string Selection = "";
    string status = null;
    string leave = null;
    DateTime toDate;
    DateTime FromDate;
    DataSet dsEmpCount = new DataSet();
    string EmpCodes = "";
    ErrorClass ec = new ErrorClass();
    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    protected void Page_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();
        if (ex is HttpRequestValidationException)
        {
            string resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" +
                   "Form doesn't need to contain valid HTML, just anything with opening and closing angled brackets (<...>).<br />" +
                   btnGoBack +
                   "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
           
            if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
            {
                Request.Browser.Adapters.Clear();
            }

            bindCompany();
            bindPaycode();
            bindDepartment();
            bindSection();
            bindGrade();
            bindCategory();
            ddlYear.Visible = false;
            lblYear.Visible = false;
            txtToDate.Visible = true;
            lblto.Visible = true;

            bindYear();
            txtDateFrom.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
            txtToDate.Text = System.DateTime.Now.AddMonths(1).AddDays(-1).ToString("dd/MM/yyyy");
        }
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }

    protected void bindCompany()
    {
        try
        {
            Strsql = "select CompanyCode,Companyname from tblCompany";
            if (Session["LoginUserName"].ToString().Trim().Trim().ToUpper() == "ADMIN")
            {
                Strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
            }
            else
            {
                Strsql += " where companycode ='" + Session["LoginCompany"].ToString().Trim().Trim() + "' ";
            }

            ds = new DataSet();
            ds = con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                LookupCompany.DataSource = ds.Tables[0];
                LookupCompany.DataBind();
            }
        }
        catch (Exception Err)
        {

            Error_Occured("bindCompany", Err.Message);
        }
    }
    protected void bindPaycode()
    {
        try
        {
            Strsql = "select PAYCODE,EMPNAME from tblemployee where active='Y' and  CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "'";
            ds = new DataSet();
            ds = con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                LookEmp.DataSource = ds.Tables[0];
                LookEmp.DataBind();
            }
        }
        catch (Exception Err)
        {

            Error_Occured("bindPaycode", Err.Message);
        }
    }

    protected void bindDepartment()
    {
        try
        {
            Strsql = "select DepartmentCode,departmentname from tbldepartment";
            if (Session["LoginUserName"].ToString().Trim().Trim().ToUpper() == "ADMIN")
            {
                Strsql += " where DepartmentCode in (" + Session["Auth_Dept"].ToString().Trim() + ") and  CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "' ";
            }
            else
            {
                Strsql += " where  CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "' ";
            }
            ds = new DataSet();
            ds = con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                LookUpDept.DataSource = ds.Tables[0];
                LookUpDept.DataBind();
            }
        }
        catch (Exception Err)
        {

            Error_Occured("bindDepartment", Err.Message);
        }
    }
    protected void bindCategory()
    {
        try
        {
            Strsql = "select CAT,CATAGORYNAME from tblCatagory where   CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "'";
            ds = new DataSet();
            ds = con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lookupCat.DataSource = ds.Tables[0];
                lookupCat.DataBind();

            }
        }
        catch (Exception Err)
        {

            Error_Occured("bindCategory", Err.Message);
        }
    }
    protected void bindSection()
    {
        try
        {
            Strsql = "select DivisionCode,DivisionName from tblDivision where   CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "'";
            ds = new DataSet();
            ds = con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lookupSec.DataSource = ds.Tables[0];
                lookupSec.DataBind();
            }
        }
        catch (Exception Err)
        {

            Error_Occured("bindSection", Err.Message);
        }
    }
    protected void bindGrade()
    {
        try
        {
            Strsql = "select GradeCode,GradeName from tblGrade where   CompanyCode='" + Session["LoginCompany"].ToString().Trim().Trim() + "'";
            ds = new DataSet();
            ds = con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                lookupGrade.DataSource = ds.Tables[0];
                lookupGrade.DataBind();
            }
        }
        catch (Exception Err)
        {

            Error_Occured("bindGrade", Err.Message);
        }
    }
    protected void bindYear()
    {
        Strsql = "select distinct lyear from tblleaveledger where paycode in (select paycode from tblemployee where companycode='" + Session["LoginCompany"].ToString().Trim() + "')  order by lyear desc";
        ds = new DataSet();
        ds = con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlYear.DataSource = ds.Tables[0];
            ddlYear.ValueField = "lyear";
            ddlYear.TextField = "lyear";
            ddlYear.DataBind();
            ddlYear.SelectedIndex = 0;
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {



        bindCompany();
        bindPaycode();
        bindDepartment();
        bindSection();
        bindGrade();
        bindCategory();

    }
   
    protected void fire(object sender, EventArgs e)
    {
        try
        {

            if ((radLeaveCard.Checked ) || (radAccuredLeave.Checked) || (radConsumedLeave.Checked) || (radBalanceLeave.Checked ) || (radLeaveRegister.Checked ))
            {
                ddlYear.Visible = true;
                lblYear.Visible = true;
                txtDateFrom.Visible = false;
                txtToDate.Visible = false;

                lblfrom.Visible = false;
                lblto.Visible = false;
            }
            //else if (RadLvReport.SelectedIndex == 6)
            //{
            //    Response.Redirect("frmLeaveReport.aspx");
            //}
            else
            {
                ddlYear.Visible = false;
                lblYear.Visible = false;
                txtDateFrom.Visible = true;
                txtToDate.Visible = true;
                lblfrom.Visible = true;
                lblto.Visible = true;
            }
        }
        catch
        {

        }
    }

    protected void UpdateTimeRegister()
    {
        string ffDate = "";
        string ttDate = "";
        if ((radLeaveCard.Checked) || (radAccuredLeave.Checked) || (radConsumedLeave.Checked) || (radBalanceLeave.Checked) || (radLeaveRegister.Checked))
        {
            ffDate = "01/01/" + ddlYear.SelectedItem.Value.ToString();
            ttDate = "31/12/" + ddlYear.SelectedItem.Value.ToString();
            FromDate = DateTime.ParseExact(ffDate.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            toDate = DateTime.ParseExact(ttDate.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
        }
        
        else
        {
            FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            toDate = DateTime.ParseExact(txtToDate.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
        }


        Module_UPD UPD = new Module_UPD();
        string str = "";
        DateTime dtFrom = System.DateTime.MinValue;
        DateTime dtTo = System.DateTime.MinValue;
        DataSet dslv = new DataSet();
        string lvcode = "";
        string lvtype = "";
        double lvamount = 0;
        DateTime lvapr = DateTime.MinValue;
        string reason = "";
        string vno = "";
        string hday = "";
        double lvamount1 = 0;
        double lvamount2 = 0;
        string Pcode = "";
        string lvcodeTm = "";
        DateTime doffice = System.DateTime.MinValue;
        DataSet ds = new DataSet();
        DataSet drlvType = new DataSet();
        DataSet dsCount = new DataSet();
        string LvType1 = "";
        string LvType2 = "";
        string FirstHalfLvCode = "";
        string SecondHalfLvCode = "";
        string ShiftAttended = "";
        string Holiday = "";
        string OldStatus = "";
        DataSet dsD = new DataSet();
        int result = 0;
        string StrSql = "";
        string isOffInclude = "";
        string isHldInclude = "";
        string Status = "";
        try
        {
            StrSql = "select l.voucherno,t.paycode,convert(varchar(10),t.dateoffice,103), " +
                    " l.leavecode,  l.leavedays,l.halfday,l.userremarks,convert(varchar(10),isnull(stage1_approval_date,getdate()),103),t.leaveamount1,t.leaveamount2,t.status,l.leavecode,t.presentvalue,t.absentvalue,T.LEAVEVALUE,t.in1,t.shiftattended,lm.isoffinclude,lm.ISHOLIDAYINCLUDE,t.Status  " +
                    " from tbltimeregister t join leave_request l 	on t.paycode=l.paycode join tblleavemaster lm on lm.leavecode=l.leavecode and t.dateoffice between l.leave_from and l.leave_to  " +
                    " join tblemployee e on e.paycode=t.paycode	where t.status not like '%'+rtrim(l.leavecode)+'%' and l.stage1_approved='Y' and l.stage2_approved='Y' " +
                    " and dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' order by t.paycode";

            ds = con.FillDataSet(StrSql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    vno = ds.Tables[0].Rows[i][0].ToString().Trim();
                    Pcode = ds.Tables[0].Rows[i][1].ToString().Trim();
                    doffice = DateTime.ParseExact(ds.Tables[0].Rows[i][2].ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    lvcode = ds.Tables[0].Rows[i][3].ToString().Trim();
                    lvcodeTm = ds.Tables[0].Rows[i][3].ToString().Trim();
                    lvamount = Convert.ToDouble(ds.Tables[0].Rows[0][4].ToString());
                    hday = ds.Tables[0].Rows[i][5].ToString().Trim();
                    ShiftAttended = ds.Tables[0].Rows[i]["ShiftAttended"].ToString().Trim();
                    OldStatus = ds.Tables[0].Rows[i]["in1"].ToString().Trim();
                    reason = ds.Tables[0].Rows[i][6].ToString().Trim().Replace("'", "");
                    lvapr = DateTime.ParseExact(ds.Tables[0].Rows[i][7].ToString().Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    isOffInclude = ds.Tables[0].Rows[i]["isoffinclude"].ToString().Trim();
                    isHldInclude = ds.Tables[0].Rows[i]["ISHOLIDAYINCLUDE"].ToString().Trim();
                    Status = ds.Tables[0].Rows[i]["Status"].ToString().Trim();
                    dtFrom = doffice;
                    str = "select * from tblleavemaster where leavecode='" + lvcode.ToString().ToUpper() + "' ";
                    drlvType = con.FillDataSet(str);
                    if (drlvType.Tables[0].Rows.Count > 0)
                    {
                        lvtype = drlvType.Tables[0].Rows[0][6].ToString().Trim();
                    }
                    if (isOffInclude.ToString().Trim().ToUpper() == "N" && hday.ToString().Trim().ToUpper() == "N" && ShiftAttended.ToString().Trim().ToUpper() == "OFF")
                    {
                        continue;
                    }
                    if (isHldInclude.ToString().Trim().ToUpper() == "N" && hday.ToString().Trim().ToUpper() == "N" && Status.ToString().Trim().ToUpper() == "HLD")
                    {
                        continue;
                    }
                    try
                    {
                        if (hday.ToString().Trim() == "N")
                        {
                            lvamount = 1;
                            lvcodeTm = lvcode;
                        }
                        else if (hday.ToString().Trim() == "F")
                        {
                            lvamount = 0.5;
                            lvamount1 = 0.5;
                            lvcodeTm = lvcode;
                        }
                        else
                        {
                            lvamount = 0.5;
                            lvamount2 = 0.5;
                            lvcodeTm = lvcode;
                        }
                        StrSql = "";
                        StrSql = "select * from holiday where hdate='" + dtFrom.ToString("yyyy-MM-dd") + "' and companycode=(select companycode from tblemployee where paycode='" + Pcode.ToString().Trim() + "') " +
                                "and departmentcode=(select departmentcode from tblemployee where paycode='" + Pcode.ToString().Trim() + "')";
                        dsD = con.FillDataSet(StrSql);
                        if (dsD.Tables[0].Rows.Count > 0)
                        {
                            Holiday = "Y";
                        }
                        StrSql = "";
                        /*StrSql = "Select * From tblTimeRegister  Where Paycode = '" + Pcode.ToString().Trim() + "'" + " And DateOffice='" + dtFrom.ToString("yyyy-MM-dd") + "'";
                        dsD = con.FillDataSet(StrSql);
                        if (dsD.Tables[0].Rows.Count > 0)
                        {
                            ShiftAttended = dsD.Tables[0].Rows[0]["SHIFTATTENDED"].ToString().Trim();
                        }*/
                        StrSql = "";
                        if (hday.ToString().Trim() == "N")
                        {
                            FirstHalfLvCode = null;
                            LvType1 = null;
                            SecondHalfLvCode = null;
                            LvType2 = null;
                            lvamount = 1;
                        }
                        else if (hday.ToString().Trim() == "F")
                        {
                            StrSql = "select isnull(SECONDHALFLEAVECODE,''),LeaveType2 from tbltimeregister where paycode='" + Pcode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                            dsD = con.FillDataSet(StrSql);
                            if (dsD.Tables[0].Rows.Count > 0)
                            {
                                if (dsD.Tables[0].Rows[0][0].ToString().Trim() != "")
                                {
                                    FirstHalfLvCode = lvcode.ToString().Trim();
                                    LvType1 = lvtype.ToString().Trim();
                                    SecondHalfLvCode = dsD.Tables[0].Rows[0][0].ToString().Trim();
                                    LvType2 = dsD.Tables[0].Rows[0][1].ToString().Trim();
                                    lvamount = 1;
                                }
                            }
                        }
                        else if (hday.ToString().Trim() == "S")
                        {
                            StrSql = "select isnull(FIRSTHALFLEAVECODE,''),LeaveType1 from tbltimeregister where paycode='" + Pcode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                            dsD = con.FillDataSet(StrSql);
                            if (dsD.Tables[0].Rows.Count > 0)
                            {
                                if (dsD.Tables[0].Rows[0][0].ToString().Trim() != "")
                                {
                                    FirstHalfLvCode = dsD.Tables[0].Rows[0][0].ToString().Trim();
                                    LvType1 = dsD.Tables[0].Rows[0][1].ToString().Trim();
                                    SecondHalfLvCode = lvcode.ToString().Trim();
                                    LvType2 = lvtype.ToString().Trim();
                                    lvamount = 1;
                                }
                            }
                        }
                        if (hday.ToString().Trim() == "N")
                        {
                            StrSql = "Update tblTimeRegister Set LeaveCode='" + lvcodeTm.ToString() + "',LeaveType='" + lvtype.ToString() + "',LeaveAmount=" + lvamount + ",LeaveAprDate=getdate(),Voucher_No='" + vno.ToString() + "',Reason='" + reason.ToString() + "' Where paycode='" + Pcode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                        }
                        else if (hday.ToString().Trim() == "F")
                        {
                            StrSql = "Update tblTimeRegister Set FIRSTHALFLEAVECODE='" + lvcodeTm.ToString() + "', LeaveCode='" + lvcodeTm.ToString() + "',LeaveType1='" + lvtype.ToString() + "',LeaveType='" + lvtype.ToString() + "',LeaveAmount=" + lvamount + ",LeaveAmount1=" + lvamount1 + ",LeaveAprDate=getdate(),Voucher_No='" + vno.ToString() + "',Reason='" + reason.ToString() + "' Where paycode='" + Pcode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                        }
                        else if (hday.ToString().Trim() == "S")
                        {
                            StrSql = "Update tblTimeRegister Set SECONDHALFLEAVECODE='" + lvcodeTm.ToString() + "', LeaveCode='" + lvcodeTm.ToString() + "',LeaveType2='" + lvtype.ToString() + "',LeaveType='" + lvtype.ToString() + "',LeaveAmount=" + lvamount + ",LeaveAmount2=" + lvamount2 + ",LeaveAprDate=getdate(),Voucher_No='" + vno.ToString() + "',Reason='" + reason.ToString() + "' Where paycode='" + Pcode.ToString().Trim() + "' and dateoffice='" + dtFrom.ToString("yyyy-MM-dd") + "' ";
                        }
                        result = con.execute_NonQuery(StrSql);
                        if (result > 0)
                        {
                            UPD.Upd(Pcode, dtFrom, lvamount, lvamount1, lvamount2, lvcode, FirstHalfLvCode, SecondHalfLvCode, lvtype, LvType1, LvType2, ShiftAttended, Holiday, OldStatus);
                        }
                    }
                    catch
                    {
                        continue;
                    }
                }
            }
        }
        catch
        {

        }
    }
    protected void txtDateFrom_CalendarDayCellPrepared(object sender, DevExpress.Web.CalendarDayCellPreparedEventArgs e)
    {

        if (e.Date.DayOfWeek == DayOfWeek.Sunday || e.Date.DayOfWeek == DayOfWeek.Saturday)
        {
            e.Cell.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
            e.Cell.ForeColor = System.Drawing.Color.Black;
        }
    }
    protected void txtToDate_CalendarDayCellPrepared(object sender, DevExpress.Web.CalendarDayCellPreparedEventArgs e)
    {
        if (e.Date.DayOfWeek == DayOfWeek.Sunday || e.Date.DayOfWeek == DayOfWeek.Saturday)
        {
            e.Cell.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
            e.Cell.ForeColor = System.Drawing.Color.Black;
        }
    }
    protected void GetSelection()
    {
        try
        {
            strsql = "";
            string Company = "";
            string Employee = "";
            string Dept = "";
            string category = "";
            string Shift = "";
            string Division = "";
            string Grade = "";

            if (LookupCompany.Text.ToString().Trim() != string.Empty || LookupCompany.Text.ToString().Trim() != "")
            {


                string[] SplitComp = LookupCompany.Text.ToString().Trim().Split(',');
                Company = "";
                foreach (string sc in SplitComp)
                    Company += "'" + sc.Trim() + "',";

                Company = Company.TrimEnd(',');


                //Company = LookupCompany.Text.ToString().Trim();
            }
            if (LookUpDept.Text.ToString().Trim() != string.Empty || LookUpDept.Text.ToString().Trim() != "")
            {
                string[] SplitDep = LookUpDept.Text.ToString().Trim().Split(',');
                Dept = "";
                foreach (string sc in SplitDep)
                    Dept += "'" + sc.Trim() + "',";

                Dept = Dept.TrimEnd(',');
            }
            if (lookupSec.Text.ToString().Trim() != string.Empty || lookupSec.Text.ToString().Trim() != "")
            {

                string[] SplitSec = lookupSec.Text.ToString().Trim().Split(',');
                Division = "";
                foreach (string sc in SplitSec)
                    Division += "'" + sc.Trim() + "',";

                Division = Division.TrimEnd(',');



                //Division = lookupSec.Text.ToString().Trim();
            }
            if (lookupGrade.Text.ToString().Trim() != string.Empty || lookupGrade.Text.ToString().Trim() != "")
            {

                string[] SplitGr = lookupGrade.Text.ToString().Trim().Split(',');
                Grade = "";
                foreach (string sc in SplitGr)
                    Grade += "'" + sc.Trim() + "',";

                Grade = Grade.TrimEnd(',');



                // Grade = lookupGrade.Text.ToString().Trim();
            }
            if (lookupCat.Text.ToString().Trim() != string.Empty || lookupCat.Text.ToString().Trim() != "")
            {


                string[] SplitCAt = lookupCat.Text.ToString().Trim().Split(',');
                category = "";
                foreach (string sc in SplitCAt)
                    category += "'" + sc.Trim() + "',";

                category = category.TrimEnd(',');

                //category = lookupCat.Text.ToString().Trim();
            }
            if (LookUpShift.Text.ToString().Trim() != string.Empty || LookUpShift.Text.ToString().Trim() != "")
            {

                string[] SplitShift = LookUpShift.Text.ToString().Trim().Split(',');
                Shift = "";
                foreach (string sc in SplitShift)
                    Shift += "'" + sc.Trim() + "',";

                Shift = Shift.TrimEnd(',');

                //Shift = LookUpShift.Text.ToString().Trim();
            }
            if (LookEmp.Text.ToString().Trim() != string.Empty || LookEmp.Text.ToString().Trim() != "")
            {
                string[] SplitEmp = LookEmp.Text.ToString().Trim().Split(',');
                Employee = "";
                foreach (string sc in SplitEmp)
                    Employee += "'" + sc.Trim() + "',";

                Employee = Employee.TrimEnd(',');


                //Employee = LookEmp.Text.ToString().Trim();
            }
            strsql = " and 1=1 ";
            if (Company != "")
            {
                strsql += "and tblemployee.Companycode in (" + Company.ToString().Trim() + ") ";
            }
            if (Employee != "")
            {
                strsql += "and tblemployee.paycode in (" + Employee.ToString().Trim() + ")  ";
            }
            if (Dept != "")
            {
                strsql += "and tblemployee.DepartmentCode in (" + Dept.ToString().Trim() + ")  ";
            }
            else
            {
                strsql += "and tblemployee.DepartmentCode in (" + Session["Auth_Dept"].ToString().Trim() + ")  ";
            }
            if (category != "")
            {
                strsql += "and tblemployee.Cat in (" + category.ToString().Trim() + ")  ";
            }
            //if (Shift != "")
            //{
            //   // strsql += "and tbltimeregister.shiftattended in (" + Shift.ToString().Trim() + ")  ";
            //}
            if (Division != "")
            {
                strsql += "and tblemployee.Divisioncode in (" + Division.ToString().Trim() + ")  ";
            }
            if (Grade != "")
            {
                strsql += "and tblemployee.Gradecode in (" + Grade.ToString().Trim() + ")";
            }
            Session["SelectionQuery"] = strsql;
        }
        catch
        {
            Session["SelectionQuery"] = "";
            Session["SelectionQuery"] = null;
        }
    }
    protected void btnGenerate_Click(object sender, EventArgs e)
    {
        try
        {

            Session["SelectionQuery"] = "";
            Session["SelectionQuery"] = null;
            string ReportView = "";
            try
            {
                FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                toDate = DateTime.ParseExact(txtToDate.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            catch
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('Invalid Date Format');", true);
                return;
            }
            datefrom = txtDateFrom.Text.ToString().Trim();
            dateTo = txtToDate.Text.ToString().Trim();
            if (txtToDate.Text.Trim().ToString().Trim() != "")
            {
                toDate = FromDate.AddMonths(1).AddDays(-1);
                if (toDate < FromDate)
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('To Date Must Be Greater Than From Date');", true);
                    return;
                }
                TimeSpan ts = toDate.Subtract(FromDate);
                int day = ts.Days;
                if (day > 30)
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('Difference Is More Than One Month');", true);
                    return;
                }
            }
            string
            CompanySelection = "and tblcompany.companycode in(" + Session["Auth_Comp"].ToString().Trim() + ") " +
                          "and tbldepartment.companycode in(" + Session["Auth_Comp"].ToString().Trim() + ") " +
                          "and tblcatagory.companycode in(" + Session["Auth_Comp"].ToString().Trim() + ") " +
                          "and tbldivision.companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") " +
                          "and tblGrade.companycode in(" + Session["Auth_Comp"].ToString().Trim() + ") ";
            GetSelection();
            if (Session["SelectionQuery"] != null)
            {
                Selection = Session["SelectionQuery"].ToString();
            }
            else
            {
                if ((Session["usertype"].ToString().Trim() == "A") && (Session["PAYCODE"].ToString().ToString().ToUpper() != "ADMIN"))
                {
                    if (Session["Auth_Comp"] != null)
                    {
                        Selection += " and tblemployee.companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
                    }
                    if (Session["Auth_Dept"] != null)
                    {
                        Selection += " and tblemployee.Departmentcode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
                    }
                }
                else if (Session["usertype"].ToString().Trim() == "H")
                {
                    if (ReportView.ToString().Trim() == "Y")
                    {
                        strsql = "select paycode from tblemployee where headid='" + Session["PAYCODE"].ToString().Trim() + "'";
                        dsEmpCount = con.FillDataSet(strsql);
                        if (dsEmpCount.Tables[0].Rows.Count > 0)
                        {
                            for (int count = 0; count < dsEmpCount.Tables[0].Rows.Count; count++)
                            {
                                EmpCodes += ",'" + dsEmpCount.Tables[0].Rows[count]["paycode"].ToString().Trim() + "'";
                            }
                            if (!string.IsNullOrEmpty(EmpCodes))
                            {
                                EmpCodes = EmpCodes.Substring(1);
                            }
                            Selection += " and tblemployee.paycode in (" + EmpCodes.ToString() + ") ";
                        }
                    }
                    else
                    {
                        if (Session["Auth_Comp"] != null)
                        {
                            Selection += " and tblemployee.companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
                        }
                        if (Session["Auth_Dept"] != null)
                        {
                            Selection += " and tblemployee.Departmentcode in (" + Session["Auth_Dept"].ToString().Trim() + ") ";
                        }
                    }
                }
                else if ((Session["usertype"].ToString().Trim() == "A") && (Session["PAYCODE"].ToString().ToString().ToUpper() == "ADMIN"))
                {
                    if (Session["Com_Selection"] != null)
                    {
                        Selection = " and tblemployee.companycode in (" + Session["Com_Selection"].ToString() + ") ";
                    }
                    if (Session["Dept_Selection"] != null)
                    {
                        Selection = " and tblemployee.Departmentcode in (" + Session["Dept_Selection"].ToString() + ") ";
                    }
                }
            }
            
            DataTable dTbl;
            UpdateTimeRegister();
            if(radSencLeave.Checked)
            {
                strsql = " select tblcompany.companyname,tblCatagory.CatagoryName, tblDivision.DivisionName, tbltimeregister.Shift,  " +
                         " tblEmployee.Divisioncode 'divisioncode', tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat 'Catagorycode'," +
                         " tblTimeRegister.PayCode 'Paycode',tblEmployee.DepartmentCode 'Departmentcode',tblDepartment.DepartmentName," +
                         " convert(varchar(10),convert(datetime,dateoffice,103),103) 'DateOffice',  tblTimeRegister.Voucher_No, " +
                         " tblTimeRegister.LeaveCode,tblTimeRegister.firsthalfLeaveCode,  tblTimeRegister.SecondhalfLeaveCode,tblTimeRegister.Leaveamount1, " +
                         " tblTimeRegister.Leaveamount2,  tblTimeRegister.LeaveAmount,tblTimeRegister.Reason,tblEmployee.EmpName 'EmpName', " +
                         " tblEmployee.PresentCardNo 'presentcardno',tblEmployee.Designation, Posted='Y'  " +
                         " from tblTimeRegister join tblemployee on tblTimeRegister.PayCode = tblEmployee.PayCode " +
                         " join tblCatagory on tblCatagory.Cat = tblEmployee.Cat " +
                         " join tblDivision on tblDivision.DivisionCode = tblEmployee.DivisionCode  " +
                         " join tblCompany on tblEmployee.Companycode=tblcompany.companycode " +
                         " join tblDepartment on tblEmployee.DepartmentCode = tblDepartment.DepartmentCode   " +
                         " join tblemployeeshiftmaster on tblemployee.SSN=tblemployeeshiftmaster.SSN  join tblGrade on tblGrade.gradecode=tblemployee.gradecode " +
                         " where tblTimeRegister.LeaveAmount>0    " +
                         " And tblTimeRegister.DateOffice Between '" + FromDate.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "'  " +
                         " And (tblemployee.LeavingDate>='" + FromDate.ToString("yyyy-MM-dd") + "'  or tblemployee.LeavingDate is null) ";
                if (Selection.ToString().Trim() != "")
                {
                    strsql += " " + Selection.ToString() + " ";
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += " order by tblEmployee.PayCode ";
                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    try
                    {
                        string Tmp = "";
                        int x, i;
                        dTbl = new System.Data.DataTable();
                        DataRow dRow;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);

                        dTbl.Columns.Add("SNo");
                        dTbl.Columns.Add("PayCode");
                        dTbl.Columns.Add("CardNo");
                        dTbl.Columns.Add("Name");
                        dTbl.Columns.Add("Designation");
                        dTbl.Columns.Add("From");
                        dTbl.Columns.Add("To");
                        dTbl.Columns.Add("VoucherNo");
                        dTbl.Columns.Add("LeaveType");
                        dTbl.Columns.Add("LeaveAmount");
                        dTbl.Columns.Add("Posted");
                        dTbl.Columns.Add("Remarks");

                        dTbl.Rows[0][0] = "SNo";
                        dTbl.Rows[0][1] = "PayCode";
                        dTbl.Rows[0][2] = "Card No";
                        dTbl.Rows[0][3] = "Employee Name";
                        dTbl.Rows[0][4] = "Designation";
                        dTbl.Rows[0][5] = "Leave From";
                        dTbl.Rows[0][6] = "Leave To";
                        dTbl.Rows[0][7] = "Voucher No";
                        dTbl.Rows[0][8] = "Leave Type";
                        dTbl.Rows[0][9] = "Leave Amount";
                        dTbl.Rows[0][10] = "Posted";
                        dTbl.Rows[0][11] = "Remarks";

                        int ct = 1;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                            {

                                dTbl.Rows[ct][0] = ct.ToString();
                                dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][8].ToString();
                                dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][21].ToString();
                                dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][20].ToString();
                                dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][22].ToString();
                                dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][11].ToString(); ;
                                dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][11].ToString();
                                dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][12].ToString();
                                dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][13].ToString();
                                dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1][18].ToString();
                                dTbl.Rows[ct][10] = ds.Tables[0].Rows[i1][23].ToString();
                                dTbl.Rows[ct][11] = ds.Tables[0].Rows[i1][19].ToString();

                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                ct = ct + 1;
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        bool isEmpty;
                        for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                        {

                            isEmpty = true;
                            for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                            {
                                if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                {
                                    isEmpty = false;
                                    break;
                                }
                            }

                            if (isEmpty == true)
                            {
                                dTbl.Rows.RemoveAt(i2);
                                i2--;
                            }
                        }
                        string companyname = "";
                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim().Trim() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }

                        int colCount = Convert.ToInt32(dTbl.Columns.Count);

                        msg = " Sanctioned Leave Report from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'> " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                        x = colCount;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                    if (Tmp.Substring(0, 1) == "0")
                                    {
                                        if (Tmp.Contains(":"))
                                        {
                                            Tmp = Tmp;
                                        }
                                        else
                                        {
                                            Tmp = Tmp;
                                        }
                                    }
                                }
                                if ((i == 1) || (i == 2))
                                {
                                    if (Tmp.ToString().Trim() != "")
                                    {
                                        sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                    }
                                }
                                else if (((i == 3) || (i == 4)) && (Tmp.ToString().Trim() != ""))
                                {
                                    sb.Append("<td style='width:170px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                else if ((i == 11) && (Tmp.ToString().Trim() != ""))
                                {
                                    sb.Append("<td style='width:230px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                else
                                    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        if (radExcel.Checked)
                        {
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=SanctionedLeave.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/SanctionedLeave.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        else
                        {
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "SanctionedLeave.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        
                    }
                    catch (Exception ex)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('" + ex.Message.ToString() + "');", true);
                        return;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                    return;
                }
            }
            else if(radLeaveCard.Checked)
            {
                DataSet dsLeave = new DataSet();
                string str = "";
                string payc = "", dateFrom = "";
                double total = 0;
                double lvAccured = 0;
                double lvConsumed = 0;
                double lvBalance = 0;
                string SSN = "";
                strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,  " +
                      " tblEmployee.Paycode,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblDepartment.DepartmentName,tblEmployee.DepartmentCode,tblemployee.SSN from tblCatagory,tblDivision,tblEmployee,tblCompany,tblDepartment,tblGrade Where  tblGrade.gradecode= tblemployee.GradeCode and  tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblEmployee.CompanyCode = tblCompany.CompanyCode And  " +
                      " tblEmployee.DepartmentCode = tblDepartment.DepartmentCode And (datepart(yyyy,tblemployee.LeavingDate)>='" + ddlYear.SelectedItem.Value.ToString().Trim() + "' or tblemployee.LeavingDate is null) AND  tblemployee.active='Y' and 1=1  and  1=1 ";

                if (Selection.ToString().Trim() != "")
                {
                    strsql += " " + Selection.ToString() + " ";
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += "order by tblEmployee.payCode ";


                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    try
                    {
                        dateFrom = ddlYear.SelectedItem.Value.ToString().Trim();
                        string Tmp = "";
                        int x, i;
                        dTbl = new System.Data.DataTable();
                        DataRow dRow;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);

                        dTbl.Columns.Add("Col1");
                        dTbl.Columns.Add("Col2");
                        dTbl.Columns.Add("Col3");
                        dTbl.Columns.Add("Col4");

                        int ct = 1;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                            {
                                payc = ds.Tables[0].Rows[i1][6].ToString();
                                SSN = ds.Tables[0].Rows[i1]["SSN"].ToString();
                                str = "select	paycode,convert(decimal(10,2),L01) 'L01',convert(decimal(10,2),L01_add) 'L01_add',balance=convert(decimal(10,2),L01_add)-convert(decimal(10,2),L01), " +
                                        "holidaytype=(select leavedescription from tblleavemaster where leavefield='L01' and CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"')  " +
                                        "from	tblLeaveLedger where L01_add!=0.0 and  lyear='" + dateFrom.ToString() + "' and SSN='"+SSN+"' and  paycode='" + payc.ToString() + "' " +
                                        "UNION " +
                                        "select	paycode,convert(decimal(10,2),L02),convert(decimal(10,2),L02_add),balance=convert(decimal(10,2),L02_add)-convert(decimal(10,2),L02), " +
                                        "	holidaytype=(select leavedescription from tblleavemaster where leavefield='L02' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "') " +
                                        "from	tblLeaveLedger where L02_add!=0.0 and lyear='" + dateFrom.ToString() + "' and SSN='" + SSN + "' and paycode='" + payc.ToString() + "' " +
                                        "UNION " +
                                        "select	paycode,convert(decimal(10,2),L03),convert(decimal(10,2),L03_add),balance=convert(decimal(10,2),L03_add)-convert(decimal(10,2),L03), " +
                                            "holidaytype=(select leavedescription from tblleavemaster where leavefield='L03' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "') " +
                                        "from	tblLeaveLedger where L03_add!=0.0 and  lyear='" + dateFrom.ToString() + "' and SSN='" + SSN + "' and paycode='" + payc.ToString() + "' " +
                                        "UNION " +
                                        "select	paycode,convert(decimal(10,2),L04),convert(decimal(10,2),L04_add),balance=convert(decimal(10,2),L04_add)-convert(decimal(10,2),L04), " +
                                            "holidaytype=(select leavedescription from tblleavemaster where leavefield='L04' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "') " +
                                        "from	tblLeaveLedger where L04_add!=0.0 and lyear='" + dateFrom.ToString() + "' and SSN='" + SSN + "' and paycode='" + payc.ToString() + "' " +
                                        "UNION " +
                                        "select	paycode,convert(decimal(10,2),L05),convert(decimal(10,2),L05_add),balance=convert(decimal(10,2),L05_add)-convert(decimal(10,2),L05), " +
                                        "	holidaytype=(select leavedescription from tblleavemaster where leavefield='L05' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "') " +
                                        "from	tblLeaveLedger where L05_add!=0.0 and lyear='" + dateFrom.ToString() + "' and SSN='" + SSN + "' and paycode='" + payc.ToString() + "' " +
                                        "UNION " +
                                        "select	paycode,convert(decimal(10,2),L06),convert(decimal(10,2),L06_add),balance=convert(decimal(10,2),L06_add)-convert(decimal(10,2),L06), " +
                                        "	holidaytype=(select leavedescription from tblleavemaster where leavefield='L06' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "') " +
                                        "from	tblLeaveLedger where L06_add!=0.0 and lyear='" + dateFrom.ToString() + "' and SSN='" + SSN + "' and paycode='" + payc.ToString() + "' " +
                                        "UNION " +
                                        "select	paycode,convert(decimal(10,2),L07),convert(decimal(10,2),L07_add),balance=convert(decimal(10,2),L07_add)-convert(decimal(10,2),L07), " +
                                            "holidaytype=(select leavedescription from tblleavemaster where leavefield='L07'and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "') " +
                                        "from	tblLeaveLedger where L07_add!=0.0 and lyear='" + dateFrom.ToString() + "' and SSN='" + SSN + "' and paycode='" + payc.ToString() + "' " +
                                        "UNION " +
                                        "select	paycode,convert(decimal(10,2),L08),convert(decimal(10,2),L08_add),balance=convert(decimal(10,2),L08_add)-convert(decimal(10,2),L08), " +
                                        "	holidaytype=(select leavedescription from tblleavemaster where leavefield='L08' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "') " +
                                        "from	tblLeaveLedger where L08_add!=0.0 and lyear='" + dateFrom.ToString() + "' and SSN='" + SSN + "' and paycode='" + payc.ToString() + "' " +
                                        "UNION " +
                                        "select	paycode,convert(decimal(10,2),L09),convert(decimal(10,2),L09_add),balance=convert(decimal(10,2),L09_add)-convert(decimal(10,2),L09), " +
                                        "	holidaytype=(select leavedescription from tblleavemaster where leavefield='L09' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "') " +
                                        "from	tblLeaveLedger where L09_add!=0.0 and lyear='" + dateFrom.ToString() + "' and SSN='" + SSN + "' and paycode='" + payc.ToString() + "' " +
                                        "UNION " +
                                        "select	paycode,convert(decimal(10,2),L10),convert(decimal(10,2),L10_add),balance=convert(decimal(10,2),L10_add)-convert(decimal(10,2),L10), " +
                                            "holidaytype=(select leavedescription from tblleavemaster where leavefield='L10' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "') " +
                                        "from	tblLeaveLedger where L10_add!=0.0 and lyear='" + dateFrom.ToString() + "' and SSN='" + SSN + "' and paycode='" + payc.ToString() + "' " +
                                        "UNION " +
                                        "select	paycode,convert(decimal(10,2),L11),convert(decimal(10,2),L11_add),balance=convert(decimal(10,2),L11_add)-convert(decimal(10,2),L11), " +
                                        "	holidaytype=(select leavedescription from tblleavemaster where leavefield='L11' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "') " +
                                        "from	tblLeaveLedger where L11_add!=0.0 and lyear='" + dateFrom.ToString() + "' and SSN='" + SSN + "' and paycode='" + payc.ToString() + "' " +
                                        "UNION " +
                                        "select	paycode,convert(decimal(10,2),L12),convert(decimal(10,2),L12_add),balance=convert(decimal(10,2),L12_add)-convert(decimal(10,2),L12), " +
                                        "	holidaytype=(select leavedescription from tblleavemaster where leavefield='L12' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "') " +
                                        "from	tblLeaveLedger where L12_add!=0.0 and lyear='" + dateFrom.ToString() + "' and SSN='" + SSN + "' and paycode='" + payc.ToString() + "' " +
                                        "UNION " +
                                        "select	paycode,convert(decimal(10,2),L13),convert(decimal(10,2),L13_add),balance=convert(decimal(10,2),L13_add)-convert(decimal(10,2),L13), " +
                                        "	holidaytype=(select leavedescription from tblleavemaster where leavefield='L13' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "') " +
                                        "from	tblLeaveLedger where L13_add!=0.0 and lyear='" + dateFrom.ToString() + "' and SSN='" + SSN + "' and paycode='" + payc.ToString() + "' " +
                                        "UNION " +
                                        "select	paycode,convert(decimal(10,2),L14),convert(decimal(10,2),L14_add),balance=convert(decimal(10,2),L14_add)-convert(decimal(10,2),L14), " +
                                            "holidaytype=(select leavedescription from tblleavemaster where leavefield='L14' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "') " +
                                        "from	tblLeaveLedger where L14_add!=0.0 and lyear='" + dateFrom.ToString() + "' and SSN='" + SSN + "' and paycode='" + payc.ToString() + "' " +
                                        "UNION " +
                                        "select	paycode,convert(decimal(10,2),L15),convert(decimal(10,2),L15_add),balance=convert(decimal(10,2),L15_add)-convert(decimal(10,2),L15), " +
                                        "	holidaytype=(select leavedescription from tblleavemaster where leavefield='L15' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "') " +
                                        "from	tblLeaveLedger where L15_add!=0.0 and lyear='" + dateFrom.ToString() + "' and SSN='" + SSN + "' and paycode='" + payc.ToString() + "' " +
                                        "UNION " +
                                        "select	paycode,convert(decimal(10,2),L16),convert(decimal(10,2),L16_add),balance=convert(decimal(10,2),L16_add)-convert(decimal(10,2),L16), " +
                                        "	holidaytype=(select leavedescription from tblleavemaster where leavefield='L16' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "') " +
                                        "from	tblLeaveLedger where L16_add!=0.0 and lyear='" + dateFrom.ToString() + "' and SSN='" + SSN + "' and paycode='" + payc.ToString() + "' " +
                                        "UNION " +
                                        "select	paycode,convert(decimal(10,2),L17),convert(decimal(10,2),L17_add),balance=convert(decimal(10,2),L17_add)-convert(decimal(10,2),L17), " +
                                        "	holidaytype=(select leavedescription from tblleavemaster where leavefield='L17' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "') " +
                                        "from	tblLeaveLedger where L17_add!=0.0 and lyear='" + dateFrom.ToString() + "' and SSN='" + SSN + "' and paycode='" + payc.ToString() + "' " +
                                        "UNION " +
                                        "select	paycode,convert(decimal(10,2),L18),convert(decimal(10,2),L18_add),balance=convert(decimal(10,2),L18_add)-convert(decimal(10,2),L18), " +
                                        "	holidaytype=(select leavedescription from tblleavemaster where leavefield='L18' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "') " +
                                        "from	tblLeaveLedger where L18_add!=0.0 and lyear='" + dateFrom.ToString() + "' and SSN='" + SSN + "' and paycode='" + payc.ToString() + "' " +
                                        "UNION " +
                                        "select	paycode,convert(decimal(10,2),L19),convert(decimal(10,2),L19_add),balance=convert(decimal(10,2),L19_add)-convert(decimal(10,2),L19), " +
                                            "holidaytype=(select leavedescription from tblleavemaster where leavefield='L19' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "') " +
                                        "from	tblLeaveLedger where L19_add!=0.0 and lyear='" + dateFrom.ToString() + "' and paycode='" + payc.ToString() + "' " +
                                        "UNION " +
                                        "select	paycode,convert(decimal(10,2),L20),convert(decimal(10,2),L20_add),balance=convert(decimal(10,2),L20_add)-convert(decimal(10,2),L20), " +
                                        "	holidaytype=(select leavedescription from tblleavemaster where leavefield='L20' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "') " +
                                        "from	tblLeaveLedger where L20_add!=0.0 and lyear='" + dateFrom.ToString() + "' and SSN='" + SSN + "' and paycode='" + payc.ToString() + "' order by holidaytype";

                                dsLeave = con.FillDataSet(str);
                                if (dsLeave.Tables[0].Rows.Count > 0)
                                {
                                    total = 0;
                                    lvAccured = 0;
                                    lvConsumed = 0;
                                    lvBalance = 0;
                                    dTbl.Rows[ct][0] = "Employee's Card No :-   " + ds.Tables[0].Rows[i1][8].ToString();
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                    dTbl.Rows[ct][0] = "Employee's Code & Name :- " + ds.Tables[0].Rows[i1][6].ToString() + " -- " + ds.Tables[0].Rows[i1][7].ToString();
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                    dTbl.Rows[ct][0] = "Department Code & Name :-  " + ds.Tables[0].Rows[i1][10].ToString() + " -- " + ds.Tables[0].Rows[i1][9].ToString();
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                    dTbl.Rows[ct][0] = "LeaveType";
                                    dTbl.Rows[ct][1] = "Leave Accrued";
                                    dTbl.Rows[ct][2] = "Leave Consumed";
                                    dTbl.Rows[ct][3] = "Leave Balance";
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);

                                    ct = ct + 1;
                                    for (int kp = 0; kp < dsLeave.Tables[0].Rows.Count; kp++)
                                    {
                                        dTbl.Rows[ct][0] = dsLeave.Tables[0].Rows[kp][4].ToString();
                                        dTbl.Rows[ct][1] = dsLeave.Tables[0].Rows[kp][2].ToString();
                                        dTbl.Rows[ct][2] = dsLeave.Tables[0].Rows[kp][1].ToString();
                                        dTbl.Rows[ct][3] = dsLeave.Tables[0].Rows[kp][3].ToString();
                                        if (dsLeave.Tables[0].Rows[kp][2].ToString().Trim() != "" || dsLeave.Tables[0].Rows[kp][2].ToString().Trim() != string.Empty || dsLeave.Tables[0].Rows[kp][2].ToString().Trim() !=null)
                                        {
                                           // lvAccured = lvAccured + Convert.ToDouble(dsLeave.Tables[0].Rows[kp][2].ToString());
                                            lvAccured = lvAccured + ((string.IsNullOrEmpty(dsLeave.Tables[0].Rows[kp][2].ToString())) ? 0 : Convert.ToDouble(dsLeave.Tables[0].Rows[kp][2].ToString()));
                                        }
                                        if (dsLeave.Tables[0].Rows[kp][1].ToString().Trim() != "" || dsLeave.Tables[0].Rows[kp][1].ToString().Trim() != string.Empty || dsLeave.Tables[0].Rows[kp][1].ToString().Trim() != null)
                                        {
                                            lvConsumed = lvConsumed + ((string.IsNullOrEmpty(dsLeave.Tables[0].Rows[kp][1].ToString())) ? 0 : Convert.ToDouble(dsLeave.Tables[0].Rows[kp][1].ToString()));
                                           // lvConsumed = lvConsumed + Convert.ToDouble(dsLeave.Tables[0].Rows[kp][1].ToString());
                                        }
                                        if (dsLeave.Tables[0].Rows[kp][3].ToString().Trim() != "" || dsLeave.Tables[0].Rows[kp][3].ToString().Trim() != string.Empty || dsLeave.Tables[0].Rows[kp][3].ToString().Trim() != null)
                                        {
                                            total = total + ((string.IsNullOrEmpty(dsLeave.Tables[0].Rows[kp][3].ToString())) ? 0 : Convert.ToDouble(dsLeave.Tables[0].Rows[kp][3].ToString()));
                                            //total = total + Convert.ToDouble(dsLeave.Tables[0].Rows[kp][3].ToString());
                                        }  
                                        
                                        

                                        dRow = dTbl.NewRow();
                                        dTbl.Rows.Add(dRow);
                                        ct = ct + 1;
                                    }
                                    dTbl.Rows[ct][0] = "Total";
                                    dTbl.Rows[ct][1] = lvAccured.ToString();
                                    dTbl.Rows[ct][2] = lvConsumed.ToString();
                                    dTbl.Rows[ct][3] = total.ToString();
                                    dRow = dTbl.NewRow();
                                    dTbl.Rows.Add(dRow);
                                    ct = ct + 1;
                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        bool isEmpty;
                        for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                        {

                            isEmpty = true;
                            for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                            {
                                if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                {
                                    isEmpty = false;
                                    break;
                                }
                            }

                            if (isEmpty == true)
                            {
                                dTbl.Rows.RemoveAt(i2);
                                i2--;
                            }
                        }
                        string companyname = "";
                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }

                        int colCount = Convert.ToInt32(dTbl.Columns.Count);

                        msg = "  LEAVE CARD FOR THE YEAR " + ddlYear.SelectedItem.Text.ToString();
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'>Run Date & Time : " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                        x = colCount;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                    if (Tmp.Substring(0, 1) == "0")
                                    {
                                        if (Tmp.Contains(":"))
                                        {
                                            Tmp = Tmp;
                                        }
                                        else
                                        {
                                            Tmp = Tmp;
                                        }
                                    }
                                }
                                if ((i == 0))
                                {
                                    if ((Tmp.ToString().Trim().Contains("Employee's Card No")) || (Tmp.ToString().Trim().Contains("Employees PayRoll Code & Name")) || (Tmp.ToString().Trim().Contains("Department Code & Name")))
                                    {
                                        //sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' colspan='colspan=" + colCount + "' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        sb.Append("<td colspan='" + colCount + "' style='width:600px;text-align: left;font-weight:bold;font-size:10px'>" + Tmp + "</td>");
                                        break;
                                    }
                                    else
                                    {
                                        sb.Append("<td  style='width:65px;text-align: left;font-weight:bold;font-size:10px'>" + Tmp + "</td>");
                                    }
                                }
                                else
                                    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        if (radExcel.Checked)
                        {
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=LeaveCard.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/LeaveCard.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        else
                        {
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "LeaveCard.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str1 = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str1);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        
                    }
                    catch (Exception ex)
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('" + ex.Message.ToString() + "');", true);
                        return;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                    return;
                }

            }
            else if(radAccuredLeave.Checked)
            {
                try
                {       DataSet dsLeave=new DataSet();
                        strsql =" select tblcompany.companyname,tblCatagory.CatagoryName, tblDivision.DivisionName, " +
                                " tblEmployee.Divisioncode 'divisioncode', tblEmployee.Gradecode, " +
                                " tblEmployee.Companycode, tblEmployee.Cat 'Catagorycode',tblLeaveLedger.PayCode 'PayCode', " +
                                " isnull(convert(decimal(10,2),tblLeaveLedger.L01_ADD),0)  L01, " +
                                " isnull(convert(decimal(10,2),tblLeaveLedger.L02_ADD),0)  L02, " +
                                " isnull(convert(decimal(10,2),tblLeaveLedger.L03_ADD),0)  L03, " +
                                " isnull(convert(decimal(10,2),tblLeaveLedger.L04_ADD),0)  L04, " +
                                " isnull(convert(decimal(10,2),tblLeaveLedger.L05_ADD),0)  L05, " +
                                " isnull(convert(decimal(10,2),tblLeaveLedger.L06_ADD),0)  L06, " +
                                " isnull(convert(decimal(10,2),tblLeaveLedger.L07_ADD),0)  L07, " +
                                " isnull(convert(decimal(10,2),tblLeaveLedger.L08_ADD),0)  L08, " +
                                " isnull(convert(decimal(10,2),tblLeaveLedger.L09_ADD),0)  L09, " +
                                " isnull(convert(decimal(10,2),tblLeaveLedger.L10_ADD),0)  L10, " +
                                " isnull(convert(decimal(10,2),tblLeaveLedger.L11_ADD),0)  L11, " +
                                " isnull(convert(decimal(10,2),tblLeaveLedger.L12_ADD),0)  L12, " +
                                " isnull(convert(decimal(10,2),tblLeaveLedger.L13_ADD),0)  L13, " +
                                " isnull(convert(decimal(10,2),tblLeaveLedger.L14_ADD),0)  L14, " +
                                " isnull(convert(decimal(10,2),tblLeaveLedger.L15_ADD),0)  L15, " +
                                " isnull(convert(decimal(10,2),tblLeaveLedger.L16_ADD),0)  L16, " +
                                " isnull(convert(decimal(10,2),tblLeaveLedger.L17_ADD),0)  L17, " +
                                " isnull(convert(decimal(10,2),tblLeaveLedger.L18_ADD),0)  L18, " +
                                " isnull(convert(decimal(10,2),tblLeaveLedger.L19_ADD),0)  L19, " +
                                " isnull(convert(decimal(10,2),tblLeaveLedger.L20_ADD) ,0) L20, " +
                                " tblEmployee.EmpName 'EmpName',tblEmployee.PresentCardNo 'presentcardno', " +
                                " tblDepartment.DepartmentName,tblEmployee.departmentcode 'Departmentcode',tblEmployee.Gradecode 'Gradecode' " +
                                " from tblCatagory,tblDivision,  tblLeaveLedger,tblEmployee,tblCompany,tblDepartment,tblgrade,tblemployeeshiftmaster " +
                                " Where tblGrade.gradecode= tblemployee.GradeCode and  tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode " +
                                " and tblemployee.companycode=tblcompany.companycode and " +
                                " tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                                " And tblLeaveLedger.PayCode = tblEmployee.PayCode And tblEmployee.CompanyCode = tblCompany.CompanyCode " +
                                " And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode and TBLLEAVELEDGER.LYEAR = '" + ddlYear.SelectedItem.Value.ToString().Trim() + "' " +
                                " And (datepart(yyyy,tblemployee.LeavingDate)>='" + ddlYear.SelectedItem.Value.ToString().Trim() + "' or tblemployee.LeavingDate is null) ";

                if (Selection.ToString().Trim() != "")
                {
                    strsql += " " + Selection.ToString() + " ";
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += "order by tblEmployee.PayCode ";
                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    try
                    {
                        string Tmp = "";
                        int x, i;
                        dTbl = new System.Data.DataTable();
                        DataRow dRow;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);

                        dTbl.Columns.Add("SNo");
                        dTbl.Columns.Add("PayCode");
                        dTbl.Columns.Add("Name");
                        dTbl.Columns.Add("L01");
                        dTbl.Columns.Add("L02");
                        dTbl.Columns.Add("L03");
                        dTbl.Columns.Add("L04");
                        dTbl.Columns.Add("L05");
                        dTbl.Columns.Add("L06");
                        dTbl.Columns.Add("L07");
                        dTbl.Columns.Add("L08");
                        dTbl.Columns.Add("L09");
                        dTbl.Columns.Add("L10");
                        dTbl.Columns.Add("L11");
                        dTbl.Columns.Add("L12");
                        dTbl.Columns.Add("L13");
                        dTbl.Columns.Add("L14");
                        dTbl.Columns.Add("L15");
                        dTbl.Columns.Add("L16");
                        dTbl.Columns.Add("L17");
                        dTbl.Columns.Add("L18");
                        dTbl.Columns.Add("L19");
                        dTbl.Columns.Add("L20");

                        dTbl.Rows[0][0] = "SNo";
                        dTbl.Rows[0][1] = "PayCode";
                        dTbl.Rows[0][2] = "Name";
                        dTbl.Rows[0][3] = "L01";
                        dTbl.Rows[0][4] = "L02";
                        dTbl.Rows[0][5] = "L03";
                        dTbl.Rows[0][6] = "L04";
                        dTbl.Rows[0][7] = "L05";
                        dTbl.Rows[0][8] = "L06";
                        dTbl.Rows[0][9] = "L07";
                        dTbl.Rows[0][10] = "L08";
                        dTbl.Rows[0][11] = "L09";
                        dTbl.Rows[0][12] = "L10";
                        dTbl.Rows[0][13] = "L11";
                        dTbl.Rows[0][14] = "L12";
                        dTbl.Rows[0][15] = "L13";
                        dTbl.Rows[0][16] = "L14";
                        dTbl.Rows[0][17] = "L15";
                        dTbl.Rows[0][18] = "L16";
                        dTbl.Rows[0][19] = "L17";
                        dTbl.Rows[0][20] = "L18";
                        dTbl.Rows[0][21] = "L19";
                        dTbl.Rows[0][22] = "L20";



                        int ct = 1;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                            {

                                dTbl.Rows[ct][0] = ct.ToString();
                                dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][7].ToString();
                                dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][28].ToString();
                                dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][8].ToString();
                                dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][9].ToString();
                                dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][10].ToString();
                                dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][11].ToString();
                                dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][12].ToString();
                                dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][13].ToString();
                                dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1][14].ToString();
                                dTbl.Rows[ct][10] = ds.Tables[0].Rows[i1][15].ToString();
                                dTbl.Rows[ct][11] = ds.Tables[0].Rows[i1][16].ToString();
                                dTbl.Rows[ct][12] = ds.Tables[0].Rows[i1][17].ToString();
                                dTbl.Rows[ct][13] = ds.Tables[0].Rows[i1][18].ToString();
                                dTbl.Rows[ct][14] = ds.Tables[0].Rows[i1][19].ToString();
                                dTbl.Rows[ct][15] = ds.Tables[0].Rows[i1][20].ToString();
                                dTbl.Rows[ct][16] = ds.Tables[0].Rows[i1][21].ToString();
                                dTbl.Rows[ct][17] = ds.Tables[0].Rows[i1][22].ToString();
                                dTbl.Rows[ct][18] = ds.Tables[0].Rows[i1][23].ToString();
                                dTbl.Rows[ct][19] = ds.Tables[0].Rows[i1][24].ToString();
                                dTbl.Rows[ct][20] = ds.Tables[0].Rows[i1][25].ToString();
                                dTbl.Rows[ct][21] = ds.Tables[0].Rows[i1][26].ToString();
                                dTbl.Rows[ct][22] = ds.Tables[0].Rows[i1][27].ToString();

                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                ct = ct + 1;
                            }

                            //strsql = "Select LeaveField,LeaveDescription from tblLeaveMaster Where CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"'";
                            //dsLeave = con.FillDataSet(strsql);
                            //if (dsLeave.Tables[0].Rows.Count > 0)
                            //{
                            //    for (int lvc = 0; lvc < dsLeave.Tables[0].Rows.Count; lvc++)
                            //    {
                            //        dTbl.Rows[ct][0] = dsLeave.Tables[0].Rows[lvc][0].ToString() + " -- " + dsLeave.Tables[0].Rows[lvc][1].ToString();
                            //        dRow = dTbl.NewRow();
                            //        dTbl.Rows.Add(dRow);
                            //        ct = ct + 1;
                            //    }
                            //}
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        bool isEmpty;
                        for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                        {

                            isEmpty = true;
                            for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                            {
                                if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                {
                                    isEmpty = false;
                                    break;
                                }
                            }

                            if (isEmpty == true)
                            {
                                dTbl.Rows.RemoveAt(i2);
                                i2--;
                            }
                        }
                        string companyname = "";
                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim().Trim() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }

                        int colCount = Convert.ToInt32(dTbl.Columns.Count);

                        msg = " ACCRUAL LEAVE FOR THE YEAR " + ddlYear.SelectedItem.Text.ToString().Trim();
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'>Run Date & Time : " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                        x = colCount;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                    if (Tmp.Substring(0, 1) == "0")
                                    {
                                        if (Tmp.Contains(":"))
                                        {
                                            Tmp = Tmp;
                                        }
                                        else
                                        {
                                            Tmp = Tmp;
                                        }
                                    }
                                }
                                if ((i == 1))
                                {
                                    if (Tmp.ToString().Trim() != "")
                                    {
                                        //if (Tmp.ToString().Substring(0, 1) == "0")
                                        // {                                              
                                        sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        //}
                                        //else
                                        //{
                                        //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                        // }
                                        // }
                                    }
                                }
                                else if ((i == 2) && (Tmp.ToString().Trim() != ""))
                                {
                                    sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                else
                                {
                                    if (Tmp != "0.00")
                                    {
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                    {
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'></td>");
                                    }
                                }
                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        if (radExcel.Checked)
                        {
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=ACCRUALLEAVE.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/ACCRUALLEAVE.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        else
                        {
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "ACCRUALLEAVE.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                       
                    }
                    catch (Exception ex)
                    {
                        //Response.Write(ex.Message.ToString());
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                    return;
                }
                }
                catch
                {

                }
            }
            else if (radBalanceLeave.Checked)
            {
                    DataSet dsLeave = new DataSet();
                    strsql = " select tblcompany.companyname,tblCatagory.CatagoryName, tblDivision.DivisionName, " +
                        " tblEmployee.Divisioncode 'divisioncode', tblEmployee.Gradecode, " +
                        " tblEmployee.Companycode, tblEmployee.Cat 'Catagorycode',tblLeaveLedger.PayCode 'PayCode', " +
                        " isnull(convert(decimal(10,2),tblLeaveLedger.L01_ADD),0) - isnull(convert(decimal(10,2),tblLeaveLedger.L01),0) L01, " +
                        " isnull(convert(decimal(10,2),tblLeaveLedger.L02_ADD),0) - isnull(convert(decimal(10,2),tblLeaveLedger.L02),0) L02, " +
                        " isnull(convert(decimal(10,2),tblLeaveLedger.L03_ADD),0) - isnull(convert(decimal(10,2),tblLeaveLedger.L03),0) L03, " +
                        " isnull(convert(decimal(10,2),tblLeaveLedger.L04_ADD),0) - isnull(convert(decimal(10,2),tblLeaveLedger.L04),0) L04, " +
                        " isnull(convert(decimal(10,2),tblLeaveLedger.L05_ADD),0) - isnull(convert(decimal(10,2),tblLeaveLedger.L05),0) L05, " +
                        " isnull(convert(decimal(10,2),tblLeaveLedger.L06_ADD),0) - isnull(convert(decimal(10,2),tblLeaveLedger.L06),0) L06, " +
                        " isnull(convert(decimal(10,2),tblLeaveLedger.L07_ADD),0) - isnull(convert(decimal(10,2),tblLeaveLedger.L07),0) L07, " +
                        " isnull(convert(decimal(10,2),tblLeaveLedger.L08_ADD),0) - isnull(convert(decimal(10,2),tblLeaveLedger.L08),0) L08, " +
                        " isnull(convert(decimal(10,2),tblLeaveLedger.L09_ADD),0) - isnull(convert(decimal(10,2),tblLeaveLedger.L09),0) L09, " +
                        " isnull(convert(decimal(10,2),tblLeaveLedger.L10_ADD),0) - isnull(convert(decimal(10,2),tblLeaveLedger.L10),0) L10, " +
                        " isnull(convert(decimal(10,2),tblLeaveLedger.L11_ADD),0) - isnull(convert(decimal(10,2),tblLeaveLedger.L11),0) L11, " +
                        " isnull(convert(decimal(10,2),tblLeaveLedger.L12_ADD),0) - isnull(convert(decimal(10,2),tblLeaveLedger.L12),0) L12, " +
                        " isnull(convert(decimal(10,2),tblLeaveLedger.L13_ADD),0) - isnull(convert(decimal(10,2), tblLeaveLedger.L13),0) L13, " +
                        " isnull(convert(decimal(10,2),tblLeaveLedger.L14_ADD),0) - isnull(convert(decimal(10,2),tblLeaveLedger.L14),0) L14, " +
                        " isnull(convert(decimal(10,2),tblLeaveLedger.L15_ADD),0) - isnull(convert(decimal(10,2),tblLeaveLedger.L15),0) L15, " +
                        " isnull(convert(decimal(10,2),tblLeaveLedger.L16_ADD),0) - isnull(convert(decimal(10,2),tblLeaveLedger.L16),0) L16, " +
                        " isnull(convert(decimal(10,2),tblLeaveLedger.L17_ADD),0) - isnull(convert(decimal(10,2),tblLeaveLedger.L17),0) L17, " +
                        " isnull(convert(decimal(10,2),tblLeaveLedger.L18_ADD),0) - isnull(convert(decimal(10,2),tblLeaveLedger.L18),0) L18, " +
                        " isnull(convert(decimal(10,2),tblLeaveLedger.L19_ADD),0) - isnull(convert(decimal(10,2),tblLeaveLedger.L19),0) L19, " +
                        " isnull(convert(decimal(10,2),tblLeaveLedger.L20_ADD),0) - isnull(convert(decimal(10,2),tblLeaveLedger.L20),0) L20, " +
                        " tblEmployee.EmpName 'EmpName',tblEmployee.PresentCardNo 'presentcardno', " +
                        " tblDepartment.DepartmentName,tblEmployee.departmentcode 'Departmentcode' " +
                        " from tblCatagory,tblDivision,  tblLeaveLedger,tblEmployee,tblCompany,tblDepartment,tblemployeeshiftmaster,tblGrade " +
                        " Where  tblGrade.gradecode= tblemployee.GradeCode and  tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode " +
                        " and tblemployee.companycode=tblcompany.companycode and " +
                        " tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                        " And tblLeaveLedger.PayCode = tblEmployee.PayCode And tblEmployee.CompanyCode = tblCompany.CompanyCode " +
                        " And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode and TBLLEAVELEDGER.LYEAR = '" + ddlYear.SelectedItem.Value.ToString().Trim() + "' " +
                        " And (datepart(yyyy,tblemployee.LeavingDate)>='" + ddlYear.SelectedItem.Value.ToString().Trim() + "' or tblemployee.LeavingDate is null) ";

                if (Selection.ToString().Trim() != "")
                {
                    strsql += " " + Selection.ToString() + " ";
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += "order by tblEmployee.PayCode ";
            
            ds = con.FillDataSet(strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {

                try
                {
                    string Tmp = "";
                    int x, i;
                    dTbl = new System.Data.DataTable();
                    DataRow dRow;
                    dRow = dTbl.NewRow();
                    dTbl.Rows.Add(dRow);

                    dTbl.Columns.Add("SNo");
                    dTbl.Columns.Add("PayCode");
                    dTbl.Columns.Add("Name");
                    dTbl.Columns.Add("L01");
                    dTbl.Columns.Add("L02");
                    dTbl.Columns.Add("L03");
                    dTbl.Columns.Add("L04");
                    dTbl.Columns.Add("L05");
                    dTbl.Columns.Add("L06");
                    dTbl.Columns.Add("L07");
                    dTbl.Columns.Add("L08");
                    dTbl.Columns.Add("L09");
                    dTbl.Columns.Add("L10");
                    dTbl.Columns.Add("L11");
                    dTbl.Columns.Add("L12");
                    dTbl.Columns.Add("L13");
                    dTbl.Columns.Add("L14");
                    dTbl.Columns.Add("L15");
                    dTbl.Columns.Add("L16");
                    dTbl.Columns.Add("L17");
                    dTbl.Columns.Add("L18");
                    dTbl.Columns.Add("L19");
                    dTbl.Columns.Add("L20");

                    dTbl.Rows[0][0] = "SNo";
                    dTbl.Rows[0][1] = "PayCode";
                    dTbl.Rows[0][2] = "Name";
                    dTbl.Rows[0][3] = "L01";
                    dTbl.Rows[0][4] = "L02";
                    dTbl.Rows[0][5] = "L03";
                    dTbl.Rows[0][6] = "L04";
                    dTbl.Rows[0][7] = "L05";
                    dTbl.Rows[0][8] = "L06";
                    dTbl.Rows[0][9] = "L07";
                    dTbl.Rows[0][10] = "L08";
                    dTbl.Rows[0][11] = "L09";
                    dTbl.Rows[0][12] = "L10";
                    dTbl.Rows[0][13] = "L11";
                    dTbl.Rows[0][14] = "L12";
                    dTbl.Rows[0][15] = "L13";
                    dTbl.Rows[0][16] = "L14";
                    dTbl.Rows[0][17] = "L15";
                    dTbl.Rows[0][18] = "L16";
                    dTbl.Rows[0][19] = "L17";
                    dTbl.Rows[0][20] = "L18";
                    dTbl.Rows[0][21] = "L19";
                    dTbl.Rows[0][22] = "L20";



                    int ct = 1;
                    dRow = dTbl.NewRow();
                    dTbl.Rows.Add(dRow);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                        {

                            dTbl.Rows[ct][0] = ct.ToString();
                            dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][7].ToString();
                            dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][28].ToString();
                            dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][8].ToString();
                            dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][9].ToString();
                            dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][10].ToString();
                            dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][11].ToString();
                            dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][12].ToString();
                            dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][13].ToString();
                            dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1][14].ToString();
                            dTbl.Rows[ct][10] = ds.Tables[0].Rows[i1][15].ToString();
                            dTbl.Rows[ct][11] = ds.Tables[0].Rows[i1][16].ToString();
                            dTbl.Rows[ct][12] = ds.Tables[0].Rows[i1][17].ToString();
                            dTbl.Rows[ct][13] = ds.Tables[0].Rows[i1][18].ToString();
                            dTbl.Rows[ct][14] = ds.Tables[0].Rows[i1][19].ToString();
                            dTbl.Rows[ct][15] = ds.Tables[0].Rows[i1][20].ToString();
                            dTbl.Rows[ct][16] = ds.Tables[0].Rows[i1][21].ToString();
                            dTbl.Rows[ct][17] = ds.Tables[0].Rows[i1][22].ToString();
                            dTbl.Rows[ct][18] = ds.Tables[0].Rows[i1][23].ToString();
                            dTbl.Rows[ct][19] = ds.Tables[0].Rows[i1][24].ToString();
                            dTbl.Rows[ct][20] = ds.Tables[0].Rows[i1][25].ToString();
                            dTbl.Rows[ct][21] = ds.Tables[0].Rows[i1][26].ToString();
                            dTbl.Rows[ct][22] = ds.Tables[0].Rows[i1][27].ToString();

                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);
                            ct = ct + 1;
                        }

                        //strsql = "Select LeaveField,LeaveDescription from tblLeaveMaster";
                        //dsLeave = con.FillDataSet(strsql);
                        //if (dsLeave.Tables[0].Rows.Count > 0)
                        //{
                        //    for (int lvc = 0; lvc < dsLeave.Tables[0].Rows.Count; lvc++)
                        //    {
                        //        dTbl.Rows[ct][0] = dsLeave.Tables[0].Rows[lvc][0].ToString() + " -- " + dsLeave.Tables[0].Rows[lvc][1].ToString();
                        //        dRow = dTbl.NewRow();
                        //        dTbl.Rows.Add(dRow);
                        //        ct = ct + 1;
                        //    }
                        //}
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                        return;
                    }

                    bool isEmpty;
                    for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                    {

                        isEmpty = true;
                        for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                        {
                            if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                            {
                                isEmpty = false;
                                break;
                            }
                        }

                        if (isEmpty == true)
                        {
                            dTbl.Rows.RemoveAt(i2);
                            i2--;
                        }
                    }
                    string companyname = "";
                    strsql = "select companyname from tblcompany";
                    if (Session["Com_Selection"] != null)
                    {
                        strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                    }
                    else if (Session["Auth_Comp"] != null)
                    {
                        strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim().Trim() + ") order by companycode ";
                    }
                    DataSet dsCom = new DataSet();
                    dsCom = con.FillDataSet(strsql);
                    if (dsCom.Tables[0].Rows.Count > 0)
                    {
                        for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                        {
                            companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                        }
                    }
                    if (!string.IsNullOrEmpty(companyname.ToString()))
                    {
                        companyname = companyname.Substring(1);
                    }

                    int colCount = Convert.ToInt32(dTbl.Columns.Count);

                    msg = "BALANCE LEAVE FOR THE YEAR " + ddlYear.SelectedItem.Text.ToString().Trim();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                    sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'>Run Date & Time : " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                    sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                    sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                    sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                    sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                    x = colCount;
                    int n, m;
                    string mm = "";
                    foreach (DataRow tr in dTbl.Rows)
                    {
                        sb.Append("<tr>");
                        for (i = 0; i < x; i++)
                        {
                            Tmp = tr[i].ToString().Trim();
                            mm = "";
                            if (Tmp != "")
                            {
                                for (n = 0, m = 1; n < Tmp.Length; n++)
                                {
                                    mm = mm + "0";
                                }
                                if (Tmp.Substring(0, 1) == "0")
                                {
                                    if (Tmp.Contains(":"))
                                    {
                                        Tmp = Tmp;
                                    }
                                    else
                                    {
                                        Tmp = Tmp;
                                    }
                                }
                            }
                            if ((i == 1))
                            {
                                if (Tmp.ToString().Trim() != "")
                                {
                                    //if (Tmp.ToString().Substring(0, 1) == "0")
                                    // {                                              
                                    sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                    //}
                                    //else
                                    //{
                                    //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    // }
                                    // }
                                }
                            }
                            else if ((i == 2) && (Tmp.ToString().Trim() != ""))
                            {
                                sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                            }
                            else
                            {
                                if (Tmp != "0.00")
                                {
                                    sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                else
                                {
                                    sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'></td>");
                                }
                            }
                        }
                        sb.Append("</tr>");
                    }
                    sb.Append("</table>");
                    string tbl = sb.ToString();
                    if (radExcel.Checked)
                    {
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=BalanceLeave.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/BalanceLeave.xls";
                        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                        Response.Write(sb.ToString());
                        Response.End();
                    }
                    else
                    {
                        Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                        string pdffilename = "BalanceLeave.pdf";
                        PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                        pdfDocument.Open();
                        String htmlText = sb.ToString();
                        StringReader str = new StringReader(htmlText);
                        HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                        htmlworker.Parse(str);
                        pdfWriter.CloseStream = false;
                        pdfDocument.Close();
                        //Download Pdf  
                        Response.Buffer = true;
                        Response.ContentType = "application/pdf";
                        Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.Write(pdfDocument);
                        Response.Flush();
                        Response.End();
                    }
                   
                }
                catch (Exception ex)
                {
                    //Response.Write(ex.Message.ToString());
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                return;
            }   
            }
            else if (radConsumedLeave.Checked)
            {
                try
                {
                    DataSet dsLeave = new DataSet();
                    strsql = " select tblcompany.companyname 'companyname',tblCatagory.CatagoryName, tblDivision.DivisionName,  tblEmployee.Divisioncode 'divisioncode', tblEmployee.Gradecode, " +
                        " tblEmployee.Companycode, tblEmployee.Cat 'Catagorycode',   tblLeaveLedger.PayCode 'Paycode',isnull(convert(decimal(10,2),tblLeaveLedger.L01),0) 'L01' ,isnull(convert(decimal(10,2),tblLeaveLedger.L02),0) 'L02', " +
                        " isnull(convert(decimal(10,2),tblLeaveLedger.L03),0) 'L03',isnull(convert(decimal(10,2),tblLeaveLedger.L04),0) 'L04',isnull(convert(decimal(10,2),tblLeaveLedger.L05),0) 'L05',isnull(convert(decimal(10,2),tblLeaveLedger.L06),0) 'L06',isnull(convert(decimal(10,2),tblLeaveLedger.L07),0) 'L07',isnull(convert(decimal(10,2),tblLeaveLedger.L08),0) 'L08', " +
                        " isnull(convert(decimal(10,2),tblLeaveLedger.L09),0) 'L09',isnull(convert(decimal(10,2),tblLeaveLedger.L10),0) 'L10',isnull(convert(decimal(10,2),tblLeaveLedger.L11),0) 'L11',isnull(convert(decimal(10,2),tblLeaveLedger.L12),0) 'L12',isnull(convert(decimal(10,2),tblLeaveLedger.L13),0) 'L13',isnull(convert(decimal(10,2),tblLeaveLedger.L14),0) 'L14', " +
                        " isnull(convert(decimal(10,2),tblLeaveLedger.L15),0) 'L15',isnull(convert(decimal(10,2),tblLeaveLedger.L16),0) 'L16',isnull(convert(decimal(10,2),tblLeaveLedger.L17),0) 'L17',isnull(convert(decimal(10,2),tblLeaveLedger.L18),0) 'L18',isnull(convert(decimal(10,2),tblLeaveLedger.L19),0) 'L19',isnull(convert(decimal(10,2),tblLeaveLedger.L20),0) 'L20', " +
                        " tblEmployee.EmpName 'EmpName',tblEmployee.PresentCardNo 'presentcardno',tblDepartment.DepartmentName,tblEmployee.departmentcode 'Departmentcode' " +
                        " from tblCatagory,tblDivision,  tblLeaveLedger,tblEmployee,tblCompany,tblDepartment,tblemployeeshiftmaster,tblGrade " +
                        " Where tblGrade.gradecode= tblemployee.GradeCode  and tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode and tblcompany.companycode=tblemployee.companycode and " +
                        " tblemployee.paycode=tblemployeeshiftmaster.paycode " +
                        " And tblLeaveLedger.PayCode = tblEmployee.PayCode And tblEmployee.CompanyCode = tblCompany.CompanyCode " +
                        " And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode and TBLLEAVELEDGER.LYEAR = '" + ddlYear.SelectedItem.Value.ToString().Trim() + "' " +
                        " And (datepart(yyyy,tblemployee.LeavingDate)>='" + ddlYear.SelectedItem.Value.ToString().Trim() + "' or tblemployee.LeavingDate is null) ";

                if (Selection.ToString().Trim() != "")
                {
                    strsql += " " + Selection.ToString() + " ";
                }
                strsql += " " + CompanySelection.ToString() + " ";
                strsql += "order by tblEmployee.PayCode ";
         
                ds = con.FillDataSet(strsql);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    try
                    {
                        string Tmp = "";
                        int x, i;
                        dTbl = new System.Data.DataTable();
                        DataRow dRow;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);

                        dTbl.Columns.Add("SNo");
                        dTbl.Columns.Add("PayCode");
                        dTbl.Columns.Add("Name");
                        dTbl.Columns.Add("L01");
                        dTbl.Columns.Add("L02");
                        dTbl.Columns.Add("L03");
                        dTbl.Columns.Add("L04");
                        dTbl.Columns.Add("L05");
                        dTbl.Columns.Add("L06");
                        dTbl.Columns.Add("L07");
                        dTbl.Columns.Add("L08");
                        dTbl.Columns.Add("L09");
                        dTbl.Columns.Add("L10");
                        dTbl.Columns.Add("L11");
                        dTbl.Columns.Add("L12");
                        dTbl.Columns.Add("L13");
                        dTbl.Columns.Add("L14");
                        dTbl.Columns.Add("L15");
                        dTbl.Columns.Add("L16");
                        dTbl.Columns.Add("L17");
                        dTbl.Columns.Add("L18");
                        dTbl.Columns.Add("L19");
                        dTbl.Columns.Add("L20");

                        dTbl.Rows[0][0] = "SNo";
                        dTbl.Rows[0][1] = "PayCode";
                        dTbl.Rows[0][2] = "Name";
                        dTbl.Rows[0][3] = "L01";
                        dTbl.Rows[0][4] = "L02";
                        dTbl.Rows[0][5] = "L03";
                        dTbl.Rows[0][6] = "L04";
                        dTbl.Rows[0][7] = "L05";
                        dTbl.Rows[0][8] = "L06";
                        dTbl.Rows[0][9] = "L07";
                        dTbl.Rows[0][10] = "L08";
                        dTbl.Rows[0][11] = "L09";
                        dTbl.Rows[0][12] = "L10";
                        dTbl.Rows[0][13] = "L11";
                        dTbl.Rows[0][14] = "L12";
                        dTbl.Rows[0][15] = "L13";
                        dTbl.Rows[0][16] = "L14";
                        dTbl.Rows[0][17] = "L15";
                        dTbl.Rows[0][18] = "L16";
                        dTbl.Rows[0][19] = "L17";
                        dTbl.Rows[0][20] = "L18";
                        dTbl.Rows[0][21] = "L19";
                        dTbl.Rows[0][22] = "L20";



                        int ct = 1;
                        dRow = dTbl.NewRow();
                        dTbl.Rows.Add(dRow);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                            {

                                dTbl.Rows[ct][0] = ct.ToString();
                                dTbl.Rows[ct][1] = ds.Tables[0].Rows[i1][7].ToString();
                                dTbl.Rows[ct][2] = ds.Tables[0].Rows[i1][28].ToString();
                                dTbl.Rows[ct][3] = ds.Tables[0].Rows[i1][8].ToString();
                                dTbl.Rows[ct][4] = ds.Tables[0].Rows[i1][9].ToString();
                                dTbl.Rows[ct][5] = ds.Tables[0].Rows[i1][10].ToString();
                                dTbl.Rows[ct][6] = ds.Tables[0].Rows[i1][11].ToString();
                                dTbl.Rows[ct][7] = ds.Tables[0].Rows[i1][12].ToString();
                                dTbl.Rows[ct][8] = ds.Tables[0].Rows[i1][13].ToString();
                                dTbl.Rows[ct][9] = ds.Tables[0].Rows[i1][14].ToString();
                                dTbl.Rows[ct][10] = ds.Tables[0].Rows[i1][15].ToString();
                                dTbl.Rows[ct][11] = ds.Tables[0].Rows[i1][16].ToString();
                                dTbl.Rows[ct][12] = ds.Tables[0].Rows[i1][17].ToString();
                                dTbl.Rows[ct][13] = ds.Tables[0].Rows[i1][18].ToString();
                                dTbl.Rows[ct][14] = ds.Tables[0].Rows[i1][19].ToString();
                                dTbl.Rows[ct][15] = ds.Tables[0].Rows[i1][20].ToString();
                                dTbl.Rows[ct][16] = ds.Tables[0].Rows[i1][21].ToString();
                                dTbl.Rows[ct][17] = ds.Tables[0].Rows[i1][22].ToString();
                                dTbl.Rows[ct][18] = ds.Tables[0].Rows[i1][23].ToString();
                                dTbl.Rows[ct][19] = ds.Tables[0].Rows[i1][24].ToString();
                                dTbl.Rows[ct][20] = ds.Tables[0].Rows[i1][25].ToString();
                                dTbl.Rows[ct][21] = ds.Tables[0].Rows[i1][26].ToString();
                                dTbl.Rows[ct][22] = ds.Tables[0].Rows[i1][27].ToString();

                                dRow = dTbl.NewRow();
                                dTbl.Rows.Add(dRow);
                                ct = ct + 1;
                            }

                            //strsql = "Select LeaveField,LeaveDescription from tblLeaveMaster where CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"'";
                            //dsLeave = con.FillDataSet(strsql);
                            //if (dsLeave.Tables[0].Rows.Count > 0)
                            //{
                            //    for (int lvc = 0; lvc < dsLeave.Tables[0].Rows.Count; lvc++)
                            //    {
                            //        dTbl.Rows[ct][0] = dsLeave.Tables[0].Rows[lvc][0].ToString() + " -- " + dsLeave.Tables[0].Rows[lvc][1].ToString();
                            //        dRow = dTbl.NewRow();
                            //        dTbl.Rows.Add(dRow);
                            //        ct = ct + 1;
                            //    }
                            //}
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                            return;
                        }

                        bool isEmpty;
                        for (int i2 = 0; i2 < dTbl.Rows.Count; i2++)
                        {

                            isEmpty = true;
                            for (int j1 = 0; j1 < dTbl.Columns.Count; j1++)
                            {
                                if (string.IsNullOrEmpty(dTbl.Rows[i2][j1].ToString()) == false)
                                {
                                    isEmpty = false;
                                    break;
                                }
                            }

                            if (isEmpty == true)
                            {
                                dTbl.Rows.RemoveAt(i2);
                                i2--;
                            }
                        }
                        string companyname = "";
                        strsql = "select companyname from tblcompany";
                        if (Session["Com_Selection"] != null)
                        {
                            strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                        }
                        else if (Session["Auth_Comp"] != null)
                        {
                            strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim().Trim() + ") order by companycode ";
                        }
                        DataSet dsCom = new DataSet();
                        dsCom = con.FillDataSet(strsql);
                        if (dsCom.Tables[0].Rows.Count > 0)
                        {
                            for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                            {
                                companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                            }
                        }
                        if (!string.IsNullOrEmpty(companyname.ToString()))
                        {
                            companyname = companyname.Substring(1);
                        }

                        int colCount = Convert.ToInt32(dTbl.Columns.Count);

                        msg = "  CONSUMED LEAVE FOR THE YEAR " + ddlYear.SelectedItem.Text.ToString().Trim();
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: right'>Run Date & Time : " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                        sb.Append("<tr><td colspan='" + colCount + "' style='text-align: left'></td></tr> ");

                        x = colCount;
                        int n, m;
                        string mm = "";
                        foreach (DataRow tr in dTbl.Rows)
                        {
                            sb.Append("<tr>");
                            for (i = 0; i < x; i++)
                            {
                                Tmp = tr[i].ToString().Trim();
                                mm = "";
                                if (Tmp != "")
                                {
                                    for (n = 0, m = 1; n < Tmp.Length; n++)
                                    {
                                        mm = mm + "0";
                                    }
                                    if (Tmp.Substring(0, 1) == "0")
                                    {
                                        if (Tmp.Contains(":"))
                                        {
                                            Tmp = Tmp;
                                        }
                                        else
                                        {
                                            Tmp = Tmp;
                                        }
                                    }
                                }
                                if ((i == 1))
                                {
                                    if (Tmp.ToString().Trim() != "")
                                    {
                                        //if (Tmp.ToString().Substring(0, 1) == "0")
                                        // {                                              
                                        sb.Append(string.Format(System.Globalization.CultureInfo.InvariantCulture, "<td align='left' style='width:65px;text-align: left;font-weight:normal;font-size:10px;mso-number-format:\\@;'>{0}</td>", Tmp));
                                        //}
                                        //else
                                        //{
                                        //    sb.Append("<td style='width:65px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                        // }
                                        // }
                                    }
                                }
                                else if ((i == 2) && (Tmp.ToString().Trim() != ""))
                                {
                                    sb.Append("<td style='width:190px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                }
                                else
                                {
                                    if (Tmp != "0.00")
                                    {
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                                    }
                                    else
                                    {
                                        sb.Append("<td style='width:50px;text-align: left;font-weight:normal;font-size:10px'></td>");
                                    }
                                }

                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        string tbl = sb.ToString();
                        if (radExcel.Checked)
                        {
                            Response.Clear();
                            Response.AddHeader("content-disposition", "attachment;filename=ConsumedLeave.xls");
                            Response.Charset = "";
                            Response.Cache.SetCacheability(HttpCacheability.Private);
                            Response.ContentType = "application/ConsumedLeave.xls";
                            System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                            System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                            Response.Write(sb.ToString());
                            Response.End();
                        }
                        else
                        {
                            Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                            string pdffilename = "ConsumedLeave.pdf";
                            PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                            pdfDocument.Open();
                            String htmlText = sb.ToString();
                            StringReader str = new StringReader(htmlText);
                            HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                            htmlworker.Parse(str);
                            pdfWriter.CloseStream = false;
                            pdfDocument.Close();
                            //Download Pdf  
                            Response.Buffer = true;
                            Response.ContentType = "application/pdf";
                            Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                            Response.Cache.SetCacheability(HttpCacheability.NoCache);
                            Response.Write(pdfDocument);
                            Response.Flush();
                            Response.End();
                        }
                        
                    }
                    catch (Exception ex)
                    {
                        //Response.Write(ex.Message.ToString());
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                    return;
                }

                }
                catch
                {

                }
            }
            else if (radLeaveRegister.Checked)
            {
                string Tmp = "";
                int colCount = 0;
                try
                {
                    int x, i;
                    //// create table 
                    dTbl = new DataTable();
                    dTbl.Columns.Add("Sno");
                    dTbl.Columns.Add("Code");
                    dTbl.Columns.Add("Name");

                    DataSet ds = new DataSet();
                    strsql = "select leaveField,leavecode,leavetype from tblleavemaster where CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"'";
                    ds = con.FillDataSet(strsql);
                    for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                    {
                        dTbl.Columns.Add(ds.Tables[0].Rows[i1][0].ToString());
                    }

                    DataRow dRow;
                    i = 0;
                    dRow = dTbl.NewRow();
                    dTbl.Rows.Add(dRow);

                    dTbl.Rows[i]["SNo"] = "SNo";
                    dTbl.Rows[i]["Code"] = "Code-Name";
                    dTbl.Rows[i]["Name"] = "Date";
                    for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                    {
                        dTbl.Rows[i][ds.Tables[0].Rows[i1][0].ToString()] = ds.Tables[0].Rows[i1][1].ToString();
                    }
                    colCount = Convert.ToInt32(dTbl.Columns.Count);

                    if (string.IsNullOrEmpty(Selection.ToString()))
                    {
                        strsql = "Select tblemployee.paycode, tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,'HOD'=(select tblemp.empname from tblemployee tblemp where tblemp.paycode=tblemployee.headid)  " +
                                 " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                " where Active='Y'  order by  tblemployee.paycode ";
                    }
                    else
                    {
                        strsql = "Select tblemployee.paycode, tblemployee.empname,tblcatagory.catagoryname,tbldepartment.departmentname,'HOD'=(select tblemp.empname from tblemployee tblemp where tblemp.paycode=tblemployee.headid)  " +
                                 " from tblemployee tblemployee join tblcatagory tblcatagory on tblemployee.cat=tblcatagory.cat join tbldepartment tbldepartment on   tblemployee.departmentcode=tbldepartment.departmentcode " +
                                " where Active='Y'  " + Selection.ToString() + " order by tblemployee.paycode ";
                    }
                    DataSet dsMuster = new DataSet();
                    dsMuster = con.FillDataSet(strsql);

                    i = 1;
                    x = 1;
                    int p = 1;
                    string inout = "";
                    double totalLeaves = 0.0;
                    double Leave = 0.0;
                    double totalHD = 0.0;
                    double HD = 0.0;
                    double totalCoff = 0.0;
                    double Coff = 0.0;
                    double Coff1 = 0.0;
                    double totalLWP = 0.0;
                    double LWP = 0.0;
                    double TotalDays = 0.0;
                    double totalWO_Hld = 0.0;
                    double totalWD = 0.0;
                    double halfdayleave = 0.0;
                    double fulldayleave = 0.0;
                    double bdayleave = 0.0;
                    double totalbdayleave = 0.0;
                    double Wdays = 0.0;
                    double TotalWdays = 0.0;
                    DataSet dsStatus;
                    DateTime dRoster = System.DateTime.MinValue;
                    DateTime djoin = System.DateTime.MinValue;
                    int dd = 0;
                    string lvField;
                    DataSet dsField;
                    string lvCode;
                    DataSet dsTimeRegister;
                    string lvType = "";
                    string sumLV = "";
                    int countRows = 1;


                    if (dsMuster.Tables[0].Rows.Count > 0)
                    {
                        for (int ms = 0; ms < dsMuster.Tables[0].Rows.Count; ms++)
                        {
                            dRow = dTbl.NewRow();
                            dTbl.Rows.Add(dRow);

                            dTbl.Rows[i]["Sno"] = Convert.ToString(ms + 1);
                            Tmp = dsMuster.Tables[0].Rows[ms]["Paycode"].ToString().Trim();
                            dTbl.Rows[i]["Code"] = Tmp + " - " + dsMuster.Tables[0].Rows[ms]["Empname"].ToString().Trim();


                            x = 3;
                            {
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {
                                    lvCode = ds.Tables[0].Rows[i1][1].ToString().Trim();
                                    lvField = ds.Tables[0].Rows[i1][0].ToString().Trim();
                                    lvType = ds.Tables[0].Rows[i1][2].ToString().Trim();
                                    if (lvType.ToString().Trim() == "P")
                                    {
                                        sumLV = "leaveamount";
                                    }
                                    else if (lvType.ToString().Trim() == "L")
                                    {
                                        sumLV = "leaveamount";
                                    }
                                    else if (lvType.ToString().Trim() == "A")
                                    {
                                        sumLV = "leaveamount";
                                    }

                                    //strsql = "select isnull(Sum(" + sumLV + "),0) from tbltimeregister where paycode='" + Tmp.ToString().Trim() + "' and dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode='" + lvCode.ToString().ToUpper() + "'   ";
                                    strsql = "select isnull(Sum(" + sumLV + "),0) from tbltimeregister where paycode='" + Tmp.ToString().Trim() + "' and year(dateoffice)= '" + ddlYear.SelectedItem.Value.ToString() + "' and Leavecode='" + lvCode.ToString().ToUpper() + "'   ";
                                    dsField = con.FillDataSet(strsql);
                                    dTbl.Rows[i][lvField] = dsField.Tables[0].Rows[0][0].ToString().Trim();
                                }
                                for (int i1 = 0; i1 < ds.Tables[0].Rows.Count; i1++)
                                {
                                    lvCode = ds.Tables[0].Rows[i1][1].ToString().Trim();
                                    lvField = ds.Tables[0].Rows[i1][0].ToString().Trim();
                                    //Strsql = "select Convert(varchar(10),Dateoffice,103),paycode,status from tbltimeregister where paycode='" + Tmp.ToString() + "' and tbltimeregister.dateoffice between '" + FromDate.ToString("yyyy-MM-dd") + "' and '" + toDate.ToString("yyyy-MM-dd") + "' and Leavecode='" + lvCode.ToString().ToUpper() + "'   ";
                                    Strsql = "select Convert(varchar(10),Dateoffice,103),paycode,status from tbltimeregister where paycode='" + Tmp.ToString() + "' and year(tbltimeregister.dateoffice)= '" + ddlYear.SelectedItem.Value.ToString() + "' and Leavecode='" + lvCode.ToString().ToUpper() + "'   ";
                                    dsTimeRegister = con.FillDataSet(Strsql);
                                    if (dsTimeRegister.Tables[0].Rows.Count > 0)
                                    {
                                        for (int tc = 0; tc < dsTimeRegister.Tables[0].Rows.Count; tc++)
                                        {
                                            i++;
                                            dRow = dTbl.NewRow();
                                            dTbl.Rows.Add(dRow);
                                            dTbl.Rows[i]["Name"] = dsTimeRegister.Tables[0].Rows[tc][0].ToString().Trim();
                                            dTbl.Rows[i]["Code"] = dsTimeRegister.Tables[0].Rows[tc][1].ToString().Trim();
                                            dTbl.Rows[i][lvField] = dsTimeRegister.Tables[0].Rows[tc][2].ToString().Trim();
                                        }
                                    }
                                }
                            }
                            i++;
                        }

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('No Record Found.');", true);
                        return;
                    }

                    /*bool isEmpty;
                    for (int i2 = 1; i2 < dTbl.Rows.Count; i2++)
                    {

                        isEmpty = true;
                        for (int j = 3; j < dTbl.Columns.Count; j++)
                        {
                            if (!string.IsNullOrEmpty(dTbl.Rows[i2][0].ToString()))
                            {
                                if (Convert.ToDouble(dTbl.Rows[i2][j].ToString()) > 0)
                                {
                                    isEmpty = false;
                                    dTbl.Rows[i2][0] = countRows;
                                    countRows = countRows + 1;
                                    break;
                                }
                            }
                            else
                            {
                                isEmpty = false;                            
                                break;
                            }
                        }
                        if (isEmpty == true)
                        {
                            dTbl.Rows.RemoveAt(i2);
                            i2--;
                        }
                    }*/

                    string companyname = "";
                    strsql = "select companyname from tblcompany";
                    if (Session["Com_Selection"] != null)
                    {
                        strsql += " where companycode in (" + Session["Com_Selection"].ToString() + ") order by companycode ";
                    }
                    else if (Session["Auth_Comp"] != null)
                    {
                        strsql += " where companycode in (" + Session["Auth_Comp"].ToString().Trim().Trim() + ") order by companycode ";
                    }
                    DataSet dsCom = new DataSet();
                    dsCom = con.FillDataSet(strsql);
                    if (dsCom.Tables[0].Rows.Count > 0)
                    {
                        for (int k = 0; k < dsCom.Tables[0].Rows.Count; k++)
                        {
                            companyname += "," + dsCom.Tables[0].Rows[k][0].ToString().Trim();
                        }
                    }
                    if (!string.IsNullOrEmpty(companyname.ToString()))
                    {
                        companyname = companyname.Substring(1);
                    }

                    int colcount = Convert.ToInt32(dTbl.Columns.Count);
                    string msg = "Leave Register from " + FromDate.ToString("dd-MM-yyyy") + " to " + toDate.ToString("dd-MM-yyyy") + " ";
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<table border='1' cellpadding='1' cellspacing='1' >");
                    sb.Append("<tr><td colspan='" + colcount + "' style='text-align: right'>Run Date & Time " + System.DateTime.Now.ToString("dd-MM-yyyy HH:mm") + "  </td></tr> ");
                    sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + companyname.ToString() + "  </td></tr> ");
                    sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");
                    sb.Append("<tr><td colspan='" + colcount + "' style='text-align: center'> " + msg.ToString() + "  </td></tr> ");
                    sb.Append("<tr><td colspan='" + colcount + "' style='text-align: left'></td></tr> ");

                    x = dTbl.Columns.Count;
                    int n, m;
                    string mm = "";
                    foreach (DataRow tr in dTbl.Rows)
                    {
                        sb.Append("<tr>");
                        for (i = 0; i < x; i++)
                        {
                            Tmp = tr[i].ToString().Trim();
                            mm = "";
                            if (Tmp != "")
                            {
                                for (n = 0, m = 1; n < Tmp.Length; n++)
                                {
                                    mm = mm + "0";
                                }
                            }
                            
                            if (Convert.ToInt32(Tmp.ToString().Length) >= 15)
                                sb.Append("<td style='width:200px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                            else if (Convert.ToInt32(Tmp.ToString().Length) == 10)
                                sb.Append("<td style='width:110px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");
                            else
                                sb.Append("<td style='width:90px;text-align: left;font-weight:normal;font-size:10px'>" + Tmp + "</td>");

                        }
                        sb.Append("</tr>");
                    }
                    sb.Append("</table>");
                    string tbl = sb.ToString();
                    if (radExcel.Checked)
                    {
                        Response.Clear();
                        Response.AddHeader("content-disposition", "attachment;filename=LeaveRegister.xls");
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.Private);
                        Response.ContentType = "application/LeaveRegister.xls";
                        System.IO.StringWriter stringWrite = new System.IO.StringWriter();
                        System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
                        Response.Write(sb.ToString());
                        Response.End();
                    }
                    else
                    {
                        Document pdfDocument = new Document(PageSize.A2.Rotate(), 10f, 10f, 100f, 0f);
                        string pdffilename = "LeaveRegister.pdf";
                        PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, HttpContext.Current.Response.OutputStream);
                        pdfDocument.Open();
                        String htmlText = sb.ToString();
                        StringReader str = new StringReader(htmlText);
                        HTMLWorker htmlworker = new HTMLWorker(pdfDocument);
                        htmlworker.Parse(str);
                        pdfWriter.CloseStream = false;
                        pdfDocument.Close();
                        //Download Pdf  
                        Response.Buffer = true;
                        Response.ContentType = "application/pdf";
                        Response.AppendHeader("Content-Disposition", "attachment; filename=" + pdffilename);
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.Write(pdfDocument);
                        Response.Flush();
                        Response.End();
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('" + Tmp.ToString() + "--" + ex.Message.ToString() + "');", true);
                    return;
                }
            }
            else
            {

            }




        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "TimeWatch", "alert('" + ex.Message.ToString() + "');", true);
            return;
        }
    }
    protected void txtDateFrom_DateChanged(object sender, EventArgs e)
    {
        try
        {
            FromDate = DateTime.ParseExact(txtDateFrom.Text.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
        }
        catch
        {
            ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "err_msg", "alert('Invalid Date Format');", true);
            txtDateFrom.Focus();
            return;
        }
        toDate = FromDate;
        txtToDate.Text = toDate.AddMonths(1).AddDays(-1).ToString("dd/MM/yyyy");
    }
}