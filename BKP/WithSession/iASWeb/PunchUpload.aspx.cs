﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using FKWeb;
using Newtonsoft.Json.Linq;
using DevExpress.Web;
using System.Text.RegularExpressions;
using System.IO;
using Newtonsoft.Json;
//using BioCloud;
using System.Data.Odbc;


public partial class PunchUpload : System.Web.UI.Page
{
    OleDbConnection MyConn = new OleDbConnection();
    OleDbCommand MyCmd = new OleDbCommand();
    Class_Connection cn = new Class_Connection();
    public string KeyValue = "";
    public string TableMain = null;
    public string TableName = null;
    public string ColumnName = null;
    const int GET_DEVICE_STATUS = 1;
    const int DEL_USER_INFO = 3;
    ErrorClass ec = new ErrorClass();
    SqlConnection msqlConn;
    string fileName = "";
    string UName, PhotoPath, Face;


    DataSet Ds;
    string StrSql, Qry, change, Tmp;
    Class1 cls = new Class1();

    OdbcCommand ocmd;
    OdbcDataAdapter oDa;


    string Strsql = null;
    string Msg;
    DataSet ds = null;
    int result = 0;
    OleDbDataReader dr;


    string PayCode = null;
    DateTime dateofjoin;
    string Shift = null;
    string ShiftAttended = null;
    string ALTERNATEOFFDAYS = null;
    string firstoff = null;
    string secondofftype = null;
    string secondoff = null;
    string HALFDAYSHIFT = null;
    string SSN = null;
    DateTime selecteddate;
    DateTime FirstDate;
    DateTime LastDate;
    DateTime date;
    string ShiftType = null;
    string shiftpattern = null;
    string WOInclude = null;
    string[] words;
    int shiftcount = 0;

    string Status = "";
    int AbsentValue = 0;
    int WoVal = 0;
    int PresentValue = 0;
    string p = null;
    int shiftRemainDays = 0;

    string btnGoBack = "";
    string resMsg = "";
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["SqlConnFkWeb"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        lblStatus.Text = "";
        if (!IsPostBack)
        {
            lblStatus.Text = "";
            msqlConn = FKWebTools.GetDBPool();
            if (Session["LoginUserType"].ToString().Trim() == null)
            {
                Response.Redirect("Login.aspx");
            }
        }
    }
    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        //Call The function to write the error log file 

        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        {
            ec.Write_Log(PageName, "Error_Occured", ess.Message);
        }
    }

    public static string[] ExcelSheetNames_First(String excelFile)
    {
        DataTable dt;
        string[] res = new string[0];
        //string connString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelFile + ";Extended Properties=HTML Import;";
        string connString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelFile + ";Extended Properties=Excel 8.0;";
        using (OleDbConnection objConn = new OleDbConnection(connString))
        {
            objConn.Open();
            dt =
            objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            if (dt == null)
            {
                return null;
            }
            res = new string[dt.Rows.Count];
            for (int i = 0; i < res.Length; i++)
            {
                string name = dt.Rows[i]["TABLE_NAME"].ToString();
                if (name[0] == '\'')
                {
                    //numeric sheetnames get single quotes around
                    //remove them here
                    if (Regex.IsMatch(name, @"^'\d\w+\$'$"))
                    {
                        name = name.Substring(1, name.Length - 2);
                    }
                }
                res[i] = name;
            }
            return res;
        }
    }
    protected void ASPxButton1_Click(object sender, EventArgs e)
    {
        try
        {
            lblStatus.Text = "";
            string str_FileLocation = "";
            string xConnStr = "";
            str_FileLocation = Server.MapPath("UploadTemp");
            str_FileLocation = str_FileLocation + "\\" + fileName;
            int date = 0;
            DateTime PunchDate;
            date = Convert.ToInt32(System.DateTime.Now.Day);
            string SSN = "";

            DataSet dsResult = new DataSet();

            DataSet dscon = new DataSet();
            DataSet dsDept = new DataSet();

            DateTime dob = System.DateTime.MinValue, doj = System.DateTime.MinValue;
            DataSet dsRecord = new DataSet();

            DataSet dspaycode = new DataSet();
            string[] d = new string[3];
            string[] dj = new string[3];
            string[] a = new string[3];

            string PayCode = "";
            string Company = "";
            int L01 = 0;
            int L02 = 0;
            int L03 = 0;
            int L04 = 0;
            int L05 = 0;
            int L06 = 0;
            int L07 = 0;
            int L08 = 0;
            int L09 = 0;
            int L10 = 0;
            int LYear = System.DateTime.Now.Year;
           
            if (File.Exists(str_FileLocation))
            {
                File.Delete(str_FileLocation);
            }
            ASPxUploadControl1.SaveAs(str_FileLocation);
            string[] f = ExcelSheetNames_First(str_FileLocation);

            if (fileName.ToString().Trim() == "")
            {
                lblStatus.Text = "Please Select File";
                return;
            }
            else
            {
                try
                {
                    if (string.IsNullOrEmpty(f[0].ToString().Trim()))
                    {

                        lblStatus.Text = "Excel File Is Not In Valid Format...";
                        return;

                    }
                    string Tmp;
                    string Pcode = "", CardNo = "", CompanyCode = "", DepartmentCode = "";
                   
                    string[] tme = new string[2];
                    int hour = 23;
                    int min = 59;
                    string SetupID = "";

                    DataSet Rs = new DataSet();
                    string sSql = " Select setupid from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tblsetup)";
                    Rs = cn.FillDataSet(sSql);
                    if (Rs.Tables[0].Rows.Count > 0)
                    {
                        SetupID = Rs.Tables[0].Rows[0][0].ToString().Trim();
                    }

                    xConnStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + str_FileLocation + @";Extended Properties=""Excel 8.0;IMEX=1;HDR=YES;TypeGuessRows=0;ImportMixedTypes=Text""";

                    using (OleDbConnection connection = new OleDbConnection(xConnStr))
                    {
                        OleDbCommand command = new OleDbCommand("Select * FROM [" + f[0] + "] ", connection);
                        connection.Open();
                        using (OleDbDataReader dr1 = command.ExecuteReader())
                        {
                            while (dr1.Read())
                            {
                                Pcode = dr1[0].ToString().Trim();
                                Company = dr1[1].ToString().Trim();
                                PunchDate = Convert.ToDateTime(dr1[2].ToString().Trim());
                                SSN = Company.Trim() + "_" + PayCode.Trim();

                                if (PunchDate> System.DateTime.Now)
                                {
                                    continue;
                                }
                                if(Company.Trim()!= Session["LoginCompany"].ToString().Trim())
                                {
                                    lblStatus.Text = "Not A Valid Company";
                                    return;
                                }


                                Strsql = "Select presentCardno,companycode,departmentcode,ssn from tblemployee where Active='Y' And paycode ='" + Pcode + "' and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' ";
                                dr = cn.ExecuteReader(Strsql);
                                if (dr.Read())
                                    CardNo = dr[0].ToString().Trim();
                                CompanyCode = dr[1].ToString().Trim();
                                DepartmentCode = dr[2].ToString().Trim();
                                SSN = dr[3].ToString().Trim();
                                dr.Close();
                                if (CardNo.Trim() == "")
                                {
                                    continue;
                                }
                                else
                                {
                                    Strsql = "Select * from MachineRawPunch where paycode = '" + Pcode + "' And OfficePunch = '" + PunchDate.ToString("yyyy-MM-dd HH:mm") + "'  and CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
                                    dr = cn.Execute_Reader(Strsql);
                                    if (dr.Read())
                                    {
                                        continue;
                                    }
                                    Strsql = "Insert into MachineRawPunch  (CARDNO,PAYCODE,ISMANUAL,P_Day,OFFICEPUNCH,CompanyCode,SSN)  VALUES('" + CardNo + "','" + Pcode + "','Y','N','" + PunchDate.ToString("yyyy-MM-dd HH:mm") + "','" + CompanyCode + "','" + SSN + "') ";
                                    result = cn.execute_NonQuery(Strsql);

                                    if (result > 0)
                                    {
                                        Strsql = "Insert into tblLog_ManualPunch (Paycode,PunchDate,LoggedUser,LoggedDate)  VALUES('" + Pcode + "','" + PunchDate.ToString("yyyy-MM-dd HH:mm") + "','" + Session["PAYCODE"].ToString().Trim() + "',getdate() )";
                                        result = cn.execute_NonQuery(Strsql);

                                        try
                                        {
                                            sSql = "INSERT INTO [dbo].[UserEditLog]([UserName],[LogDate],[LogDetails],[OldValue],[NewValue],[CompanyCode])values('" + Session["UserName"].ToString().Trim() + "',getdate(),'Manual Punch Posted For Code: " + Pcode + "','Punch Date: " + PunchDate.ToString("dd/MM/yyyy") + "','Punch Time: " + PunchDate.ToString("yyyy-MM-dd HH:mm") + "','" + Session["LoginCompany"].ToString().Trim() + "')";
                                            cn.execute_NonQuery(sSql);

                                        }
                                        catch
                                        {

                                        }

                                            try
                                            {
                                                using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString()))
                                                {

                                                    MyCmd = new OleDbCommand("ProcessBackDate", MyConn);
                                                    MyCmd.CommandTimeout = 1800;
                                                    MyCmd.CommandType = CommandType.StoredProcedure;
                                                    MyCmd.Parameters.Add("@FromDate", SqlDbType.VarChar).Value = PunchDate.AddDays(-1).ToString("yyyy-MM-dd");
                                                    MyCmd.Parameters.Add("@ToDate", SqlDbType.VarChar).Value = System.DateTime.Now.ToString("yyyy-MM-dd");
                                                    MyCmd.Parameters.Add("@CompanyCode", SqlDbType.VarChar).Value = CompanyCode;
                                                    MyCmd.Parameters.Add("@DepartmentCode", SqlDbType.VarChar).Value = DepartmentCode;
                                                    MyCmd.Parameters.Add("@PayCode", SqlDbType.VarChar).Value = Pcode;
                                                    MyCmd.Parameters.Add("@SetupId", SqlDbType.VarChar).Value = SetupID;
                                                    MyConn.Open();
                                                    result = MyCmd.ExecuteNonQuery();
                                                    MyConn.Close();

                                                }
                                            }
                                            catch
                                            {


                                            }
                                       
                                    }
                                }  

                            }
                            dr1.Close();
                        }
                    }





                    lblStatus.Text = "Data Uploaded  Successfully";
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Upload Successfully');document.location='Home.aspx'", true);
                }
                catch (Exception Ex)
                {
                    lblStatus.Text = Ex.Message;
                    Error_Occured("Upload Function", Ex.Message);
                }
            }



        }
        catch (Exception Ex)
        {
            lblStatus.Text = Ex.Message;
            Error_Occured("ASPxButton1_Click", Ex.Message);
        }
    }




    public byte[] StreamToByteArray(Stream input)
    {
        byte[] total_stream = new byte[0];
        byte[] stream_array = new byte[0];
        byte[] buffer = new byte[1024];

        int read = 0;
        while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
        {
            stream_array = new byte[total_stream.Length + read];
            total_stream.CopyTo(stream_array, 0);
            Array.Copy(buffer, 0, stream_array, total_stream.Length, read);
            total_stream = stream_array;
        }

        return total_stream;
    }
    protected void ASPxUploadControl1_FilesUploadComplete(object sender, FilesUploadCompleteEventArgs e)
    {
        System.Threading.Thread.Sleep(2000);

        ASPxUploadControl uploadControl = sender as ASPxUploadControl;

        if (uploadControl.UploadedFiles != null && uploadControl.UploadedFiles.Length > 0)
        {
            for (int i = 0; i < uploadControl.UploadedFiles.Length; i++)
            {
                UploadedFile file = uploadControl.UploadedFiles[i];
                if (file.FileName != "")
                {
                    fileName = file.FileName.ToString().Trim();//string.Format("{0}{1}", MapPath("~/UploadTemp/"), file.FileName);

                }
            }
        }

    }
}