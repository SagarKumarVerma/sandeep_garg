using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web;

    public partial class RootMaster : System.Web.UI.MasterPage {
        protected void Page_Load(object sender, EventArgs e) {
            ASPxLabel2.Text = DateTime.Now.Year + Server.HtmlDecode(" &copy; Copyright by [TimeWatch Infocom Pvt. Ltd.]");
            ASPxLabel1.Text = Server.HtmlDecode("Login Time") + DateTime.Now;
        }
        private void LogOut()
        {
            Session.Clear();
            Session.Abandon();
            Response.Redirect("Login.aspx");
        }
      
}