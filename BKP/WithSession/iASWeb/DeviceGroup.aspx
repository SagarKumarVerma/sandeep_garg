﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="DeviceGroup.aspx.cs" Inherits="DeviceGroup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
     <div style="border-style: solid; border-width: thin; border-color: inherit;">
    <table class="dxeBinImgCPnlSys">
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="left">
                <dx:ASPxLabel ID="lblStatus" runat="server" Text="" Visible="false" Theme="SoftOrange">
                </dx:ASPxLabel>
            </td>
            <td align="center">
                &nbsp;</td>
            <td align="center">
                &nbsp;</td>
            <td align="center">
              
            </td>
            <td align="center">
                &nbsp;</td>
            <td align="center">
                &nbsp;</td>
            <td align="center">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="7">
                <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" ClientInstanceName="ASPxCallbackPanel1" runat="server" Theme="SoftOrange" Width="100%">
                    <PanelCollection>
                        <dx:PanelContent runat="server">
                            <dx:ASPxGridView ID="ASPxGridView1" runat="server" AutoGenerateColumns="False"
                                EnableTheming="True" KeyFieldName="GroupID"
                                Theme="SoftOrange" Width="100%" Font-Size="Small" OnRowUpdating="ASPxGridView1_RowUpdating" OnRowInserting="ASPxGridView1_RowInserting" OnCellEditorInitialize="ASPxGridView1_CellEditorInitialize" OnRowDeleting="ASPxGridView1_RowDeleting1" OnRowValidating="ASPxGridView1_RowValidating">
                                <Settings HorizontalScrollBarMode="Visible"
                                    VerticalScrollableHeight="400" VerticalScrollBarMode="Auto"
                                    VerticalScrollBarStyle="VirtualSmooth" ShowFilterRow="True" />
                                <SettingsResizing ColumnResizeMode="Control" />
                                <SettingsPager PageSize="50">
                                </SettingsPager>
                                <SettingsEditing Mode="PopupEditForm" EditFormColumnCount="1">
                                    <BatchEditSettings EditMode="Row" />
                                </SettingsEditing>
                                <Settings HorizontalScrollBarMode="Visible"
                                    VerticalScrollableHeight="400" />
                                <SettingsBehavior ConfirmDelete="True" />
                                <SettingsResizing ColumnResizeMode="Control" />
                                <SettingsCommandButton>
                                    <NewButton ButtonType="Image" RenderMode="Image">
                                        <Image IconID="actions_add_16x16">
                                        </Image>
                                    </NewButton>
                                    <UpdateButton ButtonType="Image" RenderMode="Image">
                                        <Image IconID="save_save_16x16office2013">
                                        </Image>
                                    </UpdateButton>
                                    <CancelButton ButtonType="Image" RenderMode="Image">
                                        <Image IconID="actions_cancel_16x16office2013">
                                        </Image>
                                    </CancelButton>
                                    <EditButton ButtonType="Image" RenderMode="Image">
                                        <Image IconID="actions_edit_16x16devav">
                                        </Image>
                                    </EditButton>
                                    <DeleteButton ButtonType="Image" RenderMode="Image">
                                        <Image IconID="actions_trash_16x16">
                                        </Image>

                                    </DeleteButton>
                                </SettingsCommandButton>
                                <SettingsPopup>
                                    <EditForm Modal="True" Width="400px" HorizontalAlign="WindowCenter"
                                        PopupAnimationType="Slide" VerticalAlign="WindowCenter" />
                                </SettingsPopup>
                                <SettingsSearchPanel Visible="True" />
                                <SettingsText CommandUpdate="Save" PopupEditFormCaption="Device Group Management" ConfirmDelete="Are You Sure Want To Delete Group?" />
                                <Columns>
                                    <dx:GridViewCommandColumn ShowDeleteButton="True" ShowInCustomizationForm="True" VisibleIndex="0" Width="8%" Caption=" " ShowEditButton="True" ShowNewButtonInHeader="True" ShowClearFilterButton="True">
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn Caption="Group ID" FieldName="GroupID"
                                        ShowInCustomizationForm="True" VisibleIndex="1" Width="150px">
                                        <PropertiesTextEdit MaxLength="10">
                                            <ValidationSettings>
                                                <RequiredField ErrorText="* " IsRequired="True" />
                                            </ValidationSettings>
                                        </PropertiesTextEdit>
                                        <CellStyle ForeColor="Blue">
                                        </CellStyle>
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataTextColumn Caption="Group Name" FieldName="GroupName"
                                        ShowInCustomizationForm="True" VisibleIndex="2" Width="200px">
                                        <PropertiesTextEdit>
                                            <ValidationSettings>
                                                <RequiredField IsRequired="True" />
                                            </ValidationSettings>
                                        </PropertiesTextEdit>
                                    </dx:GridViewDataTextColumn>




                                    <dx:GridViewDataComboBoxColumn Caption="Device Type" FieldName="Device" ShowInCustomizationForm="True" VisibleIndex="3">
                                        <PropertiesComboBox>
                                            <ValidationSettings>
                                                <RequiredField IsRequired="True" />
                                            </ValidationSettings>
                                            <Items>
                                                <dx:ListEditItem Selected="True" Text="BIO" Value="B" />
                                                <dx:ListEditItem Text="HTSeries" Value="H" />
                                                <dx:ListEditItem Text="ZK" Value="Z" />

                                            </Items>
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>




                                </Columns>
                            </dx:ASPxGridView>
                        </dx:PanelContent>
                    </PanelCollection>

                </dx:ASPxCallbackPanel>
            </td>
        </tr>
    </table>
    </div>
</asp:Content>

