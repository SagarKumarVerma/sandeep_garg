﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using GlobalSettings;


public partial class frmBackDateProcess : System.Web.UI.Page
{
    OleDbConnection MyConn = new OleDbConnection();
    OleDbCommand MyCmd = new OleDbCommand();
    Class1 cCount = new Class1();
    Class_Connection Con = new Class_Connection();
    string Strsql = null;
    string Msg;
    DataSet ds = null;
    int result = 0;
    OleDbDataReader dr;
    string ReportView = "";
    string PayCode = null;
    DateTime dateofjoin;
    string Shift = null;
    string ShiftAttended = null;
    string ALTERNATEOFFDAYS = null;
    string firstoff = null;
    string secondofftype = null;
    string secondoff = null;
    string HALFDAYSHIFT = null;
    DateTime selecteddate;
    DateTime FirstDate;
    DateTime LastDate;
    DateTime date;
    string ShiftType = null;
    string shiftpattern = null;
    string WOInclude = null;
    string[] words;
    int shiftcount = 0;
    ErrorClass ec = new ErrorClass();
    string Status = "";
    int AbsentValue = 0;
    int WoVal = 0;
    int PresentValue = 0;
    string p = null;
    int shiftRemainDays = 0;
    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
            {
                Request.Browser.Adapters.Clear();
            }
            if (Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            //if (Session["BackDateProcessVisible"] == null)
            //{
            //    Response.Redirect("PageNotFound.aspx");
            //}

            TxtFromDate.Text = "01/" + System.DateTime.Now.Month.ToString("00") + "/" + System.DateTime.Now.Year.ToString("0000");
            TxtToDate.Text = System.DateTime.Now.Day.ToString("00") + "/" + System.DateTime.Now.Month.ToString("00") + "/" + System.DateTime.Now.Year.ToString("0000");
            //  TxtFromDate.Value = Convert.ToDateTime(System.DateTime.Now.ToString("yyyy-MM-01"));
            if (Request.QueryString["ID"] != null)
            {
                string Grade = Request.QueryString["ID"].ToString();

            }
            bindPaycode();

            bindCompany();
            PnlProgress.Visible = false;
        }
    }
    protected void Page_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();
        if (ex is HttpRequestValidationException)
        {
            string resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" +
                   "Form doesn't need to contain valid HTML, just anything with opening and closing angled brackets (<...>).<br />" +
                   btnGoBack +
                   "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
    }
    protected void bindCompany()
    {
        try
        {
            Strsql = "select CompanyCode,convert(varchar(100),CompanyName)+' - '+convert(varchar(100),CompanyCode) as 'Comid' from tblCompany where CompanyCode!=''";
            if ((Session["Auth_Comp"] != null) && (Session["PAYCODE"].ToString().Trim().ToUpper() != "ADMIN"))
            {
                Strsql += " and companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
            }
            else if (Session["usertype"].ToString().Trim() == "H")
            {
                Strsql += " and companycode in (select companycode from tblemployee where paycode='" + Session["PAYCODE"].ToString().Trim() + "') ";
            }

            ds = new DataSet();
            ds = Con.FillDataSet(Strsql);
            if (ds.Tables[0].Rows.Count > 0)
            {

                ddlCompany.DataSource = ds;
                ddlCompany.TextField = "Comid";
                ddlCompany.ValueField = "CompanyCode";
                ddlCompany.DataBind();
                ddlCompany.SelectedIndex = 0;
            }
            else
            {
                ddlCompany.Items.Add("NONE");
                ddlCompany.SelectedIndex = 0;

            }
        }
        catch (Exception ex)
        {
            Error_Occured("bindCompany", ex.Message);
        }
    }

    protected void bindPaycode()
    {
        ReportView = ConfigurationSettings.AppSettings["ReportView_HeadID"].ToString();
        Strsql = "Select paycode,rtrim(Empname) + ' - ' + paycode as Empname1  from tblemployee where Active='Y' ";
        if (Session["usertype"].ToString() == "A")
        {
            if (Session["Auth_Comp"] != null)
            {
                Strsql += " and companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
            }
            if (Session["Auth_Dept"] != null)
            {
                Strsql += " and Departmentcode in (" + Session["Auth_Dept"].ToString().Trim() + ") ";
            }
            Strsql += "  Order by EmpName ";
        }
        else if (Session["usertype"].ToString().Trim() == "H")
        {
            if (ReportView.ToString().Trim() == "Y")
            {
                Strsql += " and headid = '" + Session["PAYCODE"].ToString().ToString().Trim() + "' ";
            }
            else
            {
                if (Session["Auth_Comp"] != null)
                {
                    Strsql += " and companycode in (" + Session["Auth_Comp"].ToString().Trim() + ") ";
                }
                if (Session["Auth_Dept"] != null)
                {
                    Strsql += " and Departmentcode in (" + Session["Auth_Dept"].ToString().Trim() + ") ";
                }
            }
            Strsql += "  Order by EmpName ";
        }
        else
            return;


        ds = new DataSet();
        ds = Con.FillDataSet(Strsql);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlPaycode.DataSource = ds;
            ddlPaycode.TextField = "EmpName1";
            ddlPaycode.ValueField = "paycode";
            ddlPaycode.DataBind();
            ddlPaycode.Items.Add("None");
            ddlPaycode.SelectedIndex = -1;
        }
        else
        {
            ddlPaycode.Items.Add("NONE");
            ddlPaycode.SelectedIndex = 0;
        }
    }

    private void Error_Occured(string FunctionName, string ErrorMsg)
    {
        //Call The function to write the error log file 

        string PageName = HttpContext.Current.Request.Url.AbsolutePath;
        PageName = PageName.Remove(0, 1);
        PageName = PageName.Substring(PageName.IndexOf("/") + 1, PageName.Trim().Length - (PageName.Trim().IndexOf("/") + 1));
        try
        {
            ec.Write_Log(PageName, FunctionName, ErrorMsg);
        }
        catch (Exception ess)
        { }
    }

    protected void RadSal_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RadSal.SelectedItem.Text.ToString().Trim().ToUpper() == "ALL")
        {
            ddlPaycode.Enabled = false;
        }
        else
        {
            ddlPaycode.Enabled = true;
        }
    }

    protected void btnCreate_Click(object sender, EventArgs e)
    {
        try
        {
          
            string msg = "";
            int rCount = 0;
            string Pcode = "", CardNo = "", CompanyCode = "", DepartmentCode = "";

            string SetupID = "";
            DateTime dateFrom;
            DateTime dateTo=System.DateTime.Today;
            
            try
            {
                dateFrom = Convert.ToDateTime(TxtFromDate.Value);
                dateTo = Convert.ToDateTime(TxtToDate.Value);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Invalid Date');", true);
                return;
            }
           
            if(dateFrom>dateTo)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('From Date Is Greater Than To Date');", true);
                return;
            }


            DataSet Rs = new DataSet();
            string sSql = "select max(cast(setupid as int)) from tblsetup where CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
            Rs = Con.FillDataSet(sSql);
            if (Rs.Tables[0].Rows.Count > 0)
            {
                SetupID = Rs.Tables[0].Rows[0][0].ToString().Trim();
            }
            if (TxtFromDate.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Please Input Date');", true);
                TxtFromDate.Focus();
                return;
            }
            if (RadSal.SelectedItem.Text.ToString().Trim().ToUpper() != "ALL" && ddlPaycode.SelectedItem.Text.Trim().ToUpper() == "NONE")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Please Select Any Record');", true);
                ddlPaycode.Focus();
                return;
            }
            if(chkRecalculate.Checked)
            {
                if(RadDev.SelectedItem.Text.ToString().Trim().ToUpper()=="BIO")
                {
                    if (RadSal.SelectedItem.Text.ToString().Trim().ToUpper() == "ALL")
                    {
                        ProcessAttendanceBIo("");
                    }
                    else
                    {
                        ProcessAttendanceBIo(ddlPaycode.SelectedItem.Value.ToString());
                    }
                }
                else
                {
                    if (RadSal.SelectedItem.Text.ToString().Trim().ToUpper() == "ALL")
                    {
                        ProcessAttendance("");
                    }
                    else
                    {
                        ProcessAttendance(ddlPaycode.SelectedItem.Value.ToString());
                    }
                }
                
                
                return;
            }
            if (chkDup.Checked)
            {
                try
                {

                    int DupMin = 5;
                    DateTime NowDate = System.DateTime.Now;
                    if (RadSal.SelectedItem.Text.ToString().Trim().ToUpper() == "ALL")
                    {

                        sSql = "Select tblemployee.PAYCODE, isnull(DUPLICATECHECKMIN,'5') DM from tblEmployeeShiftMaster inner join " +
                            "tblemployee on tblemployee.PAYCODE=tblEmployeeShiftMaster.PAYCODE and tblemployee.ACTIVE = 'Y'and tblemployee.companycode='" + Session["LoginCompany"].ToString().Trim() + "'  ";
                        DataSet DsDup = new DataSet();
                        DsDup = Con.FillDataSet(sSql);
                        if (DsDup.Tables[0].Rows.Count > 0)
                        {
                            try
                            {
                                for (int Em = 0; Em < DsDup.Tables[0].Rows.Count; Em++)
                                {
                                    DupMin = Convert.ToInt32(DsDup.Tables[0].Rows[Em]["DM"].ToString().Trim());
                                    dateFrom = Convert.ToDateTime(TxtFromDate.Value);
                                    while (dateFrom < NowDate)
                                    {
                                        RemoveDuplicatePunch(DsDup.Tables[0].Rows[Em]["PAYCODE"].ToString().Trim(), dateFrom, DupMin);
                                        dateFrom = dateFrom.AddDays(1);
                                    }
                                }


                            }
                            catch
                            {

                            }

                        }



                    }
                    else
                    {
                        sSql = "Select DUPLICATECHECKMIN from tblEmployeeShiftMaster where paycode='" + ddlPaycode.SelectedItem.Value.ToString() + "'";
                        DataSet DsDup = new DataSet();
                        DsDup = Con.FillDataSet(sSql);
                        if (DsDup.Tables[0].Rows.Count > 0)
                        {
                            try
                            {
                                DupMin = Convert.ToInt32(DsDup.Tables[0].Rows[0]["DUPLICATECHECKMIN"].ToString().Trim());
                            }
                            catch
                            {

                            }

                        }
                        while (dateFrom < NowDate)
                        {
                            RemoveDuplicatePunch(ddlPaycode.SelectedItem.Value.ToString(), dateFrom, DupMin);
                            dateFrom = dateFrom.AddDays(1);
                        }

                    }

                }
                catch
                {

                }
            }
            dateFrom = Convert.ToDateTime(TxtFromDate.Value);
            if (RadSal.SelectedItem.Text.ToString().Trim().ToUpper()=="ALL")
            {
                DataSet DSEMP = new DataSet();
                sSql = "select paycode,departmentcode,ssn from tblemployee where Active='Y' and CompanyCode='" + ddlCompany.SelectedItem.Value.ToString().Trim() + "'";
                DSEMP = Con.FillDataSet(sSql);
                if (DSEMP.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in DSEMP.Tables[0].Rows)
                    {
                        try
                        {
                            using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString()))
                            {
                                MyCmd = new OleDbCommand("ProcessBackDate", MyConn);
                                MyCmd.CommandTimeout = 1800;
                                MyCmd.CommandType = CommandType.StoredProcedure;
                                MyCmd.Parameters.Add("@FromDate", SqlDbType.VarChar).Value = dateFrom.ToString("yyyy-MM-dd");
                                MyCmd.Parameters.Add("@ToDate", SqlDbType.VarChar).Value = dateTo.ToString("yyyy-MM-dd");
                                MyCmd.Parameters.Add("@CompanyCode", SqlDbType.VarChar).Value = ddlCompany.SelectedItem.Value.ToString().Trim();
                                MyCmd.Parameters.Add("@DepartmentCode", SqlDbType.VarChar).Value = dr["departmentcode"];
                                MyCmd.Parameters.Add("@PayCode", SqlDbType.VarChar).Value = dr["paycode"];
                                MyCmd.Parameters.Add("@SetupId", SqlDbType.VarChar).Value = SetupID;
                                MyConn.Open();
                                result = MyCmd.ExecuteNonQuery();
                                MyConn.Close();

                            }
                        }
                        catch (Exception Ex)
                        {
                           
                           // continue;
                        }

                    }

                }
            }
            else
            {
                Strsql = "Select presentCardno,companycode,departmentcode from tblemployee where Active='Y' And paycode ='" + ddlPaycode.SelectedItem.Value.ToString().Trim() + "' ";
                dr = Con.ExecuteReader(Strsql);
                if (dr.Read())
                CardNo = dr[0].ToString().Trim();
                CompanyCode = dr[1].ToString().Trim();
                DepartmentCode = dr[2].ToString().Trim();
                dr.Close();
                using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString()))
                {
                    try
                    {
                        MyCmd = new OleDbCommand("ProcessBackDate", MyConn);
                        MyCmd.CommandTimeout = 1800;
                        MyCmd.CommandType = CommandType.StoredProcedure;
                        MyCmd.Parameters.Add("@FromDate", SqlDbType.VarChar).Value = dateFrom.ToString("yyyy-MM-dd");
                        MyCmd.Parameters.Add("@ToDate", SqlDbType.VarChar).Value = dateTo.ToString("yyyy-MM-dd");
                        MyCmd.Parameters.Add("@CompanyCode", SqlDbType.VarChar).Value = ddlCompany.SelectedItem.Value.ToString().Trim();
                        MyCmd.Parameters.Add("@DepartmentCode", SqlDbType.VarChar).Value = DepartmentCode;
                        MyCmd.Parameters.Add("@PayCode", SqlDbType.VarChar).Value = ddlPaycode.SelectedItem.Value.ToString().Trim();
                        MyCmd.Parameters.Add("@SetupId", SqlDbType.VarChar).Value = SetupID;
                        MyConn.Open();
                        result = MyCmd.ExecuteNonQuery();
                        MyConn.Close();
                    }
                    catch (Exception Ex)
                    {
                       // Error_Occured("Process", Ex.Message);
                        //continue;
                    }

                }
            }
            if (result > 0)
            {
               
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Attendance Re-Calcualted Successfully');document.location='frmBackDateProcess.aspx'", true);
            
        }
        catch (Exception ex)
        {
            Error_Occured("btnCreate", ex.Message);
        }
    }
    protected void Timer1_Tick(object sender, EventArgs e)
    {
       // ShowProgress();
    }
    private void ShowProgress()
    {
        //PnlProgress.Visible = true;
        ////cmdSave.Enabled = false;
        //Strsql = "select * from tblFunctionCall where FunctionName= 'BackDateProcess' ";
        //dr = Con.Execute_Reader(Strsql);
        //if (!dr.Read())
        //{
        //    PnlProgress.Visible = false;
        //    //cmdSave.Enabled = true;
        //    Timer1.Enabled = false;
        //}
        //dr.Close();
    }

    public void ProcessAttendance(string CardNO)
    {
        try
        {

            DataSet dsTime = new DataSet();
            string Sql = "", SetupID = "1", ssn = "", CompanyCode = "", DepartmentCode = "", paycode = "", RTC = "";
            DateTime dt;
            int Result = 0;
            string MinDate = TxtFromDate.Text.ToString();
            DateTime ProcessDate = Convert.ToDateTime(TxtFromDate.Value);
            DateTime ProcessDateTo = Convert.ToDateTime(TxtFromDate.Value);
            MinDate = ProcessDate.ToString("yyyy-MM-dd");
            //PnlProgress.Visible = true;
            DataSet Rs = new DataSet();
            string PCard = "";
            string sSql = "select max(cast(setupid as int)) from tblsetup where CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
            Rs = Con.FillDataSet(sSql);
            if (Rs.Tables[0].Rows.Count > 0)
            {
                SetupID = Rs.Tables[0].Rows[0][0].ToString().Trim();
            }
            DataSet ds = new DataSet();
            if (CardNO.Trim() == "")
            {
                // Sql = "select * from tbl_realtime_glog where io_time>='" + MinDate.ToString().Trim() + "'-- and user_id!='VISITOR' and device_id In (select SerialNumber from tblMachine where CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "') order by io_time ";
                Sql = "select * from UserAttendance where AttDateTime between '" + MinDate.ToString().Trim() + "' and '" + ProcessDateTo.ToString("yyyy-MM-dd 23:59:59") + "' and and UserID!='VISITOR' and  UserID not LIKE '%[^0-9]%'   order by AttDateTime ";
            }
            else
            {
                Sql = "SELECT presentcardno FROM tblemployee where paycode='" + CardNO.Trim() + "' and Companycode='" + Session["LoginCompany"].ToString().Trim() + "' ";
                dsTime = new DataSet();
                dsTime = Con.FillDataSet(Sql);
                if (dsTime.Tables[0].Rows.Count > 0)
                {
                    PCard = dsTime.Tables[0].Rows[0]["presentcardno"].ToString();

                }
                else
                {
                    PCard = "";
                }
                Sql = "select * from UserAttendance where AttDateTime between '" + MinDate.ToString().Trim() + "' and '" + ProcessDateTo.ToString("yyyy-MM-dd 23:59:59") + "' and UserID='" + Convert.ToInt64(PCard) + "'  order by AttDateTime ";
            }

            ds = new DataSet();
            ds = Con.FillDataSet(Sql);
            if (ds.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    int cardtemp = Convert.ToInt32(dr["UserID"].ToString());
                    var presendcardno = string.Format("{0:00000000}", Convert.ToInt32(cardtemp));
                    DataSet dsemp = new DataSet();
                    //sSql = "select a.*,b.departmentname,c.isroundtheclockwork from tblemployee a,tbldepartment b,tblemployeeshiftmaster c where  a.paycode=c.paycode and a.presentcardno='" + presendcardno + "' and a.departmentcode=b.departmentcode and a.dateofjoin<='" + Convert.ToDateTime(dr["RecvTime"]).ToString("MM/dd/yyyy") + "'";
                    sSql = "select a.*,b.departmentname,c.isroundtheclockwork from tblemployee a,tbldepartment b,tblemployeeshiftmaster c where  a.SSN=c.SSN and a.presentcardno='" + presendcardno + "' and a.departmentcode=b.departmentcode   and a.COMPANYCODE=b.CompanyCode and a.CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
                    dsemp = Con.FillDataSet(sSql);
                    if (dsemp.Tables[0].Rows.Count > 0)
                    {
                        DepartmentCode = dsemp.Tables[0].Rows[0]["DepartmentCode"].ToString();
                        CompanyCode = dsemp.Tables[0].Rows[0]["CompanyCode"].ToString();
                        ssn = dsemp.Tables[0].Rows[0]["ssn"].ToString();
                        paycode = dsemp.Tables[0].Rows[0]["paycode"].ToString();
                        RTC = dsemp.Tables[0].Rows[0]["isroundtheclockwork"].ToString();

                        try
                        {
                            Sql = "insert into MachineRawPunch (CardNo, OfficePunch,P_Day, IsManual,PayCode,SSN,CompanyCode) values" +
                                   " ('" + presendcardno + "','" + Convert.ToDateTime(dr["AttDateTime"]).ToString("yyyy-MM-dd HH:mm") + "','N','N','" + paycode.Trim() + "','" + ssn + "','" + CompanyCode.Trim() + "')  ";
                            Result = Con.execute_NonQuery(Sql);
                            if (Result > 0)
                            {
                                if (RTC.ToString() == "N")
                                {
                                    try
                                    {
                                        using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString()))
                                        {
                                            int result = 0;

                                            MyCmd = new OleDbCommand("ProcessBackDate", MyConn);
                                            MyCmd.CommandTimeout = 1800;
                                            MyCmd.CommandType = CommandType.StoredProcedure;
                                            MyCmd.Parameters.AddWithValue("@FromDate", Convert.ToDateTime(dr["AttDateTime"]).ToString("yyyy-MM-dd"));
                                            MyCmd.Parameters.AddWithValue("@ToDate", ProcessDateTo.ToString("yyyy-MM-dd"));
                                            MyCmd.Parameters.AddWithValue("@CompanyCode", CompanyCode);
                                            MyCmd.Parameters.AddWithValue("@DepartmentCode", DepartmentCode.ToString().Trim());
                                            MyCmd.Parameters.AddWithValue("@PayCode", paycode.ToString().Trim());
                                            MyCmd.Parameters.AddWithValue("@SetupId", SetupID);
                                            MyConn.Open();
                                            result = MyCmd.ExecuteNonQuery();
                                            MyConn.Close();

                                        }
                                    }
                                    catch
                                    {


                                    }

                                }
                                else
                                {
                                    try
                                    {
                                        using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString()))
                                        {
                                            int result = 0;
                                            DateTime PrcoessDate = Convert.ToDateTime(dr["AttDateTime"]).AddDays(-1);

                                            MyCmd = new OleDbCommand("ProcessBackDate", MyConn);
                                            MyCmd.CommandTimeout = 1800;
                                            MyCmd.CommandType = CommandType.StoredProcedure;
                                            MyCmd.Parameters.AddWithValue("@FromDate", PrcoessDate.ToString("yyyy-MM-dd"));
                                            MyCmd.Parameters.AddWithValue("@ToDate", System.DateTime.Now);
                                            MyCmd.Parameters.AddWithValue("@CompanyCode", CompanyCode);
                                            MyCmd.Parameters.AddWithValue("@DepartmentCode", DepartmentCode.ToString().Trim());
                                            MyCmd.Parameters.AddWithValue("@PayCode", paycode.ToString().Trim());
                                            MyCmd.Parameters.AddWithValue("@SetupId", SetupID);
                                            MyConn.Open();
                                            result = MyCmd.ExecuteNonQuery();
                                            MyConn.Close();

                                        }
                                    }
                                    catch
                                    {


                                    }




                                }

                            }

                        }

                        catch (Exception Ex)
                        {
                            Error_Occured("Insert MRP", Ex.Message);
                            continue;
                        }

                    }

                }

            }

            PnlProgress.Visible = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Attendance Record Calculated Successfully');", true);
        }
        catch (Exception Ex)
        {
            Error_Occured("ProcessAttendance", Ex.Message);
            //continue;
        }

    }
    public void ProcessAttendanceBIo(string CardNO)
    {
        try
        {

            DataSet dsTime = new DataSet();
            string Sql = "", SetupID = "1", ssn = "", CompanyCode = "", DepartmentCode = "", paycode = "", RTC = "";
            DateTime dt;
            int Result = 0;
            string MinDate = TxtFromDate.Text.ToString();
            DateTime ProcessDate = Convert.ToDateTime(TxtFromDate.Value);
            DateTime ProcessDateTo = Convert.ToDateTime(TxtFromDate.Value);
            MinDate = ProcessDate.ToString("yyyy-MM-dd");
            //PnlProgress.Visible = true;
            DataSet Rs = new DataSet();
            string PCard = "";
            string sSql = "select max(cast(setupid as int)) from tblsetup where CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
            Rs = Con.FillDataSet(sSql);
            if (Rs.Tables[0].Rows.Count > 0)
            {
                SetupID = Rs.Tables[0].Rows[0][0].ToString().Trim();
            }
            DataSet ds = new DataSet();
            if (CardNO.Trim() == "")
            {
               // Sql = "select * from tbl_realtime_glog where io_time>='" + MinDate.ToString().Trim() + "'-- and user_id!='VISITOR' and device_id In (select SerialNumber from tblMachine where CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "') order by io_time ";
                Sql = "select * from tbl_realtime_glog where io_time between '" + MinDate.ToString().Trim() + "' and '" + ProcessDateTo.ToString("yyyy-MM-dd 23:59:59") + "' order by io_time ";
            }
            else
            {
                Sql = "SELECT presentcardno FROM tblemployee where paycode='" + CardNO.Trim() + "' and Companycode='"+Session["LoginCompany"].ToString().Trim()+"' ";
                dsTime = new DataSet();
                dsTime = Con.FillDataSet(Sql);
                if (dsTime.Tables[0].Rows.Count > 0)
                {
                    PCard = dsTime.Tables[0].Rows[0]["presentcardno"].ToString();

                }
                else
                {
                    PCard = "";
                }
                Sql = "select * from tbl_realtime_glog where io_time between '" + MinDate.ToString().Trim() + "' and '" + ProcessDateTo.ToString("yyyy-MM-dd 23:59:59") + "' and user_id='" + Convert.ToInt64(PCard) + "'  order by io_time ";
            }

            ds = new DataSet();
            ds = Con.FillDataSet(Sql);
            if (ds.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    int cardtemp = Convert.ToInt32(dr["user_id"].ToString());
                    var presendcardno = string.Format("{0:00000000}", Convert.ToInt32(cardtemp));
                    DataSet dsemp = new DataSet();
                    //sSql = "select a.*,b.departmentname,c.isroundtheclockwork from tblemployee a,tbldepartment b,tblemployeeshiftmaster c where  a.paycode=c.paycode and a.presentcardno='" + presendcardno + "' and a.departmentcode=b.departmentcode and a.dateofjoin<='" + Convert.ToDateTime(dr["RecvTime"]).ToString("MM/dd/yyyy") + "'";
                    sSql = "select a.*,b.departmentname,c.isroundtheclockwork from tblemployee a,tbldepartment b,tblemployeeshiftmaster c where  a.SSN=c.SSN and a.presentcardno='" + presendcardno + "' and a.departmentcode=b.departmentcode   and a.COMPANYCODE=b.CompanyCode and a.CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "'";
                    dsemp = Con.FillDataSet(sSql);
                    if (dsemp.Tables[0].Rows.Count > 0)
                    {
                        DepartmentCode = dsemp.Tables[0].Rows[0]["DepartmentCode"].ToString();
                        CompanyCode = dsemp.Tables[0].Rows[0]["CompanyCode"].ToString();
                        ssn = dsemp.Tables[0].Rows[0]["ssn"].ToString();
                        paycode = dsemp.Tables[0].Rows[0]["paycode"].ToString();
                        RTC = dsemp.Tables[0].Rows[0]["isroundtheclockwork"].ToString();

                        try
                        {
                            Sql = "insert into MachineRawPunch (CardNo, OfficePunch,P_Day, IsManual,PayCode,SSN,CompanyCode) values" +
                                   " ('" + presendcardno + "','" + Convert.ToDateTime(dr["io_time"]).ToString("yyyy-MM-dd HH:mm") + "','N','N','" + paycode.Trim() + "','" + ssn + "','" + CompanyCode.Trim() + "')  ";
                            Result = Con.execute_NonQuery(Sql);
                            if (Result > 0)
                            {
                                if (RTC.ToString() == "N")
                                {
                                    try
                                    {
                                        using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString()))
                                        {
                                            int result = 0;

                                            MyCmd = new OleDbCommand("ProcessBackDate", MyConn);
                                            MyCmd.CommandTimeout = 1800;
                                            MyCmd.CommandType = CommandType.StoredProcedure;
                                            MyCmd.Parameters.AddWithValue("@FromDate", Convert.ToDateTime(dr["AttDateTime"]).ToString("yyyy-MM-dd"));
                                            MyCmd.Parameters.AddWithValue("@ToDate", ProcessDateTo.ToString("yyyy-MM-dd"));
                                            MyCmd.Parameters.AddWithValue("@CompanyCode", CompanyCode);
                                            MyCmd.Parameters.AddWithValue("@DepartmentCode", DepartmentCode.ToString().Trim());
                                            MyCmd.Parameters.AddWithValue("@PayCode", paycode.ToString().Trim());
                                            MyCmd.Parameters.AddWithValue("@SetupId", SetupID);
                                            MyConn.Open();
                                            result = MyCmd.ExecuteNonQuery();
                                            MyConn.Close();

                                        }
                                    }
                                    catch
                                    {


                                    }

                                }
                                else
                                {
                                    try
                                    {
                                        using (MyConn = new OleDbConnection(ConfigurationManager.AppSettings["ConnectionString"].ToString()))
                                        {
                                            int result = 0;
                                            DateTime PrcoessDate = Convert.ToDateTime(dr["AttDateTime"]).AddDays(-1);

                                            MyCmd = new OleDbCommand("ProcessBackDate", MyConn);
                                            MyCmd.CommandTimeout = 1800;
                                            MyCmd.CommandType = CommandType.StoredProcedure;
                                            MyCmd.Parameters.AddWithValue("@FromDate", PrcoessDate.ToString("yyyy-MM-dd"));
                                            MyCmd.Parameters.AddWithValue("@ToDate", System.DateTime.Now);
                                            MyCmd.Parameters.AddWithValue("@CompanyCode", CompanyCode);
                                            MyCmd.Parameters.AddWithValue("@DepartmentCode", DepartmentCode.ToString().Trim());
                                            MyCmd.Parameters.AddWithValue("@PayCode", paycode.ToString().Trim());
                                            MyCmd.Parameters.AddWithValue("@SetupId", SetupID);
                                            MyConn.Open();
                                            result = MyCmd.ExecuteNonQuery();
                                            MyConn.Close();

                                        }
                                    }
                                    catch
                                    {


                                    }




                                }

                            }

                        }

                        catch (Exception Ex)
                        {
                            Error_Occured("Insert MRP", Ex.Message);
                            continue;
                        }

                    }

                }

            }

            PnlProgress.Visible = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "TimeWatch", "window.alert('Attendance Record Calculated Successfully');", true);
        }
        catch (Exception Ex)
        {
            Error_Occured("ProcessAttendance", Ex.Message);
            //continue;
        }

    }
    private void RemoveDuplicatePunch(string PayCode, DateTime DupDate, int DuplicateMin)
    {
        try
        {

            DateTime DupDt = DupDate;
            DataSet Rs = new DataSet();
            string mEmpcd = PayCode;
            DateTime mDate = DateTime.MinValue;
            int S_Dupli = 10;
            S_Dupli = Convert.ToInt32(DuplicateMin);
            DupDt = Convert.ToDateTime(DupDate);
            string sSql = "Select * from Machinerawpunch where officepunch between '" + DupDt.ToString("yyyy-MM-dd") + " 00:00:00' and '" + DupDt.ToString("yyyy-MM-dd") + " 23:59:59' and paycode='" + mEmpcd.ToString() + "' Order By Cardno,officepunch";
            Rs = Con.FillDataSet(sSql);
            if (Rs.Tables[0].Rows.Count > 0)
            {
                for (int id = 0; id < Rs.Tables[0].Rows.Count; id++)
                {
                    mEmpcd = Rs.Tables[0].Rows[id]["Cardno"].ToString().Trim();
                    mDate = Convert.ToDateTime(Rs.Tables[0].Rows[id]["officePunch"]);
                    id = id + 1;
                    if (id == Rs.Tables[0].Rows.Count)
                    {
                        break;
                    }
                    while (mEmpcd == Rs.Tables[0].Rows[id]["Cardno"].ToString().Trim())
                    {
                        if (SimulateDateDiff.DateDiff(SimulateDateDiff.DateInterval.Minute, mDate, Convert.ToDateTime(Rs.Tables[0].Rows[id]["officePunch"])) <= S_Dupli)
                        {

                            sSql = "Delete MachineRawPunch Where Cardno='" + mEmpcd + "'And OfficePunch = '" + Convert.ToDateTime(Rs.Tables[0].Rows[id]["officePunch"]).ToString("yyyy-MM-dd HH:mm") + "'";
                            Con.execute_NonQuery(sSql);

                        }
                        else
                        {
                            mDate = Convert.ToDateTime(Rs.Tables[0].Rows[id]["officePunch"]);
                        }
                        id = id + 1;
                        if (id == Rs.Tables[0].Rows.Count)
                        {
                            break;
                        }
                    }
                }
            }
            else
            {

            }
            return;
        }
        catch (Exception Ex)
        {


        }
    }
    internal static class SimulateIsDate
    {
        internal static bool IsDate(object expression)
        {
            if (expression == null)
                return false;

            System.DateTime testDate;
            return System.DateTime.TryParse(expression.ToString(), out testDate);
        }
    }
    internal static class SimulateVal
    {

        internal static double Val(string expression)
        {
            if (expression == null)
                return 0;

            //try the entire string, then progressively smaller
            //substrings to simulate the behavior of VB's 'Val',
            //which ignores trailing characters after a recognizable value:
            for (int size = expression.Length; size > 0; size--)
            {
                double testDouble;
                if (double.TryParse(expression.Substring(0, size), out testDouble))
                    return testDouble;
            }

            //no value is recognized, so return 0:
            return 0;
        }
        internal static double Val(object expression)
        {
            if (expression == null)
                return 0;

            double testDouble;
            if (double.TryParse(expression.ToString(), out testDouble))
                return testDouble;

            //VB's 'Val' function returns -1 for 'true':
            bool testBool;
            if (bool.TryParse(expression.ToString(), out testBool))
                return testBool ? -1 : 0;

            //VB's 'Val' function returns the day of the month for dates:
            System.DateTime testDate;
            if (System.DateTime.TryParse(expression.ToString(), out testDate))
                return testDate.Day;

            //no value is recognized, so return 0:
            return 0;
        }
        internal static int Val(char expression)
        {
            int testInt;
            if (int.TryParse(expression.ToString(), out testInt))
                return testInt;
            else
                return 0;
        }
    }
    //----------------------------------------------------------------------------------------
    //	Copyright © 2009 - 2010 Tangible Software Solutions Inc.
    //	This class can be used by anyone provided that the copyright notice remains intact.
    //
    //	This class simulates the behavior of the classic VB 'DateDiff' function.
    //----------------------------------------------------------------------------------------
    internal static class SimulateDateDiff
    {
        internal enum DateInterval
        {
            Day,
            DayOfYear,
            Hour,
            Minute,
            Month,
            Quarter,
            Second,
            Weekday,
            WeekOfYear,
            Year
        }


        internal static long DateDiff(DateInterval intervalType, System.DateTime dateOne, System.DateTime dateTwo)
        {
            switch (intervalType)
            {
                case DateInterval.Day:
                case DateInterval.DayOfYear:
                    System.TimeSpan spanForDays = dateTwo - dateOne;
                    return (long)spanForDays.TotalDays;
                case DateInterval.Hour:
                    System.TimeSpan spanForHours = dateTwo - dateOne;
                    return (long)spanForHours.TotalHours;
                case DateInterval.Minute:
                    System.TimeSpan spanForMinutes = dateTwo - dateOne;
                    return (long)spanForMinutes.TotalMinutes;
                case DateInterval.Month:
                    return ((dateTwo.Year - dateOne.Year) * 12) + (dateTwo.Month - dateOne.Month);
                case DateInterval.Quarter:
                    long dateOneQuarter = (long)System.Math.Ceiling(dateOne.Month / 3.0);
                    long dateTwoQuarter = (long)System.Math.Ceiling(dateTwo.Month / 3.0);
                    return (4 * (dateTwo.Year - dateOne.Year)) + dateTwoQuarter - dateOneQuarter;
                case DateInterval.Second:
                    System.TimeSpan spanForSeconds = dateTwo - dateOne;
                    return (long)spanForSeconds.TotalSeconds;
                case DateInterval.Weekday:
                    System.TimeSpan spanForWeekdays = dateTwo - dateOne;
                    return (long)(spanForWeekdays.TotalDays / 7.0);
                case DateInterval.WeekOfYear:
                    System.DateTime dateOneModified = dateOne;
                    System.DateTime dateTwoModified = dateTwo;
                    while (dateTwoModified.DayOfWeek != System.Globalization.DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek)
                    {
                        dateTwoModified = dateTwoModified.AddDays(-1);
                    }
                    while (dateOneModified.DayOfWeek != System.Globalization.DateTimeFormatInfo.CurrentInfo.FirstDayOfWeek)
                    {
                        dateOneModified = dateOneModified.AddDays(-1);
                    }
                    System.TimeSpan spanForWeekOfYear = dateTwoModified - dateOneModified;
                    return (long)(spanForWeekOfYear.TotalDays / 7.0);
                case DateInterval.Year:
                    return dateTwo.Year - dateOne.Year;
                default:
                    return 0;
            }
        }
    }
    protected void TxtFromDate_CalendarDayCellPrepared(object sender, DevExpress.Web.CalendarDayCellPreparedEventArgs e)
    {

        //if (e.Date.DayOfWeek == DayOfWeek.Sunday || e.Date.DayOfWeek == DayOfWeek.Saturday)
        //{
        //    e.Cell.ForeColor = System.Drawing.Color.Red;
        //}
        //else
        //{
        //    e.Cell.ForeColor = System.Drawing.Color.Black;
        //}
    }
}