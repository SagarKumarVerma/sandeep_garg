﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.master" AutoEventWireup="true" CodeFile="frmAutoLeaveAccural.aspx.cs" Inherits="frmAutoLeaveAccural" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <div>
        <center>
        <table align="center" style="width:100%" cellpadding="0" cellspacing="0" >
<tr>
<td style="text-align:center">Auto Leave Accural</td>
</tr>
<tr>
<td align="center" colspan="6" class="tableHeaderCss" style="height:25px;">
    &nbsp;</td>
</tr>
     
<tr>
<td align="center">    
  <dx:ASPxFormLayout ID="formLayout" runat="server"  AlignItemCaptionsInAllGroups="True" UseDefaultPaddings="False">
            <SettingsAdaptivity AdaptivityMode="SingleColumnWindowLimit" SwitchToSingleColumnAtWindowInnerWidth="200" />
            <Items>
             
                <dx:EmptyLayoutItem />
                <dx:LayoutGroup Caption="Leave Year" ColCount="1" Width="200px">
                    <Items>
                        <dx:LayoutItem Caption="Select Year" >
                            <LayoutItemNestedControlCollection>
                                <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True" >
                                   <dx:ASPxDateEdit ID="txtYear" runat="server" EditFormatString="yyyy"></dx:ASPxDateEdit> 
                                </dx:LayoutItemNestedControlContainer>
                            </LayoutItemNestedControlCollection>
                        </dx:LayoutItem>
                    
                    </Items>
                </dx:LayoutGroup>
                
                <dx:LayoutItem ShowCaption="False" CaptionSettings-HorizontalAlign="Right" Width="100%" HorizontalAlign="Center">
                    <LayoutItemNestedControlCollection>
                        <dx:LayoutItemNestedControlContainer runat="server" SupportsDisabledAttribute="True" >
                            <dx:ASPxButton ID="btnCreate" runat="server" Text="Save" Width="100" OnClick="btnCreate_Click"  />
                        </dx:LayoutItemNestedControlContainer>
                    </LayoutItemNestedControlCollection>

<CaptionSettings HorizontalAlign="Right"></CaptionSettings>
                </dx:LayoutItem>
            </Items>
        </dx:ASPxFormLayout>
    </td>
    </tr>
        </table>
            </center>
    </div>
</asp:Content>

