﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using System.Globalization;
using System.Data.SqlClient;

public partial class frmAutoLeaveAccural : System.Web.UI.Page
{
    DateTime TmpDt;
    string StrSql, Tmp;
    OleDbDataReader Dr;
    Class_Connection cn = new Class_Connection();
    int DtAccrualYear;
    string mCode = null;
    bool iFlag = false;
    int iCtr = 0;
    int iD = 0;
    double bleave = 0;
    double pLeave = 0;
    double nleave = 0;
    string lcode = null;
    string lcode1 = null;
    DataSet Rs = null;
    DataSet rsData = null;
    DataSet rsLeaveLedger = null;
    DataSet RSLeaveLedgerb = null;
    DataSet rsLeaveMaster = null;
    DataSet rsTime = null;
    double PrvYearLv = 0;
    double totLeave = 0;
    double CL2PL_AMT = 0;
    string PL_Field = null;
    string PL_Clause = null;

    string btnGoBack = "<br/><br/><input id=\"btnBack\" type=\"button\" value=\"Back\" onclick=\"history.back()\"/>";
    protected void Page_Error(object sender, EventArgs e)
    {
        Exception ex = Server.GetLastError();
        if (ex is HttpRequestValidationException)
        {
            string resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" +
                   "Form doesn't need to contain valid HTML, just anything with opening and closing angled brackets (<...>).<br />" +
                   btnGoBack +
                   "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
        else if (ex is OleDbException)
        {
            string resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" + ex.Message.ToString() + ".<br />" +
                    "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
        else
        {
            string resMsg = "<html><body><span style=\"font-size: 14pt; color: red\">" + ex.Message.ToString() + ".<br />" +
                    "<br /></span></body></html>";
            Response.Write(resMsg);
            Server.ClearError();
            Response.StatusCode = 200;
            Response.End();
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.UserAgent.IndexOf("AppleWebKit") > 0)
            {
                Request.Browser.Adapters.Clear();
            }
            if ( Session["UserName"] == null)
            {
                Session.Abandon();
                Response.Redirect("Login.aspx");
            }
            if (Session["LeaveAccuralAutoVisible"] == null)
            {
                Response.Redirect("Login.aspx");
            }
           
            //DtAccrualYear = DateTime.ParseExact(Session["Today"].ToString().Substring(0, 10), "dd/MM/yyyy", CultureInfo.InvariantCulture);

            DtAccrualYear = Convert.ToInt32(System.DateTime.Now.Year);
            txtYear.Text = DtAccrualYear.ToString("0000");
            //txtYear.Text = DtAccrualYear.Year.ToString("0000");
        }
    }
    protected void btnCreate_Click(object sender, EventArgs e)
    {
        try
        {
            DtAccrualYear = Convert.ToInt32(System.DateTime.Now.Year);
            if (txtYear.Text.Trim() == "" || txtYear.Text.Trim().Length < 4)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Invalid Year');", true);
                return;
            }
            if (Convert.ToInt32(txtYear.Text.Trim()) < DtAccrualYear)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('You can not run Leave Accrual for Last Year(s).');", true);
                return;
            }
            string Msg = "";
            int i = Convert.ToInt16(Session["VALIDYEARS"].ToString().IndexOf(txtYear.Text.ToString()));
            if (i < 0)
            {
                Msg = "<script lanaguage='javascript'>alert('Invalid year. Roster was not created for the specified Year...')</script>";
                Page.RegisterStartupScript("msg", Msg);
                txtYear.Focus();
                return;
            }
            DataSet dsCount = new DataSet();
            StrSql = "select * from tbltimeregister where datepart(yyyy,dateoffice)=" + txtYear.Text.ToString().Trim() + " ";
            dsCount = cn.FillDataSet(StrSql);
            if (dsCount.Tables[0].Rows.Count == 0)
            {
                Msg = "<script lanaguage='javascript'>alert('Invalid year. Roster was not created for the specified Year...')</script>";
                Page.RegisterStartupScript("msg", Msg);
                txtYear.Focus();
                //return;
            }

            StrSql = "select * from tblleaveledger where lyear=" + txtYear.Text.ToString().Trim() + " ";
            dsCount = cn.FillDataSet(StrSql);
            if (dsCount.Tables[0].Rows.Count > 0)
            {
                Msg = "<script lanaguage='javascript'>alert('Leave already Accured for the financial year " + txtYear.Text.ToString() + ".')</script>";
                Page.RegisterStartupScript("msg", Msg);
                txtYear.Focus();
                return;
            }

            AccruLeave();
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('" + ex.Message.ToString() + "');", true);
            return;
        }
    }
    private void AccruLeave()
    {
        try
        {
            DataSet DsEmp = new DataSet();
            DataSet DsLeaveMaster = new DataSet();
            DataSet DsLeaveLedger = new DataSet();
            DataSet DrLeaveLedgerb = new DataSet();

            string field = "";
            string Lcode = "", Lcode1 = "";
            double total = 0.0;
            int Lyr = 0;
            Lyr = Convert.ToInt32(txtYear.Text.ToString().Trim());
            double bleave, nleave, dblLvPresent, dblLvAmount;

            int row = 0;
            StrSql = "";
            StrSql = "Select Paycode,SSN from tblEmployee where Active='Y' and CompanyCode='"+Session["LoginCompany"].ToString().Trim()+"' Order by Paycode";
            DsEmp = cn.FillDataSet(StrSql);

            StrSql = "";
            StrSql = "Select * from Tblleavemaster where CompanyCode='" + Session["LoginCompany"].ToString().Trim() + "' order by leavefield";
            DsLeaveMaster = cn.FillDataSet(StrSql);

            if (DsEmp.Tables[0].Rows.Count > 0)
            {
                for (int c = 0; c < DsEmp.Tables[0].Rows.Count; c++)
                {
                    try
                    {
                        StrSql = "Select * from tblleaveledger where SSN='" + DsEmp.Tables[0].Rows[c]["SSN"].ToString().Trim() + "' And Lyear=" + Lyr;
                        DsLeaveLedger = cn.FillDataSet(StrSql);

                        if (DsLeaveLedger.Tables[0].Rows.Count > 0)
                        {
                            StrSql = "update tblleaveledger set acc_flag='Y' where SSN='" + DsEmp.Tables[0].Rows[c]["SSN"].ToString().Trim() + "' And Lyear=" + Lyr;
                        }
                        else
                        {
                            StrSql = "Insert into tblleaveledger (paycode,lyear,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13,L14,L15,L16,L17,L18,L19,L20,L01_Add,L02_Add,L03_Add,L04_Add,L05_Add,L06_Add,L07_Add,L08_Add,L09_Add,L10_Add,L11_Add,L12_Add,L13_Add,L14_Add,L15_Add,L16_Add,L17_Add,L18_Add,L19_Add,L20_Add,acc_flag,SSN) values('" + DsEmp.Tables[0].Rows[c]["paycode"].ToString().Trim() + "', '" + Lyr.ToString() + "',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'Y','" + DsEmp.Tables[0].Rows[c]["SSN"].ToString().Trim() + "')";
                        }

                        cn.execute_NonQuery(StrSql);
                        row = 0;

                        for (row = 0; row < DsLeaveMaster.Tables[0].Rows.Count; row++)
                        {
                            field = DsLeaveMaster.Tables[0].Rows[row]["leavecode"].ToString().Trim().ToUpper();
                            Lcode = DsLeaveMaster.Tables[0].Rows[row]["leavefield"].ToString().Trim().ToUpper();
                            Lcode1 = DsLeaveMaster.Tables[0].Rows[row]["leavefield"].ToString().Trim().ToUpper() + "_ADD";
                            dblLvPresent = Convert.ToDouble(DsLeaveMaster.Tables[0].Rows[row]["present"].ToString());
                            dblLvAmount = Convert.ToDouble(DsLeaveMaster.Tables[0].Rows[row]["Leave"].ToString());

                            if (DsLeaveMaster.Tables[0].Rows[row]["isleaveaccrual"].ToString().ToUpper() == "Y")
                            {
                                if (DsLeaveMaster.Tables[0].Rows[row]["fixed"].ToString().ToUpper().Trim() == "N")
                                {

                                    StrSql = "select isnull(" + Lcode + ",0) as Lcode,isnull(" + Lcode1 + ",0) as Lcode1 from tblleaveledger where paycode='" + DsEmp.Tables[0].Rows[c]["paycode"].ToString().Trim() + "' and Lyear=" + (Lyr - 1);
                                    DrLeaveLedgerb = cn.FillDataSet(StrSql);
                                    if (DrLeaveLedgerb.Tables[0].Rows.Count > 0)
                                    {
                                        bleave = Convert.ToDouble(DrLeaveLedgerb.Tables[0].Rows[0]["Lcode1"].ToString()) - Convert.ToDouble(DrLeaveLedgerb.Tables[0].Rows[0]["Lcode"].ToString());
                                    }
                                    else
                                    {
                                        bleave = 0;
                                    }
                                    pLeave = Convert.ToDouble(DsLeaveMaster.Tables[0].Rows[row]["leavelimit"].ToString().Trim());
                                    if (bleave > pLeave)
                                    {
                                        if (pLeave > 0)
                                        {
                                            nleave = Convert.ToDouble(DsLeaveMaster.Tables[0].Rows[row]["leavelimit"].ToString().Trim());
                                        }
                                        else
                                        {
                                            nleave = bleave;
                                        }
                                    }
                                    else
                                    {
                                        nleave = bleave;
                                    }
                                    total = nleave;
                                    if (total >= 0)
                                    {
                                        StrSql = "Update tblLeaveLedger Set " + Lcode1 + "=" + total + " where SSN='" + DsEmp.Tables[0].Rows[c]["SSN"].ToString().Trim() + "' And Lyear=" + Lyr;
                                        cn.execute_NonQuery(StrSql);
                                    }
                                }
                            }
                        }
                    }
                    catch 
                    {
                        
                       
                    }
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Auto Leave Accrual run Successfully.');", true);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('" + ex.Message.ToString() + "');", true);
            return;
        }
    }
}